export const Routes =
{
    Login: '/login',
    Logged: '/site',

    LoggedHome: '/site/inicio',
    LoggedInput: '/site/input',
    LoggedDash: '/site/dashboard',
    LoggedCampaign: '/site/campanha',
    LoggedPlanning: '/site/planejamento',
    LoggedSimulator: '/site/simulador',
    LoggedNotification: '/site/notificacao',
    LoggedCommunication: '/site/comunicacao',
}
Object.freeze(Routes);