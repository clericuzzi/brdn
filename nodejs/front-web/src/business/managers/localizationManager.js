const AwsMessages = require('clericuzzi-javascript-aws/managers/messageManager');
const LoginManager = require('clericuzzi-javascript-aws/managers/loginManager');
const LanguageManager = require('clericuzzi-javascript/business/managers/languageManager');
const StringsFormatter = require('clericuzzi-javascript/utils/stringsFormatter');

export default class LocalizationManager
{
    constructor()
    {
        throw new Error(`LocalizationManager should be used statically...`);
    }

    static init()
    {
        this.initPtBr();
    }
    static initPtBr()
    {
        const lang = LanguageManager.SupportedLanguages.PT_br;

        LanguageManager.addString(_SecurityGroups.Admin, `Administrador`, lang);
        LanguageManager.addString(_SecurityGroups.UserOnly, `Usuário`, lang);
        LanguageManager.addString(_SecurityGroups.Management, `Gerente`, lang);

        // login request section
        LanguageManager.addString(LoginManager.UserAttributes.name, `nome`, lang);
        LanguageManager.addString(LoginManager.UserAttributes.email, `email`, lang);
        LanguageManager.addString(LoginManager.UserAttributes.phone, `telefone`, lang);
        LanguageManager.addString(LoginManager.UserAttributes.status, `status`, lang);
        LanguageManager.addString(LoginManager.UserAttributes.enabled, `habilitado`, lang);
        LanguageManager.addString(LoginManager.UserAttributes.unknown, `desconhecido`, lang);
        LanguageManager.addString(LoginManager.UserAttributes.editedIn, `editado em`, lang);
        LanguageManager.addString(LoginManager.UserAttributes.userName, `nome de usuário`, lang);
        LanguageManager.addString(LoginManager.UserAttributes.createdIn, `criado em`, lang);
        LanguageManager.addString(LoginManager.UserAttributes.emailVerified, `email verificado`, lang);
        LanguageManager.addString(LoginManager.UserAttributes.phoneVerified, `telefone verificado`, lang);

        LanguageManager.addString(LoginManager.UserRequestProblems.noAttr, `nenhum atributo retornado`, lang);
        LanguageManager.addString(LoginManager.UserRequestProblems.noAttrName, `o atributo 'nome' não foi solicitado`, lang);
        LanguageManager.addString(LoginManager.UserRequestProblems.noAttrEmail, `o atributo 'email' não foi solicitado`, lang);
        LanguageManager.addString(LoginManager.UserRequestProblems.noAttrPhone, `o atributo 'telefone' não foi solicitado`, lang);
        LanguageManager.addString(LoginManager.UserRequestProblems.noAttrStatus, `o atributo 'status' não foi solicitado`, lang);
        LanguageManager.addString(LoginManager.UserRequestProblems.noAttrUnknown, `o atributo 'desconhecido' não foi solicitado`, lang);
        LanguageManager.addString(LoginManager.UserRequestProblems.noAttrEditedIn, `o atributo 'última modificação' não foi solicitado`, lang);
        LanguageManager.addString(LoginManager.UserRequestProblems.noAttrUsername, `o atributo 'nome de usuário' não foi solicitado`, lang);
        LanguageManager.addString(LoginManager.UserRequestProblems.noAttrCreatedIn, `o atributo 'data de criação' não foi solicitado`, lang);
        LanguageManager.addString(LoginManager.UserRequestProblems.noAttrEmailVerified, `o atributo 'email verificado' não foi solicitado`, lang);
        LanguageManager.addString(LoginManager.UserRequestProblems.noAttrPhoneVerified, `o atributo 'telefone verificado' não foi solicitado`, lang);

        LanguageManager.addString(LoginManager.UserStatus.confirmed, `confirmado`, lang);
        // login request section ended

        LanguageManager.addString(_ProjectStrings.ComponentLoginTradeMark, `Decidimos™ 2019`, lang);
        LanguageManager.addString(_ProjectStrings.ComponentLoginPortalName, `Portal Decidimos`, lang);
        LanguageManager.addString(_ProjectStrings.ComponentLoginRememberMe, `Permanecer logado`, lang);

        LanguageManager.addString(_ProjectStrings.ComponentLoginSignUpHeader, `Preencha o formulário para criar uma nova conta`, lang);
        LanguageManager.addString(_ProjectStrings.ComponentLoginSignUpConfirmation, `Deseja realmente criar um novo usuário?`, lang);

        LanguageManager.addString(_ProjectStrings.ComponentLoginForgotHeader, `Preencha o seu email para que enviemos as informações para redefinição de senha`, lang);
        LanguageManager.addString(_ProjectStrings.ComponentLoginForgotConfirmation, `Deseja realmente iniciar o processo de recuperação de senha?`, lang);
        LanguageManager.addString(_ProjectStrings.ComponentLoginLogoffConfirmation, `Deseja realmente sair?`, lang);

        LanguageManager.addString(LanguageManager.Strings.LimitExceeded, `Limite de tentativas alcançado! Tente novamente mais tarde...`, lang);
        LanguageManager.addString(LanguageManager.Strings.IncorrectLogin, `Usuário ou senha inválidos`, lang);
        LanguageManager.addString(LanguageManager.Strings.LoginAlreadyExists, `Já existe um outro usuário com o email '${StringsFormatter.Tokens.Wildcard}'`, lang);
        LanguageManager.addString(LanguageManager.Strings.LoginWhileUnconfirmedAttempted, `Um email foi enviado para '${StringsFormatter.Tokens.Wildcard}', por favor confirme a conta para poder fazer login`, lang);
        LanguageManager.addString(LanguageManager.Strings.IncorrectLoginForPasswordReset, `Não foi encontrada uma conta para o email '${StringsFormatter.Tokens.Wildcard}'`, lang);

        LanguageManager.addString(LanguageManager.Strings.UnknownError, `Erro desconhecido, entre em contato com o suporte`, lang);
        LanguageManager.addString(LanguageManager.Strings.MessagesNone, `Nenhuma mensagem no momento...`, lang);
        LanguageManager.addString(LanguageManager.Strings.NotificationsNone, `Nenhuma notificação no momento...`, lang);

        LanguageManager.addString(LanguageManager.CommonProperties.Name, `nome`, lang);
        LanguageManager.addString(LanguageManager.CommonProperties.Email, `email`, lang);
        LanguageManager.addString(LanguageManager.CommonProperties.Phone, `telefone`, lang);
        LanguageManager.addString(LanguageManager.CommonProperties.Password, `senha`, lang);
        LanguageManager.addString(LanguageManager.CommonProperties.Description, `descrição`, lang);
        LanguageManager.addString(LanguageManager.CommonProperties.Confirmation, `Confirmação`, lang);
        LanguageManager.addString(LanguageManager.CommonProperties.ResetPassword, `resetar de senha`, lang);
        LanguageManager.addString(LanguageManager.CommonProperties.AccessProfile, `ver perfil`, lang);
        LanguageManager.addString(LanguageManager.CommonProperties.ChangePassword, `alterar senha`, lang);
        LanguageManager.addString(LanguageManager.CommonProperties.PasswordConfirm, `confirmação de senha`, lang);

        LanguageManager.addString(LanguageManager.Strings.CrudConfirmationInsert, `deseja realmente inserir o novo item?`, lang);
        LanguageManager.addString(LanguageManager.Strings.CrudConfirmationUpdate, `deseja realmente alterar o item selecionado?`, lang);
        LanguageManager.addString(LanguageManager.Strings.CrudConfirmationDelete, `deseja realmente remover o(s) item(s) selecionado(s)?`, lang);

        LanguageManager.addString(LanguageManager.CommonProperties.Ok, `ok`, lang);
        LanguageManager.addString(LanguageManager.CommonProperties.No, `não`, lang);
        LanguageManager.addString(LanguageManager.CommonProperties.Yes, `sim`, lang);
        LanguageManager.addString(LanguageManager.CommonProperties.Back, `voltar`, lang);
        LanguageManager.addString(LanguageManager.CommonProperties.Login, `login`, lang);
        LanguageManager.addString(LanguageManager.CommonProperties.Logoff, `sair`, lang);
        LanguageManager.addString(LanguageManager.CommonProperties.Cancel, `cancelar`, lang);

        LanguageManager.addString(LanguageManager.Strings.InputFillNeeded, `preencha corretamente o campo`, lang);
        LanguageManager.addString(LanguageManager.Strings.InputPlaceholderTypeInMale, `digite seu`, lang);
        LanguageManager.addString(LanguageManager.Strings.InputPlaceholderTypeInFemale, `digite sua`, lang);
        LanguageManager.addString(LanguageManager.Strings.InputPlaceholderConfirmInMale, `confirme seu`, lang);
        LanguageManager.addString(LanguageManager.Strings.InputPlaceholderConfirmInFemale, `confirme sua`, lang);

        LanguageManager.addString(LanguageManager.Strings.PasswordDontMatch, `as senhas não conferem`, lang);
        LanguageManager.addString(LanguageManager.Strings.PasswordRuleLight, `sua senha deve conter entre 4 e 16 caracteres`, lang);
        LanguageManager.addString(LanguageManager.Strings.PasswordRuleMedium, `sua senha deve conter entre 8 e 16 caracteres e pelo menos um número, uma letra maíuscula e uma maiúscula`, lang);
        LanguageManager.addString(LanguageManager.Strings.PasswordRuleSecure, `sua senha deve conter entre 8 e 32 caracteres e pelo menos um número, uma letra maíuscula, uma maiúscula e um caractere especial`, lang);

        // aws error codes
        LanguageManager.addString(AwsMessages.AwsErrorCodes.invalidAction, `Ação inválida`, lang);
        LanguageManager.addString(AwsMessages.AwsErrorCodes.optInRequired, `OptIn é necessário`, lang);
        LanguageManager.addString(AwsMessages.AwsErrorCodes.missingAction, `Ação faltando`, lang);
        LanguageManager.addString(AwsMessages.AwsErrorCodes.requestExpired, `Requisição expirada`, lang);
        LanguageManager.addString(AwsMessages.AwsErrorCodes.internalFailure, `Falha interna`, lang);
        LanguageManager.addString(AwsMessages.AwsErrorCodes.validationError, `Erro de validação`, lang);
        LanguageManager.addString(AwsMessages.AwsErrorCodes.missingParameter, `Parâmetro faltando`, lang);
        LanguageManager.addString(AwsMessages.AwsErrorCodes.serviceUnavailable, `Serviço não disponível`, lang);
        LanguageManager.addString(AwsMessages.AwsErrorCodes.incompleteSignature, `Assinatura incompleta`, lang);
        LanguageManager.addString(AwsMessages.AwsErrorCodes.throttlingException, `Exceção de throttling`, lang);
        LanguageManager.addString(AwsMessages.AwsErrorCodes.invalidClientTokenId, `TokenId de cliente inválido`, lang);
        LanguageManager.addString(AwsMessages.AwsErrorCodes.malformedQueryString, `QueryString mal formada`, lang);
        LanguageManager.addString(AwsMessages.AwsErrorCodes.accessDeniedException, `Acesso negado`, lang);
        LanguageManager.addString(AwsMessages.AwsErrorCodes.invalidParameterValue, `Valor de parâmetro inválido`, lang);
        LanguageManager.addString(AwsMessages.AwsErrorCodes.invalidQueryParameter, `Query de parâmetro inválida`, lang);
        LanguageManager.addString(AwsMessages.AwsErrorCodes.userNotFoundException, `Usuário não encontrado`, lang);
        LanguageManager.addString(AwsMessages.AwsErrorCodes.internalErrorException, `Erro interno`, lang);
        LanguageManager.addString(AwsMessages.AwsErrorCodes.notAuthorizedException, `Nâo autorizado`, lang);
        LanguageManager.addString(AwsMessages.AwsErrorCodes.usernameExistsException, `Nome de usuário já existe`, lang);
        LanguageManager.addString(AwsMessages.AwsErrorCodes.tooManyRequestsException, `Muitas requisições`, lang);
        LanguageManager.addString(AwsMessages.AwsErrorCodes.invalidParameterException, `Parâmetro inválido`, lang);
        LanguageManager.addString(AwsMessages.AwsErrorCodes.resourceNotFoundException, `Recurso não encontrado`, lang);
        LanguageManager.addString(AwsMessages.AwsErrorCodes.missingAuthenticationToken, `Token de autenticação faltando`, lang);
        LanguageManager.addString(AwsMessages.AwsErrorCodes.invalidParameterCombination, `Combinação de parâmetros inválida`, lang);

        // aws error messages
        LanguageManager.addString(AwsMessages.AwsErrorMessages.limitExceeded, `Limite excedido`, lang);
        LanguageManager.addString(AwsMessages.AwsErrorMessages.userAlreadyExists, `usuário já existe`, lang);
        LanguageManager.addString(AwsMessages.AwsErrorMessages.passwordAttemptsExceeded, `limite de tentativas excedido`, lang);
        LanguageManager.addString(AwsMessages.AwsErrorMessages.incorrectUsernameOrPasword, `Usuário ou senha inválidos`, lang);
        LanguageManager.addString(AwsMessages.AwsErrorMessages.cannotResetNoVerifiedEmailOrPhone, `Não é possível resetar a senha, não há um email ou telefone verificados`, lang);
    }

    static get Strings() { return _ProjectStrings };
}

const _SecurityGroups =
{
    Admin: `admin`,
    UserOnly: `user-only`,
    Management: `management`,
}
const _ProjectStrings =
{
    ComponentLoginTradeMark: 1001,
    ComponentLoginPortalName: 1002,
    ComponentLoginRememberMe: 1003,
    ComponentLoginButtonLogin: 1004,
    ComponentLoginSignUpHeader: 1005,
    ComponentLoginSignUpConfirmation: 1006,

    ComponentLoginForgotHeader: 1007,
    ComponentLoginForgotConfirmation: 1008,
    ComponentLoginLogoffConfirmation: 1009,
}