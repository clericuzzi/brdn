import React from 'react';

export default class Campaign
{
    get name() { return `CampaignComponent`; }
    get rightPanelWidth() { return 350; }
    get rightPanelHeaderInsert() { return `Nova campanha`; }
    get rightPanelHeaderRemove() { return `Remover campanha`; }
    get rightPanelHeaderUpdate() { return `Alterar campanha`; }
    get rightPanelButtonCancel() { return `cancela`; }
    get rightPanelButtonConfirm() { return `confirma`; }
    get confirmationMessageInsert() { return `Deseja realmente registrar um nova campanha?`; }
    get confirmationMessageUpdate() { return `Deseja realmente alterar a campanha selecionada?`; }
    get confirmationMessageRemove() { return `Deseja realmente remover a(s) campanha(s) selecionada(s)?`; }

    getComponentName = () => `CampaignPanel`;

    getChildren = () =>
    {
        return <div>olar</div>;
    }

    constructor(id, model_criterion_id, point_schema)
    {
        this.id = id;
        this.model_criterion_id = model_criterion_id;
        this.point_schema = point_schema;
    }
}