import 'react-virtualized/styles.css';
import React from 'react';
import ReactDOM from 'react-dom';
import TstComponent from './components/tst/tst';

// import App from './App';
// // import moment from 'moment';
// import ReactDOM from 'react-dom';
// import LocalizationManager from './business/managers/localizationManager';
// import BootstrapAlertOverlay from './clericuzzi-react-web/components/bootstrap/alert/alertOverlay';
// import BootstrapToastOverlay from './clericuzzi-react-web/components/bootstrap/toast/toastOverlay';
// import BootstrapLoaderOverlay from './clericuzzi-react-web/components/bootstrap/loader/loaderOverlay';
// import BootstrapRightPanelOverlay from './clericuzzi-react-web/components/bootstrap/rightPanel/rightPanelOverlay';

// import { BootstrapDefinitions, BootstrapFormInputSizes } from './clericuzzi-react-web/components/bootstrap/definitions';

// const AwsConfig = require('./awsConfig');
// const HttpManager = require('clericuzzi-javascript/business/managers/httpManager');
// const LoginManager = require('clericuzzi-javascript-aws/managers/loginManager');
// const CryptoManager = require('clericuzzi-javascript/business/managers/cryptoManager');

// moment.locale('pt-br');

// HttpManager.rdsDefaultUrls(AwsConfig.LAMBDA_PREFIX);

// CryptoManager.configureHash(`sha256`, `hex`, process.env.REACT_APP_CRYPTO_KEY);
// CryptoManager.configureEncryption(process.env.REACT_APP_CRYPTO_IV, process.env.REACT_APP_CRYPTO_KEY, `aes-256-ctr`, `utf-8`, `base64`, 16);

// LocalizationManager.init();

// BootstrapDefinitions.defaultSize = BootstrapFormInputSizes.Small;
// BootstrapDefinitions.alertOverlay = React.createRef();
// BootstrapDefinitions.toastOverlay = React.createRef();
// BootstrapDefinitions.loaderOverlay = React.createRef();
// BootstrapDefinitions.rightPanelOverlay = React.createRef();

// const alertOverlay = <BootstrapAlertOverlay ref={BootstrapDefinitions.alertOverlay} />;
// const toastOverlay = <BootstrapToastOverlay ref={BootstrapDefinitions.toastOverlay} />;
// const loaderOverlay = <BootstrapLoaderOverlay ref={BootstrapDefinitions.loaderOverlay} />;
// const rightPanelOverlay = <BootstrapRightPanelOverlay ref={BootstrapDefinitions.rightPanelOverlay} />;

setTimeout(() => ReactDOM.render(<TstComponent />, document.getElementById(`root`)), 250);
// setTimeout(() => ReactDOM.render(<BrowserRouter><App /></BrowserRouter>, document.getElementById(`root`)), 250);
// ReactDOM.render(alertOverlay, document.getElementById(`bootstrap-alert-overlay`));
// ReactDOM.render(toastOverlay, document.getElementById(`bootstrap-toast-overlay`));
// ReactDOM.render(loaderOverlay, document.getElementById(`bootstrap-loader-overlay`));
// ReactDOM.render(rightPanelOverlay, document.getElementById(`bootstrap-right-panels-overlay`));
