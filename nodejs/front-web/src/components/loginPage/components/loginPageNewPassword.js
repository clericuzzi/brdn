import React from 'react';
import nextId from 'react-id-generator';

import FormComponent from '../../../clericuzzi-react-web/components/baseComponents/formComponent';
import BootstrapFormInput from '../../../clericuzzi-react-web/components/bootstrap/forms/formInput';
import BootstrapAlertOverlay from '../../../clericuzzi-react-web/components/bootstrap/alert/alertOverlay';

import BootstrapModal, { BootstrapModalTypes } from '../../../clericuzzi-react-web/components/bootstrap/modal/modal';
import BootstrapButton, { BootstrapButtonTypes } from '../../../clericuzzi-react-web/components/bootstrap/button/button';
import { BootstrapFormInputTypes } from '../../../clericuzzi-react-web/components/bootstrap/definitions';

const LoginManager = require('clericuzzi-javascript-aws/managers/loginManager');
const BindingModel = require('clericuzzi-javascript/models/binding/bindingModel');
export default class LoginPageNewPassword extends FormComponent
{
    submitAction = async () =>
    {
        if (this.validate())
        {
            this.disable();

            await LoginManager.setPassword(this.props.newPasswordInfo.login, this.props.newPasswordInfo.password, true);
            BootstrapAlertOverlay.newSuccess(`Senha altera com sucesso, efetue o login`);
            this.enable();
            this.props.toggleAction();
        }
    }

    render()
    {
        return (
            <div key={nextId()} className='grid-content-vertical'>
                <h6>É necessário criar uma senha definitiva</h6>
                <BootstrapFormInput
                    key={nextId()}
                    type={BootstrapFormInputTypes.Email}
                    action={this.submitAction}
                    required={true}
                    prepends={['fas fa-envelope']}
                    name={`email`}
                    placeholder={`digite seu email`}
                    bindingModel={new BindingModel(this.props.newPasswordInfo, `login`)}
                    validationAlert={`preencha corretamente o campo 'email'`}
                />
                <BootstrapFormInput
                    key={nextId()}
                    type={BootstrapFormInputTypes.PasswordSecure}
                    action={this.submitAction}
                    required={true}
                    prepends={['fas fa-lock']}
                    name={`password`}
                    placeholder={`digite sua senha`}
                    bindingModel={new BindingModel(this.props.newPasswordInfo, `password`)}
                    validationAlert={`sua senha deve conter entre 8 e 32 caracteres e pelo menos um número, uma letra maíuscula, uma maiúscula e um caractere especial`}
                    validationAlertPassword={`as senhas não conferem`}
                    passwordValidationProperty='passwordConfirmation'
                />
                <BootstrapFormInput
                    key={nextId()}
                    ref={i => this.pushComponent(i)}
                    type={BootstrapFormInputTypes.PasswordSecure}
                    action={this.submitAction}
                    required={true}
                    prepends={['fas fa-lock']}
                    name={`password_confirmation`}
                    placeholder={`confirme sua senha`}
                    bindingModel={new BindingModel(this.props.newPasswordInfo, `passwordConfirmation`)}
                    validationAlert={`sua senha deve conter entre 8 e 32 caracteres e pelo menos um número, uma letra maíuscula, uma maiúscula e um caractere especial`}
                    validationAlertPassword={`as senhas não conferem`}
                    passwordValidationProperty='password'
                />
                <div className='flex-row center-vertically justify-space-between full-width'>
                    <BootstrapButton
                        ref={i => this.pushComponent(i)}
                        name='button-forgot'
                        text={`redefinir senha`}
                        type={BootstrapButtonTypes.Primary}
                        action={this.submitAction}
                        textBusy={"aguarde..."}
                    />
                    <BootstrapButton
                        ref={i => this.pushComponent(i)}
                        name='button-forgot-back'
                        type={BootstrapButtonTypes.Secondary}
                        text={`volta`}
                        action={this.props.toggleAction}
                        outline={true}
                    />
                </div>

                <BootstrapModal
                    ref={i => this.pushComponent(i)}
                    type={BootstrapModalTypes.OkCancel}
                    name={`confirmação`}
                    body={`Deseja realmente gerar uma nova senha?`}
                    header={`Confirmação`}
                    action={this.submitAction}
                />
            </div>
        );
    }
}
