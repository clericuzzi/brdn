import React from 'react';
import nextId from 'react-id-generator';

import FormComponent from '../../../clericuzzi-react-web/components/baseComponents/formComponent';
import BootstrapFormInput from '../../../clericuzzi-react-web/components/bootstrap/forms/formInput';

import { FontAwesomeIcon } from '../../../clericuzzi-react-web/components/fontawesome/fontAwesomeItem';
import { BootstrapFormInputTypes, BootstrapDefinitions } from '../../../clericuzzi-react-web/components/bootstrap/definitions';

import BootstrapModal, { BootstrapModalTypes } from '../../../clericuzzi-react-web/components/bootstrap/modal/modal';
import BootstrapButton, { BootstrapButtonTypes } from '../../../clericuzzi-react-web/components/bootstrap/button/button';
import BootstrapAlertOverlay from 'src/clericuzzi-react-web/components/bootstrap/alert/alertOverlay';

const TypeUtils = require('clericuzzi-javascript/utils/typeUtils');
const LoginManager = require('clericuzzi-javascript-aws/managers/loginManager');
const BindingModel = require('clericuzzi-javascript/models/binding/bindingModel');
const StringsValidator = require('clericuzzi-javascript/validation/stringsValidator');
const StringsFormatter = require('clericuzzi-javascript/utils/stringsFormatter');
export default class LoginPageNewUser extends FormComponent
{
    signUp = () =>
    {
        if (this.validate())
            this.showModal();
    }
    signUpAction = async () =>
    {
        this.disable();
        const result = await LoginManager.signUp(this.props.signUpInfo.email, this.props.signUpInfo.password, this.props.signUpInfo.name, this.props.signUpInfo.email, this.props.signUpInfo.phone);
        this.enable();

        if (result)
        {
            if (result.user)
            {
                BootstrapAlertOverlay.newSuccess(`Usuário criado com sucesso, faça o login e siga as instruções para acessar a sua conta...`);
                this.props.toggleAction();
            }
            else
            {
                if (TypeUtils.isString(result) || TypeUtils.isNumber(result))
                    BootstrapDefinitions.alertOverlay.current.newDanger(result);
                else if (TypeUtils.isBoolean(result) && result)
                {

                }
                else
                    BootstrapDefinitions.alertOverlay.current.newDanger(`erro desconhecido`);
            }
        }
    }

    render()
    {
        return (
            <div key={nextId()} className='grid-content-vertical'>
                <h6>{`Preencha o formulário para criar uma nova conta`}</h6>
                <BootstrapFormInput
                    key={nextId()}
                    ref={i => this.pushComponent(i)}
                    type={BootstrapFormInputTypes.Text}
                    action={this.signUp}
                    prepends={[FontAwesomeIcon.User]}
                    required={true}
                    name={`nome`}
                    placeholder={`nome`}
                    bindingModel={new BindingModel(this.props.signUpInfo, `name`)}
                />
                <BootstrapFormInput
                    key={nextId()}
                    ref={i => this.pushComponent(i)}
                    type={BootstrapFormInputTypes.Number}
                    action={this.signUp}
                    prepends={[FontAwesomeIcon.Phone]}
                    required={true}
                    regex={StringsValidator.BrRegexes.PhoneFull}
                    formatter={StringsFormatter.brFormatPhoneFull}
                    maxLength={13}
                    name={`telefone`}
                    placeholder={`telefone`}
                    bindingModel={new BindingModel(this.props.signUpInfo, `phone`)}
                />
                <BootstrapFormInput
                    key={nextId()}
                    ref={i => this.pushComponent(i)}
                    type={BootstrapFormInputTypes.Email}
                    action={this.signUp}
                    prepends={[FontAwesomeIcon.Email]}
                    required={true}
                    name={`email`}
                    placeholder={`email`}
                    bindingModel={new BindingModel(this.props.signUpInfo, `email`)}
                />
                <BootstrapFormInput
                    key={nextId()}
                    ref={i => this.pushComponent(i)}
                    type={BootstrapFormInputTypes.PasswordSecure}
                    action={this.signUp}
                    prepends={[FontAwesomeIcon.Lock]}
                    required={true}
                    name={`password`}
                    placeholder={`senha`}
                    validationAlert={`sua senha deve conter entre 8 e 32 caracteres e pelo menos um número, uma letra maíuscula, uma maiúscula e um caractere especial`}
                    validationAlertPassword={`as senhas não conferem`}
                    bindingModel={new BindingModel(this.props.signUpInfo, `password`)}
                    passwordValidationProperty='passwordConfirmation'
                />
                <BootstrapFormInput
                    key={nextId()}
                    ref={i => this.pushComponent(i)}
                    type={BootstrapFormInputTypes.PasswordSecure}
                    action={this.signUp}
                    prepends={[FontAwesomeIcon.Lock]}
                    required={true}
                    name={`password_confirm`}
                    placeholder={`confirme sua senha`}
                    validationAlert={`sua senha deve conter entre 8 e 32 caracteres e pelo menos um número, uma letra maíuscula, uma maiúscula e um caractere especial`}
                    validationAlertPassword={`as senhas não conferem`}
                    bindingModel={new BindingModel(this.props.signUpInfo, `passwordConfirmation`)}
                    passwordValidationProperty='password'
                />
                <div className='flex-row center-vertically justify-space-between full-width'>
                    <BootstrapButton
                        ref={i => this.pushComponent(i)}
                        name='createUserOk'
                        text={`ok`}
                        type={BootstrapButtonTypes.Primary}
                        action={this.signUp}
                        textBusy='criando usuário...'
                    />
                    <BootstrapButton
                        ref={i => this.pushComponent(i)}
                        name='createUserback'
                        type={BootstrapButtonTypes.Secondary}
                        text={`volta`}
                        action={this.props.toggleAction}
                        outline={true}
                    />
                </div>

                <BootstrapModal
                    ref={i => this.pushComponent(i)}
                    type={BootstrapModalTypes.OkCancel}
                    name={`confirmação`}
                    body={`Deseja realmente criar um novo usuário?`}
                    header={`Confirmação`}
                    action={this.signUpAction}
                />
            </div>
        );
    }
}