import React from 'react';
import nextId from 'react-id-generator';

import BootstrapButton, { BootstrapButtonTypes } from '../../../clericuzzi-react-web/components/bootstrap/button/button';

import { Routes } from '../../../business/enums/routes.enum';
import { withRouter } from 'react-router-dom';
import { BootstrapFormInputTypes } from '../../../clericuzzi-react-web/components/bootstrap/definitions';

import RouteManager from '../../../clericuzzi-react-web/business/managers/routesManager';
import FormComponent from '../../../clericuzzi-react-web/components/baseComponents/formComponent';
import BootstrapFormInput from '../../../clericuzzi-react-web/components/bootstrap/forms/formInput';
import BootstrapAlertOverlay from '../../../clericuzzi-react-web/components/bootstrap/alert/alertOverlay';

const LoginManager = require('clericuzzi-javascript-aws/managers/loginManager');
const BindingModel = require('clericuzzi-javascript/models/binding/bindingModel');
const LanguageManager = require('clericuzzi-javascript/business/managers/languageManager');
const StringsValidator = require('clericuzzi-javascript/validation/stringsValidator');

export class LoginPageForm extends FormComponent
{
    submit = async () =>
    {
        if (this.validate())
        {
            this.disable();
            const result = await LoginManager.login(this.props.loginInfo.login, this.props.loginInfo.password);
            this.enable();

            if (result === LoginManager.LoginExceptions.newPasswordRequired)
                this.props.toggleNewPassword(this.props.loginInfo.login);
            else
                BootstrapAlertOverlay.checkResult(result, RouteManager.goTo, this.props, Routes.LoggedHome);
        }
    }

    render()
    {
        if (this.willUnmount)
            return null;
        else
            return (
                <div key={nextId()} className='grid-content-vertical'>
                    <BootstrapFormInput
                        key={nextId()}
                        ref={i => this.pushComponent(i)}
                        name={`email`}
                        type={BootstrapFormInputTypes.Email}
                        action={this.submit}
                        required={true}
                        prepends={['fas fa-envelope']}
                        placeholder={`email`}
                        bindingModel={new BindingModel(this.props.loginInfo, `login`)}
                        validationAlert={`preencha seu email`}
                    />
                    <BootstrapFormInput
                        key={nextId()}
                        ref={i => this.pushComponent(i)}
                        type={BootstrapFormInputTypes.PasswordMedium}
                        name={`senha`}
                        regex={StringsValidator.GeneralExpressions.PasswordMedium}
                        action={this.submit}
                        required={true}
                        prepends={['fas fa-lock']}
                        placeholder={LanguageManager.GetPlaceholderFemale(LanguageManager.CommonProperties.Password)}
                        bindingModel={new BindingModel(this.props.loginInfo, `password`)}
                        validationAlert={LanguageManager.getTranslation(LanguageManager.Strings.PasswordRuleSecure)}
                    />

                    <div className='flex-row center-vertically justify-space-between full-width'>
                        <BootstrapButton
                            ref={i => this.pushComponent(i)}
                            name='login'
                            text='Login'
                            action={this.submit}
                            textBusy='Logando...'
                        />

                        <BootstrapButton
                            ref={i => this.pushComponent(i)}
                            name='forgot'
                            type={BootstrapButtonTypes.Secondary}
                            text='Esqueci minha senha'
                            action={this.props.toggleForgot}
                            outline={true}
                        />
                    </div>
                    <BootstrapButton
                        ref={i => this.pushComponent(i)}
                        type={BootstrapButtonTypes.Link}
                        text='Ainda não tem uma conta?'
                        action={this.props.toggleSignUp}
                        style={{ zIndex: 1 }}
                    />
                </div>
            );
    }
}
export default withRouter(LoginPageForm);