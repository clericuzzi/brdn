import React from 'react';
import BaseComponent from 'clericuzzi-react-web/components/baseComponents/baseComponent';

import GoogleButton from 'clericuzzi-react-web/components/social-media/googleButton';
import FacebookButton from 'clericuzzi-react-web/components/social-media/facebookButton';

export default class LoginPageExternalLogin extends BaseComponent
{
    render()
    {
        return (
            <div className='flex-column full-width'>
                {<GoogleButton />}
                {<FacebookButton />}
            </div>
        );
    }
}