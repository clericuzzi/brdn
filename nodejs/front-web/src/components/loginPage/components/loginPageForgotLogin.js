import React from 'react';
import nextId from 'react-id-generator';

import FormComponent from '../../../clericuzzi-react-web/components/baseComponents/formComponent';
import BootstrapFormInput from '../../../clericuzzi-react-web/components/bootstrap/forms/formInput';
import LocalizationManager from '../../../business/managers/localizationManager';

import BootstrapModal, { BootstrapModalTypes } from '../../../clericuzzi-react-web/components/bootstrap/modal/modal';
import BootstrapButton, { BootstrapButtonTypes } from '../../../clericuzzi-react-web/components/bootstrap/button/button';
import { BootstrapFormInputTypes, BootstrapDefinitions } from '../../../clericuzzi-react-web/components/bootstrap/definitions';

const LoginManager = require('clericuzzi-javascript-aws/managers/loginManager');
const BindingModel = require('clericuzzi-javascript/models/binding/bindingModel');
const LanguageManager = require('clericuzzi-javascript/business/managers/languageManager');
export default class LoginPageForgotLogin extends FormComponent
{
    submit = () =>
    {
        if (this.validate())
            this.showModal();
    }
    submitAction = async () =>
    {
        this.disable();
        const result = await LoginManager.forgotPassword(this.props.loginInfo.forgottenEmail);
        this.enable();

        if (result)
        {
            if (result === true)
                console.log(`submit action successfull: `, result);
            else
                BootstrapDefinitions.alertOverlay.current.newDanger(result, this.props.loginInfo.forgottenEmail);
        }
    }

    render()
    {
        return (
            <div key={nextId()} className='grid-content-vertical'>
                <h6>{LanguageManager.getTranslation(LocalizationManager.Strings.ComponentLoginForgotHeader)}</h6>
                <BootstrapFormInput
                    key={nextId()}
                    ref={i => this.pushComponent(i)}
                    type={BootstrapFormInputTypes.Email}
                    action={this.submit}
                    required={true}
                    prepends={['fas fa-envelope']}
                    name='forgot-email'
                    placeholder={LanguageManager.GetPlaceholderMale(LanguageManager.CommonProperties.Email)}
                    bindingModel={new BindingModel(this.props.loginInfo, `forgottenEmail`)}
                />
                <div className='flex-row center-vertically justify-space-between full-width'>
                    <BootstrapButton
                        ref={i => this.pushComponent(i)}
                        name='button-forgot'
                        text={LanguageManager.getTranslation(LanguageManager.CommonProperties.ResetPassword)}
                        type={BootstrapButtonTypes.Primary}
                        action={this.submit}
                    />
                    <BootstrapButton
                        ref={i => this.pushComponent(i)}
                        name='button-forgot-back'
                        type={BootstrapButtonTypes.Secondary}
                        text={LanguageManager.getTranslation(LanguageManager.CommonProperties.Back)}
                        action={this.props.toggleAction}
                        outline={true}
                    />
                </div>

                <BootstrapModal
                    ref={i => this.pushComponent(i)}
                    type={BootstrapModalTypes.OkCancel}
                    name={LanguageManager.CommonProperties.Confirmation}
                    body={LanguageManager.getTranslation(LocalizationManager.Strings.ComponentLoginForgotConfirmation)}
                    header={LanguageManager.getTranslation(LanguageManager.CommonProperties.Confirmation)}
                    action={this.submitAction}
                />
            </div>
        );
    }
}