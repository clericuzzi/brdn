import React, { useState, useEffect } from 'react';
import nextId from 'react-id-generator';

import RouteManager from '../../clericuzzi-react-web/business/managers/routesManager';
import LoginPageForm from './components/loginPageForm';
import LoginPageNewUser from './components/loginPageNewUser';
import LoginPageForgotLogin from './components/loginPageForgotLogin';
import LoginPageNewPassword from './components/loginPageNewPassword';

import { Routes } from '../../business/enums/routes.enum';
import { CSSTransition } from 'react-transition-group';

const LoginManager = require('clericuzzi-javascript-aws/managers/loginManager');

const LoginPageStates =
{
    Login: 0,
    SignUp: 1,
    NewPassword: 2,
    ForgotPassword: 3,
};
Object.freeze(LoginPageStates);

const _loginInfo = { login: null, password: null, forgottenEmail: null, persistent: true };
const _signUpInfo = { name: null, phone: null, email: null, password: null, passwordConfirmation: null };
const _newPasswordInfo = { login: null, password: null, passwordConfirmation: null };

const LoginPage = props =>
{
    const [currentComponent, setCurrentComponent] = useState(LoginPageStates.Login);
    useEffect(() =>
    {
        (async function checkLogin()
        {
            await LoginManager.checkLogged();
            if (LoginManager.isLogged())
                RouteManager.goTo(props, Routes.Logged);
        })();

        return () => { clearInfo(); };
    }, [props]);

    const clearInfo = () =>
    {
        _loginInfo.login = _loginInfo.password = _loginInfo.forgottenEmail =
            _signUpInfo.name = _signUpInfo.phone = _signUpInfo.email = _signUpInfo.password = _signUpInfo.passwordConfirmation =
            _newPasswordInfo.login = _newPasswordInfo.password = _newPasswordInfo.passwordConfirmation = null;
    }

    const toggleSignUp = e =>
    {
        setCurrentComponent(currentComponent === LoginPageStates.SignUp ? LoginPageStates.Login : LoginPageStates.SignUp);
    }
    const toggleNewPassword = login =>
    {
        setCurrentComponent(currentComponent === LoginPageStates.NewPassword ? LoginPageStates.Login : LoginPageStates.NewPassword);
    }
    const toggleForgotPassowrd = e =>
    {
        setCurrentComponent(currentComponent === LoginPageStates.ForgotPassword ? LoginPageStates.Login : LoginPageStates.ForgotPassword);
    }

    const renderLoginForm = () =>
    {
        return (
            <div key={nextId()} className='login-container'>
                <LoginPageForm
                    loginInfo={_loginInfo}
                    toggleSignUp={toggleSignUp}
                    toggleForgot={toggleForgotPassowrd}
                    toggleNewPassword={toggleNewPassword}
                />
                {/* <LoginPageExternalLogin /> */}
            </div>
        );
    }
    const renderSignUpForm = () =>
    {
        return (
            <LoginPageNewUser
                signUpInfo={_signUpInfo}
                toggleAction={toggleSignUp}
            />
        );
    }
    const renderNewPasswordForm = () =>
    {
        return (
            <LoginPageNewPassword
                newPasswordInfo={_newPasswordInfo}
                toggleAction={toggleNewPassword}
            />
        );
    }
    const renderForgotPasswordForm = () =>
    {
        return (
            <LoginPageForgotLogin
                loginInfo={_loginInfo}
                toggleAction={toggleForgotPassowrd.bind(this)}
            />
        );
    }
    const renderForm = () =>
    {
        const widthStyle = { width: `300px` };
        const classnames = `flex-column visibility`;
        return (
            <div className='flex-column login-container' style={widthStyle}>
                <CSSTransition in={currentComponent === LoginPageStates.Login} timeout={200} classNames={classnames} appear={true} unmountOnExit={true}>
                    {renderLoginForm()}
                </CSSTransition>
                <CSSTransition in={currentComponent === LoginPageStates.SignUp} timeout={200} classNames={classnames} appear={true} unmountOnExit={true}>
                    {renderSignUpForm()}
                </CSSTransition>
                <CSSTransition in={currentComponent === LoginPageStates.ForgotPassword} timeout={200} classNames={classnames} appear={true} unmountOnExit={true}>
                    {renderForgotPasswordForm()}
                </CSSTransition>
                <CSSTransition in={currentComponent === LoginPageStates.NewPassword} timeout={200} classNames={classnames} appear={true} unmountOnExit={true}>
                    {renderNewPasswordForm()}
                </CSSTransition>
            </div>
        );
    }
    const renderImage = () =>
    {
        return (
            <div className='flex-column center'>
                <div className='login-image'>
                    <h1 style={{ color: `white` }}>D</h1>
                </div>
                <h4>Portal Decidimos</h4>
            </div>
        );
    }
    const renderWrapper = () =>
    {
        return (
            <div className='login-page login-background-tint login-background-image'>
                <div className='login-area flex-column'>
                    <div className='login-box'>
                        {renderImage()}
                        {renderForm()}
                    </div>
                    <div className='login-page-footer center-vertically justify-space-between'>
                        <div style={{ 'marginRight': '10px' }}>
                            Decidimos™ 2019
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    return (
        <div className='full-body'>
            {renderWrapper()}
        </div>
    );
}

export default LoginPage;