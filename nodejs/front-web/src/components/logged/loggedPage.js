import React, { Suspense, Component } from 'react';

import Loader from '../../clericuzzi-react-web/components/loader/loader';

import { Routes } from '../../business/enums/routes.enum';
import { BootstrapSideBarItemModel } from '../../clericuzzi-react-web/components/bootstrap/bars/side/sideBarItem';
import { withRouter, Route, Switch, Redirect } from 'react-router-dom';

import RouteManager from '../../clericuzzi-react-web/business/managers/routesManager';
import BootstrapSideBar from '../../clericuzzi-react-web/components/bootstrap/bars/side/sideBar';
import BootstrapHeaderBar from '../../clericuzzi-react-web/components/bootstrap/bars/header/headerBar';

// components
import Home from './components/home';
import Planning from './components/planning';
import Dashboard from './components/dashboard';
import Simulador from './components/simulator';
import ProjectCrud from './components/projectCrud';
import Notification from './components/notification';
import Communication from './components/communication';
import CampaignScreen from './components/campaignScreen';
import InputDataScreen from './components/inputData';

const _SideBarItems =
{
    Crud: `crud`,
    Dash: `dash`,
    Home: `home`,
    Campaign: `campaign`,
    Planning: `planning`,
    Simulator: `simulator`,
    Notification: `notification`,
    Communication: `communication`,
}
Object.freeze(_SideBarItems);

export class LoggedPage extends Component
{
    goToRoute = route =>
    {
        RouteManager.goTo(this.props, route);
    }

    logoff = () =>
    {
        RouteManager.goTo(this.props, Routes.Login);
    }

    mountItems()
    {
        const items = [];

        items.push(new BootstrapSideBarItemModel(`Input de dados`, this.goToRoute.bind(this), _SideBarItems.Crud, `fas fa-cog`, Routes.LoggedInput));
        items.push(new BootstrapSideBarItemModel(`Indicadores atuais`, this.goToRoute.bind(this), _SideBarItems.Home, `fas fa-home`, Routes.LoggedHome));
        items.push(new BootstrapSideBarItemModel(`Campanhas`, this.goToRoute.bind(this), _SideBarItems.Campaign, `fas fa-trophy`, Routes.LoggedCampaign));
        items.push(new BootstrapSideBarItemModel(`Comunicações`, this.goToRoute.bind(this), _SideBarItems.Communication, `fas fa-bullhorn`, Routes.LoggedCommunication));
        // items.push(new BootstrapSideBarItemModel(`Dashboard`, this.goToRoute.bind(this), _SideBarItems.Dash, `fas fa-chart-bar`, Routes.LoggedDash));
        items.push(new BootstrapSideBarItemModel(`Planejamento`, this.goToRoute.bind(this), _SideBarItems.Planning, `fas fa-clipboard`, Routes.LoggedPlanning));
        // items.push(new BootstrapSideBarItemModel(`Notificações`, this.goToRoute.bind(this), _SideBarItems.Notification, `fas fa-bell`, Routes.LoggedNotification));
        items.push(new BootstrapSideBarItemModel(`Simulador`, this.goToRoute.bind(this), _SideBarItems.Simulator, `fas fa-calculator`, Routes.LoggedSimulator));

        return items;
    }

    render()
    {
        return (
            <Suspense fallback={<Loader />} >
                <div className='full-size'>
                    <div className='full-size flex-row'>
                        <BootstrapSideBar items={this.mountItems()} />
                        <div className='bootstrap-content-body'>
                            <div className="full-size flex-column">
                                <BootstrapHeaderBar logoffAction={this.logoff} />
                                <div className="bootstrap-content-body-content">
                                    <Switch>
                                        <Route path={Routes.LoggedHome} component={Home} />
                                        <Route path={Routes.LoggedInput} component={InputDataScreen} />
                                        <Route path={Routes.LoggedDash} component={Dashboard} />
                                        <Route path={Routes.LoggedCampaign} component={CampaignScreen} />
                                        <Route path={Routes.LoggedPlanning} component={Planning} />
                                        <Route path={Routes.LoggedSimulator} component={Simulador} />
                                        <Route path={Routes.LoggedNotification} component={Notification} />
                                        <Route path={Routes.LoggedCommunication} component={Communication} />
                                        <Redirect to={Routes.LoggedInput} />
                                    </Switch>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Suspense>
        );
    }
}
export default withRouter(LoggedPage);