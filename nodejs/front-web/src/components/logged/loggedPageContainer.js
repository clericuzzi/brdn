import React, { lazy, Component } from 'react';
import { Routes } from "../../business/enums/routes.enum";
import { Redirect, Route, withRouter, BrowserRouter } from "react-router-dom";
import { BootstrapComponentName } from 'clericuzzi-react-web/components/bootstrap/definitions';

const componentHome = lazy(() => import('./components/home'));
const componentDash = lazy(() => import('./components/dashboard'));
const componentInput = lazy(() => import('./components/inputData'));
const componentCampaign = lazy(() => import('./components/campaignScreen'));
const componentPlanning = lazy(() => import('./components/planning'));
const componentSimulator = lazy(() => import('./components/simulator'));
const componentNotification = lazy(() => import('./components/notification'));
const componentCommunication = lazy(() => import('./components/communication'));

export class LoggedPageContainer extends Component
{
    constructor(props)
    {
        super(props);
        this.componentType = BootstrapComponentName(`LoggedPageContainer`);
        this.state = {
        };
    }
    render()
    {
        return (
            <div className="full-size">
                <BrowserRouter>
                    <Route path={Routes.LoggedHome} component={componentHome} />
                    <Route path={Routes.LoggedInput} component={componentInput} />
                    <Route path={Routes.LoggedDash} component={componentDash} />
                    <Route path={Routes.LoggedCampaign} component={componentCampaign} />
                    <Route path={Routes.LoggedPlanning} component={componentPlanning} />
                    <Route path={Routes.LoggedSimulator} component={componentSimulator} />
                    <Route path={Routes.LoggedNotification} component={componentNotification} />
                    <Route exact path={Routes.LoggedCommunication} component={componentCommunication} />
                    <Redirect to={Routes.LoggedHome} />
                </BrowserRouter>
            </div>
        );
    }
}
export default withRouter(LoggedPageContainer);