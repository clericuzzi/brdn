import React, { useEffect, useState } from 'react';
import BootstrapHeaderBar from '../../../clericuzzi-react-web/components/bootstrap/bars/header/headerBar';
import BootstrapFormInput from 'src/clericuzzi-react-web/components/bootstrap/forms/formInput';

import { CSSTransition } from 'react-transition-group';

import Loader from 'src/clericuzzi-react-web/components/loader/loader';
import CrudList from 'src/clericuzzi-react-web/components/bootstrap/crud/components/list/crudList';
import CampaignScreenListItem from 'src/components/listItems/campaignScreen.listItem';
import nextId from 'react-id-generator';
import BootstrapAlertOverlay from 'src/clericuzzi-react-web/components/bootstrap/alert/alertOverlay';

const loginManager = require('clericuzzi-javascript-aws/managers/loginManager');
const httpManager = require('clericuzzi-javascript/business/managers/httpManager');
const dateUtils = require('clericuzzi-javascript/utils/dateUtils');
const typeUtils = require('clericuzzi-javascript/utils/typeUtils');

const CampaignScreen = props =>  
{
    const properties = { dateTo: dateUtils.todayFormatted(), dateFrom: dateUtils.firstOfMonthFormatted(), token: null };

    const [isBusy, setBusy] = useState(false);
    const [campaignId, setCampaignId] = useState(null);
    const [campaignData, setCampaignData] = useState([]);
    const [campaignList, setCampaignList] = useState([]);
    const [campaignDataLength, setCampaignDataLength] = useState(0);

    const defaultClass = `flex-column visibility`;

    useEffect(() =>
    {
        setTimeout(() => BootstrapHeaderBar.setTitle(`Campanhas`), 500);
        (async function fetchCampaignData()
        {
            setBusy(true);

            const result = await httpManager.post({ field: `name`, table: `campaign` }, `https://g9qeehd7e1.execute-api.us-east-1.amazonaws.com/dev/rds-mysql-simple-list`);
            if (typeUtils.isArrayAndValid(result))
                for (const resultItem of result)
                    resultItem.value = resultItem.id;

            setCampaignList(result);
            setBusy(false);
        })();
    }, [campaignId]);

    const fetchData = async (campaignIdValue) =>
    {
        setBusy(true);

        if (typeUtils.isValid(campaignIdValue))
        {
            const campaignResponse = await httpManager.post({ campaign_id: campaignIdValue, token: loginManager.getToken() }, `https://g9qeehd7e1.execute-api.us-east-1.amazonaws.com/dev/module-campaign-screen`);
            if (typeUtils.isValid(campaignResponse) && typeUtils.isArrayAndValid(campaignResponse.campaignData))
            {
                setCampaignId(campaignIdValue);
                setCampaignData(campaignResponse.campaignData);
                setCampaignDataLength(campaignResponse.campaignData.length);
            }
            else
            {
                setCampaignId(null);
                setCampaignData([]);
                setCampaignDataLength(0);
            }
        }
        setBusy(false);
    }

    const filter = () =>
    {
        console.log(`filter: `, properties);
    }

    const campaignChanged = async selectedItem =>
    {
        await refresh(selectedItem.id);
    }

    const refresh = async (campaignIdValue = null) =>
    {
        if (!campaignIdValue)
            campaignIdValue = campaignId;

        if (typeUtils.isValid(campaignIdValue))
            await fetchData(campaignIdValue);
        else
            BootstrapAlertOverlay.newDanger(`Selecione uma campanha...`);
    }


    const rowRenderer = (item, info) => <CampaignScreenListItem key={nextId()} item={item} info={info} totalCount={campaignDataLength} />;
    const renderContent = () =>
    {
        if (typeUtils.isArrayAndValid(campaignData))
            return (
                <CrudList
                    data={campaignData}
                    display={(item, info) => rowRenderer(item, info)}
                    rowCount={campaignData.length}
                    rowHeight={45}
                />);
        else
        {
            if (typeUtils.isValid(campaignId))
                return <div className="full-size flex-column center">Sem resultados...</div>;
            else
                return <div className="full-size flex-column center">Selecione uma campanha...</div>;
        }
    }
    const render = () =>
    {
        return (
            <div className="full-size flex-column">
                <div className="bottom-margin">
                    {BootstrapFormInput.newSelect(campaignList, campaignChanged, `Selecione uma campanha`, `Não há mais campanhas cadastradas...`)}
                </div>
                <div className="bottom-margin flex-row justify-space-between center">
                    <div className="grid-spaced center" style={{ gridTemplateColumns: `auto auto auto`, width: `400px` }}>
                        <h6>Período dos dados</h6>
                        {BootstrapFormInput.newDate(filter, properties, `dateFrom`, `de`)}
                        {BootstrapFormInput.newDate(filter, properties, `dateTo`, `até`)}
                    </div>
                    <div className="flex-row flex-end">
                        {/* <i className="fas fa-sync-alt fa-lg pointer bootstrap-action-item" onClick={refresh} title="Recarregar dados" /> */}
                        {/* <i className="ml-2 fas fa-plus fa-lg pointer bootstrap-action-item" onClick={newCampaign} title="Nova campanha" /> */}
                    </div>
                </div>
                <CSSTransition in={isBusy} timeout={200} classNames={defaultClass} appear={true} unmountOnExit={true}>
                    <Loader />
                </CSSTransition>
                <CSSTransition in={!isBusy} timeout={200} classNames={defaultClass} appear={true} unmountOnExit={true}>
                    {renderContent()}
                </CSSTransition>
            </div>
        );
    }

    return render();
}

export default CampaignScreen;