import React, { useEffect } from 'react';
import BootstrapHeaderBar from '../../../clericuzzi-react-web/components/bootstrap/bars/header/headerBar';

const Notification = props =>  
{
    useEffect(() =>
    {
        setTimeout(() => BootstrapHeaderBar.setTitle(`Notificações`), 500);
    }, []);

    return (
        <span>Notificações</span>
    );
}

export default Notification;