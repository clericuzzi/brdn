import React, { useEffect } from 'react';
import BootstrapHeaderBar from '../../../clericuzzi-react-web/components/bootstrap/bars/header/headerBar';

const Dashboard = props =>  
{
    useEffect(() =>
    {
        setTimeout(() => BootstrapHeaderBar.setTitle(`Dashboard`), 500);
    }, []);

    return (
        <span>Dashboard</span>
    );
}

export default Dashboard;