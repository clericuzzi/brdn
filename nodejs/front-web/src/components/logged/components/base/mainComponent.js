import { Component } from 'react';
import BootstrapHeaderBar from '../../../clericuzzi-react-web/components/bootstrap/bars/header/headerBar';

export default class MainComponent extends Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            title: props.title,
        }
    }
    componentDidMount()
    {
        setTimeout(() => BootstrapHeaderBar.setTitle(this.state.title), 500);
    }
}