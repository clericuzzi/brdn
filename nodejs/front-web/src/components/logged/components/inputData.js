import React, { useEffect, useState } from 'react';
import BootstrapHeaderBar from '../../../clericuzzi-react-web/components/bootstrap/bars/header/headerBar';
import BootstrapFormInput from 'src/clericuzzi-react-web/components/bootstrap/forms/formInput';

import { CSSTransition } from 'react-transition-group';

import Loader from 'src/clericuzzi-react-web/components/loader/loader';
import CrudList from 'src/clericuzzi-react-web/components/bootstrap/crud/components/list/crudList';
import nextId from 'react-id-generator';
import BootstrapAlertOverlay from 'src/clericuzzi-react-web/components/bootstrap/alert/alertOverlay';
import DataInputListItem from 'src/components/listItems/dataInput.ListItem';

const loginManager = require('clericuzzi-javascript-aws/managers/loginManager');
const httpManager = require('clericuzzi-javascript/business/managers/httpManager');
const dateUtils = require('clericuzzi-javascript/utils/dateUtils');
const typeUtils = require('clericuzzi-javascript/utils/typeUtils');
const SqlBuilder = require('clericuzzi-javascript/models/sql/sqlBuilder');

const InputDataScreen = props =>  
{
    const properties = { dateTo: dateUtils.todayFormatted(), dateFrom: dateUtils.firstOfMonthFormatted(), token: null };

    const [isBusy, setBusy] = useState(false);
    const [inputData, setInputData] = useState([]);
    const [dataSourceId, setDataSourceId] = useState(null);
    const [dataSourceList, setDataSourceList] = useState([]);
    const [dataSourceLength, setDataSourceLength] = useState(0);

    const defaultClass = `flex-column visibility`;

    useEffect(() =>
    {
        setTimeout(() => BootstrapHeaderBar.setTitle(`Input de dados`), 500);
        (async function fetchCampaignData()
        {
            setBusy(true);

            const result = await httpManager.post({ field: `name`, table: `data_source` }, `https://g9qeehd7e1.execute-api.us-east-1.amazonaws.com/dev/rds-mysql-simple-list`);
            if (typeUtils.isArrayAndValid(result))
                for (const resultItem of result)
                    resultItem.value = resultItem.id;

            setDataSourceList(result);
            setBusy(false);
        })();
    }, [dataSourceId]);

    const fetchData = async (dataSourceIdValue) =>
    {
        setBusy(true);

        if (typeUtils.isValid(dataSourceIdValue))
        {
            const builder = new SqlBuilder();
            builder
                .from(`data_input`, `di`)
                .select(`di`, `id`)
                .select(`c`, `name`)
                .select(`di`, `amount`)
                .select(`di`, `date`)
                .join(`criterion`, `id`, `criterion_id`, `c`, `di`)
                .where(`di`, `data_source_id`, dataSourceIdValue)

            const response = await httpManager.post({ queryBuilder: builder }, `https://g9qeehd7e1.execute-api.us-east-1.amazonaws.com/dev/rds-mysql-query`);
            if (typeUtils.isValid(response) && typeUtils.isArrayAndValid(response.items))
            {
                setDataSourceId(dataSourceIdValue);
                setInputData(response.items);
                setDataSourceLength(response.items.length);
            }
            else
            {
                setDataSourceId(null);
                setInputData([]);
                setDataSourceLength(0);
            }
        }
        setBusy(false);
    }

    const filter = () =>
    {
        console.log(`filter: `, properties);
    }

    const campaignChanged = async selectedItem =>
    {
        await refresh(selectedItem.id);
    }

    const refresh = async (campaignIdValue = null) =>
    {
        if (!campaignIdValue)
            campaignIdValue = dataSourceId;

        if (typeUtils.isValid(campaignIdValue))
            await fetchData(campaignIdValue);
        else
            BootstrapAlertOverlay.newDanger(`Selecione uma campanha...`);
    }


    const rowRenderer = (item, info) => <DataInputListItem key={nextId()} item={item} info={info} totalCount={dataSourceLength} />;
    const renderContent = () =>
    {
        if (typeUtils.isArrayAndValid(inputData))
            return (
                <CrudList
                    data={inputData}
                    display={(item, info) => rowRenderer(item, info)}
                    rowCount={inputData.length}
                    rowHeight={45}
                />);
        else
        {
            if (typeUtils.isValid(dataSourceId))
                return <div className="full-size flex-column center">Sem resultados...</div>;
            else
                return <div className="full-size flex-column center">Selecione uma fonte de dados...</div>;
        }
    }
    const render = () =>
    {
        return (
            <div className="full-size flex-column">
                <div className="bottom-margin">
                    {BootstrapFormInput.newSelect(dataSourceList, campaignChanged, `Selecione uma fonde de dados`, `Não há mais fontes de dados cadastradas...`)}
                </div>
                <div className="bottom-margin flex-row justify-space-between center">
                    <div className="grid-spaced center" style={{ gridTemplateColumns: `auto auto auto`, width: `400px` }}>
                        <h6>Período dos dados</h6>
                        {BootstrapFormInput.newDate(filter, properties, `dateFrom`, `de`)}
                        {BootstrapFormInput.newDate(filter, properties, `dateTo`, `até`)}
                    </div>
                    <div className="flex-row flex-end">
                        {/* <i className="fas fa-sync-alt fa-lg pointer bootstrap-action-item" onClick={refresh} title="Recarregar dados" /> */}
                        {/* <i className="ml-2 fas fa-plus fa-lg pointer bootstrap-action-item" onClick={newCampaign} title="Nova campanha" /> */}
                    </div>
                </div>
                <CSSTransition in={isBusy} timeout={200} classNames={defaultClass} appear={true} unmountOnExit={true}>
                    <Loader />
                </CSSTransition>
                <CSSTransition in={!isBusy} timeout={200} classNames={defaultClass} appear={true} unmountOnExit={true}>
                    {renderContent()}
                </CSSTransition>
            </div>
        );
    }

    return render();
}

export default InputDataScreen;