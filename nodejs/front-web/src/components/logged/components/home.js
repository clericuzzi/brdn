import nextId from 'react-id-generator';
import React, { useEffect, useState } from 'react';

import { CSSTransition } from 'react-transition-group';

import Loader from 'src/clericuzzi-react-web/components/loader/loader';

import CrudList from 'src/clericuzzi-react-web/components/bootstrap/crud/components/list/crudList';
import HttpManager from 'clericuzzi-javascript/business/managers/httpManager';
import BootstrapHeaderBar from 'src/clericuzzi-react-web/components/bootstrap/bars/header/headerBar';
import HomeScreenListItem from 'src/components/listItems/homeScreen.listItem';

import BootstrapFormInput from 'src/clericuzzi-react-web/components/bootstrap/forms/formInput';
import BootstrapAlertOverlay from 'src/clericuzzi-react-web/components/bootstrap/alert/alertOverlay';

const dateUtils = require('clericuzzi-javascript/utils/dateUtils');
const typeUtils = require('clericuzzi-javascript/utils/typeUtils');
const loginManager = require('clericuzzi-javascript-aws/managers/loginManager');

const Home = props =>  
{
    const properties = { dateTo: dateUtils.todayFormatted(), dateFrom: dateUtils.firstOfMonthFormatted(), token: null };

    const [data, setData] = useState([]);
    const [isBusy, setBusy] = useState(false);
    const [dataLength, setDataLength] = useState(0);

    const defaultClass = `flex-column visibility`;

    useEffect(() =>
    {
        setTimeout(() => BootstrapHeaderBar.setTitle(`Indicadores atuais`), 500);

        (async function getInitialData()
        {
            await fetchData();
        })();
    }, []);

    const fetchData = async () =>
    {
        if (!isBusy)
        {
            try
            {
                setBusy(true);

                const message = {
                    token: loginManager.getToken(),
                    dateTo: dateUtils.dateTo(properties.dateTo),
                    dateFrom: dateUtils.dateFrom(properties.dateFrom),
                }
                const result = await HttpManager.post(message, `https://g9qeehd7e1.execute-api.us-east-1.amazonaws.com/dev/module-main-screen`)
                if (result && typeUtils.isArrayAndValid(result.ranking))
                    setDataLength(result.ranking.length);
                else
                    setDataLength(0);

                setData(result);
                setBusy(false);
            }
            catch (err)
            {
                console.log(`ERROR ON FETCH: `, err.message);
            }
            finally
            {
                setBusy(false);
            }
        }
        else
            BootstrapAlertOverlay.newDanger(`Aguarde o retorno dos dados...`);
    }

    const rowRenderer = (item, info) =>
    {
        return <HomeScreenListItem key={nextId()} item={item} info={info} totalCount={dataLength} />;
    }

    const filter = () =>
    {
        console.log(`filter: `, properties);
    }

    const getRanking = () =>
    {
        if (data && typeUtils.isArrayAndValid(data.ranking))
            return data.ranking;
        else
            return null;
    }

    const renderContent = (ranking) =>
    {
        if (typeUtils.isArrayAndValid(ranking))
            return (
                <CrudList
                    data={ranking}
                    display={(item, info) => rowRenderer(item, info)}
                    rowCount={ranking.length}
                    rowHeight={45}
                />);
        else
            return <div className="full-size flex-column center">Sem resultados...</div>;
    }

    const render = () =>
    {
        const ranking = getRanking();
        return (
            <div className="full-size flex-column">
                <div className="bottom-margin flex-row justify-space-between center">
                    <div className="grid-spaced center" style={{ gridTemplateColumns: `auto auto auto`, width: `400px` }}>
                        <h6>Período dos dados</h6>
                        {BootstrapFormInput.newDate(filter, properties, `dateFrom`, `de`)}
                        {BootstrapFormInput.newDate(filter, properties, `dateTo`, `até`)}
                    </div>
                    <i className="mr-2 fas fa-sync-alt fa-lg pointer bootstrap-action-item" onClick={fetchData} title="Recarregar dados" />
                </div>
                <CSSTransition in={isBusy} timeout={200} classNames={defaultClass} appear={true} unmountOnExit={true}>
                    <Loader />
                </CSSTransition>
                <CSSTransition in={!isBusy} timeout={200} classNames={defaultClass} appear={true} unmountOnExit={true}>
                    {renderContent(ranking)}
                </CSSTransition>
            </div >
        );
    }

    return render();
}

export default Home;