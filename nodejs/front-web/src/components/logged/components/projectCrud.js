import React, { useState, useEffect } from 'react';
import nextId from 'react-id-generator';
import BootstrapCrud, { CrudContentType } from '../../../clericuzzi-react-web/components/bootstrap/crud/crud';
import StatusListItem from 'src/components/listItems/status.listItem';
import BootstrapHeaderBar from 'src/clericuzzi-react-web/components/bootstrap/bars/header/headerBar';

const CrudInit = require('src/business/init/crudInit');
const ProjectCrud = () =>  
{
    // const [contentType, setContentType] = useState(CrudContentType.List);
    const contentType = useState(CrudContentType.List)[0];
    useEffect(() =>
    {
        setTimeout(() => BootstrapHeaderBar.setTitle(`Cadastros`), 500);
    }, []);

    const crudOptionSelectedHandler = event =>
    {
        // setContentType(CrudContentType.Grid);
    }

    const crudRenderer = (item, info) =>
    {
        return <StatusListItem key={nextId()} item={item} info={info} />;
    }

    const render = () =>
    {
        return (
            <BootstrapCrud
                name={`projectCrud`}

                pageSize={2}

                actionBarItems={[]}
                actionBarPlaceholder={`filtrar itens`}

                contentType={contentType}
                crudOptionItems={CrudInit.CrudModels}
                crudOptionIsBusy={false}
                crudOptionSelected={crudOptionSelectedHandler}
                optionsPlaceholder={`Selecione o tipo de cadastro...`}
                optionsPlaceholderBusy={`Carregando cadastros...`}
                optionsNoOptionsMessage={`Sem opções de cadastro...`}

                gridColumnCount={2}

                renderingMethod={(item, info) => crudRenderer(item, info)}
            />
        );
    }

    return render();
}

export default ProjectCrud;