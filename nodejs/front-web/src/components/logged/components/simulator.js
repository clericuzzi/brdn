import nextId from 'react-id-generator';
import React, { useEffect, useState } from 'react';

import { CSSTransition } from 'react-transition-group';

import Loader from 'src/clericuzzi-react-web/components/loader/loader';

import CrudList from 'src/clericuzzi-react-web/components/bootstrap/crud/components/list/crudList';
import HttpManager from 'clericuzzi-javascript/business/managers/httpManager';
import BootstrapHeaderBar from 'src/clericuzzi-react-web/components/bootstrap/bars/header/headerBar';

import BootstrapFormInput from 'src/clericuzzi-react-web/components/bootstrap/forms/formInput';
import BootstrapAlertOverlay from 'src/clericuzzi-react-web/components/bootstrap/alert/alertOverlay';
import SimulationScreenListItemDaily from 'src/components/listItems/simulationScreenDaily.listItem';
import SimulationScreenListItemWeekly from 'src/components/listItems/simulationScreenWeekly.listItem';

const dateUtils = require('clericuzzi-javascript/utils/dateUtils');
const mathUtils = require('clericuzzi-javascript/utils/mathUtils');
const typeUtils = require('clericuzzi-javascript/utils/typeUtils');
const loginManager = require('clericuzzi-javascript-aws/managers/loginManager');

const Simulator = props =>    
{
    const properties = { dateTo: dateUtils.todayFormatted(), dateFrom: dateUtils.firstOfMonthFormatted(), token: null };

    const [data, setData] = useState([]);
    const [isBusy, setBusy] = useState(false);
    const [dataLength, setDataLength] = useState(0);

    const defaultClass = `flex-column visibility`;

    useEffect(() =>
    {
        setTimeout(() => BootstrapHeaderBar.setTitle(`Simulador`), 500);

        (async function getInitialData()
        {
            await fetchData();
        })();
    }, []);

    const fetchData = async () =>
    {
        if (!isBusy)
        {
            try
            {
                setBusy(true);

                let simulationData = [];
                if (typeUtils.isArrayAndValid(data))
                {
                    const validDataSources = data.filter(i => typeUtils.isValid(i.dataSourceId));
                    for (const validDataSource of validDataSources)
                        for (const criterion of validDataSource.criteria)
                            if (criterion.criterion_id > 0)
                                simulationData.push({ dataSourceId: validDataSource.dataSourceId, criterionId: criterion.criterion_id, simulationDaily: mathUtils.round(Number.parseFloat(criterion.simulationDaily), 6), simulationWeekly: mathUtils.round(Number.parseFloat(criterion.simulationWeekly), 6) })
                }

                const message = {
                    token: loginManager.getToken(),
                    dateTo: dateUtils.dateTo(properties.dateTo),
                    dateFrom: dateUtils.dateFrom(properties.dateFrom),
                    simulationData: simulationData,
                }
                const result = await HttpManager.post(message, `https://g9qeehd7e1.execute-api.us-east-1.amazonaws.com/dev/module-simulation-screen`)
                if (result && typeUtils.isArrayAndValid(result.modelData))
                {
                    const modelData = createResult(result);
                    setData(modelData);
                    setDataLength(modelData.length);
                }
                else
                {
                    setData([]);
                    setDataLength(0);
                }

                setBusy(false);
            }
            catch (err)
            {
                console.log(`ERROR ON FETCH: `, err.message);
            }
            finally
            {
                setBusy(false);
            }
        }
        else
            BootstrapAlertOverlay.newDanger(`Aguarde o retorno dos dados...`);
    }
    const createResult = result => [{ info: result.criteriaInfo }, ...result.modelData];

    const rowRendererDay = (item, info) =>
    {
        return <SimulationScreenListItemDaily key={nextId()} item={item} info={info} totalCount={dataLength} />;
    }
    const rowRendererWeek = (item, info) =>
    {
        return <SimulationScreenListItemWeekly key={nextId()} item={item} info={info} totalCount={dataLength} />;
    }

    const filter = () =>
    {
        console.log(`filter: `, properties);
    }

    const renderContent = () =>
    {
        if (typeUtils.isArrayAndValid(data))
            return (
                <div className="full-size flex-column">
                    <h2 className="full-width flex-row center">Visão diária</h2>
                    <div className="flex-grow">
                        <CrudList
                            data={data}
                            display={(item, info) => rowRendererDay(item, info)}
                            rowCount={data.length}
                            rowHeight={55}
                        />
                    </div>
                    <h2 className="full-width flex-row center">Visão semanal</h2>
                    <div className="flex-grow">
                        <CrudList
                            data={data}
                            display={(item, info) => rowRendererWeek(item, info)}
                            rowCount={data.length}
                            rowHeight={55}
                        />
                    </div>
                </div>);
        else
            return <div className="full-size flex-column center">Sem resultados...</div>;
    }

    const render = () =>
    {
        return (
            <div className="full-size flex-column">
                <div className="bottom-margin flex-row justify-space-between center">
                    <div className="grid-spaced center" style={{ gridTemplateColumns: `auto auto auto`, width: `400px` }}>
                        <h6>Período dos dados</h6>
                        {BootstrapFormInput.newDate(filter, properties, `dateFrom`, `de`)}
                        {BootstrapFormInput.newDate(filter, properties, `dateTo`, `até`)}
                    </div>
                    <i className="mr-2 fas fa-sync-alt fa-lg pointer bootstrap-action-item" onClick={fetchData} title="Recarregar dados" />
                </div>
                <CSSTransition in={isBusy} timeout={200} classNames={defaultClass} appear={true} unmountOnExit={true}>
                    <Loader />
                </CSSTransition>
                <CSSTransition in={!isBusy} timeout={200} classNames={defaultClass} appear={true} unmountOnExit={true}>
                    {renderContent()}
                </CSSTransition>
            </div >
        );
    }

    return render();
}

export default Simulator;