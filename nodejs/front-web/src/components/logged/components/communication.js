import React, { useEffect } from 'react';
import BootstrapHeaderBar from '../../../clericuzzi-react-web/components/bootstrap/bars/header/headerBar';

const Communication = props =>  
{
    useEffect(() =>
    {
        setTimeout(() => BootstrapHeaderBar.setTitle(`Comunicação`), 500);
    }, []);

    return (
        <span>Comunicação</span>
    );
}

export default Communication;