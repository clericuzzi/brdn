import React, { useState } from 'react';

import RouteManager from './clericuzzi-react-web/business/managers/routesManager';

import { Routes } from './business/enums/routes.enum';
import { Route, Switch, withRouter } from 'react-router-dom';

import Loader from './clericuzzi-react-web/components/loader/loader';
import LoginPage from './components/loginPage/loginPage';
import LoggedPage from './components/logged/loggedPage';

const loginManager = require('clericuzzi-javascript-aws/managers/loginManager');

const App = props =>
{
  const [isBusy, setBusy] = useState(true);
  useState(() =>
  {
    RouteManager.initialize(props.history);
    (async function onStart()
    {
      await loginManager.checkLogged();
      if (loginManager.isLogged())
        RouteManager.goTo(props, props.location.pathname);
      else
        RouteManager.goTo(props, Routes.Login);
      setBusy(false)
    })();
  }, []);

  const render = () =>
  {
    if (isBusy)
      return renderLoader();
    else
      return renderContent();
  }
  const renderLoader = () =>
  {
    return <Loader />;
  }
  const renderContent = () =>
  {
    return (
      <Switch>
        <Route path={Routes.Logged} component={LoggedPage} />
        <Route component={LoginPage} />
      </Switch>
    );
  }

  return render();
}
export default withRouter(App);