module.exports = {
    MacroInfo: `https://zevcb7swm8.execute-api.us-east-1.amazonaws.com/dev/selenium-get-macro-info`,

    GoogleMinerNext: `https://zevcb7swm8.execute-api.us-east-1.amazonaws.com/dev/google-miner-next`,
    GoogleMinerConsolidate: `https://zevcb7swm8.execute-api.us-east-1.amazonaws.com/dev/google-miner-next`,

    SmartCnpjNext: `https://zevcb7swm8.execute-api.us-east-1.amazonaws.com/dev/smart-cnpj-next`,
    SmartCnpjConsolidate: `https://zevcb7swm8.execute-api.us-east-1.amazonaws.com/dev/smart-cnpj-consolidate`,
};