module.exports = class GoogleMinerCompany
{
    constructor(fantasyName, phone, email, latitude, longitude, plusCode)
    {
        this.phone = phone;
        this.email = email;
        this.plusCode = plusCode;
        this.latitude = latitude;
        this.longitude = longitude;
        this.fantasyName = fantasyName;
    }
}