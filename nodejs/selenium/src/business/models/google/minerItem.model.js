module.exports = class GoogleMinerItem
{
    constructor(id = null, city = null, keyword = null)
    {
        this.id = id;
        this.city = city;
        this.keyword = keyword;
    }

    get searchValue()
    {
        return `${this.city}, ${this.keyword}`;
    }
}