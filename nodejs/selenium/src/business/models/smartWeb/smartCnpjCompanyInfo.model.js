const typeUtils = require('clericuzzi-javascript/utils/typeUtils');

module.exports = class SmartWebCnpjCompanyInfo
{
    constructor()
    {
        this.cnpj = null;
        this.name = null;
        this.email = null;
        this.phones = [];
        this.contacts = [];
        this.services = [];
        this.addresses = [];
    }

    clean()
    {
        if (typeUtils.isArrayAndValid(this.contacts))
            for (const contact of this.contacts)
                delete contact.stringValue;

        if (typeUtils.isArrayAndValid(this.services))
            for (const service of this.services)
                delete service.stringValue;

        if (typeUtils.isArrayAndValid(this.addresses))
            for (const address of this.addresses)
                delete address.stringValue;
    }
}