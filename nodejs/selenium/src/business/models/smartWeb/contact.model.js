const stringsFormatter = require('clericuzzi-javascript/utils/stringsFormatter');

module.exports = class SmartWebContact
{
    constructor()
    {
        this.stringValue = ``;
    }

    async getValues(driver, row)
    {
        let input = null;
        let columns = null;

        try
        {
            await driver.sleep(500);
            columns = await driver.getElementImmediateChildren(row);
            input = await driver.getElementChildByTag(columns[2], `input`);
            if (!input)
                this.name = await driver.getElementText(columns[2]);
            else
                this.name = await driver.getElementText(input);
        }
        catch (error)
        {

        }

        this.email = await driver.getElementText(columns[10]);
        this.phoneRes = stringsFormatter.brCleanPhone(await driver.getElementText(columns[3]));
        this.phoneCom = stringsFormatter.brCleanPhone(await driver.getElementText(columns[5]));
        this.phoneCel = stringsFormatter.brCleanPhone(await driver.getElementText(columns[7]));

        const values = [];
        const keys = Object.keys(this);
        for (const key of keys)
            if (key !== `stringValue`)
            {
                if (this[key] === null || this[key] === undefined || this[key] === ``)
                    this[key] = null;
                else
                {
                    this[key] = this[key].trim();
                    values.push(this[key]);
                }
            }

        this.stringValue = values.join(`|`);
        return true;
    }
}