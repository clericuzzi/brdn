module.exports = class SmartWebOrder
{
    constructor()
    {
        this.stringValue = ``;
    }

    async getValues(driver, row)
    {
        const columns = await driver.getElementImmediateChildren(row);

        let i = 1;
        this.createdIn = await driver.getElementText(columns[i++]);
        this.responsible = await driver.getElementText(columns[i++]);
        this.oportunityType = await driver.getElementText(columns[i++]);
        this.saleType = await driver.getElementText(columns[i++]);
        this.saleSituation = await driver.getElementText(columns[i++]);
        this.status = await driver.getElementText(columns[i++]);
        this.justification = await driver.getElementText(columns[i++]);
        this.documentType = await driver.getElementText(columns[i++]);
        this.offer = await driver.getElementText(columns[i++]);

        const values = [];
        const keys = Object.keys(this);
        for (const key of keys)
            if (key !== `driver` && key !== `sourceRow` && key !== `stringValue`)
            {
                if (this[key] === null || this[key] === undefined || this[key] === ``)
                    this[key] = null;
                else
                {
                    this[key] = this[key].trim();
                    values.push(this[key]);
                }
            }

        this.stringValue = values.join(`|`);
    }
}