const stringsFormatter = require('clericuzzi-javascript/utils/stringsFormatter');

module.exports = class SmartWebAddress
{
    constructor()
    {
        this.stringValue = ``;
    }

    async getValues(driver, row)
    {
        const columns = await driver.getElementImmediateChildren(row);

        this.stringValue = await driver.getElementText(columns[1]);

        const parts = this.stringValue.split(',');
        this.address = parts[0];
        this.number = parts[3];
        this.neighbourhood = null;
        this.city = parts[1];
        this.state = parts[2];
        this.zipcode = null;
        this.testedIn = null;

        return true;
    }
}