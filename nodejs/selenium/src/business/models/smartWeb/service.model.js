module.exports = class SmartWebService
{
    constructor()
    {
        this.stringValue = ``;
    }

    async getValues(driver, row)
    {
        const columns = await driver.getElementImmediateChildren(row);

        let i = 0;
        this.productName = await driver.getElementText(columns[i++]);
        this.serviceInstance = await driver.getElementText(columns[i++]);
        this.serviceAddress = await driver.getElementText(columns[i++]);
        this.billingProfile = await driver.getElementText(columns[i++]);
        this.status = await driver.getElementText(columns[i++]);
        this.offer = await driver.getElementText(columns[i++]);
        this.invalid = await driver.getElementText(columns[i++]);
        this.voiceTechnology = await driver.getElementText(columns[i++]);
        this.createdIn = await driver.getElementText(columns[i++]);
        this.activationDate = await driver.getElementText(columns[i++]);
        this.portability = await driver.getElementText(columns[i++]);
        this.document = await driver.getElementText(columns[i++]);
        this.phone = await driver.getElementText(columns[i++]);
        this.serviceOrder = await driver.getElementText(columns[i++]);
        this.callSource = await driver.getElementText(columns[i++]);
        this.category = await driver.getElementText(columns[i++]);

        const values = [];
        const keys = Object.keys(this);
        for (const key of keys)
            if (key !== `driver` && key !== `sourceRow` && key !== `stringValue`)
            {
                if (this[key] === null || this[key] === undefined || this[key] === ``)
                    this[key] = null;
                else
                {
                    this[key] = this[key].trim();
                    values.push(this[key]);
                }
            }

        this.stringValue = values.join(`|`);
    }
}