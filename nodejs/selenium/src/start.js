const GoogleMiner = require('./google/miner/googleMiner');
const SmartWebCnpj = require('./smartWeb/cnpj/smartWebCnpj');
const SmartWebCnpjOrderCancelling = require('./smartWeb/orderCancelling/smartWebOrderCancelling');

module.exports = class Start
{
    async run() 
    {
        await Promise.all([
            new GoogleMiner().run(),
        ]);
    }
}