const endpoints = require('../../constants/endpoints');

const httpManager = require('clericuzzi-javascript/business/managers/httpManager');

const ChromeDriver = require('clericuzzi-javascript-selenium/chrome/seleniumChromeDriver');
const GoogleMinerItem = require('../../business/models/google/minerItem.model');
const GoogleMinerCompany = require('../../business/models/google/minerCompany.model');

module.exports = class GoogleMinerInstance
{
    constructor()
    {
        this.currentItem = new GoogleMinerItem();
        this.currentCompany = new GoogleMinerCompany();
    }

    hasNext()
    {
        return this.currentItem !== null;;
    }
    async getNext()
    {
        this.currentItem = null;
        try
        {
            const result = await httpManager.post(null, endpoints.GoogleMinerNext);
            if (!result || !result.item)
                throw Error(`problema na consulta aos itens a minerar...`);

            this.currentItem = result.item;
            return this.hasNext();
        }
        catch (err)
        {
            console.log(`ERROR GETTING NXT ITEM`, err);
            return false;
        }
    }

    async run()
    {
        let errorLimit = 4;
        let errors = 0;
        // while (true)
        // {
        try
        {
            if (!this.driver)
                this.driver = new ChromeDriver();

            console.log(`há`);
            await this.driver.sleep(880);

            console.log(`há 1`);
            await this.driver.sleep(1600);

            await this.getNext();
            if (!this.hasNext())
                return;

            await this.runItem();
        }
        catch (e)
        {
            console.log(`ERROR`, e);
        }
        finally
        {
            if (this.driver)
                this.driver.kill();
        }
        // }
    }
    async runItem()
    {
        this.navigateTo();
        this.search();
        while (this.hasResultsPage())
            while (this.hasResult())
                this.getCompany();
    }
    async navigateTo()
    {
        console.log(`navigating...`);
        await this.driver.navigate(`http://maps.google.com/`);
    }
    async search()
    {
        const searchPath = `/html/body/jsl/div[3]/div[9]/div[3]/div[1]/div[1]/div[1]/div[2]/form/div/div[3]/div/input[1]`;
        await this.driver.fillElement(searchPath, this.currentItem.searchValue);

        await this.driver.sleep(50000);
    }
    async hasResultsPage()
    {
    }
    async hasResult()
    {
    }
    async getCompany()
    {
    }
}