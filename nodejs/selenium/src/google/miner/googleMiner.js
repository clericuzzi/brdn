const GoogleMinerInstance = require('./googleMinerInstance.automation');

module.exports = class GoogleMiner
{
    constructor()
    {
        this.limit = 1;
        this.instances = [];
    }

    async run()
    {
        try
        {
            for (let i = 0; i < this.limit; i++)
                this.instances.push(new GoogleMinerInstance());

            await Promise.all(this.instances.map(i => i.run()));
        }
        catch (e)
        {
            console.log(e);
        }
    }
}