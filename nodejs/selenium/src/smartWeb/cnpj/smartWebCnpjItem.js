const auth = require('../smartWebUtils');
const endpoints = require('../../constants/endpoints');

const SmartWebAddress = require('../../business/models/smartWeb/address.model');
const SmartWebContact = require('../../business/models/smartWeb/contact.model');
const SmartWebCnpjCompanyInfo = require('../../business/models/smartWeb/smartCnpjCompanyInfo.model');

const typeUtils = require('clericuzzi-javascript/utils/typeUtils');
const httpManager = require('clericuzzi-javascript/business/managers/httpManager');
const stringsFormatter = require('clericuzzi-javascript/utils/stringsFormatter');

const smartWebUtils = require('../smartWebUtils');

const ChromeDriver = require('clericuzzi-javascript-selenium/chrome/seleniumChromeDriver');

module.exports = class SmartWebCnpjItem
{
    constructor(url, login, password)
    {
        this.errorThreshold = 4;

        this.url = url;
        this.login = login;
        this.password = password;

        this.cnpj = null;

        this.driver = null;
        this.services = [];
    }

    async hasNext()
    {
        try
        {
            return typeUtils.isValid(this.cnpj);
        }
        catch (err)
        {
            return false;
        }
    }
    async nextItem()
    {
        this.cnpj = null;
        try
        {
            const result = await httpManager.post(null, endpoints.SmartCnpjNext);
            this.cnpj = result.cnpj;
        }
        catch (err)
        {
            return null;
        }
    }
    async consolidate(companyInfo)
    {
        this.cnpj = null;
        try
        {
            companyInfo.clean();
            const result = await httpManager.post(companyInfo, endpoints.SmartCnpjConsolidate);
            this.cnpj = result.cnpj;
        }
        catch (err)
        {
            return null;
        }
    }

    async toNextPage()
    {
        let nextPageButtonPath = `/html/body/div[1]/div[1]/div[8]/div/div[6]/div/div[1]/div/div[1]/div/form/div/span/div[2]/div[3]/div/div[2]/button`;

        let tries = 10;
        let keepGoing = true;
        while (keepGoing)
        {
            const buttonExists = await this.driver.wait(nextPageButtonPath, 4000, false);
            if (!buttonExists)
                nextPageButtonPath = `/html/body/div[1]/div[1]/div[7]/div/div[6]/div/div[1]/div/div[1]/div/form/div/span/div[2]/div[3]/div/div[2]/button`;
            else
                keepGoing = false;

            if (--tries < 0)
                throw Error(`could not identify if we coulde RENTABILIZE the client`);
        }

        const button = await this.driver.getElement(nextPageButtonPath);
        const disabled = await this.driver.getAttribute(button, `disabled`);
        if (disabled == `disabled`)
            throw Error(`could not identify if we coulde RENTABILIZE the client`);
        else
            await this.driver.clickElement(nextPageButtonPath);
    }
    async runItem()
    {
        try
        {
            const companyInfo = new SmartWebCnpjCompanyInfo();

            if (!this.cnpj)
                await this.nextItem();

            await this.driver.sleep(750);
            if (typeUtils.isValid(this.cnpj))
            {
                companyInfo.cnpj = this.cnpj;
                const companyFound = await this.runItemSearch();

                if (companyFound)
                {
                    const hasServices = await this.runItemGetServices();
                    if (hasServices)
                    {
                        await this.runItemGetAddress(companyInfo);
                        await this.toNextPage();

                        await this.driver.sleep(3000);
                        await this.runItemGetName(companyInfo);
                        await this.runItemGetPhones(companyInfo);
                        await this.runItemGetContacts(companyInfo);
                    }
                }

                await this.consolidate(companyInfo);
            }
        }
        catch (err)    
        {
            if (--this.errorThreshold)
                throw err;

            err.source = `runItem`;
            console.log(err);
            await this.driver.refresh();
        }
        finally
        {
            await smartWebUtils.justifySale(this.driver);
            await this.goToSales();
        }
    }
    async runItemSearch()
    {
        const panelDivId = `S_A1`;

        let panelDiv = null;
        let searchInput = null;
        let searchButton = null;
        while (!searchInput || !searchButton)
        {
            await this.driver.sleep(500);

            panelDiv = await this.driver.getElementById(panelDivId);
            searchInput = await this.driver.getChildByTagAndAttribute(panelDiv, `input`, `aria-label`, `Documento`);
            searchButton = await this.driver.getChildByTagAndAttribute(panelDiv, `button`, `data-display`, `Pesquisar`);
        }

        await this.driver.fill(searchInput, this.cnpj);
        await this.driver.click(searchButton);

        if (await this.driver.alertHas())
        {
            await this.driver.alertDismiss();
            return false;
        }
        else
            return true;
    }
    async runItemGetName(companyInfo)
    {
        const namePath = `/html/body/div[1]/div[1]/div[8]/div/div[6]/div/div[1]/div/div[1]/div[2]/div/div/div/form/div/span/div/div[6]/div[2]/input`;
        const emailPath = `/html/body/div[1]/div[1]/div[8]/div/div[6]/div/div[1]/div/div[1]/div[2]/div/div/div/form/div/span/div/div[13]/div[2]/input`;

        companyInfo.name = await this.driver.getText(namePath);
        companyInfo.email = await this.driver.getText(emailPath);
    }
    async runItemGetPhone(companyInfo, phoneXpath)
    {
        let phoneValue = await this.driver.getText(phoneXpath);
        phoneValue = stringsFormatter.brCleanPhone(phoneValue);
        if (typeUtils.isStringAndValid(phoneValue))
            companyInfo.phones.push(phoneValue);
    }
    async runItemGetAddress(companyInfo)
    {

        await this.runItemGetAddressGoToAddress();
        await this.runItemGetAddressAction(companyInfo);
        await this.runItemGetAddressBackToServices();
    }
    async runItemGetAddressAction(companyInfo)
    {
        let tBodyPath = `/html/body/div[1]/div[1]/div[8]/div/div[6]/div/div[1]/div/div[3]/div[2]/div/form/span/div/div[3]/div/div/div[3]/div[3]/div/table/tbody`;
        let nextButtonPath = `/html/body/div[1]/div[1]/div[8]/div/div[6]/div/div[1]/div/div[3]/div[2]/div/form/span/div/div[3]/div/div/div[5]/div/table/tbody/tr/td[2]/table/tbody/tr/td[3]/span`;

        let row = null
        let keepGoing = true;
        while (keepGoing)
        {
            while (!row)
            {
                await this.driver.sleep(750);
                row = await smartWebUtils.getSelectedRow(this.driver, tBodyPath);
                await this.driver.sleep(250);

                if (row == null)
                    tBodyPath = `/html/body/div[1]/div[1]/div[7]/div/div[6]/div/div[1]/div/div[3]/div[2]/div/form/span/div/div[3]/div/div/div[3]/div[3]/div/table/tbody`;
            }

            let nextButtonExists = await this.driver.wait(nextButtonPath, 3000, false);
            if (!nextButtonExists)
                nextButtonPath = `/html/body/div[1]/div[1]/div[7]/div/div[6]/div/div[1]/div/div[3]/div[2]/div/form/span/div/div[3]/div/div/div[5]/div/table/tbody/tr/td[2]/table/tbody/tr/td[3]/span`;

            const address = new SmartWebAddress();
            if (await address.getValues(this.driver, row))
            {
                await this.driver.clickElement(nextButtonPath);
                keepGoing = this.runItemGetAddressStore(companyInfo, address);
            }
            else
                keepGoing = false;
            row = null;
        }
    }
    runItemGetAddressStore(companyInfo, address)
    {
        const hasAddress = companyInfo.addresses.find(i => i.stringValue === address.stringValue);
        if (!hasAddress)
        {
            companyInfo.addresses.push(address);
            return true;
        }
        else
            return false;
    }
    async runItemGetAddressGoToAddress()
    {
        let tBodyPath = `/html/body/div[1]/div[1]/div[8]/div/div[6]/div/div[1]/div/div[3]/div[2]/div/form/span/div/div[3]/div/div/div[3]/div[3]/div/table/tbody`;
        let nextButtonPath = `/html/body/div[1]/div[1]/div[8]/div/div[6]/div/div[1]/div/div[3]/div[2]/div/form/span/div/div[3]/div/div/div[5]/div/table/tbody/tr/td[2]/table/tbody/tr/td[3]/span`;
        let buttonAddressPath = `/html/body/div[1]/div[1]/div[8]/div/div[6]/div/div[1]/div/div[2]/div[2]/ul/li[3]/a`;

        let tries = 10;
        let keepGoing = true;
        let elementExists = false;
        while (keepGoing)
        {
            elementExists = await this.driver.wait(buttonAddressPath, 5000, false);
            if (!elementExists)
            {
                tBodyPath = `/html/body/div[1]/div[1]/div[7]/div/div[6]/div/div[1]/div/div[3]/div[2]/div/form/span/div/div[3]/div/div/div[3]/div[3]/div/table/tbody`;
                nextButtonPath = `/html/body/div[1]/div[1]/div[7]/div/div[6]/div/div[1]/div/div[3]/div[2]/div/form/span/div/div[3]/div/div/div[5]/div/table/tbody/tr/td[2]/table/tbody/tr/td[3]/span`;
                buttonAddressPath = `/html/body/div[1]/div[1]/div[7]/div/div[6]/div/div[1]/div/div[2]/div[2]/ul/li[3]/a`;
            }

            if (await this.driver.alertHas())
                await this.driver.alertDismiss();
            else
                keepGoing = !elementExists;

            if (--tries < 0)
                throw Error(`could not go the addresses list`)
        }

        elementExists = await this.driver.wait(buttonAddressPath, 5000, false);
        if (!elementExists)
            return;
        await this.driver.clickElement(buttonAddressPath);
    }
    async runItemGetAddressBackToServices()
    {
        let servicesButtonPath = `/html/body/div[1]/div[1]/div[8]/div/div[6]/div/div[1]/div/div[2]/div[2]/ul/li[1]/a`;

        let tries = 10;
        let keepGoing = true;
        let elementExists = false;
        while (keepGoing)
        {
            elementExists = await this.driver.wait(servicesButtonPath, 5000, false);
            if (!elementExists)
            {
                servicesButtonPath = `/html/body/div[1]/div[1]/div[7]/div/div[6]/div/div[1]/div/div[2]/div[2]/ul/li[1]/a`;
                elementExists = await this.driver.wait(servicesButtonPath, 5000, false);
            }

            if (await this.driver.alertHas())
                await this.driver.alertDismiss();
            else
                keepGoing = !elementExists;

            if (--tries < 0)
                throw Error(`could not go back from address to service list`)
        }

        await this.driver.sleep(500);
        await this.driver.clickElement(servicesButtonPath);
        await this.driver.sleep(500);
    }

    async runItemGetPhones(companyInfo)
    {
        await this.driver.sleep(750);
        await this.driver.wait(`/html/body/div[1]/div[1]/div[8]/div/div[6]/div/div[1]/div/div[1]/div[2]/div/div/div/form/div/span/div/div[9]/div[2]/input`, 15000);

        await this.runItemGetPhone(companyInfo, `/html/body/div[1]/div[1]/div[8]/div/div[6]/div/div[1]/div/div[1]/div[2]/div/div/div/form/div/span/div/div[9]/div[2]/input`);
        await this.runItemGetPhone(companyInfo, `/html/body/div[1]/div[1]/div[8]/div/div[6]/div/div[1]/div/div[1]/div[2]/div/div/div/form/div/span/div/div[12]/div[2]/input`);
        await this.runItemGetPhone(companyInfo, `/html/body/div[1]/div[1]/div[8]/div/div[6]/div/div[1]/div/div[1]/div[2]/div/div/div/form/div/span/div/div[11]/div[2]/input`);
    }
    async runItemGetContacts(companyInfo)
    {
        const contactsPath = `/html/body/div[1]/div[1]/div[8]/div/div[6]/div/div[1]/div/div[1]/div[2]/div/div/div/form/div/span/div/div[7]/div[2]/span`;
        const contactsClose = `/html/body/div[8]/div[1]/button/span[1]`;
        const contactsTableTop = `/html/body/div[8]/div[2]/div/div/div/form/div/div[1]/div/div/div[5]/div/table/tbody/tr/td[2]/table/tbody/tr/td[1]/span`;
        const contactsTableBody = `/html/body/div[8]/div[2]/div/div/div/form/div/div[1]/div/div/div[3]/div[3]/div/table/tbody`;
        const contactsTableNext = `/html/body/div[8]/div[2]/div/div/div/form/div/div[1]/div/div/div[5]/div/table/tbody/tr/td[2]/table/tbody/tr/td[3]/span`;

        try
        {
            await this.driver.wait(contactsPath, 15000);
            await this.driver.clickElement(contactsPath)

            await this.driver.wait(contactsTableBody, 15000);
            await this.driver.clickElement(contactsTableTop);
            await this.driver.sleep(500);

            let noData = false;
            let keepGoing = true;
            while (keepGoing)
            {
                let row = null
                let i = 0;
                while (!row)
                {
                    await this.driver.sleep(750);
                    row = await smartWebUtils.getSelectedRow(this.driver, contactsTableBody);
                    await this.driver.sleep(250);
                    if (++i > 15)
                    {
                        noData = true;
                        keepGoing = false;

                        break;
                    }
                }

                if (!noData)
                {
                    const contact = new SmartWebContact(this.driver, row);
                    if (await contact.getValues(this.driver, row))
                    {

                        await this.driver.sleep(250);
                        await this.driver.clickElement(contactsTableNext);
                        keepGoing = this.runItemGetContactsStore(companyInfo, contact);
                    }
                    else
                        keepGoing = false;
                }
            }
        }
        catch (err)
        {
            err.source = `runItemGetContacts`;
            throw err;
        }
        finally
        {
            await this.driver.sleep(250);
            await this.driver.clickElement(contactsClose);
        }
    }
    runItemGetContactsStore(companyInfo, contact)
    {
        const hasContact = companyInfo.contacts.find(i => i.stringValue === contact.stringValue);
        if (!hasContact)
        {
            companyInfo.contacts.push(contact);
            return true;
        }
        else
            return false;
    }

    async runItemGetServices()
    {
        let servicesTable = `/html/body/div[1]/div[1]/div[8]/div/div[6]/div/div[1]/div/div[3]/div[2]/div/form/span/div/div[3]/div/div/div[3]/div[3]/div/table/tbody`;
        let keepGoing = await this.driver.wait(servicesTable, 5000, false);
        if (!keepGoing)
        {
            servicesTable = `/html/body/div[1]/div[1]/div[7]/div/div[6]/div/div[1]/div/div[3]/div[2]/div/form/span/div/div[3]/div/div/div[3]/div[3]/div/table/tbody`;
            keepGoing = await this.driver.wait(servicesTable, 5000, false);
        }

        return await smartWebUtils.getSelectedRow(this.driver, servicesTable) !== null;
        /*
        let noData = false;
        const buttonNext = `/html/body/div[1]/div[1]/div[7]/div/div[6]/div/div[1]/div/div[3]/div[2]/div/form/span/div/div[3]/div/div/div[5]/div/table/tbody/tr/td[2]/table/tbody/tr/td[3]/span`;
        while (keepGoing)
        {
            console.log(`KEEP GOING`, keepGoing);
            let row = null
            let i = 0;
            while (!row)
            {
                console.log(`trying to get service row`, servicesTable);
                await this.driver.sleep(750);
                row = await smartWebUtils.getSelectedRow(this.driver, servicesTable);
                await this.driver.sleep(250);
                if (++i > 15)
                {
                    console.log(`trying to get row`, i, row);
                    noData = true;
                    keepGoing = false;

                    // has no services
                    return false;
                    break;
                }
            }
            // has services
            return true;

            if (!noData)
            {
                const service = new SmartWebService();
                await service.getValues(this.driver, row);
                await this.runItemGetServicesCheckChildren(row);

                await this.driver.sleep(250);
                await this.driver.clickElement(buttonNext);
                keepGoing = this.runItemGetServicesStore(companyInfo, service);
            }
        }
        */
    }
    runItemGetServicesStore(companyInfo, service)
    {
        const hasService = companyInfo.services.find(i => i.stringValue === service.stringValue);
        if (!hasService)
        {
            companyInfo.services.push(service);
            return true;
        }
        else
            return false;
    }
    async runItemGetServicesCheckChildren(row)
    {
        await this.driver.sleep(500);
        const columns = await this.driver.getElementImmediateChildren(row);
        const arrowDiv = await this.driver.getElementChildByTag(columns[0], `div`);
        await this.driver.click(arrowDiv);
    }

    async run()
    {
        await this.nextItem();
        while (this.hasNext())
        {
            try
            {
                this.errorThreshold = 4;
                this.driver = new ChromeDriver();
                await auth.auth(this.driver, this.url, this.login, this.password);
                await this.goToSales();

                while (this.hasNext())
                    await this.runItem();
            }
            catch (err)
            {
                console.log(`ERRO AQUI`);
                console.log(err);
            }
            finally
            {
                if (this.driver)
                    this.driver.kill();
            }

            await this.nextItem();
        }
    }
    async goToSales()
    {
        let saleScreen = false;
        while (!saleScreen)
        {
            await this.driver.sleep(500);
            await smartWebUtils.clickMenu(this.driver, `Venda`);
            saleScreen = !(await this.driver.alertHas());
            if (!saleScreen)
                await this.driver.alertDismiss();
        }
    }
}