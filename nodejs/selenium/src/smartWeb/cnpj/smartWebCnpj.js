const httpManager = require('clericuzzi-javascript/business/managers/httpManager');
const endpoints = require('../../constants/endpoints');

const SmartWebCnpjItem = require('./smartWebCnpjItem');

module.exports = class SmartWebCnpj
{
    constructor()
    {
        this.limit = 4;
        this.macros = [];
    }

    async getInfo()
    {
        this.result = await httpManager.post({ macroName: `Smart VT` }, endpoints.MacroInfo);
    }

    async run()
    {
        await this.getInfo();
        for (let i = 0; i < this.limit; i++)
            this.macros.push(new SmartWebCnpjItem(this.result.url, this.result.login, this.result.password));

        await Promise.all(this.macros.map(i => i.run()));
    }
}