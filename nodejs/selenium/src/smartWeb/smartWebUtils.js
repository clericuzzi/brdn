const typeUtils = require('clericuzzi-javascript/utils/typeUtils');
const stringsFormatter = require('clericuzzi-javascript/utils/stringsFormatter');

const SmartWebOrder = require('../business/models/smartWeb/order.model');

const _xpathLogin = `/html/body/div[1]/div[2]/div/center/table/tbody/tr[2]/td/div/form/div[1]/div/div/input`;
const _xpathSubmit = `/html/body/div[1]/div[2]/div/center/table/tbody/tr[2]/td/div/form/div[4]/div/input`;
const _xpathPassword = `/html/body/div[1]/div[2]/div/center/table/tbody/tr[2]/td/div/form/div[2]/div/div/input`;
const _xpathSliderValue = `/html/body/div[1]/div[2]/div/center/table/tbody/tr[2]/td/div/form/div[3]`;
const _xpathCaptchaValue = `/html/body/div[1]/div[2]/div/center/table/tbody/tr[2]/td/div/form/span/center[2]`;
const _xpathSliderElement = `/html/body/div[1]/div[2]/div/center/table/tbody/tr[2]/td/div/form/div[3]/div/div`;
const _xpathLoggedMenuElement = `/html/body/div[1]/div/div[6]/div/div/div[1]/div[3]/ul`;

const moveToPosition = async (driver, positionOffset) =>
{
    const sliderElement = await driver.getElement(_xpathSliderElement);
    const sliderRect = await sliderElement.getRect(_xpathSliderElement);

    const captchaText = await driver.getText(_xpathCaptchaValue);
    const captchaValue = Number.parseInt(captchaText);

    const initialPosition = -Math.floor(sliderRect.width / 2);
    let clickPosition = Math.floor(initialPosition + Math.floor((captchaValue / 100) * sliderRect.width));
    clickPosition -= positionOffset;
    await driver.moveAndClick({ x: clickPosition, y: 0, origin: sliderElement });

    return captchaValue;
}
const verifyResult = async (driver) =>
{
    try
    {
        const section = `aria-valuenow=`;
        const resultArea = await driver.getElement(_xpathSliderValue);
        const resultAreaContent = await driver.getElementContent(resultArea);
        const resultValue = stringsFormatter.digitsOnly(resultAreaContent.substring(resultAreaContent.indexOf(section) + section.length).substring(0, resultAreaContent.substring(resultAreaContent.indexOf(section) + section.length).indexOf(` `)))

        const currentValue = Number.parseInt(resultValue);

        return currentValue;
    }
    catch (err)
    {
        return -1;
    }
}
const submit = async (driver) =>
{
    await driver.clickElement(_xpathSubmit);
    await driver.wait(_xpathLoggedMenuElement, 40000);
}

const checkItem = async (driver, item, text) =>
{
    if (typeUtils.isValid(item))
    {
        try
        {
            const itemTitle = await driver.getAttribute(item, `title`);
            if (itemTitle === text)
            {
                await driver.click(item);

                return true;
            }

            return false;
        }
        catch (staleError)
        {
            console.log(`staleError: `, staleError);
        }
    }

    return false;
};

const _cancelOrdersTableBodyPath = `/html/body/div[1]/div[1]/div[7]/div/div[6]/div/div[1]/div/div[1]/div/form/span/div/div[3]/div/div/div[3]/div[3]/div/table/tbody`;
const _cancelOrdersNextButtonPath = `/html/body/div[1]/div[1]/div[7]/div/div[6]/div/div[1]/div/div[1]/div/form/span/div/div[3]/div/div/div[5]/div/table/tbody/tr/td[2]/table/tbody/tr/td[3]/span`;
const cancelOrders = async driver =>
{
    let i = 0;
    await driver.sleep(750);
    let keepGoing = true;
    await driver.wait(_cancelOrdersTableBodyPath, 10000);
    while (keepGoing)
    {
        await driver.sleep(250);
        let row = await this.getSelectedRow(driver, _cancelOrdersTableBodyPath);
        while (!row)
        {
            await driver.sleep(250);
            row = await this.getSelectedRow(driver, _cancelOrdersTableBodyPath);
        }

        const orderModel = new SmartWebOrder();
        await orderModel.getValues(driver, row);
        if (orderModel.status === `Aberto`)
        {
            const columns = await driver.getElementImmediateChildren(row);
            await driver.sleep(250);

            await driver.click(columns[7]);
            await driver.sleep(250);
            const span = await driver.getElementChildByTag(columns[7], `span`);
            await driver.sleep(250);
            await driver.click(span);
            await driver.sleep(250);

            await driver.clickElement(`/html/body/div[1]/div[1]/div[7]/div/div[6]/ul/li[7]`);
            await driver.sleep(250);
            const input = await driver.getElementChildByTag(columns[7], `input`);
            await driver.sendEnter(input);
            await driver.sleep(250);
        }

        await driver.clickElement(_cancelOrdersNextButtonPath);
        if (++i > 10)
            break;
    }
};

module.exports.getSelectedRow = async (driver, tableBodyXPath) =>
{
    try
    {
        const rows = await driver.getImmediateChildrenByTag(tableBodyXPath, `tr`);
        if (!rows)
            return null;

        let i = 0;
        for (const row of rows)
        {
            try
            {
                const isSelected = await driver.getAttribute(row, `aria-selected`);
                if (isSelected === `true`)
                    return row;
            }
            catch (err)
            {
                return null;
            }
        }
    }
    catch (err)
    {
        console.log(`error when getting current selected row`, err);
        return null;
    }
};

module.exports.keepCancelingOrders = async driver =>
{
    while (true)
        try
        {
            await this.clickMenu(driver, `Pedidos`);
            await cancelOrders(driver);
        }
        catch (e)
        {
            await driver.refresh();
        }
};
module.exports.shouldCancelOrders = async driver =>
{
    try
    {
        const hasAlert = await driver.alertHas();
        if (hasAlert)
        {
            await driver.alertDismiss();
            await this.clickMenu(driver, `Pedidos`);

            await cancelOrders(driver);
        }
    }
    catch (err)
    {
        throw err;
    }
};

module.exports.clickMenu = async (driver, title) =>
{
    const loggedMenupath = `/html/body/div[1]/div[1]/div[6]/div/div/div[1]/div[3]/ul`;
    const elementExists = await driver.wait(loggedMenupath, 5000, false);
    let subItems = null;
    if (elementExists)
        subItems = await driver.getElementsByTag(loggedMenupath, `span`);
    else
        subItems = await driver.getElementsByTag(`/html/body/div[1]/div[1]/div[7]`, `span`);

    for (const item of subItems)
    {
        const itemClicked = await checkItem(driver, item, title);
        if (itemClicked)
            break;
    }
};
module.exports.justifySale = async (driver) =>
{
    const justificationPath = `/html/body/div[1]/div[1]/div[8]/div/div[6]/div/div[1]/div/div[1]/div[1]/div/div/form/div/span/div[1]/div[6]/div[1]/span`;
    const justifyButtonPath = `/html/body/div[1]/div[1]/div[8]/div/div[6]/div/div[1]/div/div[1]/div[1]/div/div/form/div/span/div[1]/div[6]/div[2]/button/span`;
    const selectBoxOptionPath = `/html/body/div[1]/div[1]/div[8]/div/div[6]/ul[4]/li[7]`;

    const elementExists = await driver.wait(justificationPath, 2500, false);
    if (elementExists)
    {
        await driver.clickElement(justificationPath);
        await driver.sleep(450);
        await driver.clickElement(selectBoxOptionPath);
        await driver.sleep(450);
        await driver.clickElement(justifyButtonPath);
        await driver.sleep(450);
    }
};

module.exports.auth = async (driver, url, login, password) =>
{
    let tries = 0;
    await driver.navigate(url);
    await driver.fillElement(_xpathLogin, login);
    await driver.fillElement(_xpathPassword, password);

    let correctValue = 0;
    let currentValue = 0;
    let positionOffset = 0;
    while (true)
    {
        correctValue = await moveToPosition(driver, positionOffset);
        currentValue = await verifyResult(driver);
        if (currentValue === correctValue)
            break;
        else
        {
            ++tries;
            positionOffset += currentValue - correctValue;
        }

        if (++tries > 6)
            throw Error(`o sistema não conseguiu encontrar a posição correta do captcha...`);
    }

    await submit(driver);
}