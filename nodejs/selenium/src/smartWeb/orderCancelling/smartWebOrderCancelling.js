const httpManager = require('clericuzzi-javascript/business/managers/httpManager');
const endpoints = require('../../constants/endpoints');

const ChromeDriver = require('clericuzzi-javascript-selenium/chrome/seleniumChromeDriver');

const smartWebUtils = require('../smartWebUtils');

module.exports = class SmartWebCnpjOrderCancelling
{
    constructor()
    {
        this.driver = null;
    }

    async getInfo()
    {
        this.result = await httpManager.post({ macroName: `Smart VT` }, endpoints.MacroInfo);
    }

    async run()
    {
        while (true)
        {
            await this.getInfo();
            try
            {
                this.driver = new ChromeDriver();

                await smartWebUtils.auth(this.driver, this.result.url, this.result.login, this.result.password);
                await smartWebUtils.keepCancelingOrders(this.driver);
            }
            catch (err)
            {
                console.log(`running order cancelling error`, err);
            }
            finally
            {
                if (this.driver)
                    this.driver.kill();
            }
        }
    }
}