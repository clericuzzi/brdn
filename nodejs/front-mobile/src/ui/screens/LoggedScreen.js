import React from 'react';
import { View, Text } from 'react-native';
import { Styles } from '../const/styles';

const LoggedScreen = props =>
{
    return (
        <View style={Styles.blankScreen}>
            <Text>Logged...</Text>
        </View>
    );
}

export default LoggedScreen;