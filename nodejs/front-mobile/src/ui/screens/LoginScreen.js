import React from 'react';
import { View, Text } from 'react-native';
import { Styles } from '../const/styles';

const LoginScreen = props =>
{
    return (
        <View style={Styles.blankScreen}>
            <Text>Login...</Text>
        </View>
    );
}

export default LoginScreen;