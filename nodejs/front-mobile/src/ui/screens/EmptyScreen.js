import React from 'react';
import { View, Text, Button } from 'react-native';

import { Styles } from '../const/styles';
import Colors from '../const/Colors';
import Routes from '../navigation/Routes';

const EmptyScreen = props =>
{
    return (
        <View style={Styles.blankScreen}>
            <Text>Empty... NOT!</Text>
            <Button title="go to login" onPress={() => props.navigation.navigate({ routeName: Routes.Login })} />
        </View>
    );
}

EmptyScreen.navigationOptions = {
    headerTitle: 'Hello!',
    headerStyle: { backgroundColor: Colors.primaryColor },
    headerTintColor: 'white',
};

export default EmptyScreen;