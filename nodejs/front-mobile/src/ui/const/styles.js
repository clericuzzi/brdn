import { StyleSheet } from 'react-native';

export const Styles = StyleSheet.create(
    {
        blankScreen:
        {
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
        }
    }
);
