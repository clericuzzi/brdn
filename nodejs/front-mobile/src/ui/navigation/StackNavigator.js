import EmptyScreen from '../screens/EmptyScreen';
import LoginScreen from '../screens/LoginScreen';
import LoggedScreen from '../screens/LoggedScreen';

import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

const StackNavigator = createStackNavigator({
    Empty: EmptyScreen,
    Login: LoginScreen,
    Logged: LoggedScreen,
});

export default createAppContainer(StackNavigator);