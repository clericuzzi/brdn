import React from 'react';
import StackNavigator from './src/ui/navigation/StackNavigator';

export default function App()
{
  return <StackNavigator />;
}