'use strict';

const jwk = require('../../../../jwk');
const jwtManager = require('clericuzzi-javascript-aws/managers/jwtManager');
const loginManager = require('clericuzzi-javascript-aws/managers/loginManager');

const typeUtils = require('clericuzzi-javascript/utils/typeUtils');
const arrayUtils = require('clericuzzi-javascript/utils/arrayUtils');

const rdsManager = require('clericuzzi-javascript-aws/managers/rdsManager');
const requestManager = require('clericuzzi-javascript/models/communication/requestManager');
const responseManager = require('clericuzzi-javascript/models/communication/responseManager');

module.exports.companyDetail = async (event, context, callback) =>
{
    let conn = null;

    try
    {
        const info = {
            jwk
        };
        responseManager.success(callback, info);
    }
    catch (err)
    {
        responseManager.error(callback, err);
    }
    finally
    {
        if (typeUtils.isValid(conn))
            conn.release();
    }
};