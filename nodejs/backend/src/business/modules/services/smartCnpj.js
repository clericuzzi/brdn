'use strict';

const CompanySource = require('../../enums/companySource.enum');

const typeUtils = require('clericuzzi-javascript/utils/typeUtils');
const arrayUtils = require('clericuzzi-javascript/utils/arrayUtils');
const stringsValidator = require('clericuzzi-javascript/validation/stringsValidator');

const rdsManager = require('clericuzzi-javascript-aws/managers/rdsManager');
const requestManager = require('clericuzzi-javascript/models/communication/requestManager');
const responseManager = require('clericuzzi-javascript/models/communication/responseManager');

const miningStatistics = require('../../utils/miningStatistics');
const companyCharacteristics = require('../../utils/companyCharacteristics');

/**
 * gets the next available cnpj for testing
 * @param {object} conn database connection 
 */
const getNext = async (conn) =>
{
    if (!conn)
        conn = await rdsManager.newConnectionFromPool();

    const sql = 'select `cnpj` from `mining_smart_cnpj` where `priority` = (select max(`priority`) from `mining_smart_cnpj`) order by rand() limit 1';
    const queryOptions = {
        sql: sql
    };

    const result = await rdsManager.runQuery(queryOptions, conn);
    if (typeUtils.isArray(result))
    {
        if (typeUtils.isArrayAndValid(result))
            return { hasNext: true, cnpj: result[0].cnpj };
        else
            return `Não há mais empresas a consultar...`;
    }
    else
        return null;
};
/**
 * consolidates the information gotten by the system
 * @param {object} companyInfo tested company information
 * @param {object} conn database connection
 */
const consolidate = async (companyInfo, conn) =>
{
    if (companyInfo && companyInfo.name)
    {
        const companyId = await consolidateCompanyInfo(companyInfo, conn);
        await consolidateCompanyPhones(companyId, companyInfo, conn);
        await consolidateCompanyContacts(companyId, companyInfo, conn);
        await consolidateCompanyAddresses(companyId, companyInfo, conn);

        await companyCharacteristics.revoke(conn, companyId, companyCharacteristics.Enum.ToTestSmartCnpj);
        await companyCharacteristics.give(conn, companyId, companyCharacteristics.Enum.TestedSmartCnpj);
    }

    await miningStatistics.smartWebCnpj(conn);
    await rdsManager.runQuery({ sql: 'delete from `mining_smart_cnpj` where `cnpj` = ?', values: [companyInfo.cnpj] }, conn);
};
/**
 * stores the company information
 * @param {object} companyInfo tested company information
 * @param {object} conn database connection
 */
const consolidateCompanyInfo = async (companyInfo, conn) =>
{
    let companyId = null;
    const existingCompany = await rdsManager.runQuery({ sql: 'select `id` from `company` where `cnpj` = ?', values: [companyInfo.cnpj] }, conn);
    if (typeUtils.isArrayAndValid(existingCompany))
    {
        companyId = existingCompany[0].id;
        await rdsManager.runQuery({ sql: 'update `company` set `formal_name` = ?, `fantasy_name` = ?, `email` = ? where `id` = ?', values: [companyInfo.name, companyInfo.name, companyInfo.email, companyId] }, conn);
    }
    else
    {
        const newCompanyResult = await rdsManager.runQuery({ sql: 'insert into `company` values (null, ?, ?, ?, ?, null, null, ?, null, null, now());', values: [companyInfo.cnpj, companyInfo.name, companyInfo.name, companyInfo.email, CompanySource.MiningSmartWebCnpj] }, conn);
        companyId = newCompanyResult.insertId;
    }

    return companyId;
};
/**
 * stores the contact information that was mined
 * @param {*} companyId the current company's id
 * @param {object} companyInfo tested company information
 * @param {object} conn database connection
 */
const consolidateCompanyContacts = async (companyId, companyInfo, conn) =>
{
    const companyContacts = await rdsManager.runQuery({ sql: 'select `c`.`id`, `c`.`name`, `c`.`email` from `company_contact` `c` where `c`.`company_id` = ?', values: [companyId] }, conn);
    for (const contact of companyInfo.contacts)
        if (typeUtils.isStringAndValid(contact.name))
            await consolidateCompanyContact(companyId, contact, companyContacts, conn);
};
/**
 * goes throught each contact consolidating it's phones info and storing them into a single object
 * @param {number} companyId company's id 
 * @param {object} contact the contact to be stored
 * @param {array} companyContacts the mined contact list
 * @param {object} conn database connection
 */
const consolidateCompanyContact = async (companyId, contact, companyContacts, conn) =>
{
    const existingContact = companyContacts.find(i => i.name === contact.name);
    if (!typeUtils.isValid(existingContact))
        await rdsManager.runQuery({ sql: 'insert into `company_contact` values (null, null, ?, null, null, ?, ?, ?)', values: [companyId, contact.name, contact.email, consolidateCompanyContactPhone(contact)] }, conn);
    else
        await rdsManager.runQuery({ sql: 'update `company_contact` set `name` = ?, `email` = ?, `phone` = ?', values: [contact.name, contact.email, consolidateCompanyContactPhone(contact), contact.id] }, conn);
};
/**
 * this function joins all the available contact phones into a single string separated by comma
 * @param {object} contact the contact to be stored 
 */
const consolidateCompanyContactPhone = contact =>
{
    const contactPhones = [];
    if (typeUtils.isValid(contact.phoneCel))
        contactPhones.push(contact.phoneCel);
    if (typeUtils.isValid(contact.phoneCom))
        contactPhones.push(contact.phoneCom);
    if (typeUtils.isValid(contact.phoneRes))
        contactPhones.push(contact.phoneRes);

    return contactPhones.join(`;`);
};
/**
 * extracts all the phones from the current company
 * @param {object} companyInfo 
 */
const extractPhones = companyInfo =>
{
    let currentPhones = companyInfo.phones.map(i => `${i}`);
    for (const contact of companyInfo.contacts)
    {
        if (contact.phoneCel) currentPhones.push(contact.phoneCel);
        if (contact.phoneCom) currentPhones.push(contact.phoneCom);
        if (contact.phoneRes) currentPhones.push(contact.phoneReS);
    }

    return arrayUtils.distinct(currentPhones);
};
/**
 * differentiates the existing company phones from the ones contained in the request data
 * @param {number} companyId the existing company id
 * @param {object} companyInfo the new company data 
 * @param {number} conn database connection
 */
const extractNewPhones = async (companyId, companyInfo, conn) =>
{
    const currentPhones = extractPhones(companyInfo);
    const existingPhones = await rdsManager.runQuery({ sql: 'select `p`.`id`, `p`.`phone` from `company_phone` `c` join `phone` `p` on `p`.`id` = `c`.`phone_id` where `c`.`company_id` = ?;', values: [companyId] }, conn);
    const existingPhoneNumbers = existingPhones.map(i => i.phone);

    return currentPhones.filter(i => !existingPhoneNumbers.includes(i));
};
/**
 * stores the given information to the database
 * @param {number} companyId company's id
 * @param {string} phone the phone to be stored
 * @param {object} conn database connection
 */
const insertNewPhone = async (companyId, phone, conn) =>
{
    const insertPhone = await rdsManager.runQuery({ sql: 'insert into `phone` values (null, ?, ?, null, null)', values: [phone, stringsValidator.brValidatePhoneType(phone)] }, conn);
    await rdsManager.runQuery({ sql: 'insert into `company_phone` values (null, ?, ?)', values: [companyId, insertPhone.insertId] }, conn);
};
/**
 * gets all the phones contained in the company information and stores the ones that were not stored yet
 * @param {number} companyId the company's id
 * @param {object} companyInfo the company information
 * @param {object} conn database connection
 */
const consolidateCompanyPhones = async (companyId, companyInfo, conn) =>
{
    const newPhones = await extractNewPhones(companyId, companyInfo, conn);
    if (typeUtils.isArrayAndValid(newPhones))
    {
        for (const newPhone of newPhones)
            if (stringsValidator.brValidatePhone(newPhone))
                await insertNewPhone(companyId, newPhone, conn);
    }
};
/**
 * stores the address information that was mined
 * @param {number} companyId the current company's id
 * @param {object} companyInfo tested company information
 * @param {object} conn database connection
 */
const consolidateCompanyAddresses = async (companyId, companyInfo, conn) =>
{
    let companyAddresses = await rdsManager.runQuery({ sql: 'select `c`.`id`, `c`.`address` from `company_address` `c` where `c`.`company_id` = ?', values: [companyId] }, conn);
    if (typeUtils.isArrayAndValid(companyAddresses))
        companyAddresses = companyAddresses.map(i => i.address);

    for (const address of companyInfo.addresses)
        if (typeUtils.isStringAndValid(address.address) && !companyAddresses.includes(address.address))
            await consolidateCompanyAddress(companyId, address, conn);
};
/**
 * stores the given address
 * @param {number} companyId the current company's id
 * @param {object} address the information to be stored
 * @param {object} conn database connection
 */
const consolidateCompanyAddress = async (companyId, address, conn) =>
{
    if (typeUtils.isStringAndValid(address.state))
    {
        address.state = address.state.trim();
        if (address.state.length !== 2)
            address.state = null;
    }

    await rdsManager.runQuery({ sql: 'insert into `company_address` values (null, ?, ?, ?, ?, ?, ?, ?, ?)', values: [companyId, address.address, address.number, address.neighbourhood, address.city, address.state, address.zipcode, address.testedIn] }, conn);
};

module.exports.next = async (event, context, callback) =>
{
    context.callbackWaitsForEmptyEventLoop = false;
    let conn = null;
    try
    {
        conn = await rdsManager.newConnectionFromPool();
        const nextTarget = await getNext(conn);
        if (typeUtils.isValid(nextTarget))
            responseManager.success(callback, nextTarget);
        else
            responseManager.success(callback, new Error(`problema ao tentar buscar o próximo cnpj...`));
    }
    catch (err)
    {
        if (conn)
            conn.rollback();
        responseManager.error(callback, err);
    }
    finally
    {
        if (conn)
            conn.release();
    }
};
module.exports.consolidate = async (event, context, callback) =>
{
    context.callbackWaitsForEmptyEventLoop = false;
    let conn = null;
    try
    {
        conn = await rdsManager.newConnectionFromPool(true);

        const companyInfo = requestManager.parse(event);
        await consolidate(companyInfo, conn);

        const nextTarget = await getNext(conn);
        conn.commit();
        if (typeUtils.isValid(nextTarget))
            responseManager.success(callback, nextTarget);
        else
            responseManager.success(callback, new Error(`problema ao tentar buscar o próximo cnpj...`));
    }
    catch (err)
    {
        if (conn)
            conn.rollback();
        responseManager.error(callback, err);
    }
    finally
    {
        if (conn)
            conn.release();
    }
};