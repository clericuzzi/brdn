'use strict';

const CompanySource = require('../../enums/companySource.enum');

const typeUtils = require('clericuzzi-javascript/utils/typeUtils');
const arrayUtils = require('clericuzzi-javascript/utils/arrayUtils');
const stringsValidator = require('clericuzzi-javascript/validation/stringsValidator');

const rdsManager = require('clericuzzi-javascript-aws/managers/rdsManager');
const requestManager = require('clericuzzi-javascript/models/communication/requestManager');
const responseManager = require('clericuzzi-javascript/models/communication/responseManager');

const miningStatistics = require('../../utils/miningStatistics');
const companyCharacteristics = require('../../utils/companyCharacteristics');

/**
 * gets the next city keyword pair for testing
 * @param {object} conn database connection  
 */
const getNext = async conn =>
{
    if (!conn)
        conn = await rdsManager.newConnectionFromPool();

    const sql = 'select `gm`.`id`, `gm`.`city`, `k`.`value` as `keyword` from `mining_google_miner` `gm` join `keyword` `k` on `k`.`id` = `gm`.`keyword_id` where `priority` = (select max(`priority`) from `mining_google_miner`) order by rand() limit 1';
    const queryOptions = {
        sql: sql
    };

    const result = await rdsManager.runQuery(queryOptions, conn);
    if (typeUtils.isArray(result))
    {
        if (typeUtils.isArrayAndValid(result))
            return { hasNext: true, item: result[0] };
        else
            return `Não há mais empresas a minerar...`;
    }
    else
        return null;
}

const consolidateCompany = async (companyInfo, conn) =>
{

};

module.exports.next = async (event, context, callback) =>
{
    context.callbackWaitsForEmptyEventLoop = false;
    let conn = null;
    try
    {
        conn = await rdsManager.newConnectionFromPool();
        const nextTarget = await getNext(conn);
        if (typeUtils.isValid(nextTarget))
            responseManager.success(callback, nextTarget);
        else
            responseManager.success(callback, new Error(`problema ao tentar buscar o próximo cnpj...`));
    }
    catch (err)
    {
        if (conn)
            conn.rollback();
        responseManager.error(callback, err);
    }
    finally
    {
        if (conn)
            conn.release();
    }
};
module.exports.consolidate = async (event, context, callback) =>
{
    context.callbackWaitsForEmptyEventLoop = false;
    let conn = null;
    try
    {
        conn = await rdsManager.newConnectionFromPool(true);
        const companyInfo = requestManager.parse(event);
        await consolidateCompany(companyInfo, conn);

        const nextTarget = await getNext(conn);
        conn.commit();
        if (typeUtils.isValid(nextTarget))
            responseManager.success(callback, nextTarget);
        else
            responseManager.success(callback, new Error(`problema ao tentar buscar o próximo cnpj...`));
    }
    catch (err)
    {
        if (conn)
            conn.rollback();
        responseManager.error(callback, err);
    }
    finally
    {
        if (conn)
            conn.release();
    }
};