'use strict';

const typeUtils = require('clericuzzi-javascript/utils/typeUtils');

const rdsManager = require('clericuzzi-javascript-aws/managers/rdsManager');
const requestManager = require('clericuzzi-javascript/models/communication/requestManager');
const responseManager = require('clericuzzi-javascript/models/communication/responseManager');

module.exports.get = async (event, context, callback) =>
{
    try
    {
        context.callbackWaitsForEmptyEventLoop = false;
        const input = requestManager.parse(event);
        const sql = 'select `url`, `login`, `password` from `macro_login` where `macro` = ?';
        const queryOptions = {
            sql: sql,
            values: [input.macroName]
        };

        const result = await rdsManager.runQuery(queryOptions);
        if (typeUtils.isArrayAndValid(result))
            responseManager.success(callback, result[0]);
        else
            responseManager.success(callback, new Error(`problem with the query: '${sql}'`));
    }
    catch (err)
    {
        responseManager.error(callback, err);
    }
};