const rdsManager = require('clericuzzi-javascript-aws/managers/rdsManager');

module.exports.Enum = {
    LeadSoho: 126,
    LeadMobile: 125,
    LeadMigration: 124,

    ToTestSmartVt: 117,
    ToTestSmartCnpj: 119,

    TestedSmartVt: 118,
    TestedSmartCnpj: 120,

    VT_NO: 127,
    VT_OK: 121,
    VT_GPON: 122,
    VT_METALIC: 123,
};

/**
 * gives the given company the passed characteristics
 */
module.exports.give = async (conn, companyId, ...characteristics) =>
{
    const existingCharacteristics = await rdsManager.runQuery({ sql: 'select `id`, `characteristic_id` from `company_characteristic` where `company_id` = ?', values: [companyId] }, conn);
    const oldCharacteristicIds = existingCharacteristics.map(i => i.id);
    const oldCharacteristics = existingCharacteristics.map(i => i.characteristic_id);
    const newCharacteristics = characteristics.filter(i => !oldCharacteristics.includes(i));
    for (const characteristic of oldCharacteristicIds)
        await rdsManager.runQuery({ sql: 'update `company_characteristic` set `timestamp` = now() where `id` = ?', values: [characteristic] }, conn);
    for (const characteristic of newCharacteristics)
        await rdsManager.runQuery({ sql: 'insert into `company_characteristic` values (null, ?, ?, now())', values: [companyId, characteristic] }, conn);
};
/**
 * revokes the passed characteristics for the given company
 */
module.exports.revoke = async (conn, companyId, ...characteristics) =>
{
    const queryValues = characteristics.map(i => `?`).join(`,`);
    const result = await rdsManager.runQuery({ sql: 'delete from `company_characteristic` where company_id = ? and characteristic_id in (' + queryValues + ')', values: [companyId, ...characteristics] }, conn);
}