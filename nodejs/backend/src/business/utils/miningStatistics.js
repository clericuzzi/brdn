const rdsManager = require('clericuzzi-javascript-aws/managers/rdsManager');

const CompanySource = require('../enums/companySource.enum');

module.exports.smartWebCnpj = async (conn) =>
{
    await rdsManager.runQuery({ sql: 'insert into `mining_statistics` values (?, year(now()), month(now()), day(now()), now())', values: [CompanySource.MiningSmartWebCnpj] }, conn);
}