'use strict';

const rdsManager = require('clericuzzi-javascript-aws/managers/rdsManager');
const responseManager = require('clericuzzi-javascript/models/communication/responseManager');

/**
 * default query method based in a SqlBuilder
 */
module.exports.query = async (event, context, callback) =>
{
    try
    {
        context.callbackWaitsForEmptyEventLoop = false;
        return await rdsManager.query(event, context, callback);
    }
    catch (err)
    {
        return responseManager.error(callback, error, 501);
    }
};

/**
 * default pagination method based in a SqlBuilder
 */
module.exports.paginate = async (event, context, callback) =>
{
    try
    {
        context.callbackWaitsForEmptyEventLoop = false;
        return await rdsManager.paginate(event, context, callback);
    }
    catch (err)
    {
        return responseManager.error(callback, error, 501);
    }
};

/**
 * default listing method based in a SqlBuilder { id: x, label: y}
 */
module.exports.simpleList = async (event, context, callback) =>
{
    try
    {
        context.callbackWaitsForEmptyEventLoop = false;
        return await rdsManager.simpleList(event, context, callback);
    }
    catch (err)
    {
        return responseManager.error(callback, error, 501);
    }
};