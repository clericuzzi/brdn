export class DateFilter
{
  constructor(
    public DateTo: Date = null,
    public DateFrom: Date = null,
  )
  {
  }
}
