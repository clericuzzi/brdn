export enum SiteReportsEnum
{
  CALLS = 1,
  SALES = 2,
  MINING = 3,
  EVALUATION = 4,
  TABULATION = 5,
}
