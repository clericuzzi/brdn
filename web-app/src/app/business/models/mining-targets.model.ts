import { Activity } from 'clericuzzi-lib/entities/models/activity.model';
import { ComponentTypeEnum, CrudableTableColumnDefinitionModel } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

export class MiningTarget extends Activity
{
  public MiningType: number[] = [];
  public CharacteristicsMust: number[] = [];
  public CharacteristicsMustNot: number[] = [];
  constructor(public Id: number = null, public AccountId: number = null, public Name: string = null, public DailyCount: number = null)
  {
    super();
    this.InitDefinition();
  }
  private InitDefinition(): void
  {
    const nameColumn: CrudableTableColumnDefinitionModel = new CrudableTableColumnDefinitionModel(`name`, `Name`, `Empresa`, `Empresas`, ComponentTypeEnum.Text);
    const miningTypeColumn: CrudableTableColumnDefinitionModel = new CrudableTableColumnDefinitionModel(`miningType`, `MiningType`, `Tipo de mineração`, `Tipos de mineração`, ComponentTypeEnum.AutoComplete);
    const characteristicMustColumn: CrudableTableColumnDefinitionModel = new CrudableTableColumnDefinitionModel(`characteristicsMust`, `CharacteristicsMust`, `Características desejadas`, `Características desejadas`, ComponentTypeEnum.AutoComplete);
    const characteristicMustColumnNot: CrudableTableColumnDefinitionModel = new CrudableTableColumnDefinitionModel(`characteristicsMustNot`, `CharacteristicsMustNot`, `Características não desejadas`, `Características não desejadas`, ComponentTypeEnum.AutoComplete);

    this.ColumnDefinitions.Add(`miningType`, miningTypeColumn);
    this.ColumnDefinitions.Add(`name`, nameColumn);
    this.ColumnDefinitions.Add(`characteristicsMust`, characteristicMustColumn);
    this.ColumnDefinitions.Add(`characteristicsMustNot`, characteristicMustColumnNot);
  }
  public ToJson(): any
  {
    const json: any = new Object();
    if (!ObjectUtils.NullOrUndefined(this.Name))
      json['Name'] = this.Name;
    if (!ObjectUtils.NullOrUndefined(this.MiningType))
      json['MiningType'] = Array.isArray(this.MiningType) ? this.MiningType : [this.MiningType];
    if (!ObjectUtils.NullOrUndefined(this.CharacteristicsMust))
      json['CharacteristicsMust'] = Array.isArray(this.CharacteristicsMust) ? this.CharacteristicsMust : [this.CharacteristicsMust];
    if (!ObjectUtils.NullOrUndefined(this.CharacteristicsMustNot))
      json['CharacteristicsMustNot'] = Array.isArray(this.CharacteristicsMustNot) ? this.CharacteristicsMustNot : [this.CharacteristicsMustNot];

    return json;
  }
}
