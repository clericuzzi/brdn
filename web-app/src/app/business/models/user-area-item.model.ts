import { UserArea } from 'app/entities/userArea.model';

export class UserAreaItem extends UserArea
{
  constructor(
    public Id: number = null,
    public UserId: number = null,
    public Cities: string = null,
    public Neighbourhoods: string = null,
  )
  {
    super(Id, UserId, Cities, Neighbourhoods);
  }
}
