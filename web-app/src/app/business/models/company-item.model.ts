import { Company } from 'app/entities/company.model';
// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
// models
import { User } from 'clericuzzi-lib/entities/models/user.model';
import { Account } from 'clericuzzi-lib/entities/models/account.model';
import { CompanyCall } from 'app/entities/companyCall.model';
import { CompanyNote } from 'app/entities/companyNote.model';
import { CompanyContact } from 'app/entities/companyContact.model';
import { CompanyCallback } from 'app/entities/companyCallback.model';
import { JsonUtils } from 'clericuzzi-lib/utils/json-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';
import { AddressItem } from './address-item.model';
import { LineItem } from './line-item.model';
import { CompanySubtype } from 'app/entities/companySubtype.model';
import { CompanySituation } from 'app/entities/companySituation.model';
import { CompanyBill } from 'app/entities/companyBill.model';
import { CompanyActiveService } from 'app/entities/companyActiveService.model';

export class CompanyItem extends Company
{
  public LastTab: string;
  public HasLastTab: boolean;
  constructor(
    public Id: number = null,
    public AccountId: number = null,
    public UserId: number = null,
    public CompanySubtypeId: number = null,
    public Cnpj: string = null,
    public Phone: string = null,
    public Description: string = null,
    public FormalName: string = null,
    public FantasyName: string = null,
    public Latitude: number = null,
    public Longitude: number = null,
    public Timestamp: Date = null,
    public Rank: string = null,
    public ConvergingMessage: string = null,
    public WalletMobile: string = null,
    public WalletLand: string = null,
    public Capital: number = null,
    public DateOpened: Date = null,
    public SituationId: number = null,
    public SalesForceId: string = null,
    public DateLastUpdate: Date = null,

    public LastNote: string = null,
    public HasPendingTest: boolean = false,

    public Calls: CompanyCall[] = null,
    public Notes: CompanyNote[] = null,
    public Contacts: CompanyContact[] = null,

    public Lines: LineItem[] = null,
    public Bills: CompanyBill[] = null,
    public Addresses: AddressItem[] = null,
    public ActiveServices: CompanyActiveService[] = null,

    public Callback: CompanyCallback = null,
  )
  {
    super(Id, AccountId, CompanySubtypeId, UserId, Cnpj, Phone, Description, FormalName, FantasyName, Latitude, Longitude, Timestamp, Rank, ConvergingMessage, WalletMobile, WalletLand, Capital, DateOpened, SituationId, SalesForceId, DateLastUpdate);
  }

  public ParseFromJson(json: any): void
  {
    if (json)
    {
      this.Id = json['Id'];
      this.Cnpj = json['Cnpj'];
      this.Rank = json['Rank'];
      this.Phone = json['Phone'];
      this.UserId = json['UserId'];
      this.Capital = json['Capital'];
      this.Latitude = json['Latitude'];
      this.AccountId = json['AccountId'];
      this.Longitude = json['Longitude'];
      this.FormalName = json['FormalName'];
      this.Description = json['Description'];
      this.FantasyName = json['FantasyName'];
      this.SituationId = json['SituationId'];
      this.SalesForceId = json['SalesForceId'];
      this.CompanySubtypeId = json['CompanySubtypeId'];
      this.Timestamp = DateUtils.FromDateServer(json['Timestamp']);
      this.DateOpened = DateUtils.FromDateServer(json['DateOpened']);
      this.DateLastUpdate = DateUtils.FromDateServer(json['DateLastUpdate']);

      this.LastTab = json['LastTab'];
      this.LastNote = json['LastNote'];
      this.WalletLand = json['WalletLand'];
      this.WalletMobile = json['WalletMobile'];
      this.ConvergingMessage = json['ConvergingMessage'];

      if (!ObjectUtils.NullOrUndefined(json['UserIdParent']))
      {
        this.UserIdParent = new User();
        this.UserIdParent.ParseFromJson(json['UserIdParent']);
      }
      if (!ObjectUtils.NullOrUndefined(json['AccountIdParent']))
      {
        this.AccountIdParent = new Account();
        this.AccountIdParent.ParseFromJson(json['AccountIdParent']);
      }
      if (!ObjectUtils.NullOrUndefined(json['CompanySubtypeIdParent']))
      {
        this.CompanySubtypeIdParent = new CompanySubtype();
        this.CompanySubtypeIdParent.ParseFromJson(json['CompanySubtypeIdParent']);
      }
      if (!ObjectUtils.NullOrUndefined(json['SituationIdParent']))
      {
        this.SituationIdParent = new CompanySituation();
        this.SituationIdParent.ParseFromJson(json['SituationIdParent']);
      }

      this.Calls = JsonUtils.ParseList(json['Calls'], new CompanyCall());
      this.Notes = JsonUtils.ParseList(json['Notes'], new CompanyNote());
      this.Contacts = JsonUtils.ParseList(json['Contacts'], new CompanyContact());
      this.Callback = JsonUtils.ToInstanceOf<CompanyCallback>(new CompanyCallback(), json['Callback']);

      this.Bills = JsonUtils.ParseList(json['Bills'], new CompanyBill());
      this.Lines = JsonUtils.ParseList(json['Lines'], new LineItem());
      this.Addresses = JsonUtils.ParseList(json['Addresses'], new AddressItem());
      this.ActiveServices = JsonUtils.ParseList(json['ActiveServices'], new CompanyActiveService());
    }
  }
}
