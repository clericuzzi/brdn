import { ChartItem } from 'clericuzzi-bi/entities/chart-item.model';
import { JsonUtils } from 'clericuzzi-lib/utils/json-utils';

export class ReportCallsOrSales
{
  constructor(
    public Data: ChartItem[] = null,
  )
  {
  }

  public Parse(): void
  {
    this.Data = JsonUtils.ParseList<ChartItem>(this.Data, new ChartItem());
  }
}
