import { CompanyAddress } from 'app/entities/companyAddress.model';
import { CompanyAvailability } from 'app/entities/companyAvailability.model';
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { Company } from 'app/entities/company.model';
import { GeographyZipcode } from 'clericuzzi-lib/entities/models-geo/geographyZipcode.model';

export class AddressItem extends CompanyAddress
{
  constructor(
    public Id: number = null,
    public CompanyId: number = null,
    public Street: string = null,
    public Number: string = null,
    public Compliment: string = null,
    public ZipcodeId: number = null,
    public TestingTimestamp: Date = null,

    public Availability: CompanyAvailability = null,
  )
  {
    super(Id, CompanyId, Street, Number, Compliment, ZipcodeId, null, TestingTimestamp);
  }

  public ParseFromJson(json: any): void
  {
    if (json)
    {
      this.Id = json['Id'];
      this.Street = json['Street'];
      this.Number = json['Number'];
      this.CompanyId = json['CompanyId'];
      this.ZipcodeId = json['ZipcodeId'];
      this.Compliment = json['Compliment'];
      this.TestingTimestamp = DateUtils.FromDateServer(json['TestingTimestamp']);

      this.CompanyIdParent = new Company();
      this.CompanyIdParent.ParseFromJson(json['CompanyIdParent']);
      this.ZipcodeIdParent = new GeographyZipcode();
      this.ZipcodeIdParent.ParseFromJson(json['ZipcodeIdParent']);

      this.Availability = new CompanyAvailability();
      this.Availability.ParseFromJson(json['Availability']);
    }
  }
}
