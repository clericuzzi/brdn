import { CompanyLine } from 'app/entities/companyLine.model';
import { CompanyLineOperator } from 'app/entities/companyLineOperator.model';
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';
import { Company } from 'app/entities/company.model';
import { PhoneOperator } from 'app/entities/phoneOperator.model';
import { JsonUtils } from 'clericuzzi-lib/utils/json-utils';

export class LineItem extends CompanyLine
{
  constructor(
    public Id: number = null,
    public CompanyId: number = null,
    public Phone: string = null,
    public OperatorId: number = null,
    public OperatorSince: Date = null,
    public TestingTimestamp: Date = null,

    public Operators: CompanyLineOperator[] = null,
  )
  {
    super();
  }
  public ParseFromJson(json: any): void
  {
    if (json)
    {
      this.Id = json['Id'];
      this.Phone = json['Phone'];
      this.CompanyId = json['CompanyId'];
      this.OperatorId = json['OperatorId'];
      this.OperatorSince = DateUtils.FromDateServer(json['OperatorSince']);
      this.TestingTimestamp = DateUtils.FromDateServer(json['TestingTimestamp']);

      if (!ObjectUtils.NullOrUndefined(json['CompanyIdParent']))
      {
        this.CompanyIdParent = new Company();
        this.CompanyIdParent.ParseFromJson(json['CompanyIdParent']);
      }
      if (!ObjectUtils.NullOrUndefined(json['OperatorIdParent']))
      {
        this.OperatorIdParent = new PhoneOperator();
        this.OperatorIdParent.ParseFromJson(json['OperatorIdParent']);
      }

      this.Operators = JsonUtils.ParseList(json['Operators'], new CompanyLineOperator());
    }
  }
}
