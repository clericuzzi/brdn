export class CampaignInfo
{
  constructor(
    public AllowedCharacteristics: number[] = null,
    public NotAllowedCharacteristics: number[] = null,
  )
  {
  }
}
