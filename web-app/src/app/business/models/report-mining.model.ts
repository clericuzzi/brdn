export class ReportMining
{
  constructor(
    public TotalMined: number = null,
    public TotalToMine: number = null,
    public TotalLines: number = null,
    public TotalLinesTested: number = null,
    public TotalLinesUntested: number = null,
    public TotalAddresses: number = null,
    public TotalAddressesTested: number = null,
    public TotalAddressesUntested: number = null,
  )
  {
  }

  public Parse(): void
  {
  }
}
