import { ChartItem } from 'clericuzzi-bi/entities/chart-item.model';

export class ReportEvaluation
{
  constructor(
    public Abr: ChartItem[] = null,
    public Cnpj: ChartItem[] = null,
    public Simplifique: ChartItem[] = null,
    public SmartByCnpj: ChartItem[] = null,
    public SmartByZipNumber: ChartItem[] = null,
    public AbrTested: number = null,
    public AbrUntested: number = null,
    public CnpjTested: number = null,
    public CnpjUntested: number = null,
    public SmartByCnpjTested: number = null,
    public SmartByCnpjUntested: number = null,
    public SmartByZipNumberTested: number = null,
    public SmartByZipNumberUntested: number = null,
  )
  {
  }

  public Parse(): void
  {
  }
}
