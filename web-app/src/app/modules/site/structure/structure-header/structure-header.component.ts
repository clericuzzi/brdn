// components
import { Subject } from 'rxjs';
import { MatDialog } from '@angular/material';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

// utils
import { InputDecoration } from 'clericuzzi-lib/utils/input-decoration';

// services
import { UserDataService } from 'clericuzzi-lib/services/user-data.service';
import { PageTitleService } from 'clericuzzi-lib/services/page-title.service';
import { ProjectRoutingService } from 'app/services/project-routing.service';

// components
import { HostBindedComponent } from 'clericuzzi-lib/modules/shared/util-components/host-binded/host-binded.component';
import { MaterialPopupComponent } from 'clericuzzi-lib/modules/material/components/material-popup/material-popup.component';
import { ConfigUserEditComponent } from 'module-basic/modules/basic/components/config-user-edit/config-user-edit.component';

@Component({
  selector: 'app-structure-header',
  templateUrl: './structure-header.component.html',
  styleUrls: ['./structure-header.component.css']
})
export class StructureHeaderComponent extends HostBindedComponent implements OnInit
{
  @ViewChild(`logo`) Logo: ElementRef;
  @ViewChild(`title`) TitleElement: ElementRef;

  public _ShowingMenu: boolean = true;

  public OnHide: Subject<void> = new Subject<void>();
  public OnShow: Subject<void> = new Subject<void>();
  public CurrentTitle: string;
  constructor(
    private _Dialog: MatDialog,
    private _Routes: ProjectRoutingService,
    private _UserData: UserDataService,
    public pageTitle: PageTitleService,
  )
  {
    super(pageTitle);
    this.ClearComponentClass();
    this.ListenTo(pageTitle.OnTitleChanged, title => this._ChangeTitle(title));
  }

  public get Login(): string { return this._UserData.Session.NameFull; }
  public get Permission(): string { return this._UserData.Session.PermissionName; }
  ngOnInit()
  {
    this.ListenTo(this._UserData.LoginService.UserLoggedOff, () => this._Routes.Login());
    // this.MenuToggle();
  }

  public get CurrentAccount(): string
  {
    return this._UserData.CurrentAccountName;
  }

  public get Title(): string
  {
    return this.CurrentTitle;
  }
  public _ChangeTitle(title: string): void
  {
    InputDecoration.ElementFadeOut(this.TitleElement);
    setTimeout(() => this._DisplayTitle(title), 250);
  }
  private _DisplayTitle(title: string): void
  {
    this.CurrentTitle = title;
    InputDecoration.ElementFadeIn(this.TitleElement);
  }

  public MenuHide(): void
  {
    InputDecoration.ElementFadeIn(this.Logo);
    InputDecoration.ToggleVisibilityById([ `menu-bars`, `menu-arrow` ], `menu-bars`);
    this.OnHide.next();
  }
  public MenuShow(): void
  {
    InputDecoration.ElementFadeOut(this.Logo);
    InputDecoration.ToggleVisibilityById([ `menu-bars`, `menu-arrow` ], `menu-arrow`);
    this.OnShow.next();
  }
  public MenuToggle(): void
  {
    if (!this._ShowingMenu)
      this.MenuShow();
    else
      this.MenuHide();

      this._ShowingMenu = !this._ShowingMenu;
  }

  public EditUser(): void
  {
    MaterialPopupComponent.Popup(this._Dialog, ConfigUserEditComponent, { UserId: this._UserData.Id }, null, null, `450px`, `420px`);
  }

  public Logoff(): void
  {
    MaterialPopupComponent.Confirmation(this._Dialog, this._Logoff.bind(this), null, "Logoff", "Deseja realmente sair do sistema?");
  }
  private _Logoff(): void
  {
    this._UserData.LoginService.Logoff();
  }
}