import { Component, OnInit } from '@angular/core';
import { MatSnackBar, MatDialog } from '@angular/material';

// models

// services
import { UserDataService } from 'clericuzzi-lib/services/user-data.service';
import { HostBindedComponent } from 'clericuzzi-lib/modules/shared/util-components/host-binded/host-binded.component';
import { MaterialPopupComponent } from 'clericuzzi-lib/modules/material/components/material-popup/material-popup.component';
import { AccountSelectionComponent } from 'module-basic/modules/basic/components/account-selection/account-selection.component';

@Component({
  selector: 'app-structure-side-bar-account-picker',
  templateUrl: './structure-side-bar-account-picker.component.html',
})
export class StructureSideBarAccountPickerComponent extends HostBindedComponent implements OnInit
{
  constructor(
    private _Diag: MatDialog,
    private _Snack: MatSnackBar,
    private _UserData: UserDataService,
    )
  {
    super(null);
  }
    
  public get CurrentAccount(): string
  {
    return this._UserData.CurrentAccountName;
  }

  ngOnInit()
  {
    this.ComponentClass = `side-bar-account-picker-container`;
  }

  public ChangeAccount(): void
  {
    if (this._UserData.HasMultipleAccounts)
      MaterialPopupComponent.Popup(this._Diag, AccountSelectionComponent, null, null, null, `450px`, `180px`);
  }
}