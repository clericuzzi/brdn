import { MatSnackBar } from '@angular/material';
import { Component, OnInit, ViewChild, ElementRef, ViewContainerRef, ComponentFactoryResolver } from '@angular/core';

// utils
import { SitePaths } from 'app/modules/site/site-paths.routes';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';
import { InputDecoration } from 'clericuzzi-lib/utils/input-decoration';

// models

// services
import { UserDataService } from 'clericuzzi-lib/services/user-data.service';
import { ProjectRoutingService } from 'app/services/project-routing.service';

// components
import { AccordionPanelComponent } from 'clericuzzi-lib/modules/shared/accordion-panel/accordion-panel.component';
import { AccordionPanelController } from 'clericuzzi-lib/modules/shared/accordion-panel/accordion-panel.controller';
import { SubjectListenerComponent } from 'clericuzzi-lib/modules/shared/subject-listener/subject-listener.component';
import { AccordionPanelItemComponent } from 'clericuzzi-lib/modules/shared/accordion-panel/accordion-panel-item/accordion-panel-item.component';
import { CompanyActionsService } from 'app/services/company-actions.service';

@Component({
  selector: 'app-structure-side-bar',
  templateUrl: './structure-side-bar.component.html',
  styleUrls: ['./structure-side-bar.component.css']
})
export class StructureSideBarComponent extends SubjectListenerComponent implements OnInit
{
  @ViewChild(`items`) Items: ElementRef;
  @ViewChild('itemPanel', { read: ViewContainerRef }) _ContainerItems: ViewContainerRef;

  public Busy: boolean = true;
  private PanelItems: any = {};
  private PanelSubItems: any = {};

  constructor(
    public _Snack: MatSnackBar,
    public _Routing: ProjectRoutingService,
    private _UserData: UserDataService,
    public ComponentFactoryResolver: ComponentFactoryResolver,

    private _CompanyActions: CompanyActionsService,
  )
  {
    super();
  }

  private _AccordionController: AccordionPanelController;
  public get CurrentAccount(): string
  {
    return this._UserData.CurrentAccountName;
  }
  public get MultipleAccounts(): boolean
  {
    return this._UserData.HasMultipleAccounts;
  }

  ngOnInit()
  {
    this._AccordionController = new AccordionPanelController(this._ContainerItems, this.ComponentFactoryResolver);
    this._AccordionController.SingleSelection = false;

    this.ListenTo(this.OnDestroyed, () => this._AccordionController.IsAlive = false);
    this.ListenTo(this._UserData.Accounts.OnAccountChanged, () => this._Draw());
    this.ListenTo(this._UserData.LoginService.UserValidated, () => this._Draw());

    this._Draw();
  }

  private _Draw(): void
  {
    this.PanelItems = {};
    this.PanelSubItems = {};
    this._AccordionController.Clear();

    if (this._UserData.IsManagementLevel)
      this._InitManagement();
    else
      this._InitUser();
  }

  private _NewLeadSoho(): void
  {
    this._CompanyActions.SetCampaignId(1);
    this._Routing.ToSiteRoute(SitePaths.LEAD_NEW);
  }
  private _NewLeadMobile(): void
  {
    this._CompanyActions.SetCampaignId(3);
    this._Routing.ToSiteRoute(SitePaths.LEAD_NEW);
  }
  private _NewLeadHighSpeed(): void
  {
    this._CompanyActions.SetCampaignId(4);
    this._Routing.ToSiteRoute(SitePaths.LEAD_NEW);
  }
  private _NewLeadSfa(): void
  {
    this._CompanyActions.SetCampaignId(5);
    this._Routing.ToSiteRoute(SitePaths.LEAD_NEW);
  }

  private _InitUser(): void
  {
    const leads: AccordionPanelComponent = this._AccordionController.New(`Leads`);
    leads.AddItem(`Agendamentos`, () => this._Routing.ToSiteRoute(SitePaths.LEAD_CALLBACK));
    leads.AddItem(`Novo Lead Móvel`, () => this._NewLeadMobile());
    leads.AddItem(`Novo Lead Soho`, () => this._NewLeadSoho());
    leads.AddItem(`Novo Lead Soho Alta Velocidade`, () => this._NewLeadHighSpeed());
    leads.AddItem(`Novo Lead Sfa`, () => this._NewLeadSfa());
    leads.AddItem(`Meus leads`, () => this._Routing.ToSiteRoute(SitePaths.LEAD_MINE));
  }
  private _InitManagement(): void
  {
    const leads: AccordionPanelComponent = this._AccordionController.New(`Leads`);
    leads.AddItem(`Agendamentos`, () => this._Routing.ToSiteRoute(SitePaths.LEAD_CALLBACK));
    leads.AddItem(`Novo Lead Móvel`, () => this._NewLeadMobile());
    leads.AddItem(`Novo Lead Soho`, () => this._NewLeadSoho());
    leads.AddItem(`Novo Lead Soho Alta Velocidade`, () => this._NewLeadHighSpeed());
    leads.AddItem(`Novo Lead Sfa`, () => this._NewLeadSfa());
    leads.AddItem(`Meus leads`, () => this._Routing.ToSiteRoute(SitePaths.LEAD_MINE));

    const crud: AccordionPanelComponent = this._AccordionController.New(`Cadastros`);
    crud.AddItem(`Departamentos`, () => this._Routing.ToSiteRoute(SitePaths.DEPARTMENTS));
    crud.AddItem(`Login das macros`, () => this._Routing.ToSiteRoute(SitePaths.CRUD_MACRO_LOGIN));
    crud.AddItem(`Máquinas de mineração`, () => this._Routing.ToSiteRoute(SitePaths.MACRO_MACHINES));
    crud.AddItem(`Mensagens de Bloqueio`, () => this._Routing.ToSiteRoute(SitePaths.BLOCKING_MESSAGES));
    crud.AddItem(`Posições`, () => this._Routing.ToSiteRoute(SitePaths.POSITIONS));
    crud.AddItem(`Tabulações que não voltam`, () => this._Routing.ToSiteRoute(SitePaths.BLOCKED_TABULATIONS));
    crud.AddItem(`Usuários`, () => this._Routing.ToSiteRoute(SitePaths.CRUD_USER));
    // crud.AddItem(`Áreas de Atuação`, () => this.AwaitingValidation());
    // crud.AddItem(`Contas`, () => this.AwaitingValidation());
    // crud.AddItem(`Departamentos`, () => this.AwaitingValidation());
    // crud.AddItem(`Permissões`, () => this.AwaitingValidation());

    const campaigns: AccordionPanelComponent = this._AccordionController.New(`Campanhas`, () => this._Routing.ToSiteRoute(SitePaths.CAMPAIGNS));
    // campaigns.AddItem(`Acompanhamento`, () => this.AwaitingValidation());
    // campaigns.AddItem(`Nova Campanha`, () => this.AwaitingValidation());

    const importing: AccordionPanelComponent = this._AccordionController.New(`Importação`, () => this._Routing.ToSiteRoute(SitePaths.IMPORTING_COMPANIES));

    const mining: AccordionPanelComponent = this._AccordionController.New(`Mineração`);
    mining.AddItem(`Alvos Atuais`, () => this._Routing.ToSiteRoute(SitePaths.MINING_CURRENT_TARGETS));
    mining.AddItem(`Definir Alvos`, () => this._Routing.ToSiteRoute(SitePaths.MINING_TARGETS));

    const reports: AccordionPanelComponent = this._AccordionController.New(`Relatórios`);
    reports.AddItem(`Tabulações`, () => this._Routing.ToSiteRoute(SitePaths.REPORTS_TABULATIONS));
    reports.AddItem(`Avaliações`, () => this._Routing.ToSiteRoute(SitePaths.REPORTS_EVALUATIONS));
    reports.AddItem(`Mineração`, () => this._Routing.ToSiteRoute(SitePaths.REPORTS_MINING));
    reports.AddItem(`Ligações`, () => this._Routing.ToSiteRoute(SitePaths.REPORTS_CALLS));
    // reports.AddItem(`Vendas`, () => this._Routing.ToSiteRoute(SitePaths.REPORTS_SELLS));

    this._AccordionController.New(`Log`, () => this._Routing.ToSiteRoute(SitePaths.LOG));
  }

  public _GetPanel(panelName: string): AccordionPanelItemComponent
  {
    if (!ObjectUtils.NullOrUndefined(this.PanelItems[panelName]))
      return this.PanelItems[panelName];
    else
      return this.PanelSubItems[panelName];
  }

  public AwaitingValidation(): void
  {
    this._Snack.open(`Aguardando validação desta funcionalidade...`, `Aviso`);
  }

  public Hide(): void
  {
    InputDecoration.ElementFadeOut(this.Items, 50);
    InputDecoration.ElementClassAdd(`side-bar-body-component`, `side-bar-body-hidden`);
  }
  public Show(): void
  {
    InputDecoration.ElementFadeIn(this.Items);
    InputDecoration.ElementClassRemove(`side-bar-body-component`, `side-bar-body-hidden`);
  }
}
