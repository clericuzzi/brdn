import { Component, OnInit } from '@angular/core';
// utils
// models
import { AddressItem } from 'app/business/models/address-item.model';
// services
// components
// behaviours
import { SelectListItem } from 'clericuzzi-lib/business/behaviours/select-list-item.behaviour';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';

@Component({
  selector: 'app-item-company-address',
  templateUrl: './item-company-address.component.html'
})
export class ItemCompanyAddressComponent extends SelectListItem<AddressItem> implements OnInit
{
  constructor()
  {
    super(null);
  }

  ngOnInit()
  {
  }

  public get TestedIn(): string
  {
    try
    {
      return StringUtils.TransformDateToBr(this.Model.TestingTimestamp);
    }
    catch (err)
    {
      return null;
    }
  }
  public get Tested(): boolean
  {
    return !ObjectUtils.NullOrUndefined(this.Model.Availability, this.Model.Availability.Message);
  }
  public get Address(): string
  {
    if (!ObjectUtils.NullOrUndefined(this.Model, this.Model.Availability, this.Model.Street))
      return this.Model.Street;
    else
      return null;
  }
  public get Message(): string
  {
    if (!ObjectUtils.NullOrUndefined(this.Model, this.Model.Availability, this.Model.Availability.Message))
      return this.Tested ? this.Model.Availability.Message : null;
    else
      return null;
  }
  public get ClosetDistance(): string
  {
    if (!ObjectUtils.NullOrUndefined(this.Model, this.Model.Availability, this.Model.Availability.ClosetDistance))
      return this.Tested ? this.Model.Availability.ClosetDistance.toString() : null;
    else
      return null;
  }
  public get ClosetTopSpeed(): string
  {
    if (!ObjectUtils.NullOrUndefined(this.Model, this.Model.Availability, this.Model.Availability.ClosetTopSpeed))
      return this.Tested ? this.Model.Availability.ClosetTopSpeed.toString() : null;
    else
      return null;
  }
  public get Lines(): string
  {
    if (!ObjectUtils.NullOrUndefined(this.Model, this.Model.Availability, this.Model.Availability.Lines))
      return this.Tested ? this.Model.Availability.Lines.toString() : null;
    else
      return null;
  }
  public get Until20(): string
  {
    if (!ObjectUtils.NullOrUndefined(this.Model, this.Model.Availability))
      return `${this.Model.Availability.Until20} | ${ObjectUtils.NullOrUndefined(this.Model.Availability.Until20Max) ? `-` : this.Model.Availability.Until20Max} | ${this.SipTest(this.Model.Availability.Until20Sip)}`;
    else
      return null;
  }
  public get Above20(): string
  {
    if (!ObjectUtils.NullOrUndefined(this.Model, this.Model.Availability))
      return `${this.Model.Availability.From21UpTo50} | ${ObjectUtils.NullOrUndefined(this.Model.Availability.From21UpTo50Max) ? `-` : this.Model.Availability.From21UpTo50Max} | ${this.SipTest(this.Model.Availability.From21UpTo50Sip)}`;
    else
      return null;
  }
  public get Above50(): string
  {
    if (!ObjectUtils.NullOrUndefined(this.Model, this.Model.Availability))
      return `${this.Model.Availability.Over50} | ${ObjectUtils.NullOrUndefined(this.Model.Availability.Over50Max) ? `-` : this.Model.Availability.Over50Max} | ${this.SipTest(this.Model.Availability.Over50Sip)}`;
    else
      return null;
  }
  public get Network(): string
  {
    if (!ObjectUtils.NullOrUndefined(this.Model, this.Model.Availability))
      return this.Tested ? this.Model.Availability.Network : null;
    else
      return null;
  }
  public get Tecnology(): string
  {
    if (!ObjectUtils.NullOrUndefined(this.Model, this.Model.Availability))
      return this.Tested ? this.Model.Availability.Tecnology : null;
    else
      return null;
  }
  public get TecnologyTv(): string
  {
    if (!ObjectUtils.NullOrUndefined(this.Model, this.Model.Availability))
      return this.Tested ? this.Model.Availability.TecnologyForTv : null;
    else
      return null;
  }

  public SipTest(hasSip: number): string
  {
    if (ObjectUtils.NullOrUndefined(hasSip))
      return `sem SIP`;
    else
    {
      if (hasSip === 1)
        return `COM SIP`;
      else
        return `sem SIP`;
    }
  }
}
