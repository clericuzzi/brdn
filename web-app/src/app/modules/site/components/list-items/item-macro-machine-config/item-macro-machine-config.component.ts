import { Component, OnInit } from '@angular/core';
import { MacroMachineConfig } from 'app/entities/macroMachineConfig.model';
import { SelectListItem } from 'clericuzzi-lib/business/behaviours/select-list-item.behaviour';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

@Component({
  selector: 'app-item-macro-machine-config',
  templateUrl: './item-macro-machine-config.component.html'
})
export class ItemMacroMachineConfigComponent extends SelectListItem<MacroMachineConfig> implements OnInit
{
  public Name: string;
  public Ending: number;
  public Starting: number;
  public Instances: number;
  public Shutsdown: string;
  public Background: string;

  public HasEnding: boolean;
  public HasStarting: boolean;

  constructor()
  {
    super(null);
  }

  ngOnInit()
  {
    this.Name = this.Model.MacroIdParent.Macro;
    this.Ending = this.Model.Ending;
    this.Starting = this.Model.Starting;
    this.Instances = this.Model.Instances;
    this.Shutsdown = StringUtils.TransformYesNo(this.Model.TurnOffAfter.toString(10));
    this.Background = StringUtils.TransformYesNo(this.Model.FromBackground.toString(10));

    this.HasEnding = !ObjectUtils.NullOrUndefined(this.Model.Ending) && this.Model.Ending > 0;
    this.HasStarting = !ObjectUtils.NullOrUndefined(this.Model.Starting) && this.Model.Starting > 0;
  }
}
