import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';
import { SelectListItem } from 'clericuzzi-lib/business/behaviours/select-list-item.behaviour';
import { Component, OnInit } from '@angular/core';
import { BlockedTabulations } from 'app/entities/blockedTabulations.model';

@Component({
  selector: 'app-item-blocked-tabulation',
  templateUrl: './item-blocked-tabulation.component.html'
})
export class ItemBlockedTabulationComponent extends SelectListItem<BlockedTabulations> implements OnInit
{
  constructor()
  {
    super(null);
  }

  ngOnInit()
  {
  }

  public get Name(): string
  {
    if (!ObjectUtils.NullOrUndefined(this.Model))
      return this.Model.TabulationIdParent.Text;
    else
      return null;
  }
}
