import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';
import { SelectListItem } from 'clericuzzi-lib/business/behaviours/select-list-item.behaviour';
import { Component, OnInit } from '@angular/core';
import { ContactDepartment } from 'app/entities/contactDepartment.model';

@Component({
  selector: 'app-item-contact-department',
  templateUrl: './item-contact-department.component.html'
})
export class ItemContactDepartmentComponent extends SelectListItem<ContactDepartment> implements OnInit
{
  constructor()
  {
    super(null);
  }

  ngOnInit()
  {
  }

  public get Name(): string
  {
    if (!ObjectUtils.NullOrUndefined(this.Model))
      return this.Model.Name;
    else
      return null;
  }
}
