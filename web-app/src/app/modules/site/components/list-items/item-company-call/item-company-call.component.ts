import { Component, OnInit } from '@angular/core';
// utils
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
// models
import { CompanyCall } from 'app/entities/companyCall.model';
// services
// components
// behaviours
import { SelectListItem } from 'clericuzzi-lib/business/behaviours/select-list-item.behaviour';

@Component({
  selector: 'app-item-company-call',
  templateUrl: './item-company-call.component.html'
})
export class ItemCompanyCallComponent extends SelectListItem<CompanyCall> implements OnInit
{
  constructor()
  {
    super(null);
  }

  ngOnInit()
  {
  }

  public get DateTime(): string
  {
    const date: Date = ObjectUtils.ReturnIfNotNullNorUndefined(this.Model, this.Model.StartedIn);
    return StringUtils.TransformDateTimeToBr(date);
  }
  public get Creator(): string
  {
    try
    {
      return ObjectUtils.ReturnIfNotNullNorUndefined(this.Model, this.Model.UserIdParent, this.Model.UserIdParent.Name);
    }
    catch (err)
    {
      return null;
    }
  }
  public get Duration(): string
  {
    return null;
  }
  public get Contact(): string
  {
    try
    {
      let contact: string = ObjectUtils.ReturnIfNotNullNorUndefined<string>(this.Model, this.Model.ContactIdParent, this.Model.ContactIdParent.Name);
      if (ObjectUtils.NullUndefinedOrEmpty(contact))
        contact = `SEM CONTATO`;

      return contact;
    }
    catch (err)
    {
      return `SEM CONTATO`;
    }
  }
  public get ContactClass(): string
  {
    if (!ObjectUtils.NullOrUndefined(this.Model.ContactId) && this.Model.ContactId > 0)
      return `font-medium-small font-bold color-black`;
    else
      return `font-medium-small font-bold color-delete`;
  }
  public get Tabulation(): string
  {
    try
    {
      let tabulation: string = ObjectUtils.ReturnIfNotNullNorUndefined<string>(this.Model, this.Model.TabulationIdParent, this.Model.TabulationIdParent.Text);
      if (ObjectUtils.NullUndefinedOrEmpty(tabulation))
        tabulation = `SEM TABULAÇÃO`;

      return tabulation;
    }
    catch (err)
    {
      return `SEM TABULAÇÃO`;
    }
  }
  public get TabulationClass(): string
  {
    if (!ObjectUtils.NullOrUndefined(this.Model.TabulationId) && this.Model.TabulationId > 0)
      return `font-medium-small font-bold color-black`;
    else
      return `font-medium-small  font-bold color-delete`;
  }
}
