import { Component, OnInit } from '@angular/core';
import { SelectListItem } from 'clericuzzi-lib/business/behaviours/select-list-item.behaviour';
import { MacroLogin } from 'app/entities/macroLogin.model';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

@Component({
  selector: 'app-item-macro-login',
  templateUrl: './item-macro-login.component.html'
})
export class ItemMacroLoginComponent extends SelectListItem<MacroLogin> implements OnInit
{
  constructor()
  {
    super(null);
  }

  ngOnInit()
  {
  }

  public get Url(): string
  {
    if (!ObjectUtils.NullOrUndefined(this.Model))
      return this.Model.Url;
    else
      return null;
  }
  public get Name(): string
  {
    if (!ObjectUtils.NullOrUndefined(this.Model))
      return this.Model.Macro;
    else
      return null;
  }
  public get Login(): string
  {
    if (!ObjectUtils.NullOrUndefined(this.Model))
      return this.Model.Login;
    else
      return null;
  }
  public get Password(): string
  {
    if (!ObjectUtils.NullOrUndefined(this.Model))
      return this.Model.Password;
    else
      return null;
  }
}
