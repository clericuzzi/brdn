import { Component, OnInit } from '@angular/core';
import { SelectListItem } from 'clericuzzi-lib/business/behaviours/select-list-item.behaviour';
import { CompanyContact } from 'app/entities/companyContact.model';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

@Component({
  selector: 'app-item-company-contact',
  templateUrl: './item-company-contact.component.html'
})
export class ItemCompanyContactComponent extends SelectListItem<CompanyContact> implements OnInit
{
  constructor()
  {
    super(null);
  }

  ngOnInit()
  {
  }

  public get Name(): string
  {
    return ObjectUtils.ReturnIfNotNullNorUndefined(this.Model, this.Model.Name);
  }
  public get Email(): string
  {
    return ObjectUtils.ReturnIfNotNullNorUndefined(this.Model, this.Model.Email);
  }
  public get Lines(): string
  {
    return null;
  }
}
