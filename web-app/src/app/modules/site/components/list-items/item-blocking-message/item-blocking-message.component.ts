import { Component, OnInit } from '@angular/core';
// utils
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';
// models
import { BlockingMessage } from 'app/entities/blockingMessage.model';
// services
// components
// behaviours
import { SelectListItem } from 'clericuzzi-lib/business/behaviours/select-list-item.behaviour';

@Component({
  selector: 'app-item-blocking-message',
  templateUrl: './item-blocking-message.component.html'
})
export class ItemBlockingMessageComponent extends SelectListItem<BlockingMessage> implements OnInit
{
  constructor()
  {
    super(null);
  }

  ngOnInit()
  {
  }

  public get Name(): string
  {
    if (!ObjectUtils.NullOrUndefined(this.Model))
      return this.Model.Text;
    else
      return null;
  }
}