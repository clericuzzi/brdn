import { Component, OnInit } from '@angular/core';
// utils
// models
import { UserArea } from 'app/entities/userArea.model';
// services
// components
// behaviours
import { SelectListItem } from 'clericuzzi-lib/business/behaviours/select-list-item.behaviour';

@Component({
  selector: 'app-item-user-area',
  templateUrl: './item-user-area.component.html'
})
export class ItemUserAreaComponent extends SelectListItem<UserArea> implements OnInit
{
  constructor()
  {
    super(null);
  }

  ngOnInit()
  {
  }
}