import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';
import { SelectListItem } from 'clericuzzi-lib/business/behaviours/select-list-item.behaviour';
import { ContactPosition } from 'app/entities/contactPosition.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-item-contact-position',
  templateUrl: './item-contact-position.component.html'
})
export class ItemContactPositionComponent extends SelectListItem<ContactPosition> implements OnInit
{
  constructor()
  {
    super(null);
  }

  ngOnInit()
  {
  }

  public get Name(): string
  {
    if (!ObjectUtils.NullOrUndefined(this.Model))
      return this.Model.Name;
    else
      return null;
  }
}
