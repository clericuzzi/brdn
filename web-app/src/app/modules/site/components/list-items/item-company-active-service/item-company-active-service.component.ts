import { Component, OnInit } from '@angular/core';
import { CompanyActiveService } from 'app/entities/companyActiveService.model';
import { SelectListItem } from 'clericuzzi-lib/business/behaviours/select-list-item.behaviour';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

@Component({
  selector: 'app-item-company-active-service',
  templateUrl: './item-company-active-service.component.html'
})
export class ItemCompanyActiveServiceComponent extends SelectListItem<CompanyActiveService> implements OnInit
{
  constructor()
  {
    super(null);
  }

  ngOnInit()
  {
  }
  public get Product(): string
  {
    try
    {
      if (!ObjectUtils.NullUndefinedOrEmpty(this.Model.Product))
        return this.Model.Product;
      else
        return null;
    }
    catch (err)
    {
      return null;
    }
  }
  public get Instance(): string
  {
    try
    {
      if (!ObjectUtils.NullUndefinedOrEmpty(this.Model.Instance))
        return this.Model.Instance;
      else
        return null;
    }
    catch (err)
    {
      return null;
    }
  }
  public get ChargeProfile(): string
  {
    try
    {
      if (!ObjectUtils.NullUndefinedOrEmpty(this.Model.ChargeProfile))
        return this.Model.ChargeProfile;
      else
        return null;
    }
    catch (err)
    {
      return null;
    }
  }
  public get ServiceAddress(): string
  {
    try
    {
      if (!ObjectUtils.NullUndefinedOrEmpty(this.Model.ServiceAddress))
        return this.Model.ServiceAddress;
      else
        return null;
    }
    catch (err)
    {
      return null;
    }
  }
  public get Status(): string
  {
    try
    {
      if (!ObjectUtils.NullUndefinedOrEmpty(this.Model.Status))
        return this.Model.Status;
      else
        return null;
    }
    catch (err)
    {
      return null;
    }
  }
  public get ActivationDate(): string
  {
    try
    {
      if (!ObjectUtils.NullUndefinedOrEmpty(this.Model.ActivationDate))
        return this.Model.ActivationDate;
      else
        return null;
    }
    catch (err)
    {
      return null;
    }
  }
  public get TecnologyAccess(): string
  {
    try
    {
      if (!ObjectUtils.NullUndefinedOrEmpty(this.Model.TecnologyAccess))
        return this.Model.TecnologyAccess;
      else
        return null;
    }
    catch (err)
    {
      return null;
    }
  }
}
