import { Component, OnInit } from '@angular/core';
// utils
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';
// models
import { Tabulation } from 'app/entities/tabulation.model';
// services
// components
// behaviours
import { SelectListItem } from 'clericuzzi-lib/business/behaviours/select-list-item.behaviour';

@Component({
  selector: 'app-item-tabulation',
  templateUrl: './item-tabulation.component.html'
})
export class ItemTabulationComponent extends SelectListItem<Tabulation> implements OnInit
{
  constructor()
  {
    super(null);
  }

  ngOnInit()
  {
  }

  public get Name(): string
  {
    if (!ObjectUtils.NullOrUndefined(this.Model))
      return this.Model.Text;
    else
      return null;
  }
}