import { MatDialog } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { Component, OnInit, ComponentFactoryResolver, ViewChild } from '@angular/core';
// utils
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';
// models
import { CompanyItem } from 'app/business/models/company-item.model';
// services
// components
import { ItemCompanyActionsComponent } from './item-company-actions/item-company-actions.component';
// behaviours
import { SelectListItem } from 'clericuzzi-lib/business/behaviours/select-list-item.behaviour';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';

@Component({
  selector: 'app-item-company',
  templateUrl: './item-company.component.html'
})
export class ItemCompanyComponent extends SelectListItem<CompanyItem> implements OnInit
{
  @ViewChild(`actions`) _ActionsPanel: ItemCompanyActionsComponent;
  constructor(
    private _Diag: MatDialog,

    public factory: ComponentFactoryResolver,
    public sanitizer: DomSanitizer,
  )
  {
    super(null);
    this.NonSelectable = true;
  }

  ngOnInit()
  {
    this._ActionsPanel.Model = this.Model;
  }

  public get Cnpj(): string
  {
    if (!ObjectUtils.NullOrUndefined(this.Model))
      return this.Model.Cnpj;
    else
      return null;
  }
  public get Name(): string
  {
    if (!ObjectUtils.NullOrUndefined(this.Model))
      return this.Model.FantasyName;
    else
      return null;
  }
  public get Phone(): string
  {
    if (!ObjectUtils.NullOrUndefined(this.Model))
      return StringUtils.ToPhoneBr(this.Model.Phone);
    else
      return null;
  }

  public get LastTab(): string
  {
    try
    {
      if (!ObjectUtils.NullUndefinedOrEmpty(this.Model.LastTab))
        return this.Model.LastTab;
      else
        return null;
    }
    catch (err)
    {
      return null;
    }
  }
  public get LastNote(): string
  {
    try
    {
      if (!ObjectUtils.NullUndefinedOrEmpty(this.Model.LastNote))
        return this.Model.LastNote;
      else
        return null;
    }
    catch (err)
    {
      return null;
    }
  }

  public get HasCapital(): boolean
  {
    try
    {
      return !ObjectUtils.NullOrUndefined(this.Model.Capital) &&  this.Model.Capital > 0;
    }
    catch (err)
    {
      return false;
    }
  }
  public get Capital(): string
  {
    try
    {
      return StringUtils.ValueToMoney(this.Model.Capital);
    }
    catch (err)
    {
      return `R$ 0,00`;
    }
  }

  public get HasCnpj(): boolean
  {
    try
    {
      return !ObjectUtils.NullUndefinedOrEmpty(this.Model.Cnpj);
    }
    catch (err)
    {
      return false;
    }
  }
  public get HasPhone(): boolean
  {
    try
    {
      return !ObjectUtils.NullUndefinedOrEmpty(this.Model.Phone);
    }
    catch (err)
    {
      return false;
    }
  }
  public get HasLastTab(): boolean
  {
    try
    {
      return !ObjectUtils.NullUndefinedOrEmpty(this.Model.LastTab);
    }
    catch (err)
    {
      return false;
    }
  }
  public get HasLastNote(): boolean
  {
    try
    {
      return !ObjectUtils.NullUndefinedOrEmpty(this.Model.LastNote);
    }
    catch (err)
    {
      return false;
    }
  }
}
