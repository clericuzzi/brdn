import { Component, OnInit } from '@angular/core';
import { CompanyBill } from 'app/entities/companyBill.model';
import { SelectListItem } from 'clericuzzi-lib/business/behaviours/select-list-item.behaviour';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';

@Component({
  selector: 'app-item-company-bill',
  templateUrl: './item-company-bill.component.html'
})
export class ItemCompanyBillComponent extends SelectListItem<CompanyBill> implements OnInit
{
  constructor()
  {
    super(null);
  }

  ngOnInit()
  {
  }
  public get Billing(): string
  {
    try
    {
      if (!ObjectUtils.NullUndefinedOrEmpty(this.Model.Billing))
        return this.Model.Billing;
      else
        return null;
    }
    catch (err)
    {
      return null;
    }
  }
  public get Serial(): string
  {
    try
    {
      if (!ObjectUtils.NullUndefinedOrEmpty(this.Model.Serial))
        return this.Model.Serial;
      else
        return null;
    }
    catch (err)
    {
      return null;
    }
  }
  public get Value(): string
  {
    try
    {
      if (!ObjectUtils.NullOrUndefined(this.Model.Value))
        return StringUtils.ValueToMoney(this.Model.Value);
      else
        return null;
    }
    catch (err)
    {
      return null;
    }
  }
  public get Due(): string
  {
    try
    {
      if (!ObjectUtils.NullOrUndefined(this.Model.Due))
        return StringUtils.TransformDateToBr(this.Model.Due);
      else
        return null;
    }
    catch (err)
    {
      return null;
    }
  }
}
