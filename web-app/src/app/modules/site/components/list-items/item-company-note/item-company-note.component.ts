import { Component, OnInit } from '@angular/core';
// utils
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
// models
import { CompanyNote } from 'app/entities/companyNote.model';
// services
// components
// behaviours
import { SelectListItem } from 'clericuzzi-lib/business/behaviours/select-list-item.behaviour';

@Component({
  selector: 'app-item-company-note',
  templateUrl: './item-company-note.component.html'
})
export class ItemCompanyNoteComponent extends SelectListItem<CompanyNote> implements OnInit
{
  constructor()
  {
    super(null);
  }

  ngOnInit()
  {
  }

  public get DateTime(): string
  {
    const date: Date = ObjectUtils.ReturnIfNotNullNorUndefined(this.Model, this.Model.Timestamp);
    return StringUtils.TransformDateTimeToBr(date);
  }
  public get Creator(): string
  {
    return ObjectUtils.ReturnIfNotNullNorUndefined(this.Model, this.Model.UserIdParent, this.Model.UserIdParent.Name);
  }
  public get Comment(): string
  {
    if (ObjectUtils.NullOrUndefined(this.Model))
      return null;
    else
    {
      if (ObjectUtils.NullOrUndefined(this.Model.ContactIdParent))
        return this.Model.Comment;
      else
        return `${this.Model.ContactIdParent.Name.toUpperCase()}: ${this.Model.Comment}`;
    }
  }
}
