import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';
import { MacroMachine } from 'app/entities/macroMachine.model';
import { SelectListItem } from 'clericuzzi-lib/business/behaviours/select-list-item.behaviour';
import { Component, OnInit } from '@angular/core';
import { MacroMachineConfigActionsService } from 'app/services/macro-machine-config-actions.service';
import { ProjectRoutingService } from 'app/services/project-routing.service';
import { SitePaths } from 'app/modules/site/site-paths.routes';

@Component({
  selector: 'app-item-macro-machine',
  templateUrl: './item-macro-machine.component.html'
})
export class ItemMacroMachineComponent extends SelectListItem<MacroMachine> implements OnInit
{
  public Name: string;
  constructor(
    private _Routing: ProjectRoutingService,
    private _ConfigActions: MacroMachineConfigActionsService,
  )
  {
    super(null);
  }

  ngOnInit()
  {
    if (!ObjectUtils.NullOrUndefined(this.Model))
    {
      this.Name = this.Model.MachineName;
    }
  }

  public ViewDetails(): void
  {
    this._ConfigActions.SetCurrentMachine(this.Model);
    this._Routing.ToSiteRoute(SitePaths.MACRO_MACHINE_CONFIG);
  }
}
