import { Activity } from 'clericuzzi-lib/entities/models/activity.model';
import { MatDialog } from '@angular/material';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';
import { DomSanitizer } from '@angular/platform-browser';
import { SelectListItem } from 'clericuzzi-lib/business/behaviours/select-list-item.behaviour';
import { Component, OnInit, ComponentFactoryResolver } from '@angular/core';
import { CampaignActionsService } from 'app/services/campaign-actions.service';
import { ProjectRoutingService } from 'app/services/project-routing.service';
import { SitePaths } from 'app/modules/site/site-paths.routes';

@Component({
  selector: 'app-item-campaign',
  templateUrl: './item-campaign.component.html'
})
export class ItemCampaignComponent extends SelectListItem<Activity> implements OnInit
{
  constructor(
    private _Diag: MatDialog,
    private _Routing: ProjectRoutingService,
    private _Actions: CampaignActionsService,

    public factory: ComponentFactoryResolver,
    public sanitizer: DomSanitizer,
    )
  {
    super(null);
    this.NonSelectable = true;
  }

  ngOnInit()
  {
  }
  public get Name(): string
  {
    if (!ObjectUtils.NullOrUndefined(this.Model))
      return this.Model.Name;
    else
      return null;
  }
  public ViewDetails(): void
  {
    this._Actions.Campaign = this.Model;
    this._Routing.ToSiteRoute(SitePaths.CAMPAIGN_SCREEN);
  }
}
