import { Component, OnInit } from '@angular/core';
// utils
// models
import { CompanyCallback } from 'app/entities/companyCallback.model';
import { ResponseMessage } from 'clericuzzi-lib/entities/response/response-message.model';
// services
import { CompanyActionsService } from 'app/services/company-actions.service';
// components
import { HostBindedComponent } from 'clericuzzi-lib/modules/shared/util-components/host-binded/host-binded.component';
import { UserDataService } from 'clericuzzi-lib/services/user-data.service';
import { MatSnackBar } from '@angular/material';
import { BatchResponseMessage } from 'clericuzzi-lib/entities/response/batch-response-message.model';
import { StatusResponseMessage } from 'clericuzzi-lib/entities/response/status-response-message.model';
// behaviours

@Component({
  selector: 'app-comany-callback',
  templateUrl: './comany-callback.component.html'
})
export class ComanyCallbackComponent extends HostBindedComponent implements OnInit
{
  public Models: CompanyCallback[] = [];
  constructor(
    private _Snack: MatSnackBar,
    private _UserData: UserDataService,
    private _CompanyActions: CompanyActionsService,
  )
  {
    super(null);
  }

  ngOnInit()
  {
    this.ClearComponentClass();
    this.AddClasses(`full-width`);

    this.ListenTo(this._UserData.LoginService.UserValidated, () => this._Load());
    this.ListenTo(this._CompanyActions.OnCallbackCleared, response => this._LoadCallback(response));
    this.ListenTo(this._CompanyActions.OnCompanyCallbackAdded, callbackItem => this._Add(callbackItem));
  }
  private _Add(model: CompanyCallback): void
  {
    this.Models.push(model);
  }
  private _Load(): void
  {
    this.Busy = true;
    this.Models = [];
    this._CompanyActions.LoadUserCallbacks(this._LoadCallback.bind(this));
  }
  private _LoadCallback(response: BatchResponseMessage<CompanyCallback>): void
  {
    this.Busy = false;
    response.Parse<CompanyCallback>(new CompanyCallback());
    this.Models = response.Entities;
  }

  public get HasModels(): boolean
  {
    try
    {
      return this.Models.length >= 1;
    }
    catch (err)
    {
      return false;
    }
  }
  public get HasNoCallbacks(): boolean
  {
    try
    {
      return this.Models.length === 0;
    }
    catch (err)
    {
      return false;
    }
  }

  private _CallbackCleared(response: BatchResponseMessage<CompanyCallback>): void
  {
    this._CompanyActions.LoadUserCallbacks(this._LoadCallback.bind(this));
  }
}
