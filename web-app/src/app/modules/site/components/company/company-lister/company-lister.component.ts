import { ListPage } from 'clericuzzi-lib/business/behaviours/list-page.behaviour';
import { MatDialog } from '@angular/material';
import { CompanyItem } from 'app/business/models/company-item.model';
import { CompanyFilter, CompanyColumnsFilter, CompanyColumnsInsert, CompanyColumnsUpdate } from 'app/entities/company.model';
import { UserDataService } from 'clericuzzi-lib/services/user-data.service';
import { PageTitleService } from 'clericuzzi-lib/services/page-title.service';
import { Component, OnInit } from '@angular/core';
import { AutoCompleteFetchService } from 'clericuzzi-lib/services/auto-complete-fetch.service';
import { ComponentGeneratorService } from 'clericuzzi-lib/services/component-generator.service';
import { ServerUrls } from 'clericuzzi-lib/utils/server-urls';
import { ItemCompanyComponent } from '../../list-items/item-company/item-company.component';
import { EntitiesService } from 'clericuzzi-lib/services/entities.service';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';
import { ChartItem } from 'clericuzzi-bi/entities/chart-item.model';
import { SiteReportsEnum } from 'app/business/enums/site-reports-enum';
import { CampaignActionsService } from 'app/services/campaign-actions.service';

@Component({
  selector: 'app-company-lister',
  templateUrl: './company-lister.component.html'
})
export class CompanyListerComponent extends ListPage<CompanyItem, CompanyFilter> implements OnInit
{
  constructor(
    public diag: MatDialog,
    public userData: UserDataService,
    public generator: ComponentGeneratorService,
    public autoCompleteFetch: AutoCompleteFetchService,

    public pageTitle: PageTitleService,
    public _CampaignActions: CampaignActionsService,
    private _ModelActions: EntitiesService,
  )
  {
    super(diag, userData, generator, autoCompleteFetch, pageTitle);
    this.InitFactories(this.generator.Factory, this.generator.Sanitizer);
  }

  ngOnInit()
  {
    this.BaseObject = new CompanyItem();
    this.FilterObject = new CompanyFilter();
    this.ItemComponent = ItemCompanyComponent;
    this.ColumnsFilter = CompanyColumnsFilter;
    this.ColumnsInsert = CompanyColumnsInsert;
    this.ColumnsUpdate = CompanyColumnsUpdate;

    this.Component.PanelSelect.SortFuncion = function (x: CompanyItem, y: CompanyItem): number { return x.FantasyName > y.FantasyName ? 1 : -1; };
    this.Component.PanelSelect.ShowsLoader(true);

    this.Title = `Empresas envolvidas`;
    this.ListTitle = `Empresas envolvidas`;

    this.Component.PanelSelect.CustomSelect = ServerUrls.GetUrl(`companies/involved`);
    this.CanDelete = this.CanInsert = this.CanUpdate = false;

    this.BaseObject.TableDefinition.CanDelete = this.CanDelete;
    this.Initialize(this.BaseObject, this.FilterObject);
    this.Component.Init(this.BaseObject, this.FilterObject, this.ItemComponent, false, false);
    this.Component.InitFilters(... this.ColumnsFilter);

    if (this.CanInsert)
      this.ActionNewItem();
    if (this.CanUpdate)
      this.ActionUpdateItem();

    this.ListenTo(this._ModelActions.OnRefreshRequested, () => this.Component.Reload());

    this.Component.HasFilterPanel = false;
    this.Component.PanelSelect.Actions.Clear();
    this.Component.PanelSelect.ShowsFilter(false);

    if (!ObjectUtils.NullUndefinedOrEmpty(this.ListTitle))
      this.Component.PanelSelect.Title = this.ListTitle;
  }
  public Filter(chartItem: ChartItem, reportType: SiteReportsEnum, userIds: number[]): void
  {
    this.Busy = true;

    this.Component.PanelSelect.AddRequestValue(`user-ids`, userIds);
    this.Component.PanelSelect.AddRequestValue(`chart-item`, chartItem);
    this.Component.PanelSelect.AddRequestValue(`report-type`, reportType);
    this.Component.Reload();
  }
  public FilterCampaign(allowedCharacteristics: number[], notAllowedCharacteristics: number[]): void
  {
    this.Busy = true;

    this.Component.PanelSelect.AddRequestValue(`allowed`, allowedCharacteristics);
    this.Component.PanelSelect.AddRequestValue(`not-allowed`, notAllowedCharacteristics);
    this.Component.PanelSelect.AddRequestValue(`campaign-id`, this._CampaignActions.Campaign.Id);
    this.Component.Reload();
  }
}
