import { Component, OnInit, Input } from '@angular/core';
// utils
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
// models
import { CompanyCallback } from 'app/entities/companyCallback.model';
// services
// components
// behaviours
import { HostBindedComponent } from 'clericuzzi-lib/modules/shared/util-components/host-binded/host-binded.component';

@Component({
  selector: 'app-company-callback-label',
  templateUrl: './company-callback-label.component.html'
})
export class CompanyCallbackLabelComponent extends HostBindedComponent implements OnInit
{
  private static readonly _Title: string = `ligar de volta`;
  private static readonly _TitleNear: string = `ligação próxima`;
  private static readonly _TitleLate: string = `ligação atrasada`;

  private readonly _CurrentDate: Date = new Date();
  @Input() public Model: CompanyCallback;

  public static GetCallbackClass(callbackTime: Date): string
  {
    try
    {
      let title: string = ``;
      const currentDate: Date = new Date();
      if (currentDate < callbackTime)
      {
        const hours = Math.abs(callbackTime.getTime() - currentDate.getTime()) / 36e5;
        if (hours <= 1)
          title = CompanyCallbackLabelComponent._TitleNear;
        else
          title = CompanyCallbackLabelComponent._Title;
      }
      else
        title = CompanyCallbackLabelComponent._TitleLate;

      if (title === CompanyCallbackLabelComponent._Title)
        return `font-medium-small color-ok`;
      else if (title === CompanyCallbackLabelComponent._TitleNear)
        return `font-medium-small color-warning`;
      else
        return `font-medium-small color-delete`;
    }
    catch (err)
    {
      return null;
    }
  }

  constructor()
  {
    super(null);
  }

  ngOnInit()
  {
  }

  public get ElementClass(): string
  {
    const hiddenClass: string = `flex-column visibiliity-fades visibiliity-fade-out visibiliity-fade-removed`;
    try
    {
      if (!ObjectUtils.NullOrUndefined(this.Model, this.Model.CallTime))
        return `flex-column visibiliity-fades`;
      else
        return hiddenClass;
    }
    catch (err)
    {
      return hiddenClass;
    }
  }
  public get HasCallback(): boolean
  {
    try
    {
      return !ObjectUtils.NullOrUndefined(this.Model);
    }
    catch (err)
    {
      return false;
    }
  }
  public get Callback(): string
  {
    if (!ObjectUtils.NullOrUndefined(this.Model, this.Model.CallTime))
      return StringUtils.TransformDateTimeToBr(this.Model.CallTime);
    else
      return null;
  }
  public get Title(): string
  {
    try
    {
      if (this._CurrentDate < this.Model.CallTime)
      {
        const hours = Math.abs(this.Model.CallTime.getTime() - this._CurrentDate.getTime()) / 36e5;
        if (hours <= 1)
          return CompanyCallbackLabelComponent._TitleNear;
        else
          return CompanyCallbackLabelComponent._Title;
      }
      else
        return CompanyCallbackLabelComponent._TitleLate;
    }
    catch (err)
    {
      return null;
    }
  }
  public get TitleClass(): string
  {
    if (this.Title === CompanyCallbackLabelComponent._Title)
      return `font-medium-small color-ok`;
    else if (this.Title === CompanyCallbackLabelComponent._TitleNear)
      return `font-medium-small color-warning`;
    else
      return `font-medium-small color-delete`;
  }
}
