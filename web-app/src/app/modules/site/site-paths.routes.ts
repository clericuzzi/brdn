export class SitePaths
{
  constructor() { }

  public static INDEX: string = `site`;
  public static START = `inicio`;

  // company pages
  public static COMPANIES = `empresas`;
  public static POSITIONS = `cadastros/posicoes`;
  public static USER_AREA = `area-atuacao`;
  public static TABULATION = `tabulacoes`;
  public static DEPARTMENTS = `cadastros/departamentos`;
  public static MACRO_MACHINES = `cadastros/maquinas-macros`;
  public static CRUD_MACRO_LOGIN = `cadastros/login-macro`;
  public static BLOCKING_MESSAGES = `cadastros/mensagens-bloqueio`;
  public static BLOCKED_TABULATIONS = `cadastros/tabulacao-bloqueada`;
  public static MACRO_MACHINE_CONFIG = `cadastros/config-macros`;

  // importing
  public static IMPORTING_COMPANIES = `importacao-lista-empresas`;

  // log
  public static LOG = `log`;

  // campaign
  public static CAMPAIGNS = `campanhas`;
  public static CAMPAIGN_SCREEN = `campanha`;

  // mining targets
  public static MINING_TARGETS = `alvo-mineracao`;
  public static MINING_CURRENT_TARGETS = `alvo-mineracao-atual`;
  // leads
  public static LEAD_NEW = `novo-lead`;
  public static LEAD_MINE = `meus-leads`;
  public static LEAD_CALLBACK = `meus-agendamentos`;

  // crud
  public static CRUD = `cadastros`;
  public static CRUD_USER = `cadastros/usuarios`;
  public static CRUD_ACCOUNT = `cadastros/contas`;
  public static CRUD_ACTIVITY = `cadastros/atividades`;
  public static CRUD_PERMISSION = `cadastros/permissoes`;
  public static CRUD_CHARACTERISTIC = `cadastros/caracteristicas`;

  // reports
  public static REPORTS_SELLS = `relatorios/vendas`;
  public static REPORTS_CALLS = `relatorios/ligacoes`;
  public static REPORTS_MINING = `relatorios/mineracao`;
  public static REPORTS_EVALUATIONS = `relatorios/avaliacoes`;
  public static REPORTS_TABULATIONS = `relatorios/tabulacoes`;

  // users config
  public static USERS_CONFIG = `configuracao/usuarios`;
  public static ACCOUNT_CONFIG = `configuracao/contas`;
  public static PERMISSION_CONFIG = `configuracao/permissoes`;
}
