import { Component, OnInit, ViewChild } from '@angular/core';

// utlils
import { SitePaths } from './site-paths.routes';

// models
import { Account } from 'clericuzzi-lib/entities/models/account.model';

// services
import { LoaderService } from 'clericuzzi-lib/services/loader.service';
import { UserDataService } from 'clericuzzi-lib/services/user-data.service';
import { ProjectRoutingService } from 'app/services/project-routing.service';
import { AutoCompleteFetchService } from 'clericuzzi-lib/services/auto-complete-fetch.service';

// base components
import { WaitingLoaderComponent } from 'clericuzzi-lib/modules/shared/waiting-loader/waiting-loader.component';
import { SubjectListenerComponent } from 'clericuzzi-lib/modules/shared/subject-listener/subject-listener.component';
import { StructureHeaderComponent } from './structure/structure-header/structure-header.component';
import { StructureSideBarComponent } from './structure/structure-side-bar/structure-side-bar.component';

@Component({
  selector: 'app-site',
  templateUrl: './site.component.html'
})
export class SiteComponent extends SubjectListenerComponent implements OnInit
{
  @ViewChild(`header`) private _Header: StructureHeaderComponent;
  @ViewChild(`loader`) private _Loader: WaitingLoaderComponent;
  @ViewChild(`sidebar`) private _Sidebar: StructureSideBarComponent;

  constructor(
    public Loader: LoaderService,
    public UserData: UserDataService,
    public Routing: ProjectRoutingService,
    public AutocompleteFetch: AutoCompleteFetchService,
  )
  {
    super();
  }

  private _UserRedirected: boolean;
  ngOnInit()
  {
    this._UserRedirected = false;
    this.ListenTo(this.Loader.OnHide, () => this._Loader.Hide());
    this.ListenTo(this.Loader.OnShow, () => this._Loader.Show());
    this.ListenTo(this._Header.OnHide, () => this._Sidebar.Hide());
    this.ListenTo(this._Header.OnShow, () => this._Sidebar.Show());

    this.ListenTo(this.UserData.LoginService.UserValidated, accounts => this._UserValidated(accounts));

    this._Loader.Show();
    this.UserData.Validate();
  }
  private _UserValidated(accounts: Account[]): void
  {
    this._Loader.Hide();
    this.UserData.AccountsLoaded(accounts);
    if (!this._UserRedirected)
    {
      this._LoadBaseEntities();
      if (window.location.href.indexOf(`localhost`) < 0)
      {
        if (this.UserData.IsManagementLevel)
          this.Routing.ToSiteRoute(SitePaths.LOG);
        else
          this.Routing.ToSiteRoute(SitePaths.START);
      }
      else
        this.Routing.ToSiteRoute(SitePaths.IMPORTING_COMPANIES);
      this._UserRedirected = true;
    }
  }
  private _LoadBaseEntities(): void
  {
  }
}
