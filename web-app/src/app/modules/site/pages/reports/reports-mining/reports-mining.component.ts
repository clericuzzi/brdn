import { MatSnackBar } from '@angular/material';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

// utils
import { SnackUtils } from 'clericuzzi-lib/utils/snack-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { InputDecoration } from 'clericuzzi-lib/utils/input-decoration';
// models
import { DateFilter } from '../../../../../business/filters/date.filter';
import { ReportMining } from 'app/business/models/report-mining.model';
import { ResponseMessage } from 'clericuzzi-lib/entities/response/response-message.model';
import { CrudableTableColumnDefinitionModel, ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
// services
import { ReportService } from 'app/services/reports.service';
import { PageTitleService } from 'clericuzzi-lib/services/page-title.service';
import { ComponentGeneratorService } from 'clericuzzi-lib/services/component-generator.service';
// components
import { BiCardComponent } from 'clericuzzi-bi/modules/bi-components/bi-card/bi-card.component';
import { BiChartComponent } from 'clericuzzi-bi/modules/bi-components/bi-chart/bi-chart.component';
import { DateInputComponent } from 'clericuzzi-lib/modules/shared/form-controls/date-input/date-input.component';
import { CrudFilterComponent } from 'clericuzzi-lib/modules/shared/crud/crud-filter/crud-filter.component';
import { HostBindedComponent } from 'clericuzzi-lib/modules/shared/util-components/host-binded/host-binded.component';
// behaviours
import { BiChartDataSet } from 'clericuzzi-bi/modules/bi-components/bi-chart/bi-chart-dataset.model';
import { BiChartDataSetItem } from 'clericuzzi-bi/modules/bi-components/bi-chart/bi-chart-dataset-item.model';
import { ChartItem } from 'clericuzzi-bi/entities/chart-item.model';

@Component({
  selector: 'app-reports-mining',
  templateUrl: './reports-mining.component.html'
})
export class ReportsMiningComponent extends HostBindedComponent implements OnInit
{
  @ViewChild(`biCardTotalDone`) private _CardDone: BiCardComponent;
  @ViewChild(`biCardTotalToDo`) private _CardToDo: BiCardComponent;

  @ViewChild(`chartLinesPie`) private _ChartLinesPie: BiChartComponent;
  @ViewChild(`chartAddressPie`) private _ChartAddressPie: BiChartComponent;
  @ViewChild(`chartLines`) private _ChartLines: BiChartComponent;
  @ViewChild(`filter`) private _Filter: CrudFilterComponent;

  @ViewChild(`container`) private _Container: ElementRef;

  private _MiningFilter: DateFilter = new DateFilter();
  constructor(
    private _Snack: MatSnackBar,
    private _Reports: ReportService,
    private _Generator: ComponentGeneratorService,
    public pageTitle: PageTitleService,
  )
  {
    super(pageTitle);
    this.InitFactories(this._Generator.Factory, this._Generator.Sanitizer);
    this.SetTitle(`Relatórios por tipo de mineração`);
  }

  ngOnInit()
  {
    this._InitCards();
    this._InitFilter();
  }
  private _InitCards()
  {
    this._CardDone.AddClasses(`flex-grow`);
    this._CardToDo.AddClasses(`flex-grow margin-l`);

    this._CardDone.Title = `Total minerado no período`;
    this._CardDone.GradientTo = `72b3ff`;
    this._CardDone.GradientFrom = `419aff`;

    this._CardToDo.Title = `Combinação cidade/palavra-chave`;
    this._CardToDo.GradientTo = `fdb54f`;
    this._CardToDo.GradientFrom = `ffcb7f`;
  }
  private _InitFilter()
  {
    this._Filter.ClearComponentClass();

    const toColumn: CrudableTableColumnDefinitionModel = new CrudableTableColumnDefinitionModel(`date_to`, `DateTo`, `Até`, `Atés`, ComponentTypeEnum.Date);
    const fromColumn: CrudableTableColumnDefinitionModel = new CrudableTableColumnDefinitionModel(`date_from`, `DateFrom`, `De`, `Des`, ComponentTypeEnum.Date);

    const fieldFrom: DateInputComponent = this._Filter.Addfilter<DateInputComponent>(fromColumn, fromColumn.ReadableName);
    const fieldTo: DateInputComponent = this._Filter.Addfilter<DateInputComponent>(toColumn, toColumn.ReadableName);

    const date: Date = new Date();
    this._MiningFilter = new DateFilter(new Date(), new Date(date.getFullYear(), date.getMonth(), 1));

    fieldTo.Update(this._MiningFilter);
    fieldFrom.Update(this._MiningFilter);

    this.ListenTo(fieldTo.OnValueChanged, () => this._RequestReport());
    this.ListenTo(fieldFrom.OnValueChanged, () => this._RequestReport());
    this.ListenTo(this._Filter.OnFilterRequested, () => this._RequestReport());
    this._RequestReport();
  }
  private _RequestReport(): void
  {
    if (this.Busy)
      SnackUtils.Open(this._Snack, `Aguarde o retorno do último relatório solicitado...`, `Aviso`);
    else
    {
      this.Busy = true;
      this._Filter.Busy = true;
      this._Reports.Mining(this._MiningFilter, this._ReportCallback.bind(this));
    }
  }
  private _ReportCallback(response: ResponseMessage<ReportMining>): void
  {
    this.Busy = false;
    this._Filter.Busy = false;
    InputDecoration.ElementFadeIn(this._Container);

    const miningReport: ReportMining = response.Parse(new ReportMining());
    miningReport.Parse();

    this._CardDone.Value = miningReport.TotalMined;
    this._CardToDo.Value = miningReport.TotalToMine;

    const minedLine: BiChartDataSet = new BiChartDataSet();
    minedLine.Title = `Minerado`;
    minedLine.borderColor = minedLine.backgroundColor = [`#36A2EB`];
    minedLine.Items = [new ChartItem(null, null, null, `minerado`, null, null, null, null, miningReport.TotalMined)];
    minedLine.Update();

    const addressLine: BiChartDataSet = new BiChartDataSet();
    addressLine.Title = `# de endereços`;
    addressLine.borderColor = addressLine.backgroundColor = [`#8CE78C`];
    addressLine.Items = [new ChartItem(null, null, null, `tested_address`, null, null, null, null, miningReport.TotalAddressesTested)];
    addressLine.Update();

    const linesLine: BiChartDataSet = new BiChartDataSet();
    linesLine.Title = `# de linhas`;
    linesLine.borderColor = linesLine.backgroundColor = [`#EFBAF8`];
    linesLine.Items = [new ChartItem(null, null, null, `tested_lines`, null, null, null, null, miningReport.TotalLinesTested)];
    linesLine.Update();

    this._ChartLines.Type = `bar`;
    this._ChartLines.Labels = [`Mineração`];
    this._ChartLines.SetTitle(`Relatório da mineração entre ${StringUtils.TransformDateToBr(this._MiningFilter.DateFrom)} e ${StringUtils.TransformDateToBr(this._MiningFilter.DateTo)}`);
    this._ChartLines.DataSets = [minedLine, linesLine, addressLine];
    this._ChartLines.Draw();

    const pieDataAddress: BiChartDataSet = new BiChartDataSet();
    pieDataAddress.Items = [new ChartItem(null, null, null, `Testados`, null, null, null, null, miningReport.TotalAddressesTested), new ChartItem(null, null, null, `Não testados`, null, null, null, null, miningReport.TotalAddressesUntested)];
    pieDataAddress.backgroundColor = [`#8CE78C`, `#FF3D67`];
    pieDataAddress.Update();

    this._ChartAddressPie.Type = `pie`;
    this._ChartAddressPie.Labels = pieDataAddress.Items.map(i => i.LabelX);
    this._ChartAddressPie.DataSets = [pieDataAddress];
    this._ChartAddressPie.SetTitle(`Distribuição dos testes de endereço`);
    this._ChartAddressPie.HideAxes();
    this._ChartAddressPie.Draw();

    const pieDataLines: BiChartDataSet = new BiChartDataSet();
    pieDataLines.Items = [new ChartItem(null, null, null, `Testadas`, null, null, null, null, miningReport.TotalLinesTested), new ChartItem(null, null, null, `Não testadas`, null, null, null, null, miningReport.TotalLinesUntested)];
    pieDataLines.backgroundColor = [`#EFBAF8`, `#FF3D67`];
    pieDataLines.Update();

    this._ChartLinesPie.Type = `pie`;
    this._ChartLinesPie.Labels = pieDataLines.Items.map(i => i.LabelX);
    this._ChartLinesPie.DataSets = [pieDataLines];
    this._ChartLinesPie.SetTitle(`Distribuição dos testes de linhas`);
    this._ChartLinesPie.HideAxes();
    this._ChartLinesPie.Draw();
  }
}
