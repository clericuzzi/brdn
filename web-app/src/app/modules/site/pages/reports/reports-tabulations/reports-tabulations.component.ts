import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { HostBindedComponent } from 'clericuzzi-lib/modules/shared/util-components/host-binded/host-binded.component';
import { BiChartComponent } from 'clericuzzi-bi/modules/bi-components/bi-chart/bi-chart.component';
import { CrudFilterComponent } from 'clericuzzi-lib/modules/shared/crud/crud-filter/crud-filter.component';
import { EmployeeFilter } from '../../../../../business/filters/employee.filter';
import { MatSnackBar } from '@angular/material';
import { ReportService } from '../../../../../services/reports.service';
import { ComponentGeneratorService } from 'clericuzzi-lib/services/component-generator.service';
import { PageTitleService } from 'clericuzzi-lib/services/page-title.service';
import { CrudableTableColumnDefinitionModel, ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { DateInputComponent } from 'clericuzzi-lib/modules/shared/form-controls/date-input/date-input.component';
import { SnackUtils } from 'clericuzzi-lib/utils/snack-utils';
import { InputDecoration } from 'clericuzzi-lib/utils/input-decoration';
import { ResponseMessage } from 'clericuzzi-lib/entities/response/response-message.model';
import { BiChartDataSet } from 'clericuzzi-bi/modules/bi-components/bi-chart/bi-chart-dataset.model';
import { BiChartDataSetItem } from 'clericuzzi-bi/modules/bi-components/bi-chart/bi-chart-dataset-item.model';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ReportCallsOrSales } from 'app/business/models/reports-calls-or-sales.model';
import { AutocompleteInputComponent } from 'clericuzzi-lib/modules/shared/form-controls/autocomplete-input/autocomplete-input.component';
import { ServerUrls } from 'clericuzzi-lib/utils/server-urls';
import { AutoCompleteFetchService } from 'clericuzzi-lib/services/auto-complete-fetch.service';
import { CompanyListerComponent } from 'app/modules/site/components/company/company-lister/company-lister.component';
import { SiteReportsEnum } from 'app/business/enums/site-reports-enum';

@Component({
  selector: 'app-reports-tabulations',
  templateUrl: './reports-tabulations.component.html'
})
export class ReportsTabulationsComponent extends HostBindedComponent implements OnInit
{
  @ViewChild(`filter`) private _FilterContainer: CrudFilterComponent;
  @ViewChild(`container`) private _Container: ElementRef;

  @ViewChild(`chartPie`) private _ChartPie: BiChartComponent;
  @ViewChild(`chartLines`) private _ChartLines: BiChartComponent;

  @ViewChild(`companies`) private _Companies: CompanyListerComponent;

  private _Filter: EmployeeFilter = new EmployeeFilter();
  constructor(
    private _Snack: MatSnackBar,
    private _Reports: ReportService,
    private _Generator: ComponentGeneratorService,
    private _AutoComplete: AutoCompleteFetchService,
    public pageTitle: PageTitleService,
  )
  {
    super(pageTitle);
    this.InitFactories(this._Generator.Factory, this._Generator.Sanitizer);
    this.SetTitle(`Relatórios de tabulações por período`);
  }

  ngOnInit()
  {
    this._InitFilter();

    this._ChartPie.AddClick(this._GraphClickEvent.bind(this));
  }
  private _InitFilter(): void
  {
    this._FilterContainer.ClearComponentClass();

    const toColumn: CrudableTableColumnDefinitionModel = new CrudableTableColumnDefinitionModel(`date_to`, `DateTo`, `Até`, `Atés`, ComponentTypeEnum.Date);
    const fromColumn: CrudableTableColumnDefinitionModel = new CrudableTableColumnDefinitionModel(`date_from`, `DateFrom`, `De`, `Des`, ComponentTypeEnum.Date);
    const employeeColumn: CrudableTableColumnDefinitionModel = new CrudableTableColumnDefinitionModel(`employee`, `EmployeeIds`, `Colaborador`, `Colaboradores`, ComponentTypeEnum.AutoComplete);

    const fieldFrom: DateInputComponent = this._FilterContainer.Addfilter<DateInputComponent>(fromColumn, fromColumn.ReadableName);
    const fieldTo: DateInputComponent = this._FilterContainer.Addfilter<DateInputComponent>(toColumn, toColumn.ReadableName);
    const fieldEmployee: AutocompleteInputComponent<number> = this._FilterContainer.Addfilter<AutocompleteInputComponent<number>>(employeeColumn, employeeColumn.ReadableName);
    fieldEmployee.MultiSelect = true;

    const previousDate: Date = new Date();
    if (previousDate.getDay() === 1 || previousDate.getDay() === 2)
      previousDate.setDate(previousDate.getDate() - 4);
    else
      previousDate.setDate(previousDate.getDate() - 2);

    this._Filter = new EmployeeFilter(new Date(), previousDate);

    fieldTo.Update(this._Filter);
    fieldFrom.Update(this._Filter);
    fieldEmployee.Update(this._Filter);

    this._AutoComplete.FetchData(ServerUrls.GetUrl(`listing/employees`), fieldEmployee, `user`);
    this.ListenTo(fieldTo.OnValueChanged, () => this._RequestReport());
    this.ListenTo(fieldFrom.OnValueChanged, () => this._RequestReport());
    this.ListenTo(this._FilterContainer.OnFilterRequested, () => this._RequestReport());
    this._RequestReport();
  }
  private _RequestReport(): void
  {
    if (this.Busy)
      SnackUtils.Open(this._Snack, `Aguarde o retorno do último relatório solicitado...`, `Aviso`);
    else
    {
      this.Busy = true;
      this._FilterContainer.Busy = true;
      this._Reports.Tabulations(this._Filter, this._ReportCallback.bind(this));
    }
  }
  private _ReportCallback(response: ResponseMessage<ReportCallsOrSales>): void
  {
    this.Busy = false;
    this._FilterContainer.Busy = false;
    InputDecoration.ElementFadeIn(this._Container);

    const reportData: ReportCallsOrSales = response.Parse(new ReportCallsOrSales());
    reportData.Parse();

    this._ChartLines.HideAxes();
    this._ChartLines.BarAddDataset(`Tabulações por vendedor`, reportData.Data);
    this._ChartLines.Draw();

    this._ChartPie.HideAxes();
    this._ChartPie.HideLegend();
    this._ChartPie.PieAddDataset(`Total de tabulações no período`, reportData.Data);
  }
  private _GraphClickEvent(event: MouseEvent, array: any): void
  {
    this._Companies.Filter(this._ChartPie.DataSets[0].Items[array[0][`_index`]], SiteReportsEnum.TABULATION, this._Filter.EmployeeIds);
  }
}
