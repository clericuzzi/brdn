import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { HostBindedComponent } from 'clericuzzi-lib/modules/shared/util-components/host-binded/host-binded.component';
import { BiChartComponent } from 'clericuzzi-bi/modules/bi-components/bi-chart/bi-chart.component';
import { CrudFilterComponent } from 'clericuzzi-lib/modules/shared/crud/crud-filter/crud-filter.component';
import { EmployeeFilter } from '../../../../../business/filters/employee.filter';
import { MatSnackBar } from '@angular/material';
import { ReportService } from '../../../../../services/reports.service';
import { ComponentGeneratorService } from 'clericuzzi-lib/services/component-generator.service';
import { PageTitleService } from 'clericuzzi-lib/services/page-title.service';
import { CrudableTableColumnDefinitionModel, ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { DateInputComponent } from 'clericuzzi-lib/modules/shared/form-controls/date-input/date-input.component';
import { SnackUtils } from 'clericuzzi-lib/utils/snack-utils';
import { InputDecoration } from 'clericuzzi-lib/utils/input-decoration';
import { ResponseMessage } from 'clericuzzi-lib/entities/response/response-message.model';
import { ReportEvaluation } from 'app/business/models/report-evaluations.model';
import { DateFilter } from 'app/business/filters/date.filter';
import { BiChartDataSet } from 'clericuzzi-bi/modules/bi-components/bi-chart/bi-chart-dataset.model';
import { BiChartDataSetItem } from 'clericuzzi-bi/modules/bi-components/bi-chart/bi-chart-dataset-item.model';
import { ChartItem } from 'clericuzzi-bi/entities/chart-item.model';

@Component({
  selector: 'app-reports-smart',
  templateUrl: './reports-smart.component.html'
})
export class ReportsSmartComponent extends HostBindedComponent implements OnInit
{
  @ViewChild(`filter`) private _FilterContainer: CrudFilterComponent;
  @ViewChild(`container`) private _Container: ElementRef;

  @ViewChild(`chartLines`) private _ChartLines: BiChartComponent;

  @ViewChild(`chartAbr`) private _ChartAbr: BiChartComponent;
  @ViewChild(`chartCnpj`) private _ChartCnpj: BiChartComponent;
  @ViewChild(`chartSmartCnpj`) private _ChartSmartCnpj: BiChartComponent;
  @ViewChild(`chartSmartZipNumber`) private _ChartSmartZipNumber: BiChartComponent;

  private _Filter: DateFilter = new DateFilter();
  constructor(
    private _Snack: MatSnackBar,
    private _Reports: ReportService,
    private _Generator: ComponentGeneratorService,
    public pageTitle: PageTitleService,
  )
  {
    super(pageTitle);
    this.InitFactories(this._Generator.Factory, this._Generator.Sanitizer);
    this.SetTitle(`Relatórios por tipo de avaliação`);
  }

  ngOnInit()
  {
    this._ChartAbr.HideAxes();
    this._ChartCnpj.HideAxes();
    this._ChartSmartCnpj.HideAxes();
    this._ChartSmartZipNumber.HideAxes();

    this._InitFilter();
  }

  private _InitFilter(): void
  {
    this._FilterContainer.ClearComponentClass();

    const toColumn: CrudableTableColumnDefinitionModel = new CrudableTableColumnDefinitionModel(`date_to`, `DateTo`, `Até`, `Atés`, ComponentTypeEnum.Date);
    const fromColumn: CrudableTableColumnDefinitionModel = new CrudableTableColumnDefinitionModel(`date_from`, `DateFrom`, `De`, `Des`, ComponentTypeEnum.Date);

    const fieldFrom: DateInputComponent = this._FilterContainer.Addfilter<DateInputComponent>(fromColumn, fromColumn.ReadableName);
    const fieldTo: DateInputComponent = this._FilterContainer.Addfilter<DateInputComponent>(toColumn, toColumn.ReadableName);

    const date: Date = new Date();
    this._Filter = new EmployeeFilter(new Date(), new Date(date.getFullYear(), date.getMonth(), 1));

    fieldTo.Update(this._Filter);
    fieldFrom.Update(this._Filter);

    this.ListenTo(fieldTo.OnValueChanged, () => this._RequestReport());
    this.ListenTo(fieldFrom.OnValueChanged, () => this._RequestReport());
    this.ListenTo(this._FilterContainer.OnFilterRequested, () => this._RequestReport());
    this._RequestReport();
  }
  private _RequestReport(): void
  {
    if (this.Busy)
      SnackUtils.Open(this._Snack, `Aguarde o retorno do último relatório solicitado...`, `Aviso`);
    else
    {
      this.Busy = true;
      this._FilterContainer.Busy = true;
      this._Reports.Evaluations(this._Filter, this._ReportCallback.bind(this));
    }
  }
  private _ReportCallback(response: ResponseMessage<ReportEvaluation>): void
  {
    this.Busy = false;
    this._FilterContainer.Busy = false;
    InputDecoration.ElementFadeIn(this._Container);

    const reportData: ReportEvaluation = response.Parse(new ReportEvaluation());
    reportData.Parse();

    this._ChartAbr.PieAddDataset(`Avaliações Abr`, [ChartItem.SimpleItem(`Testadas`, reportData.AbrTested, `#8CE78C`), ChartItem.SimpleItem(`Não testadas`, reportData.AbrUntested, `#FF3D67`)]);
    this._ChartCnpj.PieAddDataset(`Cnpj's avaliados`, [ChartItem.SimpleItem(`Testados`, reportData.CnpjTested, `#8CE78C`), ChartItem.SimpleItem(`Não testados`, reportData.CnpjUntested, `#FF3D67`)]);
    this._ChartSmartCnpj.PieAddDataset(`Smart Cnpj`, [ChartItem.SimpleItem(`Testados`, reportData.SmartByCnpjTested, `#8CE78C`), ChartItem.SimpleItem(`Não Testados`, reportData.SmartByCnpjUntested, `#FF3D67`)]);
    this._ChartSmartZipNumber.PieAddDataset(`Smart Cep`, [ChartItem.SimpleItem(`Testadas`, reportData.SmartByZipNumberTested, `#8CE78C`), ChartItem.SimpleItem(`Não testadas`, reportData.SmartByZipNumberUntested, `#FF3D67`)]);

    const data: ChartItem[] = reportData.Abr.concat(reportData.Cnpj).concat(reportData.SmartByCnpj).concat(reportData.SmartByZipNumber);
    this._ChartLines.LineAddDataset(`Testes diários`, data);
  }
}
