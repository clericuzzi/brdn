import { Component, OnInit, ChangeDetectorRef, ViewContainerRef, ViewChild } from '@angular/core';
import { HostBindedComponent } from 'clericuzzi-lib/modules/shared/util-components/host-binded/host-binded.component';
import { MatSnackBar, MatDialog } from '@angular/material';
import { UserDataService } from 'clericuzzi-lib/services/user-data.service';
import { ComponentGeneratorService } from 'clericuzzi-lib/services/component-generator.service';
import { PageTitleService } from 'clericuzzi-lib/services/page-title.service';
import { AutocompleteInputComponent } from 'clericuzzi-lib/modules/shared/form-controls/autocomplete-input/autocomplete-input.component';
import { CrudableTableColumnDefinitionModel, ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { AutoCompleteFetchService } from 'clericuzzi-lib/services/auto-complete-fetch.service';
import { ServerUrls } from 'clericuzzi-lib/utils/server-urls';
import { BatchResponseMessageList, BatchResponseMessage } from 'clericuzzi-lib/entities/response/batch-response-message.model';
import { MiningTarget } from 'app/business/models/mining-targets.model';
import { ComboInputValueModel } from 'clericuzzi-lib/modules/shared/form-controls/custom-inputs/combo-input/combo-input-value.model';
import { CrudFilterComponent } from 'clericuzzi-lib/modules/shared/crud/crud-filter/crud-filter.component';
import { ListPage } from 'clericuzzi-lib/business/behaviours/list-page.behaviour';
import { CompanyItem } from 'app/business/models/company-item.model';
import { ItemCompanyComponent } from 'app/modules/site/components/list-items/item-company/item-company.component';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';
import { SnackUtils } from 'clericuzzi-lib/utils/snack-utils';
import { MaterialPopupComponent } from 'clericuzzi-lib/modules/material/components/material-popup/material-popup.component';
import { MiningService } from 'app/services/mining.service';
import { StatusResponseMessage } from 'clericuzzi-lib/entities/response/status-response-message.model';
import { LoaderService } from 'clericuzzi-lib/services/loader.service';
import { MacroLoginTable } from 'app/entities/macroLogin.model';
import { CustomField } from 'clericuzzi-lib/entities/crud/custom-field.model';

@Component({
  selector: 'app-mining-set-target',
  templateUrl: './mining-set-target.component.html'
})
export class MiningSetTargetComponent extends ListPage<CompanyItem, MiningTarget> implements OnInit
{
  private _LoadsOnStart: boolean = true;
  public CanClearTargets: boolean = true;
  public CanDefineTargets: boolean = false;
  public ComaniesReturned: string = ``;

  constructor(
    public diag: MatDialog,
    public snack: MatSnackBar,
    public loader: LoaderService,
    public userData: UserDataService,
    public generator: ComponentGeneratorService,
    public autoCompleteFetch: AutoCompleteFetchService,

    public pageTitle: PageTitleService,
    private _MiningActions: MiningService,
  )
  {
    super(diag, userData, generator, autoCompleteFetch, pageTitle);
    this.InitFactories(this.generator.Factory, this.generator.Sanitizer);
  }

  ngOnInit()
  {
    this.BaseObject = new CompanyItem();
    this.FilterObject = new MiningTarget();
    this.ItemComponent = ItemCompanyComponent;
    this.ColumnsFilter = [`miningType`, `name`, `characteristicsMust`, `characteristicsMustNot`];
    this.ColumnsInsert = this.ColumnsUpdate = [];

    this.Component.PanelSelect.SortFuncion = function (x: CompanyItem, y: CompanyItem): number { return x.FantasyName > y.FantasyName ? 1 : -1; };
    this.Component.PanelSelect.CustomSelect = ServerUrls.GetUrl(`mining/set-target-filter`);

    this.Title = `Lista de empresas com prioridade na automação selecioanda`;
    this.ListTitle = `Empresas na fila de automação`;

    this.CanDelete = this.CanInsert = this.CanUpdate = this._LoadsOnStart = false;

    if (!ObjectUtils.NullOrUndefined(this.BaseObject))
    {
      this.BaseObject.TableDefinition.CanDelete = this.CanDelete;
      this.Initialize(this.BaseObject, this.FilterObject);
      this.Component.Init(this.BaseObject, this.FilterObject, this.ItemComponent, false, this._LoadsOnStart);
      this.Component.InitFilters(... this.ColumnsFilter);

      const miningTypeComponent: AutocompleteInputComponent<number> = this.Component.PanelFilter.GetComponent(`MiningType`);
      miningTypeComponent.MultiSelect = true;
      if (!ObjectUtils.NullOrUndefined(miningTypeComponent))
        this.Component.LoadExternalData(ServerUrls.GetUrl(`crud/${MacroLoginTable}/listing`), new CustomField(MacroLoginTable, `MiningType`));

      this.ListenTo(this.Component.PanelSelect.OnDataRequested, model => this._RequestSent());
      this.ListenTo(this.Component.PanelSelect.OnCustomSelectCallback, items => this._RequestReturned(items));

      this.SetTitle(this.Title);
      if (!ObjectUtils.NullUndefinedOrEmpty(this.ListTitle))
        this.Component.PanelSelect.Title = this.ListTitle;
    }

    this.autoCompleteFetch.FetchAutocompleteData(ServerUrls.GetUrl(`listing/campaign-screen`), this._FillCharacteristicsInfo.bind(this));
  }
  private _FillCharacteristicsInfo(response: BatchResponseMessageList): void
  {
    this.Component.PanelFilter.GetComponent<AutocompleteInputComponent<number>>(`CharacteristicsMust`).MultiSelect = true;
    this.Component.PanelFilter.GetComponent<AutocompleteInputComponent<number>>(`CharacteristicsMustNot`).MultiSelect = true;

    this.Component.PanelFilter.GetComponent<AutocompleteInputComponent<number>>(`CharacteristicsMust`).SetItems(this.autoCompleteFetch.GetAutocompleteData(`characteristic`, response));
    this.Component.PanelFilter.GetComponent<AutocompleteInputComponent<number>>(`CharacteristicsMustNot`).SetItems(this.autoCompleteFetch.GetAutocompleteData(`characteristic`, response));
  }

  private _RequestSent(): void
  {
    this.Busy = true;
    this.CanClearTargets = false;
    this.CanDefineTargets = false;
    this.ComaniesReturned = `Aguardando consulta`;
    this.Component.PanelFilter.Busy = true;
    this.Component.PanelSelect.ClearData();
  }
  private _RequestReturned(response: BatchResponseMessage<CompanyItem>): void
  {
    this.Busy = false;
    this.Component.PanelFilter.Busy = false;
    this.CanClearTargets = true;
    if (response.HasErrors)
    {
      this.CanDefineTargets = false;
      SnackUtils.OpenError(this.snack, response.ErrorMessage);
    }
    else
    {
      const itemsReturned: number = response.Pagination.TotalCount;
      if (itemsReturned === 0)
        this.ComaniesReturned = `Nenhuma empresa se encaixa à consulta`;
      else if (itemsReturned === 1)
        this.ComaniesReturned = `${itemsReturned} empresa se encaixa à consulta`;
      else
        this.ComaniesReturned = `${itemsReturned} empresas se encaixas à consulta`;

      this.CanDefineTargets = itemsReturned > 0;
    }
  }

  public ClearTargets(): void
  {
    const miningTarget: MiningTarget = this.Component.PanelFilter.CurrentModel as MiningTarget;
    if (miningTarget.MiningType.length === 0)
      SnackUtils.OpenError(this.snack, `Selecione um tipo de automação`);
    else
      MaterialPopupComponent.Confirmation(this.diag, this._ClearTargetsAction.bind(this), null, `Confirmação`, `Deseja realmente limpar os alvos para as automações selecionadas?`);
  }
  private _ClearTargetsAction(): void
  {
    this.loader.Show();
    this._MiningActions.ClearTargets(this.Component.PanelFilter.CurrentModel as MiningTarget, response => { this.loader.Hide(); StatusResponseMessage.HandleResponse(response, this.snack, null, null, true, true, `Operação realizada com sucesso`); });
  }

  public DefineTargets(): void
  {
    const miningTarget: MiningTarget = this.Component.PanelFilter.CurrentModel as MiningTarget;
    if (miningTarget.MiningType.length === 0 && miningTarget.CharacteristicsMustNot.length === 0 && miningTarget.CharacteristicsMustNot.length === 0)
      SnackUtils.OpenError(this.snack, `Selecione alguma característica`);
    else
      MaterialPopupComponent.Confirmation(this.diag, this._DefineTargetsAction.bind(this), null, `Confirmação`, `Deseja realmente definir estas empresas como prioridade para as automações selecionadas?`);
  }
  private _DefineTargetsAction(): void
  {
    this._MiningActions.DefineTargets(this.Component.PanelFilter.CurrentModel as MiningTarget);
    SnackUtils.Open(this.snack, `Esta operação pode levar um pouco mais de tempo, pode seguir com o trabalho enquanto ela é realizada`);
  }
}
