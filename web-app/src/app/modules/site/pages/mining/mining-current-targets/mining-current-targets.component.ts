import { Component, OnInit } from '@angular/core';
import { ListPage } from 'clericuzzi-lib/business/behaviours/list-page.behaviour';
import { MatDialog } from '@angular/material';
import { UserDataService } from 'clericuzzi-lib/services/user-data.service';
import { ComponentGeneratorService } from 'clericuzzi-lib/services/component-generator.service';
import { AutoCompleteFetchService } from 'clericuzzi-lib/services/auto-complete-fetch.service';
import { PageTitleService } from 'clericuzzi-lib/services/page-title.service';
import { Company, CompanyColumnsFilter } from 'app/entities/company.model';
import { ItemCompanyComponent } from 'app/modules/site/components/list-items/item-company/item-company.component';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';
import { AutocompleteInputComponent } from 'clericuzzi-lib/modules/shared/form-controls/autocomplete-input/autocomplete-input.component';
import { ComboInputValueModel } from 'clericuzzi-lib/modules/shared/form-controls/custom-inputs/combo-input/combo-input-value.model';
import { ServerUrls } from 'clericuzzi-lib/utils/server-urls';
import { CompanyItem } from 'app/business/models/company-item.model';
import { MiningTarget } from 'app/business/models/mining-targets.model';
import { MacroLoginTable } from 'app/entities/macroLogin.model';
import { CustomField } from 'clericuzzi-lib/entities/crud/custom-field.model';

@Component({
  selector: 'app-mining-current-targets',
  templateUrl: './mining-current-targets.component.html'
})
export class MiningCurrentTargetsComponent extends ListPage<CompanyItem, MiningTarget> implements OnInit
{
  private _LoadsOnStart: boolean = true;
  constructor(
    public diag: MatDialog,
    public userData: UserDataService,
    public generator: ComponentGeneratorService,
    public autoCompleteFetch: AutoCompleteFetchService,

    public pageTitle: PageTitleService,
  )
  {
    super(diag, userData, generator, autoCompleteFetch, pageTitle);
    this.InitFactories(this.generator.Factory, this.generator.Sanitizer);
  }

  ngOnInit()
  {
    this.BaseObject = new CompanyItem();
    this.FilterObject = new MiningTarget();
    this.ItemComponent = ItemCompanyComponent;
    this.ColumnsFilter = [`miningType`];
    this.ColumnsInsert = this.ColumnsUpdate = [];

    this.Component.PanelSelect.SortFuncion = function (x: CompanyItem, y: CompanyItem): number { return x.FantasyName > y.FantasyName ? 1 : -1; };
    this.Component.PanelSelect.CustomSelect = ServerUrls.GetUrl(`mining/current-targets`);

    this.Title = `Lista de empresas com prioridade na automação selecioanda`;
    this.ListTitle = `Empresas na fila de automação`;

    this.CanDelete = this.CanInsert = this.CanUpdate = this._LoadsOnStart = false;

    if (!ObjectUtils.NullOrUndefined(this.BaseObject))
    {
      this.BaseObject.TableDefinition.CanDelete = this.CanDelete;
      this.Initialize(this.BaseObject, this.FilterObject);
      this.Component.Init(this.BaseObject, this.FilterObject, this.ItemComponent, false, this._LoadsOnStart);
      this.Component.InitFilters(... this.ColumnsFilter);

      const miningTypeComponent: AutocompleteInputComponent<number> = this.Component.PanelFilter.GetComponent(`MiningType`);
      miningTypeComponent.MultiSelect = false;
      const items: ComboInputValueModel<number>[] = [];
      items.push(new ComboInputValueModel<number>(6, `Abr`));
      items.push(new ComboInputValueModel<number>(3, `Google cnpj`));
      items.push(new ComboInputValueModel<number>(4, `Google mineração`));
      items.push(new ComboInputValueModel<number>(1, `Smart cep e número`));
      items.push(new ComboInputValueModel<number>(2, `Smart cnpj`));
      items.push(new ComboInputValueModel<number>(5, `Simplifique`));
      miningTypeComponent.SetItems(items);

      if (!ObjectUtils.NullOrUndefined(miningTypeComponent))
        this.Component.LoadExternalData(ServerUrls.GetUrl(`crud/${MacroLoginTable}/listing`), new CustomField(MacroLoginTable, `MiningType`));

      this.ListenTo(this.Component.PanelSelect.OnDataRequested, model => { this.Component.PanelSelect.ClearData(); this.Component.PanelFilter.Busy = true; });
      this.ListenTo(this.Component.PanelSelect.OnCustomSelectCallback, items => this.Component.PanelFilter.Busy = false);

      this.SetTitle(this.Title);
      if (!ObjectUtils.NullUndefinedOrEmpty(this.ListTitle))
        this.Component.PanelSelect.Title = this.ListTitle;
    }
  }
}
