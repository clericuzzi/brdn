import { MatDialog } from '@angular/material';
import { Component, OnInit } from '@angular/core';
// utils
import { ServerUrls } from 'clericuzzi-lib/utils/server-urls';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';
// models
import { CompanyItem } from 'app/business/models/company-item.model';
import { CompanyFilter, CompanyColumnsFilter, CompanyColumns, CompanyColumnsUpdate, CompanyColumnsInsert } from 'app/entities/company.model';
import { CrudPopupComponentModel } from 'clericuzzi-lib/modules/shared/crud/crud-popup/crud-popup.model';
// services
import { EntitiesService } from 'clericuzzi-lib/services/entities.service';
import { UserDataService } from 'clericuzzi-lib/services/user-data.service';
import { PageTitleService } from 'clericuzzi-lib/services/page-title.service';
import { AutoCompleteFetchService } from 'clericuzzi-lib/services/auto-complete-fetch.service';
import { ComponentGeneratorService } from 'clericuzzi-lib/services/component-generator.service';
// components
import { CrudPopupComponent } from 'clericuzzi-lib/modules/shared/crud/crud-popup/crud-popup.component';
// behaviours
import { ListPage } from 'clericuzzi-lib/business/behaviours/list-page.behaviour';
import { ItemCompanyComponent } from 'app/modules/site/components/list-items/item-company/item-company.component';
@Component({
  selector: 'app-leads-mine',
  templateUrl: './leads-mine.component.html'
})
export class LeadsMineComponent extends ListPage<CompanyItem, CompanyFilter> implements OnInit
{
  constructor(
    public diag: MatDialog,
    public userData: UserDataService,
    public generator: ComponentGeneratorService,
    public _AutoCompleteFetch: AutoCompleteFetchService,
    private _ModelActions: EntitiesService,

    public pageTitle: PageTitleService,
  )
  {
    super(diag, userData, generator, _AutoCompleteFetch, pageTitle);
    this.SetTitle(`Clientes em prospeção`);
  }

  ngOnInit()
  {
    const baseObject: CompanyItem = new CompanyItem();
    const filterObject: CompanyFilter = new CompanyFilter();
    baseObject.TableDefinition.CanDelete = false;

    this.Initialize(baseObject, filterObject);
    this.Component.Init(baseObject, filterObject, ItemCompanyComponent, false, true);
    this.Component.InitFilters(...CompanyColumnsFilter);
    this.Component.PanelSelect.CustomSelect = ServerUrls.GetUrl(`leads/mine`);

    this.Component.PanelFilter.Hide();

    // example on how to get data to the filters panel
    // this._AutoCompleteFetch.FetchData(ServerUrls.GetUrl(`crud/car_brand/listing`), this.Component.PanelFilter.GetComponent(CarColumns.ModelId), CarModelTable);

    this.ActionNewItem();
    this.ActionUpdateItem();
    this.ListenTo(this._ModelActions.OnRefreshRequested, () => this.Component.Reload());
  }
  public AddPopupModel(): CrudPopupComponentModel
  {
    this.BaseObject.UserId = this.userData.Id;
    this.BaseObject.AccountId = this.userData.CurrentAccountId;
    const crudPopupModel: CrudPopupComponentModel = CrudPopupComponentModel.NewInsert(this.BaseObject, ...CompanyColumnsInsert);
    crudPopupModel.RequestUrl = ServerUrls.GetUrl(`crud/company/insert`);
    crudPopupModel.KeepInserting = false;

    return crudPopupModel;
  }
  public UpdatePopupModel(): CrudPopupComponentModel
  {
    const crudPopupModel: CrudPopupComponentModel = CrudPopupComponentModel.NewUpdate(this.Component.PanelSelect.SelectedItem, ...CompanyColumnsUpdate);

    return crudPopupModel;
  }

  // these methods should be deleted if not used
  public AddItemAfterOpen(component: CrudPopupComponent): void
  {
    // let exampleComponent: AutocompleteInputComponent<number> = component.GetComponent(Columns.BrandId);
    // if (!ObjectUtils.NullOrUndefined(exampleComponent))
    // this._AutoCompleteFetch.FetchData(ServerUrls.GetUrl(`crud//listing`), exampleComponent, Table);
  }
  public AddItemAfterClose(): void
  {
    this.Component.Reload();
  }
  public UpdateItemAfterOpen(component: CrudPopupComponent): void
  {
    // let exampleComponent: AutocompleteInputComponent<number> = component.GetComponent(Columns.BrandId);
    // if (!ObjectUtils.NullOrUndefined(exampleComponent))
    // this._AutoCompleteFetch.FetchData(ServerUrls.GetUrl(`crud//listing`), exampleComponent, Table);
  }
  public UpdateItemAfterClose(): void
  {
    this.Component.Reload();
  }
}
