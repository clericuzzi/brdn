import { ServerUrls } from 'clericuzzi-lib/utils/server-urls';
import { MatSnackBar, MatDialog } from '@angular/material';
import { BiCardComponent } from 'clericuzzi-bi/modules/bi-components/bi-card/bi-card.component';
import { PageTitleService } from 'clericuzzi-lib/services/page-title.service';
import { CampaignScreenModel } from 'app/business/models/campaign-screen.model';
import { HostBindedComponent } from 'clericuzzi-lib/modules/shared/util-components/host-binded/host-binded.component';
import { CampaignActionsService } from 'app/services/campaign-actions.service';
import { AutoCompleteFetchService } from 'clericuzzi-lib/services/auto-complete-fetch.service';
import { BatchResponseMessageList, BatchResponseMessage } from 'clericuzzi-lib/entities/response/batch-response-message.model';
import { ComponentGeneratorService } from 'clericuzzi-lib/services/component-generator.service';
import { AutocompleteInputComponent } from 'clericuzzi-lib/modules/shared/form-controls/autocomplete-input/autocomplete-input.component';
import { Component, OnInit, ViewContainerRef, ViewChild } from '@angular/core';
import { CrudableTableColumnDefinitionModel, ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CompanyListerComponent } from 'app/modules/site/components/company/company-lister/company-lister.component';
import { ResponseMessage } from 'clericuzzi-lib/entities/response/response-message.model';
import { CampaignInfo } from 'app/business/models/campaign-info.model';
import { MaterialPopupComponent } from 'clericuzzi-lib/modules/material/components/material-popup/material-popup.component';
import { StatusResponseMessage } from 'clericuzzi-lib/entities/response/status-response-message.model';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';
import { SnackUtils } from 'clericuzzi-lib/utils/snack-utils';
import { CompanyItem } from 'app/business/models/company-item.model';

@Component({
  selector: 'app-campaign-screen',
  templateUrl: './campaign-screen.component.html'
})
export class CampaignScreenComponent extends HostBindedComponent implements OnInit
{
  @ViewChild('filterCharacteristics', { read: ViewContainerRef }) _ContainerCharacteristics: ViewContainerRef;
  @ViewChild('filterCharacteristicsNot', { read: ViewContainerRef }) _ContainerCharacteristicsNot: ViewContainerRef;

  @ViewChild(`companies`) private _Companies: CompanyListerComponent;

  private _CharacteristicMust: AutocompleteInputComponent<number>;
  private _CharacteristicMustNot: AutocompleteInputComponent<number>;

  public Model: CampaignScreenModel = new CampaignScreenModel();
  constructor(
    private _Diag: MatDialog,
    private _Snack: MatSnackBar,
    private _Actions: CampaignActionsService,
    private _Generator: ComponentGeneratorService,
    private _AutoComplete: AutoCompleteFetchService,
    public pageTitle: PageTitleService,
  )
  {
    super(pageTitle);
    this.InitFactories(this._Generator.Factory, this._Generator.Sanitizer);
    this.SetTitle(`Informações sobre a campanha '${this._Actions.Campaign.Name}'`);
  }

  ngOnInit()
  {
    this._InitAutoCompletes();

    setTimeout(() => this._ConfigCompaniesLister(), 150);
  }
  private _InitAutoCompletes()
  {
    const characteristicMustColumn: CrudableTableColumnDefinitionModel = new CrudableTableColumnDefinitionModel(`characteristicMust`, `CharacteristicsMust`, `Características permitidas`, `Características permitidas`, ComponentTypeEnum.AutoComplete);
    const characteristicMustNotColumn: CrudableTableColumnDefinitionModel = new CrudableTableColumnDefinitionModel(`characteristicMustNot`, `CharacteristicsMustNot`, `Características não permitidas`, `Características não permitidas`, ComponentTypeEnum.AutoComplete);

    this._CharacteristicMust = characteristicMustColumn.CreateComponent(this._ContainerCharacteristics, this._Generator.Factory, this.Model, null, this._Generator.Sanitizer);
    this._CharacteristicMustNot = characteristicMustNotColumn.CreateComponent(this._ContainerCharacteristicsNot, this._Generator.Factory, this.Model, null, this._Generator.Sanitizer);

    this._CharacteristicMust.MultiSelect = this._CharacteristicMustNot.MultiSelect = true;

    this._AutoComplete.FetchAutocompleteData(ServerUrls.GetUrl(`listing/campaign-screen`), this._FillCharacteristicsInfo.bind(this));
  }
  private _FillCharacteristicsInfo(response: BatchResponseMessageList): void
  {
    this._CharacteristicMust.SetItems(this._AutoComplete.GetAutocompleteData(`characteristic`, response));
    this._CharacteristicMustNot.SetItems(this._AutoComplete.GetAutocompleteData(`characteristic`, response));

    this._Actions.LoadConditions(this._ConditionsLoaded.bind(this));
  }

  private _ConfigCompaniesLister(): void
  {
    this._Companies.Component.PanelSelect.Title = `Empresas disponíveis`;
    this._Companies.Component.PanelSelect.CustomSelect = ServerUrls.GetUrl(`campaign/evaluate-by-conditions`);
    this.ListenTo(this._Companies.Component.PanelSelect.OnCustomSelectCallback, response => this._ReplaceTitle(response));
    this._Companies.Component.PanelSelect.BaseObject.TableDefinition.CanRefresh = false;
  }
  private _ReplaceTitle(response: BatchResponseMessage<CompanyItem>): void
  {
    this._Companies.Component.PanelSelect.Title = `Empresas disponíveis ${this._Companies.Component.PanelSelect.Pagination.TotalItems} / ${response.CustomFields[`new-companies`]} novas`;
  }
  private _ConditionsLoaded(response: ResponseMessage<CampaignInfo>): void
  {
    const conditions: CampaignInfo = response.Parse(new CampaignInfo());
    this.Model.CharacteristicsMust = conditions.AllowedCharacteristics;
    this.Model.CharacteristicsMustNot = conditions.NotAllowedCharacteristics;

    this._CharacteristicMust.Sync();
    this._CharacteristicMustNot.Sync();
  }

  public Filter(): void
  {
    this._Companies.Component.PanelSelect.ClearData();
    this._Companies.Component.PanelSelect.Title = `Empresas disponíveis`;
    this._Companies.FilterCampaign(this.Model.CharacteristicsMust, this.Model.CharacteristicsMustNot);
  }
  public UpdateCampaign(): void
  {
    MaterialPopupComponent.Confirmation(this._Diag, this._UpdateCampaign.bind(this), null, `Confirmação`, `Deseja reconfigurar a campanha com a configuração atual?`);
  }
  private _UpdateCampaign(): void
  {
    this._Actions.Update(this.Model.CharacteristicsMust, this.Model.CharacteristicsMustNot, this._UpdateCampaignResult.bind(this));
  }
  private _UpdateCampaignResult(response: StatusResponseMessage): void
  {
    if (!ObjectUtils.NullOrUndefined(response))
    {
      if (response.Success)
        SnackUtils.OpenSuccess(this._Snack, `Operação realizada com sucesso!`);
      else
        SnackUtils.OpenError(this._Snack, response.ErrorMessage);
    }
    else
      SnackUtils.OpenError(this._Snack, `Falha na operação! O servidor não respondeu, tente novamente mais tarde.`);
  }
}
