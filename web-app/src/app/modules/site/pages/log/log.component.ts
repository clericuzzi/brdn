import { Component, OnInit } from '@angular/core';
import { HostBindedComponent } from 'clericuzzi-lib/modules/shared/util-components/host-binded/host-binded.component';
import { PageTitleService } from 'clericuzzi-lib/services/page-title.service';
import { ComponentGeneratorService } from 'clericuzzi-lib/services/component-generator.service';

@Component({
  selector: 'app-log',
  templateUrl: './log.component.html'
})
export class LogComponent extends HostBindedComponent implements OnInit
{
  constructor(
    private _Generator: ComponentGeneratorService,
    public pageTitle: PageTitleService,
  )
  {
    super(pageTitle);
    this.InitFactories(this._Generator.Factory, this._Generator.Sanitizer);
    this.SetTitle(`Log de alterações`);
  }
}
