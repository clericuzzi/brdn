import { MatDialog } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
// utils
import { SitePaths } from 'app/modules/site/site-paths.routes';
import { ServerUrls } from 'clericuzzi-lib/utils/server-urls';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';
// models
import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model';
import { CompanyItem } from 'app/business/models/company-item.model';
import { CustomField } from 'clericuzzi-lib/entities/crud/custom-field.model';
import { ConfigUserItemComponent } from 'module-basic/modules/basic/components/config-user-item/config-user-item.component';
import { User, UserFilter, UserColumnsFilter, UserTable, UserColumnsInsert, UserColumnsUpdate } from 'clericuzzi-lib/entities/models/user.model';
import { Company, CompanyFilter, CompanyColumnsFilter, CompanyColumnsUpdate, CompanyColumnsInsert } from 'app/entities/company.model';
import { UserArea, UserAreaFilter, UserAreaColumnsUpdate, UserAreaColumnsInsert, UserAreaColumnsFilter, UserAreaColumns } from 'app/entities/userArea.model';
import { Account, AccountFilter, AccountColumnsInsert, AccountColumnsFilter, AccountColumnsUpdate, AccountTable } from 'clericuzzi-lib/entities/models/account.model';
import { Tabulation, TabulationFilter, TabulationColumnsUpdate, TabulationColumnsInsert, TabulationColumnsFilter, TabulationTable, TabulationColumns } from 'app/entities/tabulation.model';
import { Activity, ActivityFilter, ActivityColumnsUpdate, ActivityColumnsInsert, ActivityColumnsFilter, ActivityColumns } from 'clericuzzi-lib/entities/models/activity.model';
import { Permission, PermissionFilter, PermissionColumnsUpdate, PermissionColumnsInsert, PermissionColumnsFilter, PermissionColumns } from 'clericuzzi-lib/entities/models/permission.model';
import { BlockingMessage, BlockingMessageFilter, BlockingMessageColumnsInsert, BlockingMessageColumnsFilter, BlockingMessageColumnsUpdate } from 'app/entities/blockingMessage.model';
import { Characteristic, CharacteristicFilter, CharacteristicColumnsUpdate, CharacteristicColumnsInsert, CharacteristicColumnsFilter, CharacteristicColumns } from 'clericuzzi-lib/entities/models/characteristic.model';
// services
import { RoutingService } from 'clericuzzi-lib/services/routing.service';
import { EntitiesService } from 'clericuzzi-lib/services/entities.service';
import { UserDataService } from 'clericuzzi-lib/services/user-data.service';
import { PageTitleService } from 'clericuzzi-lib/services/page-title.service';
import { AutoCompleteFetchService } from 'clericuzzi-lib/services/auto-complete-fetch.service';
import { ComponentGeneratorService } from 'clericuzzi-lib/services/component-generator.service';
// components
import { CrudPopupComponent } from 'clericuzzi-lib/modules/shared/crud/crud-popup/crud-popup.component';
import { ItemCompanyComponent } from 'app/modules/site/components/list-items/item-company/item-company.component';
import { ItemUserAreaComponent } from 'app/modules/site/components/list-items/item-user-area/item-user-area.component';
import { ItemTabulationComponent } from 'app/modules/site/components/list-items/item-tabulation/item-tabulation.component';
import { AutocompleteInputComponent } from 'clericuzzi-lib/modules/shared/form-controls/autocomplete-input/autocomplete-input.component';
import { ConfigActivityItemComponent } from 'module-basic/modules/basic/components/config-activity-item/config-activity-item.component';
import { ItemBlockingMessageComponent } from 'app/modules/site/components/list-items/item-blocking-message/item-blocking-message.component';
import { ConfigPermissionItemComponent } from 'module-basic/modules/basic/components/config-permission-item/config-permission-item.component';
import { ConfigAccountListItemComponent } from 'module-basic/modules/basic/components/config-account-list-item/config-account-list-item.component';
import { ConfigCharacteristicItemComponent } from 'module-basic/modules/basic/components/config-characteristic-item/config-characteristic-item.component';
// behaviours
import { ListPage } from 'clericuzzi-lib/business/behaviours/list-page.behaviour';
import { GeographyStateTable } from 'clericuzzi-lib/entities/models-geo/geographyState.model';
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { GeographyCityTable } from 'clericuzzi-lib/entities/models-geo/geographyCity.model';
import { GeographyNeighbourhoodTable } from 'clericuzzi-lib/entities/models-geo/geographyNeighbourhood.model';
import { UserAreaInsertComponent } from 'app/modules/site/components/user-area/user-area-insert/user-area-insert.component';
import { ItemCampaignComponent } from 'app/modules/site/components/list-items/item-campaign/item-campaign.component';
import { MacroLogin, MacroLoginFilter, MacroLoginColumnsFilter, MacroLoginTable, MacroLoginColumnsInsert, MacroLoginColumnsUpdate } from 'app/entities/macroLogin.model';
import { ItemMacroLoginComponent } from 'app/modules/site/components/list-items/item-macro-login/item-macro-login.component';
import { ContactPosition, ContactPositionFilter, ContactPositionColumnsFilter } from 'app/entities/contactPosition.model';
import { ContactDepartmentFilter, ContactDepartment, ContactDepartmentColumnsFilter } from 'app/entities/contactDepartment.model';
import { ItemContactPositionComponent } from 'app/modules/site/components/list-items/item-contact-position/item-contact-position.component';
import { ItemContactDepartmentComponent } from 'app/modules/site/components/list-items/item-contact-department/item-contact-department.component';
import { BlockedTabulations, BlockedTabulationsFilter, BlockedTabulationsColumnsFilter, BlockedTabulationsColumnsInsert, BlockedTabulationsColumnsUpdate, BlockedTabulationsColumns, BlockedTabulationsTable } from 'app/entities/blockedTabulations.model';
import { MacroMachine, MacroMachineFilter, MacroMachineColumnsFilter, MacroMachineColumnsInsert, MacroMachineColumnsUpdate } from 'app/entities/macroMachine.model';
import { ItemBlockedTabulationComponent } from 'app/modules/site/components/list-items/item-blocked-tabulation/item-blocked-tabulation.component';
import { ItemMacroMachineComponent } from 'app/modules/site/components/list-items/item-macro-machine/item-macro-machine.component';
import { MacroMachineConfigActionsService } from 'app/services/macro-machine-config-actions.service';
import { MacroMachineConfig, MacroMachineConfigFilter, MacroMachineConfigColumnsUpdate, MacroMachineConfigColumnsFilter, MacroMachineConfigColumnsInsert, MacroMachineConfigColumns } from 'app/entities/macroMachineConfig.model';
import { ItemMacroMachineConfigComponent } from 'app/modules/site/components/list-items/item-macro-machine-config/item-macro-machine-config.component';

@Component({
  selector: 'app-crud-default',
  templateUrl: './crud-default.component.html'
})
export class CrudDefaultComponent extends ListPage<Crudable, Crudable> implements OnInit, OnDestroy
{
  private _LoadsOnStart: boolean = true;
  constructor(
    public diag: MatDialog,
    public userData: UserDataService,
    public generator: ComponentGeneratorService,
    public autoCompleteFetch: AutoCompleteFetchService,
    private _Route: ActivatedRoute,
    private _Routing: RoutingService,
    private _ModelActions: EntitiesService,
    private _ConfigActions: MacroMachineConfigActionsService,

    public pageTitle: PageTitleService,
  )
  {
    super(diag, userData, generator, autoCompleteFetch, pageTitle);
    this.InitFactories(this.generator.Factory, this.generator.Sanitizer);
  }

  private _CurrentRoute: string;
  ngOnInit()
  {
    this._CurrentRoute = this._Routing.GetLastSegment(this._Route, 2);
    this._HandleRoute();

    if (!ObjectUtils.NullOrUndefined(this.BaseObject))
    {
      this.BaseObject.TableDefinition.CanDelete = this.CanDelete;
      this.Initialize(this.BaseObject, this.FilterObject);
      this.Component.Init(this.BaseObject, this.FilterObject, this.ItemComponent, false, this._LoadsOnStart);
      this.Component.InitFilters(... this.ColumnsFilter);

      this._FetchExternalData();
      this._CustomFilterActions();

      if (this.CanInsert)
        this.ActionNewItem();
      if (this.CanUpdate)
        this.ActionUpdateItem();

      this.ListenTo(this._ModelActions.OnRefreshRequested, () => this.Component.Reload());
      this.SetTitle(this.Title);
      this._CustomActions();
      if (!ObjectUtils.NullUndefinedOrEmpty(this.ListTitle))
        this.Component.PanelSelect.Title = this.ListTitle;
    }
  }
  ngOnDestroy()
  {
    if (this._CurrentRoute === SitePaths.MACRO_MACHINE_CONFIG)
      this._ConfigActions.ClearCurrentMachine();
  }

  private _FetchExternalData(): void
  {
    if (this._CurrentRoute === SitePaths.CRUD_PERMISSION)
    {
      this.Component.LoadExternalData(ServerUrls.GetUrl(`crud/${AccountTable}/listing`), new CustomField(AccountTable, PermissionColumns.AccountId));
      // this.Component.LoadExternalData(ServerUrls.GetUrl(`autocomplete/clients/list`), new CustomField(CharacteristicTable, `CharacteristicsWith`), new CustomField(CharacteristicTable, `CharacteristicsWithout`))
      // const componentWith: AutocompleteInputComponent<number> = this.Component.PanelFilter.GetComponent(`CharacteristicsWith`);
      // const componentWithout: AutocompleteInputComponent<number> = this.Component.PanelFilter.GetComponent(`CharacteristicsWithout`);
      // componentWith.MultiSelect = componentWithout.MultiSelect = true;
      // componentWith.Placeholder = `Com as características`;
      // componentWithout.Placeholder = `Sem as características`;
    }
    else if (this._CurrentRoute === SitePaths.CRUD_CHARACTERISTIC)
      this.Component.LoadExternalData(ServerUrls.GetUrl(`crud/${AccountTable}/listing`), new CustomField(AccountTable, CharacteristicColumns.AccountId));
    else if (this._CurrentRoute === SitePaths.USER_AREA)
      this.Component.LoadExternalData(ServerUrls.GetUrl(`listing/crud-user-area`), new CustomField(GeographyStateTable, `StateId`), new CustomField(UserTable, UserAreaColumns.UserId));
  }
  private _CustomFilterActions(): void
  {
    if (this._CurrentRoute === SitePaths.USER_AREA)
    {
      const cityComponent: AutocompleteInputComponent<number> = this.Component.PanelFilter.GetComponent(`CityId`);
      const userComponent: AutocompleteInputComponent<number> = this.Component.PanelFilter.GetComponent(`UserId`);
      const stateComponent: AutocompleteInputComponent<number> = this.Component.PanelFilter.GetComponent(`StateId`);
      const neighbourhoodComponent: AutocompleteInputComponent<number> = this.Component.PanelFilter.GetComponent(`NeighbourhoodId`);
      cityComponent.MultiSelect = userComponent.MultiSelect = stateComponent.MultiSelect = neighbourhoodComponent.MultiSelect = true;

      this.ListenTo(cityComponent.OnValueChanged, () => this._UserAreaCitySelected());
      this.ListenTo(stateComponent.OnValueChanged, () => this._UserAreaStateSelected());
    }
  }
  private _CustomActions(): void
  {
  }
  private _HandleRoute(): void
  {
    if (this._CurrentRoute === SitePaths.CRUD_USER)
    {
      this.BaseObject = new User();
      this.FilterObject = new UserFilter();
      this.ItemComponent = ConfigUserItemComponent;
      this.ColumnsFilter = UserColumnsFilter;
      this.ColumnsInsert = UserColumnsInsert;
      this.ColumnsUpdate = UserColumnsUpdate;

      this.Component.PanelSelect.ShowsLoader(true);

      this.Title = `Cadastro de Usuários`;
      this.ListTitle = `Usuários`;

      this.CanDelete = this.CanInsert = this.CanUpdate = true;
    }
    else if (this._CurrentRoute === SitePaths.CRUD_ACCOUNT)
    {
      this.BaseObject = new Account();
      this.FilterObject = new AccountFilter();
      this.ItemComponent = ConfigAccountListItemComponent;
      this.ColumnsFilter = AccountColumnsFilter;
      this.ColumnsInsert = AccountColumnsInsert;
      this.ColumnsUpdate = AccountColumnsUpdate;

      this.Component.PanelSelect.ShowsLoader(true);

      this.Title = `Cadastro de Contas`;
      this.ListTitle = `Contas`;

      this.CanDelete = this.CanInsert = this.CanUpdate = true;
    }
    else if (this._CurrentRoute === SitePaths.CRUD_ACTIVITY)
    {
      this.BaseObject = new Activity();
      this.FilterObject = new ActivityFilter();
      this.ItemComponent = ConfigActivityItemComponent;
      this.ColumnsFilter = ActivityColumnsFilter;
      this.ColumnsInsert = ActivityColumnsInsert;
      this.ColumnsUpdate = ActivityColumnsUpdate;

      this.Component.PanelSelect.ShowsLoader(true);

      this.Title = `Cadastro de Atividades`;
      this.ListTitle = `Atividades`;

      this.CanDelete = this.CanInsert = this.CanUpdate = true;
    }
    else if (this._CurrentRoute === SitePaths.CRUD_PERMISSION)
    {
      this.BaseObject = new Permission();
      this.FilterObject = new PermissionFilter();
      this.ItemComponent = ConfigPermissionItemComponent;
      this.ColumnsFilter = PermissionColumnsFilter;
      this.ColumnsInsert = PermissionColumnsInsert;
      this.ColumnsUpdate = PermissionColumnsUpdate;

      this.Component.PanelSelect.ShowsLoader(true);

      this.Title = `Cadastro de Permissões`;
      this.ListTitle = `Permissões`;

      this.CanDelete = this.CanInsert = this.CanUpdate = true;
    }
    else if (this._CurrentRoute === SitePaths.CRUD_CHARACTERISTIC)
    {
      this.BaseObject = new Characteristic();
      this.FilterObject = new CharacteristicFilter();
      this.ItemComponent = ConfigCharacteristicItemComponent;
      this.ColumnsFilter = CharacteristicColumnsFilter;
      this.ColumnsInsert = CharacteristicColumnsInsert;
      this.ColumnsUpdate = CharacteristicColumnsUpdate;

      this.Component.PanelSelect.ShowsLoader(true);

      this.Title = `Cadastro de Características`;
      this.ListTitle = `Características`;

      this.CanDelete = this.CanInsert = this.CanUpdate = true;
    }

    else if (this._CurrentRoute === SitePaths.COMPANIES)
    {
      this.BaseObject = new CompanyItem();
      this.FilterObject = new CompanyFilter();
      this.ItemComponent = ItemCompanyComponent;
      this.ColumnsFilter = CompanyColumnsFilter;
      this.ColumnsInsert = CompanyColumnsInsert;
      this.ColumnsUpdate = CompanyColumnsUpdate;

      this.Component.PanelSelect.SortFuncion = function (x: CompanyItem, y: CompanyItem): number { return x.FantasyName > y.FantasyName ? 1 : -1; };
      this.Component.PanelSelect.ShowsLoader(true);
      this.Component.PanelSelect.LoadsOnStart = false;

      this.Title = `Cadastro de Empresas`;
      this.ListTitle = `Empresas`;

      this.Component.PanelSelect.CustomSelect = ServerUrls.GetUrl(`companies/listing`);
      this.CanDelete = this.CanInsert = this.CanUpdate = false;
    }
    else if (this._CurrentRoute === SitePaths.TABULATION)
    {
      this.BaseObject = new Tabulation();
      this.FilterObject = new TabulationFilter();
      this.ItemComponent = ItemTabulationComponent;
      this.ColumnsFilter = TabulationColumnsFilter;
      this.ColumnsInsert = TabulationColumnsInsert;
      this.ColumnsUpdate = TabulationColumnsUpdate;

      this.Component.PanelSelect.SortFuncion = function (x: Tabulation, y: Tabulation): number { return x.Text > y.Text ? 1 : -1; };
      this.Component.PanelSelect.ShowsLoader(true);

      this.Title = `Cadastro de Tabulações`;
      this.ListTitle = `Tabulações`;

      this.CanDelete = this.CanInsert = this.CanUpdate = true;
    }
    else if (this._CurrentRoute === SitePaths.BLOCKING_MESSAGES)
    {
      this.BaseObject = new BlockingMessage();
      this.FilterObject = new BlockingMessageFilter();
      this.ItemComponent = ItemBlockingMessageComponent;
      this.ColumnsFilter = BlockingMessageColumnsFilter;
      this.ColumnsInsert = BlockingMessageColumnsInsert;
      this.ColumnsUpdate = BlockingMessageColumnsUpdate;

      this.Component.PanelSelect.SortFuncion = function (x: BlockingMessage, y: BlockingMessage): number { return x.Text > y.Text ? 1 : -1; };
      this.Component.PanelSelect.ShowsLoader(true);

      this.Title = `Cadastro de Mensagens de Bloqueio`;
      this.ListTitle = `Mensagens de Bloqueio`;

      this.CanDelete = this.CanInsert = this.CanUpdate = true;
    }
    else if (this._CurrentRoute === SitePaths.USER_AREA)
    {
      this.BaseObject = new UserArea();
      this.FilterObject = new UserAreaFilter();
      this.ItemComponent = ItemUserAreaComponent;
      this.ColumnsFilter = [UserAreaColumns.UserId, `StateId`, `CityId`, `NeighbourhoodId`];
      this.ColumnsInsert = UserAreaColumnsInsert;
      this.ColumnsUpdate = UserAreaColumnsUpdate;

      this.FilterObject.SetColumnDefinition(`StateId`, `state_id`, `StateId`, `Estado`, `Estados`, ComponentTypeEnum.AutoComplete);
      this.FilterObject.SetColumnDefinition(`CityId`, `city_id`, `CityId`, `Cidade`, `Cidades`, ComponentTypeEnum.AutoComplete);
      this.FilterObject.SetColumnDefinition(`NeighbourhoodId`, `neighbourhood_id`, `NeighbourhoodId`, `Bairro`, `Bairros`, ComponentTypeEnum.AutoComplete);

      this.Component.PanelSelect.ShowsLoader(true);

      this.Title = `Cadastro de Áreas de Atuação`;
      this.ListTitle = `Áreas de Atuação`;

      this.CanDelete = this.CanInsert = this.CanUpdate = true;
      this.InsertItemComponent = UserAreaInsertComponent;
    }
    else if (this._CurrentRoute === SitePaths.CAMPAIGNS)
    {
      this.BaseObject = new Activity(null, this.userData.CurrentAccountId);
      this.FilterObject = new ActivityFilter();
      this.ItemComponent = ItemCampaignComponent;
      this.ColumnsFilter = ActivityColumnsFilter;
      this.ColumnsInsert = ActivityColumnsFilter;
      this.ColumnsUpdate = ActivityColumnsFilter;

      this.Component.PanelSelect.ShowsLoader(false);
      this.Component.PanelSelect.SortFuncion = function (x: Activity, y: Activity): number { return x.Name > y.Name ? 1 : -1; };

      this.Title = `Cadastro de campanhas`;
      this.ListTitle = `Campanhas`;

      this.CanDelete = this.CanInsert = this.CanUpdate = true;
    }
    else if (this._CurrentRoute === SitePaths.CRUD_MACRO_LOGIN)
    {
      this.BaseObject = new MacroLogin();
      this.FilterObject = new MacroLoginFilter();
      this.ItemComponent = ItemMacroLoginComponent;
      this.ColumnsFilter = MacroLoginColumnsFilter;
      this.ColumnsInsert = MacroLoginColumnsInsert;
      this.ColumnsUpdate = MacroLoginColumnsUpdate;

      this.Component.PanelSelect.ShowsLoader(false);
      this.Component.PanelSelect.SortFuncion = function (x: MacroLogin, y: MacroLogin): number { return x.Macro > y.Macro ? 1 : -1; };

      this.Title = `Login das macros`;
      this.ListTitle = `Macros`;

      this.CanDelete = this.CanInsert = this.CanUpdate = true;
    }
    else if (this._CurrentRoute === SitePaths.POSITIONS)
    {
      const position = new ContactPosition();
      position.AccountId = this.userData.CurrentAccountId;

      this.BaseObject = position;
      this.FilterObject = new ContactPositionFilter();
      this.ItemComponent = ItemContactPositionComponent;
      this.ColumnsFilter = ContactPositionColumnsFilter;
      this.ColumnsInsert = ContactPositionColumnsFilter;
      this.ColumnsUpdate = ContactPositionColumnsFilter;

      this.Component.PanelSelect.ShowsLoader(false);
      this.Component.PanelSelect.SortFuncion = function (x: ContactPosition, y: ContactPosition): number { return x.Name > y.Name ? 1 : -1; };

      this.Title = `Cadastro de posições`;
      this.ListTitle = `Posições`;

      this.CanDelete = this.CanInsert = this.CanUpdate = true;
    }
    else if (this._CurrentRoute === SitePaths.DEPARTMENTS)
    {
      const department = new ContactDepartment();
      department.AccountId = this._UserData.CurrentAccountId;

      this.BaseObject = department;
      this.FilterObject = new ContactDepartmentFilter();
      this.ItemComponent = ItemContactDepartmentComponent;
      this.ColumnsFilter = ContactDepartmentColumnsFilter;
      this.ColumnsInsert = ContactDepartmentColumnsFilter;
      this.ColumnsUpdate = ContactDepartmentColumnsFilter;

      this.Component.PanelSelect.ShowsLoader(false);
      this.Component.PanelSelect.SortFuncion = function (x: ContactDepartment, y: ContactDepartment): number { return x.Name > y.Name ? 1 : -1; };

      this.Title = `Cadastro de departamentos`;
      this.ListTitle = `Departamentos`;

      this.CanDelete = this.CanInsert = this.CanUpdate = true;
    }
    else if (this._CurrentRoute === SitePaths.BLOCKED_TABULATIONS)
    {
      this.BaseObject = new BlockedTabulations();
      this.FilterObject = new BlockedTabulationsFilter();
      this.ItemComponent = ItemBlockedTabulationComponent;
      this.ColumnsFilter = BlockedTabulationsColumnsFilter;
      this.ColumnsInsert = BlockedTabulationsColumnsInsert;
      this.ColumnsUpdate = BlockedTabulationsColumnsUpdate;

      this.Component.PanelSelect.ShowsLoader(false);
      this.Component.PanelSelect.SortFuncion = function (x: ContactDepartment, y: ContactDepartment): number { return x.Name > y.Name ? 1 : -1; };

      this.Title = `Tabulações que não voltam para os leads`;
      this.ListTitle = `Tabulações bloqueadas`;

      this.CanDelete = this.CanInsert = this.CanUpdate = true;
    }
    else if (this._CurrentRoute === SitePaths.MACRO_MACHINES)
    {
      this.BaseObject = new MacroMachine();
      this.FilterObject = new MacroMachineFilter();
      this.ItemComponent = ItemMacroMachineComponent;
      this.ColumnsFilter = MacroMachineColumnsFilter;
      this.ColumnsInsert = MacroMachineColumnsInsert;
      this.ColumnsUpdate = MacroMachineColumnsUpdate;

      this.Component.PanelSelect.ShowsLoader(false);
      this.Component.PanelSelect.SortFuncion = function (x: ContactDepartment, y: ContactDepartment): number { return x.Name > y.Name ? 1 : -1; };

      this.Title = `Cadastro de máquinas de mineração`;
      this.ListTitle = `Máquinas de mineração`;

      this.CanInsert = this.CanUpdate = false;
      this.CanDelete = true;
    }
    else if (this._CurrentRoute === SitePaths.MACRO_MACHINE_CONFIG)
    {
      const baseMachine: MacroMachineConfig = new MacroMachineConfig();
      baseMachine.MachineId = this._ConfigActions.MachineId;

      const baseMachineFilter: MacroMachineConfigFilter = new MacroMachineConfigFilter();
      baseMachineFilter.MachineId = [this._ConfigActions.MachineId];

      this.BaseObject = baseMachine;
      this.FilterObject = baseMachineFilter;
      this.ItemComponent = ItemMacroMachineConfigComponent;
      this.ColumnsFilter = MacroMachineConfigColumnsFilter;
      this.ColumnsInsert = MacroMachineConfigColumnsInsert;
      this.ColumnsUpdate = MacroMachineConfigColumnsUpdate;

      this.Component.PanelSelect.ShowsLoader(false);
      this.Component.PanelSelect.SortFuncion = function (x: MacroMachineConfig, y: MacroMachineConfig): number { return x.MacroIdParent.Macro > y.MacroIdParent.Macro ? 1 : -1; };

      this.Title = `Configuração da máquina de mineração '${this._ConfigActions.MachineName}'`;
      this.ListTitle = `Configurações de mineração`;

      this.CanInsert = this.CanUpdate = this.CanDelete = true;
    }
  }

  private _UserAreaCitySelected(): void
  {
    const cityComponent: AutocompleteInputComponent<number> = this.Component.PanelFilter.GetComponent(`CityId`);
    const neighbourhoodComponent: AutocompleteInputComponent<number> = this.Component.PanelFilter.GetComponent(`NeighbourhoodId`);

    if (ObjectUtils.NullOrUndefined(cityComponent.Values) || ObjectUtils.NullUndefinedOrEmptyArray(cityComponent.Values))
      neighbourhoodComponent.ClearItems();
    else
      this.autoCompleteFetch.FetchData(ServerUrls.GetUrl(`geography/listing/neighbourhoods-by-cities`), neighbourhoodComponent, GeographyNeighbourhoodTable, new CustomField(`city-ids`, cityComponent.Values));
  }
  private _UserAreaStateSelected(): void
  {
    const cityComponent: AutocompleteInputComponent<number> = this.Component.PanelFilter.GetComponent(`CityId`);
    const stateComponent: AutocompleteInputComponent<number> = this.Component.PanelFilter.GetComponent(`StateId`);

    if (ObjectUtils.NullOrUndefined(stateComponent.Values) || ObjectUtils.NullUndefinedOrEmptyArray(stateComponent.Values))
      cityComponent.ClearItems();
    else
      this.autoCompleteFetch.FetchData(ServerUrls.GetUrl(`geography/listing/cities-by-states`), cityComponent, GeographyCityTable, new CustomField(`state-ids`, stateComponent.Values));
  }

  // these methods should be deleted if not used
  public AddItemAfterOpen(component: CrudPopupComponent): void
  {
    if (this._CurrentRoute === SitePaths.CRUD_ACTIVITY)
    {
      const accountComponent: AutocompleteInputComponent<number> = component.GetComponent(ActivityColumns.AccountId);
      if (!ObjectUtils.NullOrUndefined(accountComponent))
        component.LoadExternalData(ServerUrls.GetUrl(`crud/${AccountTable}/listing`), new CustomField(AccountTable, ActivityColumns.AccountId));
    }
    else if (this._CurrentRoute === SitePaths.CRUD_CHARACTERISTIC)
    {
      const accountComponent: AutocompleteInputComponent<number> = component.GetComponent(CharacteristicColumns.AccountId);
      if (!ObjectUtils.NullOrUndefined(accountComponent))
        component.LoadExternalData(ServerUrls.GetUrl(`crud/${AccountTable}/listing`), new CustomField(AccountTable, CharacteristicColumns.AccountId));
    }
    else if (this._CurrentRoute === SitePaths.BLOCKED_TABULATIONS)
    {
      const tabulationComponent: AutocompleteInputComponent<number> = component.GetComponent(BlockedTabulationsColumns.TabulationId);
      if (!ObjectUtils.NullOrUndefined(tabulationComponent))
        component.LoadExternalData(ServerUrls.GetUrl(`crud/${TabulationTable}/listing`), new CustomField(TabulationTable, BlockedTabulationsColumns.TabulationId));
    }
    else if (this._CurrentRoute === SitePaths.MACRO_MACHINE_CONFIG)
    {
      const macroComponent: AutocompleteInputComponent<number> = component.GetComponent(MacroMachineConfigColumns.MacroId);
      if (!ObjectUtils.NullOrUndefined(macroComponent))
        component.LoadExternalData(ServerUrls.GetUrl(`crud/${MacroLoginTable}/listing`), new CustomField(MacroLoginTable, MacroMachineConfigColumns.MacroId));
    }
  }
  public AddItemAfterClose(): void
  {
    this.Component.Reload();
  }
  public UpdateItemAfterOpen(component: CrudPopupComponent): void
  {
    if (this._CurrentRoute === SitePaths.CRUD_ACCOUNT)
    {
      const accountComponent: AutocompleteInputComponent<number> = component.GetComponent(ActivityColumns.AccountId);
      if (!ObjectUtils.NullOrUndefined(accountComponent))
        component.LoadExternalData(ServerUrls.GetUrl(`crud/${AccountTable}/listing`), new CustomField(AccountTable, ActivityColumns.AccountId));
    }
    else if (this._CurrentRoute === SitePaths.CRUD_ACCOUNT)
    {
      const accountComponent: AutocompleteInputComponent<number> = component.GetComponent(CharacteristicColumns.AccountId);
      if (!ObjectUtils.NullOrUndefined(accountComponent))
        component.LoadExternalData(ServerUrls.GetUrl(`crud/${AccountTable}/listing`), new CustomField(AccountTable, CharacteristicColumns.AccountId));
    }
    else if (this._CurrentRoute === SitePaths.BLOCKED_TABULATIONS)
    {
      const tabulationComponent: AutocompleteInputComponent<number> = component.GetComponent(BlockedTabulationsColumns.TabulationId);
      if (!ObjectUtils.NullOrUndefined(tabulationComponent))
        component.LoadExternalData(ServerUrls.GetUrl(`crud/${TabulationTable}/listing`), new CustomField(TabulationTable, TabulationColumns.Text));
    }
    else if (this._CurrentRoute === SitePaths.MACRO_MACHINE_CONFIG)
    {
      const macroComponent: AutocompleteInputComponent<number> = component.GetComponent(MacroMachineConfigColumns.MacroId);
      if (!ObjectUtils.NullOrUndefined(macroComponent))
        component.LoadExternalData(ServerUrls.GetUrl(`crud/${MacroLoginTable}/listing`), new CustomField(MacroLoginTable, MacroMachineConfigColumns.MacroId));
    }
  }
  public UpdateItemAfterClose(): void
  {
    this.Component.Reload();
  }
}
