import { Component, OnInit, ViewChild, ViewContainerRef, ChangeDetectorRef } from '@angular/core';
// utils
import { SitePaths } from '../../site-paths.routes';
// models
// services
import { UserDataService } from 'clericuzzi-lib/services/user-data.service';
import { PageTitleService } from 'clericuzzi-lib/services/page-title.service';
import { ComponentGeneratorService } from 'clericuzzi-lib/services/component-generator.service';
// components
import { HostBindedComponent } from 'clericuzzi-lib/modules/shared/util-components/host-binded/host-binded.component';
import { CrudListItemComponent } from 'module-basic/modules/basic/components/crud-list-item/crud-list-item.component';
import { CompanyActionsService } from 'app/services/company-actions.service';
// behaviours

@Component({
  selector: 'app-crud-list',
  templateUrl: './crud-list.component.html'
})
export class CrudListComponent extends HostBindedComponent implements OnInit
{
  public CompaniesTitle: string;

  @ViewChild(`containerCrud`, { read: ViewContainerRef }) private _ContainerCrud: ViewContainerRef;
  @ViewChild(`containerReports`, { read: ViewContainerRef }) private _ContainerReports: ViewContainerRef;
  @ViewChild(`containerCompanies`, { read: ViewContainerRef }) private _ContainerCompanies: ViewContainerRef;
  @ViewChild(`containerActivities`, { read: ViewContainerRef }) private _ContainerActivities: ViewContainerRef;
  @ViewChild(`containerUserConfig`, { read: ViewContainerRef }) private _ContainerUserConfig: ViewContainerRef;
  constructor(
    public _UserData: UserDataService,
    private _Generator: ComponentGeneratorService,
    private _ChangeDetector: ChangeDetectorRef,
    public pageTitle: PageTitleService,

    private _CompanyActions: CompanyActionsService,
  )
  {
    super(pageTitle);
    this.InitFactories(this._Generator.Factory, this._Generator.Sanitizer);
    this.ClearTitle();
  }

  ngOnInit()
  {
    this._ChangeDetector.detectChanges();
    if (this._UserData.IsManagementLevel)
      this._InitManagementLevel();
    else
      this._InitUserLevel();
  }
  private _InitUserLevel(): void
  {
    this.CompaniesTitle = `Leads`;

    this._Generator.CreateComponent<CrudListItemComponent>(CrudListItemComponent, this._ContainerCompanies).SetModel(`Agendamentos`, SitePaths.LEAD_CALLBACK, SitePaths.INDEX);
    this._Generator.CreateComponent<CrudListItemComponent>(CrudListItemComponent, this._ContainerCompanies).SetModel(`Novo Lead Móvel`, SitePaths.LEAD_NEW, SitePaths.INDEX, () => this._CompanyActions.CampaignId = 3);
    this._Generator.CreateComponent<CrudListItemComponent>(CrudListItemComponent, this._ContainerCompanies).SetModel(`Novo Lead Soho`, SitePaths.LEAD_NEW, SitePaths.INDEX, () => this._CompanyActions.CampaignId = 1);
    this._Generator.CreateComponent<CrudListItemComponent>(CrudListItemComponent, this._ContainerCompanies).SetModel(`Novo Lead Soho Alta Velocidade`, SitePaths.LEAD_NEW, SitePaths.INDEX, () => this._CompanyActions.CampaignId = 4);
    this._Generator.CreateComponent<CrudListItemComponent>(CrudListItemComponent, this._ContainerCompanies).SetModel(`Novo Lead SFA`, SitePaths.LEAD_NEW, SitePaths.INDEX, () => this._CompanyActions.CampaignId = 5);
    this._Generator.CreateComponent<CrudListItemComponent>(CrudListItemComponent, this._ContainerCompanies).SetModel(`Meus leads`, SitePaths.LEAD_MINE, SitePaths.INDEX);
  }
  private _InitManagementLevel()
  {
    this.CompaniesTitle = `Empresas`;

    this._Generator.CreateComponent<CrudListItemComponent>(CrudListItemComponent, this._ContainerActivities).SetModel(`Atividades`, SitePaths.CRUD_ACTIVITY, SitePaths.INDEX);
    this._Generator.CreateComponent<CrudListItemComponent>(CrudListItemComponent, this._ContainerActivities).SetModel(`Características`, SitePaths.CRUD_CHARACTERISTIC, SitePaths.INDEX);
    this._Generator.CreateComponent<CrudListItemComponent>(CrudListItemComponent, this._ContainerActivities).SetModel(`Gerenciar`, SitePaths.CRUD_USER, SitePaths.INDEX);
    this._Generator.CreateComponent<CrudListItemComponent>(CrudListItemComponent, this._ContainerActivities).SetModel(`Minhas tarefas`, SitePaths.CRUD_USER, SitePaths.INDEX);

    this._Generator.CreateComponent<CrudListItemComponent>(CrudListItemComponent, this._ContainerCrud).SetModel(`Áreas de Atuação`, SitePaths.USER_AREA, SitePaths.INDEX);
    this._Generator.CreateComponent<CrudListItemComponent>(CrudListItemComponent, this._ContainerCrud).SetModel(`Departamentos`, SitePaths.DEPARTMENTS, SitePaths.INDEX);
    this._Generator.CreateComponent<CrudListItemComponent>(CrudListItemComponent, this._ContainerCrud).SetModel(`Mensagens de Bloqueio`, SitePaths.BLOCKING_MESSAGES, SitePaths.INDEX);
    this._Generator.CreateComponent<CrudListItemComponent>(CrudListItemComponent, this._ContainerCrud).SetModel(`Posições`, SitePaths.POSITIONS, SitePaths.INDEX);
    this._Generator.CreateComponent<CrudListItemComponent>(CrudListItemComponent, this._ContainerCrud).SetModel(`Tabulações`, SitePaths.TABULATION, SitePaths.INDEX);

    this._Generator.CreateComponent<CrudListItemComponent>(CrudListItemComponent, this._ContainerCompanies).SetModel(`Vitrine`, SitePaths.COMPANIES, SitePaths.INDEX);
    this._Generator.CreateComponent<CrudListItemComponent>(CrudListItemComponent, this._ContainerCompanies).SetModel(`Minhas empresas`, SitePaths.COMPANIES, SitePaths.INDEX);

    this._Generator.CreateComponent<CrudListItemComponent>(CrudListItemComponent, this._ContainerUserConfig).SetModel(`Contas`, SitePaths.CRUD_ACCOUNT, SitePaths.INDEX);
    this._Generator.CreateComponent<CrudListItemComponent>(CrudListItemComponent, this._ContainerUserConfig).SetModel(`Permissões`, SitePaths.CRUD_PERMISSION, SitePaths.INDEX);
    this._Generator.CreateComponent<CrudListItemComponent>(CrudListItemComponent, this._ContainerUserConfig).SetModel(`Usuários`, SitePaths.CRUD_USER, SitePaths.INDEX);

    this._Generator.CreateComponent<CrudListItemComponent>(CrudListItemComponent, this._ContainerReports).SetModel(`Minhas vendas`, SitePaths.CRUD_USER, SitePaths.INDEX);
  }
}
