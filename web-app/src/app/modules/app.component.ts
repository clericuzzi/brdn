import { DomSanitizer } from '@angular/platform-browser';
import { Component, OnInit, ComponentFactoryResolver } from '@angular/core';

// paths

// utils
import { ServerUrls } from 'clericuzzi-lib/utils/server-urls';
import { ModuleBasicDefinitions } from 'module-basic/business/constants/module-basic.definitions';

// models
import { CrudableTableColumnDefinitionModel } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';

// controllers

// project models

// project services

// base services
import { UserDataService } from 'clericuzzi-lib/services/user-data.service';
import { ProjectRoutingService } from 'app/services/project-routing.service';
import { ComponentGeneratorService } from 'clericuzzi-lib/services/component-generator.service';

// base components
import { SubjectListenerComponent } from 'clericuzzi-lib/modules/shared/subject-listener/subject-listener.component';
import { RequestPaginationDefaults } from 'clericuzzi-lib/entities/request/pagination/request-pagination.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})

export class AppComponent extends SubjectListenerComponent implements OnInit
{
    private static _IsInitialized: boolean = false;
    constructor(
      private _Routes: ProjectRoutingService,
      private _UserData: UserDataService,
      private _Generator: ComponentGeneratorService,

      public factory: ComponentFactoryResolver,
      public sanitizer: DomSanitizer,
    )
    {
      super();
    }

    ngOnInit()
    {
      this._Generator.Initialize(this.factory, this.sanitizer);
      if (!AppComponent._IsInitialized)
        this._Initialize();
    }
    private _Initialize(): void
    {
      AppComponent._IsInitialized = true;

      const serverRoot: string = `http://54.70.216.46:5018/api/`;
      if (window.location.href.indexOf(`localhost`) < 0)
        ServerUrls.SERVER_ADDRESS = serverRoot;
      else
      {
        ServerUrls.SERVER_ADDRESS = serverRoot;
        ServerUrls.SERVER_ADDRESS = ServerUrls.LOCAL_ROOT;
      }

      RequestPaginationDefaults.PageSize = 100;
      CrudableTableColumnDefinitionModel.Init();
      ModuleBasicDefinitions.Init(ServerUrls.SERVER_ROOT);

      this._UserData.Init();
      if (this._UserData.IsLogged)
        this._Routes.Site();
      else
        this._Routes.Login();
    }
}
