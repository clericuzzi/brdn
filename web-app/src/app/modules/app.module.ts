import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';

import { AppComponent } from './app.component';

// modules
import { SiteModule } from './site/site.module';

// clericuzzi-angular
import { SharedModule } from 'clericuzzi-lib/modules/shared/shared.module';

// clericuzzi layout-basic
import { ClericuzziModuleBasic } from 'module-basic/modules/basic/clericuzzi-module-basic.module';
import { ClericuzziModuleBasicLogin } from 'module-basic/modules/login/clericuzzi-module-basic-login.module';

// clericuzzi bi
import { BiComponentsModule } from 'clericuzzi-bi/modules/bi-components/bi-components.module';

// project services
import { ReportService } from 'app/services/reports.service';
import { ListingService } from 'app/services/listing.service';
import { CrudActionsService } from 'app/services/crud-actions.service';
import { CompanyActionsService } from 'app/services/company-actions.service';
import { CampaignActionsService } from 'app/services/campaign-actions.service';

// project default services
import { RoleGuardService } from 'app/services/auth/role-guard.service';
import { ProjectRoutingService } from 'app/services/project-routing.service';

// api services
import { AuthService } from 'clericuzzi-lib/services/auth.service';
import { HttpService } from 'clericuzzi-lib/services/http.service';
import { LoginService } from 'clericuzzi-lib/services/login.service';
import { FromToService } from 'clericuzzi-lib/services/from-to.service';
import { LoaderService } from 'clericuzzi-lib/services/loader.service';
import { RoutingService } from 'clericuzzi-lib/services/routing.service';
import { AccountsService } from 'clericuzzi-lib/services/accounts.service';
import { EntitiesService } from 'clericuzzi-lib/services/entities.service';
import { UserDataService } from 'clericuzzi-lib/services/user-data.service';
import { PageTitleService } from 'clericuzzi-lib/services/page-title.service';
import { UserConfigService } from 'module-basic/services/user-config.service';
import { UserSessionService } from 'clericuzzi-lib/services/user-session.service';
import { MaterialPopupService } from 'clericuzzi-lib/services/material-popup.service';
import { AutoCompleteFetchService } from 'clericuzzi-lib/services/auto-complete-fetch.service';
import { ComponentGeneratorService } from 'clericuzzi-lib/services/component-generator.service';

// 3rd party services
import { CookieService } from 'ng2-cookies';
import { MiningService } from 'app/services/mining.service';

export const routes =
[
  { path: '', component: AppComponent },
];

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    RouterModule.forRoot(routes),

    SiteModule,

    // clericuzzi-angular modules
    SharedModule,

    // clericuzzi-layout-basic modules
    ClericuzziModuleBasic,
    ClericuzziModuleBasicLogin,

    // clericuzzi-bi modules
    BiComponentsModule,
  ],
  exports: [
  ],
  providers: [
    // project services
    CrudActionsService,

    // project default services
    MiningService,
    ReportService,
    ListingService,
    CompanyActionsService,
    ProjectRoutingService,
    CampaignActionsService,

    // project guards
    RoleGuardService,

    // clericuzzi-angular services
    AuthService,
    HttpService,
    LoginService,
    FromToService,
    LoaderService,
    RoutingService,
    AccountsService,
    EntitiesService,
    UserDataService,
    PageTitleService,
    UserConfigService,
    UserSessionService,
    MaterialPopupService,
    AutoCompleteFetchService,
    ComponentGeneratorService,

    // routes
    { provide: LocationStrategy, useClass: HashLocationStrategy },

    // 3rd party services
    CookieService,
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
