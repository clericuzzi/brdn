﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model';

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// Navigation Properties
import { Company } from './company.model';
import { User } from 'clericuzzi-lib/entities/models/user.model';
import { Product } from './product.model';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const SaleClass: string = `Sale`;
export const SaleTable: string = `sale`;

/**
 * Column definitions for the `Sale` class
 */
export enum SaleColumns
{
  Id = 'Id',
  UserId = 'UserId',
  CompanyId = 'CompanyId',
  ProductId = 'ProductId',
  Timestamp = 'Timestamp',
  ApprovedBy = 'ApprovedBy',
  ApprovedIn = 'ApprovedIn',

  UserIdParent = 'UserIdParent',
  CompanyIdParent = 'CompanyIdParent',
  ProductIdParent = 'ProductIdParent',
  ApprovedByParent = 'ApprovedByParent',
}
export const SaleColumnsFilter: string[] = [];
export const SaleColumnsInsert: string[] = [SaleColumns.CompanyId, SaleColumns.UserId, SaleColumns.ProductId, SaleColumns.Timestamp, SaleColumns.ApprovedBy, SaleColumns.ApprovedIn];
export const SaleColumnsUpdate: string[] = [SaleColumns.CompanyId, SaleColumns.UserId, SaleColumns.ProductId, SaleColumns.Timestamp, SaleColumns.ApprovedBy, SaleColumns.ApprovedIn];

/**
 * Implementations of the `Sale` class
 */
export class Sale extends Crudable
{
  public CompanyIdParent: Company;
  public UserIdParent: User;
  public ProductIdParent: Product;
  public ApprovedByParent: User;

  public static FromJson(json: any): Sale
  {
    const item: Sale = new Sale();
    if (json)
      item.ParseFromJson(json);

    return item;
  }

  constructor(
    public Id: number = null,
    public CompanyId: number = null,
    public UserId: number = null,
    public ProductId: number = null,
    public Timestamp: Date = null,
    public ApprovedBy: number = null,
    public ApprovedIn: Date = null,
  )
  {
    super();
    this._InitDefinition();
  }
  private _InitDefinition(): void
  {
    this.SetTableDefinition(`Sale`, `sale`, CrudableTableDefinitionGender.Male, `Sale`, `Sales`, true, true, true, true, true, true);

    this.SetColumnDefinition(SaleColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(SaleColumns.CompanyId, `company_id`, `CompanyId`, `Company Id`, `Company Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(SaleColumns.UserId, `user_id`, `UserId`, `User Id`, `User Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(SaleColumns.ProductId, `product_id`, `ProductId`, `Product Id`, `Product Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(SaleColumns.Timestamp, `timestamp`, `Timestamp`, `Timestamp`, `Timestamps`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
    this.SetColumnDefinition(SaleColumns.ApprovedBy, `approved_by`, `ApprovedBy`, `Approved By`, `Approved Bys`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(SaleColumns.ApprovedIn, `approved_in`, `ApprovedIn`, `Approved In`, `Approved Ins`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, true);
  }

  public Clone(): Sale
  {
    const clone: Sale = new Sale();
    clone.Id = this.Id;
    clone.UserId = this.UserId;
    clone.CompanyId = this.CompanyId;
    clone.ProductId = this.ProductId;
    clone.Timestamp = this.Timestamp;
    clone.ApprovedBy = this.ApprovedBy;
    clone.ApprovedIn = this.ApprovedIn;

    clone.UserIdParent = this.UserIdParent;
    clone.CompanyIdParent = this.CompanyIdParent;
    clone.ProductIdParent = this.ProductIdParent;
    clone.ApprovedByParent = this.ApprovedByParent;

    clone.TableDefinition = this.TableDefinition;
    clone.ColumnDefinitions = this.ColumnDefinitions;

    clone.CustomProperties = this.CustomProperties;

    return clone;
  }
  public ToString(): string
  {
    return `${this.CompanyIdParent == null ? '' : this.CompanyIdParent.ToString()}`;
  }

  public ToJson(navigationProperties: boolean = false): any
  {
    const json: any = {};
    if (!ObjectUtils.NullOrUndefined(this.Id))
      json['Id'] = this.Id;
    if (!ObjectUtils.NullOrUndefined(this.UserId))
      json['UserId'] = this.UserId;
    if (!ObjectUtils.NullOrUndefined(this.CompanyId))
      json['CompanyId'] = this.CompanyId;
    if (!ObjectUtils.NullOrUndefined(this.ProductId))
      json['ProductId'] = this.ProductId;
    if (!ObjectUtils.NullOrUndefined(this.ApprovedBy))
      json['ApprovedBy'] = this.ApprovedBy;
    if (!ObjectUtils.NullOrUndefined(this.Timestamp))
      json['Timestamp'] = DateUtils.ToDateServer(this.Timestamp);
    if (!ObjectUtils.NullOrUndefined(this.ApprovedIn))
      json['ApprovedIn'] = DateUtils.ToDateServer(this.ApprovedIn);
    if (navigationProperties)
    {
      if (!ObjectUtils.NullOrUndefined(this.CompanyIdParent))
        json['CompanyIdParent'] = this.CompanyIdParent.ToJson();
      if (!ObjectUtils.NullOrUndefined(this.UserIdParent))
        json['UserIdParent'] = this.UserIdParent.ToJson();
      if (!ObjectUtils.NullOrUndefined(this.ProductIdParent))
        json['ProductIdParent'] = this.ProductIdParent.ToJson();
      if (!ObjectUtils.NullOrUndefined(this.ApprovedByParent))
        json['ApprovedByParent'] = this.ApprovedByParent.ToJson();
    }

    return json;
  }
  public ParseToJson(): string
  {
    const json: any = this.ToJson();

    return JSON.stringify(json);
  }
  public ParseFromJson(json: any): void
  {
    if (json)
    {
      this.Id = json['Id'];
      this.UserId = json['UserId'];
      this.CompanyId = json['CompanyId'];
      this.ProductId = json['ProductId'];
      this.ApprovedBy = json['ApprovedBy'];
      this.Timestamp = DateUtils.FromDateServer(json['Timestamp']);
      this.ApprovedIn = DateUtils.FromDateServer(json['ApprovedIn']);

      if (!ObjectUtils.NullOrUndefined(json['CompanyIdParent']))
      {
        this.CompanyIdParent = new Company();
        this.CompanyIdParent.ParseFromJson(json['CompanyIdParent']);
      }
      if (!ObjectUtils.NullOrUndefined(json['UserIdParent']))
      {
        this.UserIdParent = new User();
        this.UserIdParent.ParseFromJson(json['UserIdParent']);
      }
      if (!ObjectUtils.NullOrUndefined(json['ProductIdParent']))
      {
        this.ProductIdParent = new Product();
        this.ProductIdParent.ParseFromJson(json['ProductIdParent']);
      }
      if (!ObjectUtils.NullOrUndefined(json['ApprovedByParent']))
      {
        this.ApprovedByParent = new User();
        this.ApprovedByParent.ParseFromJson(json['ApprovedByParent']);
      }
    }
  }
}

/**
 * Implementations of the `SaleFilter` class
 */
export class SaleFilter extends Crudable
{
  public static FromJson(json: Object): SaleFilter
  {
    const item: SaleFilter = new SaleFilter();
    if (json)
      item.ParseFromJson(json);

    return item;
  }

  constructor(
    public Id: number = null,
    public CompanyId: number[] = null,
    public UserId: number[] = null,
    public ProductId: number[] = null,
    public Timestamp: Date = null,
    public ApprovedBy: number[] = null,
    public ApprovedIn: Date = null,
  )
  {
    super();
    this._InitDefinition();
  }
  private _InitDefinition(): void
  {
    this.SetTableDefinition(`SaleFilter`, `saleFilter`, CrudableTableDefinitionGender.Male, `Sale`, `Sales`, true, true, true, true, true, true);

    this.SetColumnDefinition(SaleColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(SaleColumns.CompanyId, `company_id`, `CompanyId`, `Company Id`, `Company Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(SaleColumns.UserId, `user_id`, `UserId`, `User Id`, `User Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(SaleColumns.ProductId, `product_id`, `ProductId`, `Product Id`, `Product Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(SaleColumns.Timestamp, `timestamp`, `Timestamp`, `Timestamp`, `Timestamps`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
    this.SetColumnDefinition(SaleColumns.ApprovedBy, `approved_by`, `ApprovedBy`, `Approved By`, `Approved Bys`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(SaleColumns.ApprovedIn, `approved_in`, `ApprovedIn`, `Approved In`, `Approved Ins`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, true);
  }

  public Clone(): SaleFilter
  {
    const clone: SaleFilter = new SaleFilter();
    clone.Id = this.Id;
    clone.UserId = this.UserId;
    clone.CompanyId = this.CompanyId;
    clone.ProductId = this.ProductId;
    clone.Timestamp = this.Timestamp;
    clone.ApprovedBy = this.ApprovedBy;
    clone.ApprovedIn = this.ApprovedIn;

    clone.TableDefinition = this.TableDefinition;
    clone.ColumnDefinitions = this.ColumnDefinitions;

    clone.CustomProperties = this.CustomProperties;

    return clone;
  }
  public ToString(): string
  {
    return `null`;
  }

  public ToJson(): any
  {
    const json: any = {};
    if (!ObjectUtils.NullOrUndefined(this.Id))
      json['Id'] = this.Id;
    if (!ObjectUtils.NullOrUndefined(this.Timestamp))
      json['Timestamp'] = DateUtils.ToDateServer(this.Timestamp);
    if (!ObjectUtils.NullOrUndefined(this.ApprovedIn))
      json['ApprovedIn'] = DateUtils.ToDateServer(this.ApprovedIn);
    if (!ObjectUtils.NullOrUndefined(this.UserId))
      json['UserId'] = Array.isArray(this.UserId) ? this.UserId : [this.UserId];
    if (!ObjectUtils.NullOrUndefined(this.CompanyId))
      json['CompanyId'] = Array.isArray(this.CompanyId) ? this.CompanyId : [this.CompanyId];
    if (!ObjectUtils.NullOrUndefined(this.ProductId))
      json['ProductId'] = Array.isArray(this.ProductId) ? this.ProductId : [this.ProductId];
    if (!ObjectUtils.NullOrUndefined(this.ApprovedBy))
      json['ApprovedBy'] = Array.isArray(this.ApprovedBy) ? this.ApprovedBy : [this.ApprovedBy];


    return json;
  }
  public ParseToJson(): string
  {
    const json: any = this.ToJson();

    return JSON.stringify(json);
  }
  public ParseFromJson(json: Object): void
  {
    if (json)
    {
      this.Id = json['Id'];
      this.UserId = json['UserId'];
      this.CompanyId = json['CompanyId'];
      this.ProductId = json['ProductId'];
      this.ApprovedBy = json['ApprovedBy'];
      this.Timestamp = DateUtils.FromDateServer(json['Timestamp']);
      this.ApprovedIn = DateUtils.FromDateServer(json['ApprovedIn']);
    }
  }
}
