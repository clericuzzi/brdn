﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model';

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const PhoneTypeClass: string = `PhoneType`;
export const PhoneTypeTable: string = `phone_type`;

/**
 * Column definitions for the `PhoneType` class
 */
export enum PhoneTypeColumns
{
  Id = 'Id',
  Name = 'Name',
}
export const PhoneTypeColumnsFilter: string[] = [];
export const PhoneTypeColumnsInsert: string[] = [PhoneTypeColumns.Name];
export const PhoneTypeColumnsUpdate: string[] = [PhoneTypeColumns.Name];

/**
 * Implementations of the `PhoneType` class
 */
export class PhoneType extends Crudable
{
  public static FromJson(json: any): PhoneType
  {
    const item: PhoneType = new PhoneType();
    if (json)
      item.ParseFromJson(json);

    return item;
  }

  constructor(
    public Id: number = null,
    public Name: string = null,
  )
  {
    super();
    this._InitDefinition();
  }
  private _InitDefinition(): void
  {
    this.SetTableDefinition(`PhoneType`, `phone_type`, CrudableTableDefinitionGender.Male, `Phone Type`, `Phone Types`, true, true, true, true, true, true);

    this.SetColumnDefinition(PhoneTypeColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(PhoneTypeColumns.Name, `name`, `Name`, `Name`, `Names`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
  }

  public Clone(): PhoneType
  {
    const clone: PhoneType = new PhoneType();
    clone.Id = this.Id;
    clone.Name = this.Name;

    clone.TableDefinition = this.TableDefinition;
    clone.ColumnDefinitions = this.ColumnDefinitions;

    clone.CustomProperties = this.CustomProperties;

    return clone;
  }
  public ToString(): string
  {
    return `${this.Name}`;
  }

  public ToJson(navigationProperties: boolean = false): any
  {
    const json: any = {};
    if (!ObjectUtils.NullOrUndefined(this.Id))
      json['Id'] = this.Id;
    if (!ObjectUtils.NullOrUndefined(this.Name))
      json['Name'] = this.Name;
    if (navigationProperties)
    {
    }

    return json;
  }
  public ParseToJson(): string
  {
    const json: any = this.ToJson();

    return JSON.stringify(json);
  }
  public ParseFromJson(json: any): void
  {
    if (json)
    {
      this.Id = json['Id'];
      this.Name = json['Name'];
    }
  }
}

/**
 * Implementations of the `PhoneTypeFilter` class
 */
export class PhoneTypeFilter extends Crudable
{
  public static FromJson(json: Object): PhoneTypeFilter
  {
    const item: PhoneTypeFilter = new PhoneTypeFilter();
    if (json)
      item.ParseFromJson(json);

    return item;
  }

  constructor(
    public Id: number = null,
    public Name: string = null,
  )
  {
    super();
    this._InitDefinition();
  }
  private _InitDefinition(): void
  {
    this.SetTableDefinition(`PhoneTypeFilter`, `phone_typeFilter`, CrudableTableDefinitionGender.Male, `Phone Type`, `Phone Types`, true, true, true, true, true, true);

    this.SetColumnDefinition(PhoneTypeColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(PhoneTypeColumns.Name, `name`, `Name`, `Name`, `Names`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
  }

  public Clone(): PhoneTypeFilter
  {
    const clone: PhoneTypeFilter = new PhoneTypeFilter();
    clone.Id = this.Id;
    clone.Name = this.Name;

    clone.TableDefinition = this.TableDefinition;
    clone.ColumnDefinitions = this.ColumnDefinitions;

    clone.CustomProperties = this.CustomProperties;

    return clone;
  }
  public ToString(): string
  {
    return `null`;
  }

  public ToJson(): any
  {
    const json: any = {};
    if (!ObjectUtils.NullOrUndefined(this.Id))
      json['Id'] = this.Id;
    if (!ObjectUtils.NullOrUndefined(this.Name))
      json['Name'] = this.Name;


    return json;
  }
  public ParseToJson(): string
  {
    const json: any = this.ToJson();

    return JSON.stringify(json);
  }
  public ParseFromJson(json: Object): void
  {
    if (json)
    {
      this.Id = json['Id'];
      this.Name = json['Name'];
    }
  }
}
