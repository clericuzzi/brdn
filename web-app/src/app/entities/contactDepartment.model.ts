﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model';

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// Navigation Properties
import { Account } from 'clericuzzi-lib/entities/models/account.model';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const ContactDepartmentClass: string = `ContactDepartment`;
export const ContactDepartmentTable: string = `contact_department`;

/**
 * Column definitions for the `ContactDepartment` class
 */
export enum ContactDepartmentColumns
{
  Id = 'Id',
  Name = 'Name',
  AccountId = 'AccountId',

  AccountIdParent = 'AccountIdParent',
}
export const ContactDepartmentColumnsFilter: string[] = [ContactDepartmentColumns.Name];
export const ContactDepartmentColumnsInsert: string[] = [ContactDepartmentColumns.Name];
export const ContactDepartmentColumnsUpdate: string[] = [ContactDepartmentColumns.Name];

/**
 * Implementations of the `ContactDepartment` class
 */
export class ContactDepartment extends Crudable
{
  public AccountIdParent: Account;

  public static FromJson(json: any): ContactDepartment
  {
    const item: ContactDepartment = new ContactDepartment();
    if (json)
      item.ParseFromJson(json);

    return item;
  }

  constructor(
    public Id: number = null,
    public AccountId: number = null,
    public Name: string = null,
  )
  {
    super();
    this._InitDefinition();
  }
  private _InitDefinition(): void
  {
    this.SetTableDefinition(`ContactDepartment`, `contact_department`, CrudableTableDefinitionGender.Male, `Departamento`, `Departamentos`, true, true, true, true, true, true);

    this.SetColumnDefinition(ContactDepartmentColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(ContactDepartmentColumns.AccountId, `account_id`, `AccountId`, `Conta`, `Contas`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(ContactDepartmentColumns.Name, `name`, `Name`, `Nome`, `Nomes`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
  }

  public Clone(): ContactDepartment
  {
    const clone: ContactDepartment = new ContactDepartment();
    clone.Id = this.Id;
    clone.Name = this.Name;
    clone.AccountId = this.AccountId;

    clone.AccountIdParent = this.AccountIdParent;

    clone.TableDefinition = this.TableDefinition;
    clone.ColumnDefinitions = this.ColumnDefinitions;

    clone.CustomProperties = this.CustomProperties;

    return clone;
  }
  public ToString(): string
  {
    return `${this.AccountIdParent == null ? '' : this.AccountIdParent.ToString()}`;
  }

  public ToJson(navigationProperties: boolean = false): any
  {
    const json: any = {};
    if (!ObjectUtils.NullOrUndefined(this.Id))
      json['Id'] = this.Id;
    if (!ObjectUtils.NullOrUndefined(this.Name))
      json['Name'] = this.Name;
    if (!ObjectUtils.NullOrUndefined(this.AccountId))
      json['AccountId'] = this.AccountId;
    if (navigationProperties)
    {
      if (!ObjectUtils.NullOrUndefined(this.AccountIdParent))
        json['AccountIdParent'] = this.AccountIdParent.ToJson();
    }

    return json;
  }
  public ParseToJson(): string
  {
    const json: any = this.ToJson();

    return JSON.stringify(json);
  }
  public ParseFromJson(json: any): void
  {
    if (json)
    {
      this.Id = json['Id'];
      this.Name = json['Name'];
      this.AccountId = json['AccountId'];

      if (!ObjectUtils.NullOrUndefined(json['AccountIdParent']))
      {
        this.AccountIdParent = new Account();
        this.AccountIdParent.ParseFromJson(json['AccountIdParent']);
      }
    }
  }
}

/**
 * Implementations of the `ContactDepartmentFilter` class
 */
export class ContactDepartmentFilter extends Crudable
{
  public static FromJson(json: Object): ContactDepartmentFilter
  {
    const item: ContactDepartmentFilter = new ContactDepartmentFilter();
    if (json)
      item.ParseFromJson(json);

    return item;
  }

  constructor(
    public Id: number = null,
    public AccountId: number[] = null,
    public Name: string = null,
  )
  {
    super();
    this._InitDefinition();
  }
  private _InitDefinition(): void
  {
    this.SetTableDefinition(`ContactDepartmentFilter`, `contact_departmentFilter`, CrudableTableDefinitionGender.Male, `Contact Department`, `Contact Departments`, true, true, true, true, true, true);

    this.SetColumnDefinition(ContactDepartmentColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(ContactDepartmentColumns.AccountId, `account_id`, `AccountId`, `Account Id`, `Account Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(ContactDepartmentColumns.Name, `name`, `Name`, `Name`, `Names`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
  }

  public Clone(): ContactDepartmentFilter
  {
    const clone: ContactDepartmentFilter = new ContactDepartmentFilter();
    clone.Id = this.Id;
    clone.Name = this.Name;
    clone.AccountId = this.AccountId;

    clone.TableDefinition = this.TableDefinition;
    clone.ColumnDefinitions = this.ColumnDefinitions;

    clone.CustomProperties = this.CustomProperties;

    return clone;
  }
  public ToString(): string
  {
    return `null`;
  }

  public ToJson(): any
  {
    const json: any = {};
    if (!ObjectUtils.NullOrUndefined(this.Id))
      json['Id'] = this.Id;
    if (!ObjectUtils.NullOrUndefined(this.Name))
      json['Name'] = this.Name;
    if (!ObjectUtils.NullOrUndefined(this.AccountId))
      json['AccountId'] = Array.isArray(this.AccountId) ? this.AccountId : [this.AccountId];


    return json;
  }
  public ParseToJson(): string
  {
    const json: any = this.ToJson();

    return JSON.stringify(json);
  }
  public ParseFromJson(json: Object): void
  {
    if (json)
    {
      this.Id = json['Id'];
      this.Name = json['Name'];
      this.AccountId = json['AccountId'];
    }
  }
}
