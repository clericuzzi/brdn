﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model'

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// Navigation Properties
import { User } from 'clericuzzi-lib/entities/models/user.model';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const UserAreaClass: string = `UserArea`;
export const UserAreaTable: string = `user_area`;

/**
 * Column definitions for the `UserArea` class
 */
export enum UserAreaColumns
{
  Id = "Id",
  UserId = "UserId",
  Cities = "Cities",
  Neighbourhoods = "Neighbourhoods",

  UserIdParent = "UserIdParent",
}
export const UserAreaColumnsFilter: string[] = [UserAreaColumns.UserId];
export const UserAreaColumnsInsert: string[] = [UserAreaColumns.UserId, UserAreaColumns.Cities, UserAreaColumns.Neighbourhoods];
export const UserAreaColumnsUpdate: string[] = [UserAreaColumns.UserId, UserAreaColumns.Cities, UserAreaColumns.Neighbourhoods];

/**
 * Implementations of the `UserArea` class
 */
export class UserArea extends Crudable
{
  public UserIdParent: User;

  constructor(
    public Id: number = null,
    public UserId: number = null,
    public Cities: string = null,
    public Neighbourhoods: string = null,
  )
  {
    super();
    this._InitDefinition();
  }
  private _InitDefinition(): void
  {
    this.SetTableDefinition(`UserArea`, `user_area`, CrudableTableDefinitionGender.Female, `Área de Atuação`, `Áreas de Atuação`, true, true, true, true, true, true);

    this.SetColumnDefinition(UserAreaColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(UserAreaColumns.UserId, `user_id`, `UserId`, `Usuário`, `Usuários`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(UserAreaColumns.Cities, `cities`, `Cities`, `Cidades`, `Cidades`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(UserAreaColumns.Neighbourhoods, `neighbourhoods`, `Neighbourhoods`, `Bairros`, `bairros`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
  }

  public Clone(): UserArea
  {
    let clone: UserArea = new UserArea();
    clone.Id = this.Id;
    clone.UserId = this.UserId;
    clone.Cities = this.Cities;
    clone.Neighbourhoods = this.Neighbourhoods;

    clone.UserIdParent = this.UserIdParent;

    clone.TableDefinition = this.TableDefinition;
    clone.ColumnDefinitions = this.ColumnDefinitions;

    clone.CustomProperties = this.CustomProperties;

    return clone;
  }
  public ToString(): string
  {
    return `${this.UserIdParent == null ? '' : this.UserIdParent.ToString()}`;
  }

  public ToJson(): any
  {
    let json: any = new Object();
    if (!ObjectUtils.NullOrUndefined(this.Id))
      json["Id"] = this.Id;
    if (!ObjectUtils.NullOrUndefined(this.UserId))
      json["UserId"] = this.UserId;
    if (!ObjectUtils.NullOrUndefined(this.Cities))
      json["Cities"] = this.Cities;
    if (!ObjectUtils.NullOrUndefined(this.Neighbourhoods))
      json["Neighbourhoods"] = this.Neighbourhoods;
    if (!ObjectUtils.NullOrUndefined(this.UserIdParent))
      json["UserIdParent"] = this.UserIdParent.ToJson();

    this.SerializeCustomProperties(json);

    return json;
  }
  public ParseToJson(): string
  {
    let json: any = this.ToJson();

    return JSON.stringify(json);
  }
  public ParseFromJson(json: Object): void
  {
    if (json)
    {
      this.Id = json["Id"];
      this.UserId = json["UserId"];
      this.Cities = json["Cities"];
      this.Neighbourhoods = json["Neighbourhoods"];

      this.UserIdParent = new User();
      this.UserIdParent.ParseFromJson(json["UserIdParent"]);
    }
  }

  public static FromJson(json: Object): UserArea
  {
    let item: UserArea = new UserArea();
    if (json)
      item.ParseFromJson(json);

    return item;
  }
}

/**
 * Implementations of the `UserAreaFilter` class
 */
export class UserAreaFilter extends Crudable
{
  constructor(
    public Id: number = null,
    public UserId: number[] = null,
    public Cities: string = null,
    public Neighbourhoods: string = null,
  )
  {
    super();
    this._InitDefinition();
  }
  private _InitDefinition(): void
  {
    this.SetTableDefinition(`UserAreaFilter`, `user_areaFilter`, CrudableTableDefinitionGender.Female, `Área de Atuação`, `Áreas de Atuação`, true, true, true, true, true, true);

    this.SetColumnDefinition(UserAreaColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(UserAreaColumns.UserId, `user_id`, `UserId`, `Usuário`, `Usuários`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(UserAreaColumns.Cities, `cities`, `Cities`, `Cidades`, `Cidades`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(UserAreaColumns.Neighbourhoods, `neighbourhoods`, `Neighbourhoods`, `Bairros`, `bairros`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
  }

  public Clone(): UserAreaFilter
  {
    let clone: UserAreaFilter = new UserAreaFilter();
    clone.Id = this.Id;
    clone.UserId = this.UserId;
    clone.Cities = this.Cities;
    clone.Neighbourhoods = this.Neighbourhoods;

    clone.TableDefinition = this.TableDefinition;
    clone.ColumnDefinitions = this.ColumnDefinitions;

    clone.CustomProperties = this.CustomProperties;

    return clone;
  }
  public ToString(): string
  {
    return `null`;
  }

  public ToJson(): any
  {
    let json: any = new Object();
    if (!ObjectUtils.NullOrUndefined(this.Id))
      json["Id"] = this.Id;
    if (!ObjectUtils.NullOrUndefined(this.Cities))
      json["Cities"] = this.Cities;
    if (!ObjectUtils.NullOrUndefined(this.Neighbourhoods))
      json["Neighbourhoods"] = this.Neighbourhoods;
    if (!ObjectUtils.NullOrUndefined(this.UserId))
      json["UserId"] = Array.isArray(this.UserId) ? this.UserId : [this.UserId];

    this.SerializeCustomProperties(json);

    return json;
  }
  public ParseToJson(): string
  {
    let json: any = this.ToJson();

    return JSON.stringify(json);
  }
  public ParseFromJson(json: Object): void
  {
    if (json)
    {
      this.Id = json["Id"];
      this.UserId = json["UserId"];
      this.Cities = json["Cities"];
      this.Neighbourhoods = json["Neighbourhoods"];
    }
  }

  public static FromJson(json: Object): UserAreaFilter
  {
    let item: UserAreaFilter = new UserAreaFilter();
    if (json)
      item.ParseFromJson(json);

    return item;
  }
}
