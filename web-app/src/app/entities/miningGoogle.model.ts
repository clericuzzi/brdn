﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model';

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// Navigation Properties
import { GeographyCity } from 'clericuzzi-lib/entities/models-geo/geographyCity.model';
import { CompanySubtype } from './companySubtype.model';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const MiningGoogleClass: string = `MiningGoogle`;
export const MiningGoogleTable: string = `mining_google`;

/**
 * Column definitions for the `MiningGoogle` class
 */
export enum MiningGoogleColumns
{
  Id = 'Id',
  CityId = 'CityId',
  Priority = 'Priority',
  CompanySubtypeId = 'CompanySubtypeId',

  CityIdParent = 'CityIdParent',
  CompanySubtypeIdParent = 'CompanySubtypeIdParent',
}
export const MiningGoogleColumnsFilter: string[] = [];
export const MiningGoogleColumnsInsert: string[] = [MiningGoogleColumns.CityId, MiningGoogleColumns.CompanySubtypeId, MiningGoogleColumns.Priority];
export const MiningGoogleColumnsUpdate: string[] = [MiningGoogleColumns.CityId, MiningGoogleColumns.CompanySubtypeId, MiningGoogleColumns.Priority];

/**
 * Implementations of the `MiningGoogle` class
 */
export class MiningGoogle extends Crudable
{
  public CityIdParent: GeographyCity;
  public CompanySubtypeIdParent: CompanySubtype;

  public static FromJson(json: any): MiningGoogle
  {
    const item: MiningGoogle = new MiningGoogle();
    if (json)
      item.ParseFromJson(json);

    return item;
  }

  constructor(
    public Id: number = null,
    public CityId: number = null,
    public CompanySubtypeId: number = null,
    public Priority: number = null,
  )
  {
    super();
    this._InitDefinition();
  }
  private _InitDefinition(): void
  {
    this.SetTableDefinition(`MiningGoogle`, `mining_google`, CrudableTableDefinitionGender.Male, `Mining Google`, `Mining Googles`, true, true, true, true, true, true);

    this.SetColumnDefinition(MiningGoogleColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(MiningGoogleColumns.CityId, `city_id`, `CityId`, `City Id`, `City Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(MiningGoogleColumns.CompanySubtypeId, `company_subtype_id`, `CompanySubtypeId`, `Company Subtype Id`, `Company Subtype Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(MiningGoogleColumns.Priority, `priority`, `Priority`, `Priority`, `Prioritys`, ComponentTypeEnum.Number, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
  }

  public Clone(): MiningGoogle
  {
    const clone: MiningGoogle = new MiningGoogle();
    clone.Id = this.Id;
    clone.CityId = this.CityId;
    clone.Priority = this.Priority;
    clone.CompanySubtypeId = this.CompanySubtypeId;

    clone.CityIdParent = this.CityIdParent;
    clone.CompanySubtypeIdParent = this.CompanySubtypeIdParent;

    clone.TableDefinition = this.TableDefinition;
    clone.ColumnDefinitions = this.ColumnDefinitions;

    clone.CustomProperties = this.CustomProperties;

    return clone;
  }
  public ToString(): string
  {
    return `${this.CityIdParent == null ? '' : this.CityIdParent.ToString()}`;
  }

  public ToJson(navigationProperties: boolean = false): any
  {
    const json: any = {};
    if (!ObjectUtils.NullOrUndefined(this.Id))
      json['Id'] = this.Id;
    if (!ObjectUtils.NullOrUndefined(this.CityId))
      json['CityId'] = this.CityId;
    if (!ObjectUtils.NullOrUndefined(this.Priority))
      json['Priority'] = this.Priority;
    if (!ObjectUtils.NullOrUndefined(this.CompanySubtypeId))
      json['CompanySubtypeId'] = this.CompanySubtypeId;
    if (navigationProperties)
    {
      if (!ObjectUtils.NullOrUndefined(this.CityIdParent))
        json['CityIdParent'] = this.CityIdParent.ToJson();
      if (!ObjectUtils.NullOrUndefined(this.CompanySubtypeIdParent))
        json['CompanySubtypeIdParent'] = this.CompanySubtypeIdParent.ToJson();
    }

    return json;
  }
  public ParseToJson(): string
  {
    const json: any = this.ToJson();

    return JSON.stringify(json);
  }
  public ParseFromJson(json: any): void
  {
    if (json)
    {
      this.Id = json['Id'];
      this.CityId = json['CityId'];
      this.Priority = json['Priority'];
      this.CompanySubtypeId = json['CompanySubtypeId'];

      if (!ObjectUtils.NullOrUndefined(json['CityIdParent']))
      {
        this.CityIdParent = new GeographyCity();
        this.CityIdParent.ParseFromJson(json['CityIdParent']);
      }
      if (!ObjectUtils.NullOrUndefined(json['CompanySubtypeIdParent']))
      {
        this.CompanySubtypeIdParent = new CompanySubtype();
        this.CompanySubtypeIdParent.ParseFromJson(json['CompanySubtypeIdParent']);
      }
    }
  }
}

/**
 * Implementations of the `MiningGoogleFilter` class
 */
export class MiningGoogleFilter extends Crudable
{
  public static FromJson(json: Object): MiningGoogleFilter
  {
    const item: MiningGoogleFilter = new MiningGoogleFilter();
    if (json)
      item.ParseFromJson(json);

    return item;
  }

  constructor(
    public Id: number = null,
    public CityId: number[] = null,
    public CompanySubtypeId: number[] = null,
    public Priority: number = null,
  )
  {
    super();
    this._InitDefinition();
  }
  private _InitDefinition(): void
  {
    this.SetTableDefinition(`MiningGoogleFilter`, `mining_googleFilter`, CrudableTableDefinitionGender.Male, `Mining Google`, `Mining Googles`, true, true, true, true, true, true);

    this.SetColumnDefinition(MiningGoogleColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(MiningGoogleColumns.CityId, `city_id`, `CityId`, `City Id`, `City Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(MiningGoogleColumns.CompanySubtypeId, `company_subtype_id`, `CompanySubtypeId`, `Company Subtype Id`, `Company Subtype Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(MiningGoogleColumns.Priority, `priority`, `Priority`, `Priority`, `Prioritys`, ComponentTypeEnum.Number, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
  }

  public Clone(): MiningGoogleFilter
  {
    const clone: MiningGoogleFilter = new MiningGoogleFilter();
    clone.Id = this.Id;
    clone.CityId = this.CityId;
    clone.Priority = this.Priority;
    clone.CompanySubtypeId = this.CompanySubtypeId;

    clone.TableDefinition = this.TableDefinition;
    clone.ColumnDefinitions = this.ColumnDefinitions;

    clone.CustomProperties = this.CustomProperties;

    return clone;
  }
  public ToString(): string
  {
    return `null`;
  }

  public ToJson(): any
  {
    const json: any = {};
    if (!ObjectUtils.NullOrUndefined(this.Id))
      json['Id'] = this.Id;
    if (!ObjectUtils.NullOrUndefined(this.Priority))
      json['Priority'] = this.Priority;
    if (!ObjectUtils.NullOrUndefined(this.CityId))
      json['CityId'] = Array.isArray(this.CityId) ? this.CityId : [this.CityId];
    if (!ObjectUtils.NullOrUndefined(this.CompanySubtypeId))
      json['CompanySubtypeId'] = Array.isArray(this.CompanySubtypeId) ? this.CompanySubtypeId : [this.CompanySubtypeId];


    return json;
  }
  public ParseToJson(): string
  {
    const json: any = this.ToJson();

    return JSON.stringify(json);
  }
  public ParseFromJson(json: Object): void
  {
    if (json)
    {
      this.Id = json['Id'];
      this.CityId = json['CityId'];
      this.Priority = json['Priority'];
      this.CompanySubtypeId = json['CompanySubtypeId'];
    }
  }
}
