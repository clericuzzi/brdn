﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model';

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// Navigation Properties
import { User } from 'clericuzzi-lib/entities/models/user.model';
import { Company } from './company.model';
import { Activity } from 'clericuzzi-lib/entities/models/activity.model';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const LeadClass: string = `Lead`;
export const LeadTable: string = `lead`;

/**
 * Column definitions for the `Lead` class
 */
export enum LeadColumns
{
  Id = 'Id',
  UserId = 'UserId',
  CompanyId = 'CompanyId',
  Timestamp = 'Timestamp',
  ActivityId = 'ActivityId',

  UserIdParent = 'UserIdParent',
  CompanyIdParent = 'CompanyIdParent',
  ActivityIdParent = 'ActivityIdParent',
}
export const LeadColumnsFilter: string[] = [];
export const LeadColumnsInsert: string[] = [LeadColumns.UserId, LeadColumns.CompanyId, LeadColumns.ActivityId, LeadColumns.Timestamp];
export const LeadColumnsUpdate: string[] = [LeadColumns.UserId, LeadColumns.CompanyId, LeadColumns.ActivityId, LeadColumns.Timestamp];

/**
 * Implementations of the `Lead` class
 */
export class Lead extends Crudable
{
  public UserIdParent: User;
  public CompanyIdParent: Company;
  public ActivityIdParent: Activity;

  public static FromJson(json: any): Lead
  {
    const item: Lead = new Lead();
    if (json)
      item.ParseFromJson(json);

    return item;
  }

  constructor(
    public Id: number = null,
    public UserId: number = null,
    public CompanyId: number = null,
    public ActivityId: number = null,
    public Timestamp: Date = null,
  )
  {
    super();
    this._InitDefinition();
  }
  private _InitDefinition(): void
  {
    this.SetTableDefinition(`Lead`, `lead`, CrudableTableDefinitionGender.Male, `Lead`, `Leads`, true, true, true, true, true, true);

    this.SetColumnDefinition(LeadColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(LeadColumns.UserId, `user_id`, `UserId`, `User Id`, `User Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(LeadColumns.CompanyId, `company_id`, `CompanyId`, `Company Id`, `Company Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(LeadColumns.ActivityId, `activity_id`, `ActivityId`, `Activity Id`, `Activity Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(LeadColumns.Timestamp, `timestamp`, `Timestamp`, `Timestamp`, `Timestamps`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, true);
  }

  public Clone(): Lead
  {
    const clone: Lead = new Lead();
    clone.Id = this.Id;
    clone.UserId = this.UserId;
    clone.CompanyId = this.CompanyId;
    clone.Timestamp = this.Timestamp;
    clone.ActivityId = this.ActivityId;

    clone.UserIdParent = this.UserIdParent;
    clone.CompanyIdParent = this.CompanyIdParent;
    clone.ActivityIdParent = this.ActivityIdParent;

    clone.TableDefinition = this.TableDefinition;
    clone.ColumnDefinitions = this.ColumnDefinitions;

    clone.CustomProperties = this.CustomProperties;

    return clone;
  }
  public ToString(): string
  {
    return `${this.UserIdParent == null ? '' : this.UserIdParent.ToString()}`;
  }

  public ToJson(navigationProperties: boolean = false): any
  {
    const json: any = {};
    if (!ObjectUtils.NullOrUndefined(this.Id))
      json['Id'] = this.Id;
    if (!ObjectUtils.NullOrUndefined(this.UserId))
      json['UserId'] = this.UserId;
    if (!ObjectUtils.NullOrUndefined(this.CompanyId))
      json['CompanyId'] = this.CompanyId;
    if (!ObjectUtils.NullOrUndefined(this.ActivityId))
      json['ActivityId'] = this.ActivityId;
    if (!ObjectUtils.NullOrUndefined(this.Timestamp))
      json['Timestamp'] = DateUtils.ToDateServer(this.Timestamp);
    if (navigationProperties)
    {
      if (!ObjectUtils.NullOrUndefined(this.UserIdParent))
        json['UserIdParent'] = this.UserIdParent.ToJson();
      if (!ObjectUtils.NullOrUndefined(this.CompanyIdParent))
        json['CompanyIdParent'] = this.CompanyIdParent.ToJson();
      if (!ObjectUtils.NullOrUndefined(this.ActivityIdParent))
        json['ActivityIdParent'] = this.ActivityIdParent.ToJson();
    }

    return json;
  }
  public ParseToJson(): string
  {
    const json: any = this.ToJson();

    return JSON.stringify(json);
  }
  public ParseFromJson(json: any): void
  {
    if (json)
    {
      this.Id = json['Id'];
      this.UserId = json['UserId'];
      this.CompanyId = json['CompanyId'];
      this.ActivityId = json['ActivityId'];
      this.Timestamp = DateUtils.FromDateServer(json['Timestamp']);

      if (!ObjectUtils.NullOrUndefined(json['UserIdParent']))
      {
        this.UserIdParent = new User();
        this.UserIdParent.ParseFromJson(json['UserIdParent']);
      }
      if (!ObjectUtils.NullOrUndefined(json['CompanyIdParent']))
      {
        this.CompanyIdParent = new Company();
        this.CompanyIdParent.ParseFromJson(json['CompanyIdParent']);
      }
      if (!ObjectUtils.NullOrUndefined(json['ActivityIdParent']))
      {
        this.ActivityIdParent = new Activity();
        this.ActivityIdParent.ParseFromJson(json['ActivityIdParent']);
      }
    }
  }
}

/**
 * Implementations of the `LeadFilter` class
 */
export class LeadFilter extends Crudable
{
  public static FromJson(json: Object): LeadFilter
  {
    const item: LeadFilter = new LeadFilter();
    if (json)
      item.ParseFromJson(json);

    return item;
  }

  constructor(
    public Id: number = null,
    public UserId: number[] = null,
    public CompanyId: number[] = null,
    public ActivityId: number[] = null,
    public Timestamp: Date = null,
  )
  {
    super();
    this._InitDefinition();
  }
  private _InitDefinition(): void
  {
    this.SetTableDefinition(`LeadFilter`, `leadFilter`, CrudableTableDefinitionGender.Male, `Lead`, `Leads`, true, true, true, true, true, true);

    this.SetColumnDefinition(LeadColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(LeadColumns.UserId, `user_id`, `UserId`, `User Id`, `User Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(LeadColumns.CompanyId, `company_id`, `CompanyId`, `Company Id`, `Company Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(LeadColumns.ActivityId, `activity_id`, `ActivityId`, `Activity Id`, `Activity Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(LeadColumns.Timestamp, `timestamp`, `Timestamp`, `Timestamp`, `Timestamps`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, true);
  }

  public Clone(): LeadFilter
  {
    const clone: LeadFilter = new LeadFilter();
    clone.Id = this.Id;
    clone.UserId = this.UserId;
    clone.CompanyId = this.CompanyId;
    clone.Timestamp = this.Timestamp;
    clone.ActivityId = this.ActivityId;

    clone.TableDefinition = this.TableDefinition;
    clone.ColumnDefinitions = this.ColumnDefinitions;

    clone.CustomProperties = this.CustomProperties;

    return clone;
  }
  public ToString(): string
  {
    return `null`;
  }

  public ToJson(): any
  {
    const json: any = {};
    if (!ObjectUtils.NullOrUndefined(this.Id))
      json['Id'] = this.Id;
    if (!ObjectUtils.NullOrUndefined(this.Timestamp))
      json['Timestamp'] = DateUtils.ToDateServer(this.Timestamp);
    if (!ObjectUtils.NullOrUndefined(this.UserId))
      json['UserId'] = Array.isArray(this.UserId) ? this.UserId : [this.UserId];
    if (!ObjectUtils.NullOrUndefined(this.CompanyId))
      json['CompanyId'] = Array.isArray(this.CompanyId) ? this.CompanyId : [this.CompanyId];
    if (!ObjectUtils.NullOrUndefined(this.ActivityId))
      json['ActivityId'] = Array.isArray(this.ActivityId) ? this.ActivityId : [this.ActivityId];


    return json;
  }
  public ParseToJson(): string
  {
    const json: any = this.ToJson();

    return JSON.stringify(json);
  }
  public ParseFromJson(json: Object): void
  {
    if (json)
    {
      this.Id = json['Id'];
      this.UserId = json['UserId'];
      this.CompanyId = json['CompanyId'];
      this.ActivityId = json['ActivityId'];
      this.Timestamp = DateUtils.FromDateServer(json['Timestamp']);
    }
  }
}
