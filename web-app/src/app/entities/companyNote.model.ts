﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model';

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// Navigation Properties
import { User } from 'clericuzzi-lib/entities/models/user.model';
import { Company } from './company.model';
import { CompanyContact } from './companyContact.model';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const CompanyNoteClass: string = `CompanyNote`;
export const CompanyNoteTable: string = `company_note`;

/**
 * Column definitions for the `CompanyNote` class
 */
export enum CompanyNoteColumns
{
  Id = 'Id',
  UserId = 'UserId',
  Comment = 'Comment',
  CompanyId = 'CompanyId',
  ContactId = 'ContactId',
  Timestamp = 'Timestamp',

  UserIdParent = 'UserIdParent',
  CompanyIdParent = 'CompanyIdParent',
  ContactIdParent = 'ContactIdParent',
}
export const CompanyNoteColumnsFilter: string[] = [];
export const CompanyNoteColumnsInsert: string[] = [CompanyNoteColumns.ContactId, CompanyNoteColumns.Comment];
export const CompanyNoteColumnsUpdate: string[] = [CompanyNoteColumns.ContactId, CompanyNoteColumns.Comment];

/**
 * Implementations of the `CompanyNote` class
 */
export class CompanyNote extends Crudable
{
  public UserIdParent: User;
  public CompanyIdParent: Company;
  public ContactIdParent: CompanyContact;

  public static FromJson(json: any): CompanyNote
  {
    const item: CompanyNote = new CompanyNote();
    if (json)
      item.ParseFromJson(json);

    return item;
  }

  constructor(
    public Id: number = null,
    public UserId: number = null,
    public CompanyId: number = null,
    public ContactId: number = null,
    public Comment: string = null,
    public Timestamp: Date = null,
  )
  {
    super();
    this._InitDefinition();
  }
  private _InitDefinition(): void
  {
    this.SetTableDefinition(`CompanyNote`, `company_note`, CrudableTableDefinitionGender.Female, `Anotação`, `Anotações`, true, true, true, true, true, true);

    this.SetColumnDefinition(CompanyNoteColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(CompanyNoteColumns.UserId, `user_id`, `UserId`, `User Id`, `User Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(CompanyNoteColumns.CompanyId, `company_id`, `CompanyId`, `Company Id`, `Company Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(CompanyNoteColumns.ContactId, `contact_id`, `ContactId`, `Contato`, `Contatos`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(CompanyNoteColumns.Comment, `comment`, `Comment`, `Anotação`, `Anotações`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(CompanyNoteColumns.Timestamp, `timestamp`, `Timestamp`, `Timestamp`, `Timestamps`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
  }

  public Clone(): CompanyNote
  {
    const clone: CompanyNote = new CompanyNote();
    clone.Id = this.Id;
    clone.UserId = this.UserId;
    clone.Comment = this.Comment;
    clone.CompanyId = this.CompanyId;
    clone.ContactId = this.ContactId;
    clone.Timestamp = this.Timestamp;

    clone.UserIdParent = this.UserIdParent;
    clone.CompanyIdParent = this.CompanyIdParent;
    clone.ContactIdParent = this.ContactIdParent;

    clone.TableDefinition = this.TableDefinition;
    clone.ColumnDefinitions = this.ColumnDefinitions;

    clone.CustomProperties = this.CustomProperties;

    return clone;
  }
  public ToString(): string
  {
    return `${this.UserIdParent == null ? '' : this.UserIdParent.ToString()}`;
  }

  public ToJson(navigationProperties: boolean = false): any
  {
    const json: any = {};
    if (!ObjectUtils.NullOrUndefined(this.Id))
      json['Id'] = this.Id;
    if (!ObjectUtils.NullOrUndefined(this.UserId))
      json['UserId'] = this.UserId;
    if (!ObjectUtils.NullOrUndefined(this.Comment))
      json['Comment'] = this.Comment;
    if (!ObjectUtils.NullOrUndefined(this.CompanyId))
      json['CompanyId'] = this.CompanyId;
    if (!ObjectUtils.NullOrUndefined(this.ContactId))
      json['ContactId'] = this.ContactId;
    if (!ObjectUtils.NullOrUndefined(this.Timestamp))
      json['Timestamp'] = DateUtils.ToDateServer(this.Timestamp);
    if (navigationProperties)
    {
      if (!ObjectUtils.NullOrUndefined(this.UserIdParent))
        json['UserIdParent'] = this.UserIdParent.ToJson();
      if (!ObjectUtils.NullOrUndefined(this.CompanyIdParent))
        json['CompanyIdParent'] = this.CompanyIdParent.ToJson();
      if (!ObjectUtils.NullOrUndefined(this.ContactIdParent))
        json['ContactIdParent'] = this.ContactIdParent.ToJson();
    }

    return json;
  }
  public ParseToJson(): string
  {
    const json: any = this.ToJson();

    return JSON.stringify(json);
  }
  public ParseFromJson(json: any): void
  {
    if (json)
    {
      this.Id = json['Id'];
      this.UserId = json['UserId'];
      this.Comment = json['Comment'];
      this.CompanyId = json['CompanyId'];
      this.ContactId = json['ContactId'];
      this.Timestamp = DateUtils.FromDateServer(json['Timestamp']);

      if (!ObjectUtils.NullOrUndefined(json['UserIdParent']))
      {
        this.UserIdParent = new User();
        this.UserIdParent.ParseFromJson(json['UserIdParent']);
      }
      if (!ObjectUtils.NullOrUndefined(json['CompanyIdParent']))
      {
        this.CompanyIdParent = new Company();
        this.CompanyIdParent.ParseFromJson(json['CompanyIdParent']);
      }
      if (!ObjectUtils.NullOrUndefined(json['ContactIdParent']))
      {
        this.ContactIdParent = new CompanyContact();
        this.ContactIdParent.ParseFromJson(json['ContactIdParent']);
      }
    }
  }
}

/**
 * Implementations of the `CompanyNoteFilter` class
 */
export class CompanyNoteFilter extends Crudable
{
  public static FromJson(json: Object): CompanyNoteFilter
  {
    const item: CompanyNoteFilter = new CompanyNoteFilter();
    if (json)
      item.ParseFromJson(json);

    return item;
  }

  constructor(
    public Id: number = null,
    public UserId: number[] = null,
    public CompanyId: number[] = null,
    public ContactId: number[] = null,
    public Comment: string = null,
    public Timestamp: Date = null,
  )
  {
    super();
    this._InitDefinition();
  }
  private _InitDefinition(): void
  {
    this.SetTableDefinition(`CompanyNoteFilter`, `company_noteFilter`, CrudableTableDefinitionGender.Male, `Company Note`, `Company Notes`, true, true, true, true, true, true);

    this.SetColumnDefinition(CompanyNoteColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(CompanyNoteColumns.UserId, `user_id`, `UserId`, `User Id`, `User Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(CompanyNoteColumns.CompanyId, `company_id`, `CompanyId`, `Company Id`, `Company Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(CompanyNoteColumns.ContactId, `contact_id`, `ContactId`, `Contact Id`, `Contact Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(CompanyNoteColumns.Comment, `comment`, `Comment`, `Comment`, `Comments`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(CompanyNoteColumns.Timestamp, `timestamp`, `Timestamp`, `Timestamp`, `Timestamps`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
  }

  public Clone(): CompanyNoteFilter
  {
    const clone: CompanyNoteFilter = new CompanyNoteFilter();
    clone.Id = this.Id;
    clone.UserId = this.UserId;
    clone.Comment = this.Comment;
    clone.CompanyId = this.CompanyId;
    clone.ContactId = this.ContactId;
    clone.Timestamp = this.Timestamp;

    clone.TableDefinition = this.TableDefinition;
    clone.ColumnDefinitions = this.ColumnDefinitions;

    clone.CustomProperties = this.CustomProperties;

    return clone;
  }
  public ToString(): string
  {
    return `null`;
  }

  public ToJson(): any
  {
    const json: any = {};
    if (!ObjectUtils.NullOrUndefined(this.Id))
      json['Id'] = this.Id;
    if (!ObjectUtils.NullOrUndefined(this.Comment))
      json['Comment'] = this.Comment;
    if (!ObjectUtils.NullOrUndefined(this.Timestamp))
      json['Timestamp'] = DateUtils.ToDateServer(this.Timestamp);
    if (!ObjectUtils.NullOrUndefined(this.UserId))
      json['UserId'] = Array.isArray(this.UserId) ? this.UserId : [this.UserId];
    if (!ObjectUtils.NullOrUndefined(this.CompanyId))
      json['CompanyId'] = Array.isArray(this.CompanyId) ? this.CompanyId : [this.CompanyId];
    if (!ObjectUtils.NullOrUndefined(this.ContactId))
      json['ContactId'] = Array.isArray(this.ContactId) ? this.ContactId : [this.ContactId];


    return json;
  }
  public ParseToJson(): string
  {
    const json: any = this.ToJson();

    return JSON.stringify(json);
  }
  public ParseFromJson(json: Object): void
  {
    if (json)
    {
      this.Id = json['Id'];
      this.UserId = json['UserId'];
      this.Comment = json['Comment'];
      this.CompanyId = json['CompanyId'];
      this.ContactId = json['ContactId'];
      this.Timestamp = DateUtils.FromDateServer(json['Timestamp']);
    }
  }
}
