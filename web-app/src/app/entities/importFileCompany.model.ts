﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model';

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// Navigation Properties
import { ImportFile } from './importFile.model';
import { Company } from './company.model';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const ImportFileCompanyClass: string = `ImportFileCompany`;
export const ImportFileCompanyTable: string = `import_file_company`;

/**
 * Column definitions for the `ImportFileCompany` class
 */
export enum ImportFileCompanyColumns
{
  Id = 'Id',
  CompanyId = 'CompanyId',
  ImportFileId = 'ImportFileId',

  CompanyIdParent = 'CompanyIdParent',
  ImportFileIdParent = 'ImportFileIdParent',
}
export const ImportFileCompanyColumnsFilter: string[] = [];
export const ImportFileCompanyColumnsInsert: string[] = [ImportFileCompanyColumns.ImportFileId, ImportFileCompanyColumns.CompanyId];
export const ImportFileCompanyColumnsUpdate: string[] = [ImportFileCompanyColumns.ImportFileId, ImportFileCompanyColumns.CompanyId];

/**
 * Implementations of the `ImportFileCompany` class
 */
export class ImportFileCompany extends Crudable
{
  public ImportFileIdParent: ImportFile;
  public CompanyIdParent: Company;

  public static FromJson(json: any): ImportFileCompany
  {
    const item: ImportFileCompany = new ImportFileCompany();
    if (json)
      item.ParseFromJson(json);

    return item;
  }

  constructor(
    public Id: number = null,
    public ImportFileId: number = null,
    public CompanyId: number = null,
  )
  {
    super();
    this._InitDefinition();
  }
  private _InitDefinition(): void
  {
    this.SetTableDefinition(`ImportFileCompany`, `import_file_company`, CrudableTableDefinitionGender.Male, `Import File Company`, `Import File Companys`, true, true, true, true, true, true);

    this.SetColumnDefinition(ImportFileCompanyColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(ImportFileCompanyColumns.ImportFileId, `import_file_id`, `ImportFileId`, `Import File Id`, `Import File Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(ImportFileCompanyColumns.CompanyId, `company_id`, `CompanyId`, `Company Id`, `Company Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
  }

  public Clone(): ImportFileCompany
  {
    const clone: ImportFileCompany = new ImportFileCompany();
    clone.Id = this.Id;
    clone.CompanyId = this.CompanyId;
    clone.ImportFileId = this.ImportFileId;

    clone.CompanyIdParent = this.CompanyIdParent;
    clone.ImportFileIdParent = this.ImportFileIdParent;

    clone.TableDefinition = this.TableDefinition;
    clone.ColumnDefinitions = this.ColumnDefinitions;

    clone.CustomProperties = this.CustomProperties;

    return clone;
  }
  public ToString(): string
  {
    return `${this.ImportFileIdParent == null ? '' : this.ImportFileIdParent.ToString()}`;
  }

  public ToJson(navigationProperties: boolean = false): any
  {
    const json: any = {};
    if (!ObjectUtils.NullOrUndefined(this.Id))
      json['Id'] = this.Id;
    if (!ObjectUtils.NullOrUndefined(this.CompanyId))
      json['CompanyId'] = this.CompanyId;
    if (!ObjectUtils.NullOrUndefined(this.ImportFileId))
      json['ImportFileId'] = this.ImportFileId;
    if (navigationProperties)
    {
      if (!ObjectUtils.NullOrUndefined(this.ImportFileIdParent))
        json['ImportFileIdParent'] = this.ImportFileIdParent.ToJson();
      if (!ObjectUtils.NullOrUndefined(this.CompanyIdParent))
        json['CompanyIdParent'] = this.CompanyIdParent.ToJson();
    }

    return json;
  }
  public ParseToJson(): string
  {
    const json: any = this.ToJson();

    return JSON.stringify(json);
  }
  public ParseFromJson(json: any): void
  {
    if (json)
    {
      this.Id = json['Id'];
      this.CompanyId = json['CompanyId'];
      this.ImportFileId = json['ImportFileId'];

      if (!ObjectUtils.NullOrUndefined(json['ImportFileIdParent']))
      {
        this.ImportFileIdParent = new ImportFile();
        this.ImportFileIdParent.ParseFromJson(json['ImportFileIdParent']);
      }
      if (!ObjectUtils.NullOrUndefined(json['CompanyIdParent']))
      {
        this.CompanyIdParent = new Company();
        this.CompanyIdParent.ParseFromJson(json['CompanyIdParent']);
      }
    }
  }
}

/**
 * Implementations of the `ImportFileCompanyFilter` class
 */
export class ImportFileCompanyFilter extends Crudable
{
  public static FromJson(json: Object): ImportFileCompanyFilter
  {
    const item: ImportFileCompanyFilter = new ImportFileCompanyFilter();
    if (json)
      item.ParseFromJson(json);

    return item;
  }

  constructor(
    public Id: number = null,
    public ImportFileId: number[] = null,
    public CompanyId: number[] = null,
  )
  {
    super();
    this._InitDefinition();
  }
  private _InitDefinition(): void
  {
    this.SetTableDefinition(`ImportFileCompanyFilter`, `import_file_companyFilter`, CrudableTableDefinitionGender.Male, `Import File Company`, `Import File Companys`, true, true, true, true, true, true);

    this.SetColumnDefinition(ImportFileCompanyColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(ImportFileCompanyColumns.ImportFileId, `import_file_id`, `ImportFileId`, `Import File Id`, `Import File Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(ImportFileCompanyColumns.CompanyId, `company_id`, `CompanyId`, `Company Id`, `Company Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
  }

  public Clone(): ImportFileCompanyFilter
  {
    const clone: ImportFileCompanyFilter = new ImportFileCompanyFilter();
    clone.Id = this.Id;
    clone.CompanyId = this.CompanyId;
    clone.ImportFileId = this.ImportFileId;

    clone.TableDefinition = this.TableDefinition;
    clone.ColumnDefinitions = this.ColumnDefinitions;

    clone.CustomProperties = this.CustomProperties;

    return clone;
  }
  public ToString(): string
  {
    return `null`;
  }

  public ToJson(): any
  {
    const json: any = {};
    if (!ObjectUtils.NullOrUndefined(this.Id))
      json['Id'] = this.Id;
    if (!ObjectUtils.NullOrUndefined(this.CompanyId))
      json['CompanyId'] = Array.isArray(this.CompanyId) ? this.CompanyId : [this.CompanyId];
    if (!ObjectUtils.NullOrUndefined(this.ImportFileId))
      json['ImportFileId'] = Array.isArray(this.ImportFileId) ? this.ImportFileId : [this.ImportFileId];


    return json;
  }
  public ParseToJson(): string
  {
    const json: any = this.ToJson();

    return JSON.stringify(json);
  }
  public ParseFromJson(json: Object): void
  {
    if (json)
    {
      this.Id = json['Id'];
      this.CompanyId = json['CompanyId'];
      this.ImportFileId = json['ImportFileId'];
    }
  }
}
