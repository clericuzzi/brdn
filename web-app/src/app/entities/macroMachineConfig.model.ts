﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model';
import { Tabulation } from './tabulation.model';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';
import { MacroMachine } from './macroMachine.model';
import { MacroLogin } from './macroLogin.model';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';

export const MacroMachineConfigClass: string = `MacroMachineConfig`;
export const MacroMachineConfigTable: string = `macro_machine_config`;

/**
 * Column definitions for the `MacroMachineConfig` class
 */
export enum MacroMachineConfigColumns
{
  Id = 'Id',
  Ending = 'Ending',
  MacroId = 'MacroId',
  IsAlive = 'IsAlive',
  Starting = 'Starting',
  MachineId = 'MachineId',
  Instances = 'Instances',
  TurnOffAfter = 'TurnOffAfter',
  FromBackground = 'FromBackground',

  MacroIdParent = 'MacroIdParent',
  MachineIdParent = 'MachineIdParent',
}
export const MacroMachineConfigColumnsFilter: string[] = [MacroMachineConfigColumns.MacroId];
export const MacroMachineConfigColumnsInsert: string[] = [MacroMachineConfigColumns.MacroId, MacroMachineConfigColumns.Instances, MacroMachineConfigColumns.Starting, MacroMachineConfigColumns.Ending, MacroMachineConfigColumns.TurnOffAfter, MacroMachineConfigColumns.FromBackground];
export const MacroMachineConfigColumnsUpdate: string[] = [MacroMachineConfigColumns.MacroId, MacroMachineConfigColumns.Instances, MacroMachineConfigColumns.Starting, MacroMachineConfigColumns.Ending, MacroMachineConfigColumns.TurnOffAfter, MacroMachineConfigColumns.FromBackground];

/**
 * Implementations of the `MacroMachineConfig` class
 */
export class MacroMachineConfig extends Crudable
{
  public MacroIdParent: MacroLogin;
  public MachineIdParent: MacroMachine;

  public static FromJson(json: any): MacroMachineConfig
  {
    const item: MacroMachineConfig = new MacroMachineConfig();
    if (json)
      item.ParseFromJson(json);

    return item;
  }

  constructor(
    public Id: number = null,
    public MachineId: number = null,
    public MacroId: number = null,
    public Instances: number = null,
    public Starting: number = null,
    public Ending: number = null,
    public IsAlive: number = null,
    public TurnOffAfter: number = null,
    public FromBackground: number = null,
  )
  {
    super();
    this._InitDefinition();
  }
  private _InitDefinition(): void
  {
    this.SetTableDefinition(`MacroMachineConfig`, `macro_machine_config`, CrudableTableDefinitionGender.Female, `Configuração de mineração`, `Configurações de mineração`, true, true, true, true, true, true);

    this.SetColumnDefinition(MacroMachineConfigColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(MacroMachineConfigColumns.MachineId, `machine_id`, `MachineId`, `Máquina`, `Máquina`, ComponentTypeEnum.AutoComplete, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(MacroMachineConfigColumns.MacroId, `macro_id`, `MacroId`, `Macro`, `Macros`, ComponentTypeEnum.AutoComplete, true, true, true, false, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(MacroMachineConfigColumns.Instances, `instances`, `Instances`, `Qtd`, `Qtd`, ComponentTypeEnum.Number, true, true, true, false, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(MacroMachineConfigColumns.Starting, `starting`, `Starting`, `Início`, `Início`, ComponentTypeEnum.Text, true, true, true, false, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(MacroMachineConfigColumns.Ending, `ending`, `Ending`, `Fim`, `Fim`, ComponentTypeEnum.Text, true, true, true, false, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(MacroMachineConfigColumns.IsAlive, `is_alive`, `IsAlive`, `Rodando`, `Rodando`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(MacroMachineConfigColumns.TurnOffAfter, `turn_off_after`, `TurnOffAfter`, `Desligar máquina`, `Desligar máquina`, ComponentTypeEnum.ComboYesNo, true, true, true, false, true, true, null, StringUtils.TransformYesNo, null, null, null, `left`, false);
    this.SetColumnDefinition(MacroMachineConfigColumns.FromBackground, `from_background`, `FromBackground`, `Segundo plano`, `Segundo plano`, ComponentTypeEnum.ComboYesNo, true, true, true, false, true, true, null, StringUtils.TransformYesNo, null, null, null, `left`, false);
  }

  public Clone(): MacroMachineConfig
  {
    const clone: MacroMachineConfig = new MacroMachineConfig();
    clone.Id = this.Id;
    clone.Ending = this.Ending;
    clone.MacroId = this.MacroId;
    clone.IsAlive = this.IsAlive;
    clone.Starting = this.Starting;
    clone.MachineId = this.MachineId;
    clone.Instances = this.Instances;
    clone.TurnOffAfter = this.TurnOffAfter;
    clone.FromBackground = this.FromBackground;

    clone.MacroIdParent = this.MacroIdParent;
    clone.MachineIdParent = this.MachineIdParent;

    clone.TableDefinition = this.TableDefinition;
    clone.ColumnDefinitions = this.ColumnDefinitions;

    clone.CustomProperties = this.CustomProperties;

    return clone;
  }
  public ToString(): string
  {
    return `${this.MachineIdParent == null ? '' : this.MachineIdParent.ToString()} ${this.MacroIdParent == null ? '' : this.MacroIdParent.ToString()}`;
  }

  public ToJson(navigationProperties: boolean = false): any
  {
    const json: any = {};
    if (!ObjectUtils.NullOrUndefined(this.Id))
      json['Id'] = this.Id;
    if (!ObjectUtils.NullOrUndefined(this.Ending))
      json['Ending'] = this.Ending;
    if (!ObjectUtils.NullOrUndefined(this.MacroId))
      json['MacroId'] = this.MacroId;
    if (!ObjectUtils.NullOrUndefined(this.IsAlive))
      json['IsAlive'] = this.IsAlive;
    if (!ObjectUtils.NullOrUndefined(this.Starting))
      json['Starting'] = this.Starting;
    if (!ObjectUtils.NullOrUndefined(this.MachineId))
      json['MachineId'] = this.MachineId;
    if (!ObjectUtils.NullOrUndefined(this.Instances))
      json['Instances'] = this.Instances;
    if (!ObjectUtils.NullOrUndefined(this.TurnOffAfter))
      json['TurnOffAfter'] = this.TurnOffAfter;
    if (!ObjectUtils.NullOrUndefined(this.FromBackground))
      json['FromBackground'] = this.FromBackground;
    if (navigationProperties)
    {
      if (!ObjectUtils.NullOrUndefined(this.MachineIdParent))
        json['MachineIdParent'] = this.MachineIdParent.ToJson();
      if (!ObjectUtils.NullOrUndefined(this.MacroIdParent))
        json['MacroIdParent'] = this.MacroIdParent.ToJson();
    }

    return json;
  }
  public ParseToJson(): string
  {
    const json: any = this.ToJson();

    return JSON.stringify(json);
  }
  public ParseFromJson(json: any): void
  {
    if (json)
    {
      this.Id = json['Id'];
      this.Ending = json['Ending'];
      this.MacroId = json['MacroId'];
      this.IsAlive = json['IsAlive'];
      this.Starting = json['Starting'];
      this.MachineId = json['MachineId'];
      this.Instances = json['Instances'];
      this.TurnOffAfter = json['TurnOffAfter'];
      this.FromBackground = json['FromBackground'];

      if (!ObjectUtils.NullOrUndefined(json['MachineIdParent']))
      {
        this.MachineIdParent = new MacroMachine();
        this.MachineIdParent.ParseFromJson(json['MachineIdParent']);
      }
      if (!ObjectUtils.NullOrUndefined(json['MacroIdParent']))
      {
        this.MacroIdParent = new MacroLogin();
        this.MacroIdParent.ParseFromJson(json['MacroIdParent']);
      }
    }
  }
}

/**
 * Implementations of the `MacroMachineConfigFilter` class
 */
export class MacroMachineConfigFilter extends Crudable
{
  public static FromJson(json: Object): MacroMachineConfigFilter
  {
    const item: MacroMachineConfigFilter = new MacroMachineConfigFilter();
    if (json)
      item.ParseFromJson(json);

    return item;
  }

  constructor(
    public Id: number = null,
    public MachineId: number[] = null,
    public MacroId: number[] = null,
    public Instances: number = null,
    public Starting: number = null,
    public Ending: number = null,
    public IsAlive: number = null,
    public TurnOffAfter: number = null,
    public FromBackground: number = null,
  )
  {
    super();
    this._InitDefinition();
  }
  private _InitDefinition(): void
  {
    this.SetTableDefinition(`MacroMachineConfigFilter`, `macro_machine_configFilter`, CrudableTableDefinitionGender.Female, `Configuração de mineração`, `Configurações de mineração`, true, true, true, true, true, true);

    this.SetColumnDefinition(MacroMachineConfigColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(MacroMachineConfigColumns.MachineId, `machine_id`, `MachineId`, `Máquina`, `Máquina`, ComponentTypeEnum.AutoComplete, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(MacroMachineConfigColumns.MacroId, `macro_id`, `MacroId`, `Macro`, `Macros`, ComponentTypeEnum.AutoComplete, true, true, true, false, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(MacroMachineConfigColumns.Instances, `instances`, `Instances`, `Qtd`, `Qtd`, ComponentTypeEnum.Number, true, true, true, false, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(MacroMachineConfigColumns.Starting, `starting`, `Starting`, `Início`, `Início`, ComponentTypeEnum.Text, true, true, true, false, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(MacroMachineConfigColumns.Ending, `ending`, `Ending`, `Fim`, `Fim`, ComponentTypeEnum.Text, true, true, true, false, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(MacroMachineConfigColumns.IsAlive, `is_alive`, `IsAlive`, `Rodando`, `Rodando`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(MacroMachineConfigColumns.TurnOffAfter, `turn_off_after`, `TurnOffAfter`, `Desligar máquina`, `Desligar máquina`, ComponentTypeEnum.ComboYesNo, true, true, true, false, true, true, null, StringUtils.TransformYesNo, null, null, null, `left`, false);
    this.SetColumnDefinition(MacroMachineConfigColumns.FromBackground, `from_background`, `FromBackground`, `Segundo plano`, `Segundo plano`, ComponentTypeEnum.ComboYesNo, true, true, true, false, true, true, null, StringUtils.TransformYesNo, null, null, null, `left`, false);
  }

  public Clone(): MacroMachineConfigFilter
  {
    const clone: MacroMachineConfigFilter = new MacroMachineConfigFilter();
    clone.Id = this.Id;
    clone.Ending = this.Ending;
    clone.MacroId = this.MacroId;
    clone.IsAlive = this.IsAlive;
    clone.Starting = this.Starting;
    clone.MachineId = this.MachineId;
    clone.Instances = this.Instances;
    clone.TurnOffAfter = this.TurnOffAfter;
    clone.FromBackground = this.FromBackground;

    clone.TableDefinition = this.TableDefinition;
    clone.ColumnDefinitions = this.ColumnDefinitions;

    clone.CustomProperties = this.CustomProperties;

    return clone;
  }
  public ToString(): string
  {
    return `null`;
  }

  public ToJson(): any
  {
    const json: any = {};
    if (!ObjectUtils.NullOrUndefined(this.Id))
      json['Id'] = this.Id;
    if (!ObjectUtils.NullOrUndefined(this.Ending))
      json['Ending'] = this.Ending;
    if (!ObjectUtils.NullOrUndefined(this.IsAlive))
      json['IsAlive'] = this.IsAlive;
    if (!ObjectUtils.NullOrUndefined(this.Starting))
      json['Starting'] = this.Starting;
    if (!ObjectUtils.NullOrUndefined(this.Instances))
      json['Instances'] = this.Instances;
    if (!ObjectUtils.NullOrUndefined(this.TurnOffAfter))
      json['TurnOffAfter'] = this.TurnOffAfter;
    if (!ObjectUtils.NullOrUndefined(this.FromBackground))
      json['FromBackground'] = this.FromBackground;
    if (!ObjectUtils.NullOrUndefined(this.MacroId))
      json['MacroId'] = Array.isArray(this.MacroId) ? this.MacroId : [this.MacroId];
    if (!ObjectUtils.NullOrUndefined(this.MachineId))
      json['MachineId'] = Array.isArray(this.MachineId) ? this.MachineId : [this.MachineId];


    return json;
  }
  public ParseToJson(): string
  {
    const json: any = this.ToJson();

    return JSON.stringify(json);
  }
  public ParseFromJson(json: Object): void
  {
    if (json)
    {
      this.Id = json['Id'];
      this.Ending = json['Ending'];
      this.MacroId = json['MacroId'];
      this.IsAlive = json['IsAlive'];
      this.Starting = json['Starting'];
      this.MachineId = json['MachineId'];
      this.Instances = json['Instances'];
      this.TurnOffAfter = json['TurnOffAfter'];
      this.FromBackground = json['FromBackground'];
    }
  }
}
