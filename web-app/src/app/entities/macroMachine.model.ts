﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model';
import { Tabulation } from './tabulation.model';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';

export const MacroMachineClass: string = `MacroMachine`;
export const MacroMachineTable: string = `macro_machine`;

/**
 * Column definitions for the `MacroMachine` class
 */
export enum MacroMachineColumns
{
  Id = 'Id',
  IsAlive = 'IsAlive',
  Updated = 'Updated',
  MachineName = 'MachineName',
}
export const MacroMachineColumnsFilter: string[] = [MacroMachineColumns.MachineName];
export const MacroMachineColumnsInsert: string[] = [MacroMachineColumns.MachineName];
export const MacroMachineColumnsUpdate: string[] = [MacroMachineColumns.MachineName];

/**
 * Implementations of the `MacroMachine` class
 */
export class MacroMachine extends Crudable
{
  public static FromJson(json: any): MacroMachine
  {
    const item: MacroMachine = new MacroMachine();
    if (json)
      item.ParseFromJson(json);

    return item;
  }

  constructor(
    public Id: number = null,
    public MachineName: string = null,
    public IsAlive: number = null,
    public Updated: number = null,
  )
  {
    super();
    this._InitDefinition();
  }
  private _InitDefinition(): void
  {
    this.SetTableDefinition(`MacroMachine`, `macro_machine`, CrudableTableDefinitionGender.Female, `Máquina de mineração`, `Máquinas de mineração`, true, true, true, true, true, true);

    this.SetColumnDefinition(MacroMachineColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(MacroMachineColumns.MachineName, `machine_name`, `MachineName`, `Máquina`, `Máquina`, ComponentTypeEnum.Text, true, true, true, false, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(MacroMachineColumns.IsAlive, `is_alive`, `IsAlive`, `Ligada`, `Ligada`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(MacroMachineColumns.Updated, `updated`, `Updated`, `Atualizada`, `Atualizada`, ComponentTypeEnum.ComboYesNo, true, true, false, false, false, true, null, StringUtils.TransformYesNo, null, null, null, `left`, false);
  }

  public Clone(): MacroMachine
  {
    const clone: MacroMachine = new MacroMachine();
    clone.Id = this.Id;
    clone.IsAlive = this.IsAlive;
    clone.Updated = this.Updated;
    clone.MachineName = this.MachineName;

    clone.TableDefinition = this.TableDefinition;
    clone.ColumnDefinitions = this.ColumnDefinitions;

    clone.CustomProperties = this.CustomProperties;

    return clone;
  }
  public ToString(): string
  {
    return `${this.MachineName}`;
  }

  public ToJson(navigationProperties: boolean = false): any
  {
    const json: any = {};
    if (!ObjectUtils.NullOrUndefined(this.Id))
      json['Id'] = this.Id;
    if (!ObjectUtils.NullOrUndefined(this.IsAlive))
      json['IsAlive'] = this.IsAlive;
    if (!ObjectUtils.NullOrUndefined(this.Updated))
      json['Updated'] = this.Updated;
    if (!ObjectUtils.NullOrUndefined(this.MachineName))
      json['MachineName'] = this.MachineName;
    if (navigationProperties)
    {
    }

    return json;
  }
  public ParseToJson(): string
  {
    const json: any = this.ToJson();

    return JSON.stringify(json);
  }
  public ParseFromJson(json: any): void
  {
    if (json)
    {
      this.Id = json['Id'];
      this.IsAlive = json['IsAlive'];
      this.Updated = json['Updated'];
      this.MachineName = json['MachineName'];
    }
  }
}

/**
 * Implementations of the `MacroMachineFilter` class
 */
export class MacroMachineFilter extends Crudable
{
  public static FromJson(json: Object): MacroMachineFilter
  {
    const item: MacroMachineFilter = new MacroMachineFilter();
    if (json)
      item.ParseFromJson(json);

    return item;
  }

  constructor(
    public Id: number = null,
    public MachineName: string = null,
    public IsAlive: number = null,
    public Updated: number = null,
  )
  {
    super();
    this._InitDefinition();
  }
  private _InitDefinition(): void
  {
    this.SetTableDefinition(`MacroMachineFilter`, `macro_machineFilter`, CrudableTableDefinitionGender.Female, `Máquina de mineração`, `Máquinas de mineração`, true, true, true, true, true, true);

    this.SetColumnDefinition(MacroMachineColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(MacroMachineColumns.MachineName, `machine_name`, `MachineName`, `Máquina`, `Máquina`, ComponentTypeEnum.Text, true, true, true, false, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(MacroMachineColumns.IsAlive, `is_alive`, `IsAlive`, `Ligada`, `Ligada`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(MacroMachineColumns.Updated, `updated`, `Updated`, `Atualizada`, `Atualizada`, ComponentTypeEnum.ComboYesNo, true, true, false, false, false, true, null, StringUtils.TransformYesNo, null, null, null, `left`, false);
  }

  public Clone(): MacroMachineFilter
  {
    const clone: MacroMachineFilter = new MacroMachineFilter();
    clone.Id = this.Id;
    clone.IsAlive = this.IsAlive;
    clone.Updated = this.Updated;
    clone.MachineName = this.MachineName;

    clone.TableDefinition = this.TableDefinition;
    clone.ColumnDefinitions = this.ColumnDefinitions;

    clone.CustomProperties = this.CustomProperties;

    return clone;
  }
  public ToString(): string
  {
    return `null`;
  }

  public ToJson(): any
  {
    const json: any = {};
    if (!ObjectUtils.NullOrUndefined(this.Id))
      json['Id'] = this.Id;
    if (!ObjectUtils.NullOrUndefined(this.IsAlive))
      json['IsAlive'] = this.IsAlive;
    if (!ObjectUtils.NullOrUndefined(this.Updated))
      json['Updated'] = this.Updated;
    if (!ObjectUtils.NullOrUndefined(this.MachineName))
      json['MachineName'] = this.MachineName;


    return json;
  }
  public ParseToJson(): string
  {
    const json: any = this.ToJson();

    return JSON.stringify(json);
  }
  public ParseFromJson(json: Object): void
  {
    if (json)
    {
      this.Id = json['Id'];
      this.IsAlive = json['IsAlive'];
      this.Updated = json['Updated'];
      this.MachineName = json['MachineName'];
    }
  }
}
