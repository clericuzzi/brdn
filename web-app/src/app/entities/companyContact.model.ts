﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model';

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// Navigation Properties
import { User } from 'clericuzzi-lib/entities/models/user.model';
import { Company } from './company.model';
import { ContactPosition } from './contactPosition.model';
import { ContactDepartment } from './contactDepartment.model';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const CompanyContactClass: string = `CompanyContact`;
export const CompanyContactTable: string = `company_contact`;

/**
 * Column definitions for the `CompanyContact` class
 */
export enum CompanyContactColumns
{
  Id = 'Id',
  Name = 'Name',
  Email = 'Email',
  UserId = 'UserId',
  CompanyId = 'CompanyId',
  Timestamp = 'Timestamp',
  PositionId = 'PositionId',
  DepartmentId = 'DepartmentId',

  UserIdParent = 'UserIdParent',
  CompanyIdParent = 'CompanyIdParent',
  PositionIdParent = 'PositionIdParent',
  DepartmentIdParent = 'DepartmentIdParent',
}
export const CompanyContactColumnsFilter: string[] = [];
export const CompanyContactColumnsInsert: string[] = [CompanyContactColumns.PositionId, CompanyContactColumns.DepartmentId, CompanyContactColumns.Name, CompanyContactColumns.Email];
export const CompanyContactColumnsUpdate: string[] = [CompanyContactColumns.PositionId, CompanyContactColumns.DepartmentId, CompanyContactColumns.Name, CompanyContactColumns.Email];

/**
 * Implementations of the `CompanyContact` class
 */
export class CompanyContact extends Crudable
{
  public UserIdParent: User;
  public CompanyIdParent: Company;
  public PositionIdParent: ContactPosition;
  public DepartmentIdParent: ContactDepartment;

  public static FromJson(json: any): CompanyContact
  {
    const item: CompanyContact = new CompanyContact();
    if (json)
      item.ParseFromJson(json);

    return item;
  }

  constructor(
    public Id: number = null,
    public UserId: number = null,
    public CompanyId: number = null,
    public PositionId: number = null,
    public DepartmentId: number = null,
    public Name: string = null,
    public Email: string = null,
    public Timestamp: Date = null,
  )
  {
    super();
    this._InitDefinition();
  }
  private _InitDefinition(): void
  {
    this.SetTableDefinition(`CompanyContact`, `company_contact`, CrudableTableDefinitionGender.Male, `Contato`, `Contato`, true, true, true, true, true, true);

    this.SetColumnDefinition(CompanyContactColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(CompanyContactColumns.UserId, `user_id`, `UserId`, `User Id`, `User Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(CompanyContactColumns.CompanyId, `company_id`, `CompanyId`, `Company Id`, `Company Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(CompanyContactColumns.PositionId, `position_id`, `PositionId`, `Posição`, `Posições`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(CompanyContactColumns.DepartmentId, `department_id`, `DepartmentId`, `Departamento`, `Departamentos`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(CompanyContactColumns.Name, `name`, `Name`, `Nome`, `Nomes`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(CompanyContactColumns.Email, `email`, `Email`, `Email`, `Emails`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(CompanyContactColumns.Timestamp, `timestamp`, `Timestamp`, `Registrado em`, `Registrados em`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
  }

  public Clone(): CompanyContact
  {
    const clone: CompanyContact = new CompanyContact();
    clone.Id = this.Id;
    clone.Name = this.Name;
    clone.Email = this.Email;
    clone.UserId = this.UserId;
    clone.CompanyId = this.CompanyId;
    clone.Timestamp = this.Timestamp;
    clone.PositionId = this.PositionId;
    clone.DepartmentId = this.DepartmentId;

    clone.UserIdParent = this.UserIdParent;
    clone.CompanyIdParent = this.CompanyIdParent;
    clone.PositionIdParent = this.PositionIdParent;
    clone.DepartmentIdParent = this.DepartmentIdParent;

    clone.TableDefinition = this.TableDefinition;
    clone.ColumnDefinitions = this.ColumnDefinitions;

    clone.CustomProperties = this.CustomProperties;

    return clone;
  }
  public ToString(): string
  {
    return `${this.UserIdParent == null ? '' : this.UserIdParent.ToString()}`;
  }

  public ToJson(navigationProperties: boolean = false): any
  {
    const json: any = {};
    if (!ObjectUtils.NullOrUndefined(this.Id))
      json['Id'] = this.Id;
    if (!ObjectUtils.NullOrUndefined(this.Name))
      json['Name'] = this.Name;
    if (!ObjectUtils.NullOrUndefined(this.Email))
      json['Email'] = this.Email;
    if (!ObjectUtils.NullOrUndefined(this.UserId))
      json['UserId'] = this.UserId;
    if (!ObjectUtils.NullOrUndefined(this.CompanyId))
      json['CompanyId'] = this.CompanyId;
    if (!ObjectUtils.NullOrUndefined(this.PositionId))
      json['PositionId'] = this.PositionId;
    if (!ObjectUtils.NullOrUndefined(this.DepartmentId))
      json['DepartmentId'] = this.DepartmentId;
    if (!ObjectUtils.NullOrUndefined(this.Timestamp))
      json['Timestamp'] = DateUtils.ToDateServer(this.Timestamp);
    if (navigationProperties)
    {
      if (!ObjectUtils.NullOrUndefined(this.UserIdParent))
        json['UserIdParent'] = this.UserIdParent.ToJson();
      if (!ObjectUtils.NullOrUndefined(this.CompanyIdParent))
        json['CompanyIdParent'] = this.CompanyIdParent.ToJson();
      if (!ObjectUtils.NullOrUndefined(this.PositionIdParent))
        json['PositionIdParent'] = this.PositionIdParent.ToJson();
      if (!ObjectUtils.NullOrUndefined(this.DepartmentIdParent))
        json['DepartmentIdParent'] = this.DepartmentIdParent.ToJson();
    }

    return json;
  }
  public ParseToJson(): string
  {
    const json: any = this.ToJson();

    return JSON.stringify(json);
  }
  public ParseFromJson(json: any): void
  {
    if (json)
    {
      this.Id = json['Id'];
      this.Name = json['Name'];
      this.Email = json['Email'];
      this.UserId = json['UserId'];
      this.CompanyId = json['CompanyId'];
      this.PositionId = json['PositionId'];
      this.DepartmentId = json['DepartmentId'];
      this.Timestamp = DateUtils.FromDateServer(json['Timestamp']);

      if (!ObjectUtils.NullOrUndefined(json['UserIdParent']))
      {
        this.UserIdParent = new User();
        this.UserIdParent.ParseFromJson(json['UserIdParent']);
      }
      if (!ObjectUtils.NullOrUndefined(json['CompanyIdParent']))
      {
        this.CompanyIdParent = new Company();
        this.CompanyIdParent.ParseFromJson(json['CompanyIdParent']);
      }
      if (!ObjectUtils.NullOrUndefined(json['PositionIdParent']))
      {
        this.PositionIdParent = new ContactPosition();
        this.PositionIdParent.ParseFromJson(json['PositionIdParent']);
      }
      if (!ObjectUtils.NullOrUndefined(json['DepartmentIdParent']))
      {
        this.DepartmentIdParent = new ContactDepartment();
        this.DepartmentIdParent.ParseFromJson(json['DepartmentIdParent']);
      }
    }
  }
}

/**
 * Implementations of the `CompanyContactFilter` class
 */
export class CompanyContactFilter extends Crudable
{
  public static FromJson(json: Object): CompanyContactFilter
  {
    const item: CompanyContactFilter = new CompanyContactFilter();
    if (json)
      item.ParseFromJson(json);

    return item;
  }

  constructor(
    public Id: number = null,
    public UserId: number[] = null,
    public CompanyId: number[] = null,
    public PositionId: number[] = null,
    public DepartmentId: number[] = null,
    public Name: string = null,
    public Email: string = null,
    public Timestamp: Date = null,
  )
  {
    super();
    this._InitDefinition();
  }
  private _InitDefinition(): void
  {
    this.SetTableDefinition(`CompanyContactFilter`, `company_contactFilter`, CrudableTableDefinitionGender.Male, `Company Contact`, `Company Contacts`, true, true, true, true, true, true);

    this.SetColumnDefinition(CompanyContactColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(CompanyContactColumns.UserId, `user_id`, `UserId`, `User Id`, `User Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(CompanyContactColumns.CompanyId, `company_id`, `CompanyId`, `Company Id`, `Company Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(CompanyContactColumns.PositionId, `position_id`, `PositionId`, `Position Id`, `Position Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(CompanyContactColumns.DepartmentId, `department_id`, `DepartmentId`, `Department Id`, `Department Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(CompanyContactColumns.Name, `name`, `Name`, `Name`, `Names`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(CompanyContactColumns.Email, `email`, `Email`, `Email`, `Emails`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(CompanyContactColumns.Timestamp, `timestamp`, `Timestamp`, `Timestamp`, `Timestamps`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
  }

  public Clone(): CompanyContactFilter
  {
    const clone: CompanyContactFilter = new CompanyContactFilter();
    clone.Id = this.Id;
    clone.Name = this.Name;
    clone.Email = this.Email;
    clone.UserId = this.UserId;
    clone.CompanyId = this.CompanyId;
    clone.Timestamp = this.Timestamp;
    clone.PositionId = this.PositionId;
    clone.DepartmentId = this.DepartmentId;

    clone.TableDefinition = this.TableDefinition;
    clone.ColumnDefinitions = this.ColumnDefinitions;

    clone.CustomProperties = this.CustomProperties;

    return clone;
  }
  public ToString(): string
  {
    return `null`;
  }

  public ToJson(): any
  {
    const json: any = {};
    if (!ObjectUtils.NullOrUndefined(this.Id))
      json['Id'] = this.Id;
    if (!ObjectUtils.NullOrUndefined(this.Name))
      json['Name'] = this.Name;
    if (!ObjectUtils.NullOrUndefined(this.Email))
      json['Email'] = this.Email;
    if (!ObjectUtils.NullOrUndefined(this.Timestamp))
      json['Timestamp'] = DateUtils.ToDateServer(this.Timestamp);
    if (!ObjectUtils.NullOrUndefined(this.UserId))
      json['UserId'] = Array.isArray(this.UserId) ? this.UserId : [this.UserId];
    if (!ObjectUtils.NullOrUndefined(this.CompanyId))
      json['CompanyId'] = Array.isArray(this.CompanyId) ? this.CompanyId : [this.CompanyId];
    if (!ObjectUtils.NullOrUndefined(this.PositionId))
      json['PositionId'] = Array.isArray(this.PositionId) ? this.PositionId : [this.PositionId];
    if (!ObjectUtils.NullOrUndefined(this.DepartmentId))
      json['DepartmentId'] = Array.isArray(this.DepartmentId) ? this.DepartmentId : [this.DepartmentId];


    return json;
  }
  public ParseToJson(): string
  {
    const json: any = this.ToJson();

    return JSON.stringify(json);
  }
  public ParseFromJson(json: Object): void
  {
    if (json)
    {
      this.Id = json['Id'];
      this.Name = json['Name'];
      this.Email = json['Email'];
      this.UserId = json['UserId'];
      this.CompanyId = json['CompanyId'];
      this.PositionId = json['PositionId'];
      this.DepartmentId = json['DepartmentId'];
      this.Timestamp = DateUtils.FromDateServer(json['Timestamp']);
    }
  }
}
