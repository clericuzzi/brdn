﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model';

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// Navigation Properties
import { Account } from 'clericuzzi-lib/entities/models/account.model';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const ContactPositionClass: string = `ContactPosition`;
export const ContactPositionTable: string = `contact_position`;

/**
 * Column definitions for the `ContactPosition` class
 */
export enum ContactPositionColumns
{
  Id = 'Id',
  Name = 'Name',
  AccountId = 'AccountId',

  AccountIdParent = 'AccountIdParent',
}
export const ContactPositionColumnsFilter: string[] = [ContactPositionColumns.Name];
export const ContactPositionColumnsInsert: string[] = [ContactPositionColumns.Name];
export const ContactPositionColumnsUpdate: string[] = [ContactPositionColumns.Name];

/**
 * Implementations of the `ContactPosition` class
 */
export class ContactPosition extends Crudable
{
  public AccountIdParent: Account;

  public static FromJson(json: any): ContactPosition
  {
    const item: ContactPosition = new ContactPosition();
    if (json)
      item.ParseFromJson(json);

    return item;
  }

  constructor(
    public Id: number = null,
    public AccountId: number = null,
    public Name: string = null,
  )
  {
    super();
    this._InitDefinition();
  }
  private _InitDefinition(): void
  {
    this.SetTableDefinition(`ContactPosition`, `contact_position`, CrudableTableDefinitionGender.Male, `Posição`, `Posições`, true, true, true, true, true, true);

    this.SetColumnDefinition(ContactPositionColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(ContactPositionColumns.AccountId, `account_id`, `AccountId`, `Conta`, `Contas`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(ContactPositionColumns.Name, `name`, `Name`, `Nome`, `Nomes`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
  }

  public Clone(): ContactPosition
  {
    const clone: ContactPosition = new ContactPosition();
    clone.Id = this.Id;
    clone.Name = this.Name;
    clone.AccountId = this.AccountId;

    clone.AccountIdParent = this.AccountIdParent;

    clone.TableDefinition = this.TableDefinition;
    clone.ColumnDefinitions = this.ColumnDefinitions;

    clone.CustomProperties = this.CustomProperties;

    return clone;
  }
  public ToString(): string
  {
    return `${this.AccountIdParent == null ? '' : this.AccountIdParent.ToString()}`;
  }

  public ToJson(navigationProperties: boolean = false): any
  {
    const json: any = {};
    if (!ObjectUtils.NullOrUndefined(this.Id))
      json['Id'] = this.Id;
    if (!ObjectUtils.NullOrUndefined(this.Name))
      json['Name'] = this.Name;
    if (!ObjectUtils.NullOrUndefined(this.AccountId))
      json['AccountId'] = this.AccountId;
    if (navigationProperties)
    {
      if (!ObjectUtils.NullOrUndefined(this.AccountIdParent))
        json['AccountIdParent'] = this.AccountIdParent.ToJson();
    }

    return json;
  }
  public ParseToJson(): string
  {
    const json: any = this.ToJson();

    return JSON.stringify(json);
  }
  public ParseFromJson(json: any): void
  {
    if (json)
    {
      this.Id = json['Id'];
      this.Name = json['Name'];
      this.AccountId = json['AccountId'];

      if (!ObjectUtils.NullOrUndefined(json['AccountIdParent']))
      {
        this.AccountIdParent = new Account();
        this.AccountIdParent.ParseFromJson(json['AccountIdParent']);
      }
    }
  }
}

/**
 * Implementations of the `ContactPositionFilter` class
 */
export class ContactPositionFilter extends Crudable
{
  public static FromJson(json: Object): ContactPositionFilter
  {
    const item: ContactPositionFilter = new ContactPositionFilter();
    if (json)
      item.ParseFromJson(json);

    return item;
  }

  constructor(
    public Id: number = null,
    public AccountId: number[] = null,
    public Name: string = null,
  )
  {
    super();
    this._InitDefinition();
  }
  private _InitDefinition(): void
  {
    this.SetTableDefinition(`ContactPositionFilter`, `contact_positionFilter`, CrudableTableDefinitionGender.Male, `Contact Position`, `Contact Positions`, true, true, true, true, true, true);

    this.SetColumnDefinition(ContactPositionColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(ContactPositionColumns.AccountId, `account_id`, `AccountId`, `Account Id`, `Account Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(ContactPositionColumns.Name, `name`, `Name`, `Name`, `Names`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
  }

  public Clone(): ContactPositionFilter
  {
    const clone: ContactPositionFilter = new ContactPositionFilter();
    clone.Id = this.Id;
    clone.Name = this.Name;
    clone.AccountId = this.AccountId;

    clone.TableDefinition = this.TableDefinition;
    clone.ColumnDefinitions = this.ColumnDefinitions;

    clone.CustomProperties = this.CustomProperties;

    return clone;
  }
  public ToString(): string
  {
    return `null`;
  }

  public ToJson(): any
  {
    const json: any = {};
    if (!ObjectUtils.NullOrUndefined(this.Id))
      json['Id'] = this.Id;
    if (!ObjectUtils.NullOrUndefined(this.Name))
      json['Name'] = this.Name;
    if (!ObjectUtils.NullOrUndefined(this.AccountId))
      json['AccountId'] = Array.isArray(this.AccountId) ? this.AccountId : [this.AccountId];


    return json;
  }
  public ParseToJson(): string
  {
    const json: any = this.ToJson();

    return JSON.stringify(json);
  }
  public ParseFromJson(json: Object): void
  {
    if (json)
    {
      this.Id = json['Id'];
      this.Name = json['Name'];
      this.AccountId = json['AccountId'];
    }
  }
}
