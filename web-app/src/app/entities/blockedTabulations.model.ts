﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model';
import { Tabulation } from './tabulation.model';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const BlockedTabulationsClass: string = `BlockedTabulations`;
export const BlockedTabulationsTable: string = `blocked_tabulations`;

/**
 * Column definitions for the `BlockedTabulations` class
 */
export enum BlockedTabulationsColumns
{
  Id = 'Id',
  TabulationId = 'TabulationId',

  TabulationIdParent = 'TabulationIdParent',
}
export const BlockedTabulationsColumnsFilter: string[] = [BlockedTabulationsColumns.TabulationId];
export const BlockedTabulationsColumnsInsert: string[] = [BlockedTabulationsColumns.TabulationId];
export const BlockedTabulationsColumnsUpdate: string[] = [BlockedTabulationsColumns.TabulationId];

/**
 * Implementations of the `BlockedTabulations` class
 */
export class BlockedTabulations extends Crudable
{
  public TabulationIdParent: Tabulation;

  public static FromJson(json: any): BlockedTabulations
  {
    const item: BlockedTabulations = new BlockedTabulations();
    if (json)
      item.ParseFromJson(json);

    return item;
  }

  constructor(
    public Id: number = null,
    public TabulationId: number = null,
  )
  {
    super();
    this._InitDefinition();
  }
  private _InitDefinition(): void
  {
    this.SetTableDefinition(`BlockedTabulations`, `blocked_tabulations`, CrudableTableDefinitionGender.Female, `Tabulação que não volta aos leads`, `Tabulações que não voltam aos leads`, true, true, true, true, true, true);

    this.SetColumnDefinition(BlockedTabulationsColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(BlockedTabulationsColumns.TabulationId, `tabulation_id`, `TabulationId`, `Tabulação`, `Tabulações`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
  }

  public Clone(): BlockedTabulations
  {
    const clone: BlockedTabulations = new BlockedTabulations();
    clone.Id = this.Id;
    clone.TabulationId = this.TabulationId;

    clone.TabulationIdParent = this.TabulationIdParent;

    clone.TableDefinition = this.TableDefinition;
    clone.ColumnDefinitions = this.ColumnDefinitions;

    clone.CustomProperties = this.CustomProperties;

    return clone;
  }
  public ToString(): string
  {
    return `${this.TabulationIdParent == null ? '' : this.TabulationIdParent.ToString()}`;
  }

  public ToJson(navigationProperties: boolean = false): any
  {
    const json: any = {};
    if (!ObjectUtils.NullOrUndefined(this.Id))
      json['Id'] = this.Id;
    if (!ObjectUtils.NullOrUndefined(this.TabulationId))
      json['TabulationId'] = this.TabulationId;
    if (navigationProperties)
    {
      if (!ObjectUtils.NullOrUndefined(this.TabulationIdParent))
        json['TabulationIdParent'] = this.TabulationIdParent.ToJson();
    }

    return json;
  }
  public ParseToJson(): string
  {
    const json: any = this.ToJson();

    return JSON.stringify(json);
  }
  public ParseFromJson(json: any): void
  {
    if (json)
    {
      this.Id = json['Id'];
      this.TabulationId = json['TabulationId'];

      if (!ObjectUtils.NullOrUndefined(json['TabulationIdParent']))
      {
        this.TabulationIdParent = new Tabulation();
        this.TabulationIdParent.ParseFromJson(json['TabulationIdParent']);
      }
    }
  }
}

/**
 * Implementations of the `BlockedTabulationsFilter` class
 */
export class BlockedTabulationsFilter extends Crudable
{
  public static FromJson(json: Object): BlockedTabulationsFilter
  {
    const item: BlockedTabulationsFilter = new BlockedTabulationsFilter();
    if (json)
      item.ParseFromJson(json);

    return item;
  }

  constructor(
    public Id: number = null,
    public Tabulation: string = null,
    public TabulationId: number[] = null,
  )
  {
    super();
    this._InitDefinition();
  }
  private _InitDefinition(): void
  {
    this.SetTableDefinition(`BlockedTabulationsFilter`, `blocked_tabulationsFilter`, CrudableTableDefinitionGender.Female, `Tabulação que não volta para os leads`, `Tabulações que não voltam para os leads`, true, true, true, true, true, true);

    this.SetColumnDefinition(BlockedTabulationsColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(BlockedTabulationsColumns.TabulationId, `tabulation_id`, `TabulationId`, `Tabulação`, `Tabulações`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
  }

  public Clone(): BlockedTabulationsFilter
  {
    const clone: BlockedTabulationsFilter = new BlockedTabulationsFilter();
    clone.Id = this.Id;
    clone.TabulationId = this.TabulationId;

    clone.TableDefinition = this.TableDefinition;
    clone.ColumnDefinitions = this.ColumnDefinitions;

    clone.CustomProperties = this.CustomProperties;

    return clone;
  }
  public ToString(): string
  {
    return `null`;
  }

  public ToJson(): any
  {
    const json: any = {};
    if (!ObjectUtils.NullOrUndefined(this.Id))
      json['Id'] = this.Id;
    if (!ObjectUtils.NullOrUndefined(this.TabulationId))
      json['TabulationId'] = Array.isArray(this.TabulationId) ? this.TabulationId : [this.TabulationId];


    return json;
  }
  public ParseToJson(): string
  {
    const json: any = this.ToJson();

    return JSON.stringify(json);
  }
  public ParseFromJson(json: Object): void
  {
    if (json)
    {
      this.Id = json['Id'];
      this.TabulationId = json['TabulationId'];
    }
  }
}
