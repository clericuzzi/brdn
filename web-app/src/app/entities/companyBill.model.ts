﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model';

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// Navigation Properties
import { Company } from './company.model';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const CompanyBillClass: string = `CompanyBill`;
export const CompanyBillTable: string = `company_bill`;

/**
 * Column definitions for the `CompanyBill` class
 */
export enum CompanyBillColumns
{
  Id = 'Id',
  Due = 'Due',
  Value = 'Value',
  Serial = 'Serial',
  Billing = 'Billing',
  CompanyId = 'CompanyId',

  CompanyIdParent = 'CompanyIdParent',
}
export const CompanyBillColumnsFilter: string[] = [];
export const CompanyBillColumnsInsert: string[] = [CompanyBillColumns.Billing, CompanyBillColumns.Serial, CompanyBillColumns.Value, CompanyBillColumns.Due];
export const CompanyBillColumnsUpdate: string[] = [CompanyBillColumns.Billing, CompanyBillColumns.Serial, CompanyBillColumns.Value, CompanyBillColumns.Due];

/**
 * Implementations of the `CompanyBill` class
 */
export class CompanyBill extends Crudable
{
  public CompanyIdParent: Company;

  public static FromJson(json: any): CompanyBill
  {
    const item: CompanyBill = new CompanyBill();
    if (json)
      item.ParseFromJson(json);

    return item;
  }

  constructor(
    public Id: number = null,
    public CompanyId: number = null,
    public Billing: string = null,
    public Serial: string = null,
    public Value: number = null,
    public Due: Date = null,
  )
  {
    super();
    this._InitDefinition();
  }
  private _InitDefinition(): void
  {
    this.SetTableDefinition(`CompanyBill`, `company_bill`, CrudableTableDefinitionGender.Female, `Fatura`, `Faturas`, true, true, true, true, true, true);

    this.SetColumnDefinition(CompanyBillColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(CompanyBillColumns.CompanyId, `company_id`, `CompanyId`, `Empresa`, `Empresas`, ComponentTypeEnum.AutoComplete, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(CompanyBillColumns.Billing, `billing`, `Billing`, `Origem`, `Origens`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(CompanyBillColumns.Serial, `serial`, `Serial`, `Fatura`, `Faturas`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(CompanyBillColumns.Value, `value`, `Value`, `Valor`, `Valores`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(CompanyBillColumns.Due, `due`, `Due`, `Vencimento`, `Vencimentos`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, true);
  }

  public Clone(): CompanyBill
  {
    const clone: CompanyBill = new CompanyBill();
    clone.Id = this.Id;
    clone.Due = this.Due;
    clone.Value = this.Value;
    clone.Serial = this.Serial;
    clone.Billing = this.Billing;
    clone.CompanyId = this.CompanyId;

    clone.CompanyIdParent = this.CompanyIdParent;

    clone.TableDefinition = this.TableDefinition;
    clone.ColumnDefinitions = this.ColumnDefinitions;

    clone.CustomProperties = this.CustomProperties;

    return clone;
  }
  public ToString(): string
  {
    return `${this.Billing} ${this.Serial}`;
  }

  public ToJson(navigationProperties: boolean = false): any
  {
    const json: any = {};
    if (!ObjectUtils.NullOrUndefined(this.Id))
      json['Id'] = this.Id;
    if (!ObjectUtils.NullOrUndefined(this.Value))
      json['Value'] = this.Value;
    if (!ObjectUtils.NullOrUndefined(this.Serial))
      json['Serial'] = this.Serial;
    if (!ObjectUtils.NullOrUndefined(this.Billing))
      json['Billing'] = this.Billing;
    if (!ObjectUtils.NullOrUndefined(this.CompanyId))
      json['CompanyId'] = this.CompanyId;
    if (!ObjectUtils.NullOrUndefined(this.Due))
      json['Due'] = DateUtils.ToDateServer(this.Due);
    if (navigationProperties)
    {
      if (!ObjectUtils.NullOrUndefined(this.CompanyIdParent))
        json['CompanyIdParent'] = this.CompanyIdParent.ToJson();
    }

    return json;
  }
  public ParseToJson(): string
  {
    const json: any = this.ToJson();

    return JSON.stringify(json);
  }
  public ParseFromJson(json: any): void
  {
    if (json)
    {
      this.Id = json['Id'];
      this.Value = json['Value'];
      this.Serial = json['Serial'];
      this.Billing = json['Billing'];
      this.CompanyId = json['CompanyId'];
      this.Due = DateUtils.FromDateServer(json['Due']);

      if (!ObjectUtils.NullOrUndefined(json['CompanyIdParent']))
      {
        this.CompanyIdParent = new Company();
        this.CompanyIdParent.ParseFromJson(json['CompanyIdParent']);
      }
    }
  }
}

/**
 * Implementations of the `CompanyBillFilter` class
 */
export class CompanyBillFilter extends Crudable
{
  public static FromJson(json: Object): CompanyBillFilter
  {
    const item: CompanyBillFilter = new CompanyBillFilter();
    if (json)
      item.ParseFromJson(json);

    return item;
  }

  constructor(
    public Id: number = null,
    public CompanyId: number[] = null,
    public Billing: string = null,
    public Serial: string = null,
    public Value: number = null,
    public Due: Date = null,
  )
  {
    super();
    this._InitDefinition();
  }
  private _InitDefinition(): void
  {
    this.SetTableDefinition(`CompanyBillFilter`, `company_billFilter`, CrudableTableDefinitionGender.Female, `Fatura`, `Faturas`, true, true, true, true, true, true);

    this.SetColumnDefinition(CompanyBillColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(CompanyBillColumns.CompanyId, `company_id`, `CompanyId`, `Empresa`, `Empresas`, ComponentTypeEnum.AutoComplete, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(CompanyBillColumns.Billing, `billing`, `Billing`, `Origem`, `Origens`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(CompanyBillColumns.Serial, `serial`, `Serial`, `Fatura`, `Faturas`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(CompanyBillColumns.Value, `value`, `Value`, `Valor`, `Valores`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(CompanyBillColumns.Due, `due`, `Due`, `Vencimento`, `Vencimentos`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, true);
  }

  public Clone(): CompanyBillFilter
  {
    const clone: CompanyBillFilter = new CompanyBillFilter();
    clone.Id = this.Id;
    clone.Due = this.Due;
    clone.Value = this.Value;
    clone.Serial = this.Serial;
    clone.Billing = this.Billing;
    clone.CompanyId = this.CompanyId;

    clone.TableDefinition = this.TableDefinition;
    clone.ColumnDefinitions = this.ColumnDefinitions;

    clone.CustomProperties = this.CustomProperties;

    return clone;
  }
  public ToString(): string
  {
    return `null`;
  }

  public ToJson(): any
  {
    const json: any = {};
    if (!ObjectUtils.NullOrUndefined(this.Id))
      json['Id'] = this.Id;
    if (!ObjectUtils.NullOrUndefined(this.Value))
      json['Value'] = this.Value;
    if (!ObjectUtils.NullOrUndefined(this.Serial))
      json['Serial'] = this.Serial;
    if (!ObjectUtils.NullOrUndefined(this.Billing))
      json['Billing'] = this.Billing;
    if (!ObjectUtils.NullOrUndefined(this.Due))
      json['Due'] = DateUtils.ToDateServer(this.Due);
    if (!ObjectUtils.NullOrUndefined(this.CompanyId))
      json['CompanyId'] = Array.isArray(this.CompanyId) ? this.CompanyId : [this.CompanyId];


    return json;
  }
  public ParseToJson(): string
  {
    const json: any = this.ToJson();

    return JSON.stringify(json);
  }
  public ParseFromJson(json: Object): void
  {
    if (json)
    {
      this.Id = json['Id'];
      this.Value = json['Value'];
      this.Serial = json['Serial'];
      this.Billing = json['Billing'];
      this.CompanyId = json['CompanyId'];
      this.Due = DateUtils.FromDateServer(json['Due']);
    }
  }
}
