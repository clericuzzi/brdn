﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model';

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const CompanyTypeClass: string = `CompanyType`;
export const CompanyTypeTable: string = `company_type`;

/**
 * Column definitions for the `CompanyType` class
 */
export enum CompanyTypeColumns
{
  Id = 'Id',
  Type = 'Type',
}
export const CompanyTypeColumnsFilter: string[] = [];
export const CompanyTypeColumnsInsert: string[] = [CompanyTypeColumns.Type];
export const CompanyTypeColumnsUpdate: string[] = [CompanyTypeColumns.Type];

/**
 * Implementations of the `CompanyType` class
 */
export class CompanyType extends Crudable
{
  public static FromJson(json: any): CompanyType
  {
    const item: CompanyType = new CompanyType();
    if (json)
      item.ParseFromJson(json);

    return item;
  }

  constructor(
    public Id: number = null,
    public Type: string = null,
  )
  {
    super();
    this._InitDefinition();
  }
  private _InitDefinition(): void
  {
    this.SetTableDefinition(`CompanyType`, `company_type`, CrudableTableDefinitionGender.Male, `Company Type`, `Company Types`, true, true, true, true, true, true);

    this.SetColumnDefinition(CompanyTypeColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(CompanyTypeColumns.Type, `type`, `Type`, `Type`, `Types`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
  }

  public Clone(): CompanyType
  {
    const clone: CompanyType = new CompanyType();
    clone.Id = this.Id;
    clone.Type = this.Type;

    clone.TableDefinition = this.TableDefinition;
    clone.ColumnDefinitions = this.ColumnDefinitions;

    clone.CustomProperties = this.CustomProperties;

    return clone;
  }
  public ToString(): string
  {
    return `${this.Type}`;
  }

  public ToJson(navigationProperties: boolean = false): any
  {
    const json: any = {};
    if (!ObjectUtils.NullOrUndefined(this.Id))
      json['Id'] = this.Id;
    if (!ObjectUtils.NullOrUndefined(this.Type))
      json['Type'] = this.Type;
    if (navigationProperties)
    {
    }

    return json;
  }
  public ParseToJson(): string
  {
    const json: any = this.ToJson();

    return JSON.stringify(json);
  }
  public ParseFromJson(json: any): void
  {
    if (json)
    {
      this.Id = json['Id'];
      this.Type = json['Type'];
    }
  }
}

/**
 * Implementations of the `CompanyTypeFilter` class
 */
export class CompanyTypeFilter extends Crudable
{
  public static FromJson(json: Object): CompanyTypeFilter
  {
    const item: CompanyTypeFilter = new CompanyTypeFilter();
    if (json)
      item.ParseFromJson(json);

    return item;
  }

  constructor(
    public Id: number = null,
    public Type: string = null,
  )
  {
    super();
    this._InitDefinition();
  }
  private _InitDefinition(): void
  {
    this.SetTableDefinition(`CompanyTypeFilter`, `company_typeFilter`, CrudableTableDefinitionGender.Male, `Company Type`, `Company Types`, true, true, true, true, true, true);

    this.SetColumnDefinition(CompanyTypeColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(CompanyTypeColumns.Type, `type`, `Type`, `Type`, `Types`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
  }

  public Clone(): CompanyTypeFilter
  {
    const clone: CompanyTypeFilter = new CompanyTypeFilter();
    clone.Id = this.Id;
    clone.Type = this.Type;

    clone.TableDefinition = this.TableDefinition;
    clone.ColumnDefinitions = this.ColumnDefinitions;

    clone.CustomProperties = this.CustomProperties;

    return clone;
  }
  public ToString(): string
  {
    return `null`;
  }

  public ToJson(): any
  {
    const json: any = {};
    if (!ObjectUtils.NullOrUndefined(this.Id))
      json['Id'] = this.Id;
    if (!ObjectUtils.NullOrUndefined(this.Type))
      json['Type'] = this.Type;


    return json;
  }
  public ParseToJson(): string
  {
    const json: any = this.ToJson();

    return JSON.stringify(json);
  }
  public ParseFromJson(json: Object): void
  {
    if (json)
    {
      this.Id = json['Id'];
      this.Type = json['Type'];
    }
  }
}
