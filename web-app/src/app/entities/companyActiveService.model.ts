﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model';

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// Navigation Properties
import { Company } from './company.model';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const CompanyActiveServiceClass: string = `CompanyActiveService`;
export const CompanyActiveServiceTable: string = `company_active_service`;

/**
 * Column definitions for the `CompanyActiveService` class
 */
export enum CompanyActiveServiceColumns
{
	Id = 'Id',
	Tv = 'Tv',
	Qtd = 'Qtd',
	Rpon = 'Rpon',
	Order = 'Order',
	Offer = 'Offer',
	Status = 'Status',
	Invalid = 'Invalid',
	Benefit = 'Benefit',
	Product = 'Product',
	Instance = 'Instance',
	CompanyId = 'CompanyId',
	CreatedId = 'CreatedId',
	Ficticious = 'Ficticious',
	Portability = 'Portability',
	DoubleAccess = 'DoubleAccess',
	ChargeProfile = 'ChargeProfile',
	ServiceAddress = 'ServiceAddress',
	TecnologyVoice = 'TecnologyVoice',
	ActivationDate = 'ActivationDate',
	TecnologyAccess = 'TecnologyAccess',
	AssociatedProduct = 'AssociatedProduct',

	CompanyIdParent = 'CompanyIdParent',
}
export const CompanyActiveServiceColumnsFilter: string[] =  [  ];
export const CompanyActiveServiceColumnsInsert: string[] =  [ CompanyActiveServiceColumns.CompanyId, CompanyActiveServiceColumns.Instance, CompanyActiveServiceColumns.ServiceAddress, CompanyActiveServiceColumns.AssociatedProduct, CompanyActiveServiceColumns.Status, CompanyActiveServiceColumns.Order, CompanyActiveServiceColumns.Offer, CompanyActiveServiceColumns.Invalid, CompanyActiveServiceColumns.TecnologyVoice, CompanyActiveServiceColumns.TecnologyAccess, CompanyActiveServiceColumns.ChargeProfile, CompanyActiveServiceColumns.CreatedId, CompanyActiveServiceColumns.ActivationDate, CompanyActiveServiceColumns.Benefit, CompanyActiveServiceColumns.Portability, CompanyActiveServiceColumns.Rpon, CompanyActiveServiceColumns.Tv, CompanyActiveServiceColumns.Ficticious, CompanyActiveServiceColumns.DoubleAccess, CompanyActiveServiceColumns.Qtd, CompanyActiveServiceColumns.Product ];
export const CompanyActiveServiceColumnsUpdate: string[] =  [ CompanyActiveServiceColumns.CompanyId, CompanyActiveServiceColumns.Instance, CompanyActiveServiceColumns.ServiceAddress, CompanyActiveServiceColumns.AssociatedProduct, CompanyActiveServiceColumns.Status, CompanyActiveServiceColumns.Order, CompanyActiveServiceColumns.Offer, CompanyActiveServiceColumns.Invalid, CompanyActiveServiceColumns.TecnologyVoice, CompanyActiveServiceColumns.TecnologyAccess, CompanyActiveServiceColumns.ChargeProfile, CompanyActiveServiceColumns.CreatedId, CompanyActiveServiceColumns.ActivationDate, CompanyActiveServiceColumns.Benefit, CompanyActiveServiceColumns.Portability, CompanyActiveServiceColumns.Rpon, CompanyActiveServiceColumns.Tv, CompanyActiveServiceColumns.Ficticious, CompanyActiveServiceColumns.DoubleAccess, CompanyActiveServiceColumns.Qtd, CompanyActiveServiceColumns.Product ];

/**
 * Implementations of the `CompanyActiveService` class
 */
export class CompanyActiveService extends Crudable
{
	public CompanyIdParent: Company;

	public static FromJson(json: any): CompanyActiveService
	{
		const item: CompanyActiveService = new CompanyActiveService();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public CompanyId: number = null,
		public Instance: string = null,
		public ServiceAddress: string = null,
		public AssociatedProduct: string = null,
		public Status: string = null,
		public Order: string = null,
		public Offer: string = null,
		public Invalid: string = null,
		public TecnologyVoice: string = null,
		public TecnologyAccess: string = null,
		public ChargeProfile: string = null,
		public CreatedId: string = null,
		public ActivationDate: string = null,
		public Benefit: string = null,
		public Portability: string = null,
		public Rpon: string = null,
		public Tv: string = null,
		public Ficticious: string = null,
		public DoubleAccess: string = null,
		public Qtd: string = null,
		public Product: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`CompanyActiveService`, `company_active_service`, CrudableTableDefinitionGender.Male, `Company Active Service`, `Company Active Services`, true, true, true, true, true, true);

		this.SetColumnDefinition(CompanyActiveServiceColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyActiveServiceColumns.CompanyId, `company_id`, `CompanyId`, `Company Id`, `Company Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyActiveServiceColumns.Instance, `instance`, `Instance`, `Instance`, `Instances`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyActiveServiceColumns.ServiceAddress, `service_address`, `ServiceAddress`, `Service Address`, `Service Addresss`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyActiveServiceColumns.AssociatedProduct, `associated_product`, `AssociatedProduct`, `Associated Product`, `Associated Products`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyActiveServiceColumns.Status, `status`, `Status`, `Status`, `Statuss`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyActiveServiceColumns.Order, `order`, `Order`, `Order`, `Orders`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyActiveServiceColumns.Offer, `offer`, `Offer`, `Offer`, `Offers`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyActiveServiceColumns.Invalid, `invalid`, `Invalid`, `Invalid`, `Invalids`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyActiveServiceColumns.TecnologyVoice, `tecnology_voice`, `TecnologyVoice`, `Tecnology Voice`, `Tecnology Voices`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyActiveServiceColumns.TecnologyAccess, `tecnology_access`, `TecnologyAccess`, `Tecnology Access`, `Tecnology Accesss`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyActiveServiceColumns.ChargeProfile, `charge_profile`, `ChargeProfile`, `Charge Profile`, `Charge Profiles`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyActiveServiceColumns.CreatedId, `created_id`, `CreatedId`, `Created Id`, `Created Ids`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyActiveServiceColumns.ActivationDate, `activation_date`, `ActivationDate`, `Activation Date`, `Activation Dates`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyActiveServiceColumns.Benefit, `benefit`, `Benefit`, `Benefit`, `Benefits`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyActiveServiceColumns.Portability, `portability`, `Portability`, `Portability`, `Portabilitys`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyActiveServiceColumns.Rpon, `rpon`, `Rpon`, `Rpon`, `Rpons`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyActiveServiceColumns.Tv, `tv`, `Tv`, `Tv`, `Tvs`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyActiveServiceColumns.Ficticious, `ficticious`, `Ficticious`, `Ficticious`, `Ficticiouss`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyActiveServiceColumns.DoubleAccess, `double_access`, `DoubleAccess`, `Double Access`, `Double Accesss`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyActiveServiceColumns.Qtd, `qtd`, `Qtd`, `Qtd`, `Qtds`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyActiveServiceColumns.Product, `product`, `Product`, `Product`, `Products`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
	}

	public Clone(): CompanyActiveService
	{
		const clone: CompanyActiveService = new CompanyActiveService();
		clone.Id = this.Id;
		clone.Tv = this.Tv;
		clone.Qtd = this.Qtd;
		clone.Rpon = this.Rpon;
		clone.Order = this.Order;
		clone.Offer = this.Offer;
		clone.Status = this.Status;
		clone.Invalid = this.Invalid;
		clone.Benefit = this.Benefit;
		clone.Product = this.Product;
		clone.Instance = this.Instance;
		clone.CompanyId = this.CompanyId;
		clone.CreatedId = this.CreatedId;
		clone.Ficticious = this.Ficticious;
		clone.Portability = this.Portability;
		clone.DoubleAccess = this.DoubleAccess;
		clone.ChargeProfile = this.ChargeProfile;
		clone.ServiceAddress = this.ServiceAddress;
		clone.TecnologyVoice = this.TecnologyVoice;
		clone.ActivationDate = this.ActivationDate;
		clone.TecnologyAccess = this.TecnologyAccess;
		clone.AssociatedProduct = this.AssociatedProduct;

		clone.CompanyIdParent = this.CompanyIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.CompanyIdParent == null ? '' : this.CompanyIdParent.ToString()}`;
	}

	public ToJson(navigationProperties: boolean = false): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Tv))
			json['Tv'] = this.Tv;
		if (!ObjectUtils.NullOrUndefined(this.Qtd))
			json['Qtd'] = this.Qtd;
		if (!ObjectUtils.NullOrUndefined(this.Rpon))
			json['Rpon'] = this.Rpon;
		if (!ObjectUtils.NullOrUndefined(this.Order))
			json['Order'] = this.Order;
		if (!ObjectUtils.NullOrUndefined(this.Offer))
			json['Offer'] = this.Offer;
		if (!ObjectUtils.NullOrUndefined(this.Status))
			json['Status'] = this.Status;
		if (!ObjectUtils.NullOrUndefined(this.Invalid))
			json['Invalid'] = this.Invalid;
		if (!ObjectUtils.NullOrUndefined(this.Benefit))
			json['Benefit'] = this.Benefit;
		if (!ObjectUtils.NullOrUndefined(this.Product))
			json['Product'] = this.Product;
		if (!ObjectUtils.NullOrUndefined(this.Instance))
			json['Instance'] = this.Instance;
		if (!ObjectUtils.NullOrUndefined(this.CompanyId))
			json['CompanyId'] = this.CompanyId;
		if (!ObjectUtils.NullOrUndefined(this.CreatedId))
			json['CreatedId'] = this.CreatedId;
		if (!ObjectUtils.NullOrUndefined(this.Ficticious))
			json['Ficticious'] = this.Ficticious;
		if (!ObjectUtils.NullOrUndefined(this.Portability))
			json['Portability'] = this.Portability;
		if (!ObjectUtils.NullOrUndefined(this.DoubleAccess))
			json['DoubleAccess'] = this.DoubleAccess;
		if (!ObjectUtils.NullOrUndefined(this.ChargeProfile))
			json['ChargeProfile'] = this.ChargeProfile;
		if (!ObjectUtils.NullOrUndefined(this.ServiceAddress))
			json['ServiceAddress'] = this.ServiceAddress;
		if (!ObjectUtils.NullOrUndefined(this.TecnologyVoice))
			json['TecnologyVoice'] = this.TecnologyVoice;
		if (!ObjectUtils.NullOrUndefined(this.ActivationDate))
			json['ActivationDate'] = this.ActivationDate;
		if (!ObjectUtils.NullOrUndefined(this.TecnologyAccess))
			json['TecnologyAccess'] = this.TecnologyAccess;
		if (!ObjectUtils.NullOrUndefined(this.AssociatedProduct))
			json['AssociatedProduct'] = this.AssociatedProduct;
		if (navigationProperties)
		{
			if (!ObjectUtils.NullOrUndefined(this.CompanyIdParent))
				json['CompanyIdParent'] = this.CompanyIdParent.ToJson();
		}

		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: any): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Tv = json['Tv'];
			this.Qtd = json['Qtd'];
			this.Rpon = json['Rpon'];
			this.Order = json['Order'];
			this.Offer = json['Offer'];
			this.Status = json['Status'];
			this.Invalid = json['Invalid'];
			this.Benefit = json['Benefit'];
			this.Product = json['Product'];
			this.Instance = json['Instance'];
			this.CompanyId = json['CompanyId'];
			this.CreatedId = json['CreatedId'];
			this.Ficticious = json['Ficticious'];
			this.Portability = json['Portability'];
			this.DoubleAccess = json['DoubleAccess'];
			this.ChargeProfile = json['ChargeProfile'];
			this.ServiceAddress = json['ServiceAddress'];
			this.TecnologyVoice = json['TecnologyVoice'];
			this.ActivationDate = json['ActivationDate'];
			this.TecnologyAccess = json['TecnologyAccess'];
			this.AssociatedProduct = json['AssociatedProduct'];

			if (!ObjectUtils.NullOrUndefined(json['CompanyIdParent']))
			{
				this.CompanyIdParent = new Company();
				this.CompanyIdParent.ParseFromJson(json['CompanyIdParent']);
			}
		}
	}
}

/**
 * Implementations of the `CompanyActiveServiceFilter` class
 */
export class CompanyActiveServiceFilter extends Crudable
{
	public static FromJson(json: Object): CompanyActiveServiceFilter
	{
		const item: CompanyActiveServiceFilter = new CompanyActiveServiceFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public CompanyId: number[] = null,
		public Instance: string = null,
		public ServiceAddress: string = null,
		public AssociatedProduct: string = null,
		public Status: string = null,
		public Order: string = null,
		public Offer: string = null,
		public Invalid: string = null,
		public TecnologyVoice: string = null,
		public TecnologyAccess: string = null,
		public ChargeProfile: string = null,
		public CreatedId: string = null,
		public ActivationDate: string = null,
		public Benefit: string = null,
		public Portability: string = null,
		public Rpon: string = null,
		public Tv: string = null,
		public Ficticious: string = null,
		public DoubleAccess: string = null,
		public Qtd: string = null,
		public Product: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`CompanyActiveServiceFilter`, `company_active_serviceFilter`, CrudableTableDefinitionGender.Male, `Company Active Service`, `Company Active Services`, true, true, true, true, true, true);

		this.SetColumnDefinition(CompanyActiveServiceColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyActiveServiceColumns.CompanyId, `company_id`, `CompanyId`, `Company Id`, `Company Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyActiveServiceColumns.Instance, `instance`, `Instance`, `Instance`, `Instances`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyActiveServiceColumns.ServiceAddress, `service_address`, `ServiceAddress`, `Service Address`, `Service Addresss`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyActiveServiceColumns.AssociatedProduct, `associated_product`, `AssociatedProduct`, `Associated Product`, `Associated Products`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyActiveServiceColumns.Status, `status`, `Status`, `Status`, `Statuss`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyActiveServiceColumns.Order, `order`, `Order`, `Order`, `Orders`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyActiveServiceColumns.Offer, `offer`, `Offer`, `Offer`, `Offers`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyActiveServiceColumns.Invalid, `invalid`, `Invalid`, `Invalid`, `Invalids`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyActiveServiceColumns.TecnologyVoice, `tecnology_voice`, `TecnologyVoice`, `Tecnology Voice`, `Tecnology Voices`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyActiveServiceColumns.TecnologyAccess, `tecnology_access`, `TecnologyAccess`, `Tecnology Access`, `Tecnology Accesss`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyActiveServiceColumns.ChargeProfile, `charge_profile`, `ChargeProfile`, `Charge Profile`, `Charge Profiles`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyActiveServiceColumns.CreatedId, `created_id`, `CreatedId`, `Created Id`, `Created Ids`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyActiveServiceColumns.ActivationDate, `activation_date`, `ActivationDate`, `Activation Date`, `Activation Dates`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyActiveServiceColumns.Benefit, `benefit`, `Benefit`, `Benefit`, `Benefits`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyActiveServiceColumns.Portability, `portability`, `Portability`, `Portability`, `Portabilitys`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyActiveServiceColumns.Rpon, `rpon`, `Rpon`, `Rpon`, `Rpons`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyActiveServiceColumns.Tv, `tv`, `Tv`, `Tv`, `Tvs`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyActiveServiceColumns.Ficticious, `ficticious`, `Ficticious`, `Ficticious`, `Ficticiouss`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyActiveServiceColumns.DoubleAccess, `double_access`, `DoubleAccess`, `Double Access`, `Double Accesss`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyActiveServiceColumns.Qtd, `qtd`, `Qtd`, `Qtd`, `Qtds`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyActiveServiceColumns.Product, `product`, `Product`, `Product`, `Products`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
	}

	public Clone(): CompanyActiveServiceFilter
	{
		const clone: CompanyActiveServiceFilter = new CompanyActiveServiceFilter();
		clone.Id = this.Id;
		clone.Tv = this.Tv;
		clone.Qtd = this.Qtd;
		clone.Rpon = this.Rpon;
		clone.Order = this.Order;
		clone.Offer = this.Offer;
		clone.Status = this.Status;
		clone.Invalid = this.Invalid;
		clone.Benefit = this.Benefit;
		clone.Product = this.Product;
		clone.Instance = this.Instance;
		clone.CompanyId = this.CompanyId;
		clone.CreatedId = this.CreatedId;
		clone.Ficticious = this.Ficticious;
		clone.Portability = this.Portability;
		clone.DoubleAccess = this.DoubleAccess;
		clone.ChargeProfile = this.ChargeProfile;
		clone.ServiceAddress = this.ServiceAddress;
		clone.TecnologyVoice = this.TecnologyVoice;
		clone.ActivationDate = this.ActivationDate;
		clone.TecnologyAccess = this.TecnologyAccess;
		clone.AssociatedProduct = this.AssociatedProduct;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Tv))
			json['Tv'] = this.Tv;
		if (!ObjectUtils.NullOrUndefined(this.Qtd))
			json['Qtd'] = this.Qtd;
		if (!ObjectUtils.NullOrUndefined(this.Rpon))
			json['Rpon'] = this.Rpon;
		if (!ObjectUtils.NullOrUndefined(this.Order))
			json['Order'] = this.Order;
		if (!ObjectUtils.NullOrUndefined(this.Offer))
			json['Offer'] = this.Offer;
		if (!ObjectUtils.NullOrUndefined(this.Status))
			json['Status'] = this.Status;
		if (!ObjectUtils.NullOrUndefined(this.Invalid))
			json['Invalid'] = this.Invalid;
		if (!ObjectUtils.NullOrUndefined(this.Benefit))
			json['Benefit'] = this.Benefit;
		if (!ObjectUtils.NullOrUndefined(this.Product))
			json['Product'] = this.Product;
		if (!ObjectUtils.NullOrUndefined(this.Instance))
			json['Instance'] = this.Instance;
		if (!ObjectUtils.NullOrUndefined(this.CreatedId))
			json['CreatedId'] = this.CreatedId;
		if (!ObjectUtils.NullOrUndefined(this.Ficticious))
			json['Ficticious'] = this.Ficticious;
		if (!ObjectUtils.NullOrUndefined(this.Portability))
			json['Portability'] = this.Portability;
		if (!ObjectUtils.NullOrUndefined(this.DoubleAccess))
			json['DoubleAccess'] = this.DoubleAccess;
		if (!ObjectUtils.NullOrUndefined(this.ChargeProfile))
			json['ChargeProfile'] = this.ChargeProfile;
		if (!ObjectUtils.NullOrUndefined(this.ServiceAddress))
			json['ServiceAddress'] = this.ServiceAddress;
		if (!ObjectUtils.NullOrUndefined(this.TecnologyVoice))
			json['TecnologyVoice'] = this.TecnologyVoice;
		if (!ObjectUtils.NullOrUndefined(this.ActivationDate))
			json['ActivationDate'] = this.ActivationDate;
		if (!ObjectUtils.NullOrUndefined(this.TecnologyAccess))
			json['TecnologyAccess'] = this.TecnologyAccess;
		if (!ObjectUtils.NullOrUndefined(this.AssociatedProduct))
			json['AssociatedProduct'] = this.AssociatedProduct;
		if (!ObjectUtils.NullOrUndefined(this.CompanyId))
			json['CompanyId'] = Array.isArray(this.CompanyId) ? this.CompanyId : [ this.CompanyId ];


		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Tv = json['Tv'];
			this.Qtd = json['Qtd'];
			this.Rpon = json['Rpon'];
			this.Order = json['Order'];
			this.Offer = json['Offer'];
			this.Status = json['Status'];
			this.Invalid = json['Invalid'];
			this.Benefit = json['Benefit'];
			this.Product = json['Product'];
			this.Instance = json['Instance'];
			this.CompanyId = json['CompanyId'];
			this.CreatedId = json['CreatedId'];
			this.Ficticious = json['Ficticious'];
			this.Portability = json['Portability'];
			this.DoubleAccess = json['DoubleAccess'];
			this.ChargeProfile = json['ChargeProfile'];
			this.ServiceAddress = json['ServiceAddress'];
			this.TecnologyVoice = json['TecnologyVoice'];
			this.ActivationDate = json['ActivationDate'];
			this.TecnologyAccess = json['TecnologyAccess'];
			this.AssociatedProduct = json['AssociatedProduct'];
		}
	}
}
