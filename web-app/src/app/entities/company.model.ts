﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model';

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// Navigation Properties
import { Account } from 'clericuzzi-lib/entities/models/account.model';
import { User } from 'clericuzzi-lib/entities/models/user.model';
import { CompanySubtype } from './companySubtype.model';
import { CompanySituation } from './companySituation.model';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const CompanyClass: string = `Company`;
export const CompanyTable: string = `company`;

/**
 * Column definitions for the `Company` class
 */
export enum CompanyColumns
{
  Id = 'Id',
  Cnpj = 'Cnpj',
  Rank = 'Rank',
  Phone = 'Phone',
  UserId = 'UserId',
  Capital = 'Capital',
  Latitude = 'Latitude',
  AccountId = 'AccountId',
  Longitude = 'Longitude',
  Timestamp = 'Timestamp',
  FormalName = 'FormalName',
  WalletLand = 'WalletLand',
  DateOpened = 'DateOpened',
  Description = 'Description',
  FantasyName = 'FantasyName',
  SituationId = 'SituationId',
  WalletMobile = 'WalletMobile',
  SalesForceId = 'SalesForceId',
  DateLastUpdate = 'DateLastUpdate',
  CompanySubtypeId = 'CompanySubtypeId',
  ConvergingMessage = 'ConvergingMessage',

  UserIdParent = 'UserIdParent',
  AccountIdParent = 'AccountIdParent',
  SituationIdParent = 'SituationIdParent',
  CompanySubtypeIdParent = 'CompanySubtypeIdParent',
}
export const CompanyColumnsFilter: string[] = [CompanyColumns.FantasyName, CompanyColumns.Cnpj];
export const CompanyColumnsInsert: string[] = [CompanyColumns.Cnpj, CompanyColumns.Phone, CompanyColumns.Description, CompanyColumns.FormalName, CompanyColumns.FantasyName];
export const CompanyColumnsUpdate: string[] = [CompanyColumns.Cnpj, CompanyColumns.Phone, CompanyColumns.Description, CompanyColumns.FormalName, CompanyColumns.FantasyName];

/**
 * Implementations of the `Company` class
 */
export class Company extends Crudable
{
  public AccountIdParent: Account;
  public UserIdParent: User;
  public CompanySubtypeIdParent: CompanySubtype;
  public SituationIdParent: CompanySituation;

  public static FromJson(json: any): Company
  {
    const item: Company = new Company();
    if (json)
      item.ParseFromJson(json);

    return item;
  }

  constructor(
    public Id: number = null,
    public AccountId: number = null,
    public UserId: number = null,
    public CompanySubtypeId: number = null,
    public Cnpj: string = null,
    public Phone: string = null,
    public Description: string = null,
    public FormalName: string = null,
    public FantasyName: string = null,
    public Latitude: number = null,
    public Longitude: number = null,
    public Timestamp: Date = null,
    public Rank: string = null,
    public ConvergingMessage: string = null,
    public WalletMobile: string = null,
    public WalletLand: string = null,
    public Capital: number = null,
    public DateOpened: Date = null,
    public SituationId: number = null,
    public SalesForceId: string = null,
    public DateLastUpdate: Date = null,
  )
  {
    super();
    this._InitDefinition();
  }
  private _InitDefinition(): void
  {
    this.SetTableDefinition(`Company`, `company`, CrudableTableDefinitionGender.Female, `Empresa`, `Empresas`, true, true, true, true, true, true);

    this.SetColumnDefinition(CompanyColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(CompanyColumns.AccountId, `account_id`, `AccountId`, `Conta`, `Contas`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(CompanyColumns.UserId, `user_id`, `UserId`, `User Id`, `User Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(CompanyColumns.CompanySubtypeId, `company_subtype_id`, `CompanySubtypeId`, `Sub-ramo`, `Sub-ramos`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(CompanyColumns.Cnpj, `cnpj`, `Cnpj`, `Cnpj`, `Cnpjs`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(CompanyColumns.Phone, `phone`, `Phone`, `Telefone`, `Telefones`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(CompanyColumns.Description, `description`, `Description`, `Descrição`, `Descrições`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(CompanyColumns.FormalName, `formal_name`, `FormalName`, `Razão Social`, `Razões Sociais`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(CompanyColumns.FantasyName, `fantasy_name`, `FantasyName`, `Nome Fantasia`, `Nomes Fantasia`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(CompanyColumns.Latitude, `latitude`, `Latitude`, `Latitude`, `Latitudes`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(CompanyColumns.Longitude, `longitude`, `Longitude`, `Longitude`, `Longitudes`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(CompanyColumns.Timestamp, `timestamp`, `Timestamp`, `Registro`, `Registros`, ComponentTypeEnum.Date, true, true, false, true, false, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
    this.SetColumnDefinition(CompanyColumns.Rank, `rank`, `Rank`, `Rank`, `Ranks`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(CompanyColumns.ConvergingMessage, `converging_message`, `ConvergingMessage`, `Converging Message`, `Converging Messages`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(CompanyColumns.WalletMobile, `wallet_mobile`, `WalletMobile`, `Wallet Mobile`, `Wallet Mobiles`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(CompanyColumns.WalletLand, `wallet_land`, `WalletLand`, `Wallet Land`, `Wallet Lands`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(CompanyColumns.Capital, `capital`, `Capital`, `Capital`, `Capitals`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(CompanyColumns.DateOpened, `date_opened`, `DateOpened`, `Date Opened`, `Date Openeds`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, true);
    this.SetColumnDefinition(CompanyColumns.SituationId, `situation_id`, `SituationId`, `Situation Id`, `Situation Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(CompanyColumns.SalesForceId, `sales_force_id`, `SalesForceId`, `Sales Force Id`, `Sales Force Ids`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(CompanyColumns.DateLastUpdate, `date_last_update`, `DateLastUpdate`, `Date Last Update`, `Date Last Updates`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, true);
  }

  public Clone(): Company
  {
    const clone: Company = new Company();
    clone.Id = this.Id;
    clone.Cnpj = this.Cnpj;
    clone.Rank = this.Rank;
    clone.Phone = this.Phone;
    clone.UserId = this.UserId;
    clone.Capital = this.Capital;
    clone.Latitude = this.Latitude;
    clone.AccountId = this.AccountId;
    clone.Longitude = this.Longitude;
    clone.Timestamp = this.Timestamp;
    clone.FormalName = this.FormalName;
    clone.WalletLand = this.WalletLand;
    clone.DateOpened = this.DateOpened;
    clone.Description = this.Description;
    clone.FantasyName = this.FantasyName;
    clone.SituationId = this.SituationId;
    clone.WalletMobile = this.WalletMobile;
    clone.SalesForceId = this.SalesForceId;
    clone.DateLastUpdate = this.DateLastUpdate;
    clone.CompanySubtypeId = this.CompanySubtypeId;
    clone.ConvergingMessage = this.ConvergingMessage;

    clone.UserIdParent = this.UserIdParent;
    clone.AccountIdParent = this.AccountIdParent;
    clone.SituationIdParent = this.SituationIdParent;
    clone.CompanySubtypeIdParent = this.CompanySubtypeIdParent;

    clone.TableDefinition = this.TableDefinition;
    clone.ColumnDefinitions = this.ColumnDefinitions;

    clone.CustomProperties = this.CustomProperties;

    return clone;
  }
  public ToString(): string
  {
    return `${this.AccountIdParent == null ? '' : this.AccountIdParent.ToString()} ${this.Cnpj}`;
  }

  public ToJson(navigationProperties: boolean = false): any
  {
    const json: any = {};
    if (!ObjectUtils.NullOrUndefined(this.Id))
      json['Id'] = this.Id;
    if (!ObjectUtils.NullOrUndefined(this.Cnpj))
      json['Cnpj'] = this.Cnpj;
    if (!ObjectUtils.NullOrUndefined(this.Rank))
      json['Rank'] = this.Rank;
    if (!ObjectUtils.NullOrUndefined(this.Phone))
      json['Phone'] = this.Phone;
    if (!ObjectUtils.NullOrUndefined(this.UserId))
      json['UserId'] = this.UserId;
    if (!ObjectUtils.NullOrUndefined(this.Capital))
      json['Capital'] = this.Capital;
    if (!ObjectUtils.NullOrUndefined(this.Latitude))
      json['Latitude'] = this.Latitude;
    if (!ObjectUtils.NullOrUndefined(this.AccountId))
      json['AccountId'] = this.AccountId;
    if (!ObjectUtils.NullOrUndefined(this.Longitude))
      json['Longitude'] = this.Longitude;
    if (!ObjectUtils.NullOrUndefined(this.FormalName))
      json['FormalName'] = this.FormalName;
    if (!ObjectUtils.NullOrUndefined(this.WalletLand))
      json['WalletLand'] = this.WalletLand;
    if (!ObjectUtils.NullOrUndefined(this.Description))
      json['Description'] = this.Description;
    if (!ObjectUtils.NullOrUndefined(this.FantasyName))
      json['FantasyName'] = this.FantasyName;
    if (!ObjectUtils.NullOrUndefined(this.SituationId))
      json['SituationId'] = this.SituationId;
    if (!ObjectUtils.NullOrUndefined(this.WalletMobile))
      json['WalletMobile'] = this.WalletMobile;
    if (!ObjectUtils.NullOrUndefined(this.SalesForceId))
      json['SalesForceId'] = this.SalesForceId;
    if (!ObjectUtils.NullOrUndefined(this.CompanySubtypeId))
      json['CompanySubtypeId'] = this.CompanySubtypeId;
    if (!ObjectUtils.NullOrUndefined(this.Timestamp))
      json['Timestamp'] = DateUtils.ToDateServer(this.Timestamp);
    if (!ObjectUtils.NullOrUndefined(this.ConvergingMessage))
      json['ConvergingMessage'] = this.ConvergingMessage;
    if (!ObjectUtils.NullOrUndefined(this.DateOpened))
      json['DateOpened'] = DateUtils.ToDateServer(this.DateOpened);
    if (!ObjectUtils.NullOrUndefined(this.DateLastUpdate))
      json['DateLastUpdate'] = DateUtils.ToDateServer(this.DateLastUpdate);
    if (navigationProperties)
    {
      if (!ObjectUtils.NullOrUndefined(this.AccountIdParent))
        json['AccountIdParent'] = this.AccountIdParent.ToJson();
      if (!ObjectUtils.NullOrUndefined(this.UserIdParent))
        json['UserIdParent'] = this.UserIdParent.ToJson();
      if (!ObjectUtils.NullOrUndefined(this.CompanySubtypeIdParent))
        json['CompanySubtypeIdParent'] = this.CompanySubtypeIdParent.ToJson();
      if (!ObjectUtils.NullOrUndefined(this.SituationIdParent))
        json['SituationIdParent'] = this.SituationIdParent.ToJson();
    }

    return json;
  }
  public ParseToJson(): string
  {
    const json: any = this.ToJson();

    return JSON.stringify(json);
  }
  public ParseFromJson(json: any): void
  {
    if (json)
    {
      this.Id = json['Id'];
      this.Cnpj = json['Cnpj'];
      this.Rank = json['Rank'];
      this.Phone = json['Phone'];
      this.UserId = json['UserId'];
      this.Capital = json['Capital'];
      this.Latitude = json['Latitude'];
      this.AccountId = json['AccountId'];
      this.Longitude = json['Longitude'];
      this.FormalName = json['FormalName'];
      this.WalletLand = json['WalletLand'];
      this.Description = json['Description'];
      this.FantasyName = json['FantasyName'];
      this.SituationId = json['SituationId'];
      this.WalletMobile = json['WalletMobile'];
      this.SalesForceId = json['SalesForceId'];
      this.CompanySubtypeId = json['CompanySubtypeId'];
      this.ConvergingMessage = json['ConvergingMessage'];
      this.Timestamp = DateUtils.FromDateServer(json['Timestamp']);
      this.DateOpened = DateUtils.FromDateServer(json['DateOpened']);
      this.DateLastUpdate = DateUtils.FromDateServer(json['DateLastUpdate']);

      if (!ObjectUtils.NullOrUndefined(json['AccountIdParent']))
      {
        this.AccountIdParent = new Account();
        this.AccountIdParent.ParseFromJson(json['AccountIdParent']);
      }
      if (!ObjectUtils.NullOrUndefined(json['UserIdParent']))
      {
        this.UserIdParent = new User();
        this.UserIdParent.ParseFromJson(json['UserIdParent']);
      }
      if (!ObjectUtils.NullOrUndefined(json['CompanySubtypeIdParent']))
      {
        this.CompanySubtypeIdParent = new CompanySubtype();
        this.CompanySubtypeIdParent.ParseFromJson(json['CompanySubtypeIdParent']);
      }
      if (!ObjectUtils.NullOrUndefined(json['SituationIdParent']))
      {
        this.SituationIdParent = new CompanySituation();
        this.SituationIdParent.ParseFromJson(json['SituationIdParent']);
      }
    }
  }
}

/**
 * Implementations of the `CompanyFilter` class
 */
export class CompanyFilter extends Crudable
{
  public static FromJson(json: Object): CompanyFilter
  {
    const item: CompanyFilter = new CompanyFilter();
    if (json)
      item.ParseFromJson(json);

    return item;
  }

  constructor(
    public Id: number = null,
    public AccountId: number[] = null,
    public UserId: number[] = null,
    public CompanySubtypeId: number[] = null,
    public Cnpj: string = null,
    public Phone: string = null,
    public Description: string = null,
    public FormalName: string = null,
    public FantasyName: string = null,
    public Latitude: number = null,
    public Longitude: number = null,
    public Timestamp: Date = null,
    public Rank: string = null,
    public ConvergingMessage: string = null,
    public WalletMobile: string = null,
    public WalletLand: string = null,
    public Capital: number = null,
    public DateOpened: Date = null,
    public SituationId: number[] = null,
    public SalesForceId: string = null,
    public DateLastUpdate: Date = null,
  )
  {
    super();
    this._InitDefinition();
  }
  private _InitDefinition(): void
  {
    this.SetTableDefinition(`CompanyFilter`, `companyFilter`, CrudableTableDefinitionGender.Female, `Empresa`, `Empresas`, true, true, true, true, true, true);

    this.SetColumnDefinition(CompanyColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(CompanyColumns.AccountId, `account_id`, `AccountId`, `Conta`, `Contas`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(CompanyColumns.UserId, `user_id`, `UserId`, `User Id`, `User Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(CompanyColumns.CompanySubtypeId, `company_subtype_id`, `CompanySubtypeId`, `Sub-ramo`, `Sub-ramos`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(CompanyColumns.Cnpj, `cnpj`, `Cnpj`, `Cnpj`, `Cnpjs`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(CompanyColumns.Phone, `phone`, `Phone`, `Telefone`, `Telefones`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(CompanyColumns.Description, `description`, `Description`, `Descrição`, `Descrições`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(CompanyColumns.FormalName, `formal_name`, `FormalName`, `Razão Social`, `Razões Sociais`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(CompanyColumns.FantasyName, `fantasy_name`, `FantasyName`, `Nome Fantasia`, `Nomes Fantasia`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(CompanyColumns.Latitude, `latitude`, `Latitude`, `Latitude`, `Latitudes`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(CompanyColumns.Longitude, `longitude`, `Longitude`, `Longitude`, `Longitudes`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(CompanyColumns.Timestamp, `timestamp`, `Timestamp`, `Registro`, `Registros`, ComponentTypeEnum.Date, true, true, false, true, false, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
    this.SetColumnDefinition(CompanyColumns.Rank, `rank`, `Rank`, `Rank`, `Ranks`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(CompanyColumns.ConvergingMessage, `converging_message`, `ConvergingMessage`, `Converging Message`, `Converging Messages`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(CompanyColumns.WalletMobile, `wallet_mobile`, `WalletMobile`, `Wallet Mobile`, `Wallet Mobiles`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(CompanyColumns.WalletLand, `wallet_land`, `WalletLand`, `Wallet Land`, `Wallet Lands`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(CompanyColumns.Capital, `capital`, `Capital`, `Capital`, `Capitals`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(CompanyColumns.DateOpened, `date_opened`, `DateOpened`, `Date Opened`, `Date Openeds`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, true);
    this.SetColumnDefinition(CompanyColumns.SituationId, `situation_id`, `SituationId`, `Situation Id`, `Situation Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(CompanyColumns.SalesForceId, `sales_force_id`, `SalesForceId`, `Sales Force Id`, `Sales Force Ids`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(CompanyColumns.DateLastUpdate, `date_last_update`, `DateLastUpdate`, `Date Last Update`, `Date Last Updates`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, true);
  }

  public Clone(): CompanyFilter
  {
    const clone: CompanyFilter = new CompanyFilter();
    clone.Id = this.Id;
    clone.Cnpj = this.Cnpj;
    clone.Rank = this.Rank;
    clone.Phone = this.Phone;
    clone.UserId = this.UserId;
    clone.Capital = this.Capital;
    clone.Latitude = this.Latitude;
    clone.AccountId = this.AccountId;
    clone.Longitude = this.Longitude;
    clone.Timestamp = this.Timestamp;
    clone.FormalName = this.FormalName;
    clone.WalletLand = this.WalletLand;
    clone.DateOpened = this.DateOpened;
    clone.Description = this.Description;
    clone.FantasyName = this.FantasyName;
    clone.SituationId = this.SituationId;
    clone.WalletMobile = this.WalletMobile;
    clone.SalesForceId = this.SalesForceId;
    clone.DateLastUpdate = this.DateLastUpdate;
    clone.CompanySubtypeId = this.CompanySubtypeId;
    clone.ConvergingMessage = this.ConvergingMessage;

    clone.TableDefinition = this.TableDefinition;
    clone.ColumnDefinitions = this.ColumnDefinitions;

    clone.CustomProperties = this.CustomProperties;

    return clone;
  }
  public ToString(): string
  {
    return `null`;
  }

  public ToJson(): any
  {
    const json: any = {};
    if (!ObjectUtils.NullOrUndefined(this.Id))
      json['Id'] = this.Id;
    if (!ObjectUtils.NullOrUndefined(this.Cnpj))
      json['Cnpj'] = this.Cnpj;
    if (!ObjectUtils.NullOrUndefined(this.Rank))
      json['Rank'] = this.Rank;
    if (!ObjectUtils.NullOrUndefined(this.Phone))
      json['Phone'] = this.Phone;
    if (!ObjectUtils.NullOrUndefined(this.Capital))
      json['Capital'] = this.Capital;
    if (!ObjectUtils.NullOrUndefined(this.Latitude))
      json['Latitude'] = this.Latitude;
    if (!ObjectUtils.NullOrUndefined(this.Longitude))
      json['Longitude'] = this.Longitude;
    if (!ObjectUtils.NullOrUndefined(this.FormalName))
      json['FormalName'] = this.FormalName;
    if (!ObjectUtils.NullOrUndefined(this.WalletLand))
      json['WalletLand'] = this.WalletLand;
    if (!ObjectUtils.NullOrUndefined(this.Description))
      json['Description'] = this.Description;
    if (!ObjectUtils.NullOrUndefined(this.FantasyName))
      json['FantasyName'] = this.FantasyName;
    if (!ObjectUtils.NullOrUndefined(this.WalletMobile))
      json['WalletMobile'] = this.WalletMobile;
    if (!ObjectUtils.NullOrUndefined(this.SalesForceId))
      json['SalesForceId'] = this.SalesForceId;
    if (!ObjectUtils.NullOrUndefined(this.Timestamp))
      json['Timestamp'] = DateUtils.ToDateServer(this.Timestamp);
    if (!ObjectUtils.NullOrUndefined(this.ConvergingMessage))
      json['ConvergingMessage'] = this.ConvergingMessage;
    if (!ObjectUtils.NullOrUndefined(this.DateOpened))
      json['DateOpened'] = DateUtils.ToDateServer(this.DateOpened);
    if (!ObjectUtils.NullOrUndefined(this.UserId))
      json['UserId'] = Array.isArray(this.UserId) ? this.UserId : [this.UserId];
    if (!ObjectUtils.NullOrUndefined(this.DateLastUpdate))
      json['DateLastUpdate'] = DateUtils.ToDateServer(this.DateLastUpdate);
    if (!ObjectUtils.NullOrUndefined(this.AccountId))
      json['AccountId'] = Array.isArray(this.AccountId) ? this.AccountId : [this.AccountId];
    if (!ObjectUtils.NullOrUndefined(this.SituationId))
      json['SituationId'] = Array.isArray(this.SituationId) ? this.SituationId : [this.SituationId];
    if (!ObjectUtils.NullOrUndefined(this.CompanySubtypeId))
      json['CompanySubtypeId'] = Array.isArray(this.CompanySubtypeId) ? this.CompanySubtypeId : [this.CompanySubtypeId];


    return json;
  }
  public ParseToJson(): string
  {
    const json: any = this.ToJson();

    return JSON.stringify(json);
  }
  public ParseFromJson(json: Object): void
  {
    if (json)
    {
      this.Id = json['Id'];
      this.Cnpj = json['Cnpj'];
      this.Rank = json['Rank'];
      this.Phone = json['Phone'];
      this.UserId = json['UserId'];
      this.Capital = json['Capital'];
      this.Latitude = json['Latitude'];
      this.AccountId = json['AccountId'];
      this.Longitude = json['Longitude'];
      this.FormalName = json['FormalName'];
      this.WalletLand = json['WalletLand'];
      this.Description = json['Description'];
      this.FantasyName = json['FantasyName'];
      this.SituationId = json['SituationId'];
      this.WalletMobile = json['WalletMobile'];
      this.SalesForceId = json['SalesForceId'];
      this.CompanySubtypeId = json['CompanySubtypeId'];
      this.ConvergingMessage = json['ConvergingMessage'];
      this.Timestamp = DateUtils.FromDateServer(json['Timestamp']);
      this.DateOpened = DateUtils.FromDateServer(json['DateOpened']);
      this.DateLastUpdate = DateUtils.FromDateServer(json['DateLastUpdate']);
    }
  }
}
