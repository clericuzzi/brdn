﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model';

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// Navigation Properties
import { CompanyContact } from './companyContact.model';
import { CompanyLine } from './companyLine.model';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const CompanyContactLineClass: string = `CompanyContactLine`;
export const CompanyContactLineTable: string = `company_contact_line`;

/**
 * Column definitions for the `CompanyContactLine` class
 */
export enum CompanyContactLineColumns
{
  Id = 'Id',
  CompanyLineId = 'CompanyLineId',
  CompanyContactId = 'CompanyContactId',

  CompanyLineIdParent = 'CompanyLineIdParent',
  CompanyContactIdParent = 'CompanyContactIdParent',
}
export const CompanyContactLineColumnsFilter: string[] = [];
export const CompanyContactLineColumnsInsert: string[] = [CompanyContactLineColumns.CompanyContactId, CompanyContactLineColumns.CompanyLineId];
export const CompanyContactLineColumnsUpdate: string[] = [CompanyContactLineColumns.CompanyContactId, CompanyContactLineColumns.CompanyLineId];

/**
 * Implementations of the `CompanyContactLine` class
 */
export class CompanyContactLine extends Crudable
{
  public CompanyContactIdParent: CompanyContact;
  public CompanyLineIdParent: CompanyLine;

  public static FromJson(json: any): CompanyContactLine
  {
    const item: CompanyContactLine = new CompanyContactLine();
    if (json)
      item.ParseFromJson(json);

    return item;
  }

  constructor(
    public Id: number = null,
    public CompanyContactId: number = null,
    public CompanyLineId: number = null,
  )
  {
    super();
    this._InitDefinition();
  }
  private _InitDefinition(): void
  {
    this.SetTableDefinition(`CompanyContactLine`, `company_contact_line`, CrudableTableDefinitionGender.Male, `Número de Contato`, `Números de Contato`, true, true, true, true, true, true);

    this.SetColumnDefinition(CompanyContactLineColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(CompanyContactLineColumns.CompanyContactId, `company_contact_id`, `CompanyContactId`, `Contato`, `Contato`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(CompanyContactLineColumns.CompanyLineId, `company_line_id`, `CompanyLineId`, `N°`, `N°s`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
  }

  public Clone(): CompanyContactLine
  {
    const clone: CompanyContactLine = new CompanyContactLine();
    clone.Id = this.Id;
    clone.CompanyLineId = this.CompanyLineId;
    clone.CompanyContactId = this.CompanyContactId;

    clone.CompanyLineIdParent = this.CompanyLineIdParent;
    clone.CompanyContactIdParent = this.CompanyContactIdParent;

    clone.TableDefinition = this.TableDefinition;
    clone.ColumnDefinitions = this.ColumnDefinitions;

    clone.CustomProperties = this.CustomProperties;

    return clone;
  }
  public ToString(): string
  {
    return `${this.CompanyContactIdParent == null ? '' : this.CompanyContactIdParent.ToString()}`;
  }

  public ToJson(navigationProperties: boolean = false): any
  {
    const json: any = {};
    if (!ObjectUtils.NullOrUndefined(this.Id))
      json['Id'] = this.Id;
    if (!ObjectUtils.NullOrUndefined(this.CompanyLineId))
      json['CompanyLineId'] = this.CompanyLineId;
    if (!ObjectUtils.NullOrUndefined(this.CompanyContactId))
      json['CompanyContactId'] = this.CompanyContactId;
    if (navigationProperties)
    {
      if (!ObjectUtils.NullOrUndefined(this.CompanyContactIdParent))
        json['CompanyContactIdParent'] = this.CompanyContactIdParent.ToJson();
      if (!ObjectUtils.NullOrUndefined(this.CompanyLineIdParent))
        json['CompanyLineIdParent'] = this.CompanyLineIdParent.ToJson();
    }

    return json;
  }
  public ParseToJson(): string
  {
    const json: any = this.ToJson();

    return JSON.stringify(json);
  }
  public ParseFromJson(json: any): void
  {
    if (json)
    {
      this.Id = json['Id'];
      this.CompanyLineId = json['CompanyLineId'];
      this.CompanyContactId = json['CompanyContactId'];

      if (!ObjectUtils.NullOrUndefined(json['CompanyContactIdParent']))
      {
        this.CompanyContactIdParent = new CompanyContact();
        this.CompanyContactIdParent.ParseFromJson(json['CompanyContactIdParent']);
      }
      if (!ObjectUtils.NullOrUndefined(json['CompanyLineIdParent']))
      {
        this.CompanyLineIdParent = new CompanyLine();
        this.CompanyLineIdParent.ParseFromJson(json['CompanyLineIdParent']);
      }
    }
  }
}

/**
 * Implementations of the `CompanyContactLineFilter` class
 */
export class CompanyContactLineFilter extends Crudable
{
  public static FromJson(json: Object): CompanyContactLineFilter
  {
    const item: CompanyContactLineFilter = new CompanyContactLineFilter();
    if (json)
      item.ParseFromJson(json);

    return item;
  }

  constructor(
    public Id: number = null,
    public CompanyContactId: number[] = null,
    public CompanyLineId: number[] = null,
  )
  {
    super();
    this._InitDefinition();
  }
  private _InitDefinition(): void
  {
    this.SetTableDefinition(`CompanyContactLineFilter`, `company_contact_lineFilter`, CrudableTableDefinitionGender.Male, `Company Contact Line`, `Company Contact Lines`, true, true, true, true, true, true);

    this.SetColumnDefinition(CompanyContactLineColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(CompanyContactLineColumns.CompanyContactId, `company_contact_id`, `CompanyContactId`, `Company Contact Id`, `Company Contact Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(CompanyContactLineColumns.CompanyLineId, `company_line_id`, `CompanyLineId`, `Company Line Id`, `Company Line Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
  }

  public Clone(): CompanyContactLineFilter
  {
    const clone: CompanyContactLineFilter = new CompanyContactLineFilter();
    clone.Id = this.Id;
    clone.CompanyLineId = this.CompanyLineId;
    clone.CompanyContactId = this.CompanyContactId;

    clone.TableDefinition = this.TableDefinition;
    clone.ColumnDefinitions = this.ColumnDefinitions;

    clone.CustomProperties = this.CustomProperties;

    return clone;
  }
  public ToString(): string
  {
    return `null`;
  }

  public ToJson(): any
  {
    const json: any = {};
    if (!ObjectUtils.NullOrUndefined(this.Id))
      json['Id'] = this.Id;
    if (!ObjectUtils.NullOrUndefined(this.CompanyLineId))
      json['CompanyLineId'] = Array.isArray(this.CompanyLineId) ? this.CompanyLineId : [this.CompanyLineId];
    if (!ObjectUtils.NullOrUndefined(this.CompanyContactId))
      json['CompanyContactId'] = Array.isArray(this.CompanyContactId) ? this.CompanyContactId : [this.CompanyContactId];


    return json;
  }
  public ParseToJson(): string
  {
    const json: any = this.ToJson();

    return JSON.stringify(json);
  }
  public ParseFromJson(json: Object): void
  {
    if (json)
    {
      this.Id = json['Id'];
      this.CompanyLineId = json['CompanyLineId'];
      this.CompanyContactId = json['CompanyContactId'];
    }
  }
}
