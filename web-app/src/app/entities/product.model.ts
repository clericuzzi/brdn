﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model';

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// Navigation Properties
import { Account } from 'clericuzzi-lib/entities/models/account.model';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const ProductClass: string = `Product`;
export const ProductTable: string = `product`;

/**
 * Column definitions for the `Product` class
 */
export enum ProductColumns
{
  Id = 'Id',
  Name = 'Name',
  AccountId = 'AccountId',

  AccountIdParent = 'AccountIdParent',
}
export const ProductColumnsFilter: string[] = [];
export const ProductColumnsInsert: string[] = [ProductColumns.AccountId, ProductColumns.Name];
export const ProductColumnsUpdate: string[] = [ProductColumns.AccountId, ProductColumns.Name];

/**
 * Implementations of the `Product` class
 */
export class Product extends Crudable
{
  public AccountIdParent: Account;

  public static FromJson(json: any): Product
  {
    const item: Product = new Product();
    if (json)
      item.ParseFromJson(json);

    return item;
  }

  constructor(
    public Id: number = null,
    public AccountId: number = null,
    public Name: string = null,
  )
  {
    super();
    this._InitDefinition();
  }
  private _InitDefinition(): void
  {
    this.SetTableDefinition(`Product`, `product`, CrudableTableDefinitionGender.Male, `Product`, `Products`, true, true, true, true, true, true);

    this.SetColumnDefinition(ProductColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(ProductColumns.AccountId, `account_id`, `AccountId`, `Account Id`, `Account Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(ProductColumns.Name, `name`, `Name`, `Name`, `Names`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
  }

  public Clone(): Product
  {
    const clone: Product = new Product();
    clone.Id = this.Id;
    clone.Name = this.Name;
    clone.AccountId = this.AccountId;

    clone.AccountIdParent = this.AccountIdParent;

    clone.TableDefinition = this.TableDefinition;
    clone.ColumnDefinitions = this.ColumnDefinitions;

    clone.CustomProperties = this.CustomProperties;

    return clone;
  }
  public ToString(): string
  {
    return `${this.AccountIdParent == null ? '' : this.AccountIdParent.ToString()}`;
  }

  public ToJson(navigationProperties: boolean = false): any
  {
    const json: any = {};
    if (!ObjectUtils.NullOrUndefined(this.Id))
      json['Id'] = this.Id;
    if (!ObjectUtils.NullOrUndefined(this.Name))
      json['Name'] = this.Name;
    if (!ObjectUtils.NullOrUndefined(this.AccountId))
      json['AccountId'] = this.AccountId;
    if (navigationProperties)
    {
      if (!ObjectUtils.NullOrUndefined(this.AccountIdParent))
        json['AccountIdParent'] = this.AccountIdParent.ToJson();
    }

    return json;
  }
  public ParseToJson(): string
  {
    const json: any = this.ToJson();

    return JSON.stringify(json);
  }
  public ParseFromJson(json: any): void
  {
    if (json)
    {
      this.Id = json['Id'];
      this.Name = json['Name'];
      this.AccountId = json['AccountId'];

      if (!ObjectUtils.NullOrUndefined(json['AccountIdParent']))
      {
        this.AccountIdParent = new Account();
        this.AccountIdParent.ParseFromJson(json['AccountIdParent']);
      }
    }
  }
}

/**
 * Implementations of the `ProductFilter` class
 */
export class ProductFilter extends Crudable
{
  public static FromJson(json: Object): ProductFilter
  {
    const item: ProductFilter = new ProductFilter();
    if (json)
      item.ParseFromJson(json);

    return item;
  }

  constructor(
    public Id: number = null,
    public AccountId: number[] = null,
    public Name: string = null,
  )
  {
    super();
    this._InitDefinition();
  }
  private _InitDefinition(): void
  {
    this.SetTableDefinition(`ProductFilter`, `productFilter`, CrudableTableDefinitionGender.Male, `Product`, `Products`, true, true, true, true, true, true);

    this.SetColumnDefinition(ProductColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(ProductColumns.AccountId, `account_id`, `AccountId`, `Account Id`, `Account Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(ProductColumns.Name, `name`, `Name`, `Name`, `Names`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
  }

  public Clone(): ProductFilter
  {
    const clone: ProductFilter = new ProductFilter();
    clone.Id = this.Id;
    clone.Name = this.Name;
    clone.AccountId = this.AccountId;

    clone.TableDefinition = this.TableDefinition;
    clone.ColumnDefinitions = this.ColumnDefinitions;

    clone.CustomProperties = this.CustomProperties;

    return clone;
  }
  public ToString(): string
  {
    return `null`;
  }

  public ToJson(): any
  {
    const json: any = {};
    if (!ObjectUtils.NullOrUndefined(this.Id))
      json['Id'] = this.Id;
    if (!ObjectUtils.NullOrUndefined(this.Name))
      json['Name'] = this.Name;
    if (!ObjectUtils.NullOrUndefined(this.AccountId))
      json['AccountId'] = Array.isArray(this.AccountId) ? this.AccountId : [this.AccountId];


    return json;
  }
  public ParseToJson(): string
  {
    const json: any = this.ToJson();

    return JSON.stringify(json);
  }
  public ParseFromJson(json: Object): void
  {
    if (json)
    {
      this.Id = json['Id'];
      this.Name = json['Name'];
      this.AccountId = json['AccountId'];
    }
  }
}
