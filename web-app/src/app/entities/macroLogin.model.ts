﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model';
// utils
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const MacroLoginClass: string = `MacroLogin`;
export const MacroLoginTable: string = `macro_login`;

/**
 * Column definitions for the `MacroLogin` class
 */
export enum MacroLoginColumns
{
  Id = 'Id',
  Url = 'Url',
  Macro = 'Macro',
  Login = 'Login',
  Password = 'Password',
  LastLoginError = 'LastLoginError',
  AdditionalInfo01 = 'AdditionalInfo01',
  AdditionalInfo02 = 'AdditionalInfo02',
  AdditionalInfo03 = 'AdditionalInfo03',
}
export const MacroLoginColumnsFilter: string[] = [MacroLoginColumns.Macro, MacroLoginColumns.Login];
export const MacroLoginColumnsInsert: string[] = [MacroLoginColumns.Macro, MacroLoginColumns.Url, MacroLoginColumns.Login, MacroLoginColumns.Password, MacroLoginColumns.AdditionalInfo01, MacroLoginColumns.AdditionalInfo02, MacroLoginColumns.AdditionalInfo03];
export const MacroLoginColumnsUpdate: string[] = [MacroLoginColumns.Macro, MacroLoginColumns.Url, MacroLoginColumns.Login, MacroLoginColumns.Password, MacroLoginColumns.AdditionalInfo01, MacroLoginColumns.AdditionalInfo02, MacroLoginColumns.AdditionalInfo03];

/**
 * Implementations of the `MacroLogin` class
 */
export class MacroLogin extends Crudable
{
  public static FromJson(json: any): MacroLogin
  {
    const item: MacroLogin = new MacroLogin();
    if (json)
      item.ParseFromJson(json);

    return item;
  }

  constructor(
    public Id: number = null,
    public Macro: string = null,
    public Url: string = null,
    public Login: string = null,
    public Password: string = null,
    public AdditionalInfo01: string = null,
    public AdditionalInfo02: string = null,
    public AdditionalInfo03: string = null,
    public LastLoginError: string = null,
  )
  {
    super();
    this._InitDefinition();
  }
  private _InitDefinition(): void
  {
    this.SetTableDefinition(`MacroLogin`, `macro_login`, CrudableTableDefinitionGender.Male, `Login da macro`, `Logins das macros`, true, true, true, true, true, true);

    this.SetColumnDefinition(MacroLoginColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(MacroLoginColumns.Macro, `macro`, `Macro`, `Macro`, `Macros`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(MacroLoginColumns.Url, `url`, `Url`, `Url`, `Urls`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(MacroLoginColumns.Login, `login`, `Login`, `Login`, `Logins`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(MacroLoginColumns.Password, `password`, `Password`, `Senha`, `Senhas`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(MacroLoginColumns.AdditionalInfo01, `additional_info_01`, `AdditionalInfo01`, `Extra 01`, `Extra 01`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(MacroLoginColumns.AdditionalInfo02, `additional_info_02`, `AdditionalInfo02`, `Extra 02`, `Extra 02`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(MacroLoginColumns.AdditionalInfo03, `additional_info_03`, `AdditionalInfo03`, `Extra 03`, `Extra 03`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(MacroLoginColumns.LastLoginError, `last_login_error`, `LastLoginError`, `Último Erro`, `Últimos Erro`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
  }

  public Clone(): MacroLogin
  {
    const clone: MacroLogin = new MacroLogin();
    clone.Id = this.Id;
    clone.Url = this.Url;
    clone.Macro = this.Macro;
    clone.Login = this.Login;
    clone.Password = this.Password;
    clone.LastLoginError = this.LastLoginError;
    clone.AdditionalInfo01 = this.AdditionalInfo01;
    clone.AdditionalInfo02 = this.AdditionalInfo02;
    clone.AdditionalInfo03 = this.AdditionalInfo03;

    clone.TableDefinition = this.TableDefinition;
    clone.ColumnDefinitions = this.ColumnDefinitions;

    clone.CustomProperties = this.CustomProperties;

    return clone;
  }
  public ToString(): string
  {
    return `${this.Macro}`;
  }

  public ToJson(navigationProperties: boolean = false): any
  {
    const json: any = {};
    if (!ObjectUtils.NullOrUndefined(this.Id))
      json['Id'] = this.Id;
    if (!ObjectUtils.NullOrUndefined(this.Url))
      json['Url'] = this.Url;
    if (!ObjectUtils.NullOrUndefined(this.Macro))
      json['Macro'] = this.Macro;
    if (!ObjectUtils.NullOrUndefined(this.Login))
      json['Login'] = this.Login;
    if (!ObjectUtils.NullOrUndefined(this.Password))
      json['Password'] = this.Password;
    if (!ObjectUtils.NullOrUndefined(this.LastLoginError))
      json['LastLoginError'] = this.LastLoginError;
    if (!ObjectUtils.NullOrUndefined(this.AdditionalInfo01))
      json['AdditionalInfo01'] = this.AdditionalInfo01;
    if (!ObjectUtils.NullOrUndefined(this.AdditionalInfo02))
      json['AdditionalInfo02'] = this.AdditionalInfo02;
    if (!ObjectUtils.NullOrUndefined(this.AdditionalInfo03))
      json['AdditionalInfo03'] = this.AdditionalInfo03;
    if (navigationProperties)
    {
    }

    return json;
  }
  public ParseToJson(): string
  {
    const json: any = this.ToJson();

    return JSON.stringify(json);
  }
  public ParseFromJson(json: any): void
  {
    if (json)
    {
      this.Id = json['Id'];
      this.Url = json['Url'];
      this.Macro = json['Macro'];
      this.Login = json['Login'];
      this.Password = json['Password'];
      this.LastLoginError = json['LastLoginError'];
      this.AdditionalInfo01 = json['AdditionalInfo01'];
      this.AdditionalInfo02 = json['AdditionalInfo02'];
      this.AdditionalInfo03 = json['AdditionalInfo03'];
    }
  }
}

/**
 * Implementations of the `MacroLoginFilter` class
 */
export class MacroLoginFilter extends Crudable
{
  public static FromJson(json: Object): MacroLoginFilter
  {
    const item: MacroLoginFilter = new MacroLoginFilter();
    if (json)
      item.ParseFromJson(json);

    return item;
  }

  constructor(
    public Id: number = null,
    public Macro: string = null,
    public Url: string = null,
    public Login: string = null,
    public Password: string = null,
    public AdditionalInfo01: string = null,
    public AdditionalInfo02: string = null,
    public AdditionalInfo03: string = null,
    public LastLoginError: string = null,
  )
  {
    super();
    this._InitDefinition();
  }
  private _InitDefinition(): void
  {
    this.SetTableDefinition(`MacroLoginFilter`, `macro_loginFilter`, CrudableTableDefinitionGender.Male, `Login da macro`, `Logins das macros`, true, true, true, true, true, true);

    this.SetColumnDefinition(MacroLoginColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(MacroLoginColumns.Macro, `macro`, `Macro`, `Macro`, `Macros`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(MacroLoginColumns.Url, `url`, `Url`, `Url`, `Urls`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(MacroLoginColumns.Login, `login`, `Login`, `Login`, `Logins`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(MacroLoginColumns.Password, `password`, `Password`, `Senha`, `Senhas`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(MacroLoginColumns.AdditionalInfo01, `additional_info_01`, `AdditionalInfo01`, `Extra 01`, `Extra 01`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(MacroLoginColumns.AdditionalInfo02, `additional_info_02`, `AdditionalInfo02`, `Extra 02`, `Extra 02`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(MacroLoginColumns.AdditionalInfo03, `additional_info_03`, `AdditionalInfo03`, `Extra 03`, `Extra 03`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(MacroLoginColumns.LastLoginError, `last_login_error`, `LastLoginError`, `Último Erro`, `Últimos Erro`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
  }

  public Clone(): MacroLoginFilter
  {
    const clone: MacroLoginFilter = new MacroLoginFilter();
    clone.Id = this.Id;
    clone.Url = this.Url;
    clone.Macro = this.Macro;
    clone.Login = this.Login;
    clone.Password = this.Password;
    clone.LastLoginError = this.LastLoginError;
    clone.AdditionalInfo01 = this.AdditionalInfo01;
    clone.AdditionalInfo02 = this.AdditionalInfo02;
    clone.AdditionalInfo03 = this.AdditionalInfo03;

    clone.TableDefinition = this.TableDefinition;
    clone.ColumnDefinitions = this.ColumnDefinitions;

    clone.CustomProperties = this.CustomProperties;

    return clone;
  }
  public ToString(): string
  {
    return `null`;
  }

  public ToJson(): any
  {
    const json: any = {};
    if (!ObjectUtils.NullOrUndefined(this.Id))
      json['Id'] = this.Id;
    if (!ObjectUtils.NullOrUndefined(this.Url))
      json['Url'] = this.Url;
    if (!ObjectUtils.NullOrUndefined(this.Macro))
      json['Macro'] = this.Macro;
    if (!ObjectUtils.NullOrUndefined(this.Login))
      json['Login'] = this.Login;
    if (!ObjectUtils.NullOrUndefined(this.Password))
      json['Password'] = this.Password;
    if (!ObjectUtils.NullOrUndefined(this.LastLoginError))
      json['LastLoginError'] = this.LastLoginError;
    if (!ObjectUtils.NullOrUndefined(this.AdditionalInfo01))
      json['AdditionalInfo01'] = this.AdditionalInfo01;
    if (!ObjectUtils.NullOrUndefined(this.AdditionalInfo02))
      json['AdditionalInfo02'] = this.AdditionalInfo02;
    if (!ObjectUtils.NullOrUndefined(this.AdditionalInfo03))
      json['AdditionalInfo03'] = this.AdditionalInfo03;


    return json;
  }
  public ParseToJson(): string
  {
    const json: any = this.ToJson();

    return JSON.stringify(json);
  }
  public ParseFromJson(json: Object): void
  {
    if (json)
    {
      this.Id = json['Id'];
      this.Url = json['Url'];
      this.Macro = json['Macro'];
      this.Login = json['Login'];
      this.Password = json['Password'];
      this.LastLoginError = json['LastLoginError'];
      this.AdditionalInfo01 = json['AdditionalInfo01'];
      this.AdditionalInfo02 = json['AdditionalInfo02'];
      this.AdditionalInfo03 = json['AdditionalInfo03'];
    }
  }
}
