﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model';

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// Navigation Properties
import { User } from 'clericuzzi-lib/entities/models/user.model';
import { Company } from './company.model';
import { CompanyContact } from './companyContact.model';
import { Tabulation } from './tabulation.model';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const CompanyCallClass: string = `CompanyCall`;
export const CompanyCallTable: string = `company_call`;

/**
 * Column definitions for the `CompanyCall` class
 */
export enum CompanyCallColumns
{
  Id = 'Id',
  UserId = 'UserId',
  EndedIn = 'EndedIn',
  CompanyId = 'CompanyId',
  ContactId = 'ContactId',
  StartedIn = 'StartedIn',
  TabulationId = 'TabulationId',

  UserIdParent = 'UserIdParent',
  CompanyIdParent = 'CompanyIdParent',
  ContactIdParent = 'ContactIdParent',
  TabulationIdParent = 'TabulationIdParent',
}
export const CompanyCallColumnsFilter: string[] = [];
export const CompanyCallColumnsInsert: string[] = [];
export const CompanyCallColumnsUpdate: string[] = [CompanyCallColumns.ContactId, CompanyCallColumns.TabulationId];

/**
 * Implementations of the `CompanyCall` class
 */
export class CompanyCall extends Crudable
{
  public UserIdParent: User;
  public CompanyIdParent: Company;
  public ContactIdParent: CompanyContact;
  public TabulationIdParent: Tabulation;

  public static FromJson(json: any): CompanyCall
  {
    const item: CompanyCall = new CompanyCall();
    if (json)
      item.ParseFromJson(json);

    return item;
  }

  constructor(
    public Id: number = null,
    public UserId: number = null,
    public CompanyId: number = null,
    public ContactId: number = null,
    public TabulationId: number = null,
    public StartedIn: Date = null,
    public EndedIn: Date = null,
  )
  {
    super();
    this._InitDefinition();
  }
  private _InitDefinition(): void
  {
    this.SetTableDefinition(`CompanyCall`, `company_call`, CrudableTableDefinitionGender.Male, `Ligação`, `Ligação`, true, true, true, true, true, true);

    this.SetColumnDefinition(CompanyCallColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(CompanyCallColumns.UserId, `user_id`, `UserId`, `User Id`, `User Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(CompanyCallColumns.CompanyId, `company_id`, `CompanyId`, `Company Id`, `Company Ids`, ComponentTypeEnum.AutoComplete, true, true, false, true, false, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(CompanyCallColumns.ContactId, `contact_id`, `ContactId`, `Contato`, `Contatos`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(CompanyCallColumns.TabulationId, `tabulation_id`, `TabulationId`, `Tabulação`, `Tabulações`, ComponentTypeEnum.AutoComplete, true, true, false, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(CompanyCallColumns.StartedIn, `started_in`, `StartedIn`, `Iniciada em`, `Iniciadas em`, ComponentTypeEnum.Date, true, true, false, true, false, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
    this.SetColumnDefinition(CompanyCallColumns.EndedIn, `ended_in`, `EndedIn`, `Ended In`, `Ended Ins`, ComponentTypeEnum.Date, true, true, false, true, false, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, true);
  }

  public Clone(): CompanyCall
  {
    const clone: CompanyCall = new CompanyCall();
    clone.Id = this.Id;
    clone.UserId = this.UserId;
    clone.EndedIn = this.EndedIn;
    clone.CompanyId = this.CompanyId;
    clone.ContactId = this.ContactId;
    clone.StartedIn = this.StartedIn;
    clone.TabulationId = this.TabulationId;

    clone.UserIdParent = this.UserIdParent;
    clone.CompanyIdParent = this.CompanyIdParent;
    clone.ContactIdParent = this.ContactIdParent;
    clone.TabulationIdParent = this.TabulationIdParent;

    clone.TableDefinition = this.TableDefinition;
    clone.ColumnDefinitions = this.ColumnDefinitions;

    clone.CustomProperties = this.CustomProperties;

    return clone;
  }
  public ToString(): string
  {
    return `${this.UserIdParent == null ? '' : this.UserIdParent.ToString()}`;
  }

  public ToJson(navigationProperties: boolean = false): any
  {
    const json: any = {};
    if (!ObjectUtils.NullOrUndefined(this.Id))
      json['Id'] = this.Id;
    if (!ObjectUtils.NullOrUndefined(this.UserId))
      json['UserId'] = this.UserId;
    if (!ObjectUtils.NullOrUndefined(this.CompanyId))
      json['CompanyId'] = this.CompanyId;
    if (!ObjectUtils.NullOrUndefined(this.ContactId))
      json['ContactId'] = this.ContactId;
    if (!ObjectUtils.NullOrUndefined(this.TabulationId))
      json['TabulationId'] = this.TabulationId;
    if (!ObjectUtils.NullOrUndefined(this.EndedIn))
      json['EndedIn'] = DateUtils.ToDateServer(this.EndedIn);
    if (!ObjectUtils.NullOrUndefined(this.StartedIn))
      json['StartedIn'] = DateUtils.ToDateServer(this.StartedIn);
    if (navigationProperties)
    {
      if (!ObjectUtils.NullOrUndefined(this.UserIdParent))
        json['UserIdParent'] = this.UserIdParent.ToJson();
      if (!ObjectUtils.NullOrUndefined(this.CompanyIdParent))
        json['CompanyIdParent'] = this.CompanyIdParent.ToJson();
      if (!ObjectUtils.NullOrUndefined(this.ContactIdParent))
        json['ContactIdParent'] = this.ContactIdParent.ToJson();
      if (!ObjectUtils.NullOrUndefined(this.TabulationIdParent))
        json['TabulationIdParent'] = this.TabulationIdParent.ToJson();
    }

    return json;
  }
  public ParseToJson(): string
  {
    const json: any = this.ToJson();

    return JSON.stringify(json);
  }
  public ParseFromJson(json: any): void
  {
    if (json)
    {
      this.Id = json['Id'];
      this.UserId = json['UserId'];
      this.CompanyId = json['CompanyId'];
      this.ContactId = json['ContactId'];
      this.TabulationId = json['TabulationId'];
      this.EndedIn = DateUtils.FromDateServer(json['EndedIn']);
      this.StartedIn = DateUtils.FromDateServer(json['StartedIn']);

      if (!ObjectUtils.NullOrUndefined(json['UserIdParent']))
      {
        this.UserIdParent = new User();
        this.UserIdParent.ParseFromJson(json['UserIdParent']);
      }
      if (!ObjectUtils.NullOrUndefined(json['CompanyIdParent']))
      {
        this.CompanyIdParent = new Company();
        this.CompanyIdParent.ParseFromJson(json['CompanyIdParent']);
      }
      if (!ObjectUtils.NullOrUndefined(json['ContactIdParent']))
      {
        this.ContactIdParent = new CompanyContact();
        this.ContactIdParent.ParseFromJson(json['ContactIdParent']);
      }
      if (!ObjectUtils.NullOrUndefined(json['TabulationIdParent']))
      {
        this.TabulationIdParent = new Tabulation();
        this.TabulationIdParent.ParseFromJson(json['TabulationIdParent']);
      }
    }
  }
}

/**
 * Implementations of the `CompanyCallFilter` class
 */
export class CompanyCallFilter extends Crudable
{
  public static FromJson(json: Object): CompanyCallFilter
  {
    const item: CompanyCallFilter = new CompanyCallFilter();
    if (json)
      item.ParseFromJson(json);

    return item;
  }

  constructor(
    public Id: number = null,
    public UserId: number[] = null,
    public CompanyId: number[] = null,
    public ContactId: number[] = null,
    public TabulationId: number[] = null,
    public StartedIn: Date = null,
    public EndedIn: Date = null,
  )
  {
    super();
    this._InitDefinition();
  }
  private _InitDefinition(): void
  {
    this.SetTableDefinition(`CompanyCallFilter`, `company_callFilter`, CrudableTableDefinitionGender.Male, `Company Call`, `Company Calls`, true, true, true, true, true, true);

    this.SetColumnDefinition(CompanyCallColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(CompanyCallColumns.UserId, `user_id`, `UserId`, `User Id`, `User Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(CompanyCallColumns.CompanyId, `company_id`, `CompanyId`, `Company Id`, `Company Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(CompanyCallColumns.ContactId, `contact_id`, `ContactId`, `Contact Id`, `Contact Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(CompanyCallColumns.TabulationId, `tabulation_id`, `TabulationId`, `Tabulation Id`, `Tabulation Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(CompanyCallColumns.StartedIn, `started_in`, `StartedIn`, `Started In`, `Started Ins`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
    this.SetColumnDefinition(CompanyCallColumns.EndedIn, `ended_in`, `EndedIn`, `Ended In`, `Ended Ins`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, true);
  }

  public Clone(): CompanyCallFilter
  {
    const clone: CompanyCallFilter = new CompanyCallFilter();
    clone.Id = this.Id;
    clone.UserId = this.UserId;
    clone.EndedIn = this.EndedIn;
    clone.CompanyId = this.CompanyId;
    clone.ContactId = this.ContactId;
    clone.StartedIn = this.StartedIn;
    clone.TabulationId = this.TabulationId;

    clone.TableDefinition = this.TableDefinition;
    clone.ColumnDefinitions = this.ColumnDefinitions;

    clone.CustomProperties = this.CustomProperties;

    return clone;
  }
  public ToString(): string
  {
    return `null`;
  }

  public ToJson(): any
  {
    const json: any = {};
    if (!ObjectUtils.NullOrUndefined(this.Id))
      json['Id'] = this.Id;
    if (!ObjectUtils.NullOrUndefined(this.EndedIn))
      json['EndedIn'] = DateUtils.ToDateServer(this.EndedIn);
    if (!ObjectUtils.NullOrUndefined(this.StartedIn))
      json['StartedIn'] = DateUtils.ToDateServer(this.StartedIn);
    if (!ObjectUtils.NullOrUndefined(this.UserId))
      json['UserId'] = Array.isArray(this.UserId) ? this.UserId : [this.UserId];
    if (!ObjectUtils.NullOrUndefined(this.CompanyId))
      json['CompanyId'] = Array.isArray(this.CompanyId) ? this.CompanyId : [this.CompanyId];
    if (!ObjectUtils.NullOrUndefined(this.ContactId))
      json['ContactId'] = Array.isArray(this.ContactId) ? this.ContactId : [this.ContactId];
    if (!ObjectUtils.NullOrUndefined(this.TabulationId))
      json['TabulationId'] = Array.isArray(this.TabulationId) ? this.TabulationId : [this.TabulationId];


    return json;
  }
  public ParseToJson(): string
  {
    const json: any = this.ToJson();

    return JSON.stringify(json);
  }
  public ParseFromJson(json: Object): void
  {
    if (json)
    {
      this.Id = json['Id'];
      this.UserId = json['UserId'];
      this.CompanyId = json['CompanyId'];
      this.ContactId = json['ContactId'];
      this.TabulationId = json['TabulationId'];
      this.EndedIn = DateUtils.FromDateServer(json['EndedIn']);
      this.StartedIn = DateUtils.FromDateServer(json['StartedIn']);
    }
  }
}
