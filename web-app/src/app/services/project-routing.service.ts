import { Injectable } from '@angular/core';

// Services
import { RoutingService } from 'clericuzzi-lib/services/routing.service';

// constants
import { SitePaths } from 'app/modules/site/site-paths.routes';
import { BasicModuleLoginPaths } from 'module-basic/modules/login/clericuzzi-module-basic-login-paths.routes';

@Injectable()
export class ProjectRoutingService
{
    constructor(public Routing: RoutingService) { }

    public Back(): void { this.Routing.Back(); }

    public ToSiteRoute(route: string, delay: number = 0, param: any = null): void
    {
        this.Routing.ToRoute(route, SitePaths.INDEX, delay, param);
    }

    public To(route: string, module: string = `Site`): void { this.Routing.ToRoute(route, module, 0, null); }
    public Site(delay: number = 0): void { this.Routing.ToRoute(null, SitePaths.INDEX, delay, null); }
    public Login(delay: number = 0): void { this.Routing.ToRoute(BasicModuleLoginPaths.INDEX, null, delay, null); }
}
