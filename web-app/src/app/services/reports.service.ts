import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';

// utils
import { ServerUrls } from 'clericuzzi-lib/utils/server-urls';
// models
import { ReportMining } from 'app/business/models/report-mining.model';
import { RequestMessage } from 'clericuzzi-lib/entities/request/request-message.model';
import { ResponseMessage } from 'clericuzzi-lib/entities/response/response-message.model';
// services
import { HttpService } from 'clericuzzi-lib/services/http.service';
import { LoaderService } from 'clericuzzi-lib/services/loader.service';
import { UserDataService } from 'clericuzzi-lib/services/user-data.service';
import { DateFilter } from '../business/filters/date.filter';
import { ReportCallsOrSales } from 'app/business/models/reports-calls-or-sales.model';
import { EmployeeFilter } from 'app/business/filters/employee.filter';
// components
// behaviours

@Injectable()
export class ReportService
{
  constructor(
    private _Http: HttpService,
    private _Snack: MatSnackBar,
    private _Loader: LoaderService,
    private _UserData: UserDataService,
  )
  {
  }

  public Calls(filter: EmployeeFilter, callback: (response: ResponseMessage<ReportCallsOrSales>) => void): void
  {
    const request: RequestMessage = new RequestMessage(ServerUrls.GetUrl(`stats/calls`));
    request.Add(`to`, filter.DateTo);
    request.Add(`from`, filter.DateFrom);
    request.Add(`employee-ids`, filter.EmployeeIds);

    const response: ResponseMessage<ReportCallsOrSales> = new ResponseMessage<ReportCallsOrSales>();
    this._Http.Post(request, response, () => callback(response));
  }
  public Sells(filter: EmployeeFilter, callback: (response: ResponseMessage<ReportCallsOrSales>) => void): void
  {
    const request: RequestMessage = new RequestMessage(ServerUrls.GetUrl(`stats/sells`));
    request.Add(`to`, filter.DateTo);
    request.Add(`from`, filter.DateFrom);
    request.Add(`employee-ids`, filter.EmployeeIds);

    const response: ResponseMessage<ReportCallsOrSales> = new ResponseMessage<ReportCallsOrSales>();
    this._Http.Post(request, response, () => callback(response));
  }
  public Mining(filter: DateFilter, callback: (response: ResponseMessage<ReportMining>) => void): void
  {
    const request: RequestMessage = new RequestMessage(ServerUrls.GetUrl(`stats/mining`));
    request.Add(`to`, filter.DateTo);
    request.Add(`from`, filter.DateFrom);

    const response: ResponseMessage<ReportMining> = new ResponseMessage<ReportMining>();
    this._Http.Post(request, response, () => callback(response));
  }
  public Evaluations(filter: DateFilter, callback: (response: ResponseMessage<ReportMining>) => void): void
  {
    const request: RequestMessage = new RequestMessage(ServerUrls.GetUrl(`stats/evaluations`));
    request.Add(`to`, filter.DateTo);
    request.Add(`from`, filter.DateFrom);

    const response: ResponseMessage<ReportMining> = new ResponseMessage<ReportMining>();
    this._Http.Post(request, response, () => callback(response));
  }
  public Tabulations(filter: EmployeeFilter, callback: (response: ResponseMessage<ReportMining>) => void): void
  {
    const request: RequestMessage = new RequestMessage(ServerUrls.GetUrl(`stats/tabulations`));
    request.Add(`to`, filter.DateTo);
    request.Add(`from`, filter.DateFrom);
    request.Add(`employee-ids`, filter.EmployeeIds);

    const response: ResponseMessage<ReportMining> = new ResponseMessage<ReportMining>();
    this._Http.Post(request, response, () => callback(response));
  }
}
