import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';
// utils
import { ServerUrls } from 'clericuzzi-lib/utils/server-urls';
import { SnackUtils } from 'clericuzzi-lib/utils/snack-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';
// models
import { CompanyCall } from 'app/entities/companyCall.model';
import { CompanyItem } from 'app/business/models/company-item.model';
import { RequestMessage } from 'clericuzzi-lib/entities/request/request-message.model';
import { ResponseMessage } from 'clericuzzi-lib/entities/response/response-message.model';
import { StatusResponseMessage } from 'clericuzzi-lib/entities/response/status-response-message.model';
// services
import { HttpService } from 'clericuzzi-lib/services/http.service';
import { LoaderService } from 'clericuzzi-lib/services/loader.service';
import { UserDataService } from 'clericuzzi-lib/services/user-data.service';
import { CompanyCallback } from 'app/entities/companyCallback.model';
import { BatchResponseMessage } from 'clericuzzi-lib/entities/response/batch-response-message.model';
// components
// behaviours

@Injectable()
export class CompanyActionsService
{
  public OnCompanyLoaded: Subject<any> = new Subject<any>();
  public OnLeadRequired: Subject<void> = new Subject<void>();
  public OnCallbackCleared: Subject<BatchResponseMessage<CompanyCallback>> = new Subject<BatchResponseMessage<CompanyCallback>>();
  public OnCompanyCallbackAdded: Subject<CompanyCallback> = new Subject<CompanyCallback>();
  public CurrentModel: CompanyItem;
  constructor(
    private _Http: HttpService,
    private _Snack: MatSnackBar,
    private _Loader: LoaderService,
    private _UserData: UserDataService,
  )
  {
  }

  public CampaignId: number;

  public Clear(): void
  {
    this.CurrentModel = null;
  }
  public SetCampaignId(campaignId: number): void
  {
    this.CampaignId = campaignId;
    this.OnLeadRequired.next();
  }

  public RequestCheck(companyId: number, callback: (response: StatusResponseMessage) => void): void
  {
    const request: RequestMessage = new RequestMessage(ServerUrls.GetUrl(`companies/request-check`));
    request.Add(`company-id`, companyId);

    const response: StatusResponseMessage = new StatusResponseMessage();
    this._Http.Post(request, response, () => callback(response));
  }

  public NewCall(callback: (response: BatchResponseMessage<CompanyCall>) => void): void
  {
    const request: RequestMessage = new RequestMessage(ServerUrls.GetUrl(`companies/new-call`));
    const newCall: CompanyCall = new CompanyCall(null, this._UserData.Id, this.CurrentModel.Id, null, null);
    request.Add(`model`, newCall);

    const response: BatchResponseMessage<CompanyCall> = new BatchResponseMessage<CompanyCall>();
    this._Http.Post(request, response, () => this._NewCallCallback(response, callback));
  }
  private _NewCallCallback(response: BatchResponseMessage<CompanyCall>, callback: (response: BatchResponseMessage<CompanyCall>) => void): void
  {
    if (response.HasErrors)
      SnackUtils.OpenError(this._Snack, response.ErrorMessage);
    else
    {
      if (ObjectUtils.NullOrUndefined(response.Entities))
        SnackUtils.OpenError(this._Snack, `falha no envio da solicitação`);
      else
      {
        callback(response);
        this.OnCompanyCallbackAdded.next();
      }
    }
  }

  public NewLead(callback: () => void): void
  {
    let url: string = `companies/new-lead`;
    if (this.CampaignId === 5)
      url = `companies/new-lead-from-file`;

    this._Loader.Show();
    const request: RequestMessage = new RequestMessage(ServerUrls.GetUrl(url));
    if (this.CampaignId === 5)
      request.Add(`import-file-id`, 1);
    else
      request.Add(`campaign-id`, this.CampaignId);

    const response: ResponseMessage<CompanyItem> = new ResponseMessage<CompanyItem>();
    this._Http.Post(request, response, () => this._NewLeadSohoCallback(response, callback));
  }
  public NewLeadMobile(callback: () => void): void
  {
    this._Loader.Show();
    const request: RequestMessage = new RequestMessage(ServerUrls.GetUrl(`companies/new-lead`));

    const response: ResponseMessage<CompanyItem> = new ResponseMessage<CompanyItem>();
    this._Http.Post(request, response, () => this._NewLeadSohoCallback(response, callback));
  }
  public NewLeadSohoHighSpeed(callback: () => void): void
  {
    this._Loader.Show();
    const request: RequestMessage = new RequestMessage(ServerUrls.GetUrl(`companies/new-lead`));

    const response: ResponseMessage<CompanyItem> = new ResponseMessage<CompanyItem>();
    this._Http.Post(request, response, () => this._NewLeadSohoCallback(response, callback));
  }
  private _NewLeadSohoCallback(response: ResponseMessage<CompanyItem>, callback: () => void): void
  {
    this._Loader.Hide();
    this.CurrentModel = ResponseMessage.HandleResponse(response, new CompanyItem(), this._Snack);
    if (!ObjectUtils.NullOrUndefined(this.CurrentModel))
      callback();
  }

  public LoadModel(): void
  {
    if (ObjectUtils.NullOrUndefined(this.CurrentModel))
      SnackUtils.OpenError(this._Snack, `Não existe uma empresa selecionada...`);
    else
    {
      this._Loader.Show();
      const request: RequestMessage = new RequestMessage(ServerUrls.GetUrl(`companies/load`));
      request.Add(`company-id`, this.CurrentModel.Id);

      const response: ResponseMessage<CompanyItem> = new ResponseMessage<CompanyItem>();
      this._Http.Post(request, response, () => this._LoadModelCallback(response));
    }
  }
  private _LoadModelCallback(response: ResponseMessage<CompanyItem>): void
  {
    this._Loader.Hide();
    this.CurrentModel = ResponseMessage.HandleResponse(response, new CompanyItem(), this._Snack);

    this.OnCompanyLoaded.next();
  }

  public LoadUserCallbacks(callback: (response: BatchResponseMessage<CompanyCallback>) => void): void
  {
    const request: RequestMessage = new RequestMessage(ServerUrls.GetUrl(`callbacks/load`));

    const response: BatchResponseMessage<CompanyCallback> = new BatchResponseMessage<CompanyCallback>();
    this._Http.Post(request, response, () => callback(response));
  }
  public ClearUserCallback(callbackId: number): void
  {
    const request: RequestMessage = new RequestMessage(ServerUrls.GetUrl(`callbacks/clear`));
    request.Add(`callback-id`, callbackId);

    const response: BatchResponseMessage<CompanyCallback> = new BatchResponseMessage<CompanyCallback>();
    this._Http.Post(request, response, () => this.OnCallbackCleared.next(response));
  }
}
