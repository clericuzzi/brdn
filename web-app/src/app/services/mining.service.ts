import { Injectable } from '@angular/core';
import { HttpService } from 'clericuzzi-lib/services/http.service';
import { ServerUrls } from 'clericuzzi-lib/utils/server-urls';
import { MiningTarget } from 'app/business/models/mining-targets.model';
import { RequestMessage } from 'clericuzzi-lib/entities/request/request-message.model';
import { StatusResponseMessage } from 'clericuzzi-lib/entities/response/status-response-message.model';

@Injectable()
export class MiningService
{
  constructor(
    private _Http: HttpService,
  )
  {
  }

  public ClearTargets(miningTarget: MiningTarget, callback: (response: StatusResponseMessage) => void): void
  {
    const request: RequestMessage = new RequestMessage(ServerUrls.GetUrl(`mining/clear-targets`));
    request.Add(`filter`, miningTarget.ToJson());

    const response: StatusResponseMessage = new StatusResponseMessage();
    this._Http.Post(request, response, () => callback(response));
  }
  public DefineTargets(miningTarget: MiningTarget): void
  {
    const request: RequestMessage = new RequestMessage(ServerUrls.GetUrl(`mining/set-targets`));
    request.Add(`filter`, miningTarget.ToJson());

    const response: StatusResponseMessage = new StatusResponseMessage();
    this._Http.Post(request, response, null);
  }
}
