import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot } from '@angular/router';

// utils
import { SitePaths } from 'app/modules/site/site-paths.routes';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// models

// services
import { UserDataService } from 'clericuzzi-lib/services/user-data.service';

@Injectable()
export class RoleGuardService implements CanActivate
{
  constructor(
    private _User: UserDataService,
    private _Router: Router,
  )
  {
  }

  canActivate(route: ActivatedRouteSnapshot) : boolean
  {
    let returnValue = true;
    if (!ObjectUtils.NullUndefinedOrEmptyArray(route.data.forbiddenList))
    {
      let forbiddenList: number[] = route.data.forbiddenList;
      if (this._User.CheckForbiddenPermissionList(forbiddenList))
      {
        // this._Router.navigate([ SitePaths.MY_ACTIONS_EVALUATIONS_PATH ]);
        returnValue = false;
      }
    }
    else if (!ObjectUtils.NullUndefinedOrEmptyArray(route.data.requiredPermissions))
    {
      let requiredPermissions: number[] = route.data.requiredPermissions;
      if (!this._User.CheckPermissionList(requiredPermissions))
      {
        // this._Router.navigate([ SitePaths.MY_ACTIONS_EVALUATIONS_PATH ]);
        returnValue = false;
      }
    }

    return returnValue;
  }
}
