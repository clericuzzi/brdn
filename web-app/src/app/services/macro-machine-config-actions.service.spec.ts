import { TestBed } from '@angular/core/testing';

import { MacroMachineConfigActionsService } from './macro-machine-config-actions.service';

describe('MacroMachineConfigActionsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MacroMachineConfigActionsService = TestBed.get(MacroMachineConfigActionsService);
    expect(service).toBeTruthy();
  });
});
