import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';
// utils
import { ServerUrls } from 'clericuzzi-lib/utils/server-urls';
import { SnackUtils } from 'clericuzzi-lib/utils/snack-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';
// models
import { CompanyCall } from 'app/entities/companyCall.model';
import { CompanyItem } from 'app/business/models/company-item.model';
import { RequestMessage } from 'clericuzzi-lib/entities/request/request-message.model';
import { ResponseMessage } from 'clericuzzi-lib/entities/response/response-message.model';
import { StatusResponseMessage } from 'clericuzzi-lib/entities/response/status-response-message.model';
// services
import { HttpService } from 'clericuzzi-lib/services/http.service';
import { LoaderService } from 'clericuzzi-lib/services/loader.service';
import { UserDataService } from 'clericuzzi-lib/services/user-data.service';
import { CompanyCallback } from 'app/entities/companyCallback.model';
import { BatchResponseMessage } from 'clericuzzi-lib/entities/response/batch-response-message.model';
import { Activity } from 'clericuzzi-lib/entities/models/activity.model';
import { CampaignInfo } from 'app/business/models/campaign-info.model';
// components
// behaviours

@Injectable()
export class CampaignActionsService
{
  public CurrentModel: Activity;
  constructor(
    private _Http: HttpService,
    private _Snack: MatSnackBar,
    private _Loader: LoaderService,
    private _UserData: UserDataService,
  )
  {
  }

  public Campaign: Activity;
  public Update(allowedCharacteristics: number[], notAllowedCharacteristics: number[], callback: (response: StatusResponseMessage) => void): void
  {
    const request: RequestMessage = new RequestMessage(ServerUrls.GetUrl(`campaign/update`));
    request.Add(`allowed`, allowedCharacteristics);
    request.Add(`not-allowed`, notAllowedCharacteristics);
    request.Add(`campaign-id`, this.Campaign.Id);

    const response: StatusResponseMessage = new StatusResponseMessage();
    this._Http.Post(request, response, () => callback(response));
  }
  public LoadConditions(callback: (response: ResponseMessage<CampaignInfo>) => void): void
  {
    const request: RequestMessage = new RequestMessage(ServerUrls.GetUrl(`campaign/load-conditions`));
    request.Add(`campaign-id`, this.Campaign.Id);

    const response: ResponseMessage<CampaignInfo> = new ResponseMessage<CampaignInfo>();
    this._Http.Post(request, response, () => callback(response));
  }
}
