import { Injectable } from '@angular/core';
import { MacroMachine } from 'app/entities/macroMachine.model';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

@Injectable({
  providedIn: 'root'
})
export class MacroMachineConfigActionsService
{
  private _Machine: MacroMachine;
  constructor()
  {
  }

  public SetCurrentMachine(machine: MacroMachine): void
  {
    this._Machine = machine;
  }
  public ClearCurrentMachine(): void
  {
    this._Machine = null;
  }

  public get MachineId(): number
  {
    return ObjectUtils.NullOrUndefined(this._Machine) ? 0 : this._Machine.Id;
  }
  public get MachineName(): string
  {
    return ObjectUtils.NullOrUndefined(this._Machine) ? null : this._Machine.MachineName;
  }
}
