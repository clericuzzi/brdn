import { Injectable } from '@angular/core';

// utils
import { ServerUrls } from 'clericuzzi-lib/utils/server-urls';

// models
import { User } from 'clericuzzi-lib/entities/models/user.model';
import { RequestMessage } from 'clericuzzi-lib/entities/request/request-message.model';
import { BatchResponseMessageList } from 'clericuzzi-lib/entities/response/batch-response-message.model';

// services
import { HttpService } from 'clericuzzi-lib/services/http.service';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';
import { StatusResponseMessage } from 'clericuzzi-lib/entities/response/status-response-message.model';
import { CustomField } from 'clericuzzi-lib/entities/crud/custom-field.model';

@Injectable()
export class UserConfigService
{
  constructor(
    private _Http: HttpService,
  )
  {
  }

  public NewUser(user: User, permissions: any[], callback: Function): void
  {
    let requestMesage: RequestMessage = new RequestMessage(ServerUrls.GetUrl(`crud/user/new`));
    requestMesage.Add(`model`, user.ToJson());
    requestMesage.Add(`password`, user.Password);
    requestMesage.Add(`permissions`, permissions);

    let response: StatusResponseMessage = new StatusResponseMessage();
    this._Http.Post(requestMesage, response, () => callback(response));
  }
  public UpdateDefinitions(url: string, paramValue: number, definitions: any[], callback: Function): void
  {
    let requestMesage: RequestMessage = new RequestMessage(url);
    requestMesage.Add(`param`, paramValue);
    requestMesage.Add(`definitions`, definitions);

    let response: StatusResponseMessage = new StatusResponseMessage();
    this._Http.Post(requestMesage, response, () => callback(response));
  }
  public GetDefinitions(url: string, paramName: string, paramValue: any, callback: Function, ... customFields: CustomField[]): void
  {
    let requestMesage: RequestMessage = new RequestMessage(url);
    if (!ObjectUtils.NullOrUndefined(paramValue))
      requestMesage.Add(paramName, paramValue);

    if (!ObjectUtils.NullUndefinedOrEmptyArray(customFields))
      for (let customField of customFields)
        requestMesage.Add(customField.Key, customField.Value);

    let response: BatchResponseMessageList = new BatchResponseMessageList();
    this._Http.Post(requestMesage, response, () => callback(response));
  }
}