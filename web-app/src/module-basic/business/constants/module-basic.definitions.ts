import { CrudSelectController } from 'clericuzzi-lib/modules/shared/crud/crud-select/crud-select.controller';
import { MaterialPopupComponent } from 'clericuzzi-lib/modules/material/components/material-popup/material-popup.component';

export class ModuleBasicDefinitions
{
    public static Init(crudListingEndpoint: string): void
    {
        CrudSelectController.RootEndpoint = crudListingEndpoint;
        CrudSelectController.ListingEndpoint = `${CrudSelectController.RootEndpoint}crud/listing`;
        CrudSelectController.CrudPaginationButtonClass = `crud-pagination-button z-button z-button-borderless z-button-small backcolor-primary color-white`;
        CrudSelectController.CrudPaginationButtonClassSelected = `crud-pagination-button z-button z-button-borderless z-button-small backcolor-white color-primary`;
  
        MaterialPopupComponent.LabelNo = `não`;
        MaterialPopupComponent.LabelYes = `sim`;
        MaterialPopupComponent.DenyClass = `z-button z-button-medium z-button-borderless backcolor-white color-gray-light`;
        MaterialPopupComponent.ConfirmClass = `z-button z-button-medium z-button-borderless backcolor-primary color-white`;
    }
}