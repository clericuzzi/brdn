import { Component, OnInit } from '@angular/core';

// utils
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// models
import { Account } from 'clericuzzi-lib/entities/models/account.model';

// behaviours
import { SelectListItem } from 'clericuzzi-lib/business/behaviours/select-list-item.behaviour';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';

@Component({
  selector: 'app-config-account-list-item',
  templateUrl: './config-account-list-item.component.html'
})
export class ConfigAccountListItemComponent extends SelectListItem<Account> implements OnInit
{
  constructor()
  {
    super(null);
  }

  ngOnInit()
  {
  }
  
  public get Fee(): string
  {
    if (ObjectUtils.NullOrUndefined(this.Model) || ObjectUtils.NullOrUndefined(this.Model.SignatureFee))
      return `falha ao recuperar a mensalidade`;
    else
      return StringUtils.ValueToMoney(this.Model.SignatureFee, `R$`);
  }
  public get Cnpj(): string
  {
    if (ObjectUtils.NullOrUndefined(this.Model) || ObjectUtils.NullUndefinedOrEmpty(this.Model.Cnpj))
      return `falha ao recuperar o CNPJ`;
    else
      return StringUtils.TransformCnpj(this.Model.Cnpj);
  }
  public get Name(): string
  {
    if (ObjectUtils.NullOrUndefined(this.Model) || ObjectUtils.NullUndefinedOrEmpty(this.Model.Name))
      return `falha ao recuperar a conta`;
    else
      return this.Model.Name;
  }
  public get Site(): string
  {
    if (ObjectUtils.NullOrUndefined(this.Model) || ObjectUtils.NullUndefinedOrEmpty(this.Model.Site))
      return `falha ao recuperar o site`;
    else
      return this.Model.Name;
  }
  public get Email(): string
  {
    if (ObjectUtils.NullOrUndefined(this.Model) || ObjectUtils.NullUndefinedOrEmpty(this.Model.Email))
      return `falha ao recuperar a conta`;
    else
      return this.Model.Email;
  }
  public get Phone(): string
  {
    if (ObjectUtils.NullOrUndefined(this.Model) || ObjectUtils.NullUndefinedOrEmpty(this.Model.Phone))
      return `falha ao recuperar o telefone`;
    else
      return StringUtils.ToPhoneBr(this.Model.Phone);
  }
}