import { Component, OnInit } from '@angular/core';
// utils
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';
// models
import { CrudListItemModel } from 'module-basic/business/models/crudListItem.model';
// services
import { RoutingService } from 'clericuzzi-lib/services/routing.service';
// components
import { HostBindedComponent } from 'clericuzzi-lib/modules/shared/util-components/host-binded/host-binded.component';
// behaviours

@Component({
  selector: 'app-crud-list-item',
  templateUrl: './crud-list-item.component.html'
})
export class CrudListItemComponent extends HostBindedComponent implements OnInit
{
  private _Model: CrudListItemModel;
  constructor(
    private _Routing: RoutingService,
  )
  {
    super(null);
    this.ClearComponentClass();
    this.AddClasses(`cursor-pointer-with-hover`, `crud-list-item`, `white-card-marginless`, `font-bold`, `full-size`, `flex-center`);
  }

  ngOnInit()
  {
  }

  public get Link(): string
  {
    return ObjectUtils.GetValue(this._Model, null, `Link`);
  }
  public get Prefix(): string
  {
    return ObjectUtils.GetValue(this._Model, null, `Prefix`);
  }
  public get Text(): string
  {
    return ObjectUtils.GetValue(this._Model, null, `Text`);
  }

  public SetModel(text: string, link: string, prefix: string, action: () => void = null): void
  {
    this._Model = new CrudListItemModel(link, prefix, text, action);
  }
  public ClickAction(): void
  {
    if (!ObjectUtils.NullOrUndefined(this._Model, this._Model.Action))
      this._Model.Action();
    this._Routing.ToRoute(this.Link, this.Prefix);
  }
}
