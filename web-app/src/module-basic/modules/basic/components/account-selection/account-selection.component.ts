import { MatDialog, MatSnackBar } from '@angular/material';
import { Component, OnInit, ViewContainerRef, ViewChild } from '@angular/core';

// utils
import { SnackUtils } from 'clericuzzi-lib/utils/snack-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// models
import { AccountColumns } from 'clericuzzi-lib/entities/models/account.model';
import { Permission, PermissionColumns } from 'clericuzzi-lib/entities/models/permission.model';
import { CrudableTableColumnDefinitionModel, ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';

// services
import { UserDataService } from 'clericuzzi-lib/services/user-data.service';
import { PageTitleService } from 'clericuzzi-lib/services/page-title.service';

// components
import { ComboInputComponent } from 'clericuzzi-lib/modules/shared/form-controls/custom-inputs/combo-input/combo-input.component';
import { HostBindedComponent } from 'clericuzzi-lib/modules/shared/util-components/host-binded/host-binded.component';
import { MaterialPopupComponent } from 'clericuzzi-lib/modules/material/components/material-popup/material-popup.component';
import { ComponentGeneratorService } from 'clericuzzi-lib/services/component-generator.service';

@Component({
  selector: 'app-account-selection',
  templateUrl: './account-selection.component.html'
})
export class AccountSelectionComponent extends HostBindedComponent implements OnInit
{
  @ViewChild(`container`, { read: ViewContainerRef }) private _Container: ViewContainerRef;

  public ChangeEnabled: boolean = false;
  private _Permission: Permission = new Permission();
  constructor(
    private _Diag: MatDialog,
    private _Snack: MatSnackBar,
    private _UserData: UserDataService,
    private _Generator: ComponentGeneratorService,

    public pageTitle: PageTitleService,
  )
  {
    super(pageTitle);
  }

  ngOnInit()
  {
    this.InitFactories(this._Generator.Factory, this._Generator.Sanitizer);
    this._InitComponent();
  }
  private _InitComponent(): void
  {
    let permission: Permission = new Permission();
    let accountColumn: CrudableTableColumnDefinitionModel = permission.GetColumnDefinition(PermissionColumns.AccountId);
    accountColumn.ReadableName = `Selecione a conta desejada`;
    accountColumn.ComponentType = ComponentTypeEnum.Combo;

    this._Permission.AccountId = this._UserData.CurrentAccountId;
    let accountComponent: ComboInputComponent<number> = this.AddComponent(this._Container, accountColumn);
    accountComponent.SetItems(this._UserData.Accounts.All, AccountColumns.Id, AccountColumns.Name);

    accountComponent.Update(this._Permission);

    this.ListenTo(accountComponent.OnValueChanged, () => this._AccountValueChanged());
  }

  private _AccountValueChanged(): void
  {
    if (ObjectUtils.NullOrUndefined(this._Permission.AccountId) || this._Permission.AccountId <= 0)
      this.ChangeEnabled = false;
    else
      this.ChangeEnabled = this._UserData.CurrentAccountId != this._Permission.AccountId;
  }
  public ChangeAccount(): void
  {
    MaterialPopupComponent.Confirmation(this._Diag, this._ChangeAccountAction.bind(this), null, `Trocar conta`, `Deseja realmente selecionar uma nova conta?`);
  }
  private _ChangeAccountAction(): void
  {
    this._UserData.Accounts.SelectAccount(this._Permission.AccountId);
    SnackUtils.Open(this._Snack, `Conta alterada com sucesso`);
    this.CloseComponent();
  }
}