import { MatSnackBar, MatDialog } from '@angular/material';
import { Component, OnInit, ViewContainerRef, ViewChild } from '@angular/core';

// utils
import { ServerUrls } from 'clericuzzi-lib/utils/server-urls';
import { SnackUtils } from 'clericuzzi-lib/utils/snack-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// models
import { RequestMessage } from 'clericuzzi-lib/entities/request/request-message.model';
import { ResponseMessage } from 'clericuzzi-lib/entities/response/response-message.model';
import { UserEditModel, UserEditModelColumns } from 'module-basic/modules/basic/business/models/user-edit.model';

// services
import { HttpService } from 'clericuzzi-lib/services/http.service';
import { LoaderService } from 'clericuzzi-lib/services/loader.service';
import { UserDataService } from 'clericuzzi-lib/services/user-data.service';
import { MaterialPopupService } from 'clericuzzi-lib/services/material-popup.service';
import { ComponentGeneratorService } from 'clericuzzi-lib/services/component-generator.service';

// components
import { TextInputComponent } from 'clericuzzi-lib/modules/shared/form-controls/text-input/text-input.component';
import { HostBindedComponent } from 'clericuzzi-lib/modules/shared/util-components/host-binded/host-binded.component';
import { MaterialPopupComponent } from 'clericuzzi-lib/modules/material/components/material-popup/material-popup.component';

@Component({
  selector: 'app-config-user-edit',
  templateUrl: './config-user-edit.component.html',
  styleUrls: ['./config-user-edit.component.css']
})
export class ConfigUserEditComponent extends HostBindedComponent implements OnInit
{
  @ViewChild('_container', { read: ViewContainerRef }) public ItemsContainer: ViewContainerRef;

  public User: UserEditModel;
  public get ValidInformation(): boolean
  {
    if (ObjectUtils.NullOrUndefined(this.User))
      return false;
    else
      return !ObjectUtils.NullUndefinedOrEmpty(this.User.Name) && !ObjectUtils.NullUndefinedOrEmpty(this.User.Email)
  }

  constructor(
    private _Diag: MatDialog,
    private _Http: HttpService,
    private _Snack: MatSnackBar,
    private _Loader: LoaderService,
    private _UserData: UserDataService,
    private _Generator: ComponentGeneratorService,
    private _MaterialPopup: MaterialPopupService,
  )
  {
    super(null);
  }

  private _PasswordComponent: TextInputComponent;
  ngOnInit()
  {
    this.InitFactories(this._Generator.Factory, this._Generator.Sanitizer);
    this.Busy = true;
    this._Load();

    let user: UserEditModel = new UserEditModel();
    this.AddComponent(this.ItemsContainer, user.GetColumnDefinition(UserEditModelColumns.Name), `Nome`);
    this.AddComponent(this.ItemsContainer, user.GetColumnDefinition(UserEditModelColumns.Email), `Email`);
    this.AddComponent(this.ItemsContainer, user.GetColumnDefinition(UserEditModelColumns.Phone), `Telefone`);

    for (let component of this.FormItems)
      this.ListenTo(component.OnSend, () => this.ConfirmUpdate());
  }

  private _Load(): void
  {
    let request: RequestMessage = new RequestMessage(ServerUrls.GetUrl(`crud/user/load`));
    request.AddId(this._UserData.Id);

    let response: ResponseMessage<UserEditModel> = new ResponseMessage<UserEditModel>();
    this._Http.Post(request, response, () => this._LoadCallback(response));
  }
  private _LoadCallback(response: ResponseMessage<UserEditModel>): void
  {
    this.Busy = false;
    if (response.HasErrors)
      SnackUtils.OpenError(this._Snack, response.ErrorMessage);
    else
    {
      this.User = response.Parse(new UserEditModel());
      this.Sync(this.User);
    }
  }
  public ConfirmUpdate(): void
  {
    MaterialPopupComponent.ConfirmationWithText(this._Diag, () => this._UpdateAction(), null, `Confirmar alterações`, `Deseja realmente alterar suas informações?`, `Digite sua senha para continuar`, true);
  }
  private _UpdateAction(): void
  {
    this._Loader.Show();
    let request: RequestMessage = new RequestMessage(ServerUrls.GetUrl(`crud/user/change-settings`));
    request.AddId(this._UserData.Id);
    request.Add(`user-settings`, this.User);
    request.Add(`current-password`, this._MaterialPopup.GetConfirmationText());

    let response: ResponseMessage<UserEditModel> = new ResponseMessage<UserEditModel>();
    this._Http.Post(request, response, () => this._UpdateCallback(response));
  }
  private _UpdateCallback(response: ResponseMessage<UserEditModel>): void
  {
    this._Loader.Hide();
    if (response.HasErrors)
      SnackUtils.OpenError(this._Snack, response.ErrorMessage);
    else
    {
      this._UserData.Session.NameFull = this.User.Name;
      this.CloseComponent();
    }
  }
}