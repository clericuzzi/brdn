import { MatDialog, MatSnackBar, MAT_DIALOG_DATA } from '@angular/material';
import { Component, OnInit, ViewContainerRef, ViewChild, Inject } from '@angular/core';

// utils
import { ServerUrls } from 'clericuzzi-lib/utils/server-urls';
import { SnackUtils } from 'clericuzzi-lib/utils/snack-utils';

// models
import { User, UserColumns } from 'clericuzzi-lib/entities/models/user.model';
import { StatusResponseMessage } from 'clericuzzi-lib/entities/response/status-response-message.model';
import { Permission, PermissionTable, PermissionClass } from 'clericuzzi-lib/entities/models/permission.model';
import { UserPermission, UserPermissionColumns, UserPermissionClass } from 'clericuzzi-lib/entities/models/userPermission.model';

// services
import { LoaderService } from 'clericuzzi-lib/services/loader.service';
import { UserConfigService } from 'module-basic/services/user-config.service';
import { MaterialPopupService } from 'clericuzzi-lib/services/material-popup.service';
import { ComponentGeneratorService } from 'clericuzzi-lib/services/component-generator.service';

// components
import { FromToComponent } from '../from-to/from-to.component';
import { TextInputComponent } from 'clericuzzi-lib/modules/shared/form-controls/text-input/text-input.component';
import { HostBindedComponent } from 'clericuzzi-lib/modules/shared/util-components/host-binded/host-binded.component';
import { MaterialPopupComponent } from 'clericuzzi-lib/modules/material/components/material-popup/material-popup.component';
import { CrudPopupComponentModel } from 'clericuzzi-lib/modules/shared/crud/crud-popup/crud-popup.model';
import { ConfigPermissionUserItemComponent } from '../config-permission-user-item/config-permission-user-item.component';

@Component({
  selector: 'app-config-user-new',
  templateUrl: './config-user-new.component.html'
})
export class ConfigUserNewComponent extends HostBindedComponent implements OnInit
{
  @ViewChild(`permissionConfig`) private _PermissionComponent: FromToComponent<Permission>;
  @ViewChild('userConfigContainer', { read: ViewContainerRef }) _UserConfigContainer: ViewContainerRef;

  private _User: User = new User();
  private _Permission: UserPermission = new UserPermission();
  constructor(
    @Inject(MAT_DIALOG_DATA) public Definitions: CrudPopupComponentModel,
    private _Diag: MatDialog,
    private _Snack: MatSnackBar,
    private _Loader: LoaderService,
    private _Generator: ComponentGeneratorService,
    private _UserConfig: UserConfigService,
    private _PopupServices: MaterialPopupService,
  )
  {
    super(null);
  }

  ngOnInit()
  {
    this.InitFactories(this._Generator.Factory, this._Generator.Sanitizer);

    this.AddComponent(this._UserConfigContainer, this._User.GetColumnDefinition(UserColumns.Name), `Nome`, () => this.SendRequest());
    this.AddComponent(this._UserConfigContainer, this._User.GetColumnDefinition(UserColumns.Phone), `Telefone`, () => this.SendRequest());
    this.AddComponent(this._UserConfigContainer, this._User.GetColumnDefinition(UserColumns.Email), `Email`, () => this.SendRequest());
    let passwordComponent: TextInputComponent = this.AddComponent(this._UserConfigContainer, this._User.GetColumnDefinition(UserColumns.Password), `Senha`, () => this.SendRequest());
    passwordComponent.Type = `password`;

    this._User = this.Definitions.BaseObject as User;
    this.Sync(this._User);
    this._PermissionComponent.Initialize(new Permission(), ConfigPermissionUserItemComponent as any, `Permissões adicionadas`, `Permissões disponíveis`);
    this._PermissionComponent.Configure(UserPermissionColumns.UserId, UserPermissionColumns.PermissionId, PermissionTable, PermissionClass, UserPermissionClass, false);
    this._PermissionComponent.GetInitialData(null, `user-id`, ServerUrls.GetUrl(`user-permission/get-definitions`));
  }

  public SendRequest(): void
  {
    if (this.ValidateModel())
      MaterialPopupComponent.ConfirmationWithText(this._Diag, () => this._SendRequestConfirmation(), null, `Cadastrar novo usuário`, `Deseja realmente cadastrar o novo usuário?`, `Confirme a senha`, true);
    else
      SnackUtils.OpenError(this._Snack, `Preencha corretamente os campos`);
  }
  private _SendRequestConfirmation(): void
  {
    let typedPassword: string = this._PopupServices.GetConfirmationText();
    if (typedPassword != this._User.Password)
      SnackUtils.OpenError(this._Snack, `As senhas não conferem`);
    else
    {
      this._Loader.Show();
      this._UserConfig.NewUser(this._User, this._PermissionComponent.SelectedItems, this._SendRequestCallback.bind(this))
    }
  }
  private _SendRequestCallback(response: StatusResponseMessage): void
  {
    this._Loader.Hide();
    if (this.HandleResponse(response, this._Snack))
    {
      SnackUtils.Open(this._Snack, `Cadastro realizado com sucesso!`);
      this.CloseComponent();
    }
  }
}