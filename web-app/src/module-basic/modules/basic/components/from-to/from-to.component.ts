import { ComponentType } from '@angular/core/src/render3';
import { MatDialog, MatSnackBar } from '@angular/material';
import { Component, OnInit, OnDestroy, ViewChild, ComponentFactoryResolver } from '@angular/core';

// utils
import { SnackUtils } from 'clericuzzi-lib/utils/snack-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// models
import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model';
import { CustomField } from 'clericuzzi-lib/entities/crud/custom-field.model';
import { StatusResponseMessage } from 'clericuzzi-lib/entities/response/status-response-message.model';
import { BatchResponseMessageList } from 'clericuzzi-lib/entities/response/batch-response-message.model';

// services
import { FromToService } from 'clericuzzi-lib/services/from-to.service';
import { LoaderService } from 'clericuzzi-lib/services/loader.service';

// components
import { CrudSelectComponent } from 'clericuzzi-lib/modules/shared/crud/crud-select/crud-select.component';
import { HostBindedComponent } from 'clericuzzi-lib/modules/shared/util-components/host-binded/host-binded.component';
import { MaterialPopupComponent } from 'clericuzzi-lib/modules/material/components/material-popup/material-popup.component';

// behaviours
import { SelectListItem } from 'clericuzzi-lib/business/behaviours/select-list-item.behaviour';
import { ServerUrls } from 'clericuzzi-lib/utils/server-urls';

@Component({
  selector: 'app-from-to',
  templateUrl: './from-to.component.html'
})
export class FromToComponent<T extends Crudable> extends HostBindedComponent implements OnInit, OnDestroy
{
  @ViewChild(`availableItems`) private _AvailableItemsComponent: CrudSelectComponent<T>;
  @ViewChild(`nonAvailableItems`) private _NonAvailableItemsComponent: CrudSelectComponent<T>;

  private _BaseObject: T;

  private _Title: string;
  private _ItemProperty: string;
  private _RelationshipProperty: string;
  private _ObjectModelName: string;

  private _ItemId: number;
  private _AssociatedIds: number[];
  private _AvailableItems: T[];

  private _SelectedItems: T[];
  private _UnselectedItems: T[];

  private _SourceType: string;
  private _TargetType: string;

  private _UpdateUrl: string;
  private _ConfirmationTitle: string;
  private _ConfirmationMessage: string;
  private _UpdateSuccessMessage: string;

  public ViewOnly: boolean = false;
  private _HasButtonBar: boolean = false;
  constructor(
    private _Diag: MatDialog,
    private _Snack: MatSnackBar,
    private _Loader: LoaderService,
    private _FromTo: FromToService,
  )
  {
    super(null);
  }

  ngOnInit()
  {
  }
  ngOnDestroy(): void
  {
  }

  public get Title(): string
  {
    return ObjectUtils.GetValue(this._Title);
  }
  public get HasTitle(): boolean
  {
    return !ObjectUtils.NullUndefinedOrEmpty(this._Title);
  }
  public get HasButtonBar(): boolean
  {
    return this._HasButtonBar;
  }
  public get SelectedItems(): number[]
  {
    return this._SelectedItems.map(i => i[`Id`]);
  }
  private _DelayedInit(): void
  {
    this._AvailableItemsComponent.SetMultiSelect(true);
    this._NonAvailableItemsComponent.SetMultiSelect(true);
  }

  public Initialize(baseObject: T, dataTemplate: ComponentType<SelectListItem<T>>, titleAvailable: string, titleAssociated: string): void
  {
    this._BaseObject = baseObject;

    this._NonAvailableItemsComponent.SetBaseObject(this._BaseObject);
    this._NonAvailableItemsComponent.HidePagination();
    this._NonAvailableItemsComponent.Title = titleAssociated;
    this._NonAvailableItemsComponent.DataTemplate = dataTemplate;

    this._AvailableItemsComponent.SetBaseObject(this._BaseObject);
    this._AvailableItemsComponent.HidePagination();
    this._AvailableItemsComponent.Title = titleAvailable;
    this._AvailableItemsComponent.DataTemplate = dataTemplate;

    this._AvailableItemsComponent.CustomSelect = this._NonAvailableItemsComponent.CustomSelect = null;
    this._AvailableItemsComponent.LoadsOnStart = this._NonAvailableItemsComponent.LoadsOnStart = false;
    this._AvailableItemsComponent.ShowsHeaderAction = this._NonAvailableItemsComponent.ShowsHeaderAction = false;

    setTimeout(() => this._DelayedInit(), 250);
  }

  public Configure(itemProperty: string, relationshipProperty: string, baseObjectName: string, sourceType: string, targetType: string, hasButtonBar: boolean = true, viewOnly: boolean = false, title: string = null): void
  {
    this._Title = title;
    this.ViewOnly = viewOnly;
    this._HasButtonBar = hasButtonBar;

    this._SourceType = sourceType;
    this._TargetType = targetType;

    this._ItemProperty = itemProperty;
    this._ObjectModelName = baseObjectName;
    this._RelationshipProperty = relationshipProperty;
  }
  public ConfigureUpdate(title: string, message: string, successMessage: string): void
  {
    this._ConfirmationTitle = title;
    this._ConfirmationMessage = message;
    this._UpdateSuccessMessage = successMessage;
  }

  public GetInitialData(paramValue: number, paramName: string, url: string, ...customFields: CustomField[]): void
  {
    this._ItemId = paramValue;
    this._UpdateUrl = ServerUrls.GetUrl(`from-to/update-relationship`);
    this._FromTo.GetRelationship(url, paramName, this._ItemId, this._InitialDataCallback.bind(this), ...customFields);
  }
  private _InitialDataCallback(response: BatchResponseMessageList): void
  {
    this._AvailableItems = response.GetList(this._ObjectModelName, this._BaseObject);
    this._AssociatedIds = response.GetArrayItems(`associated-ids`);

    this._UnselectedItems = this._AvailableItems.filter(i => !this._AssociatedIds.find(j => i[`Id`] == j));
    this._SelectedItems = this._AvailableItems.filter(i => this._AssociatedIds.find(j => i[`Id`] == j));

    this._FillLists();
  }
  private _FillLists(): void
  {
    this._AvailableItemsComponent.SetItems(this._UnselectedItems);
    this._NonAvailableItemsComponent.SetItems(this._SelectedItems);
  }

  public SaveChanges(): void
  {
    MaterialPopupComponent.Confirmation(this._Diag, () => this._SaveChanges(), null, this._ConfirmationTitle, this._ConfirmationMessage);
  }
  private _SaveChanges(): void
  {
    this._Loader.Show();
    this._FromTo.UpdateRelationship(this._UpdateUrl
      , this._ItemId
      , this.SelectedItems
      , this._SourceType
      , this._TargetType
      , this._ItemProperty
      , this._RelationshipProperty
      , this._SaveChangesCallback.bind(this));
  }
  private _SaveChangesCallback(response: StatusResponseMessage): void
  {
    this._Loader.Hide();
    if (this.HandleResponse(response, this._Snack))
    {
      SnackUtils.Open(this._Snack, this._UpdateSuccessMessage);
      this.CloseComponent();
    }
  }

  public AddAll(): void
  {
    this._SelectedItems = this._AvailableItems;
    this._UnselectedItems = [];
    this._FillLists();
  }
  public RemoveAll(): void
  {
    this._UnselectedItems = this._AvailableItems;
    this._SelectedItems = [];
    this._FillLists();
  }
  public AddSelected(): void
  {
    let selectedIds: number[] = this._AvailableItemsComponent.SelectedItems.map(i => i[`Id`]);
    var unavailableItems: T[] = this._NonAvailableItemsComponent.Items;
    if (!ObjectUtils.NullUndefinedOrEmptyArray(unavailableItems))
      selectedIds = selectedIds.concat(unavailableItems.map(i => i[`Id`]));

    this._UnselectedItems = this._AvailableItems.filter(i => !selectedIds.find(j => j == i[`Id`]));
    this._SelectedItems = this._AvailableItems.filter(i => selectedIds.find(j => j == i[`Id`]));

    this._FillLists();
  }
  public RemoveSelected(): void
  {
    let selectedIds: number[] = this._NonAvailableItemsComponent.SelectedItems.map(i => i[`Id`]);
    var unavailableItems: T[] = this._AvailableItemsComponent.Items;
    if (!ObjectUtils.NullUndefinedOrEmptyArray(unavailableItems))
      selectedIds = selectedIds.concat(unavailableItems.map(i => i[`Id`]));

    this._UnselectedItems = this._AvailableItems.filter(i => selectedIds.find(j => j == i[`Id`]));
    this._SelectedItems = this._AvailableItems.filter(i => !selectedIds.find(j => j == i[`Id`]));

    this._FillLists();
  }
}