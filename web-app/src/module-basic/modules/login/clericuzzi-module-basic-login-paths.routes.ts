export class BasicModuleLoginPaths
{
    public static INDEX: string = "login";

    public static AUTH_PATH: string = "login/auth";
    public static AUTH_ROUTE: string = "auth";
    public static PASSWORD_CHANGE_PATH: string = "login/password-change";
    public static PASSWORD_CHANGE_ROUTE: string = "password-change";
    public static PASSWORD_RETRIEVE_PATH: string = "login/password-retrieve";
    public static PASSWORD_RETRIEVE_ROUTE: string = "password-retrieve";
}