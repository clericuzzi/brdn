import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
// utils
import { BasicModuleLoginPaths } from './clericuzzi-module-basic-login-paths.routes';
// models
// services
// components
import { LoginAuthComponent } from './login-auth/login-auth.component';
import { BasicModuleLoginComponent } from './clericuzzi-module-basic-login.component';
import { LoginPasswordChangeComponent } from './login-password-change/login-password-change.component';
import { LoginPasswordRetrieveComponent } from './login-password-retrieve/login-password-retrieve.component';
// modules
import { SharedModule } from 'clericuzzi-lib/modules/shared/shared.module';

// Routing
export const routes =
[
  {
    path: BasicModuleLoginPaths.INDEX, component: BasicModuleLoginComponent
    , children:
    [
      { path: BasicModuleLoginPaths.AUTH_PATH, component: LoginAuthComponent },
      { path: BasicModuleLoginPaths.PASSWORD_CHANGE_PATH, component: LoginPasswordChangeComponent },
      { path: BasicModuleLoginPaths.PASSWORD_RETRIEVE_PATH, component: LoginPasswordRetrieveComponent },
    ]
  },
];

@NgModule({
  imports: [
    SharedModule,

    RouterModule.forChild(routes)
  ],
  exports: [
    BasicModuleLoginComponent
  ],
  declarations: [
    BasicModuleLoginComponent,
    LoginAuthComponent,
    LoginPasswordChangeComponent,
    LoginPasswordRetrieveComponent,
  ]
})
export class ClericuzziModuleBasicLogin { }