import { Chart } from 'chart.js';
import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import ChartDataLabels from 'chartjs-plugin-datalabels';

// models

// models
import { BiChartDataSet } from './bi-chart-dataset.model';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ChartItem } from 'clericuzzi-bi/entities/chart-item.model';
import { ColorUtils } from 'clericuzzi-lib/utils/color-utils';

@Component({
  selector: 'app-bi-chart',
  templateUrl: './bi-chart.component.html'
})
export class BiChartComponent implements OnInit
{
  @ViewChild(`canvas`) public Canvas: ElementRef;

  @Input() public Type: string;
  @Input() public Title: string;
  @Input() public Labels: string[] = [];
  @Input() public Options: any = { responsive: true, maintainAspectRatio: false, scales: { xAxes: [{}], yAxes: [{ ticks: { beginAtZero: true } }] } };
  @Input() public DataSets: BiChartDataSet[] = [];
  @Input() public BackgroundColors: string[] = [];

  public Chart: Chart;
  public _HasLabels: boolean = false;

  constructor()
  {
  }

  ngOnInit()
  {
    this._InitChart();
  }
  _InitChart(): void
  {
    this.Draw();
  }
  public Clear(): void
  {
    const context = this.Canvas.nativeElement.getContext('2d');
    context.clearRect(0, 0, this.Canvas.nativeElement.width, this.Canvas.nativeElement.height);

    if (this.Chart != null)
      this.Chart.destroy();
  }
  public Draw(): void
  {
    this.Clear();
    const chartObject: any = {
      type: this.Type,
      Title: this.Title,
      data: {
        labels: this.Labels,
        datasets: this.DataSets,
      },
      options: this.Options,
      backgroundColor: this.BackgroundColors,
    };

    // if (this._HasLabels)
    //   chartObject[`plugins`] = [ChartDataLabels];
    // else
    // {
    //   if (!ObjectUtils.NullUndefinedOrEmpty(chartObject.options.plugins))
    //     setTimeout(() => delete chartObject.options.plugins.datalabels, 450);
    //   setTimeout(() => delete chartObject[`plugins`], 450);
    // }

    this.Chart = new Chart(this.Canvas.nativeElement.getContext('2d'), chartObject);
  }
  public StartAtZero(): void
  {
    const yAxes: any = this.Options[`scales`][`yAxes`][0];
    delete yAxes[`ticks`];
    yAxes[`ticks`] = { ticks: { beginAtZero: true } };
  }

  public AddClick(action: () => void): void
  {
    if (ObjectUtils.NullOrUndefined(this.Options))
      this.Options = { responsive: true, maintainAspectRatio: false, scales: { xAxes: [{}], yAxes: [{ ticks: { beginAtZero: true } }] } };

    this.Options[`onClick`] = action;
  }
  public HideAxes(): void
  {
    this.Options[`scales`] = { xAxes: { display: false }, yAxes: { display: false } };
  }
  public HideLegend(): void
  {
    if (ObjectUtils.NullOrUndefined(this.Options))
      this.Options = {};

    delete this.Options[`legend`];
    this.Options[`legend`] = { display: false };
  }
  public HideGridLines(): void
  {
    const xAxes: any = this.Options[`scales`][`xAxes`][0];
    delete xAxes[`gridLines`];
    xAxes[`gridLines`] = { display: false };

    const yAxes: any = this.Options[`scales`][`yAxes`][0];
    delete yAxes[`gridLines`];
    yAxes[`gridLines`] = { display: false };
  }

  public SetTitle(title: string): void
  {
    this.Options[`title`] = { display: true, text: title };
  }
  public SetAxesNames(xAxesName: string, yAxesName: string): void
  {
    const xAxes: any = this.Options[`scales`][`xAxes`][0];
    delete xAxes[`scaleLabel`];
    xAxes[`scaleLabel`] = { display: true, labelString: xAxesName };

    const yAxes: any = this.Options[`scales`][`yAxes`][0];
    delete yAxes[`scaleLabel`];
    yAxes[`scaleLabel`] = { display: true, labelString: yAxesName };
  }

  public ClearLabel(): void
  {
    this._HasLabels = false;
    delete this.Options[`plugins`];
  }
  public SetLabelColor(labelColor: string): void
  {
    let plugin: any = this.Options[`plugins`];
    if (ObjectUtils.NullOrUndefined(plugin))
      plugin = { datalabels: {} };

    plugin.datalabels.color = labelColor;
    this.Options[`plugins`] = plugin;

    this._HasLabels = true;
  }

  public BarAddDataset(title: string, dataset: ChartItem[]): void
  {
    if (ObjectUtils.NullUndefinedOrEmptyArray(dataset))
      return;

    this.DataSets = [];
    const labels: string[] = dataset.map(i => i.Item).filter(ObjectUtils.Distinct);
    this.DataSets = [];
    for (const label of labels)
    {
      const chartDataset: BiChartDataSet = new BiChartDataSet();

      chartDataset.Items = dataset.filter(i => i.Item === label);
      chartDataset.Title = label;
      const color: string = ObjectUtils.NullUndefinedOrEmpty(chartDataset.Items[0].Color) ? ColorUtils.RandomColor() : chartDataset.Items[0].Color;
      while (chartDataset.borderColor.length < chartDataset.Items.length)
      {
        chartDataset.borderColor.push(color);
        chartDataset.backgroundColor.push(color);
      }
      for (const item of chartDataset.Items)
        item.Color = item[`backgroundColor`] = color;
      chartDataset.Update();

      this.DataSets.push(chartDataset);
    }

    this.Type = `bar`;
    this.Labels = dataset.map(i => i.LabelX).filter(ObjectUtils.Distinct);
    this.SetTitle(title);

    this.Draw();
  }
  public LineAddDataset(title: string, dataset: ChartItem[]): void
  {
    if (ObjectUtils.NullUndefinedOrEmptyArray(dataset))
      return;

    this.DataSets = [];
    const labels: string[] = dataset.map(i => i.Item).filter(ObjectUtils.Distinct);
    this.DataSets = [];
    for (const label of labels)
    {
      const chartDataset: BiChartDataSet = new BiChartDataSet();

      chartDataset.Items = dataset.filter(i => i.Item === label);
      chartDataset.Title = label;
      const color: string = ObjectUtils.NullUndefinedOrEmpty(chartDataset.Items[0].Color) ? ColorUtils.RandomColor() : chartDataset.Items[0].Color;
      chartDataset.borderColor.push(color);
      chartDataset.backgroundColor.push(color);
      chartDataset.Update();

      this.DataSets.push(chartDataset);
    }

    this.Type = `line`;
    this.Labels = dataset.map(i => i.LabelX).filter(ObjectUtils.Distinct);
    this.SetTitle(title);

    this.Draw();
  }
  public PieAddDataset(title: string, dataset: ChartItem[]): void
  {
    if (ObjectUtils.NullUndefinedOrEmptyArray(dataset))
      return;

    this.Labels = [];
    this.DataSets = [];
    const total: number = dataset.reduce((sum, current) => sum + current.Amount, 0);
    const labels: string[] = dataset.map(i => i.Item).filter(ObjectUtils.Distinct);
    const dataSet: BiChartDataSet = new BiChartDataSet();
    dataSet.backgroundColor = dataset.map(i => i.Color).filter(ObjectUtils.Distinct);
    for (const label of labels)
    {
      const setItems: ChartItem[] = dataset.filter(i => i.Item === label);
      const newItem: ChartItem = new ChartItem();
      if (setItems.length > 0)
      {
        this.Labels.push(label);

        newItem.Amount = dataset.filter(i => i.Item === label).reduce((sum, current) => sum + current.Amount, 0);
        newItem.Id = setItems[0].Id;
        newItem.To = setItems[0].To;
        newItem.Date = setItems[0].Date;
        newItem.Item = setItems[0].Item;
        newItem.From = setItems[0].From;
        newItem.Color = setItems[0].Color;
        newItem.LabelX = setItems[0].LabelX;
        newItem.LabelY = setItems[0].LabelY;

        dataSet.data.push(newItem.Amount);
        dataSet.Items.push(newItem);
      }
    }
    this.Type = `pie`;
    this.DataSets.push(dataSet);
    if (ObjectUtils.NullUndefinedOrEmpty(title))
      title = `Total: ${total}`;
    else
      title = `${title} (${total})`;
    this.SetTitle(title);

    this.Draw();
  }
  public SetLabelDateRange(from: Date, to: Date): void
  {
    const labels: string[] = [];
    const upperLimit: Date = new Date(to.getFullYear(), to.getMonth(), to.getDate(), 23, 59, 59, 999);
    const currentDate: Date = new Date(from.getFullYear(), from.getMonth(), from.getDate(), 0, 0, 0, 0);
    while (currentDate < upperLimit)
    {
      labels.push(StringUtils.TransformDateToFormat(currentDate, `DD/MM`));
      currentDate.setDate(currentDate.getDate() + 1);
    }

    this.Labels = labels;
  }
}
