import { ChartItem } from 'clericuzzi-bi/entities/chart-item.model';

export class BiChartDataSet
{
  public Title: string;
  public Items: ChartItem[] = [];

  public data: any[] = [];

  public fill: boolean = false;
  public label: string;
  public borderColor: string[] = [];
  public backgroundColor: string[] = [];
  public Update(): void
  {
    this.data = this.Items.map(i => i.Amount);
    this.label = this.Title;
  }
}
