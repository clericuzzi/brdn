export class BiChartDataSetItem
{
  constructor(
    public Key: string = null,
    public Value: number = null,
  )
  {
  }
}
