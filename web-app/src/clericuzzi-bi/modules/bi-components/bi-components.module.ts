import { NgModule } from '@angular/core';

// clericuzzi angular
import { MaterialModule } from 'clericuzzi-lib/modules/material/material.module';

// components
import { BiCardComponent } from './bi-card/bi-card.component';
import { BiChartComponent } from './bi-chart/bi-chart.component';
import { BiRankingComponent } from './bi-ranking/bi-ranking.component';

@NgModule({
  imports: [
    MaterialModule,
  ],
  exports: [
    BiCardComponent,
    BiChartComponent,
    BiRankingComponent,
  ],
  declarations: [
    BiCardComponent,
    BiChartComponent,
    BiRankingComponent,
  ],
  entryComponents: [
  ]
})
export class BiComponentsModule { }
