export class MonthlyChartItemModel
{
  constructor(
      public Date: Date,
      public Amount: number,
  )
  {
  }
}
