export class ChartItem
{
  public static SimpleItem(item: string, amount: number, color: string = null): ChartItem
  {
    const chartItem: ChartItem = new ChartItem(null, item, color, null, null, null, null, null, amount, null);

    return chartItem;
  }
  public static SimpleLabel(labelX: string, amount: number, color: string = null): ChartItem
  {
    const chartItem: ChartItem = new ChartItem(null, null, color, labelX, null, null, null, null, amount, null);

    return chartItem;
  }

  constructor(
    public Id: number = null,
    public Item: string = null,
    public Color: string = null,
    public LabelX: string = null,
    public LabelY: string = null,
    public To: Date = null,
    public Date: Date = null,
    public From: Date = null,
    public Amount: number = null,
    public Percentage: number = null,
  )
  {
  }
}
