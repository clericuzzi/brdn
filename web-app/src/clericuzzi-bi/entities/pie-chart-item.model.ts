export class PieChartItemModel
{
  constructor(
    public Text: string,
    public Amount: number,
    public Percentage: number,
  )
  {
  }
}
