export class TupleModel<T>
{
    constructor(
        public Key: T,
        public Value: string,
    )
    {
    }
}