// models
import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

export class SelectListItemModel<T>
{
    constructor(
        public Model: T = null,
        public Selected: boolean = false,
        public KeyProperty: string = `Id`,
    )
    {
    }

    public get Id(): number
    {
        if (ObjectUtils.NullOrUndefined(this.Model))
            return 0;
        else
            return this.Model[this.KeyProperty];
    }
}
