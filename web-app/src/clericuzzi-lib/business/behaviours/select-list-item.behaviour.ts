import { Subject } from "rxjs";
import { Injectable } from "@angular/core";

// models
import { Crudable } from "clericuzzi-lib/entities/crud/crudable.model";
import { SelectListItemModel } from "../models/select-list-item.model";

// components
import { HostBindedComponent } from "clericuzzi-lib/modules/shared/util-components/host-binded/host-binded.component";

export abstract class SelectListItem<T> extends HostBindedComponent
{
    /** a flag value indicating if this instance is the last child */
    public LastChild: boolean;
    /** a flag value indicating if this instance is the first child */
    public FirstChild: boolean;
    /** a collection of custom css classes to be appended to the resulting object class */
    public CustomClasses: string[] = [];
    /** the key property in the item's model class */
    public KeyProperty: string = `Id`;

    /** if TRUE the item cannot be selected */
    public NonSelectable: boolean = false;
    /** the model to be drawn */
    public Model: T;
    /** the item's index, this determines if the item is EVEN or ODD */
    public Index: number;
    /** if TRUE, means that this item was selected by the user */
    public Selected: boolean;
    /** a selection update event that is fired when the component is clicked */
    public OnSelectionUpdated: Subject<SelectListItemModel<T>> = new Subject<SelectListItemModel<T>>();
    /** an removal event that is fired when the component must be removed */
    public OnComponentRemoved: Subject<void> = new Subject<void>();

    /** Passes the current selection model to the list container */
    public get SelectionModel(): SelectListItemModel<T>
    {
        return new SelectListItemModel<T>(this.Model, this.Selected, this.KeyProperty);
    }
    /** the current component css class */
    public get RowClass(): string
    {
      let classValue: string = `select-body-item crud-select-row full-size`;
      for (let classItem of this.CustomClasses)
        classValue = `${classItem} ${classValue}`;

      let selectionClass: string = this.Selected ? `select-body-item-selected ` : ``;
      if (this.Index % 2 == 1)
        return `${classValue} ${selectionClass}crud-select-row-odd`;
      else
        return `${classValue} ${selectionClass}crud-select-row-even`;
    }

    /** mouse click handler */
    public ItemClicked(): void
    {
      if (!this.NonSelectable)
        this.Selected = !this.Selected;
      else
        this.Selected = false;

      this.OnSelectionUpdated.next(this.SelectionModel);
    }
    public SetSelection(selected: boolean): void
    {
        this.Selected = selected;
    }
}