import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable()
export class LoaderService
{
  public OnHide: Subject<void> = new Subject<void>();
  public OnShow: Subject<void> = new Subject<void>();
  
  constructor() { }
  
  public Hide(): void { this.OnHide.next(); }
  public Show(): void { this.OnShow.next(); }
}