import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable()
export class PageTitleService
{
  public Title: string;
  public OnTitleChanged: Subject<string> = new Subject<string>();
  constructor()
  {
  }

  public SetTitle(title: string): void
  {
      this.Title = title;
      this.OnTitleChanged.next(title);
  }
}