import { Injectable } from '@angular/core';

// utils
import { JsonUtils } from '../utils/json-utils';
import { ObjectUtils } from '../utils/object-utils';

// models
import { CustomField } from '../entities/crud/custom-field.model';
import { RequestMessage } from '../entities/request/request-message.model';
import { ComboInputValueModel } from '../modules/shared/form-controls/custom-inputs/combo-input/combo-input-value.model';
import { BatchResponseMessageList } from '../entities/response/batch-response-message.model';

// services
import { HttpService } from './http.service';
import { UserDataService } from './user-data.service';

// components
import { AutocompleteInputComponent } from 'clericuzzi-lib/modules/shared/form-controls/autocomplete-input/autocomplete-input.component';
import { Subject } from 'rxjs';
import { ServerUrls } from 'clericuzzi-lib/utils/server-urls';
import { ComboInputComponent } from 'clericuzzi-lib/modules/shared/form-controls/custom-inputs/combo-input/combo-input.component';

@Injectable()
export class AutoCompleteFetchService
{
  public OnListUpdated: Subject<string> = new Subject<string>();
  private _List: any = {};
  constructor(
    private _Http: HttpService,
    private _UserData: UserDataService,
  )
  {
  }

  public GetData(dataKey: string): ComboInputValueModel<number>[]
  {
    return this._List[dataKey];
  }
  public StoreData(url: string, dataKey: string, ...customValues: CustomField[]): void
  {
    const request: RequestMessage = new RequestMessage(url);
    if (!ObjectUtils.NullUndefinedOrEmptyArray(customValues))
    {
      for (const customValue of customValues)
        if (!ObjectUtils.NullOrUndefined(customValue))
          request.Add(customValue.Key, customValue.Value);
    }
    request.AddId(this._UserData.Id);

    const response: BatchResponseMessageList = new BatchResponseMessageList();
    this._Http.Post(request, response, () => this._StoreDataCallback(response, dataKey));
  }
  private _StoreDataCallback(response: BatchResponseMessageList, dataKey: string): void
  {
    const items: any[] = response.GetArrayItems(dataKey);
    const entities: ComboInputValueModel<number>[] = JsonUtils.ParseList(items, new ComboInputValueModel<number>());
    this._List[dataKey] = entities;

    setTimeout(() => this.OnListUpdated.next(dataKey), 500);
  }

  public FetchData<T>(url: string, component: AutocompleteInputComponent<T>, dataKey: string, ...customValues: CustomField[]): void
  {
    const request: RequestMessage = new RequestMessage(url);
    if (!ObjectUtils.NullUndefinedOrEmptyArray(customValues))
    {
      for (const customValue of customValues)
        if (!ObjectUtils.NullOrUndefined(customValue))
          request.Add(customValue.Key, customValue.Value);
    }

    component.Disable();
    component.Loading = true;
    const response: BatchResponseMessageList = new BatchResponseMessageList();
    this._Http.Post(request, response, () => this._FetchDataCallback(response, component, dataKey));
  }
  private _FetchDataCallback<T>(response: BatchResponseMessageList, component: AutocompleteInputComponent<T>, dataKey: string): void
  {
    component.SetItems(this.GetAutocompleteData(dataKey, response));
    component.Sync();
  }

  public FetchAutocompleteData(url: string, callbackFunction: Function, ...customValues: CustomField[]): void
  {
    const request: RequestMessage = new RequestMessage(url);
    if (!ObjectUtils.NullUndefinedOrEmptyArray(customValues))
    {
      for (const customValue of customValues)
        if (!ObjectUtils.NullOrUndefined(customValue))
          request.Add(customValue.Key, customValue.Value);
    }
    request.AddId(this._UserData.Id);

    const response: BatchResponseMessageList = new BatchResponseMessageList();
    this._Http.Post(request, response, () => callbackFunction(response));
  }
  public GetAutocompleteData<T>(key: string, response: BatchResponseMessageList): ComboInputValueModel<T>[]
  {
    if (!ObjectUtils.NullOrUndefined(response) && !ObjectUtils.NullOrUndefined(response.Entities))
    {
      const items: any[] = response.GetArrayItems(key);
      const entities: ComboInputValueModel<T>[] = JsonUtils.ParseList(items, new ComboInputValueModel<T>());
      return entities;
    }
    else
      return null;
  }
}
