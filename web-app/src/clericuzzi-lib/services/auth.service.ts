import { Injectable } from '@angular/core';

// services
import { UserDataService } from 'clericuzzi-lib/services/user-data.service';

@Injectable()
export class AuthService
{
  constructor(
    private UserData: UserDataService,)
  {
  }

  public get UserRole(): string
  {
    return this.UserData.UserPermission;
  }
  public CheckPermission(priority: number): boolean
  {
    return this.UserData.CheckPermission(priority);
  }
}