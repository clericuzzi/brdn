import { Injectable } from '@angular/core';

// utils
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// models
import { User } from 'clericuzzi-lib/entities/models/user.model';
import { Account } from 'clericuzzi-lib/entities/models/account.model';

// services
import { LoginService } from 'clericuzzi-lib/services/login.service';
import { AccountsService } from 'clericuzzi-lib/services/accounts.service';
import { UserSessionService } from 'clericuzzi-lib/services/user-session.service';
import { ServerUrls } from 'clericuzzi-lib/utils/server-urls';

@Injectable()
export class UserDataService
{
  constructor(
    public Session: UserSessionService,
    public Accounts: AccountsService,
    public LoginService: LoginService,
  )
  {
  }

  public get IsLogged(): boolean { return this.Session.Logged; }

  public get Id(): number { return this.Session.Id; }
  public get UserNameFull(): string { return this.Session.NameFull; }
  public get UserNameLast(): string { return this.Session.NameLast; };
  public get UserNameFirst(): string { return this.Session.NameFull; }
  public get UserPermission(): string { return this.Session.PermissionName; }

  public get CurrentAccount(): Account { return this.Accounts.Current; }
  public get CurrentAccountId(): number { return this.Accounts.CurrentId; }
  public get CurrentAccountName(): string { return this.Accounts.CurrentName; }
  public get HasMultipleAccounts(): boolean
  {
    if (!ObjectUtils.NullOrUndefined(this.Accounts) && !ObjectUtils.NullOrUndefined(this.Accounts.All))
      return this.Accounts.All.length > 1;
    else
      return false;
  }

  public AccountsLoaded(accounts: Account[]): void
  {
    this.Accounts.AccountsLoaded(accounts);
  }

  public Init(): void
  {
    this.Accounts.DefineBaseUrl(ServerUrls.SERVER_ADDRESS);
    this.LoginService.DefineBaseUrl(ServerUrls.SERVER_ADDRESS);
    this.LoginService.LoadSession();
  }

  public Login(user: User): void
  {
    this.LoginService.Login(user);
  }
  public Validate(): void
  {
    this.Accounts.Clear();
    this.LoginService.InitValidate();
  }

  public get IsWorkerLevel(): boolean
  {
    return !this.CheckPermissionRangeUnder(300);
  }
  public get IsManagementLevel(): boolean
  {
    return this.CheckPermissionRangeOver(200) && this.CheckPermissionRangeUnder(299) || this.IsAdminLevel;
  }
  public get IsAdminLevel(): boolean
  {
    return this.CheckPermissionRangeUnder(199);
  }

  public CheckPermission(permission: number): boolean
  {
    if (ObjectUtils.NullOrUndefined(this.Session) || ObjectUtils.NullOrUndefined(this.Session.PermissionList))
      return false;

    if (!ObjectUtils.NullOrUndefined(this.Session.PermissionList.find(i => i == permission)))
      return true;

    return false;
  }
  public CheckPermissionList(permissionList: number[]): boolean
  {
    if (ObjectUtils.NullOrUndefined(this.Session) || ObjectUtils.NullOrUndefined(this.Session.PermissionList))
      return false;

    for (let permission of permissionList)
      if (!ObjectUtils.NullOrUndefined(this.Session.PermissionList.find(i => i == permission)))
        return true;

    return false;
  }
  public CheckForbiddenPermission(forbiddenPermission: number): boolean
  {
    if (ObjectUtils.NullOrUndefined(this.Session) || ObjectUtils.NullOrUndefined(this.Session.PermissionList))
      return false;

    if (!ObjectUtils.NullOrUndefined(this.Session.PermissionList.find(i => i == forbiddenPermission)))
      return true;

    return false;
  }
  public CheckForbiddenPermissionList(forbiddenPermissionList: number[]): boolean
  {
    if (ObjectUtils.NullOrUndefined(this.Session) || ObjectUtils.NullOrUndefined(this.Session.PermissionList))
      return false;

    for (let forbiddenPermission of forbiddenPermissionList)
      if (!ObjectUtils.NullOrUndefined(this.Session.PermissionList.find(i => i == forbiddenPermission)))
        return true;

    return false;
  }
  public CheckPermissionRange(from: number, to: number): boolean
  {
    return this.CheckPermissionRangeOver(from) && this.CheckPermissionRangeUnder(to);
  }
  public CheckPermissionRangeOver(permission: number): boolean
  {
    if (ObjectUtils.NullOrUndefined(this.Session) || ObjectUtils.NullOrUndefined(this.Session.PermissionList))
      return false;

    if (!ObjectUtils.NullOrUndefined(this.Session.PermissionList.find(i => i >= permission)))
      return true;

    return false;
  }
  public CheckPermissionRangeUnder(permission: number): boolean
  {
    if (ObjectUtils.NullOrUndefined(this.Session) || ObjectUtils.NullOrUndefined(this.Session.PermissionList))
      return false;

    if (!ObjectUtils.NullOrUndefined(this.Session.PermissionList.find(i => i <= permission)))
      return true;

    return false;
  }
}