import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';

// models
import { Account } from 'clericuzzi-lib/entities/models/account.model';
import { RequestMessageDefaults } from 'clericuzzi-lib/entities/request/request-message.model';

// services
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

@Injectable()
export class AccountsService
{
  public OnAccountChanged: Subject<void> = new Subject<void>();
  public OnAccountsLoaded: Subject<void> = new Subject<void>();

  public All: Account[];
  public Current: Account;
  public CurrentId: number;
  public CurrentName: string;
  public DefaultName: string;

  public LoadUrl: string;
  public DefineBaseUrl(baseUrl: string): void
  {
    this.LoadUrl = `${baseUrl}account/load`;
  }

  public HasAccounts: boolean = false;
  public MultipleAccounts: boolean = false;

  constructor()
  {
  }

  public AccountsLoaded(accounts: Account[]): void
  {
    this.All = accounts;
    if (ObjectUtils.NullOrUndefined(this.Current) && !ObjectUtils.NullUndefinedOrEmptyArray(this.All))
      this.Select(this.All[0]);
  }

  public Clear(): void
  {
    this.Current = null;
    this.CurrentId = null;
    this.CurrentName = null;
  }
  public Select(account: Account): void
  {
    this.Clear();
    if (account != null)
    {
      this.Current = account;
      this.CurrentId = account.Id;
      this.CurrentName = account.Name;

      RequestMessageDefaults.AccountId = account.Id;
      this.OnAccountChanged.next();
    }
  }
  public SelectAccount(accountId: number): void
  {
    let account: Account = this.All.find(i => i.Id == accountId);
    if (!ObjectUtils.NullOrUndefined(account))
      this.Select(account);
  }
  public Dispose(): void
  {
    this.All = [];
    this.HasAccounts = false;
    this.Clear();
  }
}