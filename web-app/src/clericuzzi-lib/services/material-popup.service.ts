import { Injectable } from '@angular/core';

@Injectable()
export class MaterialPopupService
{
  private _ConfirmationText: string;
  public GetConfirmationText(): string
  {
      let value: string = this._ConfirmationText;
      this.ConfirmationText = null;

      return value;
  }
  public set ConfirmationText(value: string) { this._ConfirmationText = value; }

  constructor()
  {
  }
}