import { Injectable } from '@angular/core';

// utils
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// models
import { CustomField } from 'clericuzzi-lib/entities/crud/custom-field.model';
import { RequestMessage } from 'clericuzzi-lib/entities/request/request-message.model';
import { StatusResponseMessage } from 'clericuzzi-lib/entities/response/status-response-message.model';
import { BatchResponseMessageList } from 'clericuzzi-lib/entities/response/batch-response-message.model';

// services
import { HttpService } from './http.service';

@Injectable()
export class FromToService
{
  constructor(
    private _Http: HttpService,
  )
  {
  }

  public GetRelationship(url: string, paramName: string, paramValue: any, callback: Function, ... customFields: CustomField[]): void
  {
    let requestMesage: RequestMessage = new RequestMessage(url);
    if (!ObjectUtils.NullOrUndefined(paramValue))
      requestMesage.Add(paramName, paramValue);

    if (!ObjectUtils.NullUndefinedOrEmptyArray(customFields))
      for (let customField of customFields)
        requestMesage.Add(customField.Key, customField.Value);

    let response: BatchResponseMessageList = new BatchResponseMessageList();
    this._Http.Post(requestMesage, response, () => callback(response));
  }
  public UpdateRelationship(url: string, itemId: number, associatedIds: number[], sourceType: string, targetType: string, baseProperty: string, relationalProperty: string, callback: Function, ... customFields: CustomField[]): void
  {
    let requestMesage: RequestMessage = new RequestMessage(url);
    requestMesage.Add(`item-id`, itemId);
    requestMesage.Add(`source-type`, sourceType);
    requestMesage.Add(`target-type`, targetType);
    requestMesage.Add(`base-property`, baseProperty);
    requestMesage.Add(`associated-ids`, associatedIds);
    requestMesage.Add(`relational-property`, relationalProperty);

    if (!ObjectUtils.NullUndefinedOrEmptyArray(customFields))
      for (let customField of customFields)
        requestMesage.Add(customField.Key, customField.Value);

    let response: StatusResponseMessage = new StatusResponseMessage();
    this._Http.Post(requestMesage, response, () => callback(response));
  }
}
