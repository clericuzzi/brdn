import { DomSanitizer } from '@angular/platform-browser';
import { Injectable, ComponentFactoryResolver, ViewContainerRef, ComponentRef } from '@angular/core';

@Injectable()
export class ComponentGeneratorService
{
  constructor(
    public Factory: ComponentFactoryResolver,
    public Sanitizer: DomSanitizer,
  )
  {
  }

  public CmpRef: ComponentRef<any>;
  public Initialize(factory: ComponentFactoryResolver, sanitizer: DomSanitizer): void
  {
    this.Factory = factory;
    this.Sanitizer = sanitizer;
  }
  public CreateComponent<T>(type: any, container: ViewContainerRef, index: number = null): T
  {
    let factory = this.Factory.resolveComponentFactory(type);
    let component: ComponentRef<any> = factory.create(container.parentInjector) as ComponentRef<any>;
    this.CmpRef = component;

    container.insert(component.hostView, index);

    return  component['_component'] as T;
  }
}
