import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';

// utils
import { SnackUtils } from 'clericuzzi-lib/utils/snack-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// models
import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model';
import { RequestMessage } from 'clericuzzi-lib/entities/request/request-message.model';
import { BatchResponseMessage } from 'clericuzzi-lib/entities/response/batch-response-message.model';
import { StatusResponseMessage } from 'clericuzzi-lib/entities/response/status-response-message.model';

// services
import { HttpService } from './http.service';
import { LoaderService } from './loader.service';
import { CustomField } from 'clericuzzi-lib/entities/crud/custom-field.model';

@Injectable()
export class EntitiesService
{
  public OnDelete: Subject<number[]> = new Subject<number[]>();
  public OnInsert: Subject<Crudable[]> = new Subject<Crudable[]>();
  public OnRefreshRequested: Subject<void> = new Subject<void>();
  constructor(
    private _Http: HttpService,
    private _Snack: MatSnackBar,
    private _Loader: LoaderService,
  )
  {
  }

  public Requestrefresh(): void
  {
    this.OnRefreshRequested.next();
  }

  public Request(url: string, loader: boolean, callback: Function, ...customFields: CustomField[]): void
  {
    if (loader)
      this._Loader.Show();
    let request: RequestMessage = new RequestMessage(url);
    for (let customField of customFields)
      request.Add(customField.Key, customField.Value);

    let response: StatusResponseMessage = new StatusResponseMessage();
    this._Http.Post(request, response, () => this._RequestCallback(response, callback));
  }
  private _RequestCallback(response: StatusResponseMessage, callback: Function = null): void
  {
    this._Loader.Hide();
    SnackUtils.HandleStatusResponse(this._Snack, response);
    if (callback)
      callback(response.Success);
  }

  public Delete(url: string, ids: number[]): void
  {
    this._Loader.Show();
    let request: RequestMessage = new RequestMessage(url);
    request.Add(`entities`, ids);

    let response: StatusResponseMessage = new StatusResponseMessage();
    this._Http.Post(request, response, () => this._DeleteCallback(response, ids));
  }
  private _DeleteCallback(response: StatusResponseMessage, ids: number[]): void
  {
    this._Loader.Hide();
    if (ObjectUtils.GetValue(response.Success, false))
      this.OnDelete.next(ids);

    SnackUtils.HandleStatusResponse(this._Snack, response);
  }

  public Insert(url: string, models: any): void
  {
    this._Loader.Show();
    let request: RequestMessage = new RequestMessage(url);
    if (!Array.isArray(models))
      request.Add(`model`, ((models as any) as Crudable).ToJson());
    else
    {
      let entities: any[] = [];
      for (let model of models)
        entities.push(model.ToJson());
      request.Add(`entities`, entities);
    }

    let response: BatchResponseMessage<any> = new BatchResponseMessage<any>();
    this._Http.Post(request, response, () => this._InsertCallback(response));
  }
  private _InsertCallback(response: BatchResponseMessage<any>): void
  {
    this._Loader.Hide();
    if (!ObjectUtils.GetValue(response.HasErrors, true))
      this.OnInsert.next(response.Entities);

    SnackUtils.HandleBatchResponse(this._Snack, response)
  }

  public Update(url: string, model: any): void
  {
    this._Loader.Show();
    let request: RequestMessage = new RequestMessage(url);
    request.Add(`model`, model);

    let response: StatusResponseMessage = new StatusResponseMessage();
    this._Http.Post(request, response, () => this._UpdateCallback(response));
  }
  private _UpdateCallback(response: StatusResponseMessage): void
  {
    this._Loader.Hide();
    SnackUtils.HandleStatusResponse(this._Snack, response);
  }
}