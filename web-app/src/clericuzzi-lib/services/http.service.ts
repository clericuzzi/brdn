﻿import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions, RequestMethod, ResponseContentType } from '@angular/http';

// models
import { RequestMessage } from 'clericuzzi-lib/entities/request/request-message.model';

@Injectable()
export class HttpService
{
    private Headers: Headers;

    constructor(public Http: Http)
    {
        this.Headers = new Headers();
        this.Headers.append(`Content-Type`, `application/json`);
        this.Headers.append(`Accept`, `application/json`);

        this.Headers.append(`Access-Control-Allow-Origin`, `*`);
        this.Headers.append(`Access-Control-Allow-Methods`, `GET, PUT, POST, DELETE, PATCH, OPTIONS`);
        this.Headers.append(`Access-Control-Allow-Headers`, `Cache-Control, Pragma, Origin, Authorization, Content-Type, X-Requested-With`);
    }

    public Get<T>(url: string, returnObject: T, completeCallback: Function, headers: Headers = null): void
    {
        let observable = this.Http.get(url, { headers: headers == null ? this.Headers : headers });
        this.CallObservable<T>(returnObject, observable, completeCallback);
    }
    public Put<T>(request: RequestMessage, returnObject: T, completeCallback: Function, headers: Headers = null): void
    {
        let requestOptions = new RequestOptions({
            method: RequestMethod.Put
            , headers: this.Headers
            , body: request
            , responseType: ResponseContentType.Json
        });
        let observable = this.Http.request(request.Url, requestOptions);
        this.CallObservable<T>(returnObject, observable, completeCallback);
    }
    public Post<T>(request: RequestMessage, returnObject: T, completeCallback: Function, headers: Headers = null): void
    {
        let url: string = request.Url;
        delete request[`Url`];

        let requestOptions = new RequestOptions({
            method: RequestMethod.Post
            , headers: headers == null ? this.Headers : headers
            , body: request
            , responseType: ResponseContentType.Json
        });
        let observable = this.Http.request(url, requestOptions);
        this.CallObservable<T>(returnObject, observable, completeCallback);
    }
    public Delete<T>(request: RequestMessage, returnObject: T, completeCallback: Function, headers: Headers = null): void
    {
        let requestOptions = new RequestOptions({
            method: RequestMethod.Delete
            , headers: headers == null ? this.Headers : headers
            , body: request
            , responseType: ResponseContentType.Json
        });
        let observable = this.Http.request(request.Url, requestOptions);
        this.CallObservable<T>(returnObject, observable, completeCallback);
    }
    
    public PutData<T>(url: string, data: any, returnObject: T, completeCallback: Function, headers: Headers = null): void
    {
        let requestOptions = new RequestOptions({
            method: RequestMethod.Put
            , headers: headers == null ? this.Headers : headers
            , body: data
            , responseType: ResponseContentType.Json
        });
        let observable = this.Http.request(url, requestOptions);
        this.CallObservable<T>(returnObject, observable, completeCallback);
    }
    public PostData<T>(url: string, data: any, returnObject: T, completeCallback: Function, headers: Headers = null): void
    {
        let requestOptions = new RequestOptions({
            method: RequestMethod.Post
            , headers: headers == null ? this.Headers : headers
            , body: data
            , responseType: ResponseContentType.Json
        });
        let observable = this.Http.request(url, requestOptions);
        this.CallObservable<T>(returnObject, observable, completeCallback);
    }
    public DeleteData<T>(url: string, data: any, returnObject: T, completeCallback: Function, headers: Headers = null): void
    {
        let requestOptions = new RequestOptions({
            method: RequestMethod.Delete
            , headers: headers == null ? this.Headers : headers
            , body: data
            , responseType: ResponseContentType.Json
        });
        let observable = this.Http.request(url, requestOptions);
        this.CallObservable<T>(returnObject, observable, completeCallback);
    }

    private CallObservable<T>(returnObject: T, observable: Observable<Response>, completeCallback: Function): void
    {
        observable.subscribe(response => this.ToInstance<T>(returnObject, response, completeCallback), err => this.ToInstanceError<T>(returnObject, err, completeCallback));
    }
    private ToInstance<T>(obj: T, response: Response, callback: Function): void
    {
        let json = response.json();
        if (!json)
        {
            if (callback == null)
                return;
                
            throw new Error('Resposta inválida');
        }

        if (typeof obj["fromJSON"] === "function")
            obj["fromJSON"](json);
        else
            for (var propName in json)
                obj[propName] = json[propName]

        if (callback != null)
            callback();
    }
    private ToInstanceError<T>(obj: T, error: any, callback: Function): void
    {
        if (callback != null)
            callback();
    }
}