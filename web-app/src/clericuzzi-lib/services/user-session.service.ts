import { Injectable } from '@angular/core';
import { CookieService } from 'ng2-cookies';

// utils
import { ObjectUtils } from '../utils/object-utils';

// models
import { User } from 'clericuzzi-lib/entities/models/user.model';
import { CustomFieldable } from 'clericuzzi-lib/entities/crud/custom-fieldable.model';

@Injectable()
export class UserSessionService
{
  private readonly EXPIRES_IN: number = (24 * 60 * 60 * 1000);
  private readonly COOKIE_NAME: string = `app-cookie`;
  constructor(private _Cookies: CookieService)
  {
  }

  public readonly SETTINGS_ID: string = `login-settings-id`;
  public readonly SETTINGS_USER: string = `login-settings-user`;
  public readonly SETTINGS_TOKEN: string = `login-settings-token`;
  public readonly SETTINGS_LOGGED: string = `login-settings-logged`;
  public readonly SETTINGS_NAME_FULL: string = `login-settings-name-full`;
  public readonly SETTINGS_NAME_LAST: string = `login-settings-name-last`;
  public readonly SETTINGS_NAME_FIRST: string = `login-settings-name-first`;
  public readonly SETTINGS_PERMISSION: string = `login-settings-name-permission`;
  public readonly SETTINGS_PERMISSION_LIST: string = `login-settings-permission-list`;

  public get Id(): number { return this.GetValue<number>(this.SETTINGS_ID); }
  public set Id(value: number) { this.AddValue(this.SETTINGS_ID, value); }

  public get User(): User { return this.GetValue<User>(this.SETTINGS_USER); }
  public set User(value: User) { this.AddValue(this.SETTINGS_USER, value.ToJson()); }

  public get Token(): string { return this.GetValue<string>(this.SETTINGS_TOKEN); }
  public set Token(value: string) { this.AddValue(this.SETTINGS_TOKEN, value); }

  public get Logged(): boolean { return this.GetValue<boolean>(this.SETTINGS_LOGGED); }
  public set Logged(value: boolean) { this.AddValue(this.SETTINGS_LOGGED, value); }

  public get NameFull(): string { return this.GetValue<string>(this.SETTINGS_NAME_FULL); }
  public set NameFull(value: string) { this.AddValue(this.SETTINGS_NAME_FULL, value); }

  public set NameLast(value: string) { this.AddValue(this.SETTINGS_NAME_LAST, value); }
  public get NameLast(): string
  {
    let name: string = this.GetValue<string>(this.SETTINGS_NAME_LAST);
    if (name !== undefined && name.indexOf(' ') >= 0)
      name = name.substring(name.lastIndexOf(' ') + 1);
    return name;
  }

  public set NameFirst(value: string) { this.AddValue(this.SETTINGS_NAME_FIRST, value); }
  public get NameFirst(): string
  {
    let name: string = this.GetValue<string>(this.SETTINGS_NAME_FIRST);
    if (name !== undefined && name.indexOf(' ') >= 0)
      name = name.substring(0, name.indexOf(' '));
    return name;
  }

  public set PermissionName(value: string) { this.AddValue(this.SETTINGS_PERMISSION, value); }
  public get PermissionName(): string { return this.GetValue<string>(this.SETTINGS_PERMISSION); }

  public get PermissionList(): number[] { return this.GetValue<number[]>(this.SETTINGS_PERMISSION_LIST); }
  public set PermissionList(value: number[]) { this.AddValue(this.SETTINGS_PERMISSION_LIST, value); }

  public _Data: CustomFieldable;

  public Load(): void
  {
    this._Data = new CustomFieldable();
    const rawCookie: string = this._Cookies.get(this.COOKIE_NAME);
    if (!ObjectUtils.NullUndefinedOrEmpty(rawCookie))
    {
      try
      {
        this._Data.CustomFields = JSON.parse(rawCookie);
      }
      catch (e)
      {
      }
    }
  }
  public Save(): void
  {
    try
    {
      this._Cookies.set(this.COOKIE_NAME, JSON.stringify(this._Data.CustomFields), this.EXPIRES_IN);
    }
    catch (e)
    {
      console.log(e);
    }
  }
  public Clear(): void
  {
    this._Data = new CustomFieldable();
    this.Save();
  }

  public Delete(key: string, save: boolean = false): void
  {
    this._Data.Remove(key);
    if (save)
      this.Save();
  }
  public AddValue(key: string, value: any, save: boolean = false): void
  {
    this._Data.Add(key, value);
    if (save)
      this.Save();
  }
  public GetValue<T>(key: string, reviver: (key: any, value: any) => any = null): T
  {
    return this._Data.GetValue<T>(key, reviver);
  }
}
