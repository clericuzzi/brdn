export class NumberUtils
{
    public static Round(value: number, decimals: number = 2): number
    {
        value = Number.parseFloat(value.toFixed(decimals));

        return value;
    }
}