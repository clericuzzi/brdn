import * as jQuery from 'jquery';
import { JSOnList } from 'clericuzzi-lib/entities/response/json-list.model';
import { JSOnListSet } from 'clericuzzi-lib/entities/response/json-list-set.model';

export class JsonUtils
{
    public static Stringify(obj: any): any
    {
        if (typeof obj === 'string' || obj instanceof String)
            return JSON.stringify(obj);
        else if (typeof obj === 'number' || obj instanceof Number)
            return JSON.stringify(obj);
        else if (Array.isArray(obj))
            return JSON.stringify(obj);
        else
        {
            let json: any = {};
            let hasProperties: boolean = false;
            for (var propName in obj)
            {
                if (!hasProperties)
                hasProperties = true;

                if (obj[propName] != null && obj[propName] != undefined)
                json[propName] = obj[propName]
            }

            if (!hasProperties)
            json = obj;

            return JSON.stringify(json);
        }
    }

    public static Clone(obj: any): any
    {
        return jQuery.extend({}, obj);
    }
    public static CloneDeep(obj: any): any
    {
        return jQuery.extend(true, {}, obj);
    }

    public static ToInstance<T>(obj: T, json: string): T
    {
        let jsonObject = JSON.parse(json);
        for (var propName in jsonObject)
            obj[propName] = jsonObject[propName]

        return obj;
    }
    public static ToInstanceOf<T>(objectTemplate: T, dataset: Object): T
    {
        if (objectTemplate['ParseFromJson'] != null)
            objectTemplate['ParseFromJson'](dataset);
        else
        {
            for (var propName in dataset)
                objectTemplate[propName] = dataset[propName];
        }

        return objectTemplate;
    }
    public static ParseList<T>(list: any[], baseObject: T): T[]
    {
        var result: T[] = [];
        for (let item of list)
            result.push(this.ToInstanceOf<T>(JsonUtils.Clone(baseObject), item));

        return result;
    }
    public static ParseJsonList<T>(response: JSOnListSet, key: string,  baseObject: T): T[]
    {
        var jsonList: JSOnList = response.GetList(key, baseObject, null);
        if (jsonList != null && jsonList != undefined)
        {
            jsonList.Parse();
            return jsonList.List;
        }
        else
            return [];
    }
}
