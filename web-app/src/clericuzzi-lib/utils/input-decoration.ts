import { ElementRef } from '@angular/core';  
import { ObjectUtils } from './object-utils';

export class InputDecoration
{
    public static ElementFocus(elementid): void
    {
        let element: HTMLElement = document.getElementById(elementid);
        if (element != null)
            element.focus();
    }
    public static ElementClassAdd(elementid: string, ...classNames: string[]): void
    {
        let element: HTMLElement = document.getElementById(elementid);
        if (element != null)
            for (let className of classNames)
                if (element != null && !element.classList.contains(className))
                    element.classList.add(className);
    }
    public static ElementClassRemove(elementid: string, ...classNames: string[]): void
    {
        let element: HTMLElement = document.getElementById(elementid);
        if (element != null)
            for (let className of classNames)
                if (element != null && element.classList.contains(className))
                    element.classList.remove(className);
    }
    
    public static _AddClass(element: HTMLElement, className: string): void
    {
        if (element != null && !element.classList.contains(className))
            element.classList.add(className);
    }
    public static _RemoveClass(element: HTMLElement, ...classNames: string[]): void
    {
        if (element != null)
            for (let className of classNames)
                if (element != null && element.classList.contains(className))
                    element.classList.remove(className);
    }
    
    public static ElementHasClass(elementid: string, className: string): boolean
    {
        let classContained: boolean = false;
        let element: HTMLElement = document.getElementById(elementid);
        if (element != null)
            classContained = element.classList.contains(className);

        return classContained;
    }
    public static ElementToggleClass(elementid: string, add: string, remove: string): void
    {
        let element: HTMLElement = document.getElementById(elementid);
        if (element != null)
        {
            this._AddClass(element, add);
            this._RemoveClass(element, remove);                
        }
    }
    public static ElementClassManipulation(elementid: string, add: string[], remove: string[]): void
    {
        let element: HTMLElement = document.getElementById(elementid);
        if (element != null)
        {
            for (let className of remove)
                this._RemoveClass(element, className);
                
            for (let className of add)
                this._AddClass(element, className);
        }
    }
    
    public static RemoveClassesContaining(id: string, ...classPieces: string[]): void
    {
        let element: HTMLElement = document.getElementById(id);
        if (element != null)
        {
            let classesToRemove: string[] = []
            for (let index = 0; index < element.classList.length; index++)
            {
                let className: string = element.classList.item(index);
                for (let classPiece of classPieces )
                    if (className.indexOf(classPiece) > -1)
                    {
                        InputDecoration._RemoveClass(element, className);
                        index--;
                    }
            }
        }
    }

    public static FadeInById(elements: string[], classInvisible: string, classHidden: string, timeout: number): void
    {
        let htmlElements: HTMLElement[] = [];
        for (let item of elements)
            htmlElements.push(document.getElementById(item));

        InputDecoration.FadeIn(htmlElements, classInvisible, classHidden, timeout);
    }
    public static FadeIn(elements: HTMLElement[], classInvisible: string, classHidden: string, timeout: number): void
    {
        for (let item of elements)
            InputDecoration._RemoveClass(item, classHidden);

        setTimeout(() => InputDecoration._FadeIn(elements, classInvisible), timeout);
    }
    private static _FadeIn(elements: HTMLElement[], classInvisible: string): void
    {
        for (let item of elements)
            InputDecoration._RemoveClass(item, classInvisible);
    }

    public static FadeOutById(elements: string[], classInvisible: string, classHidden: string, timeout: number): void
    {
        let htmlElements: HTMLElement[] = [];
        for (let item of elements)
            htmlElements.push(document.getElementById(item));

        InputDecoration.FadeOut(htmlElements, classInvisible, classHidden, timeout);
    }
    public static FadeOut(elements: HTMLElement[], classInvisible: string, classHidden: string, timeout: number): void
    {
        for (let item of elements)
            InputDecoration._AddClass(item, classInvisible);

        setTimeout(() => InputDecoration._FadeOut(elements, classHidden), timeout);
    }
    private static _FadeOut(elements: HTMLElement[], classHidden: string): void
    {
        for (let item of elements)
            InputDecoration._AddClass(item, classHidden);
    }

    public static ToggleVisibilityById(elements: string[], elementVisible: string, classInvisible: string = null, classHidden: string = null, timeout: number = 150): void
    {
        if (classInvisible == null)
            classInvisible = `visibiliity-fade-out`;

        if (classHidden == null)
            classHidden = `visibiliity-fade-removed`;

        let visibleElement: HTMLElement = document.getElementById(elementVisible);
        let htmlElements: HTMLElement[] = [];
        for (let item of elements)
            htmlElements.push(document.getElementById(item));

        InputDecoration.ToggleVisibility(htmlElements, visibleElement, classInvisible, classHidden, timeout);
    }
    public static ToggleVisibility(elements: HTMLElement[], elementVisible: HTMLElement, classInvisible: string, classHidden: string, timeout: number): void
    {
        for (let item of elements)
            InputDecoration._AddClass(item, classInvisible);
        
        setTimeout(() => InputDecoration._ToggleVisibilityStepTwo(elements, elementVisible, classInvisible, classHidden, timeout), timeout);
    }
    private static _ToggleVisibilityStepTwo(elements: HTMLElement[], elementVisible: HTMLElement, classInvisible: string, classHidden: string, timeout: number): void
    {
        for (let item of elements)
            InputDecoration._AddClass(item, classHidden);

        InputDecoration._RemoveClass(elementVisible, classHidden);
        setTimeout(() => InputDecoration._RemoveClass(elementVisible, classInvisible), timeout);
    }

    public static AddClass(element: ElementRef, ... classes: string[]): void
    {
        if (!ObjectUtils.NullOrUndefined(element) && !ObjectUtils.NullOrUndefined(element.nativeElement))
            classes.forEach(x => element.nativeElement.classList.add(x));
    }
    public static RemoveClass(element: ElementRef, ... classes: string[]): void
    {
        if (!ObjectUtils.NullOrUndefined(element) && !ObjectUtils.NullOrUndefined(element.nativeElement))
            classes.forEach(x => element.nativeElement.classList.remove(x));
    }
    public static ElementFadeIn(element: ElementRef): void
    {
        if (!ObjectUtils.NullOrUndefined(element) && !ObjectUtils.NullOrUndefined(element.nativeElement))
        {
            element.nativeElement.classList.remove(`visibiliity-fade-removed`);
            setTimeout(() => element.nativeElement.classList.remove(`visibiliity-fade-out`), 50);
        }
    }
    public static ElementFadeOut(element: ElementRef, timeout: number = 250): void
    {
        if (!ObjectUtils.NullOrUndefined(element) && !ObjectUtils.NullOrUndefined(element.nativeElement))
        {
            element.nativeElement.classList.add(`visibiliity-fade-out`);
            setTimeout(() => element.nativeElement.classList.add(`visibiliity-fade-removed`), timeout);
        }
    }
    public static HasClass(element: ElementRef, className: string): boolean
    {
        if (ObjectUtils.NullOrUndefined(element) || ObjectUtils.NullOrUndefined(element.nativeElement))
            return false;
        else
        {
            for (let currentClass of element.nativeElement.classList)
                if (currentClass == className)
                    return true;

            return false;
        }
    }
    
    public static BindingAddClass(element: any, hostBindingProperty: string = `ComponentClass`, className: string): void
    {
        InputDecoration.BindingRemoveClass(element, hostBindingProperty, className);
        element[hostBindingProperty] = `${element[hostBindingProperty]} ${className}`;
    }
    public static BindingAddClasses(element: any, hostBindingProperty: string = `ComponentClass`, ... classes: string[]): void
    {
        for (let className of classes)
            InputDecoration.BindingAddClass(element, hostBindingProperty, className);
    }
    public static BindingRemoveClass(element: any, hostBindingProperty: string = `ComponentClass`, className: string): void
    {
        let currentClassSetting: string = element[hostBindingProperty];
        while (currentClassSetting.indexOf(`  `) >= 0)
            currentClassSetting = currentClassSetting.replace(`  `, ` `);

        let classes: string[] = currentClassSetting.split(' ');
        let nonRemovedClasses: string[] = classes.filter(i => i != className);
        currentClassSetting = nonRemovedClasses.join(` `);
        element[hostBindingProperty] = currentClassSetting;
    }
    public static BindingRemoveClasses(element: any, hostBindingProperty: string = `ContainerClass`, ... classes: string[]): void
    {
        for (let className of classes)
            InputDecoration.BindingRemoveClass(element, hostBindingProperty, className);
    }
    public static BindingHide(element: any, hostBindingProperty: string = `ComponentClass`, animated: boolean = true): void
    {
        let currentClassSetting: string = element[hostBindingProperty];
        if (currentClassSetting.indexOf(`visibiliity-fade-removed`) >= 0)
            return;

        if (animated)
            InputDecoration.BindingAddClass(element, hostBindingProperty, `visibiliity-fades`);
        InputDecoration.BindingAddClass(element, hostBindingProperty, `visibiliity-fade-out`);

        setTimeout(() => InputDecoration.BindingAddClass(element, hostBindingProperty, `visibiliity-fade-removed`), 250);
    }
    public static BindingShow(element: any, hostBindingProperty: string = `ComponentClass`, animated: boolean = true): void
    {
        InputDecoration.BindingRemoveClass(element, hostBindingProperty, `visibiliity-fade-removed`);
        if (animated)
            InputDecoration.BindingAddClass(element, hostBindingProperty, `visibiliity-fades`);
        InputDecoration.BindingRemoveClass(element, hostBindingProperty, `visibiliity-fade-out`);
    }
    public static BindingHidden(element: any, hostBindingProperty: string = `ComponentClass`): void
    {
        InputDecoration.BindingAddClasses(element, hostBindingProperty, `visibiliity-fades`, `visibiliity-fade-out`, `visibiliity-fade-removed`);
    }
    public static BindingFadeIn(element: any, hostBindingProperty: string = `ContainerClass`, timeout: number = 0): void
    {
        let currentClassSetting: string = element[hostBindingProperty];
        
        currentClassSetting = currentClassSetting.replace(`  `, ` `);
        currentClassSetting = currentClassSetting.replace(`  `, ` `);
        currentClassSetting = currentClassSetting.replace(`  `, ` `);
        currentClassSetting = currentClassSetting.replace(`visibiliity-fade-removed`, ``).trim();

        if (timeout > 0)
            setTimeout(() => element[hostBindingProperty] = currentClassSetting, timeout);
        else
            element[hostBindingProperty] = currentClassSetting;
        setTimeout(() => element[hostBindingProperty] = currentClassSetting.replace(`visibiliity-fade-out`, ``).trim(), timeout + 50);
    }
    public static BindingFadeOut(element: any, hostBindingProperty: string = `ContainerClass`, timeout: number = 0): void
    {
        let currentClassSetting: string = element[hostBindingProperty];
        if (currentClassSetting.indexOf(`visibiliity-fade-removed`) < 0)
        {
            currentClassSetting = currentClassSetting.replace(`  `, ` `);
            currentClassSetting = currentClassSetting.replace(`  `, ` `);
            currentClassSetting = currentClassSetting.replace(`  `, ` `);
            currentClassSetting = currentClassSetting.replace(`visibiliity-fade-out`, ``);
            currentClassSetting = currentClassSetting.replace(`visibiliity-fade-removed`, ``);
            currentClassSetting = currentClassSetting + ` visibiliity-fade-out`;
            
            if (timeout > 0)
                setTimeout(() => element[hostBindingProperty] = currentClassSetting, timeout);
            else
                element[hostBindingProperty] = currentClassSetting

            setTimeout(() => element[hostBindingProperty] = currentClassSetting + ` visibiliity-fade-removed`, timeout + 250);
        }
    }
    
    public static ClearInputfile(elementId: string): void
    {
        let component: HTMLInputElement = document.getElementById(elementId) as HTMLInputElement;
        if (component != null)
        {
            component.type = '';
            component.type = 'file';
        }
    }
}