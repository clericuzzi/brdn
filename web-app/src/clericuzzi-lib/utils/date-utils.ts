import * as moment from 'moment/moment';

// utils
import { StringUtils } from './string-utils';
import { ObjectUtils } from './object-utils';

export class DateUtils
{
    private static _DefaultFormat: string = `DDMMYYYY`;
    public static ToDateBr(value: string): Date
    {
        value = StringUtils.DigitsOnly(value);
        let date: any = new Date(moment(value, this._DefaultFormat, true).format());
        if (date instanceof Date && isFinite(date.getTime()))
            return date;
        else
            return null;
    }
    public static FromDateBr(value: string): Date
    {
        value = StringUtils.DigitsOnly(value);
        let date: any = new Date(moment(value, this._DefaultFormat, true).format());
        if (date instanceof Date && isFinite(date.getTime()))
            return date;
        else
            return null;
    }
    public static FromDateTimeBr(value: string): Date
    {
        let format: string = `DDMMYYYYHHmmss`;
        value = StringUtils.DigitsOnly(value);
        format = format.substring(0, value.length);
        let date: any = new Date(moment(value, format, true).format());
        if (date instanceof Date && isFinite(date.getTime()))
            return date;
        else
            return null;
    }
    public static ToDateServer(value: Date): string
    {
        if (!ObjectUtils.NullOrUndefined(value))
        {
            let date: string = moment(value).format(`YYYY-MM-DD HH:mm:ss`);
            return date;
        }
        else
            return null;
    }
    public static FromDateServer(value: string, convertToLocalTime: boolean = false): Date
    {
        let date: any = new Date(moment(value).format());
        if (date instanceof Date && isFinite(date.getTime()))
        {
            if (convertToLocalTime)
            {
                var inverseOffset = moment().utcOffset() * -1;
                var timestamp: any = moment(value).utcOffset(inverseOffset).format();
                var offsetDate: any = moment(timestamp).add(inverseOffset, 'm');
                var offsetDateValue: any = moment(offsetDate).format();
                let timestampValue: Date = new Date(offsetDateValue);

                return timestampValue;
            }
            else
                return date;
        }
        else
            return null;
    }
    public static IsValid(date: Date): boolean
    {
        return date instanceof Date && isFinite(date.getTime());
    }

    public static GetAge(birth: Date): number
    {
        return moment().diff(moment(this.ToDateServer(birth), `YYYYMMDD`), `years`);
    }
}