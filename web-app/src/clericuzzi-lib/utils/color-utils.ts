export class ColorUtils
{
  public static RandomColor(): string
  {
    return '#' + Math.floor(Math.random() * 16777215).toString(16);
  }
}
