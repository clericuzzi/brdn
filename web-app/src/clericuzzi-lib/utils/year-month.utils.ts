import { MonthModel } from 'clericuzzi-lib/business/models/month.model';
import { ComboInputValueModel } from 'clericuzzi-lib/modules/shared/form-controls/custom-inputs/combo-input/combo-input-value.model';

export class YearMonthUtils
{
  public static GetMonthByValue(value: number): string
  {
    if (value === 1)
      return `Janeiro`;
    else if (value === 2)
      return `Fevereito`;
    else if (value === 3)
      return `Março`;
    else if (value === 4)
      return `Abril`;
    else if (value === 5)
      return `Maio`;
    else if (value === 6)
      return `Junho`;
    else if (value === 7)
      return `Julho`;
    else if (value === 8)
      return `Agosto`;
    else if (value === 9)
      return `Setembro`;
    else if (value === 10)
      return `Outubro`;
    else if (value === 11)
      return `Novembro`;
    else if (value === 12)
      return `Dezembro`;

    return null;
  }
  public static GetMonthArrayBr(): string[]
  {
    return [
      `Janeiro`,
      `Feveriro`,
      `Março`,
      `Abril`,
      `Maio`,
      `Junho`,
      `Julho`,
      `Agosto`,
      `Setembro`,
      `Outubro`,
      `Novembro`,
      `Dezembro`,
    ];
  }
  public static GetMonthListBr(): MonthModel[]
  {
    return [
      new MonthModel(1, `Janeiro`),
      new MonthModel(2, `Feveriro`),
      new MonthModel(3, `Março`),
      new MonthModel(4, `Abril`),
      new MonthModel(5, `Maio`),
      new MonthModel(6, `Junho`),
      new MonthModel(7, `Julho`),
      new MonthModel(8, `Agosto`),
      new MonthModel(9, `Setembro`),
      new MonthModel(10, `Outubro`),
      new MonthModel(11, `Novembro`),
      new MonthModel(12, `Dezembro`),
    ];
  }
  public static GetMonthListBrForCombo(): ComboInputValueModel<number>[]
  {
    return [
      new ComboInputValueModel<number>(1, `Janeiro`),
      new ComboInputValueModel<number>(2, `Feveriro`),
      new ComboInputValueModel<number>(3, `Março`),
      new ComboInputValueModel<number>(4, `Abril`),
      new ComboInputValueModel<number>(5, `Maio`),
      new ComboInputValueModel<number>(6, `Junho`),
      new ComboInputValueModel<number>(7, `Julho`),
      new ComboInputValueModel<number>(8, `Agosto`),
      new ComboInputValueModel<number>(9, `Setembro`),
      new ComboInputValueModel<number>(10, `Outubro`),
      new ComboInputValueModel<number>(11, `Novembro`),
      new ComboInputValueModel<number>(12, `Dezembro`),
    ];
  }
}
