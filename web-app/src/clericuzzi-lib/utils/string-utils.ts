import * as moment from 'moment/moment';
import { ObjectUtils } from './object-utils';

export class StringUtils
{
  public static AddClasses(baseClass: string, ...classes: string[]): string
  {
    let currentClasses: string[] = baseClass.split(' ').concat(classes);
    currentClasses = currentClasses.filter((v, i, a) => a.indexOf(v) === i);

    return currentClasses.join(' ').trim();
  }
  public static RemoveClasses(baseClass: string, ...classes: string[]): string
  {
    classes = classes.filter((v, i, a) => a.indexOf(v) === i);
    let currentClasses: string[] = baseClass.split(' ').filter(i => !classes.find(j => j === i));
    currentClasses = currentClasses.filter((v, i, a) => a.indexOf(v) === i);

    return currentClasses.join(' ').trim();
  }

  public static TransformCep(value: string): string
  {
    if (ObjectUtils.NullUndefinedOrEmpty(value))
      return null;
    else
    {
      while (value.length < 8)
        value = `0${value}`;
      return `${value.substr(0, 5)}-${value.substr(5, 3)}`;
    }
  }
  public static TransformCpf(value: string): string
  {
    if (ObjectUtils.NullUndefinedOrEmpty(value) || value.length !== 11)
      return ``;
    else
      return `${value.substr(0, 3)}.${value.substr(3, 3)}.${value.substr(6, 3)}-${value.substr(9, 2)}`;
  }
  public static TransformCnpj(value: string): string
  {
    if (ObjectUtils.NullUndefinedOrEmpty(value) || value.length !== 14)
      return ``;
    else
      return `${value.substr(0, 2)}.${value.substr(2, 3)}.${value.substr(5, 3)}/${value.substr(8, 4)}-${value.substr(12, 2)}`;
  }

  public static TransformYesNo(value: string)
  {
    if (value === `1`)
      return `Sim`;
    else if (value === `0`)
      return `Não`;
    else
      return ``;
  }

  public static TransformZipCodeBr(value: string)
  {
    if (ObjectUtils.NullUndefinedOrEmpty(value))
      return null;
    else
    {
      if (value.length === 8)
        return `${value.substring(0, 5)}-${value.substring(5, 8)}`;
      else
        return value;
    }
  }

  public static TransformDateToBr(value: Date, defaultValue: string = null): string
  {
    if (ObjectUtils.NullOrUndefined(value))
      return ObjectUtils.NullUndefinedOrEmpty(defaultValue) ? null : defaultValue;
    else
      return moment(value).format('DD/MM/YYYY');
  }
  public static TransformDateToFormat(value: Date, format: string): string
  {
    if (ObjectUtils.NullOrUndefined(value))
      return null;
    else
      return moment(value).format(format);
  }
  public static TransformDateTimeToBr(value: Date, defaultValue: string = null, amPm: boolean = false): string
  {
    let hourValue: string = `HH:mm`;
    if (amPm)
      hourValue = `hh:mm TT`;
    if (ObjectUtils.NullOrUndefined(value))
      return ObjectUtils.NullUndefinedOrEmpty(defaultValue) ? null : defaultValue;
    else
      return moment(value).format(`DD/MM/YYYY ${hourValue}`);
  }

  public static ToPhoneBr(value: string): string
  {
    if (value === null || value === undefined)
      return null;
    else
    {
      let phone: string = value.trim();
      if (value.length === 8)
        phone = `${value.substr(0, 4)}-${value.substr(4)}`;
      else if (value.length === 9)
        phone = `${value.substr(0, 5)}-${value.substr(5)}`;
      else if (value.length === 10)
        phone = `(${value.substr(0, 2)}) ${value.substr(2, 4)}-${value.substr(6)}`;
      else if (value.length === 11)
        phone = `(${value.substr(0, 2)}) ${value.substr(2, 5)}-${value.substr(7)}`;


      else if (value.length === 12)
        phone = `+${value.substr(0, 2)} (${value.substr(2, 2)}) ${value.substr(4, 4)}-${value.substr(8)}`;

      else if (value.length === 13)
        phone = `+${value.substr(0, 2)} (${value.substr(2, 2)}) ${value.substr(4, 5)}-${value.substr(9)}`;

      return phone;
    }
  }
  public static ValueToMoney(value: any, moneySign: string = `R$ `): string
  {
    if (value === null || value === undefined)
      return `${moneySign} 0,00`;
    else
    {
      const formatted: string = StringUtils.NumberWithCommas(value.toString());

      return moneySign + formatted;
    }
  }

  public static DigitsOnly(value: any): string
  {
    if (value === null || value === undefined)
      value = ``;

    return value.toString().replace(/[^0-9]/g, '');
  }
  public static DecimalOnly(value: any): string
  {
    if (value === null || value === undefined)
      value = ``;

    value = value.toString().replace(/[^0-9$.,]/g, '');
    value = value.replace(`,`, `.`);
    let lastIndex: number = value.lastIndexOf(`.`);
    while (lastIndex > 0 && lastIndex !== value.indexOf(`.`))
    {
      const dotIndex: number = value.indexOf(`.`);
      const head: string = value.substr(0, dotIndex);
      const tail: string = value.substr(dotIndex + 1);
      value = head + tail;

      lastIndex = value.lastIndexOf(`.`);
    }
    while (value.indexOf(`,`) >= 0)
      value = value.replace(`,`, `.`);

    return value;
  }
  public static ValidateNumericField(value: any): number
  {
    if (value === null || value === undefined)
      value = 0;

    let stringValue: string = StringUtils.DigitsOnly(value);
    stringValue = stringValue.replace(`,`, `.`);
    let lastIndex: number = stringValue.lastIndexOf(`.`);
    while (lastIndex > 0 && lastIndex !== stringValue.indexOf(`.`))
    {
      const dotIndex: number = stringValue.indexOf(`.`);
      const head: string = stringValue.substr(0, dotIndex);
      const tail: string = stringValue.substr(dotIndex + 1);
      stringValue = head + tail;

      lastIndex = stringValue.lastIndexOf(`.`);
    }

    if (stringValue.length === 0)
      stringValue = `0`;

    if (stringValue.indexOf(`.`) >= 0)
      return Number.parseFloat(stringValue);
    else
      return Number.parseInt(stringValue, 10);
  }

  static Percentage(x: any, decimals: number = 2): string
  {
    const input = x.toString();
    let value: number = Number.parseFloat(input);
    value = Number.parseFloat(value.toFixed(decimals));

    return `${this.NumberWithCommas(value)}%`;
  }
  static NumberWithCommas(x: any): string
  {
    let input = x.toString();
    input = input.toString().replace(/\B(?=(\d{3})+(?!\d))/g, `,`);
    input = input.replace(`,`, `_(@)_`).replace(`_(@)_`, `.`);

    return input;
  }

  public static DistinctArray(data: string[]): string[]
  {
    data = data.filter(s => data.indexOf(s) === data.lastIndexOf(s));
    return data;
  }
  public static ExcelDate(excelDate: number): Date
  {
    return new Date((excelDate - (25567 + 2)) * 86400 * 1000);
  }

  public static GetTimeString(value: number, suffix: string): string
  {
    if (value < 10)
      return `0${value}${suffix}`;
    else
      return `${value}${suffix}`;
  }

  public static ToLowerString(value: any): string
  {
    if (ObjectUtils.NullOrUndefined(value))
      return ``;
    else
      return value.toString().toLocaleLowerCase();
  }
  public static EmptyStringIfNullOrUndefined(value: any): string
  {
    if (ObjectUtils.NullOrUndefined(value))
      return ``;
    else
      return value.toString();
  }
}
