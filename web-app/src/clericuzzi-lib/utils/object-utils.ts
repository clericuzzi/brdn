/**
 * This class manages null or undefined comparisos as well as "LINQ" utilities
 */
export class ObjectUtils
{
  public static Distinct = (value, index, self: any[]) =>
  {
    return self.indexOf(value) === index;
  }

  /**
   * returns the last value tested
   * @param params a list of inputs that breaks the condition if ANY are null or undefined
   */
  public static ReturnIfNotNullNorUndefined<T>(...params: any[]): T
  {
    if (params === null || params === undefined || params.length === 0)
      return null;

    if (!ObjectUtils.NullOrUndefined(...params))
      return params[params.length - 1];
    else
      return null;
  }

  /**
   * Returns the given value if valid
   * Otherwise returns the given default value (if any)
   * @param value the value to be evaluated, if it cannot be acessed, the default value will be returned
   */
  public static GetValue<T>(obj: any, defaultValue: T = null, ...properties: string[]): T
  {
    try
    {
      if (properties[0] === `File`)
      {
        console.log(obj);
      }
      if (properties.length === 1 && properties[0].indexOf(`.`) > 0)
        properties = properties[0].split(`.`);

      if (ObjectUtils.NullOrUndefined(obj))
        return defaultValue as T;
      else
      {
        if (properties.length === 0)
          return obj;
        else
        {
          const property: string = properties.shift();
          return ObjectUtils.GetValue(obj[property], defaultValue, ...properties);
        }
      }
    }
    catch (e)
    {
      if (ObjectUtils.NullOrUndefined(defaultValue))
        return null;
      else
        return defaultValue;
    }
  }
  /**
   * Evaluates a list of objects and returns TRUE if any of them are null OR undefined
   * @param objs the list of objects to be evaluated
   */
  public static NullOrUndefined(...objs: any[]): boolean
  {
    try
    {
      for (const obj of objs)
        if (obj === null || obj === undefined)
          return true;

      return false;
    }
    catch (e)
    {
      return true;
    }
  }
  /**
   * Evaluates a list of string and returns true if ALL OF THEM are null, undefined or zero length
   * @param objs the list of objects to be evaluated
   */
  public static NullUndefinedOrEmpty(...objs: string[]): boolean
  {
    try
    {
      for (const obj of objs)
        if (obj === null || obj === undefined || obj.length === 0)
          return true;

      return false;
    }
    catch (e)
    {
      return true;
    }
  }
  /**
   * Evaluates an array and returns TRUE if it is null, undefined or has no elements
   * @param array the array to be avaluated
   */
  public static NullUndefinedOrEmptyArray(array: any[]): boolean
  {
    try
    {
      if (ObjectUtils.NullOrUndefined(array))
        return true;
      else
        return array.length === 0;
    }
    catch (e)
    {
      return true;
    }
  }

  /**
   * Returs the first item that matches the lambda comparer, OR
   * the array's first item IF the lambda comparer is not provided
   * @param array the array that contains the desired data
   * @param lambda the function to narrow the data spectrum
   */
  public static First<T>(array: T[], lambda: Function = null): T
  {
    if (!ObjectUtils.NullUndefinedOrEmptyArray(array))
    {
      if (lambda != null)
      {
        const first: T[] = array.filter(i => lambda(i));
        if (ObjectUtils.NullUndefinedOrEmptyArray(first))
          return null;
        else
          return first[0];
      }
      else
        return array[0];
    }
    else
      return null;
  }
  public static CreateObjectArray(...values: any[]): any[]
  {
    const array: any[] = [];
    for (const value of values)
      array.push(value);

    return array;
  }
}
