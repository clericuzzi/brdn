export class WeekDayUtils
{
    public GetAllPtBr(): string[]
    {
        return [ `Domingo`, `Segunda`, `Terça`, `Quarta`, `Quinta`, `Sexta`, `Sábado` ]
    }
    public GetAllPtBrFull(): string[]
    {
        return [ `Domingo`, `Segunda-Feira`, `Terça-Feira`, `Quarta-Feira`, `Quinta-Feira`, `Sexta-Feira`, `Sábado` ]
    }

    public GetListPtBr(): WeekDay[]
    {
        return [ new WeekDay(0, `Domingo`), new WeekDay(1, `Segunda`), new WeekDay(2, `Terça`), new WeekDay(3, `Quarta`), new WeekDay(4, `Quinta`), new WeekDay(5, `Sexta`), new WeekDay(6, `Sábado`) ];
    }
    public GetListPtBrFull(): WeekDay[]
    {
        return [ new WeekDay(0, `Domingo`), new WeekDay(1, `Segunda-Feira`), new WeekDay(2, `Terça-Feira`), new WeekDay(3, `Quarta-Feira`), new WeekDay(4, `Quinta-Feira`), new WeekDay(5, `Sexta-Feira`), new WeekDay(6, `Sábado`) ];
    }
}

export class WeekDay
{
    constructor(
        public Index: number,
        public Day: string,
    )
    {
    }
}