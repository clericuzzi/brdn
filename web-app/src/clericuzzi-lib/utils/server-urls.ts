﻿export class ServerUrls
{
    public static SERVER_ADDRESS: string;

    // Server root endpoint
    public static get LOCAL_ROOT(): string { return `http://localhost:58111/api/`; }
    public static get SERVER_ROOT(): string { return ServerUrls.SERVER_ADDRESS; }

    public static GetUrl(url :string) :string { return `${this.SERVER_ROOT}${url}`; }
    public static GetLocalUrl(url :string) :string { return `${this.LOCAL_ROOT}${url}`; }

    // LOGIN API
    public static get LOGIN_PASSWORD_CHANGE(): string { return ServerUrls.GetUrl("web/login/password-change"); }
    public static get LOGIN_PASSWORD_RETRIEVE(): string { return ServerUrls.GetUrl("web/login/password-retrieve"); }
    
    // CRUD
    public static get CRUD_LIST(): string { return ServerUrls.GetUrl("entities/crud/list"); }
    public static get CRUD_SELECT(): string { return ServerUrls.GetUrl("entities/crud/select"); }
    public static get CRUD_INSERT(): string { return ServerUrls.GetUrl("entities/crud/insert"); }
    public static get CRUD_UPDATE(): string { return ServerUrls.GetUrl("entities/crud/update"); }
    public static get CRUD_DELETE(): string { return ServerUrls.GetUrl("entities/crud/delete"); }
    public static get CRUD_PAGINATION(): string { return ServerUrls.GetUrl("entities/crud/pagination"); }
    
    public static get CRUD_LIST_LOCALHOST(): string { return ServerUrls.GetLocalUrl("entities/crud/list"); }
    public static get CRUD_SELECT_LOCALHOST(): string { return ServerUrls.GetLocalUrl("entities/crud/select"); }
    public static get CRUD_INSERT_LOCALHOST(): string { return ServerUrls.GetLocalUrl("entities/crud/insert"); }
    public static get CRUD_UPDATE_LOCALHOST(): string { return ServerUrls.GetLocalUrl("entities/crud/update"); }
    public static get CRUD_DELETE_LOCALHOST(): string { return ServerUrls.GetLocalUrl("entities/crud/delete"); }
    public static get CRUD_PAGINATION_LOCALHOST(): string { return ServerUrls.GetLocalUrl("entities/crud/pagination"); }
}