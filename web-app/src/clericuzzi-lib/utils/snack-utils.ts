import { MatSnackBar } from "@angular/material";
// utils
import { ObjectUtils } from "./object-utils";
// models
import { StatusResponseMessage } from "clericuzzi-lib/entities/response/status-response-message.model";
import { BatchResponseMessage, BatchResponseMessageList } from 'clericuzzi-lib/entities/response/batch-response-message.model';
// services
// components
// behaviours

export class SnackUtils
{
  public static Open(snackBar: MatSnackBar, message: string, title: string = `Aviso`, duration: number = 8000): void
  {
    snackBar.open(message, title, { duration: duration });
  }
  public static OpenError(snackBar: MatSnackBar, message: string, duration: number = 8000): void
  {
    snackBar.open(message, `Erro`, { duration: duration });
  }
  public static OpenSuccess(snackBar: MatSnackBar, message: string, duration: number = 8000): void
  {
    snackBar.open(message, `Sucesso`, { duration: duration });
  }

  public static HandleStatusResponse(snackBar: MatSnackBar, response: StatusResponseMessage, defaultSuccess: string = `Operação realizada com sucesso`, defaultError: string = `Falha no envio da solicitação`): boolean
  {
    let returnValue: boolean = false;
    if (ObjectUtils.NullOrUndefined(response) || ObjectUtils.NullOrUndefined(response.HasErrors) || ObjectUtils.NullOrUndefined(response.Success))
      SnackUtils.OpenError(snackBar, defaultError);
    else
    {
      if (ObjectUtils.GetValue(response.Success, false))
      {
        returnValue = true;
        SnackUtils.OpenSuccess(snackBar, defaultSuccess);
      }
      else
      {
        let errorMessage: string = defaultError;
        if (!ObjectUtils.NullUndefinedOrEmpty(errorMessage))
          errorMessage = `${errorMessage} `;
        if (!ObjectUtils.NullUndefinedOrEmpty(response.ErrorMessage))
          errorMessage = `${errorMessage} ${response.ErrorMessage}`;

        SnackUtils.OpenError(snackBar, errorMessage);
      }
    }

    return returnValue;
  }
  public static HandleBatchResponse(snackBar: MatSnackBar, response: BatchResponseMessage<any>, defaultSuccess: string = `Operação realizada com sucesso`, defaultError: string = `Falha no envio da solicitação`): boolean
  {
    let returnValue: boolean = false;
    if (ObjectUtils.NullOrUndefined(response) || ObjectUtils.NullOrUndefined(response.HasErrors))
      SnackUtils.OpenError(snackBar, defaultError);
    else
    {
      if (!ObjectUtils.GetValue(response.HasErrors, true))
      {
        returnValue = true;
        SnackUtils.OpenSuccess(snackBar, defaultSuccess);
      }
      else
        SnackUtils.OpenError(snackBar, response.ErrorMessage);
    }

    return returnValue;
  }
  public static HandleBatchListResponse(snackBar: MatSnackBar, response: BatchResponseMessageList, defaultSuccess: string = `Operação realizada com sucesso`, defaultError: string = `Falha no envio da solicitação`): boolean
  {
    let returnValue: boolean = false;
    if (ObjectUtils.NullOrUndefined(response) || ObjectUtils.NullOrUndefined(response.HasErrors))
      SnackUtils.OpenError(snackBar, defaultError);
    else
    {
      if (!ObjectUtils.GetValue(response.HasErrors, true))
      {
        returnValue = true;
        SnackUtils.OpenSuccess(snackBar, defaultSuccess);
      }
      else
        SnackUtils.OpenError(snackBar, response.ErrorMessage);
    }

    return returnValue;
  }
}