﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model'

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const GeographyCountryClass: string = `GeographyCountry`;
export const GeographyCountryTable: string = `geography_country`;

/**
 * Column definitions for the `GeographyCountry` class
 */
export enum GeographyCountryColumns
{
	Id = "Id",
	Name = "Name",
}
export const GeographyCountryColumnsFilter: string[] =  [  ];
export const GeographyCountryColumnsInsert: string[] =  [ GeographyCountryColumns.Name ];
export const GeographyCountryColumnsUpdate: string[] =  [ GeographyCountryColumns.Name ];

/**
 * Implementations of the `GeographyCountry` class
 */
export class GeographyCountry extends Crudable
{
	constructor(
		public Id: number = null,
		public Name: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`GeographyCountry`, `geography_country`, CrudableTableDefinitionGender.Male, `País`, `Países`, true, true, true, true, true, true);

		this.SetColumnDefinition(GeographyCountryColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(GeographyCountryColumns.Name, `name`, `Name`, `País`, `Países`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}
	
	public Clone(): GeographyCountry
	{
		let clone: GeographyCountry = new GeographyCountry();
		clone.Id = this.Id;
		clone.Name = this.Name;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;
		
		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.Name}`;
	}
	
	public ToJson(): any
	{
		let json: any = new Object();
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json["Id"] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json["Name"] = this.Name;
		
		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.Name = json["Name"];
		}
	}
	
	public static FromJson(json: Object): GeographyCountry
	{
		let item: GeographyCountry = new GeographyCountry();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}

/**
 * Implementations of the `GeographyCountryFilter` class
 */
export class GeographyCountryFilter extends Crudable
{
	constructor(
		public Id: number = null,
		public Name: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`GeographyCountryFilter`, `geography_countryFilter`, CrudableTableDefinitionGender.Male, `País`, `Países`, true, true, true, true, true, true);

		this.SetColumnDefinition(GeographyCountryColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(GeographyCountryColumns.Name, `name`, `Name`, `País`, `Países`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}
	
	public Clone(): GeographyCountryFilter
	{
		let clone: GeographyCountryFilter = new GeographyCountryFilter();
		clone.Id = this.Id;
		clone.Name = this.Name;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;
		
		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}
	
	public ToJson(): any
	{
		let json: any = new Object();
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json["Id"] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json["Name"] = this.Name;
		
		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.Name = json["Name"];
		}
	}
	
	public static FromJson(json: Object): GeographyCountryFilter
	{
		let item: GeographyCountryFilter = new GeographyCountryFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}