﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model'

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// Navigation Properties
import { GeographyCity } from 'clericuzzi-lib/entities/models-geo/geographyCity.model';
import { GeographyNeighbourhood } from 'clericuzzi-lib/entities/models-geo/geographyNeighbourhood.model';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const GeographyZipcodeClass: string = `GeographyZipcode`;
export const GeographyZipcodeTable: string = `geography_zipcode`;

/**
 * Column definitions for the `GeographyZipcode` class
 */
export enum GeographyZipcodeColumns
{
	Id = "Id",
	CityId = "CityId",
	Zipcode = "Zipcode",
	Location = "Location",
	NeighbourhoodId = "NeighbourhoodId",

	CityIdParent = "CityIdParent",
	NeighbourhoodIdParent = "NeighbourhoodIdParent",
}
export const GeographyZipcodeColumnsFilter: string[] =  [  ];
export const GeographyZipcodeColumnsInsert: string[] =  [ GeographyZipcodeColumns.Zipcode, GeographyZipcodeColumns.CityId, GeographyZipcodeColumns.NeighbourhoodId ];
export const GeographyZipcodeColumnsUpdate: string[] =  [ GeographyZipcodeColumns.Zipcode, GeographyZipcodeColumns.CityId, GeographyZipcodeColumns.NeighbourhoodId ];

/**
 * Implementations of the `GeographyZipcode` class
 */
export class GeographyZipcode extends Crudable
{
	public CityIdParent: GeographyCity;
	public NeighbourhoodIdParent: GeographyNeighbourhood;

	constructor(
		public Id: number = null,
		public Zipcode: string = null,
		public CityId: number = null,
		public NeighbourhoodId: number = null,
		public Location: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`GeographyZipcode`, `geography_zipcode`, CrudableTableDefinitionGender.Male, `Cep`, `Ceps`, true, true, true, true, true, true);

		this.SetColumnDefinition(GeographyZipcodeColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(GeographyZipcodeColumns.Zipcode, `zipcode`, `Zipcode`, `Cep`, `Ceps`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(GeographyZipcodeColumns.CityId, `city_id`, `CityId`, `Cidade`, `Cidade`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(GeographyZipcodeColumns.NeighbourhoodId, `neighbourhood_id`, `NeighbourhoodId`, `Bairro`, `Bairro`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(GeographyZipcodeColumns.Location, `location`, `Location`, `Rua`, `Ruas`, ComponentTypeEnum.Text, true, true, false, true, false, true, null, null, null, null, null, `left`, true);
	}

	public Clone(): GeographyZipcode
	{
		let clone: GeographyZipcode = new GeographyZipcode();
		clone.Id = this.Id;
		clone.CityId = this.CityId;
		clone.Zipcode = this.Zipcode;
		clone.Location = this.Location;
		clone.NeighbourhoodId = this.NeighbourhoodId;

		clone.CityIdParent = this.CityIdParent;
		clone.NeighbourhoodIdParent = this.NeighbourhoodIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.Zipcode}`;
	}

	public ToJson(): any
	{
		let json: any = new Object();
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json["Id"] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.CityId))
			json["CityId"] = this.CityId;
		if (!ObjectUtils.NullOrUndefined(this.Zipcode))
			json["Zipcode"] = this.Zipcode;
		if (!ObjectUtils.NullOrUndefined(this.Location))
			json["Location"] = this.Location;
		if (!ObjectUtils.NullOrUndefined(this.CityIdParent))
			json["CityIdParent"] = this.CityIdParent.ToJson();
		if (!ObjectUtils.NullOrUndefined(this.NeighbourhoodId))
			json["NeighbourhoodId"] = this.NeighbourhoodId;
		if (!ObjectUtils.NullOrUndefined(this.NeighbourhoodIdParent))
			json["NeighbourhoodIdParent"] = this.NeighbourhoodIdParent.ToJson();

		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.CityId = json["CityId"];
			this.Zipcode = json["Zipcode"];
			this.Location = json["Location"];
			this.NeighbourhoodId = json["NeighbourhoodId"];

			this.CityIdParent = new GeographyCity();
			this.CityIdParent.ParseFromJson(json["CityIdParent"]);
			this.NeighbourhoodIdParent = new GeographyNeighbourhood();
			this.NeighbourhoodIdParent.ParseFromJson(json["NeighbourhoodIdParent"]);
		}
	}

	public static FromJson(json: Object): GeographyZipcode
	{
		let item: GeographyZipcode = new GeographyZipcode();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}

/**
 * Implementations of the `GeographyZipcodeFilter` class
 */
export class GeographyZipcodeFilter extends Crudable
{
	constructor(
		public Id: number = null,
		public Zipcode: string = null,
		public CityId: number[] = null,
		public NeighbourhoodId: number[] = null,
		public Location: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`GeographyZipcodeFilter`, `geography_zipcodeFilter`, CrudableTableDefinitionGender.Male, `Cep`, `Ceps`, true, true, true, true, true, true);

		this.SetColumnDefinition(GeographyZipcodeColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(GeographyZipcodeColumns.Zipcode, `zipcode`, `Zipcode`, `Cep`, `Ceps`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(GeographyZipcodeColumns.CityId, `city_id`, `CityId`, `Cidade`, `Cidade`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(GeographyZipcodeColumns.NeighbourhoodId, `neighbourhood_id`, `NeighbourhoodId`, `Bairro`, `Bairro`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(GeographyZipcodeColumns.Location, `location`, `Location`, `Rua`, `Ruas`, ComponentTypeEnum.Text, true, true, false, true, false, true, null, null, null, null, null, `left`, true);
	}

	public Clone(): GeographyZipcodeFilter
	{
		let clone: GeographyZipcodeFilter = new GeographyZipcodeFilter();
		clone.Id = this.Id;
		clone.CityId = this.CityId;
		clone.Zipcode = this.Zipcode;
		clone.Location = this.Location;
		clone.NeighbourhoodId = this.NeighbourhoodId;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		let json: any = new Object();
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json["Id"] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Zipcode))
			json["Zipcode"] = this.Zipcode;
		if (!ObjectUtils.NullOrUndefined(this.Location))
			json["Location"] = this.Location;
		if (!ObjectUtils.NullOrUndefined(this.CityId))
			json["CityId"] = Array.isArray(this.CityId) ? this.CityId : [ this.CityId ];
		if (!ObjectUtils.NullOrUndefined(this.NeighbourhoodId))
			json["NeighbourhoodId"] = Array.isArray(this.NeighbourhoodId) ? this.NeighbourhoodId : [ this.NeighbourhoodId ];

		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.CityId = json["CityId"];
			this.Zipcode = json["Zipcode"];
			this.Location = json["Location"];
			this.NeighbourhoodId = json["NeighbourhoodId"];
		}
	}

	public static FromJson(json: Object): GeographyZipcodeFilter
	{
		let item: GeographyZipcodeFilter = new GeographyZipcodeFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}
