﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model'

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// Navigation Properties
import { GeographyCity } from 'clericuzzi-lib/entities/models-geo/geographyCity.model';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const GeographyNeighbourhoodClass: string = `GeographyNeighbourhood`;
export const GeographyNeighbourhoodTable: string = `geography_neighbourhood`;

/**
 * Column definitions for the `GeographyNeighbourhood` class
 */
export enum GeographyNeighbourhoodColumns
{
	Id = "Id",
	Name = "Name",
	CityId = "CityId",

	CityIdParent = "CityIdParent",
}
export const GeographyNeighbourhoodColumnsFilter: string[] =  [  ];
export const GeographyNeighbourhoodColumnsInsert: string[] =  [ GeographyNeighbourhoodColumns.CityId, GeographyNeighbourhoodColumns.Name ];
export const GeographyNeighbourhoodColumnsUpdate: string[] =  [ GeographyNeighbourhoodColumns.CityId, GeographyNeighbourhoodColumns.Name ];

/**
 * Implementations of the `GeographyNeighbourhood` class
 */
export class GeographyNeighbourhood extends Crudable
{
	public CityIdParent: GeographyCity;

	constructor(
		public Id: number = null,
		public CityId: number = null,
		public Name: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`GeographyNeighbourhood`, `geography_neighbourhood`, CrudableTableDefinitionGender.Male, `Bairro`, `Bairros`, true, true, true, true, true, true);

		this.SetColumnDefinition(GeographyNeighbourhoodColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(GeographyNeighbourhoodColumns.CityId, `city_id`, `CityId`, `Cidade`, `Cidades`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(GeographyNeighbourhoodColumns.Name, `name`, `Name`, `Bairro`, `Bairros`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): GeographyNeighbourhood
	{
		let clone: GeographyNeighbourhood = new GeographyNeighbourhood();
		clone.Id = this.Id;
		clone.Name = this.Name;
		clone.CityId = this.CityId;

		clone.CityIdParent = this.CityIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.CityIdParent == null ? '' : this.CityIdParent.ToString()} ${this.Name}`;
	}

	public ToJson(): any
	{
		let json: any = new Object();
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json["Id"] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json["Name"] = this.Name;
		if (!ObjectUtils.NullOrUndefined(this.CityId))
			json["CityId"] = this.CityId;
		if (!ObjectUtils.NullOrUndefined(this.CityIdParent))
			json["CityIdParent"] = this.CityIdParent.ToJson();

		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.Name = json["Name"];
			this.CityId = json["CityId"];

			this.CityIdParent = new GeographyCity();
			this.CityIdParent.ParseFromJson(json["CityIdParent"]);
		}
	}

	public static FromJson(json: Object): GeographyNeighbourhood
	{
		let item: GeographyNeighbourhood = new GeographyNeighbourhood();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}

/**
 * Implementations of the `GeographyNeighbourhoodFilter` class
 */
export class GeographyNeighbourhoodFilter extends Crudable
{
	constructor(
		public Id: number = null,
		public CityId: number[] = null,
		public Name: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`GeographyNeighbourhoodFilter`, `geography_neighbourhoodFilter`, CrudableTableDefinitionGender.Male, `Bairro`, `Bairros`, true, true, true, true, true, true);

		this.SetColumnDefinition(GeographyNeighbourhoodColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(GeographyNeighbourhoodColumns.CityId, `city_id`, `CityId`, `Cidade`, `Cidades`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(GeographyNeighbourhoodColumns.Name, `name`, `Name`, `Bairro`, `Bairros`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): GeographyNeighbourhoodFilter
	{
		let clone: GeographyNeighbourhoodFilter = new GeographyNeighbourhoodFilter();
		clone.Id = this.Id;
		clone.Name = this.Name;
		clone.CityId = this.CityId;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		let json: any = new Object();
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json["Id"] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json["Name"] = this.Name;
		if (!ObjectUtils.NullOrUndefined(this.CityId))
			json["CityId"] = Array.isArray(this.CityId) ? this.CityId : [ this.CityId ];

		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.Name = json["Name"];
			this.CityId = json["CityId"];
		}
	}

	public static FromJson(json: Object): GeographyNeighbourhoodFilter
	{
		let item: GeographyNeighbourhoodFilter = new GeographyNeighbourhoodFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}
