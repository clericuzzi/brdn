﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model'

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// Navigation Properties
import { GeographyCountry } from 'clericuzzi-lib/entities/models-geo/geographyCountry.model';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const GeographyStateClass: string = `GeographyState`;
export const GeographyStateTable: string = `geography_state`;

/**
 * Column definitions for the `GeographyState` class
 */
export enum GeographyStateColumns
{
	Id = "Id",
	Name = "Name",
	CountryId = "CountryId",
	Abbreviation = "Abbreviation",

	CountryIdParent = "CountryIdParent",
}
export const GeographyStateColumnsFilter: string[] =  [  ];
export const GeographyStateColumnsInsert: string[] =  [ GeographyStateColumns.CountryId, GeographyStateColumns.Name, GeographyStateColumns.Abbreviation ];
export const GeographyStateColumnsUpdate: string[] =  [ GeographyStateColumns.CountryId, GeographyStateColumns.Name, GeographyStateColumns.Abbreviation ];

/**
 * Implementations of the `GeographyState` class
 */
export class GeographyState extends Crudable
{
	public CountryIdParent: GeographyCountry;

	constructor(
		public Id: number = null,
		public CountryId: number = null,
		public Name: string = null,
		public Abbreviation: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`GeographyState`, `geography_state`, CrudableTableDefinitionGender.Male, `Estado`, `Estados`, true, true, true, true, true, true);

		this.SetColumnDefinition(GeographyStateColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(GeographyStateColumns.CountryId, `country_id`, `CountryId`, `País`, `País`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(GeographyStateColumns.Name, `name`, `Name`, `Estado`, `Estado`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(GeographyStateColumns.Abbreviation, `abbreviation`, `Abbreviation`, `Sigla`, `Sigla`, ComponentTypeEnum.Text, true, true, true, false, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): GeographyState
	{
		let clone: GeographyState = new GeographyState();
		clone.Id = this.Id;
		clone.Name = this.Name;
		clone.CountryId = this.CountryId;
		clone.Abbreviation = this.Abbreviation;

		clone.CountryIdParent = this.CountryIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.CountryIdParent == null ? '' : this.CountryIdParent.ToString()} ${this.Abbreviation}`;
	}

	public ToJson(): any
	{
		let json: any = new Object();
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json["Id"] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json["Name"] = this.Name;
		if (!ObjectUtils.NullOrUndefined(this.CountryId))
			json["CountryId"] = this.CountryId;
		if (!ObjectUtils.NullOrUndefined(this.Abbreviation))
			json["Abbreviation"] = this.Abbreviation;
		if (!ObjectUtils.NullOrUndefined(this.CountryIdParent))
			json["CountryIdParent"] = this.CountryIdParent.ToJson();

		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.Name = json["Name"];
			this.CountryId = json["CountryId"];
			this.Abbreviation = json["Abbreviation"];

			this.CountryIdParent = new GeographyCountry();
			this.CountryIdParent.ParseFromJson(json["CountryIdParent"]);
		}
	}

	public static FromJson(json: Object): GeographyState
	{
		let item: GeographyState = new GeographyState();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}

/**
 * Implementations of the `GeographyStateFilter` class
 */
export class GeographyStateFilter extends Crudable
{
	constructor(
		public Id: number = null,
		public CountryId: number[] = null,
		public Name: string = null,
		public Abbreviation: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`GeographyStateFilter`, `geography_stateFilter`, CrudableTableDefinitionGender.Male, `Estado`, `Estados`, true, true, true, true, true, true);

		this.SetColumnDefinition(GeographyStateColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(GeographyStateColumns.CountryId, `country_id`, `CountryId`, `País`, `País`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(GeographyStateColumns.Name, `name`, `Name`, `Estado`, `Estado`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(GeographyStateColumns.Abbreviation, `abbreviation`, `Abbreviation`, `Sigla`, `Sigla`, ComponentTypeEnum.Text, true, true, true, false, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): GeographyStateFilter
	{
		let clone: GeographyStateFilter = new GeographyStateFilter();
		clone.Id = this.Id;
		clone.Name = this.Name;
		clone.CountryId = this.CountryId;
		clone.Abbreviation = this.Abbreviation;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		let json: any = new Object();
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json["Id"] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json["Name"] = this.Name;
		if (!ObjectUtils.NullOrUndefined(this.Abbreviation))
			json["Abbreviation"] = this.Abbreviation;
		if (!ObjectUtils.NullOrUndefined(this.CountryId))
			json["CountryId"] = Array.isArray(this.CountryId) ? this.CountryId : [ this.CountryId ];

		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.Name = json["Name"];
			this.CountryId = json["CountryId"];
			this.Abbreviation = json["Abbreviation"];
		}
	}

	public static FromJson(json: Object): GeographyStateFilter
	{
		let item: GeographyStateFilter = new GeographyStateFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}
