﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model'

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// Navigation Properties
import { Account } from 'clericuzzi-lib/entities/models/account.model';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const AccountPaymentHistoryClass: string = `AccountPaymentHistory`;
export const AccountPaymentHistoryTable: string = `account_payment_history`;

/**
 * Column definitions for the `AccountPaymentHistory` class
 */
export enum AccountPaymentHistoryColumns
{
	Id = "Id",
	Year = "Year",
	Month = "Month",
	Payment = "Payment",
	AccountId = "AccountId",

	AccountIdParent = "AccountIdParent",
}
export const AccountPaymentHistoryColumnsFilter: string[] =  [ AccountPaymentHistoryColumns.AccountId, AccountPaymentHistoryColumns.Year, AccountPaymentHistoryColumns.Month ];
export const AccountPaymentHistoryColumnsInsert: string[] =  [  ];
export const AccountPaymentHistoryColumnsUpdate: string[] =  [  ];

/**
 * Implementations of the `AccountPaymentHistory` class
 */
export class AccountPaymentHistory extends Crudable
{
	public AccountIdParent: Account;

	constructor(
		public Id: number = null,
		public AccountId: number = null,
		public Year: number = null,
		public Month: number = null,
		public Payment: number = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`AccountPaymentHistory`, `account_payment_history`, CrudableTableDefinitionGender.Male, `Histórico de Pagamento`, `Histórico de Pagamentos`, true, true, true, true, true, true);

		this.SetColumnDefinition(AccountPaymentHistoryColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountPaymentHistoryColumns.AccountId, `account_id`, `AccountId`, `Conta`, `Conta`, ComponentTypeEnum.AutoComplete, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountPaymentHistoryColumns.Year, `year`, `Year`, `Ane`, `Anos`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountPaymentHistoryColumns.Month, `month`, `Month`, `Mês`, `Meses`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountPaymentHistoryColumns.Payment, `payment`, `Payment`, `Pagamento`, `Pagamentos`, ComponentTypeEnum.Decimal, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
	}
	
	public Clone(): AccountPaymentHistory
	{
		let clone: AccountPaymentHistory = new AccountPaymentHistory();
		clone.Id = this.Id;
		clone.Year = this.Year;
		clone.Month = this.Month;
		clone.Payment = this.Payment;
		clone.AccountId = this.AccountId;

		clone.AccountIdParent = this.AccountIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;
		
		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.AccountIdParent == null ? '' : this.AccountIdParent.ToString()} ${this.Year} ${this.Month} ${this.Payment}`;
	}
	
	public ToJson(): any
	{
		let json: any = new Object();
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json["Id"] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Year))
			json["Year"] = this.Year;
		if (!ObjectUtils.NullOrUndefined(this.Month))
			json["Month"] = this.Month;
		if (!ObjectUtils.NullOrUndefined(this.Payment))
			json["Payment"] = this.Payment;
		if (!ObjectUtils.NullOrUndefined(this.AccountId))
			json["AccountId"] = this.AccountId;
		if (!ObjectUtils.NullOrUndefined(this.AccountIdParent))
			json["AccountIdParent"] = this.AccountIdParent.ToJson();
		
		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.Year = json["Year"];
			this.Month = json["Month"];
			this.Payment = json["Payment"];
			this.AccountId = json["AccountId"];
			
			this.AccountIdParent = new Account();
			this.AccountIdParent.ParseFromJson(json["AccountIdParent"]);
		}
	}
	
	public static FromJson(json: Object): AccountPaymentHistory
	{
		let item: AccountPaymentHistory = new AccountPaymentHistory();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}

/**
 * Implementations of the `AccountPaymentHistoryFilter` class
 */
export class AccountPaymentHistoryFilter extends Crudable
{
	constructor(
		public Id: number = null,
		public AccountId: number[] = null,
		public Year: number = null,
		public Month: number = null,
		public Payment: number = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`AccountPaymentHistoryFilter`, `account_payment_historyFilter`, CrudableTableDefinitionGender.Male, `Histórico de Pagamento`, `Histórico de Pagamentos`, true, true, true, true, true, true);

		this.SetColumnDefinition(AccountPaymentHistoryColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountPaymentHistoryColumns.AccountId, `account_id`, `AccountId`, `Conta`, `Conta`, ComponentTypeEnum.AutoComplete, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountPaymentHistoryColumns.Year, `year`, `Year`, `Ane`, `Anos`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountPaymentHistoryColumns.Month, `month`, `Month`, `Mês`, `Meses`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountPaymentHistoryColumns.Payment, `payment`, `Payment`, `Pagamento`, `Pagamentos`, ComponentTypeEnum.Decimal, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
	}
	
	public Clone(): AccountPaymentHistoryFilter
	{
		let clone: AccountPaymentHistoryFilter = new AccountPaymentHistoryFilter();
		clone.Id = this.Id;
		clone.Year = this.Year;
		clone.Month = this.Month;
		clone.Payment = this.Payment;
		clone.AccountId = this.AccountId;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;
		
		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}
	
	public ToJson(): any
	{
		let json: any = new Object();
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json["Id"] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Year))
			json["Year"] = this.Year;
		if (!ObjectUtils.NullOrUndefined(this.Month))
			json["Month"] = this.Month;
		if (!ObjectUtils.NullOrUndefined(this.Payment))
			json["Payment"] = this.Payment;
		if (!ObjectUtils.NullOrUndefined(this.AccountId))
			json["AccountId"] = Array.isArray(this.AccountId) ? this.AccountId : [ this.AccountId ];
		
		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.Year = json["Year"];
			this.Month = json["Month"];
			this.Payment = json["Payment"];
			this.AccountId = json["AccountId"];
		}
	}
	
	public static FromJson(json: Object): AccountPaymentHistoryFilter
	{
		let item: AccountPaymentHistoryFilter = new AccountPaymentHistoryFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}