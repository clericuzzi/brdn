﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model'

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// Navigation Properties
import { StatusType } from 'clericuzzi-lib/entities/models/statusType.model';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const StatusClass: string = `Status`;
export const StatusTable: string = `status`;

/**
 * Column definitions for the `Status` class
 */
export enum StatusColumns
{
	Id = "Id",
	Name = "Name",
	StatusTypeId = "StatusTypeId",

	StatusTypeIdParent = "StatusTypeIdParent",
}
export const StatusColumnsFilter: string[] =  [  ];
export const StatusColumnsInsert: string[] =  [ StatusColumns.StatusTypeId, StatusColumns.Name ];
export const StatusColumnsUpdate: string[] =  [ StatusColumns.StatusTypeId, StatusColumns.Name ];

/**
 * Implementations of the `Status` class
 */
export class Status extends Crudable
{
	public StatusTypeIdParent: StatusType;

	constructor(
		public Id: number = null,
		public StatusTypeId: number = null,
		public Name: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`Status`, `status`, CrudableTableDefinitionGender.Male, `Status`, `Status`, true, true, true, true, true, true);

		this.SetColumnDefinition(StatusColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(StatusColumns.StatusTypeId, `status_type_id`, `StatusTypeId`, `Tipo`, `Tipos`, ComponentTypeEnum.AutoComplete, true, true, true, false, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(StatusColumns.Name, `name`, `Name`, `Status`, `Status`, ComponentTypeEnum.Text, true, true, true, false, true, true, null, null, null, null, null, `left`, false);
	}
	
	public Clone(): Status
	{
		let clone: Status = new Status();
		clone.Id = this.Id;
		clone.Name = this.Name;
		clone.StatusTypeId = this.StatusTypeId;

		clone.StatusTypeIdParent = this.StatusTypeIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;
		
		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.StatusTypeIdParent == null ? '' : this.StatusTypeIdParent.ToString()} ${this.Name}`;
	}
	
	public ToJson(): any
	{
		let json: any = new Object();
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json["Id"] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json["Name"] = this.Name;
		if (!ObjectUtils.NullOrUndefined(this.StatusTypeId))
			json["StatusTypeId"] = this.StatusTypeId;
		if (!ObjectUtils.NullOrUndefined(this.StatusTypeIdParent))
			json["StatusTypeIdParent"] = this.StatusTypeIdParent.ToJson();
		
		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.Name = json["Name"];
			this.StatusTypeId = json["StatusTypeId"];
			
			this.StatusTypeIdParent = new StatusType();
			this.StatusTypeIdParent.ParseFromJson(json["StatusTypeIdParent"]);
		}
	}
	
	public static FromJson(json: Object): Status
	{
		let item: Status = new Status();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}

/**
 * Implementations of the `StatusFilter` class
 */
export class StatusFilter extends Crudable
{
	constructor(
		public Id: number = null,
		public StatusTypeId: number[] = null,
		public Name: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`StatusFilter`, `statusFilter`, CrudableTableDefinitionGender.Male, `Status`, `Status`, true, true, true, true, true, true);

		this.SetColumnDefinition(StatusColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(StatusColumns.StatusTypeId, `status_type_id`, `StatusTypeId`, `Tipo`, `Tipos`, ComponentTypeEnum.AutoComplete, true, true, true, false, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(StatusColumns.Name, `name`, `Name`, `Status`, `Status`, ComponentTypeEnum.Text, true, true, true, false, true, true, null, null, null, null, null, `left`, false);
	}
	
	public Clone(): StatusFilter
	{
		let clone: StatusFilter = new StatusFilter();
		clone.Id = this.Id;
		clone.Name = this.Name;
		clone.StatusTypeId = this.StatusTypeId;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;
		
		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}
	
	public ToJson(): any
	{
		let json: any = new Object();
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json["Id"] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json["Name"] = this.Name;
		if (!ObjectUtils.NullOrUndefined(this.StatusTypeId))
			json["StatusTypeId"] = Array.isArray(this.StatusTypeId) ? this.StatusTypeId : [ this.StatusTypeId ];
		
		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.Name = json["Name"];
			this.StatusTypeId = json["StatusTypeId"];
		}
	}
	
	public static FromJson(json: Object): StatusFilter
	{
		let item: StatusFilter = new StatusFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}