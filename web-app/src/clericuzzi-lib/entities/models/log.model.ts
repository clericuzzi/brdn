﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model'

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// Navigation Properties
import { User } from 'clericuzzi-lib/entities/models/user.model';
import { LogType } from 'clericuzzi-lib/entities/models/logType.model';
import { Status } from 'clericuzzi-lib/entities/models/status.model';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const LogClass: string = `Log`;
export const LogTable: string = `log`;

/**
 * Column definitions for the `Log` class
 */
export enum LogColumns
{
	Id = "Id",
	UserId = "UserId",
	Message = "Message",
	StatusId = "StatusId",
	LogTypeId = "LogTypeId",
	Timestamp = "Timestamp",

	UserIdParent = "UserIdParent",
	StatusIdParent = "StatusIdParent",
	LogTypeIdParent = "LogTypeIdParent",
}

/**
 * Implementations of the `Log` class
 */
export class Log extends Crudable
{
	public UserIdParent: User;
	public LogTypeIdParent: LogType;
	public StatusIdParent: Status;

	constructor(
		public Id: number = null,
		public UserId: number = null,
		public LogTypeId: number = null,
		public StatusId: number = null,
		public Message: string = null,
		public Timestamp: Date = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`Log`, `log`, CrudableTableDefinitionGender.Male, `Log`, `Logs`, true, true, true, true, true, true);

		this.SetColumnDefinition(LogColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(LogColumns.UserId, `user_id`, `UserId`, `Usuário`, `Usuários`, ComponentTypeEnum.AutoComplete, true, true, false, true, false, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(LogColumns.LogTypeId, `log_type_id`, `LogTypeId`, `Tipo`, `Tipos`, ComponentTypeEnum.AutoComplete, true, true, false, true, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(LogColumns.StatusId, `status_id`, `StatusId`, `Status`, `Status`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(LogColumns.Message, `message`, `Message`, `Mensagem`, `Mensagens`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(LogColumns.Timestamp, `timestamp`, `Timestamp`, `Registrado Em`, `Registrados Em`, ComponentTypeEnum.Date, true, true, false, true, false, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
	}

	public Clone(): Log
	{
		let clone: Log = new Log();
		clone.Id = this.Id;
		clone.UserId = this.UserId;
		clone.Message = this.Message;
		clone.StatusId = this.StatusId;
		clone.LogTypeId = this.LogTypeId;
		clone.Timestamp = this.Timestamp;

		clone.UserIdParent = this.UserIdParent;
		clone.StatusIdParent = this.StatusIdParent;
		clone.LogTypeIdParent = this.LogTypeIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.Message}`;
	}

	public ToJson(): any
	{
		let json: any = new Object();
		json["Id"] = ObjectUtils.NullOrUndefined(this.Id) ? 0 : this.Id.toString();
		json["UserId"] = ObjectUtils.NullOrUndefined(this.UserId) ? null : this.UserId.toString();
		json["Message"] = ObjectUtils.NullOrUndefined(this.Message) ? null : this.Message.toString();
		json["LogTypeId"] = ObjectUtils.NullOrUndefined(this.LogTypeId) ? 0 : this.LogTypeId.toString();
		json["StatusId"] = ObjectUtils.NullOrUndefined(this.StatusId) ? null : this.StatusId.toString();
		json["Timestamp"] = ObjectUtils.NullOrUndefined(this.Timestamp) ? DateUtils.ToDateServer(new Date()) : DateUtils.ToDateServer(this.Timestamp);

		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.UserId = json["UserId"];
			this.Message = json["Message"];
			this.StatusId = json["StatusId"];
			this.LogTypeId = json["LogTypeId"];
			this.Timestamp = DateUtils.FromDateServer(json["Timestamp"]);

			this.UserIdParent = new User();
			this.UserIdParent.ParseFromJson(json["UserIdParent"]);
			this.LogTypeIdParent = new LogType();
			this.LogTypeIdParent.ParseFromJson(json["LogTypeIdParent"]);
			this.StatusIdParent = new Status();
			this.StatusIdParent.ParseFromJson(json["StatusIdParent"]);
		}
	}

	public static FromJson(json: Object): Log
	{
		let item: Log = new Log();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}