﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model'

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const SystemParameterClass: string = `SystemParameter`;
export const SystemParameterTable: string = `system_parameter`;

/**
 * Column definitions for the `SystemParameter` class
 */
export enum SystemParameterColumns
{
	Id = "Id",
	Value = "Value",
	Parameter = "Parameter",
}
export const SystemParameterColumnsFilter: string[] =  [  ];
export const SystemParameterColumnsInsert: string[] =  [ SystemParameterColumns.Parameter, SystemParameterColumns.Value ];
export const SystemParameterColumnsUpdate: string[] =  [ SystemParameterColumns.Parameter, SystemParameterColumns.Value ];

/**
 * Implementations of the `SystemParameter` class
 */
export class SystemParameter extends Crudable
{
	constructor(
		public Id: number = null,
		public Parameter: string = null,
		public Value: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`SystemParameter`, `system_parameter`, CrudableTableDefinitionGender.Male, `Parâmetro do Sistema`, `Parâmetros do Sistema`, true, true, true, true, true, true);

		this.SetColumnDefinition(SystemParameterColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(SystemParameterColumns.Parameter, `parameter`, `Parameter`, `Parâmetro`, `Parâmetros`, ComponentTypeEnum.Text, true, true, true, false, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(SystemParameterColumns.Value, `value`, `Value`, `Valor`, `Valores`, ComponentTypeEnum.Text, true, true, true, false, true, true, null, null, null, null, null, `left`, false);
	}
	
	public Clone(): SystemParameter
	{
		let clone: SystemParameter = new SystemParameter();
		clone.Id = this.Id;
		clone.Value = this.Value;
		clone.Parameter = this.Parameter;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;
		
		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.Parameter}`;
	}
	
	public ToJson(): any
	{
		let json: any = new Object();
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json["Id"] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Value))
			json["Value"] = this.Value;
		if (!ObjectUtils.NullOrUndefined(this.Parameter))
			json["Parameter"] = this.Parameter;
		
		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.Value = json["Value"];
			this.Parameter = json["Parameter"];
		}
	}
	
	public static FromJson(json: Object): SystemParameter
	{
		let item: SystemParameter = new SystemParameter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}

/**
 * Implementations of the `SystemParameterFilter` class
 */
export class SystemParameterFilter extends Crudable
{
	constructor(
		public Id: number = null,
		public Parameter: string = null,
		public Value: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`SystemParameterFilter`, `system_parameterFilter`, CrudableTableDefinitionGender.Male, `Parâmetro do Sistema`, `Parâmetros do Sistema`, true, true, true, true, true, true);

		this.SetColumnDefinition(SystemParameterColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(SystemParameterColumns.Parameter, `parameter`, `Parameter`, `Parâmetro`, `Parâmetros`, ComponentTypeEnum.Text, true, true, true, false, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(SystemParameterColumns.Value, `value`, `Value`, `Valor`, `Valores`, ComponentTypeEnum.Text, true, true, true, false, true, true, null, null, null, null, null, `left`, false);
	}
	
	public Clone(): SystemParameterFilter
	{
		let clone: SystemParameterFilter = new SystemParameterFilter();
		clone.Id = this.Id;
		clone.Value = this.Value;
		clone.Parameter = this.Parameter;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;
		
		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}
	
	public ToJson(): any
	{
		let json: any = new Object();
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json["Id"] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Value))
			json["Value"] = this.Value;
		if (!ObjectUtils.NullOrUndefined(this.Parameter))
			json["Parameter"] = this.Parameter;
		
		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.Value = json["Value"];
			this.Parameter = json["Parameter"];
		}
	}
	
	public static FromJson(json: Object): SystemParameterFilter
	{
		let item: SystemParameterFilter = new SystemParameterFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}