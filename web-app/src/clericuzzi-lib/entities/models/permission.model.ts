﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model'

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// Navigation Properties
import { Account } from 'clericuzzi-lib/entities/models/account.model';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const PermissionClass: string = `Permission`;
export const PermissionTable: string = `permission`;

/**
 * Column definitions for the `Permission` class
 */
export enum PermissionColumns
{
	Id = "Id",
	Name = "Name",
	Priority = "Priority",
	AccountId = "AccountId",

	AccountIdParent = "AccountIdParent",
}
export const PermissionColumnsFilter: string[] =  [ PermissionColumns.AccountId, PermissionColumns.Name ];
export const PermissionColumnsInsert: string[] =  [ PermissionColumns.AccountId, PermissionColumns.Name, PermissionColumns.Priority ];
export const PermissionColumnsUpdate: string[] =  [ PermissionColumns.AccountId, PermissionColumns.Name, PermissionColumns.Priority ];

/**
 * Implementations of the `Permission` class
 */
export class Permission extends Crudable
{
	public AccountIdParent: Account;

	constructor(
		public Id: number = null,
		public AccountId: number = null,
		public Name: string = null,
		public Priority: number = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`Permission`, `permission`, CrudableTableDefinitionGender.Female, `Permissão`, `Permissões`, true, true, true, true, true, true);

		this.SetColumnDefinition(PermissionColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(PermissionColumns.AccountId, `account_id`, `AccountId`, `Conta`, `Contas`, ComponentTypeEnum.AutoComplete, true, true, true, false, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(PermissionColumns.Name, `name`, `Name`, `Nome`, `Nomes`, ComponentTypeEnum.Text, true, true, true, false, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(PermissionColumns.Priority, `priority`, `Priority`, `Prioridade`, `Prioridades`, ComponentTypeEnum.Number, true, true, true, false, true, true, null, null, null, null, null, `left`, false);
	}
	
	public Clone(): Permission
	{
		let clone: Permission = new Permission();
		clone.Id = this.Id;
		clone.Name = this.Name;
		clone.Priority = this.Priority;
		clone.AccountId = this.AccountId;

		clone.AccountIdParent = this.AccountIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;
		
		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.AccountIdParent == null ? '' : this.AccountIdParent.ToString()} ${this.Name}`;
	}
	
	public ToJson(): any
	{
		let json: any = new Object();
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json["Id"] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json["Name"] = this.Name;
		if (!ObjectUtils.NullOrUndefined(this.Priority))
			json["Priority"] = this.Priority;
		if (!ObjectUtils.NullOrUndefined(this.AccountId))
			json["AccountId"] = this.AccountId;
		if (!ObjectUtils.NullOrUndefined(this.AccountIdParent))
			json["AccountIdParent"] = this.AccountIdParent.ToJson();
		
		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.Name = json["Name"];
			this.Priority = json["Priority"];
			this.AccountId = json["AccountId"];
			
			this.AccountIdParent = new Account();
			this.AccountIdParent.ParseFromJson(json["AccountIdParent"]);
		}
	}
	
	public static FromJson(json: Object): Permission
	{
		let item: Permission = new Permission();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}

/**
 * Implementations of the `PermissionFilter` class
 */
export class PermissionFilter extends Crudable
{
	constructor(
		public Id: number = null,
		public AccountId: number[] = null,
		public Name: string = null,
		public Priority: number = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`PermissionFilter`, `permissionFilter`, CrudableTableDefinitionGender.Female, `Permissão`, `Permissões`, true, true, true, true, true, true);

		this.SetColumnDefinition(PermissionColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(PermissionColumns.AccountId, `account_id`, `AccountId`, `Conta`, `Contas`, ComponentTypeEnum.AutoComplete, true, true, true, false, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(PermissionColumns.Name, `name`, `Name`, `Nome`, `Nomes`, ComponentTypeEnum.Text, true, true, true, false, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(PermissionColumns.Priority, `priority`, `Priority`, `Prioridade`, `Prioridades`, ComponentTypeEnum.Number, true, true, true, false, true, true, null, null, null, null, null, `left`, false);
	}
	
	public Clone(): PermissionFilter
	{
		let clone: PermissionFilter = new PermissionFilter();
		clone.Id = this.Id;
		clone.Name = this.Name;
		clone.Priority = this.Priority;
		clone.AccountId = this.AccountId;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;
		
		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}
	
	public ToJson(): any
	{
		let json: any = new Object();
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json["Id"] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json["Name"] = this.Name;
		if (!ObjectUtils.NullOrUndefined(this.Priority))
			json["Priority"] = this.Priority;
		if (!ObjectUtils.NullOrUndefined(this.AccountId))
			json["AccountId"] = Array.isArray(this.AccountId) ? this.AccountId : [ this.AccountId ];
		
		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.Name = json["Name"];
			this.Priority = json["Priority"];
			this.AccountId = json["AccountId"];
		}
	}
	
	public static FromJson(json: Object): PermissionFilter
	{
		let item: PermissionFilter = new PermissionFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}