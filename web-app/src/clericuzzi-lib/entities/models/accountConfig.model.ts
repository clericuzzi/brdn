﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model'

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// Navigation Properties
import { Account } from 'clericuzzi-lib/entities/models/account.model';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const AccountConfigClass: string = `AccountConfig`;
export const AccountConfigTable: string = `account_config`;

/**
 * Column definitions for the `AccountConfig` class
 */
export enum AccountConfigColumns
{
	Id = "Id",
	Value = "Value",
	AccountId = "AccountId",
	Parameter = "Parameter",

	AccountIdParent = "AccountIdParent",
}
export const AccountConfigColumnsFilter: string[] =  [  ];
export const AccountConfigColumnsInsert: string[] =  [ AccountConfigColumns.AccountId, AccountConfigColumns.Parameter, AccountConfigColumns.Value ];
export const AccountConfigColumnsUpdate: string[] =  [ AccountConfigColumns.AccountId, AccountConfigColumns.Parameter, AccountConfigColumns.Value ];

/**
 * Implementations of the `AccountConfig` class
 */
export class AccountConfig extends Crudable
{
	public AccountIdParent: Account;

	constructor(
		public Id: number = null,
		public AccountId: number = null,
		public Parameter: string = null,
		public Value: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`AccountConfig`, `account_config`, CrudableTableDefinitionGender.Female, `Configuração de Conta`, `Configurações de Conta`, true, true, true, true, true, true);

		this.SetColumnDefinition(AccountConfigColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountConfigColumns.AccountId, `account_id`, `AccountId`, `Conta`, `Contas`, ComponentTypeEnum.AutoComplete, true, true, true, false, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountConfigColumns.Parameter, `parameter`, `Parameter`, `Parâmetro`, `Parâmetros`, ComponentTypeEnum.Text, true, true, true, false, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountConfigColumns.Value, `value`, `Value`, `Valor`, `Valore`, ComponentTypeEnum.Text, true, true, true, false, true, true, null, null, null, null, null, `left`, false);
	}
	
	public Clone(): AccountConfig
	{
		let clone: AccountConfig = new AccountConfig();
		clone.Id = this.Id;
		clone.Value = this.Value;
		clone.AccountId = this.AccountId;
		clone.Parameter = this.Parameter;

		clone.AccountIdParent = this.AccountIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;
		
		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.AccountIdParent == null ? '' : this.AccountIdParent.ToString()} ${this.Parameter}`;
	}
	
	public ToJson(): any
	{
		let json: any = new Object();
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json["Id"] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Value))
			json["Value"] = this.Value;
		if (!ObjectUtils.NullOrUndefined(this.AccountId))
			json["AccountId"] = this.AccountId;
		if (!ObjectUtils.NullOrUndefined(this.Parameter))
			json["Parameter"] = this.Parameter;
		if (!ObjectUtils.NullOrUndefined(this.AccountIdParent))
			json["AccountIdParent"] = this.AccountIdParent.ToJson();
		
		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.Value = json["Value"];
			this.AccountId = json["AccountId"];
			this.Parameter = json["Parameter"];
			
			this.AccountIdParent = new Account();
			this.AccountIdParent.ParseFromJson(json["AccountIdParent"]);
		}
	}
	
	public static FromJson(json: Object): AccountConfig
	{
		let item: AccountConfig = new AccountConfig();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}

/**
 * Implementations of the `AccountConfigFilter` class
 */
export class AccountConfigFilter extends Crudable
{
	constructor(
		public Id: number = null,
		public AccountId: number[] = null,
		public Parameter: string = null,
		public Value: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`AccountConfigFilter`, `account_configFilter`, CrudableTableDefinitionGender.Female, `Configuração de Conta`, `Configurações de Conta`, true, true, true, true, true, true);

		this.SetColumnDefinition(AccountConfigColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountConfigColumns.AccountId, `account_id`, `AccountId`, `Conta`, `Contas`, ComponentTypeEnum.AutoComplete, true, true, true, false, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountConfigColumns.Parameter, `parameter`, `Parameter`, `Parâmetro`, `Parâmetros`, ComponentTypeEnum.Text, true, true, true, false, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountConfigColumns.Value, `value`, `Value`, `Valor`, `Valore`, ComponentTypeEnum.Text, true, true, true, false, true, true, null, null, null, null, null, `left`, false);
	}
	
	public Clone(): AccountConfigFilter
	{
		let clone: AccountConfigFilter = new AccountConfigFilter();
		clone.Id = this.Id;
		clone.Value = this.Value;
		clone.AccountId = this.AccountId;
		clone.Parameter = this.Parameter;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;
		
		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}
	
	public ToJson(): any
	{
		let json: any = new Object();
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json["Id"] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Value))
			json["Value"] = this.Value;
		if (!ObjectUtils.NullOrUndefined(this.Parameter))
			json["Parameter"] = this.Parameter;
		if (!ObjectUtils.NullOrUndefined(this.AccountId))
			json["AccountId"] = Array.isArray(this.AccountId) ? this.AccountId : [ this.AccountId ];
		
		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.Value = json["Value"];
			this.AccountId = json["AccountId"];
			this.Parameter = json["Parameter"];
		}
	}
	
	public static FromJson(json: Object): AccountConfigFilter
	{
		let item: AccountConfigFilter = new AccountConfigFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}