﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model'

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// Navigation Properties
import { Country } from 'clericuzzi-lib/entities/models/country.model';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const StateClass: string = `GeographyState`;
export const StateTable: string = `geography_state`;

/**
 * Column definitions for the `State` class
 */
export enum StateColumns
{
	Id = "Id",
	Name = "Name",
	CountryId = "CountryId",
	Abbreviation = "Abbreviation",

	CountryIdParent = "CountryIdParent",
}

/**
 * Implementations of the `State` class
 */
export class State extends Crudable
{
	public CountryIdParent: Country;

	constructor(
		public Id: number = null,
		public CountryId: number = null,
		public Name: string = null,
		public Abbreviation: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`State`, `geography_state`, CrudableTableDefinitionGender.Male, `Estado`, `Estados`, true, true, true, true, true, true);

		this.SetColumnDefinition(StateColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(StateColumns.CountryId, `country_id`, `CountryId`, `País`, `Países`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(StateColumns.Name, `name`, `Name`, `Estado`, `Estado`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(StateColumns.Abbreviation, `abbreviation`, `Abbreviation`, `Sigla`, `Siglas`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): State
	{
		let clone: State = new State();
		clone.Id = this.Id;
		clone.Name = this.Name;
		clone.CountryId = this.CountryId;
		clone.Abbreviation = this.Abbreviation;

		clone.CountryIdParent = this.CountryIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.CountryIdParent == null ? '' : this.CountryIdParent.ToString()}`;
	}

	public ToJson(): any
	{
		let json: any = new Object();
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json["Id"] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json["Name"] = this.Name;
		if (!ObjectUtils.NullOrUndefined(this.CountryId))
			json["CountryId"] = this.CountryId;
		if (!ObjectUtils.NullOrUndefined(this.Abbreviation))
			json["Abbreviation"] = this.Abbreviation;

		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.Name = json["Name"];
			this.CountryId = json["CountryId"];
			this.Abbreviation = json["Abbreviation"];

			this.CountryIdParent = new Country();
			this.CountryIdParent.ParseFromJson(json["CountryIdParent"]);
		}
	}

	public static FromJson(json: Object): State
	{
		let item: State = new State();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}

/**
 * Implementations of the `StateFilter` class
 */
export class StateFilter extends Crudable
{
	constructor(
		public Id: number = null,
		public CountryId: number[] = null,
		public Name: string = null,
		public Abbreviation: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`StateFilter`, `stateFilter`, CrudableTableDefinitionGender.Male, `Estado`, `Estados`, true, true, true, true, true, true);

		this.SetColumnDefinition(StateColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(StateColumns.CountryId, `country_id`, `CountryId`, `País`, `Países`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(StateColumns.Name, `name`, `Name`, `Estado`, `Estado`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(StateColumns.Abbreviation, `abbreviation`, `Abbreviation`, `Sigla`, `Siglas`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): StateFilter
	{
		let clone: StateFilter = new StateFilter();
		clone.Id = this.Id;
		clone.Name = this.Name;
		clone.CountryId = this.CountryId;
		clone.Abbreviation = this.Abbreviation;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		let json: any = new Object();
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json["Id"] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json["Name"] = this.Name;
		if (!ObjectUtils.NullOrUndefined(this.Abbreviation))
			json["Abbreviation"] = this.Abbreviation;
		if (!ObjectUtils.NullOrUndefined(this.CountryId))
			json["CountryId"] = Array.isArray(this.CountryId) ? this.CountryId : [ this.CountryId ];

		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.Name = json["Name"];
			this.CountryId = json["CountryId"];
			this.Abbreviation = json["Abbreviation"];
		}
	}

	public static FromJson(json: Object): StateFilter
	{
		let item: StateFilter = new StateFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}