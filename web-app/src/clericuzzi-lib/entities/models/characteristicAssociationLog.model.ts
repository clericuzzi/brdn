﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model'

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// Navigation Properties
import { Characteristic } from './characteristic.model';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const CharacteristicAssociationLogClass: string = `CharacteristicAssociationLog`;
export const CharacteristicAssociationLogTable: string = `characteristic_association_log`;

/**
 * Column definitions for the `CharacteristicAssociationLog` class
 */
export enum CharacteristicAssociationLogColumns
{
	Id = "Id",
	Action = "Action",
	EntityId = "EntityId",
	Timestamp = "Timestamp",
	CharacteristicId = "CharacteristicId",

	CharacteristicIdParent = "CharacteristicIdParent",
}
export const CharacteristicAssociationLogColumnsFilter: string[] =  [  ];
export const CharacteristicAssociationLogColumnsInsert: string[] =  [  ];
export const CharacteristicAssociationLogColumnsUpdate: string[] =  [  ];

/**
 * Implementations of the `CharacteristicAssociationLog` class
 */
export class CharacteristicAssociationLog extends Crudable
{
	public CharacteristicIdParent: Characteristic;

	constructor(
		public Id: number = null,
		public CharacteristicId: number = null,
		public EntityId: number = null,
		public Action: number = null,
		public Timestamp: Date = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`CharacteristicAssociationLog`, `characteristic_association_log`, CrudableTableDefinitionGender.Male, `Log de Características`, `Logs de Características`, true, true, true, true, true, true);

		this.SetColumnDefinition(CharacteristicAssociationLogColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CharacteristicAssociationLogColumns.CharacteristicId, `characteristic_id`, `CharacteristicId`, `Característica`, `Características`, ComponentTypeEnum.AutoComplete, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CharacteristicAssociationLogColumns.EntityId, `entity_id`, `EntityId`, `Entidade`, `Entidades`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CharacteristicAssociationLogColumns.Action, `action`, `Action`, `Ação`, `Ações`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CharacteristicAssociationLogColumns.Timestamp, `timestamp`, `Timestamp`, `Registro`, `Registros`, ComponentTypeEnum.Date, true, true, false, false, false, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
	}
	
	public Clone(): CharacteristicAssociationLog
	{
		let clone: CharacteristicAssociationLog = new CharacteristicAssociationLog();
		clone.Id = this.Id;
		clone.Action = this.Action;
		clone.EntityId = this.EntityId;
		clone.Timestamp = this.Timestamp;
		clone.CharacteristicId = this.CharacteristicId;

		clone.CharacteristicIdParent = this.CharacteristicIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;
		
		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.CharacteristicIdParent == null ? '' : this.CharacteristicIdParent.ToString()} ${this.EntityId} ${this.Action}`;
	}
	
	public ToJson(): any
	{
		let json: any = new Object();
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json["Id"] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Action))
			json["Action"] = this.Action;
		if (!ObjectUtils.NullOrUndefined(this.EntityId))
			json["EntityId"] = this.EntityId;
		if (!ObjectUtils.NullOrUndefined(this.CharacteristicId))
			json["CharacteristicId"] = this.CharacteristicId;
		if (!ObjectUtils.NullOrUndefined(this.Timestamp))
			json["Timestamp"] = DateUtils.ToDateServer(this.Timestamp);
		if (!ObjectUtils.NullOrUndefined(this.CharacteristicIdParent))
			json["CharacteristicIdParent"] = this.CharacteristicIdParent.ToJson();
		
		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.Action = json["Action"];
			this.EntityId = json["EntityId"];
			this.CharacteristicId = json["CharacteristicId"];
			this.Timestamp = DateUtils.FromDateServer(json["Timestamp"]);
			
			this.CharacteristicIdParent = new Characteristic();
			this.CharacteristicIdParent.ParseFromJson(json["CharacteristicIdParent"]);
		}
	}
	
	public static FromJson(json: Object): CharacteristicAssociationLog
	{
		let item: CharacteristicAssociationLog = new CharacteristicAssociationLog();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}

/**
 * Implementations of the `CharacteristicAssociationLogFilter` class
 */
export class CharacteristicAssociationLogFilter extends Crudable
{
	constructor(
		public Id: number = null,
		public CharacteristicId: number[] = null,
		public EntityId: number = null,
		public Action: number = null,
		public Timestamp: Date = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`CharacteristicAssociationLogFilter`, `characteristic_association_logFilter`, CrudableTableDefinitionGender.Male, `Log de Características`, `Logs de Características`, true, true, true, true, true, true);

		this.SetColumnDefinition(CharacteristicAssociationLogColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CharacteristicAssociationLogColumns.CharacteristicId, `characteristic_id`, `CharacteristicId`, `Característica`, `Características`, ComponentTypeEnum.AutoComplete, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CharacteristicAssociationLogColumns.EntityId, `entity_id`, `EntityId`, `Entidade`, `Entidades`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CharacteristicAssociationLogColumns.Action, `action`, `Action`, `Ação`, `Ações`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CharacteristicAssociationLogColumns.Timestamp, `timestamp`, `Timestamp`, `Registro`, `Registros`, ComponentTypeEnum.Date, true, true, false, false, false, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
	}
	
	public Clone(): CharacteristicAssociationLogFilter
	{
		let clone: CharacteristicAssociationLogFilter = new CharacteristicAssociationLogFilter();
		clone.Id = this.Id;
		clone.Action = this.Action;
		clone.EntityId = this.EntityId;
		clone.Timestamp = this.Timestamp;
		clone.CharacteristicId = this.CharacteristicId;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;
		
		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}
	
	public ToJson(): any
	{
		let json: any = new Object();
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json["Id"] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Action))
			json["Action"] = this.Action;
		if (!ObjectUtils.NullOrUndefined(this.EntityId))
			json["EntityId"] = this.EntityId;
		if (!ObjectUtils.NullOrUndefined(this.Timestamp))
			json["Timestamp"] = DateUtils.ToDateServer(this.Timestamp);
		if (!ObjectUtils.NullOrUndefined(this.CharacteristicId))
			json["CharacteristicId"] = Array.isArray(this.CharacteristicId) ? this.CharacteristicId : [ this.CharacteristicId ];
		
		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.Action = json["Action"];
			this.EntityId = json["EntityId"];
			this.CharacteristicId = json["CharacteristicId"];
			this.Timestamp = DateUtils.FromDateServer(json["Timestamp"]);
		}
	}
	
	public static FromJson(json: Object): CharacteristicAssociationLogFilter
	{
		let item: CharacteristicAssociationLogFilter = new CharacteristicAssociationLogFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}