﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model'

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// Navigation Properties
import { State } from 'clericuzzi-lib/entities/models/state.model';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const CityClass: string = `GeographyCity`;
export const CityTable: string = `geography_city`;

/**
 * Column definitions for the `City` class
 */
export enum CityColumns
{
	Id = "Id",
	Name = "Name",
	StateId = "StateId",

	StateIdParent = "StateIdParent",
}

/**
 * Implementations of the `City` class
 */
export class City extends Crudable
{
	public StateIdParent: State;

	constructor(
		public Id: number = null,
		public StateId: number = null,
		public Name: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`City`, `geography_statecity`, CrudableTableDefinitionGender.Female, `Cidade`, `Cidades`, true, true, true, true, true, true);

		this.SetColumnDefinition(CityColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CityColumns.StateId, `state_id`, `StateId`, `Estado`, `Estados`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CityColumns.Name, `name`, `Name`, `Cidade`, `Cidades`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): City
	{
		let clone: City = new City();
		clone.Id = this.Id;
		clone.Name = this.Name;
		clone.StateId = this.StateId;

		clone.StateIdParent = this.StateIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.StateIdParent == null ? '' : this.StateIdParent.ToString()}`;
	}

	public ToJson(): any
	{
		let json: any = new Object();
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json["Id"] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json["Name"] = this.Name;
		if (!ObjectUtils.NullOrUndefined(this.StateId))
			json["StateId"] = this.StateId;

		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.Name = json["Name"];
			this.StateId = json["StateId"];

			this.StateIdParent = new State();
			this.StateIdParent.ParseFromJson(json["StateIdParent"]);
		}
	}

	public static FromJson(json: Object): City
	{
		let item: City = new City();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}

/**
 * Implementations of the `CityFilter` class
 */
export class CityFilter extends Crudable
{
	constructor(
		public Id: number = null,
		public StateId: number[] = null,
		public Name: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`CityFilter`, `cityFilter`, CrudableTableDefinitionGender.Female, `Cidade`, `Cidades`, true, true, true, true, true, true);

		this.SetColumnDefinition(CityColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CityColumns.StateId, `state_id`, `StateId`, `Estado`, `Estados`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CityColumns.Name, `name`, `Name`, `Cidade`, `Cidades`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): CityFilter
	{
		let clone: CityFilter = new CityFilter();
		clone.Id = this.Id;
		clone.Name = this.Name;
		clone.StateId = this.StateId;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		let json: any = new Object();
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json["Id"] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json["Name"] = this.Name;
		if (!ObjectUtils.NullOrUndefined(this.StateId))
			json["StateId"] = Array.isArray(this.StateId) ? this.StateId : [ this.StateId ];

		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.Name = json["Name"];
			this.StateId = json["StateId"];
		}
	}

	public static FromJson(json: Object): CityFilter
	{
		let item: CityFilter = new CityFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}