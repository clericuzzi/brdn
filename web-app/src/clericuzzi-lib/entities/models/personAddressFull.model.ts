﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model'

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// Navigation Properties
import { Person } from 'clericuzzi-lib/entities/models/person.model';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const PersonAddressFullClass: string = `PersonAddressFull`;
export const PersonAddressFullTable: string = `person_address_full`;

/**
 * Column definitions for the `PersonAddressFull` class
 */
export enum PersonAddressFullColumns
{
	Id = "Id",
	City = "City",
	Main = "Main",
	State = "State",
	Street = "Street",
	Number = "Number",
	Zipcode = "Zipcode",
	PersonId = "PersonId",
	Compliment = "Compliment",
	Neighbourhood = "Neighbourhood",

	PersonIdParent = "PersonIdParent",
}

/**
 * Implementations of the `PersonAddressFull` class
 */
export class PersonAddressFull extends Crudable
{
	public PersonIdParent: Person;

	constructor(
		public Id: number = null,
		public PersonId: number = null,
		public Street: string = null,
		public Number: string = null,
		public Zipcode: string = null,
		public State: string = null,
		public City: string = null,
		public Neighbourhood: string = null,
		public Compliment: string = null,
		public Main: number = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`PersonAddressFull`, `person_address_full`, CrudableTableDefinitionGender.Male, `Endereço`, `Endereços`, true, true, true, true, true, true);

		this.SetColumnDefinition(PersonAddressFullColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(PersonAddressFullColumns.PersonId, `person_id`, `PersonId`, `Pessoa`, `Pessoas`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(PersonAddressFullColumns.Street, `street`, `Street`, `Rua`, `Ruas`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(PersonAddressFullColumns.Number, `number`, `Number`, `Número`, `Números`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `center`, true);
		this.SetColumnDefinition(PersonAddressFullColumns.Zipcode, `zipcode`, `Zipcode`, `Cep`, `Ceps`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `center`, true);
		this.SetColumnDefinition(PersonAddressFullColumns.State, `state`, `State`, `Estado`, `Estados`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `center`, true);
		this.SetColumnDefinition(PersonAddressFullColumns.City, `city`, `City`, `Cidade`, `Cidades`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(PersonAddressFullColumns.Neighbourhood, `neighbourhood`, `Neighbourhood`, `Bairro`, `Bairros`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(PersonAddressFullColumns.Compliment, `compliment`, `Compliment`, `Complemento`, `Complementos`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(PersonAddressFullColumns.Main, `main`, `Main`, `Principal`, `Principais`, ComponentTypeEnum.ComboYesNo, true, true, true, true, true, true, null, StringUtils.TransformYesNo, null, null, null, `left`, false);
	}

	public Clone(): PersonAddressFull
	{
		let clone: PersonAddressFull = new PersonAddressFull();
		clone.Id = this.Id;
		clone.City = this.City;
		clone.Main = this.Main;
		clone.State = this.State;
		clone.Street = this.Street;
		clone.Number = this.Number;
		clone.Zipcode = this.Zipcode;
		clone.PersonId = this.PersonId;
		clone.Compliment = this.Compliment;
		clone.Neighbourhood = this.Neighbourhood;

		clone.PersonIdParent = this.PersonIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.Street}`;
	}

	public ToJson(): any
	{
		let json: any = new Object();
		json["Id"] = ObjectUtils.NullOrUndefined(this.Id) ? 0 : this.Id.toString();
		json["Main"] = ObjectUtils.NullOrUndefined(this.Main) ? 0 : this.Main.toString();
		json["City"] = ObjectUtils.NullOrUndefined(this.City) ? null : this.City.toString();
		json["State"] = ObjectUtils.NullOrUndefined(this.State) ? null : this.State.toString();
		json["Street"] = ObjectUtils.NullOrUndefined(this.Street) ? null : this.Street.toString();
		json["Number"] = ObjectUtils.NullOrUndefined(this.Number) ? null : this.Number.toString();
		json["PersonId"] = ObjectUtils.NullOrUndefined(this.PersonId) ? 0 : this.PersonId.toString();
		json["Zipcode"] = ObjectUtils.NullOrUndefined(this.Zipcode) ? null : this.Zipcode.toString();
		json["Compliment"] = ObjectUtils.NullOrUndefined(this.Compliment) ? null : this.Compliment.toString();
		json["Neighbourhood"] = ObjectUtils.NullOrUndefined(this.Neighbourhood) ? null : this.Neighbourhood.toString();

		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.City = json["City"];
			this.Main = json["Main"];
			this.State = json["State"];
			this.Street = json["Street"];
			this.Number = json["Number"];
			this.Zipcode = json["Zipcode"];
			this.PersonId = json["PersonId"];
			this.Compliment = json["Compliment"];
			this.Neighbourhood = json["Neighbourhood"];

			this.PersonIdParent = new Person();
			this.PersonIdParent.ParseFromJson(json["PersonIdParent"]);
		}
	}

	public static FromJson(json: Object): PersonAddressFull
	{
		let item: PersonAddressFull = new PersonAddressFull();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}