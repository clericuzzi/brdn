﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model'

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// Navigation Properties
import { Activity } from './activity.model';
import { Characteristic } from './characteristic.model';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const ActivityCharacteristicMustNotClass: string = `ActivityCharacteristicMustNot`;
export const ActivityCharacteristicMustNotTable: string = `activity_characteristic_must_not`;

/**
 * Column definitions for the `ActivityCharacteristicMustNot` class
 */
export enum ActivityCharacteristicMustNotColumns
{
	Id = "Id",
	ActivityId = "ActivityId",
	CharacteristicId = "CharacteristicId",

	ActivityIdParent = "ActivityIdParent",
	CharacteristicIdParent = "CharacteristicIdParent",
}
export const ActivityCharacteristicMustNotColumnsFilter: string[] =  [  ];
export const ActivityCharacteristicMustNotColumnsInsert: string[] =  [ ActivityCharacteristicMustNotColumns.ActivityId, ActivityCharacteristicMustNotColumns.CharacteristicId ];
export const ActivityCharacteristicMustNotColumnsUpdate: string[] =  [ ActivityCharacteristicMustNotColumns.ActivityId, ActivityCharacteristicMustNotColumns.CharacteristicId ];

/**
 * Implementations of the `ActivityCharacteristicMustNot` class
 */
export class ActivityCharacteristicMustNot extends Crudable
{
	public ActivityIdParent: Activity;
	public CharacteristicIdParent: Characteristic;

	constructor(
		public Id: number = null,
		public ActivityId: number = null,
		public CharacteristicId: number = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`ActivityCharacteristicMustNot`, `activity_characteristic_must_not`, CrudableTableDefinitionGender.Male, `Activity Characteristic Must Not`, `Activity Characteristic Must Nots`, true, true, true, true, true, true);

		this.SetColumnDefinition(ActivityCharacteristicMustNotColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ActivityCharacteristicMustNotColumns.ActivityId, `activity_id`, `ActivityId`, `Activity Id`, `Activity Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ActivityCharacteristicMustNotColumns.CharacteristicId, `characteristic_id`, `CharacteristicId`, `Characteristic Id`, `Characteristic Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}
	
	public Clone(): ActivityCharacteristicMustNot
	{
		let clone: ActivityCharacteristicMustNot = new ActivityCharacteristicMustNot();
		clone.Id = this.Id;
		clone.ActivityId = this.ActivityId;
		clone.CharacteristicId = this.CharacteristicId;

		clone.ActivityIdParent = this.ActivityIdParent;
		clone.CharacteristicIdParent = this.CharacteristicIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;
		
		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.ActivityIdParent == null ? '' : this.ActivityIdParent.ToString()}`;
	}
	
	public ToJson(): any
	{
		let json: any = new Object();
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json["Id"] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.ActivityId))
			json["ActivityId"] = this.ActivityId;
		if (!ObjectUtils.NullOrUndefined(this.CharacteristicId))
			json["CharacteristicId"] = this.CharacteristicId;
		if (!ObjectUtils.NullOrUndefined(this.ActivityIdParent))
			json["ActivityIdParent"] = this.ActivityIdParent.ToJson();
		if (!ObjectUtils.NullOrUndefined(this.CharacteristicIdParent))
			json["CharacteristicIdParent"] = this.CharacteristicIdParent.ToJson();
		
		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.ActivityId = json["ActivityId"];
			this.CharacteristicId = json["CharacteristicId"];
			
			this.ActivityIdParent = new Activity();
			this.ActivityIdParent.ParseFromJson(json["ActivityIdParent"]);
			this.CharacteristicIdParent = new Characteristic();
			this.CharacteristicIdParent.ParseFromJson(json["CharacteristicIdParent"]);
		}
	}
	
	public static FromJson(json: Object): ActivityCharacteristicMustNot
	{
		let item: ActivityCharacteristicMustNot = new ActivityCharacteristicMustNot();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}

/**
 * Implementations of the `ActivityCharacteristicMustNotFilter` class
 */
export class ActivityCharacteristicMustNotFilter extends Crudable
{
	constructor(
		public Id: number = null,
		public ActivityId: number[] = null,
		public CharacteristicId: number[] = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`ActivityCharacteristicMustNotFilter`, `activity_characteristic_must_notFilter`, CrudableTableDefinitionGender.Male, `Activity Characteristic Must Not`, `Activity Characteristic Must Nots`, true, true, true, true, true, true);

		this.SetColumnDefinition(ActivityCharacteristicMustNotColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ActivityCharacteristicMustNotColumns.ActivityId, `activity_id`, `ActivityId`, `Activity Id`, `Activity Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ActivityCharacteristicMustNotColumns.CharacteristicId, `characteristic_id`, `CharacteristicId`, `Characteristic Id`, `Characteristic Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}
	
	public Clone(): ActivityCharacteristicMustNotFilter
	{
		let clone: ActivityCharacteristicMustNotFilter = new ActivityCharacteristicMustNotFilter();
		clone.Id = this.Id;
		clone.ActivityId = this.ActivityId;
		clone.CharacteristicId = this.CharacteristicId;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;
		
		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}
	
	public ToJson(): any
	{
		let json: any = new Object();
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json["Id"] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.ActivityId))
			json["ActivityId"] = Array.isArray(this.ActivityId) ? this.ActivityId : [ this.ActivityId ];
		if (!ObjectUtils.NullOrUndefined(this.CharacteristicId))
			json["CharacteristicId"] = Array.isArray(this.CharacteristicId) ? this.CharacteristicId : [ this.CharacteristicId ];
		
		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.ActivityId = json["ActivityId"];
			this.CharacteristicId = json["CharacteristicId"];
		}
	}
	
	public static FromJson(json: Object): ActivityCharacteristicMustNotFilter
	{
		let item: ActivityCharacteristicMustNotFilter = new ActivityCharacteristicMustNotFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}