﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model'

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// Navigation Properties
import { Person } from 'clericuzzi-lib/entities/models/person.model';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const PersonEmailClass: string = `PersonEmail`;
export const PersonEmailTable: string = `person_email`;

/**
 * Column definitions for the `PersonEmail` class
 */
export enum PersonEmailColumns
{
	Id = "Id",
	Main = "Main",
	Email = "Email",
	PersonId = "PersonId",

	PersonIdParent = "PersonIdParent",
}

/**
 * Implementations of the `PersonEmail` class
 */
export class PersonEmail extends Crudable
{
	public PersonIdParent: Person;

	constructor(
		public Id: number = null,
		public PersonId: number = null,
		public Email: string = null,
		public Main: number = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`PersonEmail`, `person_email`, CrudableTableDefinitionGender.Male, `Person Email`, `Person Emails`, true, true, true, true, true, true);

		this.SetColumnDefinition(PersonEmailColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(PersonEmailColumns.PersonId, `person_id`, `PersonId`, `Person Id`, `Person Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(PersonEmailColumns.Email, `email`, `Email`, `Email`, `Emails`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(PersonEmailColumns.Main, `main`, `Main`, `Main`, `Mains`, ComponentTypeEnum.Number, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): PersonEmail
	{
		let clone: PersonEmail = new PersonEmail();
		clone.Id = this.Id;
		clone.Main = this.Main;
		clone.Email = this.Email;
		clone.PersonId = this.PersonId;

		clone.PersonIdParent = this.PersonIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.PersonIdParent == null ? '' : this.PersonIdParent.ToString()}`;
	}

	public ToJson(): any
	{
		let json: any = new Object();
		json["Id"] = ObjectUtils.NullOrUndefined(this.Id) ? 0 : this.Id.toString();
		json["Main"] = ObjectUtils.NullOrUndefined(this.Main) ? 0 : this.Main.toString();
		json["Email"] = ObjectUtils.NullOrUndefined(this.Email) ? null : this.Email.toString();
		json["PersonId"] = ObjectUtils.NullOrUndefined(this.PersonId) ? 0 : this.PersonId.toString();

		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.Main = json["Main"];
			this.Email = json["Email"];
			this.PersonId = json["PersonId"];

			this.PersonIdParent = new Person();
			this.PersonIdParent.ParseFromJson(json["PersonIdParent"]);
		}
	}

	public static FromJson(json: Object): PersonEmail
	{
		let item: PersonEmail = new PersonEmail();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}