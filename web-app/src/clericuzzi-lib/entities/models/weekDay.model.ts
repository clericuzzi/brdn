﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model'

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const WeekDayClass: string = `WeekDay`;
export const WeekDayTable: string = `week_day`;

/**
 * Column definitions for the `WeekDay` class
 */
export enum WeekDayColumns
{
	Id = "Id",
	Name = "Name",
}

/**
 * Implementations of the `WeekDay` class
 */
export class WeekDay extends Crudable
{
	constructor(
		public Id: number = null,
		public Name: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`WeekDay`, `week_day`, CrudableTableDefinitionGender.Male, `Dia da semana`, `Dias da semana`, true, true, true, true, true, true);

		this.SetColumnDefinition(WeekDayColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(WeekDayColumns.Name, `name`, `Name`, `Dia`, `Dias`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): WeekDay
	{
		let clone: WeekDay = new WeekDay();
		clone.Id = this.Id;
		clone.Name = this.Name;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.Name}`;
	}

	public ToJson(): any
	{
		let json: any = new Object();
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json["Id"] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json["Name"] = this.Name;

		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.Name = json["Name"];
		}
	}

	public static FromJson(json: Object): WeekDay
	{
		let item: WeekDay = new WeekDay();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}

/**
 * Implementations of the `WeekDayFilter` class
 */
export class WeekDayFilter extends Crudable
{
	constructor(
		public Id: number = null,
		public Name: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`WeekDayFilter`, `week_dayFilter`, CrudableTableDefinitionGender.Male, `Dia da semana`, `Dias da semana`, true, true, true, true, true, true);

		this.SetColumnDefinition(WeekDayColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(WeekDayColumns.Name, `name`, `Name`, `Dia`, `Dias`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): WeekDayFilter
	{
		let clone: WeekDayFilter = new WeekDayFilter();
		clone.Id = this.Id;
		clone.Name = this.Name;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		let json: any = new Object();
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json["Id"] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json["Name"] = this.Name;

		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.Name = json["Name"];
		}
	}

	public static FromJson(json: Object): WeekDayFilter
	{
		let item: WeekDayFilter = new WeekDayFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}