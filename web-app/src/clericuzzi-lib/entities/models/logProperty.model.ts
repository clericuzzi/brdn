﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model'

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// Navigation Properties
import { User } from 'clericuzzi-lib/entities/models/user.model';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const LogPropertyClass: string = `LogProperty`;
export const LogPropertyTable: string = `log_property`;

/**
 * Column definitions for the `LogProperty` class
 */
export enum LogPropertyColumns
{
	Id = "Id",
	UserId = "UserId",
	Property = "Property",
	OldValue = "OldValue",
	NewValue = "NewValue",
	Timestamp = "Timestamp",

	UserIdParent = "UserIdParent",
}

/**
 * Implementations of the `LogProperty` class
 */
export class LogProperty extends Crudable
{
	public UserIdParent: User;

	constructor(
		public Id: number = null,
		public UserId: number = null,
		public Property: string = null,
		public OldValue: string = null,
		public NewValue: string = null,
		public Timestamp: Date = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`LogProperty`, `log_property`, CrudableTableDefinitionGender.Male, `Log de Propriedades`, `Logs de Propriedades`, true, true, true, true, true, true);

		this.SetColumnDefinition(LogPropertyColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(LogPropertyColumns.UserId, `user_id`, `UserId`, `Usuário`, `Usuários`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(LogPropertyColumns.Property, `property`, `Property`, `Propriedade`, `Propriedades`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(LogPropertyColumns.OldValue, `old_value`, `OldValue`, `De`, `De`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(LogPropertyColumns.NewValue, `new_value`, `NewValue`, `Para`, `Para`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(LogPropertyColumns.Timestamp, `timestamp`, `Timestamp`, `Em`, `Em`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
	}

	public Clone(): LogProperty
	{
		let clone: LogProperty = new LogProperty();
		clone.Id = this.Id;
		clone.UserId = this.UserId;
		clone.Property = this.Property;
		clone.OldValue = this.OldValue;
		clone.NewValue = this.NewValue;
		clone.Timestamp = this.Timestamp;

		clone.UserIdParent = this.UserIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.UserIdParent == null ? '' : this.UserIdParent.ToString()} ${this.Property}`;
	}

	public ToJson(): any
	{
		let json: any = new Object();
		json["Id"] = ObjectUtils.NullOrUndefined(this.Id) ? 0 : this.Id.toString();
		json["UserId"] = ObjectUtils.NullOrUndefined(this.UserId) ? null : this.UserId.toString();
		json["Property"] = ObjectUtils.NullOrUndefined(this.Property) ? null : this.Property.toString();
		json["OldValue"] = ObjectUtils.NullOrUndefined(this.OldValue) ? null : this.OldValue.toString();
		json["NewValue"] = ObjectUtils.NullOrUndefined(this.NewValue) ? null : this.NewValue.toString();
		json["Timestamp"] = ObjectUtils.NullOrUndefined(this.Timestamp) ? DateUtils.ToDateServer(new Date()) : DateUtils.ToDateServer(this.Timestamp);

		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.UserId = json["UserId"];
			this.Property = json["Property"];
			this.OldValue = json["OldValue"];
			this.NewValue = json["NewValue"];
			this.Timestamp = DateUtils.FromDateServer(json["Timestamp"]);

			this.UserIdParent = new User();
			this.UserIdParent.ParseFromJson(json["UserIdParent"]);
		}
	}

	public static FromJson(json: Object): LogProperty
	{
		let item: LogProperty = new LogProperty();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}