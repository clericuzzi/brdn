﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model'

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// Navigation Properties
import { Account } from 'clericuzzi-lib/entities/models/account.model';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const SecureZoneClass: string = `SecureZone`;
export const SecureZoneTable: string = `secure_zone`;

/**
 * Column definitions for the `SecureZone` class
 */
export enum SecureZoneColumns
{
	Id = "Id",
	Name = "Name",
	Latitude = "Latitude",
	AccountId = "AccountId",
	Longitude = "Longitude",

	AccountIdParent = "AccountIdParent",
}

/**
 * Implementations of the `SecureZone` class
 */
export class SecureZone extends Crudable
{
	public AccountIdParent: Account;

	constructor(
		public Id: number = null,
		public AccountId: number = null,
		public Name: string = null,
		public Latitude: number = null,
		public Longitude: number = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`SecureZone`, `secure_zone`, CrudableTableDefinitionGender.Male, `Secure Zone`, `Secure Zones`, true, true, true, true, true, true);

		this.SetColumnDefinition(SecureZoneColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(SecureZoneColumns.AccountId, `account_id`, `AccountId`, `Account Id`, `Account Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(SecureZoneColumns.Name, `name`, `Name`, `Name`, `Names`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(SecureZoneColumns.Latitude, `latitude`, `Latitude`, `Latitude`, `Latitudes`, ComponentTypeEnum.Number, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(SecureZoneColumns.Longitude, `longitude`, `Longitude`, `Longitude`, `Longitudes`, ComponentTypeEnum.Number, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): SecureZone
	{
		let clone: SecureZone = new SecureZone();
		clone.Id = this.Id;
		clone.Name = this.Name;
		clone.Latitude = this.Latitude;
		clone.AccountId = this.AccountId;
		clone.Longitude = this.Longitude;

		clone.AccountIdParent = this.AccountIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.AccountIdParent == null ? '' : this.AccountIdParent.ToString()}`;
	}

	public ToJson(): any
	{
		let json: any = new Object();
		json["Id"] = ObjectUtils.NullOrUndefined(this.Id) ? 0 : this.Id.toString();
		json["Name"] = ObjectUtils.NullOrUndefined(this.Name) ? null : this.Name.toString();
		json["Latitude"] = ObjectUtils.NullOrUndefined(this.Latitude) ? 0 : this.Latitude.toString();
		json["AccountId"] = ObjectUtils.NullOrUndefined(this.AccountId) ? 0 : this.AccountId.toString();
		json["Longitude"] = ObjectUtils.NullOrUndefined(this.Longitude) ? 0 : this.Longitude.toString();

		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.Name = json["Name"];
			this.Latitude = json["Latitude"];
			this.AccountId = json["AccountId"];
			this.Longitude = json["Longitude"];

			this.AccountIdParent = new Account();
			this.AccountIdParent.ParseFromJson(json["AccountIdParent"]);
		}
	}

	public static FromJson(json: Object): SecureZone
	{
		let item: SecureZone = new SecureZone();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}