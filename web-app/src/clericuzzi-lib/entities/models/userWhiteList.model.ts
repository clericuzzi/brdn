﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model'

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// Navigation Properties
import { User } from 'clericuzzi-lib/entities/models/user.model';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const UserWhiteListClass: string = `UserWhiteList`;
export const UserWhiteListTable: string = `user_white_list`;

/**
 * Column definitions for the `UserWhiteList` class
 */
export enum UserWhiteListColumns
{
	Id = "Id",
	UserId = "UserId",

	UserIdParent = "UserIdParent",
}

/**
 * Implementations of the `UserWhiteList` class
 */
export class UserWhiteList extends Crudable
{
	public UserIdParent: User;

	constructor(
		public Id: number = null,
		public UserId: number = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`UserWhiteList`, `user_white_list`, CrudableTableDefinitionGender.Male, `User White List`, `User White Lists`, true, true, true, true, true, true);

		this.SetColumnDefinition(UserWhiteListColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(UserWhiteListColumns.UserId, `user_id`, `UserId`, `User Id`, `User Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): UserWhiteList
	{
		let clone: UserWhiteList = new UserWhiteList();
		clone.Id = this.Id;
		clone.UserId = this.UserId;

		clone.UserIdParent = this.UserIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.UserIdParent == null ? '' : this.UserIdParent.ToString()}`;
	}

	public ToJson(): any
	{
		let json: any = new Object();
		json["Id"] = ObjectUtils.NullOrUndefined(this.Id) ? 0 : this.Id.toString();
		json["UserId"] = ObjectUtils.NullOrUndefined(this.UserId) ? 0 : this.UserId.toString();

		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.UserId = json["UserId"];

			this.UserIdParent = new User();
			this.UserIdParent.ParseFromJson(json["UserIdParent"]);
		}
	}

	public static FromJson(json: Object): UserWhiteList
	{
		let item: UserWhiteList = new UserWhiteList();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}