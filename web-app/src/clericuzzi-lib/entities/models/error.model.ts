﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model'

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const ErrorClass: string = `Error`;
export const ErrorTable: string = `error`;

/**
 * Column definitions for the `Error` class
 */
export enum ErrorColumns
{
	Id = "Id",
	ClassName = "ClassName",
	Timestamp = "Timestamp",
	MethodName = "MethodName",
	ExceptionType = "ExceptionType",
	ExceptionMessage = "ExceptionMessage",
	ExceptionStackTrace = "ExceptionStackTrace",
}

/**
 * Implementations of the `Error` class
 */
export class Error extends Crudable
{
	constructor(
		public Id: number = null,
		public ClassName: string = null,
		public MethodName: string = null,
		public ExceptionType: string = null,
		public ExceptionMessage: string = null,
		public ExceptionStackTrace: string = null,
		public Timestamp: Date = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`Error`, `error`, CrudableTableDefinitionGender.Male, `Error`, `Errors`, true, true, true, true, true, true);

		this.SetColumnDefinition(ErrorColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ErrorColumns.ClassName, `class_name`, `ClassName`, `Class Name`, `Class Names`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ErrorColumns.MethodName, `method_name`, `MethodName`, `Method Name`, `Method Names`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ErrorColumns.ExceptionType, `exception_type`, `ExceptionType`, `Exception Type`, `Exception Types`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ErrorColumns.ExceptionMessage, `exception_message`, `ExceptionMessage`, `Exception Message`, `Exception Messages`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ErrorColumns.ExceptionStackTrace, `exception_stack_trace`, `ExceptionStackTrace`, `Exception Stack Trace`, `Exception Stack Traces`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ErrorColumns.Timestamp, `timestamp`, `Timestamp`, `Timestamp`, `Timestamps`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
	}

	public Clone(): Error
	{
		let clone: Error = new Error();
		clone.Id = this.Id;
		clone.ClassName = this.ClassName;
		clone.Timestamp = this.Timestamp;
		clone.MethodName = this.MethodName;
		clone.ExceptionType = this.ExceptionType;
		clone.ExceptionMessage = this.ExceptionMessage;
		clone.ExceptionStackTrace = this.ExceptionStackTrace;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.ClassName}`;
	}

	public ToJson(): any
	{
		let json: any = new Object();
		json["Id"] = ObjectUtils.NullOrUndefined(this.Id) ? 0 : this.Id.toString();
		json["ClassName"] = ObjectUtils.NullOrUndefined(this.ClassName) ? null : this.ClassName.toString();
		json["MethodName"] = ObjectUtils.NullOrUndefined(this.MethodName) ? null : this.MethodName.toString();
		json["ExceptionType"] = ObjectUtils.NullOrUndefined(this.ExceptionType) ? null : this.ExceptionType.toString();
		json["ExceptionMessage"] = ObjectUtils.NullOrUndefined(this.ExceptionMessage) ? null : this.ExceptionMessage.toString();
		json["ExceptionStackTrace"] = ObjectUtils.NullOrUndefined(this.ExceptionStackTrace) ? null : this.ExceptionStackTrace.toString();
		json["Timestamp"] = ObjectUtils.NullOrUndefined(this.Timestamp) ? DateUtils.ToDateServer(new Date()) : DateUtils.ToDateServer(this.Timestamp);

		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.ClassName = json["ClassName"];
			this.MethodName = json["MethodName"];
			this.ExceptionType = json["ExceptionType"];
			this.ExceptionMessage = json["ExceptionMessage"];
			this.ExceptionStackTrace = json["ExceptionStackTrace"];
			this.Timestamp = DateUtils.FromDateServer(json["Timestamp"]);
		}
	}

	public static FromJson(json: Object): Error
	{
		let item: Error = new Error();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}