﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model';

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// Navigation Properties
import { Account } from 'clericuzzi-lib/entities/models/account.model';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const ActivityClass: string = `Activity`;
export const ActivityTable: string = `activity`;

/**
 * Column definitions for the `Activity` class
 */
export enum ActivityColumns
{
  Id = 'Id',
  Name = 'Name',
  AccountId = 'AccountId',
  DailyCount = 'DailyCount',

  AccountIdParent = 'AccountIdParent',
}
export const ActivityColumnsFilter: string[] = [ActivityColumns.Name];
export const ActivityColumnsInsert: string[] = [ActivityColumns.Name];
export const ActivityColumnsUpdate: string[] = [ActivityColumns.Name];

/**
 * Implementations of the `Activity` class
 */
export class Activity extends Crudable
{
  public AccountIdParent: Account;

  public static FromJson(json: Object): Activity
  {
    const item: Activity = new Activity();
    if (json)
      item.ParseFromJson(json);

    return item;
  }

  constructor(
    public Id: number = null,
    public AccountId: number = null,
    public Name: string = null,
    public DailyCount: number = null,
  )
  {
    super();
    this._InitDefinition();
  }
  private _InitDefinition(): void
  {
    this.SetTableDefinition(`Campanha`, `activity`, CrudableTableDefinitionGender.Female, `Atividade`, `Atividades`, true, true, true, true, true, true);

    this.SetColumnDefinition(ActivityColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(ActivityColumns.AccountId, `account_id`, `AccountId`, `Conta`, `Contas`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(ActivityColumns.Name, `name`, `Name`, `Nome`, `Nome`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(ActivityColumns.DailyCount, `daily_count`, `DailyCount`, `Cota diária`, `Cotas diária`, ComponentTypeEnum.Text, true, true, true, false, true, true, null, null, null, null, null, `left`, true);
  }

  public Clone(): Activity
  {
    const clone: Activity = new Activity();
    clone.Id = this.Id;
    clone.Name = this.Name;
    clone.AccountId = this.AccountId;
    clone.DailyCount = this.DailyCount;

    clone.AccountIdParent = this.AccountIdParent;

    clone.TableDefinition = this.TableDefinition;
    clone.ColumnDefinitions = this.ColumnDefinitions;

    clone.CustomProperties = this.CustomProperties;

    return clone;
  }
  public ToString(): string
  {
    return `${this.AccountIdParent == null ? '' : this.AccountIdParent.ToString()} ${this.Name}`;
  }

  public ToJson(): any
  {
    const json: any = new Object();
    if (!ObjectUtils.NullOrUndefined(this.Id))
      json['Id'] = this.Id;
    if (!ObjectUtils.NullOrUndefined(this.Name))
      json['Name'] = this.Name;
    if (!ObjectUtils.NullOrUndefined(this.AccountId))
      json['AccountId'] = this.AccountId;
    if (!ObjectUtils.NullOrUndefined(this.DailyCount))
      json['DailyCount'] = this.DailyCount;
    if (!ObjectUtils.NullOrUndefined(this.AccountIdParent))
      json['AccountIdParent'] = this.AccountIdParent.ToJson();

    this.SerializeCustomProperties(json);

    return json;
  }
  public ParseToJson(): string
  {
    const json: any = this.ToJson();

    return JSON.stringify(json);
  }
  public ParseFromJson(json: Object): void
  {
    if (json)
    {
      this.Id = json['Id'];
      this.Name = json['Name'];
      this.AccountId = json['AccountId'];
      this.DailyCount = json['DailyCount'];

      this.AccountIdParent = new Account();
      this.AccountIdParent.ParseFromJson(json['AccountIdParent']);
    }
  }
}

/**
 * Implementations of the `ActivityFilter` class
 */
export class ActivityFilter extends Crudable
{
  public static FromJson(json: Object): ActivityFilter
  {
    const item: ActivityFilter = new ActivityFilter();
    if (json)
      item.ParseFromJson(json);

    return item;
  }

  constructor(
    public Id: number = null,
    public AccountId: number[] = null,
    public Name: string = null,
    public DailyCount: number = null,
  )
  {
    super();
    this._InitDefinition();
  }
  private _InitDefinition(): void
  {
    this.SetTableDefinition(`ActivityFilter`, `activityFilter`, CrudableTableDefinitionGender.Female, `Atividade`, `Atividades`, true, true, true, true, true, true);

    this.SetColumnDefinition(ActivityColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(ActivityColumns.AccountId, `account_id`, `AccountId`, `Conta`, `Contas`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(ActivityColumns.Name, `name`, `Name`, `Nome`, `Nome`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(ActivityColumns.DailyCount, `daily_count`, `DailyCount`, `Cota diária`, `Cotas diária`, ComponentTypeEnum.Text, true, true, true, false, true, true, null, null, null, null, null, `left`, true);
  }

  public Clone(): ActivityFilter
  {
    const clone: ActivityFilter = new ActivityFilter();
    clone.Id = this.Id;
    clone.Name = this.Name;
    clone.AccountId = this.AccountId;
    clone.DailyCount = this.DailyCount;

    clone.TableDefinition = this.TableDefinition;
    clone.ColumnDefinitions = this.ColumnDefinitions;

    clone.CustomProperties = this.CustomProperties;

    return clone;
  }
  public ToString(): string
  {
    return `null`;
  }

  public ToJson(): any
  {
    const json: any = new Object();
    if (!ObjectUtils.NullOrUndefined(this.Id))
      json['Id'] = this.Id;
    if (!ObjectUtils.NullOrUndefined(this.Name))
      json['Name'] = this.Name;
    if (!ObjectUtils.NullOrUndefined(this.DailyCount))
      json['DailyCount'] = this.DailyCount;
    if (!ObjectUtils.NullOrUndefined(this.AccountId))
      json['AccountId'] = Array.isArray(this.AccountId) ? this.AccountId : [this.AccountId];

    this.SerializeCustomProperties(json);

    return json;
  }
  public ParseToJson(): string
  {
    const json: any = this.ToJson();

    return JSON.stringify(json);
  }
  public ParseFromJson(json: Object): void
  {
    if (json)
    {
      this.Id = json['Id'];
      this.Name = json['Name'];
      this.AccountId = json['AccountId'];
      this.DailyCount = json['DailyCount'];
    }
  }
}
