﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model'

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// Navigation Properties
import { City } from 'clericuzzi-lib/entities/models/city.model';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const NeighbourhoodClass: string = `GeographyNeighbourhood`;
export const NeighbourhoodTable: string = `geography_neighbourhood`;

/**
 * Column definitions for the `Neighbourhood` class
 */
export enum NeighbourhoodColumns
{
	Id = "Id",
	Name = "Name",
	CityId = "CityId",

	CityIdParent = "CityIdParent",
}

/**
 * Implementations of the `Neighbourhood` class
 */
export class Neighbourhood extends Crudable
{
	public CityIdParent: City;

	constructor(
		public Id: number = null,
		public CityId: number = null,
		public Name: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`Neighbourhood`, `geography_stateneighbourhood`, CrudableTableDefinitionGender.Male, `Bairro`, `Bairros`, true, true, true, true, true, true);

		this.SetColumnDefinition(NeighbourhoodColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(NeighbourhoodColumns.CityId, `city_id`, `CityId`, `Cidade`, `Cidades`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(NeighbourhoodColumns.Name, `name`, `Name`, `Bairro`, `Bairros`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): Neighbourhood
	{
		let clone: Neighbourhood = new Neighbourhood();
		clone.Id = this.Id;
		clone.Name = this.Name;
		clone.CityId = this.CityId;

		clone.CityIdParent = this.CityIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.CityIdParent == null ? '' : this.CityIdParent.ToString()}`;
	}

	public ToJson(): any
	{
		let json: any = new Object();
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json["Id"] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json["Name"] = this.Name;
		if (!ObjectUtils.NullOrUndefined(this.CityId))
			json["CityId"] = this.CityId;

		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.Name = json["Name"];
			this.CityId = json["CityId"];

			this.CityIdParent = new City();
			this.CityIdParent.ParseFromJson(json["CityIdParent"]);
		}
	}

	public static FromJson(json: Object): Neighbourhood
	{
		let item: Neighbourhood = new Neighbourhood();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}

/**
 * Implementations of the `NeighbourhoodFilter` class
 */
export class NeighbourhoodFilter extends Crudable
{
	constructor(
		public Id: number = null,
		public CityId: number[] = null,
		public Name: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`NeighbourhoodFilter`, `neighbourhoodFilter`, CrudableTableDefinitionGender.Male, `Bairro`, `Bairros`, true, true, true, true, true, true);

		this.SetColumnDefinition(NeighbourhoodColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(NeighbourhoodColumns.CityId, `city_id`, `CityId`, `Cidade`, `Cidades`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(NeighbourhoodColumns.Name, `name`, `Name`, `Bairro`, `Bairros`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): NeighbourhoodFilter
	{
		let clone: NeighbourhoodFilter = new NeighbourhoodFilter();
		clone.Id = this.Id;
		clone.Name = this.Name;
		clone.CityId = this.CityId;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		let json: any = new Object();
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json["Id"] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json["Name"] = this.Name;
		if (!ObjectUtils.NullOrUndefined(this.CityId))
			json["CityId"] = Array.isArray(this.CityId) ? this.CityId : [ this.CityId ];

		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.Name = json["Name"];
			this.CityId = json["CityId"];
		}
	}

	public static FromJson(json: Object): NeighbourhoodFilter
	{
		let item: NeighbourhoodFilter = new NeighbourhoodFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}