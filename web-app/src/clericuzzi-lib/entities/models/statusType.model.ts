﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model'

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const StatusTypeClass: string = `StatusType`;
export const StatusTypeTable: string = `status_type`;

/**
 * Column definitions for the `StatusType` class
 */
export enum StatusTypeColumns
{
	Id = "Id",
	Name = "Name",
}
export const StatusTypeColumnsFilter: string[] =  [  ];
export const StatusTypeColumnsInsert: string[] =  [ StatusTypeColumns.Name ];
export const StatusTypeColumnsUpdate: string[] =  [ StatusTypeColumns.Name ];

/**
 * Implementations of the `StatusType` class
 */
export class StatusType extends Crudable
{
	constructor(
		public Id: number = null,
		public Name: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`StatusType`, `status_type`, CrudableTableDefinitionGender.Male, `Tipo de Status`, `Tipos de Status`, true, true, true, true, true, true);

		this.SetColumnDefinition(StatusTypeColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(StatusTypeColumns.Name, `name`, `Name`, `Nome`, `Nomes`, ComponentTypeEnum.Text, true, true, true, false, true, true, null, null, null, null, null, `left`, false);
	}
	
	public Clone(): StatusType
	{
		let clone: StatusType = new StatusType();
		clone.Id = this.Id;
		clone.Name = this.Name;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;
		
		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.Name}`;
	}
	
	public ToJson(): any
	{
		let json: any = new Object();
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json["Id"] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json["Name"] = this.Name;
		
		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.Name = json["Name"];
		}
	}
	
	public static FromJson(json: Object): StatusType
	{
		let item: StatusType = new StatusType();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}

/**
 * Implementations of the `StatusTypeFilter` class
 */
export class StatusTypeFilter extends Crudable
{
	constructor(
		public Id: number = null,
		public Name: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`StatusTypeFilter`, `status_typeFilter`, CrudableTableDefinitionGender.Male, `Tipo de Status`, `Tipos de Status`, true, true, true, true, true, true);

		this.SetColumnDefinition(StatusTypeColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(StatusTypeColumns.Name, `name`, `Name`, `Nome`, `Nomes`, ComponentTypeEnum.Text, true, true, true, false, true, true, null, null, null, null, null, `left`, false);
	}
	
	public Clone(): StatusTypeFilter
	{
		let clone: StatusTypeFilter = new StatusTypeFilter();
		clone.Id = this.Id;
		clone.Name = this.Name;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;
		
		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}
	
	public ToJson(): any
	{
		let json: any = new Object();
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json["Id"] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json["Name"] = this.Name;
		
		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.Name = json["Name"];
		}
	}
	
	public static FromJson(json: Object): StatusTypeFilter
	{
		let item: StatusTypeFilter = new StatusTypeFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}