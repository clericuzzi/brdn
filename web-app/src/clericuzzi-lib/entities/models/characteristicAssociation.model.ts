﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model'

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// Navigation Properties
import { Characteristic } from './characteristic.model';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const CharacteristicAssociationClass: string = `CharacteristicAssociation`;
export const CharacteristicAssociationTable: string = `characteristic_association`;

/**
 * Column definitions for the `CharacteristicAssociation` class
 */
export enum CharacteristicAssociationColumns
{
	Id = "Id",
	EntityId = "EntityId",
	Timestamp = "Timestamp",
	CharacteristicId = "CharacteristicId",

	CharacteristicIdParent = "CharacteristicIdParent",
}
export const CharacteristicAssociationColumnsFilter: string[] =  [  ];
export const CharacteristicAssociationColumnsInsert: string[] =  [  ];
export const CharacteristicAssociationColumnsUpdate: string[] =  [  ];

/**
 * Implementations of the `CharacteristicAssociation` class
 */
export class CharacteristicAssociation extends Crudable
{
	public CharacteristicIdParent: Characteristic;

	constructor(
		public Id: number = null,
		public CharacteristicId: number = null,
		public EntityId: number = null,
		public Timestamp: Date = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`CharacteristicAssociation`, `characteristic_association`, CrudableTableDefinitionGender.Female, `Características`, `Características`, true, true, true, true, true, true);

		this.SetColumnDefinition(CharacteristicAssociationColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CharacteristicAssociationColumns.CharacteristicId, `characteristic_id`, `CharacteristicId`, `Característica`, `Características`, ComponentTypeEnum.AutoComplete, true, true, false, true, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CharacteristicAssociationColumns.EntityId, `entity_id`, `EntityId`, `Entidade`, `Entidades`, ComponentTypeEnum.Number, true, true, false, true, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CharacteristicAssociationColumns.Timestamp, `timestamp`, `Timestamp`, `Registro`, `Registros`, ComponentTypeEnum.Date, true, true, false, false, false, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
	}
	
	public Clone(): CharacteristicAssociation
	{
		let clone: CharacteristicAssociation = new CharacteristicAssociation();
		clone.Id = this.Id;
		clone.EntityId = this.EntityId;
		clone.Timestamp = this.Timestamp;
		clone.CharacteristicId = this.CharacteristicId;

		clone.CharacteristicIdParent = this.CharacteristicIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;
		
		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.CharacteristicIdParent == null ? '' : this.CharacteristicIdParent.ToString()} ${this.EntityId}`;
	}
	
	public ToJson(): any
	{
		let json: any = new Object();
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json["Id"] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.EntityId))
			json["EntityId"] = this.EntityId;
		if (!ObjectUtils.NullOrUndefined(this.CharacteristicId))
			json["CharacteristicId"] = this.CharacteristicId;
		if (!ObjectUtils.NullOrUndefined(this.Timestamp))
			json["Timestamp"] = DateUtils.ToDateServer(this.Timestamp);
		if (!ObjectUtils.NullOrUndefined(this.CharacteristicIdParent))
			json["CharacteristicIdParent"] = this.CharacteristicIdParent.ToJson();
		
		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.EntityId = json["EntityId"];
			this.CharacteristicId = json["CharacteristicId"];
			this.Timestamp = DateUtils.FromDateServer(json["Timestamp"]);
			
			this.CharacteristicIdParent = new Characteristic();
			this.CharacteristicIdParent.ParseFromJson(json["CharacteristicIdParent"]);
		}
	}
	
	public static FromJson(json: Object): CharacteristicAssociation
	{
		let item: CharacteristicAssociation = new CharacteristicAssociation();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}

/**
 * Implementations of the `CharacteristicAssociationFilter` class
 */
export class CharacteristicAssociationFilter extends Crudable
{
	constructor(
		public Id: number = null,
		public CharacteristicId: number[] = null,
		public EntityId: number = null,
		public Timestamp: Date = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`CharacteristicAssociationFilter`, `characteristic_associationFilter`, CrudableTableDefinitionGender.Female, `Características`, `Características`, true, true, true, true, true, true);

		this.SetColumnDefinition(CharacteristicAssociationColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CharacteristicAssociationColumns.CharacteristicId, `characteristic_id`, `CharacteristicId`, `Característica`, `Características`, ComponentTypeEnum.AutoComplete, true, true, false, true, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CharacteristicAssociationColumns.EntityId, `entity_id`, `EntityId`, `Entidade`, `Entidades`, ComponentTypeEnum.Number, true, true, false, true, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CharacteristicAssociationColumns.Timestamp, `timestamp`, `Timestamp`, `Registro`, `Registros`, ComponentTypeEnum.Date, true, true, false, false, false, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
	}
	
	public Clone(): CharacteristicAssociationFilter
	{
		let clone: CharacteristicAssociationFilter = new CharacteristicAssociationFilter();
		clone.Id = this.Id;
		clone.EntityId = this.EntityId;
		clone.Timestamp = this.Timestamp;
		clone.CharacteristicId = this.CharacteristicId;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;
		
		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}
	
	public ToJson(): any
	{
		let json: any = new Object();
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json["Id"] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.EntityId))
			json["EntityId"] = this.EntityId;
		if (!ObjectUtils.NullOrUndefined(this.Timestamp))
			json["Timestamp"] = DateUtils.ToDateServer(this.Timestamp);
		if (!ObjectUtils.NullOrUndefined(this.CharacteristicId))
			json["CharacteristicId"] = Array.isArray(this.CharacteristicId) ? this.CharacteristicId : [ this.CharacteristicId ];
		
		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.EntityId = json["EntityId"];
			this.CharacteristicId = json["CharacteristicId"];
			this.Timestamp = DateUtils.FromDateServer(json["Timestamp"]);
		}
	}
	
	public static FromJson(json: Object): CharacteristicAssociationFilter
	{
		let item: CharacteristicAssociationFilter = new CharacteristicAssociationFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}