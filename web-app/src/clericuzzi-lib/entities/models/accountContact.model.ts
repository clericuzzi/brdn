﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model'

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// Navigation Properties
import { Account } from 'clericuzzi-lib/entities/models/account.model';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const AccountContactClass: string = `AccountContact`;
export const AccountContactTable: string = `account_contact`;

/**
 * Column definitions for the `AccountContact` class
 */
export enum AccountContactColumns
{
	Id = "Id",
	Name = "Name",
	Email = "Email",
	Phone = "Phone",
	CelPhone = "CelPhone",
	AccountId = "AccountId",

	AccountIdParent = "AccountIdParent",
}
export const AccountContactColumnsFilter: string[] =  [  ];
export const AccountContactColumnsInsert: string[] =  [ AccountContactColumns.AccountId, AccountContactColumns.Name, AccountContactColumns.Email, AccountContactColumns.Phone, AccountContactColumns.CelPhone ];
export const AccountContactColumnsUpdate: string[] =  [ AccountContactColumns.AccountId, AccountContactColumns.Name, AccountContactColumns.Email, AccountContactColumns.Phone, AccountContactColumns.CelPhone ];

/**
 * Implementations of the `AccountContact` class
 */
export class AccountContact extends Crudable
{
	public AccountIdParent: Account;

	constructor(
		public Id: number = null,
		public AccountId: number = null,
		public Name: string = null,
		public Email: string = null,
		public Phone: string = null,
		public CelPhone: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`AccountContact`, `account_contact`, CrudableTableDefinitionGender.Male, `Contato`, `Contatos`, true, true, true, true, true, true);

		this.SetColumnDefinition(AccountContactColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountContactColumns.AccountId, `account_id`, `AccountId`, `Conta`, `Contas`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountContactColumns.Name, `name`, `Name`, `Nome`, `Nomes`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountContactColumns.Email, `email`, `Email`, `Email`, `Emails`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(AccountContactColumns.Phone, `phone`, `Phone`, `Telefone 01`, `Telefone 01`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(AccountContactColumns.CelPhone, `cel_phone`, `CelPhone`, `Telefone 02`, `Telefone 02`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
	}
	
	public Clone(): AccountContact
	{
		let clone: AccountContact = new AccountContact();
		clone.Id = this.Id;
		clone.Name = this.Name;
		clone.Email = this.Email;
		clone.Phone = this.Phone;
		clone.CelPhone = this.CelPhone;
		clone.AccountId = this.AccountId;

		clone.AccountIdParent = this.AccountIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;
		
		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.AccountIdParent == null ? '' : this.AccountIdParent.ToString()} ${this.Name}`;
	}
	
	public ToJson(): any
	{
		let json: any = new Object();
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json["Id"] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json["Name"] = this.Name;
		if (!ObjectUtils.NullOrUndefined(this.Email))
			json["Email"] = this.Email;
		if (!ObjectUtils.NullOrUndefined(this.Phone))
			json["Phone"] = this.Phone;
		if (!ObjectUtils.NullOrUndefined(this.CelPhone))
			json["CelPhone"] = this.CelPhone;
		if (!ObjectUtils.NullOrUndefined(this.AccountId))
			json["AccountId"] = this.AccountId;
		if (!ObjectUtils.NullOrUndefined(this.AccountIdParent))
			json["AccountIdParent"] = this.AccountIdParent.ToJson();
		
		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.Name = json["Name"];
			this.Email = json["Email"];
			this.Phone = json["Phone"];
			this.CelPhone = json["CelPhone"];
			this.AccountId = json["AccountId"];
			
			this.AccountIdParent = new Account();
			this.AccountIdParent.ParseFromJson(json["AccountIdParent"]);
		}
	}
	
	public static FromJson(json: Object): AccountContact
	{
		let item: AccountContact = new AccountContact();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}

/**
 * Implementations of the `AccountContactFilter` class
 */
export class AccountContactFilter extends Crudable
{
	constructor(
		public Id: number = null,
		public AccountId: number[] = null,
		public Name: string = null,
		public Email: string = null,
		public Phone: string = null,
		public CelPhone: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`AccountContactFilter`, `account_contactFilter`, CrudableTableDefinitionGender.Male, `Contato`, `Contatos`, true, true, true, true, true, true);

		this.SetColumnDefinition(AccountContactColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountContactColumns.AccountId, `account_id`, `AccountId`, `Conta`, `Contas`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountContactColumns.Name, `name`, `Name`, `Nome`, `Nomes`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountContactColumns.Email, `email`, `Email`, `Email`, `Emails`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(AccountContactColumns.Phone, `phone`, `Phone`, `Telefone 01`, `Telefone 01`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(AccountContactColumns.CelPhone, `cel_phone`, `CelPhone`, `Telefone 02`, `Telefone 02`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
	}
	
	public Clone(): AccountContactFilter
	{
		let clone: AccountContactFilter = new AccountContactFilter();
		clone.Id = this.Id;
		clone.Name = this.Name;
		clone.Email = this.Email;
		clone.Phone = this.Phone;
		clone.CelPhone = this.CelPhone;
		clone.AccountId = this.AccountId;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;
		
		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}
	
	public ToJson(): any
	{
		let json: any = new Object();
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json["Id"] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json["Name"] = this.Name;
		if (!ObjectUtils.NullOrUndefined(this.Email))
			json["Email"] = this.Email;
		if (!ObjectUtils.NullOrUndefined(this.Phone))
			json["Phone"] = this.Phone;
		if (!ObjectUtils.NullOrUndefined(this.CelPhone))
			json["CelPhone"] = this.CelPhone;
		if (!ObjectUtils.NullOrUndefined(this.AccountId))
			json["AccountId"] = Array.isArray(this.AccountId) ? this.AccountId : [ this.AccountId ];
		
		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.Name = json["Name"];
			this.Email = json["Email"];
			this.Phone = json["Phone"];
			this.CelPhone = json["CelPhone"];
			this.AccountId = json["AccountId"];
		}
	}
	
	public static FromJson(json: Object): AccountContactFilter
	{
		let item: AccountContactFilter = new AccountContactFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}