﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model'

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// Navigation Properties
import { Account } from 'clericuzzi-lib/entities/models/account.model';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const PersonClass: string = `Person`;
export const PersonTable: string = `person`;

/**
 * Column definitions for the `Person` class
 */
export enum PersonColumns
{
	Id = "Id",
	Sex = "Sex",
	Name = "Name",
	Birth = "Birth",
	Mother = "Mother",
	LastName = "LastName",
	Document = "Document",
	AccountId = "AccountId",
	FirstName = "FirstName",
	Timestamp = "Timestamp",

	AccountIdParent = "AccountIdParent",
}

/**
 * Implementations of the `Person` class
 */
export class Person extends Crudable
{
	public AccountIdParent: Account;

	constructor(
		public Id: number = null,
		public AccountId: number = null,
		public Sex: string = null,
		public Name: string = null,
		public FirstName: string = null,
		public LastName: string = null,
		public Mother: string = null,
		public Birth: Date = null,
		public Document: string = null,
		public Timestamp: Date = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`Person`, `person`, CrudableTableDefinitionGender.Male, `Pessoa`, `Pessoas`, true, true, true, true, true, true);

		this.SetColumnDefinition(PersonColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(PersonColumns.AccountId, `account_id`, `AccountId`, `Conta`, `Contas`, ComponentTypeEnum.AutoComplete, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(PersonColumns.Sex, `sex`, `Sex`, `Sexo`, `Sexos`, ComponentTypeEnum.ComboSex, true, true, true, false, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(PersonColumns.Name, `name`, `Name`, `Nome`, `Nomes`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(PersonColumns.FirstName, `first_name`, `FirstName`, `Primeiro nome`, `Primeiros nomes`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(PersonColumns.LastName, `last_name`, `LastName`, `Sobrenome`, `Sobrenomes`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(PersonColumns.Mother, `mother`, `Mother`, `Nome da mãe`, `Nome das mães`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(PersonColumns.Birth, `birth`, `Birth`, `Nascimento`, `Nascimentos`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, true);
		this.SetColumnDefinition(PersonColumns.Document, `document`, `Document`, `Documento`, `Documentos`, ComponentTypeEnum.Text, true, true, true, false, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(PersonColumns.Timestamp, `timestamp`, `Timestamp`, `Registro`, `Registros`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
	}

	public Clone(): Person
	{
		let clone: Person = new Person();
		clone.Id = this.Id;
		clone.Sex = this.Sex;
		clone.Name = this.Name;
		clone.Birth = this.Birth;
		clone.Mother = this.Mother;
		clone.LastName = this.LastName;
		clone.Document = this.Document;
		clone.AccountId = this.AccountId;
		clone.FirstName = this.FirstName;
		clone.Timestamp = this.Timestamp;

		clone.AccountIdParent = this.AccountIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.Name}`;
	}

	public ToJson(): any
	{
		let json: any = new Object();
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json["Id"] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Sex))
			json["Sex"] = this.Sex;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json["Name"] = this.Name;
		if (!ObjectUtils.NullOrUndefined(this.Mother))
			json["Mother"] = this.Mother;
		if (!ObjectUtils.NullOrUndefined(this.LastName))
			json["LastName"] = this.LastName;
		if (!ObjectUtils.NullOrUndefined(this.Document))
			json["Document"] = this.Document;
		if (!ObjectUtils.NullOrUndefined(this.AccountId))
			json["AccountId"] = this.AccountId;
		if (!ObjectUtils.NullOrUndefined(this.FirstName))
			json["FirstName"] = this.FirstName;
		if (!ObjectUtils.NullOrUndefined(this.Birth))
			json["Birth"] = DateUtils.ToDateServer(this.Birth);
		if (!ObjectUtils.NullOrUndefined(this.Timestamp))
			json["Timestamp"] = DateUtils.ToDateServer(this.Timestamp);

		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.Sex = json["Sex"];
			this.Name = json["Name"];
			this.Mother = json["Mother"];
			this.LastName = json["LastName"];
			this.Document = json["Document"];
			this.AccountId = json["AccountId"];
			this.FirstName = json["FirstName"];
			this.Birth = DateUtils.FromDateServer(json["Birth"]);
			this.Timestamp = DateUtils.FromDateServer(json["Timestamp"]);

			this.AccountIdParent = new Account();
			this.AccountIdParent.ParseFromJson(json["AccountIdParent"]);
		}
	}

	public static FromJson(json: Object): Person
	{
		let item: Person = new Person();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}

/**
 * Implementations of the `PersonFilter` class
 */
export class PersonFilter extends Crudable
{
	constructor(
		public Id: number = null,
		public AccountId: number[] = null,
		public Sex: string = null,
		public Name: string = null,
		public FirstName: string = null,
		public LastName: string = null,
		public Mother: string = null,
		public Birth: Date = null,
		public Document: string = null,
		public Timestamp: Date = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`PersonFilter`, `personFilter`, CrudableTableDefinitionGender.Male, `Person`, `Persons`, true, true, true, true, true, true);

		this.SetColumnDefinition(PersonColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(PersonColumns.AccountId, `account_id`, `AccountId`, `Conta`, `Contas`, ComponentTypeEnum.AutoComplete, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(PersonColumns.Sex, `sex`, `Sex`, `Sexo`, `Sexos`, ComponentTypeEnum.Text, true, true, true, false, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(PersonColumns.Name, `name`, `Name`, `Nome`, `Nomes`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(PersonColumns.FirstName, `first_name`, `FirstName`, `First Name`, `First Names`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(PersonColumns.LastName, `last_name`, `LastName`, `Last Name`, `Last Names`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(PersonColumns.Mother, `mother`, `Mother`, `Mother`, `Mothers`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(PersonColumns.Birth, `birth`, `Birth`, `Nascimento`, `Nascimentos`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, true);
		this.SetColumnDefinition(PersonColumns.Document, `document`, `Document`, `Documento`, `Documentos`, ComponentTypeEnum.Text, true, true, true, false, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(PersonColumns.Timestamp, `timestamp`, `Timestamp`, `Registrada Em`, `Registradas Em`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
	}

	public Clone(): PersonFilter
	{
		let clone: PersonFilter = new PersonFilter();
		clone.Id = this.Id;
		clone.Sex = this.Sex;
		clone.Name = this.Name;
		clone.Birth = this.Birth;
		clone.Mother = this.Mother;
		clone.LastName = this.LastName;
		clone.Document = this.Document;
		clone.AccountId = this.AccountId;
		clone.FirstName = this.FirstName;
		clone.Timestamp = this.Timestamp;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		let json: any = new Object();
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json["Id"] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Sex))
			json["Sex"] = this.Sex;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json["Name"] = this.Name;
		if (!ObjectUtils.NullOrUndefined(this.Mother))
			json["Mother"] = this.Mother;
		if (!ObjectUtils.NullOrUndefined(this.LastName))
			json["LastName"] = this.LastName;
		if (!ObjectUtils.NullOrUndefined(this.Document))
			json["Document"] = this.Document;
		if (!ObjectUtils.NullOrUndefined(this.AccountId))
			json["AccountId"] = this.AccountId;
		if (!ObjectUtils.NullOrUndefined(this.FirstName))
			json["FirstName"] = this.FirstName;
		if (!ObjectUtils.NullOrUndefined(this.Birth))
			json["Birth"] = DateUtils.ToDateServer(this.Birth);
		if (!ObjectUtils.NullOrUndefined(this.Timestamp))
			json["Timestamp"] = DateUtils.ToDateServer(this.Timestamp);

		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.Sex = json["Sex"];
			this.Name = json["Name"];
			this.Mother = json["Mother"];
			this.LastName = json["LastName"];
			this.Document = json["Document"];
			this.AccountId = json["AccountId"];
			this.FirstName = json["FirstName"];
			this.Birth = DateUtils.FromDateServer(json["Birth"]);
			this.Timestamp = DateUtils.FromDateServer(json["Timestamp"]);
		}
	}

	public static FromJson(json: Object): PersonFilter
	{
		let item: PersonFilter = new PersonFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}