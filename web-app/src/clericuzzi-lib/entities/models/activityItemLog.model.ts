﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model'

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// Navigation Properties
import { Activity } from './activity.model';
import { User } from 'clericuzzi-lib/entities/models/user.model';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const ActivityItemLogClass: string = `ActivityItemLog`;
export const ActivityItemLogTable: string = `activity_item_log`;

/**
 * Column definitions for the `ActivityItemLog` class
 */
export enum ActivityItemLogColumns
{
	Id = "Id",
	UserId = "UserId",
	Action = "Action",
	ActivityId = "ActivityId",

	UserIdParent = "UserIdParent",
	ActivityIdParent = "ActivityIdParent",
}
export const ActivityItemLogColumnsFilter: string[] =  [  ];
export const ActivityItemLogColumnsInsert: string[] =  [  ];
export const ActivityItemLogColumnsUpdate: string[] =  [  ];

/**
 * Implementations of the `ActivityItemLog` class
 */
export class ActivityItemLog extends Crudable
{
	public ActivityIdParent: Activity;
	public UserIdParent: User;

	constructor(
		public Id: number = null,
		public ActivityId: number = null,
		public UserId: number = null,
		public Action: number = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`ActivityItemLog`, `activity_item_log`, CrudableTableDefinitionGender.Male, `Log de Tarefa`, `Logs de Tarefa`, true, true, true, true, true, true);

		this.SetColumnDefinition(ActivityItemLogColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ActivityItemLogColumns.ActivityId, `activity_id`, `ActivityId`, `Atividade`, `Atividades`, ComponentTypeEnum.AutoComplete, true, true, false, true, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ActivityItemLogColumns.UserId, `user_id`, `UserId`, `Usuário`, `Usuários`, ComponentTypeEnum.AutoComplete, true, true, false, true, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ActivityItemLogColumns.Action, `action`, `Action`, `Ação`, `Ações`, ComponentTypeEnum.Number, true, true, false, true, false, true, null, null, null, null, null, `left`, false);
	}
	
	public Clone(): ActivityItemLog
	{
		let clone: ActivityItemLog = new ActivityItemLog();
		clone.Id = this.Id;
		clone.UserId = this.UserId;
		clone.Action = this.Action;
		clone.ActivityId = this.ActivityId;

		clone.UserIdParent = this.UserIdParent;
		clone.ActivityIdParent = this.ActivityIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;
		
		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.ActivityIdParent == null ? '' : this.ActivityIdParent.ToString()} ${this.UserIdParent == null ? '' : this.UserIdParent.ToString()} ${this.Action}`;
	}
	
	public ToJson(): any
	{
		let json: any = new Object();
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json["Id"] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.UserId))
			json["UserId"] = this.UserId;
		if (!ObjectUtils.NullOrUndefined(this.Action))
			json["Action"] = this.Action;
		if (!ObjectUtils.NullOrUndefined(this.ActivityId))
			json["ActivityId"] = this.ActivityId;
		if (!ObjectUtils.NullOrUndefined(this.UserIdParent))
			json["UserIdParent"] = this.UserIdParent.ToJson();
		if (!ObjectUtils.NullOrUndefined(this.ActivityIdParent))
			json["ActivityIdParent"] = this.ActivityIdParent.ToJson();
		
		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.UserId = json["UserId"];
			this.Action = json["Action"];
			this.ActivityId = json["ActivityId"];
			
			this.ActivityIdParent = new Activity();
			this.ActivityIdParent.ParseFromJson(json["ActivityIdParent"]);
			this.UserIdParent = new User();
			this.UserIdParent.ParseFromJson(json["UserIdParent"]);
		}
	}
	
	public static FromJson(json: Object): ActivityItemLog
	{
		let item: ActivityItemLog = new ActivityItemLog();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}

/**
 * Implementations of the `ActivityItemLogFilter` class
 */
export class ActivityItemLogFilter extends Crudable
{
	constructor(
		public Id: number = null,
		public ActivityId: number[] = null,
		public UserId: number[] = null,
		public Action: number = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`ActivityItemLogFilter`, `activity_item_logFilter`, CrudableTableDefinitionGender.Male, `Log de Tarefa`, `Logs de Tarefa`, true, true, true, true, true, true);

		this.SetColumnDefinition(ActivityItemLogColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ActivityItemLogColumns.ActivityId, `activity_id`, `ActivityId`, `Atividade`, `Atividades`, ComponentTypeEnum.AutoComplete, true, true, false, true, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ActivityItemLogColumns.UserId, `user_id`, `UserId`, `Usuário`, `Usuários`, ComponentTypeEnum.AutoComplete, true, true, false, true, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ActivityItemLogColumns.Action, `action`, `Action`, `Ação`, `Ações`, ComponentTypeEnum.Number, true, true, false, true, false, true, null, null, null, null, null, `left`, false);
	}
	
	public Clone(): ActivityItemLogFilter
	{
		let clone: ActivityItemLogFilter = new ActivityItemLogFilter();
		clone.Id = this.Id;
		clone.UserId = this.UserId;
		clone.Action = this.Action;
		clone.ActivityId = this.ActivityId;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;
		
		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}
	
	public ToJson(): any
	{
		let json: any = new Object();
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json["Id"] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Action))
			json["Action"] = this.Action;
		if (!ObjectUtils.NullOrUndefined(this.UserId))
			json["UserId"] = Array.isArray(this.UserId) ? this.UserId : [ this.UserId ];
		if (!ObjectUtils.NullOrUndefined(this.ActivityId))
			json["ActivityId"] = Array.isArray(this.ActivityId) ? this.ActivityId : [ this.ActivityId ];
		
		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.UserId = json["UserId"];
			this.Action = json["Action"];
			this.ActivityId = json["ActivityId"];
		}
	}
	
	public static FromJson(json: Object): ActivityItemLogFilter
	{
		let item: ActivityItemLogFilter = new ActivityItemLogFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}