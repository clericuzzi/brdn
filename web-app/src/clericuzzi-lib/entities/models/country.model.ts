﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model'

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const CountryClass: string = `GeographyCountry`;
export const CountryTable: string = `geography_country`;

/**
 * Column definitions for the `Country` class
 */
export enum CountryColumns
{
	Id = "Id",
	Name = "Name",
}

/**
 * Implementations of the `Country` class
 */
export class Country extends Crudable
{
	constructor(
		public Id: number = null,
		public Name: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`Country`, `geography_statecountry`, CrudableTableDefinitionGender.Male, `País`, `Países`, true, true, true, true, true, true);

		this.SetColumnDefinition(CountryColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CountryColumns.Name, `name`, `Name`, `País`, `Países`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): Country
	{
		let clone: Country = new Country();
		clone.Id = this.Id;
		clone.Name = this.Name;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.Name}`;
	}

	public ToJson(): any
	{
		let json: any = new Object();
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json["Id"] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json["Name"] = this.Name;

		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.Name = json["Name"];
		}
	}

	public static FromJson(json: Object): Country
	{
		let item: Country = new Country();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}

/**
 * Implementations of the `CountryFilter` class
 */
export class CountryFilter extends Crudable
{
	constructor(
		public Id: number = null,
		public Name: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`CountryFilter`, `countryFilter`, CrudableTableDefinitionGender.Male, `País`, `Países`, true, true, true, true, true, true);

		this.SetColumnDefinition(CountryColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CountryColumns.Name, `name`, `Name`, `País`, `Países`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): CountryFilter
	{
		let clone: CountryFilter = new CountryFilter();
		clone.Id = this.Id;
		clone.Name = this.Name;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		let json: any = new Object();
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json["Id"] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json["Name"] = this.Name;

		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.Name = json["Name"];
		}
	}

	public static FromJson(json: Object): CountryFilter
	{
		let item: CountryFilter = new CountryFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}