﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model'

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// Navigation Properties
import { Account } from 'clericuzzi-lib/entities/models/account.model';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const AccountSignatureFeeHistoryClass: string = `AccountSignatureFeeHistory`;
export const AccountSignatureFeeHistoryTable: string = `account_signature_fee_history`;

/**
 * Column definitions for the `AccountSignatureFeeHistory` class
 */
export enum AccountSignatureFeeHistoryColumns
{
	Id = "Id",
	Year = "Year",
	Month = "Month",
	AccountId = "AccountId",
	SignatureFee = "SignatureFee",

	AccountIdParent = "AccountIdParent",
}
export const AccountSignatureFeeHistoryColumnsFilter: string[] =  [  ];
export const AccountSignatureFeeHistoryColumnsInsert: string[] =  [  ];
export const AccountSignatureFeeHistoryColumnsUpdate: string[] =  [  ];

/**
 * Implementations of the `AccountSignatureFeeHistory` class
 */
export class AccountSignatureFeeHistory extends Crudable
{
	public AccountIdParent: Account;

	constructor(
		public Id: number = null,
		public AccountId: number = null,
		public Year: number = null,
		public Month: number = null,
		public SignatureFee: number = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`AccountSignatureFeeHistory`, `account_signature_fee_history`, CrudableTableDefinitionGender.Male, `Hostórico de Mensalidade`, `Hostórico de Mensalidades`, true, true, true, true, true, true);

		this.SetColumnDefinition(AccountSignatureFeeHistoryColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountSignatureFeeHistoryColumns.AccountId, `account_id`, `AccountId`, `Conta`, `Contas`, ComponentTypeEnum.AutoComplete, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountSignatureFeeHistoryColumns.Year, `year`, `Year`, `Ano`, `Anos`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountSignatureFeeHistoryColumns.Month, `month`, `Month`, `Mês`, `Meses`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountSignatureFeeHistoryColumns.SignatureFee, `signature_fee`, `SignatureFee`, `Assinatura`, `Assinaturas`, ComponentTypeEnum.Decimal, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
	}
	
	public Clone(): AccountSignatureFeeHistory
	{
		let clone: AccountSignatureFeeHistory = new AccountSignatureFeeHistory();
		clone.Id = this.Id;
		clone.Year = this.Year;
		clone.Month = this.Month;
		clone.AccountId = this.AccountId;
		clone.SignatureFee = this.SignatureFee;

		clone.AccountIdParent = this.AccountIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;
		
		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.AccountIdParent == null ? '' : this.AccountIdParent.ToString()} ${this.Year} ${this.Month} ${this.SignatureFee}`;
	}
	
	public ToJson(): any
	{
		let json: any = new Object();
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json["Id"] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Year))
			json["Year"] = this.Year;
		if (!ObjectUtils.NullOrUndefined(this.Month))
			json["Month"] = this.Month;
		if (!ObjectUtils.NullOrUndefined(this.AccountId))
			json["AccountId"] = this.AccountId;
		if (!ObjectUtils.NullOrUndefined(this.SignatureFee))
			json["SignatureFee"] = this.SignatureFee;
		if (!ObjectUtils.NullOrUndefined(this.AccountIdParent))
			json["AccountIdParent"] = this.AccountIdParent.ToJson();
		
		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.Year = json["Year"];
			this.Month = json["Month"];
			this.AccountId = json["AccountId"];
			this.SignatureFee = json["SignatureFee"];
			
			this.AccountIdParent = new Account();
			this.AccountIdParent.ParseFromJson(json["AccountIdParent"]);
		}
	}
	
	public static FromJson(json: Object): AccountSignatureFeeHistory
	{
		let item: AccountSignatureFeeHistory = new AccountSignatureFeeHistory();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}

/**
 * Implementations of the `AccountSignatureFeeHistoryFilter` class
 */
export class AccountSignatureFeeHistoryFilter extends Crudable
{
	constructor(
		public Id: number = null,
		public AccountId: number[] = null,
		public Year: number = null,
		public Month: number = null,
		public SignatureFee: number = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`AccountSignatureFeeHistoryFilter`, `account_signature_fee_historyFilter`, CrudableTableDefinitionGender.Male, `Hostórico de Mensalidade`, `Hostórico de Mensalidades`, true, true, true, true, true, true);

		this.SetColumnDefinition(AccountSignatureFeeHistoryColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountSignatureFeeHistoryColumns.AccountId, `account_id`, `AccountId`, `Conta`, `Contas`, ComponentTypeEnum.AutoComplete, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountSignatureFeeHistoryColumns.Year, `year`, `Year`, `Ano`, `Anos`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountSignatureFeeHistoryColumns.Month, `month`, `Month`, `Mês`, `Meses`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountSignatureFeeHistoryColumns.SignatureFee, `signature_fee`, `SignatureFee`, `Assinatura`, `Assinaturas`, ComponentTypeEnum.Decimal, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
	}
	
	public Clone(): AccountSignatureFeeHistoryFilter
	{
		let clone: AccountSignatureFeeHistoryFilter = new AccountSignatureFeeHistoryFilter();
		clone.Id = this.Id;
		clone.Year = this.Year;
		clone.Month = this.Month;
		clone.AccountId = this.AccountId;
		clone.SignatureFee = this.SignatureFee;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;
		
		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}
	
	public ToJson(): any
	{
		let json: any = new Object();
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json["Id"] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Year))
			json["Year"] = this.Year;
		if (!ObjectUtils.NullOrUndefined(this.Month))
			json["Month"] = this.Month;
		if (!ObjectUtils.NullOrUndefined(this.SignatureFee))
			json["SignatureFee"] = this.SignatureFee;
		if (!ObjectUtils.NullOrUndefined(this.AccountId))
			json["AccountId"] = Array.isArray(this.AccountId) ? this.AccountId : [ this.AccountId ];
		
		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.Year = json["Year"];
			this.Month = json["Month"];
			this.AccountId = json["AccountId"];
			this.SignatureFee = json["SignatureFee"];
		}
	}
	
	public static FromJson(json: Object): AccountSignatureFeeHistoryFilter
	{
		let item: AccountSignatureFeeHistoryFilter = new AccountSignatureFeeHistoryFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}