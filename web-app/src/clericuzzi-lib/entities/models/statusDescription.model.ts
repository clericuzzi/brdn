﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model'

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const StatusDescriptionClass: string = `StatusDescription`;
export const StatusDescriptionTable: string = `status_description`;

/**
 * Column definitions for the `StatusDescription` class
 */
export enum StatusDescriptionColumns
{
	Id = "Id",
	Description = "Description",
}

/**
 * Implementations of the `StatusDescription` class
 */
export class StatusDescription extends Crudable
{
	constructor(
		public Id: number = null,
		public Description: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`StatusDescription`, `status_description`, CrudableTableDefinitionGender.Male, `Status Description`, `Status Descriptions`, true, true, true, true, true, true);

		this.SetColumnDefinition(StatusDescriptionColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(StatusDescriptionColumns.Description, `description`, `Description`, `Description`, `Descriptions`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): StatusDescription
	{
		let clone: StatusDescription = new StatusDescription();
		clone.Id = this.Id;
		clone.Description = this.Description;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.Description}`;
	}

	public ToJson(): any
	{
		let json: any = new Object();
		json["Id"] = ObjectUtils.NullOrUndefined(this.Id) ? 0 : this.Id.toString();
		json["Description"] = ObjectUtils.NullOrUndefined(this.Description) ? null : this.Description.toString();

		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.Description = json["Description"];
		}
	}

	public static FromJson(json: Object): StatusDescription
	{
		let item: StatusDescription = new StatusDescription();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}