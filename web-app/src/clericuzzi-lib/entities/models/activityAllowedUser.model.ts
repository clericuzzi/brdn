﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model'

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// Navigation Properties
import { ActivityItem } from './activityItem.model';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const ActivityAllowedUserClass: string = `ActivityAllowedUser`;
export const ActivityAllowedUserTable: string = `activity_allowed_user`;

/**
 * Column definitions for the `ActivityAllowedUser` class
 */
export enum ActivityAllowedUserColumns
{
	Id = "Id",
	UserId = "UserId",
	Timestamp = "Timestamp",
	ActivityId = "ActivityId",

	ActivityIdParent = "ActivityIdParent",
}
export const ActivityAllowedUserColumnsFilter: string[] =  [  ];
export const ActivityAllowedUserColumnsInsert: string[] =  [ ActivityAllowedUserColumns.ActivityId, ActivityAllowedUserColumns.UserId ];
export const ActivityAllowedUserColumnsUpdate: string[] =  [ ActivityAllowedUserColumns.ActivityId, ActivityAllowedUserColumns.UserId ];

/**
 * Implementations of the `ActivityAllowedUser` class
 */
export class ActivityAllowedUser extends Crudable
{
	public ActivityIdParent: ActivityItem;

	constructor(
		public Id: number = null,
		public ActivityId: number = null,
		public UserId: number = null,
		public Timestamp: Date = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`ActivityAllowedUser`, `activity_allowed_user`, CrudableTableDefinitionGender.Male, `Usuários por Atividade`, `Usuários por Atividade`, true, true, true, true, true, true);

		this.SetColumnDefinition(ActivityAllowedUserColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ActivityAllowedUserColumns.ActivityId, `activity_id`, `ActivityId`, `Atividade`, `Atividades`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ActivityAllowedUserColumns.UserId, `user_id`, `UserId`, `Usuário`, `Usuários`, ComponentTypeEnum.Number, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ActivityAllowedUserColumns.Timestamp, `timestamp`, `Timestamp`, `Registro`, `registros`, ComponentTypeEnum.Date, true, true, false, false, false, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
	}
	
	public Clone(): ActivityAllowedUser
	{
		let clone: ActivityAllowedUser = new ActivityAllowedUser();
		clone.Id = this.Id;
		clone.UserId = this.UserId;
		clone.Timestamp = this.Timestamp;
		clone.ActivityId = this.ActivityId;

		clone.ActivityIdParent = this.ActivityIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;
		
		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.ActivityIdParent == null ? '' : this.ActivityIdParent.ToString()} ${this.UserId}`;
	}
	
	public ToJson(): any
	{
		let json: any = new Object();
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json["Id"] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.UserId))
			json["UserId"] = this.UserId;
		if (!ObjectUtils.NullOrUndefined(this.ActivityId))
			json["ActivityId"] = this.ActivityId;
		if (!ObjectUtils.NullOrUndefined(this.Timestamp))
			json["Timestamp"] = DateUtils.ToDateServer(this.Timestamp);
		if (!ObjectUtils.NullOrUndefined(this.ActivityIdParent))
			json["ActivityIdParent"] = this.ActivityIdParent.ToJson();
		
		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.UserId = json["UserId"];
			this.ActivityId = json["ActivityId"];
			this.Timestamp = DateUtils.FromDateServer(json["Timestamp"]);
			
			this.ActivityIdParent = new ActivityItem();
			this.ActivityIdParent.ParseFromJson(json["ActivityIdParent"]);
		}
	}
	
	public static FromJson(json: Object): ActivityAllowedUser
	{
		let item: ActivityAllowedUser = new ActivityAllowedUser();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}

/**
 * Implementations of the `ActivityAllowedUserFilter` class
 */
export class ActivityAllowedUserFilter extends Crudable
{
	constructor(
		public Id: number = null,
		public ActivityId: number[] = null,
		public UserId: number = null,
		public Timestamp: Date = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`ActivityAllowedUserFilter`, `activity_allowed_userFilter`, CrudableTableDefinitionGender.Male, `Usuários por Atividade`, `Usuários por Atividade`, true, true, true, true, true, true);

		this.SetColumnDefinition(ActivityAllowedUserColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ActivityAllowedUserColumns.ActivityId, `activity_id`, `ActivityId`, `Atividade`, `Atividades`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ActivityAllowedUserColumns.UserId, `user_id`, `UserId`, `Usuário`, `Usuários`, ComponentTypeEnum.Number, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ActivityAllowedUserColumns.Timestamp, `timestamp`, `Timestamp`, `Registro`, `registros`, ComponentTypeEnum.Date, true, true, false, false, false, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
	}
	
	public Clone(): ActivityAllowedUserFilter
	{
		let clone: ActivityAllowedUserFilter = new ActivityAllowedUserFilter();
		clone.Id = this.Id;
		clone.UserId = this.UserId;
		clone.Timestamp = this.Timestamp;
		clone.ActivityId = this.ActivityId;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;
		
		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}
	
	public ToJson(): any
	{
		let json: any = new Object();
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json["Id"] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.UserId))
			json["UserId"] = this.UserId;
		if (!ObjectUtils.NullOrUndefined(this.Timestamp))
			json["Timestamp"] = DateUtils.ToDateServer(this.Timestamp);
		if (!ObjectUtils.NullOrUndefined(this.ActivityId))
			json["ActivityId"] = Array.isArray(this.ActivityId) ? this.ActivityId : [ this.ActivityId ];
		
		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.UserId = json["UserId"];
			this.ActivityId = json["ActivityId"];
			this.Timestamp = DateUtils.FromDateServer(json["Timestamp"]);
		}
	}
	
	public static FromJson(json: Object): ActivityAllowedUserFilter
	{
		let item: ActivityAllowedUserFilter = new ActivityAllowedUserFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}