﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model'

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// Navigation Properties
import { Account } from 'clericuzzi-lib/entities/models/account.model';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const CharacteristicClass: string = `Characteristic`;
export const CharacteristicTable: string = `characteristic`;

/**
 * Column definitions for the `Characteristic` class
 */
export enum CharacteristicColumns
{
	Id = "Id",
	Name = "Name",
	AccountId = "AccountId",

	AccountIdParent = "AccountIdParent",
}
export const CharacteristicColumnsFilter: string[] =  [ CharacteristicColumns.AccountId, CharacteristicColumns.Name ];
export const CharacteristicColumnsInsert: string[] =  [ CharacteristicColumns.AccountId, CharacteristicColumns.Name ];
export const CharacteristicColumnsUpdate: string[] =  [ CharacteristicColumns.AccountId, CharacteristicColumns.Name ];

/**
 * Implementations of the `Characteristic` class
 */
export class Characteristic extends Crudable
{
	public AccountIdParent: Account;

	constructor(
		public Id: number = null,
		public AccountId: number = null,
		public Name: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`Characteristic`, `characteristic`, CrudableTableDefinitionGender.Female, `Característica`, `Características`, true, true, true, true, true, true);

		this.SetColumnDefinition(CharacteristicColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CharacteristicColumns.AccountId, `account_id`, `AccountId`, `Conta`, `Contas`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CharacteristicColumns.Name, `name`, `Name`, `Característica`, `Características`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): Characteristic
	{
		let clone: Characteristic = new Characteristic();
		clone.Id = this.Id;
		clone.Name = this.Name;
		clone.AccountId = this.AccountId;

		clone.AccountIdParent = this.AccountIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.AccountIdParent == null ? '' : this.AccountIdParent.ToString()} ${this.Name}`;
	}

	public ToJson(): any
	{
		let json: any = new Object();
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json["Id"] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json["Name"] = this.Name;
		if (!ObjectUtils.NullOrUndefined(this.AccountId))
			json["AccountId"] = this.AccountId;
		if (!ObjectUtils.NullOrUndefined(this.AccountIdParent))
			json["AccountIdParent"] = this.AccountIdParent.ToJson();

		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.Name = json["Name"];
			this.AccountId = json["AccountId"];

			this.AccountIdParent = new Account();
			this.AccountIdParent.ParseFromJson(json["AccountIdParent"]);
		}
	}

	public static FromJson(json: Object): Characteristic
	{
		let item: Characteristic = new Characteristic();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}

/**
 * Implementations of the `CharacteristicFilter` class
 */
export class CharacteristicFilter extends Crudable
{
	constructor(
		public Id: number = null,
		public AccountId: number[] = null,
		public Name: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`CharacteristicFilter`, `characteristicFilter`, CrudableTableDefinitionGender.Female, `Característica`, `Características`, true, true, true, true, true, true);

		this.SetColumnDefinition(CharacteristicColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CharacteristicColumns.AccountId, `account_id`, `AccountId`, `Conta`, `Contas`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CharacteristicColumns.Name, `name`, `Name`, `Característica`, `Características`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): CharacteristicFilter
	{
		let clone: CharacteristicFilter = new CharacteristicFilter();
		clone.Id = this.Id;
		clone.Name = this.Name;
		clone.AccountId = this.AccountId;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		let json: any = new Object();
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json["Id"] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json["Name"] = this.Name;
		if (!ObjectUtils.NullOrUndefined(this.AccountId))
			json["AccountId"] = Array.isArray(this.AccountId) ? this.AccountId : [ this.AccountId ];

		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.Name = json["Name"];
			this.AccountId = json["AccountId"];
		}
	}

	public static FromJson(json: Object): CharacteristicFilter
	{
		let item: CharacteristicFilter = new CharacteristicFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}