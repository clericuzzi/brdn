﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model'

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const AccountClass: string = `Account`;
export const AccountTable: string = `account`;

/**
 * Column definitions for the `Account` class
 */
export enum AccountColumns
{
	Id = "Id",
	Cnpj = "Cnpj",
	Name = "Name",
	Site = "Site",
	Email = "Email",
	Phone = "Phone",
	Blocked = "Blocked",
	Latitude = "Latitude",
	Longitude = "Longitude",
	Timestamp = "Timestamp",
	SignatureFee = "SignatureFee",
}
export const AccountColumnsFilter: string[] =  [ AccountColumns.Cnpj, AccountColumns.Name, AccountColumns.Blocked ];
export const AccountColumnsInsert: string[] =  [ AccountColumns.Cnpj, AccountColumns.Name, AccountColumns.Site, AccountColumns.Email, AccountColumns.Phone, AccountColumns.SignatureFee ];
export const AccountColumnsUpdate: string[] =  [ AccountColumns.Cnpj, AccountColumns.Name, AccountColumns.Site, AccountColumns.Email, AccountColumns.Phone, AccountColumns.SignatureFee ];

/**
 * Implementations of the `Account` class
 */
export class Account extends Crudable
{
	constructor(
		public Id: number = null,
		public Cnpj: string = null,
		public Name: string = null,
		public Site: string = null,
		public Email: string = null,
		public Phone: string = null,
		public Blocked: number = null,
		public Latitude: number = null,
		public Longitude: number = null,
		public SignatureFee: number = null,
		public Timestamp: Date = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`Account`, `account`, CrudableTableDefinitionGender.Female, `Conta`, `Contas`, true, true, true, true, true, true);

		this.SetColumnDefinition(AccountColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountColumns.Cnpj, `cnpj`, `Cnpj`, `Cnpj`, `Cnpjs`, ComponentTypeEnum.Text, true, true, true, false, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountColumns.Name, `name`, `Name`, `Nome`, `Nomes`, ComponentTypeEnum.Text, true, true, true, false, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(AccountColumns.Site, `site`, `Site`, `Site`, `Sites`, ComponentTypeEnum.Text, true, true, true, false, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(AccountColumns.Email, `email`, `Email`, `Email`, `Emails`, ComponentTypeEnum.Text, true, true, true, false, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(AccountColumns.Phone, `phone`, `Phone`, `Telefone`, `Telefones`, ComponentTypeEnum.Text, true, true, true, false, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(AccountColumns.Blocked, `blocked`, `Blocked`, `Bloqueada`, `Bloqueadas`, ComponentTypeEnum.ComboYesNo, true, true, false, false, false, true, null, StringUtils.TransformYesNo, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountColumns.Latitude, `latitude`, `Latitude`, `Latitude`, `Latitudes`, ComponentTypeEnum.Text, true, true, false, false, false, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(AccountColumns.Longitude, `longitude`, `Longitude`, `Longitude`, `Longitudes`, ComponentTypeEnum.Text, true, true, false, false, false, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(AccountColumns.SignatureFee, `signature_fee`, `SignatureFee`, `Mensalidade`, `Mensalidades`, ComponentTypeEnum.Text, true, true, true, false, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(AccountColumns.Timestamp, `timestamp`, `Timestamp`, `Registro`, `Registros`, ComponentTypeEnum.Date, true, true, false, false, false, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
	}
	
	public Clone(): Account
	{
		let clone: Account = new Account();
		clone.Id = this.Id;
		clone.Cnpj = this.Cnpj;
		clone.Name = this.Name;
		clone.Site = this.Site;
		clone.Email = this.Email;
		clone.Phone = this.Phone;
		clone.Blocked = this.Blocked;
		clone.Latitude = this.Latitude;
		clone.Longitude = this.Longitude;
		clone.Timestamp = this.Timestamp;
		clone.SignatureFee = this.SignatureFee;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;
		
		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.Cnpj} ${this.Name}`;
	}
	
	public ToJson(): any
	{
		let json: any = new Object();
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json["Id"] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Cnpj))
			json["Cnpj"] = this.Cnpj;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json["Name"] = this.Name;
		if (!ObjectUtils.NullOrUndefined(this.Site))
			json["Site"] = this.Site;
		if (!ObjectUtils.NullOrUndefined(this.Email))
			json["Email"] = this.Email;
		if (!ObjectUtils.NullOrUndefined(this.Phone))
			json["Phone"] = this.Phone;
		if (!ObjectUtils.NullOrUndefined(this.Blocked))
			json["Blocked"] = this.Blocked;
		if (!ObjectUtils.NullOrUndefined(this.Latitude))
			json["Latitude"] = this.Latitude;
		if (!ObjectUtils.NullOrUndefined(this.Longitude))
			json["Longitude"] = this.Longitude;
		if (!ObjectUtils.NullOrUndefined(this.SignatureFee))
			json["SignatureFee"] = this.SignatureFee;
		if (!ObjectUtils.NullOrUndefined(this.Timestamp))
			json["Timestamp"] = DateUtils.ToDateServer(this.Timestamp);
		
		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.Cnpj = json["Cnpj"];
			this.Name = json["Name"];
			this.Site = json["Site"];
			this.Email = json["Email"];
			this.Phone = json["Phone"];
			this.Blocked = json["Blocked"];
			this.Latitude = json["Latitude"];
			this.Longitude = json["Longitude"];
			this.SignatureFee = json["SignatureFee"];
			this.Timestamp = DateUtils.FromDateServer(json["Timestamp"]);
		}
	}
	
	public static FromJson(json: Object): Account
	{
		let item: Account = new Account();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}

/**
 * Implementations of the `AccountFilter` class
 */
export class AccountFilter extends Crudable
{
	constructor(
		public Id: number = null,
		public Cnpj: string = null,
		public Name: string = null,
		public Site: string = null,
		public Email: string = null,
		public Phone: string = null,
		public Blocked: number = null,
		public Latitude: number = null,
		public Longitude: number = null,
		public SignatureFee: number = null,
		public Timestamp: Date = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`AccountFilter`, `accountFilter`, CrudableTableDefinitionGender.Female, `Conta`, `Contas`, true, true, true, true, true, true);

		this.SetColumnDefinition(AccountColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountColumns.Cnpj, `cnpj`, `Cnpj`, `Cnpj`, `Cnpjs`, ComponentTypeEnum.Text, true, true, true, false, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountColumns.Name, `name`, `Name`, `Nome`, `Nomes`, ComponentTypeEnum.Text, true, true, true, false, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(AccountColumns.Site, `site`, `Site`, `Site`, `Sites`, ComponentTypeEnum.Text, true, true, true, false, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(AccountColumns.Email, `email`, `Email`, `Email`, `Emails`, ComponentTypeEnum.Text, true, true, true, false, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(AccountColumns.Phone, `phone`, `Phone`, `Telefone`, `Telefones`, ComponentTypeEnum.Text, true, true, true, false, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(AccountColumns.Blocked, `blocked`, `Blocked`, `Bloqueada`, `Bloqueadas`, ComponentTypeEnum.ComboYesNo, true, true, false, false, false, true, null, StringUtils.TransformYesNo, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountColumns.Latitude, `latitude`, `Latitude`, `Latitude`, `Latitudes`, ComponentTypeEnum.Text, true, true, false, false, false, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(AccountColumns.Longitude, `longitude`, `Longitude`, `Longitude`, `Longitudes`, ComponentTypeEnum.Text, true, true, false, false, false, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(AccountColumns.SignatureFee, `signature_fee`, `SignatureFee`, `Mensalidade`, `Mensalidades`, ComponentTypeEnum.Text, true, true, true, false, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(AccountColumns.Timestamp, `timestamp`, `Timestamp`, `Registro`, `Registros`, ComponentTypeEnum.Date, true, true, false, false, false, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
	}
	
	public Clone(): AccountFilter
	{
		let clone: AccountFilter = new AccountFilter();
		clone.Id = this.Id;
		clone.Cnpj = this.Cnpj;
		clone.Name = this.Name;
		clone.Site = this.Site;
		clone.Email = this.Email;
		clone.Phone = this.Phone;
		clone.Blocked = this.Blocked;
		clone.Latitude = this.Latitude;
		clone.Longitude = this.Longitude;
		clone.Timestamp = this.Timestamp;
		clone.SignatureFee = this.SignatureFee;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;
		
		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}
	
	public ToJson(): any
	{
		let json: any = new Object();
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json["Id"] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Cnpj))
			json["Cnpj"] = this.Cnpj;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json["Name"] = this.Name;
		if (!ObjectUtils.NullOrUndefined(this.Site))
			json["Site"] = this.Site;
		if (!ObjectUtils.NullOrUndefined(this.Email))
			json["Email"] = this.Email;
		if (!ObjectUtils.NullOrUndefined(this.Phone))
			json["Phone"] = this.Phone;
		if (!ObjectUtils.NullOrUndefined(this.Blocked))
			json["Blocked"] = this.Blocked;
		if (!ObjectUtils.NullOrUndefined(this.Latitude))
			json["Latitude"] = this.Latitude;
		if (!ObjectUtils.NullOrUndefined(this.Longitude))
			json["Longitude"] = this.Longitude;
		if (!ObjectUtils.NullOrUndefined(this.SignatureFee))
			json["SignatureFee"] = this.SignatureFee;
		if (!ObjectUtils.NullOrUndefined(this.Timestamp))
			json["Timestamp"] = DateUtils.ToDateServer(this.Timestamp);
		
		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.Cnpj = json["Cnpj"];
			this.Name = json["Name"];
			this.Site = json["Site"];
			this.Email = json["Email"];
			this.Phone = json["Phone"];
			this.Blocked = json["Blocked"];
			this.Latitude = json["Latitude"];
			this.Longitude = json["Longitude"];
			this.SignatureFee = json["SignatureFee"];
			this.Timestamp = DateUtils.FromDateServer(json["Timestamp"]);
		}
	}
	
	public static FromJson(json: Object): AccountFilter
	{
		let item: AccountFilter = new AccountFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}