﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model'

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// Navigation Properties
import { User } from 'clericuzzi-lib/entities/models/user.model';
import { Permission } from 'clericuzzi-lib/entities/models/permission.model';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const UserPermissionClass: string = `UserPermission`;
export const UserPermissionTable: string = `user_permission`;

/**
 * Column definitions for the `UserPermission` class
 */
export enum UserPermissionColumns
{
	Id = "Id",
	UserId = "UserId",
	PermissionId = "PermissionId",

	UserIdParent = "UserIdParent",
	PermissionIdParent = "PermissionIdParent",
}
export const UserPermissionColumnsFilter: string[] =  [  ];
export const UserPermissionColumnsInsert: string[] =  [ UserPermissionColumns.UserId, UserPermissionColumns.PermissionId ];
export const UserPermissionColumnsUpdate: string[] =  [ UserPermissionColumns.UserId, UserPermissionColumns.PermissionId ];

/**
 * Implementations of the `UserPermission` class
 */
export class UserPermission extends Crudable
{
	public UserIdParent: User;
	public PermissionIdParent: Permission;

	constructor(
		public Id: number = null,
		public UserId: number = null,
		public PermissionId: number = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`UserPermission`, `user_permission`, CrudableTableDefinitionGender.Female, `Permissão`, `Permissões`, true, true, true, true, true, true);

		this.SetColumnDefinition(UserPermissionColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(UserPermissionColumns.UserId, `user_id`, `UserId`, `Usuário`, `Usuários`, ComponentTypeEnum.AutoComplete, true, true, true, false, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(UserPermissionColumns.PermissionId, `permission_id`, `PermissionId`, `Permissão`, `Permissões`, ComponentTypeEnum.AutoComplete, true, true, true, false, true, true, null, null, null, null, null, `left`, false);
	}
	
	public Clone(): UserPermission
	{
		let clone: UserPermission = new UserPermission();
		clone.Id = this.Id;
		clone.UserId = this.UserId;
		clone.PermissionId = this.PermissionId;

		clone.UserIdParent = this.UserIdParent;
		clone.PermissionIdParent = this.PermissionIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;
		
		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.UserIdParent == null ? '' : this.UserIdParent.ToString()} ${this.PermissionIdParent == null ? '' : this.PermissionIdParent.ToString()}`;
	}
	
	public ToJson(): any
	{
		let json: any = new Object();
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json["Id"] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.UserId))
			json["UserId"] = this.UserId;
		if (!ObjectUtils.NullOrUndefined(this.PermissionId))
			json["PermissionId"] = this.PermissionId;
		if (!ObjectUtils.NullOrUndefined(this.UserIdParent))
			json["UserIdParent"] = this.UserIdParent.ToJson();
		if (!ObjectUtils.NullOrUndefined(this.PermissionIdParent))
			json["PermissionIdParent"] = this.PermissionIdParent.ToJson();
		
		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.UserId = json["UserId"];
			this.PermissionId = json["PermissionId"];
			
			this.UserIdParent = new User();
			this.UserIdParent.ParseFromJson(json["UserIdParent"]);
			this.PermissionIdParent = new Permission();
			this.PermissionIdParent.ParseFromJson(json["PermissionIdParent"]);
		}
	}
	
	public static FromJson(json: Object): UserPermission
	{
		let item: UserPermission = new UserPermission();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}

/**
 * Implementations of the `UserPermissionFilter` class
 */
export class UserPermissionFilter extends Crudable
{
	constructor(
		public Id: number = null,
		public UserId: number[] = null,
		public PermissionId: number[] = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`UserPermissionFilter`, `user_permissionFilter`, CrudableTableDefinitionGender.Female, `Permissão`, `Permissões`, true, true, true, true, true, true);

		this.SetColumnDefinition(UserPermissionColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(UserPermissionColumns.UserId, `user_id`, `UserId`, `Usuário`, `Usuários`, ComponentTypeEnum.AutoComplete, true, true, true, false, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(UserPermissionColumns.PermissionId, `permission_id`, `PermissionId`, `Permissão`, `Permissões`, ComponentTypeEnum.AutoComplete, true, true, true, false, true, true, null, null, null, null, null, `left`, false);
	}
	
	public Clone(): UserPermissionFilter
	{
		let clone: UserPermissionFilter = new UserPermissionFilter();
		clone.Id = this.Id;
		clone.UserId = this.UserId;
		clone.PermissionId = this.PermissionId;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;
		
		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}
	
	public ToJson(): any
	{
		let json: any = new Object();
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json["Id"] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.UserId))
			json["UserId"] = Array.isArray(this.UserId) ? this.UserId : [ this.UserId ];
		if (!ObjectUtils.NullOrUndefined(this.PermissionId))
			json["PermissionId"] = Array.isArray(this.PermissionId) ? this.PermissionId : [ this.PermissionId ];
		
		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.UserId = json["UserId"];
			this.PermissionId = json["PermissionId"];
		}
	}
	
	public static FromJson(json: Object): UserPermissionFilter
	{
		let item: UserPermissionFilter = new UserPermissionFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}