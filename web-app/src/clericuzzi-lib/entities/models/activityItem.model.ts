﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model'

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// Navigation Properties
import { Activity } from './activity.model';
import { User } from 'clericuzzi-lib/entities/models/user.model';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const ActivityItemClass: string = `ActivityItem`;
export const ActivityItemTable: string = `activity_item`;

/**
 * Column definitions for the `ActivityItem` class
 */
export enum ActivityItemColumns
{
	Id = "Id",
	UserId = "UserId",
	Result = "Result",
	EntityId = "EntityId",
	Timestamp = "Timestamp",
	ActivityId = "ActivityId",
	FinishedIn = "FinishedIn",

	UserIdParent = "UserIdParent",
	ActivityIdParent = "ActivityIdParent",
}
export const ActivityItemColumnsFilter: string[] =  [  ];
export const ActivityItemColumnsInsert: string[] =  [ ActivityItemColumns.ActivityId, ActivityItemColumns.UserId, ActivityItemColumns.EntityId ];
export const ActivityItemColumnsUpdate: string[] =  [ ActivityItemColumns.ActivityId, ActivityItemColumns.UserId, ActivityItemColumns.EntityId ];

/**
 * Implementations of the `ActivityItem` class
 */
export class ActivityItem extends Crudable
{
	public ActivityIdParent: Activity;
	public UserIdParent: User;

	constructor(
		public Id: number = null,
		public ActivityId: number = null,
		public UserId: number = null,
		public EntityId: number = null,
		public FinishedIn: Date = null,
		public Result: number = null,
		public Timestamp: Date = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`ActivityItem`, `activity_item`, CrudableTableDefinitionGender.Female, `Tarefa`, `Tarefa`, true, true, true, true, true, true);

		this.SetColumnDefinition(ActivityItemColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ActivityItemColumns.ActivityId, `activity_id`, `ActivityId`, `Atividade`, `Atividade`, ComponentTypeEnum.AutoComplete, true, true, true, false, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ActivityItemColumns.UserId, `user_id`, `UserId`, `Usuário`, `Usuários`, ComponentTypeEnum.AutoComplete, true, true, true, false, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ActivityItemColumns.EntityId, `entity_id`, `EntityId`, `Entidade`, `Entidades`, ComponentTypeEnum.Number, true, true, true, false, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ActivityItemColumns.FinishedIn, `finished_in`, `FinishedIn`, `Finalizada em`, `Finalizadas em`, ComponentTypeEnum.Date, true, true, false, false, false, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, true);
		this.SetColumnDefinition(ActivityItemColumns.Result, `result`, `Result`, `Resultado`, `Resultado`, ComponentTypeEnum.Text, true, true, false, false, false, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(ActivityItemColumns.Timestamp, `timestamp`, `Timestamp`, `Registro`, `Registros`, ComponentTypeEnum.Date, true, true, false, false, false, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
	}
	
	public Clone(): ActivityItem
	{
		let clone: ActivityItem = new ActivityItem();
		clone.Id = this.Id;
		clone.UserId = this.UserId;
		clone.Result = this.Result;
		clone.EntityId = this.EntityId;
		clone.Timestamp = this.Timestamp;
		clone.ActivityId = this.ActivityId;
		clone.FinishedIn = this.FinishedIn;

		clone.UserIdParent = this.UserIdParent;
		clone.ActivityIdParent = this.ActivityIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;
		
		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.ActivityIdParent == null ? '' : this.ActivityIdParent.ToString()} ${this.UserIdParent == null ? '' : this.UserIdParent.ToString()} ${this.EntityId}`;
	}
	
	public ToJson(): any
	{
		let json: any = new Object();
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json["Id"] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.UserId))
			json["UserId"] = this.UserId;
		if (!ObjectUtils.NullOrUndefined(this.Result))
			json["Result"] = this.Result;
		if (!ObjectUtils.NullOrUndefined(this.EntityId))
			json["EntityId"] = this.EntityId;
		if (!ObjectUtils.NullOrUndefined(this.ActivityId))
			json["ActivityId"] = this.ActivityId;
		if (!ObjectUtils.NullOrUndefined(this.UserIdParent))
			json["UserIdParent"] = this.UserIdParent.ToJson();
		if (!ObjectUtils.NullOrUndefined(this.Timestamp))
			json["Timestamp"] = DateUtils.ToDateServer(this.Timestamp);
		if (!ObjectUtils.NullOrUndefined(this.FinishedIn))
			json["FinishedIn"] = DateUtils.ToDateServer(this.FinishedIn);
		if (!ObjectUtils.NullOrUndefined(this.ActivityIdParent))
			json["ActivityIdParent"] = this.ActivityIdParent.ToJson();
		
		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.UserId = json["UserId"];
			this.Result = json["Result"];
			this.EntityId = json["EntityId"];
			this.ActivityId = json["ActivityId"];
			this.Timestamp = DateUtils.FromDateServer(json["Timestamp"]);
			this.FinishedIn = DateUtils.FromDateServer(json["FinishedIn"]);
			
			this.ActivityIdParent = new Activity();
			this.ActivityIdParent.ParseFromJson(json["ActivityIdParent"]);
			this.UserIdParent = new User();
			this.UserIdParent.ParseFromJson(json["UserIdParent"]);
		}
	}
	
	public static FromJson(json: Object): ActivityItem
	{
		let item: ActivityItem = new ActivityItem();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}

/**
 * Implementations of the `ActivityItemFilter` class
 */
export class ActivityItemFilter extends Crudable
{
	constructor(
		public Id: number = null,
		public ActivityId: number[] = null,
		public UserId: number[] = null,
		public EntityId: number = null,
		public FinishedIn: Date = null,
		public Result: number = null,
		public Timestamp: Date = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`ActivityItemFilter`, `activity_itemFilter`, CrudableTableDefinitionGender.Female, `Tarefa`, `Tarefa`, true, true, true, true, true, true);

		this.SetColumnDefinition(ActivityItemColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ActivityItemColumns.ActivityId, `activity_id`, `ActivityId`, `Atividade`, `Atividade`, ComponentTypeEnum.AutoComplete, true, true, true, false, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ActivityItemColumns.UserId, `user_id`, `UserId`, `Usuário`, `Usuários`, ComponentTypeEnum.AutoComplete, true, true, true, false, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ActivityItemColumns.EntityId, `entity_id`, `EntityId`, `Entidade`, `Entidades`, ComponentTypeEnum.Number, true, true, true, false, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ActivityItemColumns.FinishedIn, `finished_in`, `FinishedIn`, `Finalizada em`, `Finalizadas em`, ComponentTypeEnum.Date, true, true, false, false, false, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, true);
		this.SetColumnDefinition(ActivityItemColumns.Result, `result`, `Result`, `Resultado`, `Resultado`, ComponentTypeEnum.Text, true, true, false, false, false, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(ActivityItemColumns.Timestamp, `timestamp`, `Timestamp`, `Registro`, `Registros`, ComponentTypeEnum.Date, true, true, false, false, false, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
	}
	
	public Clone(): ActivityItemFilter
	{
		let clone: ActivityItemFilter = new ActivityItemFilter();
		clone.Id = this.Id;
		clone.UserId = this.UserId;
		clone.Result = this.Result;
		clone.EntityId = this.EntityId;
		clone.Timestamp = this.Timestamp;
		clone.ActivityId = this.ActivityId;
		clone.FinishedIn = this.FinishedIn;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;
		
		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}
	
	public ToJson(): any
	{
		let json: any = new Object();
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json["Id"] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Result))
			json["Result"] = this.Result;
		if (!ObjectUtils.NullOrUndefined(this.EntityId))
			json["EntityId"] = this.EntityId;
		if (!ObjectUtils.NullOrUndefined(this.Timestamp))
			json["Timestamp"] = DateUtils.ToDateServer(this.Timestamp);
		if (!ObjectUtils.NullOrUndefined(this.FinishedIn))
			json["FinishedIn"] = DateUtils.ToDateServer(this.FinishedIn);
		if (!ObjectUtils.NullOrUndefined(this.UserId))
			json["UserId"] = Array.isArray(this.UserId) ? this.UserId : [ this.UserId ];
		if (!ObjectUtils.NullOrUndefined(this.ActivityId))
			json["ActivityId"] = Array.isArray(this.ActivityId) ? this.ActivityId : [ this.ActivityId ];
		
		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.UserId = json["UserId"];
			this.Result = json["Result"];
			this.EntityId = json["EntityId"];
			this.ActivityId = json["ActivityId"];
			this.Timestamp = DateUtils.FromDateServer(json["Timestamp"]);
			this.FinishedIn = DateUtils.FromDateServer(json["FinishedIn"]);
		}
	}
	
	public static FromJson(json: Object): ActivityItemFilter
	{
		let item: ActivityItemFilter = new ActivityItemFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}