﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model'

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// Navigation Properties
import { Person } from 'clericuzzi-lib/entities/models/person.model';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const PersonAddressClass: string = `PersonAddress`;
export const PersonAddressTable: string = `person_address`;

/**
 * Column definitions for the `PersonAddress` class
 */
export enum PersonAddressColumns
{
	Id = "Id",
	Main = "Main",
	PersonId = "PersonId",
	Latitude = "Latitude",
	Longitude = "Longitude",
	Description = "Description",

	PersonIdParent = "PersonIdParent",
}

/**
 * Implementations of the `PersonAddress` class
 */
export class PersonAddress extends Crudable
{
	public PersonIdParent: Person;

	constructor(
		public Id: number = null,
		public PersonId: number = null,
		public Description: string = null,
		public Latitude: number = null,
		public Longitude: number = null,
		public Main: number = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`PersonAddress`, `person_address`, CrudableTableDefinitionGender.Male, `Person Address`, `Person Addresss`, true, true, true, true, true, true);

		this.SetColumnDefinition(PersonAddressColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(PersonAddressColumns.PersonId, `person_id`, `PersonId`, `Person Id`, `Person Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(PersonAddressColumns.Description, `description`, `Description`, `Description`, `Descriptions`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(PersonAddressColumns.Latitude, `latitude`, `Latitude`, `Latitude`, `Latitudes`, ComponentTypeEnum.Number, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(PersonAddressColumns.Longitude, `longitude`, `Longitude`, `Longitude`, `Longitudes`, ComponentTypeEnum.Number, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(PersonAddressColumns.Main, `main`, `Main`, `Main`, `Mains`, ComponentTypeEnum.Number, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): PersonAddress
	{
		let clone: PersonAddress = new PersonAddress();
		clone.Id = this.Id;
		clone.Main = this.Main;
		clone.PersonId = this.PersonId;
		clone.Latitude = this.Latitude;
		clone.Longitude = this.Longitude;
		clone.Description = this.Description;

		clone.PersonIdParent = this.PersonIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.PersonIdParent == null ? '' : this.PersonIdParent.ToString()}`;
	}

	public ToJson(): any
	{
		let json: any = new Object();
		json["Id"] = ObjectUtils.NullOrUndefined(this.Id) ? 0 : this.Id.toString();
		json["Main"] = ObjectUtils.NullOrUndefined(this.Main) ? 0 : this.Main.toString();
		json["PersonId"] = ObjectUtils.NullOrUndefined(this.PersonId) ? 0 : this.PersonId.toString();
		json["Latitude"] = ObjectUtils.NullOrUndefined(this.Latitude) ? 0 : this.Latitude.toString();
		json["Longitude"] = ObjectUtils.NullOrUndefined(this.Longitude) ? 0 : this.Longitude.toString();
		json["Description"] = ObjectUtils.NullOrUndefined(this.Description) ? null : this.Description.toString();

		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.Main = json["Main"];
			this.PersonId = json["PersonId"];
			this.Latitude = json["Latitude"];
			this.Longitude = json["Longitude"];
			this.Description = json["Description"];

			this.PersonIdParent = new Person();
			this.PersonIdParent.ParseFromJson(json["PersonIdParent"]);
		}
	}

	public static FromJson(json: Object): PersonAddress
	{
		let item: PersonAddress = new PersonAddress();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}