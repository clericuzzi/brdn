﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model';

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// Navigation Properties
import { Person } from 'clericuzzi-lib/entities/models/person.model';
import { PhoneType } from 'clericuzzi-lib/entities/models/phoneType.model';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const PersonPhoneClass: string = `PersonPhone`;
export const PersonPhoneTable: string = `person_phone`;

/**
 * Column definitions for the `PersonPhone` class
 */
export enum PersonPhoneColumns
{
  Id = 'Id',
  Main = 'Main',
  Phone = 'Phone',
  PersonId = 'PersonId',
  Operator = 'Operator',
  PhoneTypeId = 'PhoneTypeId',

  PersonIdParent = 'PersonIdParent',
  PhoneTypeIdParent = 'PhoneTypeIdParent',
}

/**
 * Implementations of the `PersonPhone` class
 */
export class PersonPhone extends Crudable
{
  public PersonIdParent: Person;
  public PhoneTypeIdParent: PhoneType;

  public static FromJson(json: Object): PersonPhone
  {
    const item: PersonPhone = new PersonPhone();
    if (json)
      item.ParseFromJson(json);

    return item;
  }

  constructor(
    public Id: number = null,
    public PersonId: number = null,
    public PhoneTypeId: number = null,
    public Phone: string = null,
    public Operator: string = null,
    public Main: number = null,
  )
  {
    super();
    this._InitDefinition();
  }
  private _InitDefinition(): void
  {
    this.SetTableDefinition(`PersonPhone`, `person_phone`, CrudableTableDefinitionGender.Male, `Person Phone`, `Person Phones`, true, true, true, true, true, true);

    this.SetColumnDefinition(PersonPhoneColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(PersonPhoneColumns.PersonId, `person_id`, `PersonId`, `Person Id`, `Person Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(PersonPhoneColumns.PhoneTypeId, `phone_type_id`, `PhoneTypeId`, `Phone Type Id`, `Phone Type Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(PersonPhoneColumns.Phone, `phone`, `Phone`, `Phone`, `Phones`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
    this.SetColumnDefinition(PersonPhoneColumns.Operator, `operator`, `Operator`, `Operator`, `Operators`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
    this.SetColumnDefinition(PersonPhoneColumns.Main, `main`, `Main`, `Main`, `Mains`, ComponentTypeEnum.Number, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
  }

  public Clone(): PersonPhone
  {
    const clone: PersonPhone = new PersonPhone();
    clone.Id = this.Id;
    clone.Main = this.Main;
    clone.Phone = this.Phone;
    clone.PersonId = this.PersonId;
    clone.Operator = this.Operator;
    clone.PhoneTypeId = this.PhoneTypeId;

    clone.PersonIdParent = this.PersonIdParent;
    clone.PhoneTypeIdParent = this.PhoneTypeIdParent;

    clone.TableDefinition = this.TableDefinition;
    clone.ColumnDefinitions = this.ColumnDefinitions;

    clone.CustomProperties = this.CustomProperties;

    return clone;
  }
  public ToString(): string
  {
    return `${this.PersonIdParent == null ? '' : this.PersonIdParent.ToString()}`;
  }

  public ToJson(): any
  {
    const json: any = new Object();
    json['Id'] = ObjectUtils.NullOrUndefined(this.Id) ? 0 : this.Id.toString();
    json['Main'] = ObjectUtils.NullOrUndefined(this.Main) ? 0 : this.Main.toString();
    json['Phone'] = ObjectUtils.NullOrUndefined(this.Phone) ? null : this.Phone.toString();
    json['PersonId'] = ObjectUtils.NullOrUndefined(this.PersonId) ? 0 : this.PersonId.toString();
    json['Operator'] = ObjectUtils.NullOrUndefined(this.Operator) ? null : this.Operator.toString();
    json['PhoneTypeId'] = ObjectUtils.NullOrUndefined(this.PhoneTypeId) ? 0 : this.PhoneTypeId.toString();

    this.SerializeCustomProperties(json);

    return json;
  }
  public ParseToJson(): string
  {
    const json: any = this.ToJson();

    return JSON.stringify(json);
  }
  public ParseFromJson(json: Object): void
  {
    if (json)
    {
      this.Id = json['Id'];
      this.Main = json['Main'];
      this.Phone = json['Phone'];
      this.PersonId = json['PersonId'];
      this.Operator = json['Operator'];
      this.PhoneTypeId = json['PhoneTypeId'];

      this.PersonIdParent = new Person();
      this.PersonIdParent.ParseFromJson(json['PersonIdParent']);
      this.PhoneTypeIdParent = new PhoneType();
      this.PhoneTypeIdParent.ParseFromJson(json['PhoneTypeIdParent']);
    }
  }
}
