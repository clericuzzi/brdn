﻿import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model'

// utils
import { DateUtils } from 'clericuzzi-lib/utils/date-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// components helpers
import { ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

export const LogTypeClass: string = `LogType`;
export const LogTypeTable: string = `log_type`;

/**
 * Column definitions for the `LogType` class
 */
export enum LogTypeColumns
{
	Id = "Id",
	Name = "Name",
}

/**
 * Implementations of the `LogType` class
 */
export class LogType extends Crudable
{
	constructor(
		public Id: number = null,
		public Name: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`LogType`, `log_type`, CrudableTableDefinitionGender.Male, `Tipo de Log`, `Tipos de Log`, true, true, true, true, true, true);

		this.SetColumnDefinition(LogTypeColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(LogTypeColumns.Name, `name`, `Name`, `Tipo`, `Tipos`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): LogType
	{
		let clone: LogType = new LogType();
		clone.Id = this.Id;
		clone.Name = this.Name;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.Name}`;
	}

	public ToJson(): any
	{
		let json: any = new Object();
		json["Id"] = ObjectUtils.NullOrUndefined(this.Id) ? 0 : this.Id.toString();
		json["Name"] = ObjectUtils.NullOrUndefined(this.Name) ? null : this.Name.toString();

		this.SerializeCustomProperties(json);

		return json;
	}
	public ParseToJson(): string
	{
		let json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json["Id"];
			this.Name = json["Name"];
		}
	}

	public static FromJson(json: Object): LogType
	{
		let item: LogType = new LogType();
		if (json)
			item.ParseFromJson(json);

		return item;
	}
}