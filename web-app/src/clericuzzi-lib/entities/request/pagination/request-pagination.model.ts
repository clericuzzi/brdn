/**
 * Sets the default behaviour for a new RequestPagination object 
 */
export class RequestPaginationDefaults
{
    /**
     * the default pagination size
     */
    public static PageSize: number = 25
}

/**
 * Defines a request's pagination properties
 */
export class RequestPagination
{
    /**
     * Default constructor
     * @param Pages the amount of pages
     * @param PageSize the pagination size
     * @param PageIndex the current page index
     * @param TotalCount the total amount of data fetched
     */
    constructor(
        public Pages: number = 1,
        public PageSize: number = RequestPaginationDefaults.PageSize,
        public PageIndex: number = 0,
        public TotalCount: number = 0,
    )
    {
    }
}