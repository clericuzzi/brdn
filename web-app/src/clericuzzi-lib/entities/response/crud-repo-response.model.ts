import { JSOnListSet } from './json-list-set.model';

export class CrudRepositoryResponse
{
    constructor(
        public RequestName: string,
        public Data: JSOnListSet,
    )
    {
    }
}