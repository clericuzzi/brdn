﻿import { ErrorMessage } from '../error/error-message.model';
import { RequestMessage } from '../request/request-message.model';

export class BaseMessage extends RequestMessage
{
  public Errors: ErrorMessage[];
  public HasErrors: boolean;
  public ErrorMessage: string;

  public get ContainsErrors(): boolean
  {
    return this.Errors !== null && this.Errors !== undefined && this.Errors.length > 0;
  }

  public Exceptions: any[];
  public AddException(e: any)
  {
    if (this.Exceptions === null)
      this.Exceptions = [];

    this.Exceptions.push(e);
  }

  public get Token(): string { return this.Identifier !== null ? this.Identifier.RequestToken : null; }

  public get ErrorCode(): number
  {
    if (this.Errors !== null && this.Errors.length > 0)
      return this.Errors[0].ErrorCode;

    return null;
  }
  public get ErrorCodes(): number[]
  {
    const Errors = [];
    if (this.Errors !== null && this.Errors.length > 0)
      for (let i = 0; i < this.Errors.length; i++)
        Errors.push(this.Errors[i].ErrorCode);

    return Errors;
  }
}
