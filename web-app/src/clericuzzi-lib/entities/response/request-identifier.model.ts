﻿/**
 * A structure that contains user info for authentication
 */
export class RequestIdentifier
{
    /**
     * The requester user's id
     */
    public UserId: number;
    /**
     * The requester user's name
     */
    public UserName: string;
    /**
     * The request's token
     */
    public RequestToken: string;
}