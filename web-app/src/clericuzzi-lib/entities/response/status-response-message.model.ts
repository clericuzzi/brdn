﻿import { MatSnackBar } from '@angular/material';
// utils
import { SnackUtils } from 'clericuzzi-lib/utils/snack-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';
// models
import { BaseMessage } from './base-message.model'
// services
// components
// behaviours

/**
 * This class represents a response from the server containing a flag indicating the request's success
 */
export class StatusResponseMessage extends BaseMessage
{
    /**
     * A flag defining if the request was successfull or not
     */
    public Success: boolean;

    public static DefaultError: string = `Falha no envio da solicitação`;
    public static DefaultSuccess: string = `Solicitação enviada com sucesso`;
    public static HandleResponse(response: StatusResponseMessage, snack: MatSnackBar, successAction: Function, errorAction: Function, showSuccessMessage: boolean = true, showErrorMessage: boolean = true, successMessage: string = null, errorMessage: string = null): void
    {
        if (ObjectUtils.NullOrUndefined(response, response.Success))
            SnackUtils.OpenError(snack, StatusResponseMessage.DefaultError);
        else
        {
            if (response.Success)
            {
                if (showSuccessMessage)
                    SnackUtils.OpenSuccess(snack, ObjectUtils.NullUndefinedOrEmpty(successMessage) ? StatusResponseMessage.DefaultSuccess : successMessage);
                if (!ObjectUtils.NullOrUndefined(successAction))
                    successAction();
            }
            else
            {
                if (showErrorMessage)
                    SnackUtils.OpenError(snack, ObjectUtils.NullUndefinedOrEmpty(errorMessage) ? response.ErrorMessage : (ObjectUtils.NullUndefinedOrEmpty(errorMessage) ? StatusResponseMessage.DefaultError : errorMessage));
                if (!ObjectUtils.NullOrUndefined(errorAction))
                    errorAction();
            }
        }
    }
}