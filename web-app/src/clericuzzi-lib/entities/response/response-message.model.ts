﻿import * as jQuery from 'jquery';
import { JsonUtils } from 'clericuzzi-lib/utils/json-utils';
import { BaseMessage } from './base-message.model'
import { MatSnackBar } from '@angular/material';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';
import { SnackUtils } from 'clericuzzi-lib/utils/snack-utils';
import { BatchResponseMessage } from './batch-response-message.model';

/**
 * This class represents a response from the server containing a single entity
 */
export class ResponseMessage<T> extends BaseMessage
{
  /**
   * The returned entity
   */
  public Entity: T;

  /**
   * Parses the returned data and returns it as an type T object
   * @param baseObject the base object to create the returned one
   */
  public Parse(baseObject: T): T
  {
    const newInstance: T = JsonUtils.ToInstanceOf(jQuery.extend({}, baseObject), this.Entity);

    return newInstance;
  }

  public static HandleResponse<T>(response: ResponseMessage<T>, baseObject: T, snack: MatSnackBar): T
  {
    if (ObjectUtils.NullOrUndefined(response))
      SnackUtils.OpenError(snack, `Falha no envio da requisição...`);
    else
    {
      if (ObjectUtils.NullOrUndefined(response.Entity))
        SnackUtils.Open(snack, `Nenhum dado retornado...`, `aviso`);
      else
        return response.Parse(baseObject);
    }

    return null;
  }
  public static HandleResponseList<T>(response: BatchResponseMessage<T>, baseObject: T, snack: MatSnackBar): T[]
  {
    if (ObjectUtils.NullOrUndefined(response))
      SnackUtils.OpenError(snack, `Falha no envio da requisição...`);
    else
    {
      if (ObjectUtils.NullUndefinedOrEmptyArray(response.Entities))
        SnackUtils.Open(snack, `Nenhum dado retornado...`, `aviso`);
      else
      {
        const items: T[] = JsonUtils.ParseList(response.Entities, baseObject);
        return items;
      }
    }

    return null;
  }
}
