﻿import * as _ from 'lodash';
import * as jQuery from 'jquery';
import { JsonUtils } from 'clericuzzi-lib/utils/json-utils';

// utils
import { Dictionary } from 'clericuzzi-lib/utils/dictionary-utils';

// models
import { BaseMessage } from './base-message.model'
import { ObjectUtils } from '../../utils/object-utils';
import { Crudable } from '../crud/crudable.model';

export class BatchResponseMessageList extends BaseMessage
{
  /**
   * The entities list
   */
  public Entities: Dictionary<any[]>;
  /**
   * The amount of data returned
   */
  public EntityCount: number;

  public HasKey(key: string): any
  {
    if (!ObjectUtils.NullOrUndefined(this.Entities))
      return !ObjectUtils.NullOrUndefined(this.Entities[key]);
    else
      return false;
  }
  public GetArrayItem(key: string): any
  {
    let entityList: any[] = this.Entities[key];
    if (ObjectUtils.NullUndefinedOrEmptyArray(entityList))
      return [];
    else
    {
      let entities: any[] = [];
      for (let entity of entityList)
        entities.push(entity);

      return entities[0];
    }
  }
  public GetArrayItems(key: string): any[]
  {
    let entityList: any[] = this.Entities[key];
    if (ObjectUtils.NullUndefinedOrEmptyArray(entityList))
      return [];
    else
    {
      let entities: any[] = [];
      for (let entity of entityList)
        entities.push(entity);

      return entities;
    }
  }
  public GetList<T extends Crudable>(key: string, baseObject: T): T[]
  {
    let entityList: any[] = this.Entities[key];
    if (ObjectUtils.NullUndefinedOrEmptyArray(entityList))
      return [];
    else
    {
      let entities: T[] = [];
      for (let entity of entityList)
      {
        baseObject.ParseFromJson(entity);
        entities.push(baseObject.Clone() as T);
      }

      return entities;
    }
  }
  public GetItem<T extends Crudable>(key: string, baseObject: T): T
  {
    let entityList: any[] = this.Entities[key];
    if (ObjectUtils.NullUndefinedOrEmptyArray(entityList))
      return null;
    else
    {
      let entity: any = entityList[0];
      baseObject.ParseFromJson(entity);

      return baseObject;
    }
  }
  public GetAny(key: string): any
  {
    let entityList: any[] = this.Entities[key];
    if (ObjectUtils.NullUndefinedOrEmptyArray(entityList))
      return null;
    else
    {
      let entity: any = entityList[0];

      return entity;
    }
  }
  public Exists<T extends Crudable>(key: string, baseObject: T): boolean
  {
    let entity: T = this.GetItem(key, baseObject);
    return !ObjectUtils.NullOrUndefined(entity);
  }
}

/**
 * This class represents a response from the server containing a list of entities
 */
export class BatchResponseMessage<T> extends BaseMessage
{
  /**
   * The entities list
   */
  public Entities: T[];
  /**
   * The amount of data returned
   */
  public EntityCount: number;

  /**
   * Parses the returned data and returns it as an type T object
   * @param baseObject the base object to create the returned one
   */
  public Parse<T>(baseObject: T): void
  {
    const entities = [];
    for (let i = 0; i < this.Entities.length; i++)
    {
      const model: T = JsonUtils.ToInstanceOf(_.cloneDeep(baseObject), this.Entities[i]);
      if (baseObject instanceof Crudable)
      {
        const crudableModel: Crudable = (model as any) as Crudable;
        const clone: Crudable = crudableModel.Clone();

        entities.push(clone);
      }
      else
        entities.push(model);
    }

    this.Entities = entities;
  }
  /**
   * Parses the returned data and returns it as an type T object array
   * @param baseObject the base object to create the returned one
   */
  public ParseJSOnContent<T>(baseObject: T): void
  {
    if (this.Entities != null && this.Entities.length > 0)
    {
      let json = JSON.stringify(this.Entities);
      this.Entities = JsonUtils.ToInstance([], json);
      this.Parse(baseObject);
    }
  }
}
