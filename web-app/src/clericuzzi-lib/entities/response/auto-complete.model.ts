// utils
import { JsonUtils } from "../../utils/json-utils";
import { ObjectUtils } from "../../utils/object-utils";

// models
import { BaseMessage } from "./base-message.model";
import { ComboInputValueModel } from "../../modules/shared/form-controls/custom-inputs/combo-input/combo-input-value.model";

/**
 * A response containing a list of AutoCompleteResponses
 */
export class AutoCompleteResponseList extends BaseMessage
{
    /**
     * Default constructor
     * @param Entities The actual list of response items
     */
    constructor(
        public Entities: AutoCompleteResponse[] = [],
    )
    {
        super();
    }
    public GetEntities(sourceProperty: string): ComboInputValueModel<number>[]
    {
        this.Entities = JsonUtils.ParseList(this.Entities, new AutoCompleteResponse);
        for (let response of this.Entities)
        {
            response.Entities = JsonUtils.ParseList(response.Entities, new ComboInputValueModel<number>());
            if (response.SourceProperty == sourceProperty)
            return response.Entities;
        }
        
        return null;
    }
    
    public static ParseFrom(obj: any): AutoCompleteResponseList
    {
        let list: AutoCompleteResponseList = new AutoCompleteResponseList();
        list.Entities = obj[`Entities`];
        if (!ObjectUtils.NullUndefinedOrEmptyArray(list.Entities))
            for (let entityList of list.Entities)
                entityList = AutoCompleteResponse.ParseFrom(entityList);

        return list;
    }
}

/**
 * A class that encapsulates a list of entities for autocomplete field presentation
 */
export class AutoCompleteResponse extends BaseMessage
{
    /**
     * Default constructor
     * @param SourceProperty The foreign key property that will be filled
     * @param Entities The actual list of items to be presented
     * @param EntityCount The amount of data returned
     */
    constructor(
        public SourceProperty: string = null,
        public Entities: ComboInputValueModel<number>[] = [],
        public EntityCount: number = null,
    )
    {
        super();
    }

    public static ParseFrom(obj: any): AutoCompleteResponse
    {
        let response: AutoCompleteResponse = new AutoCompleteResponse();
        response.Entities = obj[`Entities`];
        response.EntityCount = obj[`EntityCount`];
        response.SourceProperty = obj[`SourceProperty`];
        if (!ObjectUtils.NullUndefinedOrEmptyArray(response.Entities))
            for (let entity of response.Entities)
                entity = ComboInputValueModel.From(entity);

        return response;
    }
}