// utils
import { JsonUtils } from 'clericuzzi-lib/utils/json-utils';

/**
 * This class creates a serialized representation of a data set
 */
export class JSOnList
{
    public List: any[] = [];
    /**
     * The json representation of the given data, call GetEntities to deserialize it
     */
    public JsonContent: string;
    constructor(public BaseObject: any, public TypeName: string) { }

    /**
     * Parses the returned data and returns it as an type T object
     * @param baseObject the base object to create the returned one
     */
    public Parse(baseObject: any = null): void
    {
        if (baseObject != null && baseObject != undefined)
            this.BaseObject = baseObject;
        
        let objects: any[] = JSON.parse(this.JsonContent);
        for (let item of objects)
        {
            let currentItem: any = JsonUtils.ToInstanceOf(JsonUtils.CloneDeep(this.BaseObject), item);
            this.List.push(currentItem);
        }

        this.JsonContent = null;
    }
}