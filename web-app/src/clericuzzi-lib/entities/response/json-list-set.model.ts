import { BaseMessage } from './base-message.model';

// utils
import { JsonUtils } from 'clericuzzi-lib/utils/json-utils';

// models
import { JSOnList } from './json-list.model';
import { RequestPagination } from '../request/pagination/request-pagination.model';

export class JSOnListSet extends BaseMessage
{
    public TypeName: string;
    public JsonContent: string;
    public ResultCount: number;

    public List: any = [];
    
    public GetData<T>(key: string, baseObject: any, typeFullName: string): T[]
    {
        if (this.List != null && this.List != undefined && this.List[key] != null && this.List[key] != undefined)
        {
            var list: T[] = JsonUtils.ParseList<T>(this.List[key], baseObject);
            return list;
        }
        else
            return null;
    }
    public GetList(key: string, baseObject: any, typeFullName: string): JSOnList
    {
        if (this.List != null && this.List != undefined && this.List[key] != null && this.List[key] != undefined)
        {
            var jsonList: JSOnList = JsonUtils.ToInstanceOf<JSOnList>(new JSOnList(baseObject, typeFullName), this.List[key]);
            return jsonList;
        }
        else
            return null;
    }
    public GetEntity<T>(key: string, baseObject: T): T
    {
        let entities: T[] = this.GetEntities(key, baseObject);
        if (entities != null && entities != undefined && entities.length > 0)
            return entities[0];
        else
            return null;
    }
    public GetEntities<T>(key: string, baseObject: T): T[]
    {
        if (this.List != null && this.List != undefined && this.List[key] != null && this.List[key] != undefined)
        {
            let jsonList: any = this.List[key];
            let elements = JSON.parse(jsonList[`JsonContent`]);
            return JsonUtils.ParseList(elements, baseObject);
        }
        else
            return [];
    }
    public GetPagination(key: string): RequestPagination
    {
        if (this.List != null && this.List != undefined && this.List[key] != null && this.List[key] != undefined)
        {
            let jsonList: any = this.List[key];
            let paginationObject: RequestPagination = jsonList[`Pagination`];
            let pagination = JsonUtils.ToInstanceOf<RequestPagination>(new RequestPagination(), paginationObject);
            
            return pagination;
        }
        else
            return new RequestPagination();
    }
}