﻿import { CustomField } from './custom-field.model';

// utils
import { JsonUtils } from 'clericuzzi-lib/utils/json-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

/**
 * This class represents an object that can extend it's properties with custom fields
 */
export class CustomFieldable
{
    public CustomFields: CustomField[] = [];

    /**
     * Clears the custom fields array
     */
    public Clear(): void
    {
        this.CustomFields = [];
    }
    /**
     * Gets the count of the custom fields currently in the collection
     */
    public get Count(): number
    {
        if (ObjectUtils.NullUndefinedOrEmptyArray(this.CustomFields))
            return 0;
        else
            return this.CustomFields.length;
    }
    private _CheckFields(): void
    {
        if (this.CustomFields == null)
            this.CustomFields = new Array<CustomField>();
    }

    /**
     * Adds a new parameter to the collection
     * @param key the param's name
     * @param value the param's value
     */
    public Add(key: string, value: any): boolean
    {
        if (ObjectUtils.NullOrUndefined(value))
            value = null;
        
        this._CheckFields();
        if (!this.Contains(key))
        {
            this.CustomFields.push(new CustomField(key, JsonUtils.Stringify(value)));
            return true;
        }
        else
        {
            this.Remove(key);
            this.Add(key, value);
        }

        return false;
    }
    /**
     * Adds a new parameter to the collection without serializing it
     * @param key the param's name
     * @param value the param's value
     */
    public AddRaw(key: string, value: any): boolean
    {
        if (ObjectUtils.NullOrUndefined(value))
            value = null;
        
        this._CheckFields();
        if (!this.Contains(key))
        {
            this.CustomFields.push(new CustomField(key, value));
            return true;
        }
        else
        {
            this.Remove(key);
            this.Add(key, value);
        }

        return false;
    }
    /**
     * Removes a parameter from the collection
     * @param key the param's name
     */
    public Remove(key: string): boolean
    {
        this._CheckFields();
        if (this.Contains(key))
        {
            for (var i = 0; i < this.CustomFields.length; i++)
            {
                let field: CustomField = this.CustomFields[i];
                if (field.Key == key)
                {
                    this.CustomFields.splice(i, 1);
                    return true;
                }
            }
        }

        return false;
    }
    /**
     * Checks if the given parameter exists in the collection
     * @param key the param's name
     */
    public Contains(key: string): boolean
    {
        this._CheckFields();
        if (this.CustomFields == null || this.CustomFields.length == 0)
            return false;

        for (var i = 0; i < this.CustomFields.length; i++)
            if (this.CustomFields[i].Key == key)
                return true;

        return false;
    }
    /**
     * Returns the custom field that matchs with the provided param name
     * @param key the param's name
     */
    private _GetField(key: string): CustomField
    {
        this._CheckFields();
        if (this.CustomFields == null || this.CustomFields.length == 0)
            return null;

        for (var i = 0; i < this.CustomFields.length; i++)
            if (this.CustomFields[i].Key == key)
                return this.CustomFields[i];

        return null;
    }
    /**
     * Returns a value stored in the collection
     * @param key the param's name
     * @param reviver the reviver class
     */
    public GetValue<T>(key: string, reviver: (key: any, value: any) => any): T
    {
        try
        {
            if (this.Contains(key))
            {
                let field: CustomField = this._GetField(key);
                if (field.Value != null && field != undefined)
                {
                    if (reviver == null)
                        return JSON.parse(field.Value) as T;
                    else
                        return reviver('', JSON.parse(field.Value));
                }
            }
        }
        catch (e)
        {
        }

        return null;
    }

    public ParseFieldsFrom(obj: any): void
    {
        this.CustomFields = [];
        let fields: any = obj[`CustomFields`];
        if (!ObjectUtils.NullUndefinedOrEmptyArray(fields))
        {
            for (var prop in fields)
                this.CustomFields.push(new CustomField(prop, fields[prop]));
        }
    }
}