export class PropertyFilter
{
    constructor(
        public Name: string,
        public Value: any,
        public Operation: string = `Equals`,
    ) {
    }
}