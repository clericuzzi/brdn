﻿// utils
import { Dictionary } from 'clericuzzi-lib/utils/dictionary-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// models
import { CrudableTableColumnDefinitionModel, ComponentTypeEnum } from './definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionModel, CrudableTableDefinitionGender } from './definitions/crudable-table-definition.model';

export abstract class Crudable
{
    public PkProperty: string = `Id`;
    public TableDefinition: CrudableTableDefinitionModel;
    public CustomProperties: string[] = [];
    public ColumnDefinitions: Dictionary<CrudableTableColumnDefinitionModel> = new Dictionary<CrudableTableColumnDefinitionModel>();

    constructor()
    {
    }

    public SerializeCustomProperties(json: any): void
    {
        for (const property in this)
            if (this.CustomProperties.indexOf(property) >= 0)
                json[property] = ObjectUtils.NullOrUndefined(this[property]) ? null : this[property];
    }
    public AddCustomProperty(...propertyNames: string[]): void
    {
        if (!ObjectUtils.NullUndefinedOrEmptyArray(propertyNames))
            for (const propertyName of propertyNames)
                if (this.CustomProperties.indexOf(propertyName) < 0)
                    this.CustomProperties.push(propertyName);
    }
    public RemoveCustomProperty(...propertyNames: string[]): void
    {
        if (!ObjectUtils.NullUndefinedOrEmptyArray(propertyNames))
            for (const propertyName of propertyNames)
            {
                const index: number = this.CustomProperties.indexOf(propertyName);
                if (index >= 0)
                    this.CustomProperties = this.CustomProperties.splice(index, 1);
            }
    }

    public GetColumnDefinition(columnName: string): CrudableTableColumnDefinitionModel
    {
        return this.ColumnDefinitions.Get(columnName);
    }

    public SetTableDefinition(className: string, tableName: string, tableGender: CrudableTableDefinitionGender, readableName: string, readableNamePlural: string, canFilter: boolean = true, canSearch: boolean = true, canInsert: boolean = true, canDelete: boolean = true, canUpdate: boolean = true, hasSelectionBox: boolean = true): void
    {
        this.TableDefinition = new CrudableTableDefinitionModel(className, tableName, tableGender, readableName, readableNamePlural, canFilter, canSearch, canInsert, canDelete, canUpdate, hasSelectionBox);
    }

    private _UpdateColumnProperty(key: string, property: string, value: any): void
    {
        if (!ObjectUtils.NullOrUndefined(this.ColumnDefinitions))
        {
            const column: CrudableTableColumnDefinitionModel = this.ColumnDefinitions.Get(key);
            if (!ObjectUtils.NullOrUndefined(column))
                column[property] = value;
        }
    }
    public CloneColumn(columnName: string): CrudableTableColumnDefinitionModel
    {
        let column: CrudableTableColumnDefinitionModel = this.ColumnDefinitions.Get(columnName);
        if (!ObjectUtils.NullOrUndefined(column))
            column = this.ColumnDefinitions.Get(columnName).Clone();

        return column;
    }
    public SetColumnDefinition(key: string, columnName: string, propertyName: string, readableName: string, readableNamePlural: string, componentType: ComponentTypeEnum, canSort: boolean = true, canFilter: boolean = false, canInsert: boolean = true, canSelect: boolean = true, canUpdate: boolean = true, hasShadow: boolean = true, componentSubcategory: string = null, transform: Function = null, maxWidth: number = null, widthPixels: number = null, widthPercentage: number = null, textAlign: string = `center`, isNullable: boolean = false): void
    {
        this.ColumnDefinitions.Add(key, new CrudableTableColumnDefinitionModel(columnName, propertyName, readableName, readableNamePlural, componentType, canSort, canFilter, canInsert, canSelect, canUpdate, hasShadow, componentSubcategory, transform, maxWidth, widthPixels, widthPercentage, textAlign, isNullable));
    }

    public get InsertTitle(): string
    {
        if (ObjectUtils.NullOrUndefined(this.TableDefinition))
            return null;
        else
            return `Nov${this.TableDefinition.Article()} ${this.TableDefinition.ReadableName}`;
    }
    public get UpdateTitle(): string
    {
        if (ObjectUtils.NullOrUndefined(this.TableDefinition))
            return null;
        else
            return `Atualizar ${this.TableDefinition.ReadableName}`;
    }
    public get SearchTitle(): string
    {
        if (ObjectUtils.NullOrUndefined(this.TableDefinition))
            return null;
        else
            return `Pesquisar ${this.TableDefinition.ReadableName}`;
    }

    public get Key(): number
    {
        if (ObjectUtils.NullOrUndefined(this[this.PkProperty]))
            return null;
        else
            return this[this.PkProperty];
    }
    public GetKey(): number
    {
        if (ObjectUtils.NullOrUndefined(this[this.PkProperty]))
            return null;
        else
            return this[this.PkProperty];
    }

    public Filter(filter: string): boolean
    {
        filter = filter.toLocaleLowerCase();

        const json: any = this.ToJson();
        const properties: string[] = Object.keys(json);
        for (const property of properties)
        {
            const value: any = ObjectUtils.GetValue(json, null, property);
            if (!ObjectUtils.NullOrUndefined(value))
            {
                if (typeof (value) === `object`)
                {
                    const innerValues: string[] = Object.values(value);
                    for (const innerValue of innerValues)
                        if (this._FilterValue(innerValue, filter))
                            return true;
                }
                else
                {
                    if (this._FilterValue(value, filter))
                        return true;
                }
            }
        }

        return false;
    }
    private _FilterValue(value: any, filterString: string): boolean
    {
        if (!ObjectUtils.NullOrUndefined(value))
        {
            let stringValue: string = value.toString();
            stringValue = stringValue.toLocaleLowerCase();
            if (stringValue.indexOf(filterString) >= 0)
                return true;
        }
        return false;
    }

    // ICRUDABLE IMPLEMENTATION
    public abstract Clone(): Crudable;
    public abstract ToJson(): string;
    public abstract ToString(): string;
    public abstract ParseToJson(): string;
    public abstract ParseFromJson(json: Object): void;

    public ValidateProperty(column: CrudableTableColumnDefinitionModel): boolean
    {
        const value: any = this[column.PropertyName];
        if (ObjectUtils.NullUndefinedOrEmpty(value))
            return false;
        else
        {
            if (column.ComponentType === ComponentTypeEnum.Text
                || column.ComponentType === ComponentTypeEnum.TextArea
                || column.ComponentType === ComponentTypeEnum.Password)
                return !ObjectUtils.NullUndefinedOrEmpty(value);
            else if (column.ComponentType === ComponentTypeEnum.AutoComplete)
                return value > 0;
            else if (column.ComponentType === ComponentTypeEnum.ComboSex)
                return value === 'm' || value === 'f';
            else if (column.ComponentType === ComponentTypeEnum.ComboYesNo)
                return value === 0 || value === 1;
            else if (column.ComponentType === ComponentTypeEnum.Date || column.ComponentType === ComponentTypeEnum.Number || column.ComponentType === ComponentTypeEnum.Decimal)
                return true;
            else
                return false;
        }
    }
}
