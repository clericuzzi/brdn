import { Type } from '@angular/compiler/src/core';
import { Dictionary } from 'clericuzzi-lib/utils/dictionary-utils';
import { SafeStyle, DomSanitizer } from '@angular/platform-browser';
import { ViewContainerRef, ComponentFactoryResolver } from '@angular/core';

// utils
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// models
import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model';
import { ComboInputValueModel } from '../../../modules/shared/form-controls/custom-inputs/combo-input/combo-input-value.model';

// components
import { DateInputComponent } from 'clericuzzi-lib/modules/shared/form-controls/date-input/date-input.component';
import { TextInputComponent } from 'clericuzzi-lib/modules/shared/form-controls/text-input/text-input.component';
import { HourInputComponent } from 'clericuzzi-lib/modules/shared/form-controls/custom-inputs/hour-input/hour-input.component';
import { ComboInputComponent } from 'clericuzzi-lib/modules/shared/form-controls/custom-inputs/combo-input/combo-input.component';
import { NumberInputComponent } from 'clericuzzi-lib/modules/shared/form-controls/number-input/number-input.component';
import { FormControlComponent } from 'clericuzzi-lib/modules/shared/form-controls/form-control.component';
import { LabelControlComponent } from 'clericuzzi-lib/modules/shared/form-controls/label-control/label-control.component';
import { SelectionBoxComponent } from 'clericuzzi-lib/modules/shared/form-controls/selection-box/selection-box.component';
import { MaterialPopupComponent } from '../../../modules/material/components/material-popup/material-popup.component';
import { TextareaInputComponent } from 'clericuzzi-lib/modules/shared/form-controls/textarea-input/textarea-input.component';
import { AutocompleteInputComponent } from 'clericuzzi-lib/modules/shared/form-controls/autocomplete-input/autocomplete-input.component';
import { JsonUtils } from 'clericuzzi-lib/utils/json-utils';

export enum ComponentTypeEnum
{
  Date = 'input-date',
  Hour = 'input-hour',
  Text = 'input-text',
  Check = 'input-check',
  Combo = 'input-combo',
  Radio = 'input-radio',
  Label = 'label-control',
  Number = 'input-number',
  Decimal = 'input-decimal',
  TextArea = 'text-area',
  Password = 'input-password',
  ComboSex = 'input-combo-sex',
  ComboYear = 'input-combo-year',
  ComboMonth = 'input-combo-month',
  ComboYesNo = 'input-combo-yes-no',
  AutoComplete = 'auto-complete',
}

export class CrudableTableColumnDefinitionModel
{
  public static _ComponentMapping: Dictionary<Type> = new Dictionary<Type>();

  /**
   * Is this the table's main field
   */
  public IsPk: boolean = false;

  /**
   * Is this field from another table
   */
  public IsFk: boolean = false;
  /**
   * The table from where this field references
   */
  public ParentTable: string;

  public static CreateSelectionBox(container: ViewContainerRef, factoryResolver: ComponentFactoryResolver, customClass: string = null, sanitizer: DomSanitizer = null, customStyle: SafeStyle = null): SelectionBoxComponent
  {
    const factory = factoryResolver.resolveComponentFactory(CrudableTableColumnDefinitionModel._ComponentMapping.Get(ComponentTypeEnum.Check));
    const component = factory.create(container.injector);
    container.insert(component.hostView);

    const element: SelectionBoxComponent = component['_component'];
    if (!ObjectUtils.NullUndefinedOrEmpty(customClass))
      element.SelectionClass = customClass;

    if (!ObjectUtils.NullOrUndefined(customStyle))
      element.Style = customStyle;

    return element;
  }
  public static Init(): void
  {
    CrudableTableColumnDefinitionModel._ComponentMapping.Add(ComponentTypeEnum.Check, SelectionBoxComponent);
    CrudableTableColumnDefinitionModel._ComponentMapping.Add(ComponentTypeEnum.Label, LabelControlComponent);

    CrudableTableColumnDefinitionModel._ComponentMapping.Add(ComponentTypeEnum.Date, DateInputComponent);
    CrudableTableColumnDefinitionModel._ComponentMapping.Add(ComponentTypeEnum.Hour, HourInputComponent);
    CrudableTableColumnDefinitionModel._ComponentMapping.Add(ComponentTypeEnum.Text, TextInputComponent);
    CrudableTableColumnDefinitionModel._ComponentMapping.Add(ComponentTypeEnum.Combo, ComboInputComponent);
    CrudableTableColumnDefinitionModel._ComponentMapping.Add(ComponentTypeEnum.Number, NumberInputComponent);
    CrudableTableColumnDefinitionModel._ComponentMapping.Add(ComponentTypeEnum.Decimal, NumberInputComponent);
    CrudableTableColumnDefinitionModel._ComponentMapping.Add(ComponentTypeEnum.TextArea, TextareaInputComponent);
    CrudableTableColumnDefinitionModel._ComponentMapping.Add(ComponentTypeEnum.Password, TextInputComponent);
    CrudableTableColumnDefinitionModel._ComponentMapping.Add(ComponentTypeEnum.ComboSex, ComboInputComponent);
    CrudableTableColumnDefinitionModel._ComponentMapping.Add(ComponentTypeEnum.ComboYear, ComboInputComponent);
    CrudableTableColumnDefinitionModel._ComponentMapping.Add(ComponentTypeEnum.ComboMonth, ComboInputComponent);
    CrudableTableColumnDefinitionModel._ComponentMapping.Add(ComponentTypeEnum.ComboYesNo, ComboInputComponent);
    CrudableTableColumnDefinitionModel._ComponentMapping.Add(ComponentTypeEnum.AutoComplete, AutocompleteInputComponent);
  }

  /**
   * Default constructor
   * @param ColumnName the column's name
   * @param PropertyName the property's name
   * @param ReadableName the property's readable name
   * @param ReadableNamePlural the property's plural name
   * @param ComponentType the crud component's type
   * @param CanSort is the field's colun sortable?
   * @param CanFilter can we search the crud based on this field?
   * @param CanInsert does this field have a component in the insert crud?
   * @param CanSelect will this field be shown in the default select crud?
   * @param CanUpdate does this field have a component in the update crud?
   * @param HasShadow does this component will have a shadow behint it?
   * @param ComponentSubcategory ex: password, is a subcategory od the Text type
   * @param Transform a transformation to be applied whenever this field appears in the select crud
   * @param MaxWidth the component's maximum width in pixels
   * @param WidthPixels the component's default width in pixels
   * @param WidthPercentage  the component's width in percentage
   * @param TextAlign the component's align inside the select list
   * @param IsNullable defines if the column can have a null value
   * @param DefaultHeight the component's default height in the crud panels
   */
  constructor(
    public ColumnName: string,
    public PropertyName: string,
    public ReadableName: string,
    public ReadableNamePlural: string,

    public ComponentType: ComponentTypeEnum,

    public CanSort: boolean = true,
    public CanFilter: boolean = false,
    public CanInsert: boolean = true,
    public CanSelect: boolean = true,
    public CanUpdate: boolean = true,

    public HasShadow: boolean = true,
    public ComponentSubcategory: string = null,
    public Transform: Function = null,

    public MaxWidth: number = null,
    public WidthPixels: number = null,
    public WidthPercentage: number = null,

    public TextAlign: string = `center`,
    public IsNullable: boolean = false,
    public DefaultHeight: number = 40,
  )
  {
  }

  public CreateLabel(container: ViewContainerRef, factoryResolver: ComponentFactoryResolver, model: any, customClass: string = null, sanitizer: DomSanitizer = null, customStyle: SafeStyle = null, alternateProperty: string = null): LabelControlComponent
  {
    const factory = factoryResolver.resolveComponentFactory(CrudableTableColumnDefinitionModel._ComponentMapping.Get(ComponentTypeEnum.Label));
    const component = factory.create(container.injector);
    container.insert(component.hostView);

    const element: LabelControlComponent = component['_component'];
    element.Model = model;
    element.Align = this.TextAlign;
    element.Property = this.PropertyName;
    element.Transform = this.Transform;
    element.Placeholder = this.ReadableName;
    element.AlternateProperty = alternateProperty;
    if (!ObjectUtils.NullUndefinedOrEmpty(customClass))
      element.CustomClass = customClass;

    if (!ObjectUtils.NullOrUndefined(customStyle))
      element.Style = customStyle;
    else
    {
      let styleDefinition: string = ``;
      if (this.WidthPixels)
        styleDefinition = `${styleDefinition} width: ${this.WidthPixels}px;`;
      else if (this.WidthPercentage)
        styleDefinition = `${styleDefinition} width: ${this.WidthPercentage}%;`;

      if (!ObjectUtils.NullUndefinedOrEmpty(styleDefinition))
      {
        styleDefinition = styleDefinition.trim();
        element.Style = sanitizer.bypassSecurityTrustStyle(styleDefinition);
      }
    }

    return element;
  }
  public CreateComponent<T>(container: ViewContainerRef, factoryResolver: ComponentFactoryResolver, model: Crudable, customClass: string = null, sanitizer: DomSanitizer = null, customStyle: SafeStyle = null, index: number = null): T
  {
    const factory = factoryResolver.resolveComponentFactory(CrudableTableColumnDefinitionModel._ComponentMapping.Get(this.ComponentType));
    const component = factory.create(container.injector);
    if (ObjectUtils.NullOrUndefined(index))
      container.insert(component.hostView);
    else
      container.insert(component.hostView, index);

    const element: FormControlComponent<any> = component['_component'];
    element.Type = this.ComponentType;
    element.Model = model;
    element.Property = this.PropertyName;
    element.Required = !this.IsNullable;
    element.HasShadow = this.HasShadow;
    element.Placeholder = this.ReadableName;
    element.CustomClass = customClass;
    element.ComponentHeight = this.DefaultHeight;

    element.IsDate = this.ComponentType === ComponentTypeEnum.Date;
    element.IsText = this.ComponentType === ComponentTypeEnum.Text;
    element.IsCombo = this.ComponentType === ComponentTypeEnum.Combo || this.ComponentType === ComponentTypeEnum.ComboYesNo || this.ComponentType === ComponentTypeEnum.ComboSex;
    element.IsAutoComplete = this.ComponentType === ComponentTypeEnum.AutoComplete;

    if (this.ComponentType === ComponentTypeEnum.Decimal)
      element[`Decimal`] = true;
    if (this.ComponentType === ComponentTypeEnum.ComboSex)
      element[`Items`] = [new ComboInputValueModel<string>(`m`, `Masculino`), new ComboInputValueModel<string>(`f`, `Feminino`)];
    if (this.ComponentType === ComponentTypeEnum.ComboYesNo)
      element[`Items`] = [new ComboInputValueModel<number>(0, MaterialPopupComponent.LabelNo), new ComboInputValueModel<number>(1, MaterialPopupComponent.LabelYes)];
    if (this.ComponentType === ComponentTypeEnum.ComboMonth)
      element[`Items`] = [new ComboInputValueModel<number>(1, `Janeiro`), new ComboInputValueModel<number>(2, `Feveriro`), new ComboInputValueModel<number>(3, `Março`), new ComboInputValueModel<number>(4, `Abril`), new ComboInputValueModel<number>(5, `Maio`), new ComboInputValueModel<number>(6, `Junho`), new ComboInputValueModel<number>(7, `Julho`), new ComboInputValueModel<number>(8, `Agosto`), new ComboInputValueModel<number>(9, `Setembro`), new ComboInputValueModel<number>(10, `Outubro`), new ComboInputValueModel<number>(11, `Novembro`), new ComboInputValueModel<number>(12, `Dezembro`)];

    if (!ObjectUtils.NullOrUndefined(customStyle))
      element.Style = customStyle;
    else
    {
      let styleDefinition: string = ``;
      if (ObjectUtils.GetValue(this.WidthPixels, 0) > 0)
      {
        element.FlexGrow = false;
        styleDefinition = `${styleDefinition} width: ${this.WidthPixels}px;`;
      }
      else if (ObjectUtils.GetValue(this.WidthPercentage, 0) > 0)
      {
        element.FlexGrow = false;
        styleDefinition = `${styleDefinition} width: ${this.WidthPercentage}%;`;
      }

      if (!ObjectUtils.NullUndefinedOrEmpty(styleDefinition))
      {
        styleDefinition = styleDefinition.trim();
        element.Style = sanitizer.bypassSecurityTrustStyle(styleDefinition);
      }
    }

    element.Columndefinition = JsonUtils.Clone(this);

    return (element as any) as T;
  }
  public Clone(): CrudableTableColumnDefinitionModel
  {
    const clone: CrudableTableColumnDefinitionModel = new CrudableTableColumnDefinitionModel(
      this.ColumnName,
      this.PropertyName,
      this.ReadableName,
      this.ReadableNamePlural,

      this.ComponentType,

      this.CanSort,
      this.CanFilter,
      this.CanInsert,
      this.CanSelect,
      this.CanUpdate,

      this.HasShadow,
      this.ComponentSubcategory,
      this.Transform,

      this.MaxWidth,
      this.WidthPixels,
      this.WidthPercentage,

      this.TextAlign,
      this.IsNullable,
      this.DefaultHeight,
    );

    return clone;
  }
}
