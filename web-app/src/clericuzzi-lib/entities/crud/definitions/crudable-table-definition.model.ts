// utils
import { ObjectUtils } from "clericuzzi-lib/utils/object-utils";

// models

// business
import { EntitiesList } from "clericuzzi-lib/business/behaviours/entities-list.model";

/**
 * Enumeration of posible genders
 */
export enum CrudableTableDefinitionGender
{
    Male = "m",
    Female = "f",
}

/**
 * This class defines the crud behaviour of a certain data type 
 */
export class CrudableTableDefinitionModel
{
    /**
     * 
     * @param ClassName the class's name
     * @param TableName the class's table name
     * @param TableGender the class's gender
     * @param ReadableName the class's readable name
     * @param ReadableNamePlural the class's plural name
     * @param CanSearch if the class's search panel is enabled
     * @param CanInsert if the class's insert panel is enabled
     * @param CanUpdate if the class's update panel is enabled
     * @param HasSelectionBox if the class's selection box is enabled
     */
    constructor(
        public ClassName: string,
        public TableName: string,
        public TableGender: CrudableTableDefinitionGender,
        public ReadableName: string,
        public ReadableNamePlural: string,
        
        public CanFilter: boolean = true,
        public CanSearch: boolean = true,
        public CanInsert: boolean = true,
        public CanDelete: boolean = true,
        public CanUpdate: boolean = true,
        public CanRefresh: boolean = true,
        public HasSelectionBox: boolean = true,
    )
    {   
    }

    public get ListTitle(): string
    {
        return `Lista de ${this.ReadableNamePlural.toLocaleLowerCase()}`;
    }
    public get NewItemTitle(): string
    {
        return `Nov${this.Article()} ${this.ReadableName.toLocaleLowerCase()}`;
    }
    public get UpdateItemTitle(): string
    {
        return `Alterar ${this.ReadableName.toLocaleLowerCase()}`;
    }

    /**
     * Defines the crud's initial sorting
     * @param field the field's name
     */
    public StartingSort(field: string): void
    {
    }
    /**
     * Returns the singular article for the table's gender
     * @param male the template for the male gender
     * @param female the template for the female gender
     */
    public Article(male: string = "o", female: string = "a"): string
    {
        return this.TableGender == CrudableTableDefinitionGender.Male ? male : female ;
    }
    /**
     * Returns the singular indefinite article for the table's gender
     * @param male the template for the male gender
     * @param female the template for the female gender
     */
    public Indefinite(male: string = "um", female: string = "uma"): string
    {
        return this.TableGender == CrudableTableDefinitionGender.Male ? male : female ;
    }
    /**
     * Returns the plural article for the table's gender
     * @param male the template for the male gender
     * @param female the template for the female gender
     */
    public ArticlePlural(male: string = "os", female: string = "as"): string
    {
        return this.TableGender == CrudableTableDefinitionGender.Male ? male : female ;
    }
    /**
     * Returns the plural indefinite article for the table's gender
     * @param male the template for the male gender
     * @param female the template for the female gender
     */
    public IndefinitePlural(male: string = "uns", female: string = "umas"): string
    {
        return this.TableGender == CrudableTableDefinitionGender.Male ? male : female ;
    }
}

/**
 * This class will dictate the custom actions performed by a crud component
 */
export class CrudableTableDefinitionActions extends EntitiesList<CrudableTableDefinitionActionModel>
{
    /**
     * Retrieves an item from the list by its name
     * @param itemName the item to be searched
     * @returns the item where it's name matches with the given one
     */
    public Get(itemName: string): CrudableTableDefinitionActionModel
    {
        let matchingItems: CrudableTableDefinitionActionModel[] = this.List.filter(i => i.Key == itemName);
        if (ObjectUtils.NullUndefinedOrEmptyArray(matchingItems))
            return null;
        else
            return matchingItems[0];
    }
    /**
     * Tests if the list contains the given field name
     * @param itemName the field to be searched
     * @returns TRUE if the field exists within the LIST
     */
    public Contains(itemName: string): boolean
    {
        itemName = itemName.toLocaleLowerCase();
        let matchingItems: CrudableTableDefinitionActionModel[] = this.List.filter(i => i.Key.toLocaleLowerCase() == itemName);
        return !ObjectUtils.NullUndefinedOrEmptyArray(matchingItems);
    }
}
/**
 * A custom action to be performed by user demand
 */
export class CrudableTableDefinitionActionModel
{
    /**
     * Default constructor
     * @param Key the action string key (must be unique for a particular crud)
     * @param Title the title displayed in the mouse over
     * @param Action the function that will be called when the item is clicked
     * @param MultiItem accepts one or n crudable items as its input parameter
     * @param IconClass the icon class
     * @param IconDefinition the icon definition
     * @param SelectedItemsNeeded the number of selected items that will trigger the item's showing
     */
    constructor(
        public Key: string = null,
        public Title: string = null,
        public Action: Function = null,
        public MultiItem: boolean = false,
        public IconClass: string = null,
        public IconDefinition: string = null,
        public SelectedItemsNeeded: number = 0,
    )
    {
    }

    public Execute(): void
    {
        if (this.Action != null)
            this.Action();
    }
}