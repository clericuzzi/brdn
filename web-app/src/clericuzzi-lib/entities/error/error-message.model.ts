﻿export class ErrorMessage
{
    public Message: string;
    public ErrorCode: number;
    public DeveloperMessage: string;
    public StackTraceMessage: string;
}