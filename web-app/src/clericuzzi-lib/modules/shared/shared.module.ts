import { NgModule } from '@angular/core';
import { MAT_DATE_LOCALE } from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';

// modules
import { MaterialModule } from 'clericuzzi-lib/modules/material/material.module';
import { ImageViewerModule } from "ngx-image-viewer";

// Components
import { UploadFileComponent } from './upload-file/upload-file.component';
import { WaitingLoaderComponent } from './waiting-loader/waiting-loader.component';

// form components
import { TextInputComponent } from './form-controls/text-input/text-input.component';
import { DateInputComponent } from './form-controls/date-input/date-input.component';
import { HourInputComponent } from './form-controls/custom-inputs/hour-input/hour-input.component';
import { ComboInputComponent } from './form-controls/custom-inputs/combo-input/combo-input.component';
import { NumberInputComponent } from './form-controls/number-input/number-input.component';
import { LabelControlComponent } from './form-controls/label-control/label-control.component';
import { SelectionBoxComponent } from './form-controls/selection-box/selection-box.component';
import { TextareaInputComponent } from './form-controls/textarea-input/textarea-input.component';
import { AccordionPanelComponent } from './accordion-panel/accordion-panel.component';
import { AutocompleteInputComponent } from './form-controls/autocomplete-input/autocomplete-input.component';

import { CrudSelectListComponent } from './crud/crud-select/crud-select-list/crud-select-list.component';
import { AccordionPanelController } from './accordion-panel/accordion-panel.controller';
import { UploadFileViewerComponent } from './upload-file/upload-file-viewer/upload-file-viewer.component';
import { AccordionPanelItemComponent } from './accordion-panel/accordion-panel-item/accordion-panel-item.component';
import { AutocompleteInputItemComponent } from './form-controls/autocomplete-input/autocomplete-input-item/autocomplete-input-item.component';

// crud
// crud select
import { CrudItemComponent } from './crud/crud-item/crud-item.component';
import { CrudPopupComponent } from './crud/crud-popup/crud-popup.component';
import { CrudPanelComponent } from './crud/crud-panel/crud-panel.component';
import { CrudFilterComponent } from './crud/crud-filter/crud-filter.component';
import { CrudSelectComponent } from './crud/crud-select/crud-select.component';
import { CrudSelectActionsComponent } from './crud/crud-select/crud-select-actions/crud-select-actions.component';
import { CrudSelectPaginationComponent } from './crud/crud-select/crud-select-pagination/crud-select-pagination.component';
import { CrudSelectActionsItemComponent } from './crud/crud-select/crud-select-actions/crud-select-actions-item/crud-select-actions-item.component';
import { CrudSelectPaginationPanelComponent } from './crud/crud-select/crud-select-pagination/crud-select-pagination-panel/crud-select-pagination-panel.component';
import { CrudSelectPaginationPanelButtonComponent } from './crud/crud-select/crud-select-pagination/crud-select-pagination-panel/crud-select-pagination-panel-button/crud-select-pagination-panel-button.component';

@NgModule({
  imports: [
    MaterialModule,
    ReactiveFormsModule,

    ImageViewerModule.forRoot(),
  ],
  providers: [
    // date location
    { provide: MAT_DATE_LOCALE, useValue: 'pt-BR' },
  ],
  exports: [
    MaterialModule,

    UploadFileComponent,
    WaitingLoaderComponent,

    DateInputComponent,
    HourInputComponent,
    TextInputComponent,
    ComboInputComponent,
    NumberInputComponent,
    LabelControlComponent,
    SelectionBoxComponent,
    TextareaInputComponent,
    AutocompleteInputComponent,
    AutocompleteInputItemComponent,

    AccordionPanelComponent,
    AccordionPanelController,
    AccordionPanelItemComponent,

    CrudSelectComponent,
    CrudSelectActionsComponent,
    CrudSelectPaginationComponent,
    CrudSelectActionsItemComponent,
    CrudSelectPaginationPanelComponent,
    CrudSelectPaginationPanelButtonComponent,

    CrudItemComponent,
    CrudPanelComponent,
    CrudPopupComponent,
    CrudFilterComponent,
    CrudSelectListComponent,
  ],
  declarations: [
    UploadFileComponent,
    WaitingLoaderComponent,

    DateInputComponent,
    HourInputComponent,
    TextInputComponent,
    ComboInputComponent,
    NumberInputComponent,
    LabelControlComponent,
    SelectionBoxComponent,
    TextareaInputComponent,
    AutocompleteInputComponent,
    AutocompleteInputItemComponent,

    AccordionPanelComponent,
    AccordionPanelController,
    AccordionPanelItemComponent,

    CrudSelectComponent,
    CrudSelectActionsComponent,
    CrudSelectPaginationComponent,
    CrudSelectActionsItemComponent,
    CrudSelectPaginationPanelComponent,
    CrudSelectPaginationPanelButtonComponent,

    UploadFileViewerComponent,

    CrudItemComponent,
    CrudPanelComponent,
    CrudPopupComponent,
    CrudFilterComponent,
    CrudSelectListComponent,
  ],
  entryComponents: [
    UploadFileComponent,

    DateInputComponent,
    HourInputComponent,
    TextInputComponent,
    ComboInputComponent,
    NumberInputComponent,
    LabelControlComponent,
    SelectionBoxComponent,
    TextareaInputComponent,
    AutocompleteInputComponent,
    AutocompleteInputItemComponent,

    AccordionPanelComponent,
    AccordionPanelController,
    AccordionPanelItemComponent,

    CrudSelectComponent,
    CrudSelectActionsComponent,
    CrudSelectPaginationComponent,
    CrudSelectActionsItemComponent,
    CrudSelectPaginationPanelComponent,
    CrudSelectPaginationPanelButtonComponent,

    CrudItemComponent,
    CrudPopupComponent,
  ]
})
export class SharedModule { }