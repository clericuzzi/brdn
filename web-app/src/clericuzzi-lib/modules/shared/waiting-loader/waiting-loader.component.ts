import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { InputDecoration } from 'clericuzzi-lib/utils/input-decoration';

@Component({
  selector: 'app-waiting-loader',
  templateUrl: './waiting-loader.component.html',
  styleUrls: ['./waiting-loader.component.css']
})
export class WaitingLoaderComponent implements OnInit
{
  @ViewChild(`loaderComponent`) private _Component: ElementRef;
  constructor() { }

  ngOnInit()
  {
  }

  public Init(): void
  { 
  }
  public Hide(): void
  {
    InputDecoration.AddClass(this._Component, 'waiting-loader-body-fade-out');
    InputDecoration.RemoveClass(this._Component, 'waiting-loader-body-fade-in');
    setTimeout(() => InputDecoration.AddClass(this._Component, 'waiting-loader-body-hide'), 300);
  }
  public Show(): void
  {
    InputDecoration.RemoveClass(this._Component, 'waiting-loader-body-hide');
    setTimeout(() => InputDecoration.RemoveClass(this._Component, 'waiting-loader-body-fade-in'), 50);
    setTimeout(() => InputDecoration.RemoveClass(this._Component, 'waiting-loader-body-fade-out'), 150);
  }
}