export class UploadFileModel
{
    constructor(
        public Reader: FileReader,
        public File: File,
        public Base64: string,
        public TextData: string[],
        public FileData: Uint8Array) {}
}