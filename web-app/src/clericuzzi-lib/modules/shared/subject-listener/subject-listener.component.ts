// definitions
import { Subject } from 'rxjs';
import { takeWhile } from 'rxjs/operators';
import { OnDestroy } from '@angular/core';

// models
import { CustomFieldable } from 'clericuzzi-lib/entities/crud/custom-fieldable.model';
import { SubjectListenerTimeoutLoopModel } from './subject-listener-timeout-loop.model';

export class SubjectListenerComponent extends CustomFieldable implements OnDestroy
{
    public OnDestroyed: Subject<void> = new Subject<void>();
    private _LoopModels: SubjectListenerTimeoutLoopModel[] = [];
    
    public IsAlive: boolean = true;
    public ActiveListeners: Subject<any>[] = [];

    constructor()
    {
        super();
    }
    ngOnDestroy(): void
    {
        this.OnDestroyed.next();

        this.IsAlive = false;
    }

    private _CheckSubject(subject: Subject<any>): boolean
    {
        let index: number = this.ActiveListeners.indexOf(subject);
        let value: boolean = this.IsAlive && index >= 0;

        return value;
    }
    public ListenTo<T>(subject: Subject<T>, next?: (value: T) => void, error?: (any: T) => T, complete?: () => T): void
    {
        this.ActiveListeners.push(subject);
        subject.pipe(takeWhile(() => this._CheckSubject(subject))).subscribe(next, error, complete);
    }
    public StopListeningTo<T>(subject: Subject<T>): void
    {
        // let index: number = this.ActiveListeners.indexOf(subject);
        // if (index >= 0)
        //     this.ActiveListeners.splice(index, 1);
        subject.complete();
    }
    public ResumeListeningTo<T>(subject: Subject<T>): void
    {
        let index: number = this.ActiveListeners.indexOf(subject);
        if (index < 0)
            this.ActiveListeners.push(subject);
    }

    public NewLoop(name: string, action: Function, timeout: number): void
    {
        var currentLoops: SubjectListenerTimeoutLoopModel[] = this._LoopModels.filter(l => l.Name == name);
        if (currentLoops != null && currentLoops != undefined && currentLoops.length == 0)
        {
            let currentLoop: SubjectListenerTimeoutLoopModel = new SubjectListenerTimeoutLoopModel();
            currentLoop.Name = name;
            currentLoop.IsAlive = true;
            currentLoop.Action = action;
            currentLoop.Timeout = timeout;
            this._LoopModels.push(currentLoop);

            this._Loop(currentLoop);
        }
    }
    public KillLoop(name: string): void
    {
        var currentLoops: SubjectListenerTimeoutLoopModel[] = this._LoopModels.filter(l => l.Name == name);
        if (currentLoops != null && currentLoops != undefined && currentLoops.length == 1)
        {
            let loopModel: SubjectListenerTimeoutLoopModel = currentLoops[0];
            loopModel.IsAlive = false;
            let index: number = this._LoopModels.indexOf(loopModel);
            if (index >= 0)
                this._LoopModels.splice(index, 1);
        }
    }
    private _Loop(loopModel: SubjectListenerTimeoutLoopModel): void
    {
        if (loopModel != null && loopModel != undefined)
        {
            if (loopModel.IsAlive && this.IsAlive)
            {
                loopModel.Action();
                setTimeout(() => this._Loop(loopModel), loopModel.Timeout);
            }
            else
            {
                let index: number = this._LoopModels.indexOf(loopModel);
                if (index >= 0)
                    this._LoopModels.splice(index, 1);
            }
        }
    }
}