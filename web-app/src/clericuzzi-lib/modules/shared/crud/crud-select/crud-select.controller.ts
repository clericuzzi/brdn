// utils
import { Dictionary } from 'clericuzzi-lib/utils/dictionary-utils';

// models
import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model';

export class CrudSelectController
{
    public static RootEndpoint: string;
    public static ListingEndpoint: string;
    public static CrudPaginationButtonClass: string;
    public static CrudPaginationButtonClassSelected: string;

    private static _Dictionary: Dictionary<Crudable> = new Dictionary<Crudable>();
    public static Add(key: string, item: any): void
    {
        CrudSelectController._Dictionary.Add(key, item);
    }
    public static GetBaseObject(key: string): Crudable
    {
        return CrudSelectController._Dictionary.Get(key);
    }
}