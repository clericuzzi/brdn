import { Subject } from 'rxjs';
import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';

// utils
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { InputDecoration } from 'clericuzzi-lib/utils/input-decoration';

// models
import { CrudableTableDefinitionActionModel } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';

@Component({
  selector: 'app-crud-select-actions-item',
  templateUrl: './crud-select-actions-item.component.html'
})
export class CrudSelectActionsItemComponent implements OnInit
{
  public OnItemActivated: Subject<CrudableTableDefinitionActionModel> = new Subject<CrudableTableDefinitionActionModel>();
  public get Key(): string
  {
    if (ObjectUtils.NullOrUndefined(this.Action))
      return null;
    else
      return this.Action.Key;
  }
  private _IsActive: boolean = false;
  private _IsVisible: boolean = false;
  public Disabled: boolean = false;

  @ViewChild(`item`) ButtonBody: ElementRef;
  @Input() Action: CrudableTableDefinitionActionModel;

  constructor()
  {
  }

  public Enable(): void
  {
    this.Disabled = false;
    this.Action.IconClass = StringUtils.RemoveClasses(this.Action.IconClass, `fa-spin`);
  }
  public Disable(): void
  {
    this.Disabled = true;
    this.Action.IconClass = StringUtils.AddClasses(this.Action.IconClass, `fa-spin`);
  }

  ngOnInit()
  {
  }

  public ClickAction(): void
  {
    this.OnItemActivated.next(this.Action);
  }
  public CheckDisplay(itemsSelected: number): void
  {
    if (itemsSelected >= this.Action.SelectedItemsNeeded)
    {
      if (!this.Action.MultiItem)
      {
        if (itemsSelected > 1)
          this.Hide();
        else
          this.Show();
      }
      else
        this.Show();
    }
    else
      this.Hide();
  }

  public Hide(): void
  {
    if (this._IsVisible)
    {
      if (!ObjectUtils.NullOrUndefined(this.ButtonBody, this.ButtonBody[`_elementRef`]))
      {
        this._IsVisible = false;
        InputDecoration.ElementFadeOut(this.ButtonBody[`_elementRef`]);
      }
    }
  }
  public Show(): void
  {
    if (!this._IsVisible)
    {
      if (!ObjectUtils.NullOrUndefined(this.ButtonBody, this.ButtonBody[`_elementRef`]))
      {
        this._IsVisible = true;
        InputDecoration.ElementFadeIn(this.ButtonBody[`_elementRef`]);
      }
    }
  }
}