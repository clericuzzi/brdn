import { Component, OnInit, ViewChild } from '@angular/core';

// utils
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';

// models
import { RequestPagination } from 'clericuzzi-lib/entities/request/pagination/request-pagination.model';

// components
import { CrudSelectPaginationPanelComponent } from './crud-select-pagination-panel/crud-select-pagination-panel.component';

@Component({
  selector: 'app-crud-select-pagination',
  templateUrl: './crud-select-pagination.component.html',
  styleUrls: ['./crud-select-pagination.component.css']
})
export class CrudSelectPaginationComponent implements OnInit
{
  @ViewChild(`paginationPanel`) PaginationPanel: CrudSelectPaginationPanelComponent;

  public Busy: boolean = false;

  public Page: number;
  public Pages: number;
  public ItemsCount: number;
  public TotalItems: number;
  public Pagination: RequestPagination;

  constructor()
  {
  }

  public get Message(): string
  {
    if (!ObjectUtils.NullOrUndefined(this.Page, this.TotalItems))
      return `Página ${StringUtils.NumberWithCommas(this.Page)} de ${StringUtils.NumberWithCommas(this.Pages)} (${this.ItemsCount} itens de ${StringUtils.NumberWithCommas(this.TotalItems)})`;
    else
      return ``;
  }
  public get MessageClass(): string
  {
    if (this.ItemsCount == 0)
      return `color-text-light`;
    else
      return `color-text`;
  }

  ngOnInit()
  {
  }

  public Clear(): void
  {
    this.ItemsCount = this.TotalItems = null;
    this.PaginationPanel.Clear();

    this.Busy = true;
  }
  public Remove(amount: number): void
  {
    this.ItemsCount = this.ItemsCount - amount;
    this.TotalItems = this.TotalItems - amount;
  }
  public DrawResults(pagination: RequestPagination, itemsCount: number): void
  {
    if (!ObjectUtils.NullOrUndefined(pagination))
      this.Pagination = pagination;

    this.Page = pagination.PageIndex + 1;
    this.Pages = pagination.Pages;
    this.ItemsCount = itemsCount;
    this.TotalItems = pagination.TotalCount;
    this.PaginationPanel.Update(pagination);

    this.Busy = false;
  }
}