import { Subject } from 'rxjs';
import { Component, OnInit } from '@angular/core';

// definitions
import { CrudSelectController } from 'clericuzzi-lib/modules/shared/crud/crud-select/crud-select.controller';

@Component({
  selector: 'app-crud-select-pagination-panel-button',
  templateUrl: './crud-select-pagination-panel-button.component.html'
})
export class CrudSelectPaginationPanelButtonComponent implements OnInit
{
  public OnIndexClicked: Subject<number> = new Subject<number>();
  public Index: number;
  public Label: string;
  
  public ElementClass: string;

  public IsSelected: boolean = false;
  public IsLastElement: boolean = false;
  public IsNextElement: boolean = false;
  public IsFirstElement: boolean = false;
  public IsPreviousElement: boolean = false;
  constructor()
  {
  }

  ngOnInit()
  {
  }

  public SetIndex(index: number, label: string, isFirst: boolean, isPrevious: boolean, isNext: boolean, isLast: boolean, isSelected: boolean): void
  {
    this.Index = index;
    this.Label = label;
    this.IsSelected = isSelected;
    this.IsFirstElement = isFirst;
    this.IsPreviousElement = isPrevious;
    this.IsNextElement = isNext;
    this.IsLastElement = isLast;

    this.ElementClass = isSelected ? CrudSelectController.CrudPaginationButtonClassSelected : CrudSelectController.CrudPaginationButtonClass;
  }
  public GoToIndex(): void
  {
    if (!this.IsSelected)
    {
      this.OnIndexClicked.next(this.Index);
      this.ElementClass = CrudSelectController.CrudPaginationButtonClassSelected;
    }
  }
}