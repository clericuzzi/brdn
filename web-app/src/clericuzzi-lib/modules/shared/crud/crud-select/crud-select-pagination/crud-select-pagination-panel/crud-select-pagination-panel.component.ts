import { Subject } from 'rxjs';
import { Component, OnInit, ViewChild, ViewContainerRef, ComponentFactoryResolver } from '@angular/core';

// models
import { RequestPagination, RequestPaginationDefaults } from 'clericuzzi-lib/entities/request/pagination/request-pagination.model';
import { ComboInputValueModel } from 'clericuzzi-lib/modules/shared/form-controls/custom-inputs/combo-input/combo-input-value.model';

// components
import { ComboInputComponent } from 'clericuzzi-lib/modules/shared/form-controls/custom-inputs/combo-input/combo-input.component';
import { SubjectListenerComponent } from 'clericuzzi-lib/modules/shared/subject-listener/subject-listener.component';
import { CrudSelectPaginationPanelButtonComponent } from './crud-select-pagination-panel-button/crud-select-pagination-panel-button.component';
import { HostBindedComponent } from 'clericuzzi-lib/modules/shared/util-components/host-binded/host-binded.component';
import { InputDecoration } from 'clericuzzi-lib/utils/input-decoration';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

@Component({
  selector: 'app-crud-select-pagination-panel',
  templateUrl: './crud-select-pagination-panel.component.html',
  styleUrls: ['./crud-select-pagination-panel.component.css']
})
export class CrudSelectPaginationPanelComponent extends HostBindedComponent implements OnInit
{
  public OnPageRequested: Subject<RequestPagination> = new Subject<RequestPagination>();
  public OnPageSizeChanged: Subject<RequestPagination> = new Subject<RequestPagination>();
  
  @ViewChild(`paginationSize`) PaginationSizeCombo: ComboInputComponent<number>;
  @ViewChild('buttonsContainer', { read: ViewContainerRef }) _ContainerItems: ViewContainerRef;

  public Pagination: RequestPagination;
  constructor(
    private _FactoryResolver: ComponentFactoryResolver,
  )
  {
    super(null);
    this.ClearComponentClass();
  }

  ngOnInit()
  {
    this.Pagination = new RequestPagination();
    this.PaginationSizeCombo.Model = this.Pagination;
    this.PaginationSizeCombo.Property = `PageSize`;
    this.PaginationSizeCombo.Items = [ new ComboInputValueModel<number>(10, `10`), new ComboInputValueModel<number>(25, `25`), new ComboInputValueModel<number>(50, `50`), new ComboInputValueModel<number>(100, `100`) ];
    this.PaginationSizeCombo.CanClear = false;
    this.PaginationSizeCombo.HasShadow = false;
    this.PaginationSizeCombo.MarginBottom = 0;
    this.PaginationSizeCombo.SetValue(RequestPaginationDefaults.PageSize, true);

    this.ListenTo(this.PaginationSizeCombo.OnValueChanged, () => this._PageSizeChanged());
  }
  private _PageSizeChanged(): void
  {
    if (!ObjectUtils.NullOrUndefined(this.PaginationSizeCombo.Value) && this.Pagination.PageSize != this.PaginationSizeCombo.Value)
    {
      this.Pagination.PageSize = this.PaginationSizeCombo.Value;
      this.OnPageSizeChanged.next(this.Pagination);
    }
  }
  private _DrawButtons(): void
  {
    InputDecoration.BindingShow(this);
    this._ContainerItems.clear();
    let i: number = this.Pagination.PageIndex - 1;
    if (i < 1)
      i = 1;
    
    if (this.Pagination.PageIndex > 2)
      this._DrawButton(-4, `<<`, true, false, false, false);
    if (this.Pagination.PageIndex > 0)
      this._DrawButton(-3, `<`, false, true, false, false);

    let index: number = 0;    
    for (i; this._ValidateLoop(i, index); i++)
    {
      this._DrawButton(i, i.toString(), false, false, false, false);
      index++;
    }

    if (this.Pagination.PageIndex < this.Pagination.Pages - 1)
      this._DrawButton(-2, `>`, false, false, true, false);
    if (this.Pagination.PageIndex < this.Pagination.Pages - 2)
      this._DrawButton(-1, `>>`, false, true, false, true);
  }
  private _DrawButton(index: number, label: string, isFirst: boolean, isPrevious: boolean, isNext: boolean, isLast: boolean): void
  {
    let element: CrudSelectPaginationPanelButtonComponent;
    let factory = this._FactoryResolver.resolveComponentFactory(CrudSelectPaginationPanelButtonComponent);
    let component = factory.create(this._ContainerItems.parentInjector);
    this._ContainerItems.insert(component.hostView);
  
    element = component['_component'];
    element.SetIndex(index, label, isFirst, isPrevious, isNext, isLast, (this.Pagination.PageIndex + 1) == index);
    this.ListenTo(element.OnIndexClicked, index => this.Page(index));
  }
  private _ValidateLoop(currentPage: number, pageIndex: number): boolean
  {
    if (pageIndex <= 4)
    {
      if (currentPage > this.Pagination.Pages)
        return false;
      else
        return true;
    }
    else
      return false;
  }
  
  public Clear(): void
  {
    this._ContainerItems.clear();
  }
  public Update(pagination: RequestPagination): void
  {
    this.Pagination = pagination;
    this._DrawButtons();
  }
  
  public Page(page: number): void
  {
    if (page < 0)
    {
      if (page == -4)
        page = 0;
      else if (page == -3)
        page = this.Pagination.PageIndex - 1;
      else if (page == -2)
        page = this.Pagination.PageIndex + 1;
      else if (page == -1)
        page = this.Pagination.Pages - 1;
        
      this.Pagination.PageIndex = page;
    }
    else
      this.Pagination.PageIndex = page - 1;
    
    InputDecoration.BindingHide(this);
    this.OnPageRequested.next(this.Pagination);
  }
  public PageNext(): void { this.Page(this.Pagination.PageIndex++); }
  public PageLast(): void { this.Page(this.Pagination.Pages - 1); }
  public PageFirst(): void { this.Page(1); }
  public PagePrevious(): void { this.Page(this.Pagination.PageIndex--); }
}