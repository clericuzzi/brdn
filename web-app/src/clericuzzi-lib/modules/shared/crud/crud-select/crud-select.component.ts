import { Subject } from 'rxjs';
import { ComponentType } from '@angular/cdk/portal';
import { MatSnackBar, MatDialog } from '@angular/material';
import { Component, OnInit, ViewChild, HostBinding, ElementRef, ViewContainerRef, ChangeDetectorRef } from '@angular/core';

// utils
import { JsonUtils } from 'clericuzzi-lib/utils/json-utils';
import { SnackUtils } from 'clericuzzi-lib/utils/snack-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';
import { InputDecoration } from 'clericuzzi-lib/utils/input-decoration';

// models
import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model';
import { RequestMessage } from 'clericuzzi-lib/entities/request/request-message.model';
import { CustomFieldable } from 'clericuzzi-lib/entities/crud/custom-fieldable.model';
import { RequestPagination } from 'clericuzzi-lib/entities/request/pagination/request-pagination.model';
import { StatusResponseMessage } from 'clericuzzi-lib/entities/response/status-response-message.model';
import { CrudableTableDefinitionActionModel } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-definition.model';
import { BatchResponseMessage, BatchResponseMessageList } from 'clericuzzi-lib/entities/response/batch-response-message.model';
import { CrudableTableColumnDefinitionModel, ComponentTypeEnum } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';

// behaviour
import { SelectListItem } from 'clericuzzi-lib/business/behaviours/select-list-item.behaviour';

// components
import { TextInputComponent } from '../../form-controls/text-input/text-input.component';
import { CrudSelectController } from './crud-select.controller';
import { MaterialPopupComponent } from 'clericuzzi-lib/modules/material/components/material-popup/material-popup.component';
import { CrudSelectListComponent } from './crud-select-list/crud-select-list.component';
import { SubjectListenerComponent } from 'clericuzzi-lib/modules/shared/subject-listener/subject-listener.component';
import { CrudSelectActionsComponent } from './crud-select-actions/crud-select-actions.component';
import { CrudSelectPaginationComponent } from './crud-select-pagination/crud-select-pagination.component';

// services
import { HttpService } from 'clericuzzi-lib/services/http.service';
import { LoaderService } from 'clericuzzi-lib/services/loader.service';
import { ComponentGeneratorService } from 'clericuzzi-lib/services/component-generator.service';

// business

// controllers

@Component({
  selector: 'app-crud-select',
  templateUrl: './crud-select.component.html',
  styleUrls: ['./crud-select.component.css']
})
export class CrudSelectComponent<T extends Crudable> extends SubjectListenerComponent implements OnInit
{
  private _ActionsInitialized: boolean = false;

  public OnDataRequested: Subject<void> = new Subject<void>();
  public OnSelectCallback: Subject<void> = new Subject<void>();
  public OnSelectionChanged: Subject<Crudable[]> = new Subject<Crudable[]>();
  public OnCustomSelectCallback: Subject<BatchResponseMessage<T>> = new Subject<BatchResponseMessage<T>>();
  public OnCustomSelectCallbackList: Subject<BatchResponseMessageList> = new Subject<BatchResponseMessageList>();

  private _CustomRequestValues: CustomFieldable = new CustomFieldable();
  private _CustomRequestValuesForDelete: CustomFieldable = new CustomFieldable();

  private _Pagination: RequestPagination = new RequestPagination();

  public SortFuncion: Function;
  private _FilterValue: string = ``;
  private _MultiSelect: boolean = true;

  private _ShowFilter: boolean = true;
  private _ShowLoader: boolean = false;
  private _ClearsDataOnFetch: boolean = false;
  private _LoadsCallbackList: boolean = false;

  public Title: string;
  public BaseObject: T;
  public ActionItems: CrudableTableDefinitionActionModel[] = [];
  public LoadsOnStart: boolean = true;
  public ShowsNoResultsMessage: boolean = true;
  public SelectedItems: Crudable[];

  public ShowHeaderTitle: boolean = true;
  public ShowsHeaderAction: boolean = true;
  public DataTemplate: ComponentType<SelectListItem<T>>;
  public CustomSelect: string;
  public CustomInsert: string;
  public CustomUpdate: string;
  public CustomDelete: string;

  @ViewChild('filterContainer', { read: ViewContainerRef }) private _FilterContainer: ViewContainerRef;
  @ViewChild('paginationContainer', { read: ViewContainerRef }) PaginationContainer: ViewContainerRef;

  @HostBinding(`class`) ContainerClass: string = `flex-column full-size`;

  @ViewChild(`selectContainer`) Container: ElementRef;
  @ViewChild(`componentContainer`) ComponentContainer: ElementRef;

  @ViewChild(`list`) List: CrudSelectListComponent<T>;
  @ViewChild(`pagination`) Pagination: CrudSelectPaginationComponent;

  @ViewChild(`actions`) Actions: CrudSelectActionsComponent;

  public ShowsFilter(show: boolean = true): void
  {
    this._ShowFilter = show;
  }
  public ShowsLoader(show: boolean = true): void
  {
    this._ShowLoader = show;
  }
  public UseCallbackList(): void
  {
    this._LoadsCallbackList = true;
  }
  public ClearsBeforeFetch(): void
  {
    this._ClearsDataOnFetch = true;
  }

  public get SelectedItem(): Crudable
  {
    if (!ObjectUtils.NullUndefinedOrEmptyArray(this.SelectedItems))
      return ObjectUtils.First(this.SelectedItems);
    else
      return null;
  }
  public get Items(): T[]
  {
    return this.List.Items as T[];
  }
  public get ItemsCount(): number
  {
    if (ObjectUtils.NullOrUndefined(this.List.Items))
      return 0;
    else
      return ObjectUtils.GetValue(this.List.Items.length, 0);
  }

  public DetectChanges(): void
  {
    this._ChangeDetector.detectChanges();
  }

  public get HasTemplate(): boolean { return !ObjectUtils.NullOrUndefined(this.DataTemplate); }

  public get UrlSelect(): string { return ObjectUtils.NullUndefinedOrEmpty(this.CustomSelect) ? `${CrudSelectController.RootEndpoint}crud/${this.BaseObject.TableDefinition.TableName}/filter` : this.CustomSelect; }
  public get UrlInsert(): string { return ObjectUtils.NullUndefinedOrEmpty(this.CustomInsert) ? `${CrudSelectController.RootEndpoint}crud/${this.BaseObject.TableDefinition.TableName}/insert` : this.CustomInsert; }
  public get UrlUpdate(): string { return ObjectUtils.NullUndefinedOrEmpty(this.CustomUpdate) ? `${CrudSelectController.RootEndpoint}crud/${this.BaseObject.TableDefinition.TableName}/update` : this.CustomUpdate; }
  public get UrlDelete(): string { return ObjectUtils.NullUndefinedOrEmpty(this.CustomDelete) ? `${CrudSelectController.RootEndpoint}crud/${this.BaseObject.TableDefinition.TableName}/delete` : this.CustomDelete; }

  constructor(
    private _Http: HttpService,
    private _Snack: MatSnackBar,
    private _Dialog: MatDialog,
    private _Loader: LoaderService,
    private _Generator: ComponentGeneratorService,
    private _ChangeDetector: ChangeDetectorRef,
  )
  {
    super();
  }

  ngOnInit()
  {
    this.ListenTo(this.Pagination.PaginationPanel.OnPageRequested, pagination => this._UpdatePagination(pagination));
    this.ListenTo(this.Pagination.PaginationPanel.OnPageSizeChanged, pagination => this._PaginationSize(pagination));

    setTimeout(() => this._DelayedInitialization(), 500);
  }

  private _DelayedInitialization(): void
  {
    if (this._ShowFilter)
      this._InitFilter();
    if (this._InitComponents())
      if (this.LoadsOnStart)
        this._FetchData();
  }
  private _InitFilter(): void
  {
    if (ObjectUtils.NullOrUndefined(this.BaseObject))
      return;

    const column: CrudableTableColumnDefinitionModel = new CrudableTableColumnDefinitionModel(`filter`, `_FilterValue`, `Filtro`, `Filtros`, ComponentTypeEnum.Text);
    const filterComponent: TextInputComponent = column.CreateComponent(this._FilterContainer, this._Generator.Factory, null, null, this._Generator.Sanitizer);
    filterComponent.Required = false;
    filterComponent.ShowWarning = false;
    filterComponent.Placeholder = `Filtrar resultados`;
    filterComponent.AddClasses(`flex-grow`, `margin-both`);

    filterComponent.Update(this);

    this.ListenTo(filterComponent.OnSend, () => this._FilterAction());
  }
  private _InitActionPanel(): void
  {
    this.InitActions(this.BaseObject);
  }
  private _InitComponents(): boolean
  {
    if (ObjectUtils.NullOrUndefined(this.BaseObject))
    {
      if (this.IsAlive)
        setTimeout(() => this._DelayedInitialization(), 250);

      return false;
    }

    this._Pagination = new RequestPagination();

    this._InitList();
    this._InitActionPanel();
    this.SetMultiSelect(this._MultiSelect);

    if (!ObjectUtils.NullOrUndefined(this.BaseObject) && !ObjectUtils.NullOrUndefined(this.Actions))
    {
      this.ListenTo(this.Actions.OnDelete, () => this._Delete());
      this.ListenTo(this.Actions.OnRefresh, () => this._FetchData());
      this.ListenTo(this.Actions.OnActionInvoked, action => this._CustomAction(action));
    }

    return true;
  }
  private _InitList(): void
  {
    this.ListenTo(this.List.OnSelectionChanged, selectedItems => this._ToggleBodySelection(selectedItems));
  }
  public InitActions(baseObject: T = null): void
  {
    if (ObjectUtils.NullOrUndefined(baseObject))
      baseObject = this.BaseObject.Clone() as T;

    this.DetectChanges();
    if (!ObjectUtils.NullOrUndefined(this.Actions) && !this._ActionsInitialized)
    {
      this.Actions.Init(baseObject);
      this.Actions.ListenTo(this.OnDataRequested, () => this.Actions.CannotRefrensh());
      this.Actions.ListenTo(this.OnSelectCallback, () => this.Actions.CanRefresh());
      this.Actions.ListenTo(this.OnCustomSelectCallback, () => this.Actions.CanRefresh());
      this.Actions.ListenTo(this.OnCustomSelectCallbackList, () => this.Actions.CanRefresh());
      this._ActionsInitialized = true;
    }

    if (!ObjectUtils.NullOrUndefined(this.Actions) && !ObjectUtils.NullUndefinedOrEmptyArray(this.ActionItems))
    {
      this.ActionItems = this.ActionItems.reverse();
      for (const actionItem of this.ActionItems)
        this.Actions.NewAction(actionItem, 0);

      this.ActionItems = this.ActionItems.reverse();
    }
  }
  public DrawActions(): void
  {
    if (!ObjectUtils.NullOrUndefined(this.Actions))
    {
      this.Actions.Clear();
      this.InitActions(this.BaseObject);
    }
  }

  public SetBaseObject(baseObject: T): void
  {
    this.BaseObject = baseObject;

    this.DetectChanges();
  }

  private _ToggleBodySelection(selectedItems: Crudable[]): void
  {
    this.SelectedItems = selectedItems;
    if (ObjectUtils.NullUndefinedOrEmptyArray(this.SelectedItems))
    {
      if (!ObjectUtils.NullOrUndefined(this.Actions))
        this.Actions.SelectionUpdated(0);
    }
    else
    {
      if (!ObjectUtils.NullOrUndefined(this.Actions))
        this.Actions.SelectionUpdated(this.SelectedItems.length);
    }

    this.OnSelectionChanged.next(this.SelectedItems);
  }

  public LoaderHide(): void { this._Loader.Hide(); }
  public LoaderShow(): void { this._Loader.Show(); }

  private _FilterAction(): void
  {
    const items: T[] = this.Items;
    if (ObjectUtils.NullUndefinedOrEmpty(this._FilterValue))
      this.SetItems(this.Items, this.Pagination.Pagination);
    else
      this.SetItems(this.Items.filter(i => this._FilterActionItem(i, this._FilterValue)), this.Pagination.Pagination);

    this.List.Items = items;
  }
  private _FilterActionItem(item: T, filterValue: string): boolean
  {
    return item.Filter(filterValue);
  }

  public SelectItems(indexes: number[]): void
  {
  }
  public AddItems(newItems: Crudable[], baseObject: Crudable = null): void
  {
    const items: Crudable[] = this.List.Items;
    if (ObjectUtils.NullOrUndefined(baseObject))
      this.SetItems(newItems.concat(items));
    else
    {
      let entities: Crudable[] = JsonUtils.ParseList(newItems, baseObject);
      entities = items.concat(entities);
      this.SetItems(entities);
    }
  }
  public SetItems(data: Crudable[], pagination: RequestPagination = null): void
  {
    if (ObjectUtils.NullOrUndefined(pagination))
      pagination = new RequestPagination();

    this.List.SetItems(data as T[], this.DataTemplate);
    this.Pagination.DrawResults(pagination, data.length);
  }
  public RemoveItems(ids: number[]): void
  {
    if (!ObjectUtils.NullOrUndefined(this.List))
      this.List.RemoveItems(ids);
  }

  public ClearPagination(): void
  {
    this.Pagination.DrawResults(new RequestPagination(), 0);
  }

  private _FetchData(customUrl: string = null): void
  {
    if (this._ShowLoader)
      this._Loader.Show();

    if (!this._ShowFilter)
      this._FilterContainer.clear();

    if (this._ClearsDataOnFetch)
      this.ClearData();

    this.Pagination.Busy = true;
    this.OnDataRequested.next();
    this.OnSelectionChanged.next([]);
    this.List.OnSelectionChanged.next([]);

    const url: string = ObjectUtils.NullOrUndefined(customUrl) ? this.UrlSelect : customUrl;
    const message: RequestMessage = new RequestMessage(url);
    message.Pagination = this._Pagination;
    if (this._CustomRequestValues.Count > 0)
      for (const customField of this._CustomRequestValues.CustomFields)
        message.AddRaw(customField.Key, customField.Value);

    const response: BatchResponseMessage<T> = new BatchResponseMessage<T>();
    if (!ObjectUtils.NullUndefinedOrEmpty(this.CustomSelect) || !ObjectUtils.NullUndefinedOrEmpty(customUrl))
    {
      if (this._LoadsCallbackList)
      {
        const responseList: BatchResponseMessageList = new BatchResponseMessageList();
        this._Http.Post(message, responseList, () => this.OnCustomSelectCallbackList.next(responseList));
      }
      else if (!ObjectUtils.NullUndefinedOrEmptyArray(this.OnCustomSelectCallback.observers))
        this._Http.Post(message, response, () => this.OnCustomSelectCallback.next(response));
    }
    else
      this._Http.Post(message, response, () => this.Callback(response, this.BaseObject));
  }

  private _PaginationSize(pagination: RequestPagination): void
  {
    pagination.PageIndex = 0;
    this._Pagination = pagination;
    this._FetchData();
  }

  private _UpdatePagination(pagination: RequestPagination): void
  {
    this._Pagination = pagination;
    this._FetchData();
  }

  /**
   * Called from the actions panel
   * Prompts a delete confirmation for all selected items
   */
  public _Delete(): void
  {
    if (!ObjectUtils.NullUndefinedOrEmptyArray(this.SelectedItems))
      MaterialPopupComponent.Confirmation(this._Dialog, this._DeleteAction.bind(this), null, `Confirmação`, `Deseja remover os itens selecionados?`);
  }
  /**
   * Method called once the user confirms the deletion of the selected items
   */
  private _DeleteAction(): void
  {
    const ids: number[] = this.SelectedItems.map(i => i[this.BaseObject.PkProperty]).filter(i => !ObjectUtils.NullOrUndefined(i));
    if (!ObjectUtils.NullUndefinedOrEmptyArray(ids))
    {
      const response: StatusResponseMessage = new StatusResponseMessage();
      const message: RequestMessage = new RequestMessage(this.UrlDelete);
      message.AddEntities(ids);
      if (this._CustomRequestValuesForDelete.Count > 0)
        for (const customField of this._CustomRequestValuesForDelete.CustomFields)
          message.AddRaw(customField.Key, customField.Value);

      this._Loader.Show();
      this._Http.Post(message, response, () => this._DeleteCallback(response, ids));

      this.SelectedItems = [];
      this._ToggleBodySelection([]);
    }
  }
  private _DeleteCallback(response: StatusResponseMessage, ids: number[]): void
  {
    this._Loader.Hide();
    if (response.Success)
    {
      if (ObjectUtils.NullOrUndefined(response.ErrorMessage))
      {
        if (!ObjectUtils.NullOrUndefined(this.List))
          this.List.RemoveItems(ids);
        this.Pagination.Remove(ids.length);

        if (!ObjectUtils.NullUndefinedOrEmptyArray(ids))
        {
          if (ids.length > 1)
            this._Snack.open(`Itens removidos com sucesso`, `Sucesso`);
          else
            this._Snack.open(`Item removido com sucesso`, `Sucesso`);
        }
      }
      else
      {
        this._Snack.open(response.ErrorMessage, `Alerta`);
        this._FetchData();
      }
    }
    else
      this._Snack.open(response.ErrorMessage, `Erro`);
  }

  /**
   * Reloads the data with the current pagination, filter and sorting settings
   */
  public Reload(): void
  {
    this._FetchData();
  }

  /**
   * Creates a new item within the actions panel based on a given action model
   * @param actionModel the action model to be added to the actions panel
   */
  public AddAction(key: string, title: string, action: Function, multiItem: boolean, iconClass: string, iconDefinition: string, selectedItemsNeeded: number): void
  {
    if (!ObjectUtils.NullOrUndefined(this.Actions))
      this.ActionItems.push(new CrudableTableDefinitionActionModel(key, title, action, multiItem, iconClass, iconDefinition, selectedItemsNeeded));
  }
  /**
   * Creates a new item within the actions panel based on a given action model
   * @param actionModel the action model to be added to the actions panel
   */
  public NewAction(actionModel: CrudableTableDefinitionActionModel): void
  {
    if (!ObjectUtils.NullOrUndefined(this.Actions))
      this.ActionItems.push(actionModel);
  }
  /**
   * Calls an user defined action from the actions panel
   * @param action the action to be called
   */
  private _CustomAction(action: Function): void
  {
    action.call(null, this.SelectedItems);
  }

  /**
   * Adds a new custom parameter to the outgoing select request
   * @param key the param's name
   * @param value the param's value
   */
  public AddRequestValue(key: string, value: any): void
  {
    this._CustomRequestValues.Remove(key);
    this._CustomRequestValues.Add(key, value);
  }
  /**
   * Removes a custom value IF ithe given key is contained within the collection
   * @param key the param's name
   */
  public RemoveRequestValue(key: string): void
  {
    this._CustomRequestValues.Remove(key);
  }
  /**
   * Clears the custom values collection
   */
  public ClearCustomValues(): void
  {
    this._CustomRequestValues.Clear();
  }

  /**
   * Adds a new custom parameter to the outgoing select request
   * @param key the param's name
   * @param value the param's value
   */
  public AddRequestValueForDelete(key: string, value: any): void
  {
    this._CustomRequestValuesForDelete.Remove(key);
    this._CustomRequestValuesForDelete.Add(key, value);
  }
  /**
   * Removes a custom value IF ithe given key is contained within the collection
   * @param key the param's name
   */
  public RemoveRequestValueForDelete(key: string): void
  {
    this._CustomRequestValuesForDelete.Remove(key);
  }
  /**
   * Clears the custom values collection
   */
  public ClearCustomValuesForDelete(): void
  {
    this._CustomRequestValuesForDelete.Clear();
  }

  /**
   * Clears all data in the select component
   */
  public ClearData(): void
  {
    if (!ObjectUtils.NullOrUndefined(this.List))
      this.List.Clear();
    if (!ObjectUtils.NullOrUndefined(this.Actions))
      this.Actions.SelectionUpdated(0);
    this.Pagination.Clear();
  }
  /** Clears the current selection */
  public ClearSelection(): void
  {
    this.SelectedItems = [];
    if (!ObjectUtils.NullOrUndefined(this.List))
      this.List.ClearSelection();
  }

  /**
   * Defines the multiselect behaviour based on a given value
   * @param multiSelect flag indicating if the component wil have a multiselect behavior
   */
  public SetMultiSelect(multiSelect: boolean): void
  {
    this._MultiSelect = multiSelect;
    if (!ObjectUtils.NullOrUndefined(this.List))
      this.List.SetMultiselect(multiSelect);
  }

  /**
   * Hides the ENTIRE component
   */
  public Hide(): void
  {
    InputDecoration.ElementFadeOut(this.ComponentContainer);
  }
  /**
   * Shows the Entire component
   */
  public Show(): void
  {
    InputDecoration.ElementFadeIn(this.ComponentContainer);
  }

  public HidePagination(): void
  {
    InputDecoration.ElementFadeOut(this.PaginationContainer.element);
  }
  public ShowPagination(): void
  {
    InputDecoration.ElementFadeIn(this.PaginationContainer.element);
  }

  public Callback<T extends Crudable>(response: BatchResponseMessage<T>, baseObject: T, items: T[] = null): void
  {
    if (!this._ShowFilter)
      this._FilterContainer.clear();

    if (!ObjectUtils.NullOrUndefined(this.Actions))
      this.Actions.CanRefresh();
    this._Loader.Hide();

    if (!ObjectUtils.NullOrUndefined(response) && response.HasErrors)
    {
      SnackUtils.OpenError(this._Snack, response.ErrorMessage);
      return;
    }

    if (!ObjectUtils.NullOrUndefined(items))
    {
      if (!ObjectUtils.NullOrUndefined(this.SortFuncion))
        items.sort((a, b) => this.SortFuncion(a, b));
      if (ObjectUtils.NullOrUndefined(response))
        this.SetItems(items);
      else
        this.SetItems(items, response.Pagination);
    }
    else
    {
      if (ObjectUtils.NullOrUndefined(response))
      {
        SnackUtils.OpenError(this._Snack, `Falha no envio da solicitação`);
        return;
      }

      const listItems: T[] = JsonUtils.ParseList(response.Entities, baseObject);
      if (!ObjectUtils.NullUndefinedOrEmptyArray(listItems))
      {
        if (!ObjectUtils.NullOrUndefined(this.SortFuncion))
          listItems.sort((a, b) => this.SortFuncion(a, b));
        this.SetItems(listItems, response.Pagination);
      }
      else
      {
        this.ClearData();
        this.ClearPagination();
        if (this.ShowsNoResultsMessage)
          SnackUtils.Open(this._Snack, `Nenhum registro encontrado...`, `Aviso`);
      }
    }
  }
  public CallbackList<T extends Crudable>(response: BatchResponseMessageList, key: string, baseObject: T, items: T[] = null): void
  {
    if (!this._ShowFilter)
      this._FilterContainer.clear();

    if (!ObjectUtils.NullOrUndefined(this.Actions))
      this.Actions.CanRefresh();
    this._Loader.Hide();

    if (!ObjectUtils.NullOrUndefined(response) && response.HasErrors)
    {
      SnackUtils.OpenError(this._Snack, response.ErrorMessage);
      return;
    }

    if (!ObjectUtils.NullOrUndefined(items))
    {
      if (!ObjectUtils.NullOrUndefined(this.SortFuncion))
        items.sort((a, b) => this.SortFuncion(a, b));
      if (ObjectUtils.NullOrUndefined(response))
        this.SetItems(items);
      else
        this.SetItems(items, response.Pagination);
    }
    else
    {
      if (ObjectUtils.NullOrUndefined(response))
      {
        SnackUtils.OpenError(this._Snack, `Falha no envio da solicitação`);
        return;
      }

      const listItems: T[] = response.GetList(key, baseObject);
      if (!ObjectUtils.NullUndefinedOrEmptyArray(listItems))
      {
        if (!ObjectUtils.NullOrUndefined(this.SortFuncion))
          listItems.sort((a, b) => this.SortFuncion(a, b));
        this.SetItems(listItems, response.Pagination);
      }
      else
      {
        this.ClearData();
        this.ClearPagination();
        if (this.ShowsNoResultsMessage)
          SnackUtils.Open(this._Snack, `Nenhum registro encontrado...`, `Aviso`);
      }
    }
  }
}
