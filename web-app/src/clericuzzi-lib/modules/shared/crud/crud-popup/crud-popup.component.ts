import { MAT_DIALOG_DATA, MatDialog, MatSnackBar } from '@angular/material';
import { Component, OnInit, Inject, ViewContainerRef, ViewChild, ViewRef, ComponentFactoryResolver, ElementRef } from '@angular/core';

// utils
import { SnackUtils } from 'clericuzzi-lib/utils/snack-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// models
import { BaseMessage } from 'clericuzzi-lib/entities/response/base-message.model';
import { RequestMessage } from 'clericuzzi-lib/entities/request/request-message.model';
import { BatchResponseMessage, BatchResponseMessageList } from 'clericuzzi-lib/entities/response/batch-response-message.model';
import { StatusResponseMessage } from 'clericuzzi-lib/entities/response/status-response-message.model';
import { CrudPopupComponentModel } from './crud-popup.model';
import { CrudableTableColumnDefinitionModel } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';

// services
import { HttpService } from 'clericuzzi-lib/services/http.service';
import { LoaderService } from 'clericuzzi-lib/services/loader.service';
import { AutoCompleteFetchService } from 'clericuzzi-lib/services/auto-complete-fetch.service';
import { ComponentGeneratorService } from 'clericuzzi-lib/services/component-generator.service';

// components
import { HostBindedComponent } from '../../util-components/host-binded/host-binded.component';
import { MaterialPopupComponent } from 'clericuzzi-lib/modules/material/components/material-popup/material-popup.component';
import { CustomField } from 'clericuzzi-lib/entities/crud/custom-field.model';

@Component({
  selector: 'app-crud-popup',
  templateUrl: './crud-popup.component.html'
})
export class CrudPopupComponent extends HostBindedComponent implements OnInit
{
  @ViewChild('_container', { read: ViewContainerRef }) public ItemsContainer: ViewContainerRef;
  public Busy: boolean = false;
  public IsInsert: boolean = false;
  public IsUpdate: boolean = false;
  constructor(
    @Inject(MAT_DIALOG_DATA) public Definitions: CrudPopupComponentModel,
    private _Diag: MatDialog,
    private _Http: HttpService,
    private _Snack: MatSnackBar,
    private _Loader: LoaderService,
    private _Generator: ComponentGeneratorService,
    private _AutoCompleteFetch: AutoCompleteFetchService,
  )
  {
    super(null);
  }

  ngOnInit()
  {
    this.InitFactories(this._Generator.Factory, this._Generator.Sanitizer);
    if (!ObjectUtils.NullOrUndefined(this.Definitions) && !ObjectUtils.NullUndefinedOrEmptyArray(this.Definitions.Columns))
      for (const column of this.Definitions.Columns)
        this.AddFilter(column, column.ReadableName);

    this.CurrentModel = this.Definitions.BaseObject.Clone();
    setTimeout(() => this._SyncModel(), 250);
  }

  public get CanAskForKeepInserting(): boolean
  {
    if (this.IsUpdate)
      return false;
    else
      return this.Definitions.AskKeepInserting;
  }
  public get HasTitle(): boolean
  {
    return !ObjectUtils.NullOrUndefined(this.Definitions) && !ObjectUtils.NullUndefinedOrEmpty(this.Definitions.Title);
  }

  private _NewModel(): void
  {
    this.CurrentModel = this.Definitions.BaseObject.Clone();
    for (const control of this.FormItems)
    {
      control.Model = this.CurrentModel;
      control.ClearValue();
      control.SetPristine();
    }
  }
  private _SyncModel(): void
  {
    this.CurrentModel = this.Definitions.BaseObject.Clone();
    for (const control of this.FormItems)
    {
      control.Model = this.CurrentModel;
      control.Sync();
    }
  }

  public LoadExternalData(url: string, ...customFields: CustomField[]): void
  {
    this._AutoCompleteFetch.FetchAutocompleteData(url, response => this._LoadExternalDataCallback(response, customFields));
  }
  public LoadExternalDataWithParams(url: string, params: CustomField[], ...customFields: CustomField[]): void
  {
    this._AutoCompleteFetch.FetchAutocompleteData(url, response => this._LoadExternalDataCallback(response, customFields), ...params);
  }
  private _LoadExternalDataCallback(response: BatchResponseMessageList, customFields: CustomField[]): void
  {
    for (const customField of customFields)
      this.FillAutoCompleteData(response, customField.Key, customField.Value as string);

    this.Sync();
  }
  public AddFilter<T>(columnDefinition: CrudableTableColumnDefinitionModel, placeholder: string): T
  {
    return this.AddComponent(this.ItemsContainer, columnDefinition, placeholder, this.SendRequest.bind(this));
  }

  public SendRequest(): void
  {
    if (this.ValidateModel())
      MaterialPopupComponent.Confirmation(this._Diag, this._SendRequestAction.bind(this), null, this.Definitions.ConfirmationTitle, this.Definitions.ConfirmationMessage, this.Definitions.ConfirmationMessageNo, this.Definitions.ConfirmationMessageYes);
    else
      SnackUtils.OpenError(this._Snack, this.Definitions.FormValidationMessage);
  }
  private _SendRequestAction(): void
  {
    this.Busy = true;
    this._Loader.Show();
    const request: RequestMessage = new RequestMessage(this.Definitions.RequestUrl);
    if (this.Definitions.SendArray)
      request.Add(`entities`, [this.CurrentModel.ToJson()]);
    else
      request.Add(`model`, this.CurrentModel.ToJson());

    if (this.CustomFields.length > 0)
      for (const customField of this.CustomFields)
        request.Add(customField.Key, customField.Value);

    if (this.Definitions.SendArray)
    {
      const response: BatchResponseMessage<any> = new BatchResponseMessage<any>();
      this._Http.Post(request, response, () => this._SendRequestActionCallback(response));
    }
    else
    {
      const response: StatusResponseMessage = new StatusResponseMessage();
      this._Http.Post(request, response, () => this._SendRequestActionCallback(response));
    }
  }
  private _SendRequestActionCallback(response: BaseMessage): void
  {
    this.Busy = false;
    this._Loader.Hide();
    if (ObjectUtils.NullOrUndefined(response) || ObjectUtils.NullOrUndefined(response.HasErrors))
      SnackUtils.OpenError(this._Snack, this.Definitions.ErrorMessage, 8000);
    else
    {
      if (response.HasErrors)
        SnackUtils.OpenError(this._Snack, response.ErrorMessage, 8000);
      else
      {
        SnackUtils.OpenSuccess(this._Snack, this.Definitions.SuccessMessage, 8000);
        if (!this.Definitions.KeepInserting || this.IsUpdate)
          this.CloseComponent();
        else
          this._NewModel();
      }
    }
  }
}
