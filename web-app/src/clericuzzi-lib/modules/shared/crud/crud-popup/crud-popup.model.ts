// utils
import { ServerUrls } from 'clericuzzi-lib/utils/server-urls';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// models
import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model';
import { CrudableTableColumnDefinitionModel } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';

export class CrudPopupComponentModel
{
  public Columns: CrudableTableColumnDefinitionModel[];

  public SendArray: boolean = false;
  public KeepInserting: boolean = false;
  public AskKeepInserting: boolean = false;

  public Title: string;
  public RequestUrl: string;
  public ErrorMessage: string = `Falha no envio da requisição...`;
  public SuccessMessage: string;
  public TitleActionDeny: string = `Cancelar`;
  public TitleActionConfirm: string;
  public ConfirmationTitle: string = `Confirmação`;
  public ConfirmationMessage: string;
  public FormValidationMessage: string = `Preencha corretamente os campos`;
  public ConfirmationMessageNo: string = `não`;
  public ConfirmationMessageYes: string = `sim`;

  public CustomComponents: any[];

  public static NewInsert(baseObject: Crudable, ...columns: string[]): CrudPopupComponentModel
  {
    const crudModel: CrudPopupComponentModel = new CrudPopupComponentModel(baseObject, true);
    crudModel.IsInsert = true;
    crudModel.AskKeepInserting = true;
    crudModel.Columns = [];
    for (const column of columns)
      crudModel.Columns.push(baseObject.GetColumnDefinition(column));

    return crudModel;
  }
  public static NewUpdate(baseObject: Crudable, ...columns: string[]): CrudPopupComponentModel
  {
    const crudModel: CrudPopupComponentModel = new CrudPopupComponentModel(baseObject, false);
    crudModel.IsInsert = false;
    crudModel.Columns = [];
    for (const column of columns)
      crudModel.Columns.push(baseObject.GetColumnDefinition(column));

    return crudModel;
  }

  constructor(public BaseObject: Crudable = null, public IsInsert: boolean = true)
  {
    if (!ObjectUtils.NullOrUndefined(this.BaseObject))
      this._InitComponent();
  }
  private _InitComponent(): void
  {
    const question: string = this.IsInsert ? `${this.BaseObject.TableDefinition.Indefinite()} nov${this.BaseObject.TableDefinition.Article()} ${this.BaseObject.TableDefinition.ReadableName.toLocaleLowerCase()}` : `${this.BaseObject.TableDefinition.Article()} ${this.BaseObject.TableDefinition.ReadableName.toLocaleLowerCase()} selecianad${this.BaseObject.TableDefinition.Article()}`;
    const actionTitle: string = this.IsInsert ? `Cadastrar` : `Alterar`;
    const remoteEndpoint: string = this.IsInsert ? `insert` : `update`;

    this.Title = this.IsInsert ? `Nov${this.BaseObject.TableDefinition.Article()} ${this.BaseObject.TableDefinition.ReadableName}` : `Alterar ${this.BaseObject.TableDefinition.ReadableName}`;
    this.RequestUrl = ServerUrls.GetUrl(`crud/${this.BaseObject.TableDefinition.TableName}/${remoteEndpoint}`);
    this.TitleActionConfirm = actionTitle;
    this.ConfirmationMessage = `Deseja realmente ${actionTitle.toLocaleLowerCase()} ${question}?`;
    this.SuccessMessage = this.IsInsert ? `Cadastro realizado com sucesso` : `Alteração realizada com sucesso`;
  }
}
