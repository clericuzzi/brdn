import { DomSanitizer } from '@angular/platform-browser';
import { Component, OnInit, ViewContainerRef, ViewChild, ComponentFactoryResolver, HostBinding } from '@angular/core';

@Component({
  selector: 'app-crud-item',
  templateUrl: './crud-item.component.html'
})
export class CrudItemComponent implements OnInit
{
  @HostBinding(`class`) CustomClass: string = `crud-pupup-item`;
  @ViewChild('_container', { read: ViewContainerRef }) public Container: ViewContainerRef;
  constructor(
    public Sanitizer: DomSanitizer,
    public FactoryResolver: ComponentFactoryResolver,
  )
  {
  }

  ngOnInit()
  {
  }
}