import { DomSanitizer } from "@angular/platform-browser";
import { HostBinding, ViewChild, ViewContainerRef, ComponentFactoryResolver } from "@angular/core";

// utils
import { Dictionary } from "clericuzzi-lib/utils/dictionary-utils";
import { ObjectUtils } from "clericuzzi-lib/utils/object-utils";
import { InputDecoration } from "clericuzzi-lib/utils/input-decoration";
import { CrudableTableColumnDefinitionModel } from "clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model";
import { AutoCompleteResponseList } from "clericuzzi-lib/entities/response/auto-complete.model";

// models
import { Crudable } from "clericuzzi-lib/entities/crud/crudable.model";

// components
import { ComboInputValueModel } from "../form-controls/custom-inputs/combo-input/combo-input-value.model";
import { FormControlComponent } from "clericuzzi-lib/modules/shared/form-controls/form-control.component";
import { AutocompleteInputComponent } from "../form-controls/autocomplete-input/autocomplete-input.component";

export abstract class CrudSelectPanel<T extends Crudable>
{
    public ConfirmAction: Function;
    public MessageOverride: string;
    public IsAlive: boolean = true;
    public CanHide: boolean = true;
  
    public Key: string;
    public Title: string;
    public UrlAction: string;
    public Busy: boolean = false;
    public HasShadow: boolean = true;
    public IsVisible: boolean = false;
    public BaseObject: T;
    public InitialObject: T;
    public Components: Dictionary<FormControlComponent<any>> = new Dictionary<FormControlComponent<any>>();
    
    constructor(
        public Sanitizer: DomSanitizer,
        public FactoryResolver: ComponentFactoryResolver,
    )
    {
    }

    @ViewChild('itemsPanel', { read: ViewContainerRef }) ContainerItems: ViewContainerRef;
    @HostBinding(`class`) ContainerClass: string = `crud-select-body visibiliity-fades visibiliity-fade-removed`;
    public Hide(): void
    {
        if (this.IsVisible && this.CanHide)
        {
            InputDecoration.BindingFadeOut(this, `ContainerClass`);
            this.IsVisible = false;
            setTimeout(() => this.Busy = true, 250);
        }        
    }
    public Show(): void
    {
        if (!this.IsVisible)
        {
            InputDecoration.BindingFadeIn(this, `ContainerClass`);
            this.IsVisible = true;
            this.Busy = false;
        }
    }
    public HideShadow(): void
    {
        InputDecoration.BindingRemoveClasses(this, `ContainerClass`, `crud-select-body-shadow`)
    }
    
    /**
     * Initializes the panel
     * @param baseObject the base crudable object
     * @param title the panel title
     * @param urlAction the request endpoint
     * @param required if provided, this value will override ALL the components required status
     * @param cloneInstance if FALSE, the component will work in the same instance that was provided
     */
    public Init(baseObject: T, title: string, urlAction: string, updateFilter: boolean, insertFilter: boolean, searchFilter: boolean, required: boolean = null, cloneInstance: boolean = true): void
    {
        this.Components = new Dictionary<FormControlComponent<any>>();
        if (!ObjectUtils.NullUndefinedOrEmpty(title))
            this.Title = title;
        if (!ObjectUtils.NullUndefinedOrEmpty(urlAction))
            this.UrlAction = urlAction;
        if (cloneInstance)
        {
            this.BaseObject = baseObject.Clone() as T;
            this.InitialObject = baseObject.Clone() as T;
        }
        else
        {
            this.BaseObject = baseObject as T;
            this.InitialObject = baseObject as T;
        }
        
        if (!ObjectUtils.NullOrUndefined(this.ContainerItems))
        {
            this.ContainerItems.clear();
            let targetColumns: CrudableTableColumnDefinitionModel[] = this.BaseObject.ColumnDefinitions.All();
            if (insertFilter)
                targetColumns = targetColumns.filter(i => i.CanInsert);
            else if (updateFilter)
                targetColumns = targetColumns.filter(i => i.CanUpdate);
            else if (searchFilter)
                targetColumns = targetColumns.filter(i => i.CanFilter);
            
            for (let columnDefinition of targetColumns)
            {
                let clone: CrudableTableColumnDefinitionModel = columnDefinition.Clone();
                clone.HasShadow = false;
                clone.DefaultHeight = 40;
                clone.WidthPercentage = 100;
                
                let component: FormControlComponent<any> = clone.CreateComponent(this.ContainerItems, this.FactoryResolver, this.BaseObject, null, this.Sanitizer, null);
                if (!ObjectUtils.NullOrUndefined(required))
                    component.Required = required;
                
                this.Components.Add(clone.PropertyName, component);
            }
        }
    }

    public Clear(): void
    {
        for (let component of this.Components.All())
            component.ClearValue();
    }
    public Validate(): boolean
    {
        let isValid: boolean = true;
        let components: FormControlComponent<any>[] = this.Components.All();
        for (let component of components)
        {
            if (component.Required)
            {
                if (!component.IsValid)
                {
                    component.SetDirty();
                    if (isValid)
                        isValid = false;
                }
            }
        }
                    
        return isValid;
    }
    public Enable(): void
    {
        for (let component of this.Components.All())
            if (!ObjectUtils.NullOrUndefined(component))
                component.Enable();
    }
    public Disable(): void
    {
        for (let component of this.Components.All())
            if (!ObjectUtils.NullOrUndefined(component))
                component.Disable();
    }
    public SetUpdateUrl(url: string, ... componentNames: string[]): void
    {
        if (!ObjectUtils.NullUndefinedOrEmptyArray(componentNames))
            for (let componentName of componentNames)
            {
                let component: FormControlComponent<any> = this.GetComponent(componentName);
                if (!ObjectUtils.NullOrUndefined(component))
                    component.SetUpdateUrl(url);
            }
    }
    public LoadFksCallback(response: AutoCompleteResponseList): void
    {
        for (let sourceProperty of this.Components.Keys)
        {
            let entities: ComboInputValueModel<number>[] = response.GetEntities(sourceProperty);
            if (!ObjectUtils.NullUndefinedOrEmptyArray(entities))
            {
                let items: ComboInputValueModel<number>[] = [];
                for (let entity of entities)
                    items.push(ComboInputValueModel.From(entity));
                
                let component: AutocompleteInputComponent<number> = this.Components.Get(sourceProperty) as AutocompleteInputComponent<number>;
                if (component != null)
                    component.SetItems(items);
            }
        }
    }

    public Sync(): void
    {
        if (!ObjectUtils.NullOrUndefined(this.Components))
        {
            let components: FormControlComponent<any>[] = this.Components.All();
            if (!ObjectUtils.NullUndefinedOrEmptyArray(components))
                for (let component of components)
                    component.Sync();
        }
    }
    public GetValue<T>(componentKey: string): any
    {
        let component: FormControlComponent<T> = this.Components.Get(componentKey) as FormControlComponent<T>;
        if (!ObjectUtils.NullOrUndefined(component))
        {
            if (component instanceof AutocompleteInputComponent)
            {
                let autoComplete: AutocompleteInputComponent<any> = component as AutocompleteInputComponent<any>;
                if (ObjectUtils.NullOrUndefined(autoComplete))
                    return component.Value;
                else
                {
                    if (autoComplete.MultiSelect)
                        return autoComplete.Values;
                    else                        
                        return autoComplete.Value;
                }
            }
            else
                return component.Value;
        }
        else
            return null;
    }
    public GetComponent<T>(componentKey: string): any
    {
        return this.Components.Get(componentKey) as FormControlComponent<T>;
    }
    public Reset(updateFilter: boolean, insertFilter: boolean, searchFilter: boolean, required: boolean = null): void
    {
        this.Init(this.BaseObject, this.Title, this.UrlAction, updateFilter, insertFilter, searchFilter, required);
    }

    public abstract Initialize(baseObject: T, title: string, urlAction: string): void;
    public abstract LoadEntity(entity: any): void;
}