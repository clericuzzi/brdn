import { Subject } from 'rxjs';
import { DomSanitizer } from '@angular/platform-browser';
import { Component, OnInit, ViewChild, ViewContainerRef, ComponentFactoryResolver } from '@angular/core';

// utils
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// components
import { HostBindedComponent } from '../../util-components/host-binded/host-binded.component';
import { FormControlComponent } from '../../form-controls/form-control.component';
import { BatchResponseMessageList } from 'clericuzzi-lib/entities/response/batch-response-message.model';
import { CrudableTableColumnDefinitionModel } from 'clericuzzi-lib/entities/crud/definitions/crudable-table-column-definition.model';
import { AutocompleteInputComponent } from '../../form-controls/autocomplete-input/autocomplete-input.component';
import { ComboInputValueModel } from '../../form-controls/custom-inputs/combo-input/combo-input-value.model';
import { JsonUtils } from 'clericuzzi-lib/utils/json-utils';

@Component({
  selector: 'app-crud-filter',
  templateUrl: './crud-filter.component.html'
})
export class CrudFilterComponent extends HostBindedComponent implements OnInit
{
  @ViewChild('container', { read: ViewContainerRef }) _Container: ViewContainerRef;

  private _Height: number = null;
  private _Components: FormControlComponent<any>[] = [];
  public OnFilterRequested: Subject<any> = new Subject<any>();
  /**
   * Defines a function that will be called whenever LoadAutoCompleteData gets completed.
   * It must receive ONE argument of type 'BatchResponseMessageList'
   */
  public CustomAutoCompleteHandler: Function;
  constructor(
    private factory: ComponentFactoryResolver,
    private sanitizer: DomSanitizer,
  )
  {
    super(null);
    this.Factory = factory;
    this.Sanitizer = sanitizer;
  }

  ngOnInit()
  {
  }

  public get GetStyle(): any
  {
    const style = {};
    style[`height`] = `${this._Height}px`;

    return style;
  }

  public Addfilter<T>(columnDefinition: CrudableTableColumnDefinitionModel, placeholder: string, onSendAction: Function = null, maxLength: number = 0): T
  {
    const component: FormControlComponent<any> = this.AddComponent(this._Container, columnDefinition, placeholder, onSendAction, maxLength);
    if (!ObjectUtils.NullOrUndefined(this.CurrentModel))
      component.Update(this.CurrentModel);

    component.Required = false;
    this._Components.push(component);
    this.ListenTo(component.OnSend, () => this.Filter());
    return (component as any) as T;
  }
  public Filter(): void
  {
    this.OnFilterRequested.next(this.CurrentModel);
  }

  public SetAutocompleteItems<T>(componentName: string, key: string, response: BatchResponseMessageList): void
  {
    const component: AutocompleteInputComponent<T> = this.GetComponent(componentName);
    if (!ObjectUtils.NullOrUndefined(component))
    {
      const items: any[] = response.GetArrayItems(key);
      const entities: ComboInputValueModel<T>[] = JsonUtils.ParseList(items, new ComboInputValueModel<T>());
      component.SetItems(entities);
    }
  }
  public LoadAutoCompleteData(response: BatchResponseMessageList): void
  {
    if (!ObjectUtils.NullOrUndefined(this.CustomAutoCompleteHandler))
      this.CustomAutoCompleteHandler(response);
    else
      throw new Error(`Você deve definir um método de retorno para tratar a lista de conteúdo dos campos AutoComplete`);
  }
}
