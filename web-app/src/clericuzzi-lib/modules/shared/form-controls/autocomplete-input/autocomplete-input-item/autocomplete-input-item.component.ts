import { Subject } from 'rxjs';
import { FormControl } from '@angular/forms';
import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';

// utils
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

// models
import { HostBindedComponent } from 'clericuzzi-lib/modules/shared/util-components/host-binded/host-binded.component';
import { ComboInputValueModel } from 'clericuzzi-lib/modules/shared/form-controls/custom-inputs/combo-input/combo-input-value.model';

@Component({
  selector: 'app-autocomplete-input-item',
  templateUrl: './autocomplete-input-item.component.html'
})
export class AutocompleteInputItemComponent<T> extends HostBindedComponent implements OnInit
{
  public OnClick: Subject<AutocompleteInputItemComponent<T>> = new Subject<AutocompleteInputItemComponent<T>>();

  public Control = new FormControl();
  @ViewChild(`inputField`) Input: ElementRef;
  public Model: ComboInputValueModel<T>;
  public ShowSelectionBox: boolean = false;
  
  public get Id(): T
  {
    if (ObjectUtils.NullOrUndefined(this.Model))
      return null;
    else
      return this.Model.Id;
  }
  public get Label(): string
  {
    if (ObjectUtils.NullOrUndefined(this.Model))
      return null;
    else
      return this.Model.Label;
  }
  public IsSelected(): boolean
  {
    if (ObjectUtils.NullOrUndefined(this.Model))
      return false;
    else
    {
      if (ObjectUtils.NullOrUndefined(this.Model.Selected))
        return false;
      else
        return this.Model.Selected;
    }
  }
  constructor()
  {
    super(null);
  }

  ngOnInit()
  {
  }

  public SetState(): void
  {
    this.Control.setValue(this.Model.Selected);
  }
  public ToggleSelection(): void
  {
    if (!ObjectUtils.NullOrUndefined(this.Model))
    {
      if (ObjectUtils.NullOrUndefined(this.Model.Selected))
        this.Model.Selected = true;
      else
        this.Model.Selected = !this.Model.Selected;
      
      this.SetState();
      this.OnClick.next(this);
    }
  }
}