import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { MatSelect } from '@angular/material';

// utils
import { StringUtils } from 'clericuzzi-lib/utils/string-utils';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';

@Component({
  selector: 'app-hour-input',
  templateUrl: './hour-input.component.html'
})
export class HourInputComponent implements OnInit
{
  @Input() Limit: number = 23;
  @Input() Suffix: string = `h`;

  @Input() Min: number;
  @Input() Model: any;
  @Input() Warning: string;
  @Input() Disabled: boolean;
  @Input() Property: string;
  @Input() Required: boolean = false;
  @Input() Placeholder: string;
  @Output() OnValueChanged: EventEmitter<void> = new EventEmitter<void>();

  @ViewChild(`select`) Select: MatSelect;

  public Value: number;
  public Hours: any[] = [];
  constructor() { }

  ngOnInit()
  {
    var minValue: number = ObjectUtils.NullOrUndefined(this.Min) ?  0 : this.Min;
    for (let i = minValue; i <= this.Limit; i++)
      this.Hours.push(i);
  }
  public Sync(): void
  {
    if (this.Model != null && this.Model != undefined)
      this.Value = this.Model[this.Property];
  }
  public Update(model: any): void
  {
      this.Model = model;
      this.Sync();
  }
  public OnChange(): void
  {
    this.Model[this.Property] = this.Value;
    this.OnValueChanged.next();
  }
  public ClearValue(): void
  {
    this.Model[this.Property] = null;
    this.OnValueChanged.next();
  }

  public GetStringValue(value: number): string
  {
    return StringUtils.GetTimeString(value, this.Suffix);
  }
}