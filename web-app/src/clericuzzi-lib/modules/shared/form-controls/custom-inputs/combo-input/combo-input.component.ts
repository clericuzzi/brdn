import { Component, Input, OnInit, Renderer, ViewChild } from '@angular/core';

import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';
import { ComboInputValueModel } from './combo-input-value.model';
import { FormControlComponent } from '../../form-control.component';

// services
import { HttpService } from 'clericuzzi-lib/services/http.service';

@Component({
  selector: 'app-combo-input',
  templateUrl: './combo-input.component.html'
})
export class ComboInputComponent<T> extends FormControlComponent<T> implements OnInit
{
  public Loading: boolean = false;
  public MultiSelect: boolean = false;
  @Input() Items: ComboInputValueModel<T>[];
  public static GetRecentYears(yearsBack: number = 1): ComboInputValueModel<number>[]
  {
    const items: ComboInputValueModel<number>[] = [];
    const date: Date = new Date();
    let currentYear: number = date.getFullYear();
    while (yearsBack-- > 0)
      items.push(new ComboInputValueModel<number>(currentYear, (currentYear--).toString()));

    return items;
  }

  public get HasValue(): boolean
  {
    return !ObjectUtils.NullOrUndefined(this.Value);
  }
  public get IsValid(): boolean
  {
    if (this.Required && ObjectUtils.NullOrUndefined(this.Value))
      return false;
    else
      return true;
  }
  public Validate(): void
  {
    if (!ObjectUtils.NullOrUndefined(this.Model) && !ObjectUtils.NullUndefinedOrEmpty(this.Property))
      this.Model[this.Property] = this.Value;
  }

  constructor(
    public _Http: HttpService,
  )
  {
    super(_Http);
  }

  public Select(value: T): void
  {
    if (!ObjectUtils.NullOrUndefined(this.Model) && !ObjectUtils.NullOrUndefined(this.Property))
    {
      this.Model[this.Property] = value;
      this.Sync();
    }
  }
  public SetItems(source: any[], propertyId: string = `Id`, propertyLabel: string = `Label`): void
  {
    if (ObjectUtils.NullUndefinedOrEmptyArray(source))
      this.Items = null;
    else
    {
      const items: ComboInputValueModel<T>[] = [];
      for (const item of source)
        items.push(new ComboInputValueModel<T>(item[propertyId], item[propertyLabel]));

      this.Items = items;
    }
  }

  ngOnInit()
  {
    this.Sync();
  }
  public Send(): void
  {
    this.OnSend.next();
  }
  public FocusOut(): void
  {
    this._FocusOut();
  }
  public ValueChanged(): void
  {
    this.Model[this.Property] = this.Value;
    this.OnValueChanged.next();
  }
}
