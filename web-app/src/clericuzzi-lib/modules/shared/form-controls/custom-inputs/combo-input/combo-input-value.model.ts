import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';
import { CustomFieldable } from 'clericuzzi-lib/entities/crud/custom-fieldable.model';

export class ComboInputValueModel<T> extends CustomFieldable
{
  public static From<T>(obj: any): ComboInputValueModel<T>
  {
    const value: ComboInputValueModel<T> = new ComboInputValueModel<T>();
    value.ParseFrom(obj);

    return value;
  }

  constructor(
    public Id: T = null,
    public Label: string = null,
    public Selected: boolean = false,
  )
  {
    super();

    if (ObjectUtils.NullOrUndefined(this.Label) && !ObjectUtils.NullOrUndefined(this.Id))
      this.Label = this.Id.toString();
  }
  public FilterByField(key: string, value: T): boolean
  {
    const fieldValue: T = this.GetValue(key, null);
    if (!ObjectUtils.NullOrUndefined(fieldValue))
      return fieldValue === value;
    else
      return ObjectUtils.NullOrUndefined(value);
  }

  public ParseFrom(obj: any): void
  {
    this.Id = obj[`Id`];
    this.Label = obj[`Label`];
    this.Selected = obj[`Selected`];
    this.ParseFieldsFrom(obj);
  }
}
