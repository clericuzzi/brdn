import { Component, OnInit, HostBinding, Renderer } from '@angular/core';
import { FormControlComponent } from '../form-control.component';
import { Crudable } from 'clericuzzi-lib/entities/crud/crudable.model';
import { ObjectUtils } from 'clericuzzi-lib/utils/object-utils';
import { Subject } from 'rxjs';

// services
import { HttpService } from 'clericuzzi-lib/services/http.service';

@Component({
  selector: 'app-label-control',
  templateUrl: './label-control.component.html',
  styleUrls: ['./label-control.component.css']
})
export class LabelControlComponent extends FormControlComponent<string> implements OnInit
{
  public OnClick: Subject<void> = new Subject<void>();

  public Align: string = `center`;
  public IsValid: boolean;
  public Transform: Function;
  public AlternateProperty: string;
  public Validate(): void
  {
  }

  public get AlternateValue(): string
  {
    if (ObjectUtils.NullOrUndefined(this.Model) || ObjectUtils.NullUndefinedOrEmpty(this.AlternateProperty))
      return this.GetLabelValue;
    else
      return ObjectUtils.GetValue(this.Model[this.AlternateProperty], this.GetLabelValue);
  }
  public get GetLabelValue(): string
  {
    if (ObjectUtils.NullOrUndefined(this.Model))
      return this.Placeholder;

    let value: any = this.Model[this.Property];
    if (ObjectUtils.NullOrUndefined(value))
    {
      if (!ObjectUtils.NullOrUndefined(this.Transform))
        return this.Transform(value);
      else
        return null;
    }

    let parentProperty: string = this.Property + `Parent`;
    let parentValue: any = this.Model[parentProperty]
    if (!ObjectUtils.NullOrUndefined(parentValue))
    {
      if (!ObjectUtils.NullOrUndefined(this.Transform))
        return this.Transform(value);
      else
        return (parentValue as Crudable).ToString();
    }
    else
    {
      if (!ObjectUtils.NullOrUndefined(this.Transform))
        return this.Transform(value);
      else
        return value;
    }
  }
  constructor(
    public _Http: HttpService,
  )
  {
    super(_Http);
  }

  ngOnInit()
  {
  }

  public get LabelClass(): string
  {
    let labelClass: string = `label-control-default`;
    if (!ObjectUtils.NullOrUndefined(this._OnClickFunction))
      labelClass = `${labelClass} cursor-pointer`;

    return labelClass;
  }
  private _OnClickFunction: Function;
  public SetClickEvent(clickFunction: Function): void
  {
    this._OnClickFunction = clickFunction;
  }

  public OnClickAction(): void
  {
    if (!ObjectUtils.NullOrUndefined(this._OnClickFunction))
      this._OnClickFunction();
  }
  public FocusOut(): void
  {
    this._FocusOut();
  }
}
