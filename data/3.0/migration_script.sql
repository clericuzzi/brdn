alter table `company` drop foreign key `fk|company|user`;
alter table `company_call` drop foreign key `fk|company_call|user`;
alter table `company_callback` drop foreign key `fk|company_callback|user`;
alter table `company_contact` drop foreign key `fk|company_contact|user`;
alter table `company_note` drop foreign key `fk|company_note|user`;
alter table `company` change column user_id `user` varchar(120) not null;
alter table `company_call` change column user_id `user` varchar(120) not null;
alter table `company_callback` change column user_id `user` varchar(120) not null;
alter table `company_contact` change column user_id `user` varchar(120) not null;
alter table `company_note` change column user_id `user` varchar(120) not null;

update `company` `cn` set `cn`.`user` = (select `email` from `user` `us` where `us`.`id` = `cn`.`user`);
update `company_call` `cn` set `cn`.`user` = (select `email` from `user` `us` where `us`.`id` = `cn`.`user`);
update `company_callback` `cn` set `cn`.`user` = (select `email` from `user` `us` where `us`.`id` = `cn`.`user`);
update `company_contact` `cn` set `cn`.`user` = (select `email` from `user` `us` where `us`.`id` = `cn`.`user`);
update `company_note` `cn` set `cn`.`user` = (select `email` from `user` `us` where `us`.`id` = `cn`.`user`);

drop table `import_file_company`;
drop table `import_file`;
drop table `user_permission`;
drop table `user`;
drop table if exists `company_active_service`;

create table `company_service`
(
	`id` int(11) unsigned not null auto_increment
	, `company_id` int(11) unsigned not null
	, `index` tinyint(1) unsigned not null
	 , `product_name` varchar(120) null
	 , `service_address` varchar(120) null
	 , `product_associated` varchar(120) null
	 , `offer` varchar(120) null
	 , `service_instance` varchar(120) null
	 , `invalid` varchar(120) null
	 , `voice_technology` varchar(120) null
	 , `billing_profile` varchar(120) null
	 , `created_in` varchar(120) null
	 , `activation_date` varchar(120) null
	 , `portability` varchar(120) null
	 , `document` varchar(120) null
	 , `phone` varchar(120) null
	 , `service_order` varchar(120) null
	 , `call_source` varchar(120) null
	 , `category` varchar(120) null
	 , constraint `pk|company_active_service` primary key (`id`)
	 , constraint `fk|company_active_service|company` foreign key (`company_id`) references `company` (`id`)
);

drop table if exists `company_owner`;
create table `company_owner`
(
	`id` int(11) unsigned not null auto_increment
	, `user` varchar(120) not null
	, `company_id` int(11) unsigned not null
	 , constraint `pk|company_owner` primary key (`id`)
	 , constraint `fk|company_owner|company` foreign key (`company_id`) references `company` (`id`)
	 , constraint `uc|company_owner` unique (`user`, `company_id`)
);
create index `ix|company_owner` on `company_owner` (`user`);

drop table if exists `company_phone`;
drop table if exists `company_phone_operator`;
create table `company_phone`
(
	`id` int(11) unsigned not null auto_increment
	, `company_id` int(11) unsigned not null
	, `phone` varchar(12) not null
	, `type` tinyint(1) not null
	, `operator_id` int(11) unsigned null
	, `operator_since` timestamp null
	 , constraint `pk|company_phone` primary key (`id`)
	 , constraint `fk|company_phone|company` foreign key (`company_id`) references `company` (`id`)
	 , constraint `uc|company_phone` unique (`company_id`, `phone`)
);
create index `ix|company_phone|phone` on `company_phone` (`phone`);
create table `company_phone_operator`
(
	`id` int(11) unsigned not null auto_increment
	, `phone_id` int(11) unsigned not null
	, `operator_id` int(11) unsigned null
	, `operator_since` timestamp null
	 , constraint `pk|company_phone` primary key (`id`)
	 , constraint `fk|company_phone_operator|company_phone` foreign key (`phone_id`) references `company_phone` (`id`)
	 , constraint `fk|company_phone_operator|phone_operator` foreign key (`operator_id`) references `phone_operator` (`id`)
);

drop table if exists `company_phone`;
create table `company_phone`
(
	`id` int(11) unsigned not null auto_increment
	, `company_id` int(11) unsigned null
	, `phone_id` int(11) unsigned not null
	 , constraint `pk|company_phone` primary key (`id`)
	 , constraint `fk|company_phone|company` foreign key (`company_id`) references `company` (`id`)
	 , constraint `fk|company_phone|phone` foreign key (`phone_id`) references `phone` (`id`)
	 , constraint `uc|company_phone` unique (`company_id`, `phone_id`)
);

drop table if exists `company_contact_phone`;
create table `company_contact_phone`
(
	`id` int(11) unsigned not null auto_increment
	, `contact_id` int(11) unsigned null
	, `phone_id` int(11) unsigned not null
	 , constraint `pk|company_contact_phone` primary key (`id`)
	 , constraint `fk|company_contact_phone|contact` foreign key (`contact_id`) references `company_contact` (`id`)
	 , constraint `fk|company_contact_phone|phone` foreign key (`phone_id`) references `phone` (`id`)
	 , constraint `uc|company_contact_phone` unique (`contact_id`, `phone_id`)
);

drop table if exists `lead_history`;
drop table if exists `lead`;
drop table if exists `lead_type_characteristics_allowed`;
drop table if exists `lead_type_characteristics_allowed_not`;
drop table if exists `lead_type`;

create table `lead_type`
(
	`id` int(11) unsigned not null auto_increment
	, `name` int(11) unsigned not null
	, constraint `pk|lead_type` primary key (`id`)
	, constraint `uc|lead_type` unique (`name`)
);

create table `lead_type_characteristics_allowed`
(
	`id` int(11) unsigned not null auto_increment
	, `lead_type_id` int(11) unsigned not null
	, `characteristic_id` int(11) unsigned not null
	, constraint `pk|lead_type_characteristics_allowed` primary key (`id`)
	, constraint `fk|lead_type_characteristics_allowed|lead_type` foreign key (`lead_type_id`) references `lead_type` (`id`)
	, constraint `fk|lead_type_characteristics_allowed|characteristic` foreign key (`characteristic_id`) references `characteristic` (`id`)
	, constraint `uc|lead_type_characteristics_allowed` unique (`lead_type_id`, `characteristic_id`)
);
create table `lead_type_characteristics_allowed_not`
(
	`id` int(11) unsigned not null auto_increment
	, `lead_type_id` int(11) unsigned not null
	, `characteristic_id` int(11) unsigned not null
	, constraint `pk|lead_type_characteristics_allowed_not` primary key (`id`)
	, constraint `fk|lead_type_characteristics_allowed_not|lead_type` foreign key (`lead_type_id`) references `lead_type` (`id`)
	, constraint `fk|lead_type_characteristics_allowed_not|characteristic` foreign key (`characteristic_id`) references `characteristic` (`id`)
	, constraint `uc|lead_type_characteristics_allowed_not` unique (`lead_type_id`, `characteristic_id`)
);

create table `lead`
(
	`id` int(11) unsigned not null auto_increment
	, `user` varchar(120) null
	, `company_id` int(11) unsigned not null
	, `lead_type_id` int(11) unsigned not null
	, `aquired_in` timestamp not null
	, `last_updated_in` timestamp null
	, constraint `pk|lead` primary key (`id`)
	, constraint `fk|lead|company` foreign key (`company_id`) references `company` (`id`)
	, constraint `fk|lead|lead_type` foreign key (`lead_type_id`) references `lead_type` (`id`)
	, constraint `uc|lead` unique (`company_id`, `lead_type_id`)
);
create index `ix|lead|user` on `lead` (`user`);

create table `lead_history`
(
	`id` int(11) unsigned not null auto_increment
	, `user` varchar(120) not null
	, `company_id` int(11) unsigned not null
	, `lead_type_id` int(11) unsigned not null
	, `aquired_in` timestamp not null
	, `dropped_in` timestamp null
	, constraint `pk|lead_history` primary key (`id`)
	, constraint `fk|lead_history|company` foreign key (`company_id`) references `company` (`id`)
	, constraint `fk|lead_history|lead_type` foreign key (`lead_type_id`) references `lead_type` (`id`)
);
create index `ix|lead_history|user` on `lead_history` (`user`);

drop table if exists `mining_statistics`;
create table `mining_statistics`
(
	`mining_type_id` tinyint(1) not null
	, `year` int(11) unsigned not null
	, `month` int(11) unsigned not null
	, `date` int(11) unsigned not null
	, `timestamp` timestamp not null
);
create index `ix|mining_statistics|year` on `mining_statistics` (`year`);
create index `ix|mining_statistics|month` on `mining_statistics` (`month`);
create index `ix|mining_statistics|date` on `mining_statistics` (`date`);
create index `ix|mining_statistics|year-month` on `mining_statistics` (`year`, `month`);
create index `ix|mining_statistics|year-month-date` on `mining_statistics` (`year`, `month`, `date`);
create index `ix|mining_statistics|timestamp` on `mining_statistics` (`timestamp`);