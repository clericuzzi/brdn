drop procedure if exists `prc_fix_company_doubles`;
delimiter |
create procedure `prc_fix_company_doubles` ()
begin

	DECLARE done INT DEFAULT FALSE;
	DECLARE companyId int(11);
	DECLARE cur CURSOR FOR select `id` from (select min(id) as `id` from company where length(concat(ifnull(ddd, ''), ifnull(phone, ''))) > 0 group by concat(ifnull(ddd, ''), ifnull(phone, '')) having count(0) > 1) as q;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

	OPEN cur;

	read_loop: LOOP
	FETCH cur INTO companyId;
    set @phone = (select concat(ifnull(ddd, ''), ifnull(phone, '')) from company where id = companyId);
    
    delete from company_line_operator where company_line_id in (select id from company_line where company_id in (select id from company where concat(ifnull(ddd, ''), ifnull(phone, '')) = @phone and id <> companyId));
    delete from company_contact_line where company_contact_id in (select id from company_contact where company_id in (select id from company where concat(ifnull(ddd, ''), ifnull(phone, '')) = @phone and id <> companyId));
    delete from company_line where company_id in (select id from company where concat(ifnull(ddd, ''), ifnull(phone, '')) = @phone and id <> companyId);
    delete from company_contact where company_id in (select id from company where concat(ifnull(ddd, ''), ifnull(phone, '')) = @phone and id <> companyId);
    
    update company_call set company_id = companyId, contact_id = null where company_id in (select id from company where concat(ifnull(ddd, ''), ifnull(phone, '')) = @phone);
    update company_note set company_id = companyId, contact_id = null where company_id in (select id from company where concat(ifnull(ddd, ''), ifnull(phone, '')) = @phone);
    update company_callback set company_id = companyId where company_id in (select id from company where concat(ifnull(ddd, ''), ifnull(phone, '')) = @phone);

    update lead set company_id = companyId where company_id in (select id from company where concat(ifnull(ddd, ''), ifnull(phone, '')) = @phone);

	delete from company_availability where company_address_id in (select id from company_address where company_id in (select id from company where concat(ifnull(ddd, ''), ifnull(phone, '')) = @phone and id <> companyId));
	delete from company_active_service where company_id in (select id from company where concat(ifnull(ddd, ''), ifnull(phone, '')) = @phone and id <> companyId);
    delete from company_address where company_id in (select id from company where concat(ifnull(ddd, ''), ifnull(phone, '')) = @phone and id <> companyId);
    delete from company_bill where company_id in (select id from company where concat(ifnull(ddd, ''), ifnull(phone, '')) = @phone and id <> companyId);
    
    delete from company_priority where company_id in (select id from company where concat(ifnull(ddd, ''), ifnull(phone, '')) = @phone and id <> companyId);
    delete from company_check_request where company_id in (select id from company where concat(ifnull(ddd, ''), ifnull(phone, '')) = @phone and id <> companyId);
    
    delete from company where concat(ifnull(ddd, ''), ifnull(phone, '')) = @phone and id <> companyId;
    
    IF done THEN
      LEAVE read_loop;
    END IF;
    
	END LOOP;

	CLOSE cur;

end|
delimiter ;

call `prc_fix_company_doubles`();
/*
select * from company where id = 456943;
select * from company where concat(ifnull(ddd, ''), ifnull(phone, '')) = '1145273838';
select * from company_note where company_id in (select id from company where concat(ifnull(ddd, ''), ifnull(phone, '')) = '1145273838');

select concat(ifnull(ddd, ''), ifnull(phone, '')), count(0) from company where length(concat(ifnull(ddd, ''), ifnull(phone, ''))) > 0 group by concat(ifnull(ddd, ''), ifnull(phone, '')) having count(0) > 1;
select `id` from (select min(id) as `id` from company where length(concat(ifnull(ddd, ''), ifnull(phone, ''))) > 0 group by concat(ifnull(ddd, ''), ifnull(phone, '')) having count(0) > 1) as q limit 1;
select id from company where concat(ifnull(ddd, ''), ifnull(phone, '')) = '1100000000' and id <> 540779;
*/