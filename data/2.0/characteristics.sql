-- select * from `characteristic`;
-- select * from `characteristic_association` limit 15;

-- soho vt
delete from `characteristic_association` where `characteristic_id` = 1;
insert into `characteristic_association` select distinct null, 1, `company_id`, now() from `company_address` where `id` in (select `company_address_id` from `company_availability` where (`lines` > 0 or `until_20` > 0 or `from_21_up_to_50` > 0 or `over_50` > 0) and (`message` like '%rtb operacao realizada com sucesso%' or `message` like '%endereco com cobertura%' or `message` like '%ha disponibilidade para o recurso%' or `message` like '%ha portas disponiveis%'));

-- abr tested
delete from `characteristic_association` where `characteristic_id` = 7;
insert into `brdn_erp`.`characteristic_association` select distinct null, 7, `company_id`, now() from `brdn_erp`.`company_line` where `testing_timestamp` is not null;

-- is from mobile list
delete from `characteristic_association` where `characteristic_id` = 10;
insert into `brdn_erp`.`characteristic_association` select distinct null, 10, `id`, now() from `brdn_erp`.`company` where `state_registry` is not null;

-- gpon
delete from `characteristic_association` where `characteristic_id` = 5;
insert into `characteristic_association` select distinct null, 5, `id`, now() from `company` where `id` in (select `company_id` from `company_address` where `id` in (select `company_address_id` from `company_availability` where `tecnology` = 'gpon'));

-- new google mined companies
delete from `characteristic_association` where `characteristic_id` = 11;
insert into `characteristic_association` select distinct distinct null, 11, `company_id`, now() from `company_address` where `zipcode_id` in (select `id` from `geography_zipcode` where `city_id` in (28, 159, 192, 173));

-- availability tested
delete from `characteristic_association` where `characteristic_id` = 12;
insert into `characteristic_association` select distinct null, 12, `company_id`, now() from `company_address` where `id` in (select `company_address_id` from `company_availability`);