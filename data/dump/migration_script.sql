-- ----------------------------------------------------------------------------
-- MySQL Workbench Migration
-- Migrated Schemata: brdn_erp
-- Source Schemata: brdn_erp
-- Created: Mon Aug 12 00:01:55 2019
-- Workbench Version: 8.0.16
-- ----------------------------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------------------------------------------------------
-- Schema brdn_erp
-- ----------------------------------------------------------------------------
DROP SCHEMA IF EXISTS `brdn_erp` ;
CREATE SCHEMA IF NOT EXISTS `brdn_erp` ;

-- ----------------------------------------------------------------------------
-- Table brdn_erp._tmp_zipcodes_using
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`_tmp_zipcodes_using` (
  `zipcode_id` INT(11) UNSIGNED NULL DEFAULT NULL,
  INDEX `ix|_tmp_zipcodes_using` (`zipcode_id` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

-- ----------------------------------------------------------------------------
-- Table brdn_erp._tmp_zipcodes_using_not
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`_tmp_zipcodes_using_not` (
  `id` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  INDEX `ix|_tmp_zipcodes_using_not` (`id` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.account
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`account` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `cnpj` CHAR(14) NOT NULL,
  `name` VARCHAR(200) NULL DEFAULT NULL,
  `site` VARCHAR(160) NULL DEFAULT NULL,
  `email` VARCHAR(160) NULL DEFAULT NULL,
  `phone` VARCHAR(160) NULL DEFAULT NULL,
  `blocked` INT(1) UNSIGNED NOT NULL DEFAULT '0',
  `latitude` DECIMAL(12,9) NULL DEFAULT NULL,
  `longitude` DECIMAL(12,9) NULL DEFAULT NULL,
  `signature_fee` DECIMAL(7,2) NULL DEFAULT NULL,
  `timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uc|account` (`cnpj` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.account_config
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`account_config` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `account_id` INT(11) UNSIGNED NOT NULL,
  `parameter` VARCHAR(60) NOT NULL,
  `value` VARCHAR(1000) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uc|account_config` (`account_id` ASC, `parameter` ASC) VISIBLE,
  CONSTRAINT `fk|account_config|account`
    FOREIGN KEY (`account_id`)
    REFERENCES `brdn_erp`.`account` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.account_contact
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`account_contact` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `account_id` INT(11) UNSIGNED NOT NULL,
  `name` VARCHAR(120) NOT NULL,
  `email` VARCHAR(60) NULL DEFAULT NULL,
  `phone` VARCHAR(15) NULL DEFAULT NULL,
  `cel_phone` VARCHAR(15) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uc|account_contact` (`account_id` ASC, `email` ASC) VISIBLE,
  CONSTRAINT `fk|account_contact|account`
    FOREIGN KEY (`account_id`)
    REFERENCES `brdn_erp`.`account` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.account_payment_history
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`account_payment_history` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `account_id` INT(11) UNSIGNED NOT NULL,
  `year` INT(11) UNSIGNED NOT NULL,
  `month` INT(11) UNSIGNED NOT NULL,
  `payment` DECIMAL(12,9) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uc|account_payment_history` (`account_id` ASC, `year` ASC, `month` ASC) VISIBLE,
  CONSTRAINT `fk|account_payment_history|account`
    FOREIGN KEY (`account_id`)
    REFERENCES `brdn_erp`.`account` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.account_signature_fee_history
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`account_signature_fee_history` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `account_id` INT(11) UNSIGNED NOT NULL,
  `year` INT(11) UNSIGNED NOT NULL,
  `month` INT(11) UNSIGNED NOT NULL,
  `signature_fee` DECIMAL(7,2) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uc|account_signature_fee_history` (`account_id` ASC, `year` ASC, `month` ASC) VISIBLE,
  CONSTRAINT `fk|account_signature_fee_history|account`
    FOREIGN KEY (`account_id`)
    REFERENCES `brdn_erp`.`account` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.activity
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`activity` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `account_id` INT(11) UNSIGNED NOT NULL,
  `name` VARCHAR(60) NOT NULL,
  `daily_count` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uc|activity` (`name` ASC) VISIBLE,
  INDEX `fk|activity|account` (`account_id` ASC) VISIBLE,
  CONSTRAINT `fk|activity|account`
    FOREIGN KEY (`account_id`)
    REFERENCES `brdn_erp`.`account` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.activity_allowed_user
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`activity_allowed_user` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `activity_id` INT(11) UNSIGNED NOT NULL,
  `user_id` INT(11) UNSIGNED NOT NULL,
  `timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk|activity_allowed_user|user_idx` (`user_id` ASC) VISIBLE,
  INDEX `fk|activity_allowed_user|activity_idx` (`activity_id` ASC) VISIBLE,
  CONSTRAINT `fk|activity_allowed_user|activity`
    FOREIGN KEY (`activity_id`)
    REFERENCES `brdn_erp`.`activity` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk|activity_allowed_user|user`
    FOREIGN KEY (`user_id`)
    REFERENCES `brdn_erp`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 12
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.activity_characteristic_must
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`activity_characteristic_must` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `activity_id` INT(11) UNSIGNED NOT NULL,
  `characteristic_id` INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uc|activity_characteristic_must` (`activity_id` ASC, `characteristic_id` ASC) VISIBLE,
  INDEX `fk|activity_characteristic_must|characteristic` (`characteristic_id` ASC) VISIBLE,
  CONSTRAINT `fk|activity_characteristic_must|activity`
    FOREIGN KEY (`activity_id`)
    REFERENCES `brdn_erp`.`activity` (`id`),
  CONSTRAINT `fk|activity_characteristic_must|characteristic`
    FOREIGN KEY (`characteristic_id`)
    REFERENCES `brdn_erp`.`characteristic` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.activity_characteristic_must_not
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`activity_characteristic_must_not` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `activity_id` INT(11) UNSIGNED NOT NULL,
  `characteristic_id` INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uc|activity_characteristic_must_not` (`activity_id` ASC, `characteristic_id` ASC) VISIBLE,
  INDEX `fk|activity_characteristic_must_not|characteristic` (`characteristic_id` ASC) VISIBLE,
  CONSTRAINT `fk|activity_characteristic_must_not|activity`
    FOREIGN KEY (`activity_id`)
    REFERENCES `brdn_erp`.`activity` (`id`),
  CONSTRAINT `fk|activity_characteristic_must_not|characteristic`
    FOREIGN KEY (`characteristic_id`)
    REFERENCES `brdn_erp`.`characteristic` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.activity_characteristic_on_conclusion_awarded
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`activity_characteristic_on_conclusion_awarded` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `activity_id` INT(11) UNSIGNED NOT NULL,
  `characteristic_id` INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uc|activity_characteristic_on_conclusion_awarded` (`activity_id` ASC, `characteristic_id` ASC) VISIBLE,
  INDEX `fk|activity_characteristic_on_conclusion_awarded|characteristic` (`characteristic_id` ASC) VISIBLE,
  CONSTRAINT `fk|activity_characteristic_on_conclusion_awarded|activity`
    FOREIGN KEY (`activity_id`)
    REFERENCES `brdn_erp`.`activity` (`id`),
  CONSTRAINT `fk|activity_characteristic_on_conclusion_awarded|characteristic`
    FOREIGN KEY (`characteristic_id`)
    REFERENCES `brdn_erp`.`characteristic` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.activity_characteristic_on_conclusion_revoked
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`activity_characteristic_on_conclusion_revoked` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `activity_id` INT(11) UNSIGNED NOT NULL,
  `characteristic_id` INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uc|activity_characteristic_on_conclusion_revoked` (`activity_id` ASC, `characteristic_id` ASC) VISIBLE,
  INDEX `fk|activity_characteristic_on_conclusion_revoked|characteristic` (`characteristic_id` ASC) VISIBLE,
  CONSTRAINT `fk|activity_characteristic_on_conclusion_revoked|activity`
    FOREIGN KEY (`activity_id`)
    REFERENCES `brdn_erp`.`activity` (`id`),
  CONSTRAINT `fk|activity_characteristic_on_conclusion_revoked|characteristic`
    FOREIGN KEY (`characteristic_id`)
    REFERENCES `brdn_erp`.`characteristic` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.activity_item
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`activity_item` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `activity_id` INT(11) UNSIGNED NOT NULL,
  `user_id` INT(11) UNSIGNED NOT NULL,
  `entity_id` INT(11) UNSIGNED NOT NULL,
  `finished_in` TIMESTAMP NULL DEFAULT NULL,
  `result` TINYINT(1) UNSIGNED NULL DEFAULT NULL,
  `timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uc|activity_item` (`activity_id` ASC, `user_id` ASC, `entity_id` ASC) VISIBLE,
  INDEX `fk|activity_item|user` (`user_id` ASC) VISIBLE,
  INDEX `ix|activity_item|entity` (`entity_id` ASC) VISIBLE,
  CONSTRAINT `fk|activity_item|activity`
    FOREIGN KEY (`activity_id`)
    REFERENCES `brdn_erp`.`activity` (`id`),
  CONSTRAINT `fk|activity_item|user`
    FOREIGN KEY (`user_id`)
    REFERENCES `brdn_erp`.`user` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 1375
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.activity_item_log
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`activity_item_log` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `activity_id` INT(11) UNSIGNED NOT NULL,
  `user_id` INT(11) UNSIGNED NOT NULL,
  `action` INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk|activity_item_log|user` (`user_id` ASC) VISIBLE,
  INDEX `fk|activity_item_log|activity` (`activity_id` ASC) VISIBLE,
  CONSTRAINT `fk|activity_item_log|activity`
    FOREIGN KEY (`activity_id`)
    REFERENCES `brdn_erp`.`activity` (`id`),
  CONSTRAINT `fk|activity_item_log|user`
    FOREIGN KEY (`user_id`)
    REFERENCES `brdn_erp`.`user` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.analytics_statistic
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`analytics_statistic` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type_id` INT(11) UNSIGNED NOT NULL,
  `name` VARCHAR(60) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uc|statistic` (`type_id` ASC, `name` ASC) VISIBLE,
  CONSTRAINT `fk|statistic|statistic_type`
    FOREIGN KEY (`type_id`)
    REFERENCES `brdn_erp`.`analytics_statistic_type` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.analytics_statistic_type
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`analytics_statistic_type` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(60) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uc|statistic_type` (`name` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.analytics_stats
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`analytics_stats` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `statistic_id` INT(11) UNSIGNED NOT NULL,
  `user_id` INT(11) UNSIGNED NULL DEFAULT NULL,
  `entity_id` INT(11) UNSIGNED NULL DEFAULT NULL,
  `timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk|statistics|user` (`user_id` ASC) VISIBLE,
  INDEX `fk|statistics|statistic` (`statistic_id` ASC) VISIBLE,
  CONSTRAINT `fk|statistics|statistic`
    FOREIGN KEY (`statistic_id`)
    REFERENCES `brdn_erp`.`analytics_statistic` (`id`),
  CONSTRAINT `fk|statistics|user`
    FOREIGN KEY (`user_id`)
    REFERENCES `brdn_erp`.`user` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 17
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.blocking_message
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`blocking_message` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `text` VARCHAR(60) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uc|blocking_message` (`text` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.characteristic
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`characteristic` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `account_id` INT(11) UNSIGNED NOT NULL,
  `name` VARCHAR(60) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uc|characteristic_type` (`name` ASC) VISIBLE,
  INDEX `fk|characteristic|account` (`account_id` ASC) VISIBLE,
  CONSTRAINT `fk|characteristic|account`
    FOREIGN KEY (`account_id`)
    REFERENCES `brdn_erp`.`account` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 10
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.characteristic_association
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`characteristic_association` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `characteristic_id` INT(11) UNSIGNED NOT NULL,
  `entity_id` INT(11) UNSIGNED NOT NULL,
  `timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uc|characteristic_association` (`characteristic_id` ASC, `entity_id` ASC) VISIBLE,
  INDEX `ix|characteristic_association|entity` (`entity_id` ASC) VISIBLE,
  CONSTRAINT `fk|characteristic_association|characteristic`
    FOREIGN KEY (`characteristic_id`)
    REFERENCES `brdn_erp`.`characteristic` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 387094
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.characteristic_association_log
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`characteristic_association_log` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `characteristic_id` INT(11) UNSIGNED NOT NULL,
  `entity_id` INT(11) UNSIGNED NOT NULL,
  `action` TINYINT(1) UNSIGNED NOT NULL,
  `timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk|characteristic_association_log|characteristic` (`characteristic_id` ASC) VISIBLE,
  INDEX `ix|characteristic_association_log|entity` (`entity_id` ASC) VISIBLE,
  CONSTRAINT `fk|characteristic_association_log|characteristic`
    FOREIGN KEY (`characteristic_id`)
    REFERENCES `brdn_erp`.`characteristic` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.company
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`company` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `account_id` INT(11) UNSIGNED NOT NULL,
  `company_subtype_id` INT(11) UNSIGNED NULL DEFAULT NULL,
  `cnpj` VARCHAR(16) NULL DEFAULT NULL,
  `site` VARCHAR(160) NULL DEFAULT NULL,
  `ddd` VARCHAR(3) NULL DEFAULT NULL,
  `phone` VARCHAR(10) NULL DEFAULT NULL,
  `operator_id` INT(11) UNSIGNED NULL DEFAULT NULL,
  `description` VARCHAR(200) NULL DEFAULT NULL,
  `formal_name` VARCHAR(90) NULL DEFAULT NULL,
  `fantasy_name` VARCHAR(90) NOT NULL,
  `state_registry` VARCHAR(60) NULL DEFAULT NULL,
  `images_url` VARCHAR(1200) NULL DEFAULT NULL,
  `timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uc|company` (`cnpj` ASC) VISIBLE,
  INDEX `fk|company|account` (`account_id` ASC) VISIBLE,
  INDEX `fk|company|phone_operator` (`operator_id` ASC) VISIBLE,
  INDEX `ix|company|fantasy_name` (`fantasy_name` ASC) VISIBLE,
  INDEX `fk|company|company_subtype` (`company_subtype_id` ASC) VISIBLE,
  CONSTRAINT `fk|company|account`
    FOREIGN KEY (`account_id`)
    REFERENCES `brdn_erp`.`account` (`id`),
  CONSTRAINT `fk|company|company_subtype`
    FOREIGN KEY (`company_subtype_id`)
    REFERENCES `brdn_erp`.`company_subtype` (`id`),
  CONSTRAINT `fk|company|phone_operator`
    FOREIGN KEY (`operator_id`)
    REFERENCES `brdn_erp`.`phone_operator` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 81317
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.company_address
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`company_address` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `company_id` INT(11) UNSIGNED NOT NULL,
  `street` VARCHAR(600) NOT NULL,
  `number` VARCHAR(12) NULL DEFAULT NULL,
  `compliment` VARCHAR(60) NULL DEFAULT NULL,
  `zipcode_id` INT(11) UNSIGNED NULL DEFAULT NULL,
  `testing_timestamp` DATE NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uc|company_address` (`company_id` ASC, `street`(255) ASC, `number` ASC, `zipcode_id` ASC) VISIBLE,
  INDEX `fk|company_address|zipcode` (`zipcode_id` ASC) VISIBLE,
  CONSTRAINT `fk|company_address|company`
    FOREIGN KEY (`company_id`)
    REFERENCES `brdn_erp`.`company` (`id`),
  CONSTRAINT `fk|company_address|zipcode`
    FOREIGN KEY (`zipcode_id`)
    REFERENCES `brdn_erp`.`geography_zipcode` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 46833
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.company_availability
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`company_availability` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `company_address_id` INT(11) UNSIGNED NOT NULL,
  `message` VARCHAR(460) NULL DEFAULT NULL,
  `closet_distance` INT(11) NULL DEFAULT NULL,
  `lines` INT(11) NULL DEFAULT NULL,
  `until_20` INT(11) NULL DEFAULT NULL,
  `from_21_up_to_50` INT(11) NULL DEFAULT NULL,
  `over_50` INT(11) NULL DEFAULT NULL,
  `network` VARCHAR(40) NULL DEFAULT NULL,
  `tecnology` VARCHAR(40) NULL DEFAULT NULL,
  `closet_top_speed` INT(11) NULL DEFAULT NULL,
  `tecnology_for_tv` VARCHAR(40) NULL DEFAULT NULL,
  `timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `pk|company_availability` (`company_address_id` ASC) VISIBLE,
  INDEX `ix|company_availability|lines` (`lines` ASC) VISIBLE,
  INDEX `ix|company_availability|speeds` (`until_20` ASC, `from_21_up_to_50` ASC, `over_50` ASC) VISIBLE,
  CONSTRAINT `fk|company_availability|company_address`
    FOREIGN KEY (`company_address_id`)
    REFERENCES `brdn_erp`.`company_address` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 45772
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.company_call
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`company_call` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) UNSIGNED NOT NULL,
  `company_id` INT(11) UNSIGNED NOT NULL,
  `contact_id` INT(11) UNSIGNED NULL DEFAULT NULL,
  `tabulation_id` INT(11) UNSIGNED NULL DEFAULT NULL,
  `started_in` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ended_in` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk|company_call|user` (`user_id` ASC) VISIBLE,
  INDEX `fk|company_call|company` (`company_id` ASC) VISIBLE,
  INDEX `fk|company_call|tabulation` (`tabulation_id` ASC) VISIBLE,
  INDEX `fk|company_call|contact` (`contact_id` ASC) VISIBLE,
  CONSTRAINT `fk|company_call|company`
    FOREIGN KEY (`company_id`)
    REFERENCES `brdn_erp`.`company` (`id`),
  CONSTRAINT `fk|company_call|contact`
    FOREIGN KEY (`contact_id`)
    REFERENCES `brdn_erp`.`company_contact` (`id`),
  CONSTRAINT `fk|company_call|tabulation`
    FOREIGN KEY (`tabulation_id`)
    REFERENCES `brdn_erp`.`tabulation` (`id`),
  CONSTRAINT `fk|company_call|user`
    FOREIGN KEY (`user_id`)
    REFERENCES `brdn_erp`.`user` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 241
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.company_callback
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`company_callback` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) UNSIGNED NOT NULL,
  `product_id` INT(11) UNSIGNED NOT NULL,
  `company_id` INT(11) UNSIGNED NOT NULL,
  `contact_id` INT(11) UNSIGNED NOT NULL,
  `call_time` DATETIME NOT NULL,
  `done` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk|company_callback|user` (`user_id` ASC) VISIBLE,
  INDEX `fk|company_callback|product` (`product_id` ASC) VISIBLE,
  INDEX `fk|company_callback|company` (`company_id` ASC) VISIBLE,
  INDEX `fk|company_callback|contact` (`contact_id` ASC) VISIBLE,
  CONSTRAINT `fk|company_callback|company`
    FOREIGN KEY (`company_id`)
    REFERENCES `brdn_erp`.`company` (`id`),
  CONSTRAINT `fk|company_callback|contact`
    FOREIGN KEY (`contact_id`)
    REFERENCES `brdn_erp`.`company_contact` (`id`),
  CONSTRAINT `fk|company_callback|product`
    FOREIGN KEY (`product_id`)
    REFERENCES `brdn_erp`.`product` (`id`),
  CONSTRAINT `fk|company_callback|user`
    FOREIGN KEY (`user_id`)
    REFERENCES `brdn_erp`.`user` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.company_check_request
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`company_check_request` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `company_id` INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uc|company_check_request` (`company_id` ASC) VISIBLE,
  CONSTRAINT `fk|company_check_request|company`
    FOREIGN KEY (`company_id`)
    REFERENCES `brdn_erp`.`company` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 17
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.company_contact
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`company_contact` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) UNSIGNED NOT NULL,
  `company_id` INT(11) UNSIGNED NOT NULL,
  `position_id` INT(11) UNSIGNED NOT NULL,
  `department_id` INT(11) UNSIGNED NOT NULL,
  `name` VARCHAR(120) NOT NULL,
  `email` VARCHAR(60) NULL DEFAULT NULL,
  `phone` VARCHAR(15) NULL DEFAULT NULL,
  `cel_phone` VARCHAR(15) NULL DEFAULT NULL,
  `timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk|company_contact|user` (`user_id` ASC) VISIBLE,
  INDEX `fk|company_contact|company` (`company_id` ASC) VISIBLE,
  INDEX `fk|company_contact|position` (`position_id` ASC) VISIBLE,
  INDEX `fk|company_contact|department` (`department_id` ASC) VISIBLE,
  CONSTRAINT `fk|company_contact|company`
    FOREIGN KEY (`company_id`)
    REFERENCES `brdn_erp`.`company` (`id`),
  CONSTRAINT `fk|company_contact|department`
    FOREIGN KEY (`department_id`)
    REFERENCES `brdn_erp`.`contact_department` (`id`),
  CONSTRAINT `fk|company_contact|position`
    FOREIGN KEY (`position_id`)
    REFERENCES `brdn_erp`.`contact_position` (`id`),
  CONSTRAINT `fk|company_contact|user`
    FOREIGN KEY (`user_id`)
    REFERENCES `brdn_erp`.`user` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.company_installation_address
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`company_installation_address` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) UNSIGNED NOT NULL,
  `company_id` INT(11) UNSIGNED NOT NULL,
  `reference` VARCHAR(250) NOT NULL,
  `street` VARCHAR(250) NOT NULL,
  `number` VARCHAR(20) NOT NULL,
  `zipcode_id` INT(11) UNSIGNED NOT NULL,
  `timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk|company_installation_address|user` (`user_id` ASC) VISIBLE,
  INDEX `fk|company_installation_address|company` (`company_id` ASC) VISIBLE,
  INDEX `fk|company_installation_address|zipcode` (`zipcode_id` ASC) VISIBLE,
  CONSTRAINT `fk|company_installation_address|company`
    FOREIGN KEY (`company_id`)
    REFERENCES `brdn_erp`.`company` (`id`),
  CONSTRAINT `fk|company_installation_address|user`
    FOREIGN KEY (`user_id`)
    REFERENCES `brdn_erp`.`user` (`id`),
  CONSTRAINT `fk|company_installation_address|zipcode`
    FOREIGN KEY (`zipcode_id`)
    REFERENCES `brdn_erp`.`geography_zipcode` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.company_note
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`company_note` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) UNSIGNED NOT NULL,
  `company_id` INT(11) UNSIGNED NOT NULL,
  `contact_id` INT(11) UNSIGNED NULL DEFAULT NULL,
  `comment` VARCHAR(120) NOT NULL,
  `timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk|company_note|user` (`user_id` ASC) VISIBLE,
  INDEX `fk|company_note|company` (`company_id` ASC) VISIBLE,
  INDEX `fk|company_note|contact` (`contact_id` ASC) VISIBLE,
  CONSTRAINT `fk|company_note|company`
    FOREIGN KEY (`company_id`)
    REFERENCES `brdn_erp`.`company` (`id`),
  CONSTRAINT `fk|company_note|contact`
    FOREIGN KEY (`contact_id`)
    REFERENCES `brdn_erp`.`company_contact` (`id`),
  CONSTRAINT `fk|company_note|user`
    FOREIGN KEY (`user_id`)
    REFERENCES `brdn_erp`.`user` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 27
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.company_priority
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`company_priority` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `company_id` INT(11) UNSIGNED NOT NULL,
  `priority` INT(11) NOT NULL DEFAULT '0',
  `timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk|company_priority|company` (`company_id` ASC) VISIBLE,
  CONSTRAINT `fk|company_priority|company`
    FOREIGN KEY (`company_id`)
    REFERENCES `brdn_erp`.`company` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.company_subtype
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`company_subtype` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `company_type_id` INT(11) UNSIGNED NOT NULL,
  `subtype` VARCHAR(60) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uc|company_subtype` (`company_type_id` ASC, `subtype` ASC) VISIBLE,
  CONSTRAINT `fk|company_subtype|company_type`
    FOREIGN KEY (`company_type_id`)
    REFERENCES `brdn_erp`.`company_type` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.company_type
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`company_type` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` VARCHAR(60) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uc|company_type` (`type` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.concurrency
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`concurrency` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `action` VARCHAR(60) NOT NULL,
  `user_id` INT(11) UNSIGNED NULL DEFAULT NULL,
  `machine_name` VARCHAR(160) NULL DEFAULT NULL,
  `entity_id` INT(11) NULL DEFAULT NULL,
  `entity_value` VARCHAR(160) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uc|concurrency` (`action` ASC, `user_id` ASC, `machine_name` ASC) VISIBLE,
  INDEX `fk|concurrency|user` (`user_id` ASC) VISIBLE,
  INDEX `ix|concurrency|action` (`action` ASC) VISIBLE,
  CONSTRAINT `fk|concurrency|user`
    FOREIGN KEY (`user_id`)
    REFERENCES `brdn_erp`.`user` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 52754
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.contact_department
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`contact_department` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `account_id` INT(11) UNSIGNED NOT NULL,
  `name` VARCHAR(120) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uc|contact_department` (`account_id` ASC, `name` ASC) VISIBLE,
  CONSTRAINT `pk|contact_department|account`
    FOREIGN KEY (`account_id`)
    REFERENCES `brdn_erp`.`account` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 12
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.contact_position
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`contact_position` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `account_id` INT(11) UNSIGNED NOT NULL,
  `name` VARCHAR(120) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uc|contact_position` (`account_id` ASC, `name` ASC) VISIBLE,
  CONSTRAINT `pk|contact_position|account`
    FOREIGN KEY (`account_id`)
    REFERENCES `brdn_erp`.`account` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 9
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.geography_city
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`geography_city` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `state_id` INT(11) UNSIGNED NULL DEFAULT NULL,
  `name` VARCHAR(60) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uc|geography_city` (`state_id` ASC, `name` ASC) VISIBLE,
  CONSTRAINT `fk|geography_city|state`
    FOREIGN KEY (`state_id`)
    REFERENCES `brdn_erp`.`geography_state` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 655
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.geography_country
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`geography_country` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(60) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uc|geography_country` (`name` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.geography_neighbourhood
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`geography_neighbourhood` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `city_id` INT(11) UNSIGNED NULL DEFAULT NULL,
  `name` VARCHAR(60) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uc|geography_neighbourhood` (`city_id` ASC, `name` ASC) VISIBLE,
  CONSTRAINT `fk|geography_neighbourhood|city`
    FOREIGN KEY (`city_id`)
    REFERENCES `brdn_erp`.`geography_city` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 760233
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.geography_state
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`geography_state` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `country_id` INT(11) UNSIGNED NOT NULL,
  `name` VARCHAR(60) NOT NULL,
  `abbreviation` VARCHAR(60) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uc|geography_state` (`country_id` ASC, `name` ASC) VISIBLE,
  CONSTRAINT `fk|geography_state|country`
    FOREIGN KEY (`country_id`)
    REFERENCES `brdn_erp`.`geography_country` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 28
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.geography_zipcode
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`geography_zipcode` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `zipcode` CHAR(8) NOT NULL,
  `city_id` INT(11) UNSIGNED NULL DEFAULT NULL,
  `neighbourhood_id` INT(11) UNSIGNED NULL DEFAULT NULL,
  `location` VARCHAR(240) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uc|geography_zipcode` (`zipcode` ASC) VISIBLE,
  INDEX `fk|geography_zipcode|city` (`city_id` ASC) VISIBLE,
  INDEX `fk|geography_zipcode|neighbourhood` (`neighbourhood_id` ASC) VISIBLE,
  CONSTRAINT `fk|geography_zipcode|city`
    FOREIGN KEY (`city_id`)
    REFERENCES `brdn_erp`.`geography_city` (`id`),
  CONSTRAINT `fk|geography_zipcode|neighbourhood`
    FOREIGN KEY (`neighbourhood_id`)
    REFERENCES `brdn_erp`.`geography_neighbourhood` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 190819
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.log
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`log` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) UNSIGNED NOT NULL,
  `target_id` INT(11) UNSIGNED NOT NULL,
  `action` VARCHAR(40) NOT NULL,
  `timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk|log|user` (`user_id` ASC) VISIBLE,
  CONSTRAINT `fk|log|user`
    FOREIGN KEY (`user_id`)
    REFERENCES `brdn_erp`.`user` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.log_dataset_change
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`log_dataset_change` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) UNSIGNED NOT NULL,
  `action` VARCHAR(40) NULL DEFAULT NULL,
  `entity_type` VARCHAR(60) NULL DEFAULT NULL,
  `entity_summary` VARCHAR(600) NULL DEFAULT NULL,
  `timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk|log_dataset_change|user` (`user_id` ASC) VISIBLE,
  CONSTRAINT `fk|log_dataset_change|user`
    FOREIGN KEY (`user_id`)
    REFERENCES `brdn_erp`.`user` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 398
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.log_error
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`log_error` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) UNSIGNED NULL DEFAULT NULL,
  `class_name` VARCHAR(60) NOT NULL,
  `method_name` VARCHAR(60) NOT NULL,
  `exception_type` VARCHAR(60) NOT NULL,
  `exception_message` VARCHAR(60) NOT NULL,
  `exception_stack_trace` VARCHAR(60) NOT NULL,
  `timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk|log_error|user` (`user_id` ASC) VISIBLE,
  CONSTRAINT `fk|log_error|user`
    FOREIGN KEY (`user_id`)
    REFERENCES `brdn_erp`.`user` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.log_property_change
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`log_property_change` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) UNSIGNED NOT NULL,
  `entity_type` VARCHAR(60) NULL DEFAULT NULL,
  `property_name` VARCHAR(60) NULL DEFAULT NULL,
  `property_value_old` VARCHAR(120) NULL DEFAULT NULL,
  `property_value_new` VARCHAR(120) NULL DEFAULT NULL,
  `timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk|log_property_change|user` (`user_id` ASC) VISIBLE,
  CONSTRAINT `fk|log_property_change|user`
    FOREIGN KEY (`user_id`)
    REFERENCES `brdn_erp`.`user` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.macro_login
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`macro_login` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `macro` VARCHAR(60) NOT NULL,
  `url` VARCHAR(160) NOT NULL,
  `login` VARCHAR(160) NOT NULL,
  `password` VARCHAR(160) NOT NULL,
  `additional_info_01` VARCHAR(160) NULL DEFAULT NULL,
  `additional_info_02` VARCHAR(160) NULL DEFAULT NULL,
  `additional_info_03` VARCHAR(160) NULL DEFAULT NULL,
  `last_login_error` VARCHAR(160) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uc|macro_login` (`macro` ASC) VISIBLE,
  INDEX `ix|macro_login|macro` (`macro` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.permission
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`permission` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `account_id` INT(11) UNSIGNED NULL DEFAULT NULL,
  `name` VARCHAR(60) NOT NULL,
  `priority` INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uc|permission` (`account_id` ASC, `name` ASC) VISIBLE,
  CONSTRAINT `fk|permission|account`
    FOREIGN KEY (`account_id`)
    REFERENCES `brdn_erp`.`account` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 14
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.phone_operator
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`phone_operator` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(60) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uc|phone_operator` (`name` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 32
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.phone_type
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`phone_type` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(60) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uc|phone_type` (`name` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.product
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`product` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `account_id` INT(11) UNSIGNED NOT NULL,
  `name` VARCHAR(60) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uc|product` (`account_id` ASC, `name` ASC) VISIBLE,
  CONSTRAINT `fk|product|account`
    FOREIGN KEY (`account_id`)
    REFERENCES `brdn_erp`.`account` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.sale
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`sale` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `company_id` INT(11) UNSIGNED NOT NULL,
  `user_id` INT(11) UNSIGNED NOT NULL,
  `product_id` INT(11) UNSIGNED NOT NULL,
  `timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `approved_by` INT(11) UNSIGNED NOT NULL,
  `approved_in` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk|sale|user` (`user_id` ASC) VISIBLE,
  INDEX `fk|sale|product` (`product_id` ASC) VISIBLE,
  INDEX `fk|sale|company` (`company_id` ASC) VISIBLE,
  INDEX `fk|sale|user_approve` (`approved_by` ASC) VISIBLE,
  CONSTRAINT `fk|sale|company`
    FOREIGN KEY (`company_id`)
    REFERENCES `brdn_erp`.`company` (`id`),
  CONSTRAINT `fk|sale|product`
    FOREIGN KEY (`product_id`)
    REFERENCES `brdn_erp`.`product` (`id`),
  CONSTRAINT `fk|sale|user`
    FOREIGN KEY (`user_id`)
    REFERENCES `brdn_erp`.`user` (`id`),
  CONSTRAINT `fk|sale|user_approve`
    FOREIGN KEY (`approved_by`)
    REFERENCES `brdn_erp`.`user` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.secure_zone
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`secure_zone` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `account_id` INT(11) UNSIGNED NOT NULL,
  `name` VARCHAR(60) NOT NULL,
  `latitude` DECIMAL(12,9) NOT NULL,
  `longitude` DECIMAL(12,9) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uc|secure_zone` (`name` ASC) VISIBLE,
  INDEX `fk|secure_zone|account` (`account_id` ASC) VISIBLE,
  CONSTRAINT `fk|secure_zone|account`
    FOREIGN KEY (`account_id`)
    REFERENCES `brdn_erp`.`account` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.status
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`status` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `status_type_id` INT(11) UNSIGNED NOT NULL,
  `name` VARCHAR(60) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uc|status` (`status_type_id` ASC, `name` ASC) VISIBLE,
  CONSTRAINT `fk|status|status_type`
    FOREIGN KEY (`status_type_id`)
    REFERENCES `brdn_erp`.`status_type` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.status_type
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`status_type` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(60) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uc|status_type` (`name` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.system_parameter
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`system_parameter` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `parameter` VARCHAR(60) NOT NULL,
  `value` VARCHAR(1000) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uc|system_parameter` (`parameter` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.tabulation
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`tabulation` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tabulation_type_id` INT(11) UNSIGNED NOT NULL,
  `text` VARCHAR(60) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uc|blocking_message` (`text` ASC) VISIBLE,
  INDEX `fk|tabulation|tabulation_type` (`tabulation_type_id` ASC) VISIBLE,
  CONSTRAINT `fk|tabulation|tabulation_type`
    FOREIGN KEY (`tabulation_type_id`)
    REFERENCES `brdn_erp`.`tabulation_type` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 18
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.tabulation_type
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`tabulation_type` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` VARCHAR(60) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uc|tabulation_type` (`type` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.user
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`user` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(60) NOT NULL,
  `phone` VARCHAR(15) NULL DEFAULT NULL,
  `email` VARCHAR(60) NOT NULL,
  `password` VARCHAR(60) NOT NULL,
  `active` INT(11) UNSIGNED NOT NULL DEFAULT '1',
  `blocked` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uc|user` (`email` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 52
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.user_area
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`user_area` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) UNSIGNED NOT NULL,
  `cities` VARCHAR(450) NULL DEFAULT NULL,
  `neighbourhoods` VARCHAR(450) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uc|user_area` (`user_id` ASC) VISIBLE,
  CONSTRAINT `pk|user_area|user`
    FOREIGN KEY (`user_id`)
    REFERENCES `brdn_erp`.`user` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.user_permission
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`user_permission` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) UNSIGNED NOT NULL,
  `permission_id` INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uc|user_permission` (`user_id` ASC, `permission_id` ASC) VISIBLE,
  INDEX `fk|user_permission|permission` (`permission_id` ASC) VISIBLE,
  CONSTRAINT `fk|user_permission|permission`
    FOREIGN KEY (`permission_id`)
    REFERENCES `brdn_erp`.`permission` (`id`),
  CONSTRAINT `fk|user_permission|user`
    FOREIGN KEY (`user_id`)
    REFERENCES `brdn_erp`.`user` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 52
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table brdn_erp.user_white_list
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `brdn_erp`.`user_white_list` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uc|user_white_list` (`user_id` ASC) VISIBLE,
  CONSTRAINT `fk|user_white_list|user`
    FOREIGN KEY (`user_id`)
    REFERENCES `brdn_erp`.`user` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;
SET FOREIGN_KEY_CHECKS = 1;
