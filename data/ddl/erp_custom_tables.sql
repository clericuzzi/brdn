drop table if exists `sale`;
drop table if exists `soho_availability`;

drop table if exists `company_evaluation`;
drop table if exists `company_address`;
drop table if exists `company_comment`;
drop table if exists `company_contact`;
drop table if exists `company_callback`;
drop table if exists `company_note`;
drop table if exists `company_call`;
drop table if exists `company`;

drop table if exists `phone_operator`;

drop table if exists `company_position`;
drop table if exists `company_department`;

drop table if exists `product`;
/*
drop table if exists `plan_product`;
drop table if exists `plan`;

create table `plan` (
	`id` int(11) unsigned not null auto_increment,
	`account_id` int(11) unsigned not null,
	`name` varchar(120) not null,
	`data` int(11) not null default '0',
	`minutes` int(11) not null default '0',
	`ilimited` int(1) not null default '0',
	`campaign` varchar(120) not null,
	`discount_value` decimal(6,2) not null default '0.00',
	`discount_percentage` decimal(5,2) not null default '0.00',
	`timestamp` timestamp not null default current_timestamp,
	primary key (`id`),
	unique key `uc|plan` (`account_id`,`name`),
	constraint `fk|plan|account` foreign key (`account_id`) references `account` (`id`)
) engine=innodb auto_increment=4 default charset=utf8;
create table `plan_product` (
	`id` int(11) unsigned not null auto_increment,
	`account_id` int(11) unsigned not null,
	`plan_id` int(11) unsigned not null,
	`product_id` int(11) unsigned not null,
	`timestamp` timestamp not null default current_timestamp,
	constraint `pk|company` primary key (`id`),
	constraint `fk|company|account` foreign key (`account_id`) references `account` (`id`),
	constraint `uc|company` unique (`cnpj`)
) engine=innodb default charset=utf8;
*/

create table `product` (
	`id` int(11) unsigned not null auto_increment,
	`account_id` int(11) unsigned not null,
	`name` varchar(60) default null,
	constraint `pk|product` primary key (`id`),
	constraint `fk|product|account` foreign key (`account_id`) references `account` (`id`),
	constraint `uc|product` unique (`account_id`, `name`)
) engine=innodb default charset=utf8;

create table `phone_type` (
	`id` int(11) unsigned not null auto_increment,
	`name` varchar(60) default null,
	constraint `pk|phone_type` primary key (`id`),
	constraint `uc|phone_type` unique (`name`)
) engine=innodb default charset=utf8;
create table `phone_operator` (
	`id` int(11) unsigned not null auto_increment,
	`name` varchar(60) default null,
	constraint `pk|phone_operator` primary key (`id`),
	constraint `uc|phone_operator` unique (`name`)
) engine=innodb default charset=utf8;

create table `company` (
	`id` int(11) unsigned not null auto_increment,
	`account_id` int(11) unsigned not null,
	`cnpj` varchar(16) default null,
	`site` varchar(160) default null,
	`ddd` varchar(3) default null,
	`phone` varchar(10) default null,
	`operator_id` int(11) unsigned null,
	`description` varchar(200) default null,
	`formal_name` varchar(90) default null,
	`fantasy_name` varchar(90) not null,
	`state_registry` varchar(60) default null,
	`images_url` varchar(1200) default null,
	`timestamp` timestamp not null default now(),
	constraint `pk|company` primary key (`id`),
	constraint `fk|company|account` foreign key (`account_id`) references `account` (`id`),
	constraint `fk|company|phone_operator` foreign key (`operator_id`) references `phone_operator` (`id`),
	constraint `uc|company` unique (`cnpj`)
) engine=innodb default charset=utf8;
create table `company_call` (
	`id` int(11) unsigned not null auto_increment,
	`user_id` int(11) unsigned not null,
	`company_id` int(11) unsigned not null,
	`contact_id` int(11) unsigned null,
	`tabulation_id` int(11) unsigned null,
	`started_in` timestamp not null default now(),
	`ended_in` timestamp null,
	constraint `pk|company_call` primary key (`id`),
	constraint `fk|company_call|user` foreign key (`user_id`) references `user` (`id`),
	constraint `fk|company_call|company` foreign key (`company_id`) references `company` (`id`),
	constraint `fk|company_call|contact` foreign key (`contact_id`) references `company_contact` (`id`),
	constraint `fk|company_call|tabulation` foreign key (`tabulation_id`) references `tabulation` (`id`)
) engine=innodb default charset=utf8;
create table `company_address` (
	`id` int(11) unsigned not null auto_increment,
	`company_id` int(11) unsigned not null,
	`street` varchar(600) not null,
	`number` varchar(12) default null,
	`compliment` varchar(60) default null,
	`zipcode_id` int(11) unsigned default null,
	`testing_timestamp` date null,
	constraint `pk|company_address` primary key (`id`),
	constraint `fk|company_address|company` foreign key (`company_id`) references `company` (`id`),
	constraint `fk|company_address|zipcode` foreign key (`zipcode_id`) references `geography_zipcode` (`id`),
	constraint `uc|company_address` unique (`company_id`, `street`, `number`, `zipcode_id`)
) engine=innodb default charset=utf8;
create table `company_note` (
	`id` int(11) unsigned not null auto_increment,
	`user_id` int(11) unsigned not null,
	`company_id` int(11) unsigned not null,
	`contact_id` int(11) unsigned null,
	`comment` varchar(120) not null,
	`timestamp` timestamp not null default now(),
	constraint `pk|company_note` primary key (`id`),
	constraint `fk|company_note|user` foreign key (`user_id`) references `user` (`id`),
	constraint `fk|company_note|company` foreign key (`company_id`) references `company` (`id`),
	constraint `fk|company_note|contact` foreign key (`contact_id`) references `company_contact` (`id`)
) engine=innodb default charset=utf8;
create table `company_callback` (
	`id` int(11) unsigned not null auto_increment,
	`user_id` int(11) unsigned not null,
	`product_id` int(11) unsigned not null,
	`company_id` int(11) unsigned not null,
	`contact_id` int(11) unsigned not null,
	`call_time` date not null,
	`done` tinyint(1) unsigned not null default 0,
	`timestamp` timestamp not null default now(),
	constraint `pk|company_callback` primary key (`id`),
	constraint `fk|company_callback|user` foreign key (`user_id`) references `user` (`id`),
	constraint `fk|company_callback|product` foreign key (`product_id`) references `product` (`id`),
	constraint `fk|company_callback|company` foreign key (`company_id`) references `company` (`id`),
	constraint `fk|company_callback|contact` foreign key (`contact_id`) references `company_contact` (`id`)
) engine=innodb default charset=utf8;
create table `company_contact` (
	`id` int(11) unsigned not null auto_increment,
	`user_id` int(11) unsigned not null,
	`company_id` int(11) unsigned not null,
	`position_id` int(11) unsigned not null,
	`department_id` int(11) unsigned not null,
	`name` varchar(120) not null,
	`email` varchar(60) null,
	`phone` varchar(15) null,
	`cel_phone` varchar(15) null,
	`timestamp` timestamp not null default now(),
	constraint `pk|company_contact` primary key (`id`),
	constraint `fk|company_contact|user` foreign key (`user_id`) references `user` (`id`),
	constraint `fk|company_contact|company` foreign key (`company_id`) references `company` (`id`),
	constraint `fk|company_contact|position` foreign key (`position_id`) references `contact_position` (`id`),
	constraint `fk|company_contact|department` foreign key (`department_id`) references `contact_department` (`id`)
) engine=innodb default charset=utf8;
create table `company_availability` (
	`id` int(11) unsigned not null auto_increment,
	`company_address_id` int(11) unsigned not null,
	`message` varchar(460) default null,
	`closet_distance` int(11) default null,
	`lines` int(11) default null,
	`until_20` int(11) default null,
	`from_21_up_to_50` int(11) default null,
	`over_50` int(11) default null,
	`network` varchar(40) default null,
	`tecnology` varchar(40) default null,
	`closet_top_speed` int(11) default null,
	`tecnology_for_tv` varchar(40) default null,
	`timestamp` timestamp not null default now(),
	constraint `pk|company_availability` primary key (`id`),
	constraint `fk|company_availability|company_address` foreign key (`company_address_id`) references `company_address` (`id`),
    constraint `pk|company_availability` unique (`company_address_id`)
) engine=innodb default charset=utf8;
create index `ix|company_availability|lines` on `company_availability` (`lines`);
create index `ix|company_availability|speeds` on `company_availability` (`until_20`, `from_21_up_to_50`, `over_50`);

create table `sale` (
	`id` int(11) unsigned not null auto_increment,
	`company_id` int(11) unsigned not null,
	`user_id` int(11) unsigned not null,
	`product_id` int(11) unsigned not null,
	`timestamp` timestamp not null default now(),
	`approved_by` int(11) unsigned not null,
	`approved_in` timestamp null,
	constraint `pk|sale` primary key (`id`),
	constraint `fk|sale|user` foreign key (`user_id`) references `user` (`id`),
	constraint `fk|sale|product` foreign key (`product_id`) references `product` (`id`),
	constraint `fk|sale|company` foreign key (`company_id`) references `company` (`id`),
	constraint `fk|sale|user_approve` foreign key (`approved_by`) references `user` (`id`)
) engine=innodb default charset=utf8;

-- macros
drop table if exists `macro_login`;
create table `macro_login`(
	`id` int(11) unsigned not null auto_increment,
    `macro` varchar(60) not null,
    `url` varchar(160) not null,
    `login` varchar(160) not null,
    `password` varchar(160) not null,
    `additional_info_01` varchar(160) null,
    `additional_info_02` varchar(160) null,
    `additional_info_03` varchar(160) null,
    `last_login_error` varchar(160) null,
	constraint `pk|macro_login` primary key (`id`),
    constraint `uc|macro_login` unique (`macro`)
) engine=innodb default charset=utf8;
create index `ix|macro_login|macro` on `macro_login` (`macro`);

-- concurrency
drop table if exists `concurrency`;
create table `concurrency`(
	`id` int(11) unsigned not null auto_increment,
    `action` varchar(60) not null,
    `user_id` int(11) unsigned null,
    `machine_name` varchar(160) null,
    `entity_id` int(11) null,
    `entity_value` varchar(160) null,
	constraint `pk|concurrency` primary key (`id`),
	constraint `fk|concurrency|user` foreign key (`user_id`) references `user` (`id`),
    constraint `uc|concurrency` unique (`action`, `user_id`, `machine_name`)
) engine=innodb default charset=utf8;
create index `ix|concurrency|action` on `concurrency` (`action`);

-- custom tables
drop table if exists `user_area`;
create table `user_area` (
	`id` int(11) unsigned not null auto_increment,
	`user_id` int(11) unsigned not null,
	`cities` varchar(450) null,
	`neighbourhoods` varchar(450) null,
	constraint `pk|user_area` primary key (`id`),
	constraint `pk|user_area|user` foreign key (`user_id`) references `user` (`id`),
	constraint `uc|user_area` unique (`user_id`)
) engine=innodb default charset=utf8;

drop table if exists `contact_position`;
create table `contact_position` (
	`id` int(11) unsigned not null auto_increment,
	`account_id` int(11) unsigned not null,
	`name` varchar(120) not null,
	constraint `pk|contact_position` primary key (`id`),
	constraint `pk|contact_position|account` foreign key (`account_id`) references `account` (`id`),
	constraint `uc|contact_position` unique (`account_id`, `name`)
) engine=innodb default charset=utf8;
drop table if exists `contact_department`;
create table `contact_department` (
	`id` int(11) unsigned not null auto_increment,
	`account_id` int(11) unsigned not null,
	`name` varchar(120) not null,
	constraint `pk|contact_department` primary key (`id`),
	constraint `pk|contact_department|account` foreign key (`account_id`) references `account` (`id`),
	constraint `uc|contact_department` unique (`account_id`, `name`)
) engine=innodb default charset=utf8;

drop table if exists `tabulation_type`;
create table `tabulation_type` (
	`id` int(11) unsigned not null auto_increment,
	`type` varchar(60) not null,
	constraint `pk|tabulation_type` primary key (`id`),
	constraint `uc|tabulation_type` unique (`type`)
) engine=innodb default charset=utf8;
drop table if exists `tabulation`;
create table `tabulation` (
	`id` int(11) unsigned not null auto_increment,
	`text` varchar(60) not null,
	constraint `pk|blocking_message` primary key (`id`),
	constraint `uc|blocking_message` unique (`text`)
) engine=innodb default charset=utf8;

drop table if exists `company_type`;
create table `company_type` (
	`id` int(11) unsigned not null auto_increment,
	`type` varchar(60) not null,
	constraint `pk|company_type` primary key (`id`),
	constraint `uc|company_type` unique (`type`)
) engine=innodb default charset=utf8;
drop table if exists `company_subtype`;
create table `company_subtype` (
	`id` int(11) unsigned not null auto_increment,
	`company_type_id` int(11) unsigned not null,
	`subtype` varchar(60) not null,
	constraint `pk|company_subtype` primary key (`id`),
	constraint `fk|company_subtype|company_type` foreign key (`company_type_id`) references `company_type` (`id`),
	constraint `uc|company_subtype` unique (`company_type_id`, `subtype`)
) engine=innodb default charset=utf8;

drop table if exists `blocking_message`;
create table `blocking_message` (
	`id` int(11) unsigned not null auto_increment,
	`text` varchar(60) not null,
	constraint `pk|blocking_message` primary key (`id`),
	constraint `uc|blocking_message` unique (`text`)
) engine=innodb default charset=utf8;

drop table if exists `company_installation_address`;
create table `company_installation_address` (
	`id` int(11) unsigned not null auto_increment,
	`user_id` int(11) unsigned not null,
	`company_id` int(11) unsigned not null,
	`reference` varchar(250) not null,
	`street` varchar(250) not null,
	`number` varchar(20) not null,
	`zipcode_id` int(11) unsigned not null,
    `timestamp` timestamp not null default now(),
	constraint `pk|company_installation_address` primary key (`id`),
	constraint `fk|company_installation_address|user` foreign key (`user_id`) references `user` (`id`),
	constraint `fk|company_installation_address|company` foreign key (`company_id`) references `company` (`id`),
	constraint `fk|company_installation_address|zipcode` foreign key (`zipcode_id`) references `geography_zipcode` (`id`)
) engine=innodb default charset=utf8;

drop table if exists `mining_google`;
create table `mining_google` (
	`id` int(11) unsigned not null auto_increment,
	`city_id` int(11) unsigned not null,
	`company_subtype_id` int(11) unsigned not null,
	constraint `pk|mining_google` primary key (`id`),
	constraint `fk|mining_google|city` foreign key (`city_id`) references `geography_city` (`id`),
	constraint `fk|mining_google|company_subtype` foreign key (`company_subtype_id`) references `company_subtype` (`id`),
	constraint `uc|mining_google` unique (`city_id`, `company_subtype_id`)
) engine=innodb default charset=utf8;