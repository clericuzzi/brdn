select
	case when availability_test is not null then 'tested' else 'not tested' end
    , count(0)
    , (count(0) / (select count(0) from company)) * 100 as `%`
from
	company
group by
	case when availability_test is not null then 'tested' else 'not tested' end
with rollup;