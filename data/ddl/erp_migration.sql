insert into `brdn_erp`.`account` select * from `brdn`.`account`;
insert into `brdn_erp`.`permission` select `id`, 1, `name`, `priority` from `brdn`.`permission`;

insert into `brdn_erp`.`user` select * from `brdn`.`user`;
insert into `brdn_erp`.`user_permission` select `id`, `user_id`, `permission_id` from `brdn`.`user_permission`;

insert into `brdn_erp`.`geography_country` select `id`, `name` from `brdn`.`country`;
insert into `brdn_erp`.`geography_state` select `id`, `country_id`, `name`, `abbreviation` from `brdn`.`state`;
insert into `brdn_erp`.`geography_city` select `id`, `state_id`, `name` from `brdn`.`city`;
insert into `brdn_erp`.`geography_neighbourhood` select `id`, `city_id`, `name` from `brdn`.`neighbourhood`;
insert into `brdn_erp`.`geography_zipcode` select distinct `id`, `zip`, `city_id`, `neighbourhood_id`, null from `brdn`.`zipcode`;

insert into `brdn_erp`.`company` select `id`, `account_id`, `cnpj`, `site`, `ddd`, `phone`, null, `description`, `formal_name`, `fantasy_name`, `state_registry`, null, `timestamp` from `brdn`.`company`;
insert into `brdn_erp`.`company_address` select null, `company_id`, `street`, `number`, `compliment`, `zipcode_id`, null from `brdn`.`company_address`;

delete from `brdn_erp`.`macro_login`;
insert into `brdn_erp`.`macro_login` values
	(1, 'web-vendas', 'https://webvendas.gvt.com.br/siebelview/dealer/login.sv', '80596357', 'VIVO@1011', null, null, null, null)
,	(2, 'catta', 'https://www.catta.com.br/entrar', 'm2p', 'isupw2kth', null, null, null, null)
,	(3, 'web-vendas-new', 'https://vivovendas.vivo.com.br', '80503032', 'VIVO#3006', null, null, null, null)
,	(4, 'vivo-corp', 'https://vivocorp-parceiro.vivo.com.br', '80503032', 'VIVO#3006', null, null, null, null)
,	(5, 'web-vendas-availability', 'https://vivovendas.vivo.com.br', '80697317', 'Brdn@012', null, null, null, null);

insert into `phone_type` values (null, 'fixo'), (null, 'celular');