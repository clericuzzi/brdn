drop schema if exists `brdn_erp`;
create schema `brdn_erp`;
use `brdn_erp`;

-- system parameters
create table `system_parameter` (
	`id` int(11) unsigned not null auto_increment,
	`parameter` varchar(60) not null,
	`value` varchar(1000) not null,
	constraint `pk|system_parameter` primary key (`id`),
	constraint `uc|system_parameter` unique (`parameter`)
) engine=innodb default charset=utf8;

-- statuses
create table `status_type` (
	`id` int(11) unsigned not null auto_increment,
	`name` varchar(60) not null,
	constraint `pk|status_type` primary key (`id`),
	constraint `uc|status_type` unique (`name`)
) engine=innodb default charset=utf8;
create table `status` (
	`id` int(11) unsigned not null auto_increment,
	`status_type_id` int(11) unsigned not null,
	`name` varchar(60) not null,
	constraint `pk|status` primary key (`id`),
	constraint `fk|status|status_type` foreign key (`status_type_id`) references `status_type` (`id`),
	constraint `uc|status` unique (`status_type_id`, `name`)
) engine=innodb default charset=utf8;

-- users
create table `permission` (
	`id` int(11) unsigned not null auto_increment,
	`account_id` int(11) unsigned null,
	`name` varchar(60) not null,
	`priority` int(11) unsigned not null,
	constraint `pk|permission` primary key (`id`),
	constraint `uc|permission` unique (`account_id`, `name`)
) engine=innodb default charset=utf8;
create table `user` (
	`id` int(11) unsigned not null auto_increment,
	`name` varchar(60) not null,
	`phone` varchar(15) null,
	`email` varchar(60) not null,
	`password` varchar(60) not null,
	`active` int (11) unsigned not null default 1,
	`blocked` int (11) unsigned not null default 0,
	`timestamp` timestamp not null default now(),
	constraint `pk|user` primary key (`id`),
	constraint `uc|user` unique (`email`)
) engine=innodb default charset=utf8;
create table `user_permission` (
	`id` int(11) unsigned not null auto_increment,
	`user_id` int(11) unsigned not null,
	`permission_id` int(11) unsigned not null,
	constraint `pk|user_permission` primary key (`id`),
	constraint `fk|user_permission|user` foreign key (`user_id`) references `user` (`id`),
	constraint `fk|user_permission|permission` foreign key (`permission_id`) references `permission` (`id`),
	constraint `uc|user_permission` unique (`user_id`, `permission_id`)
) engine=innodb default charset=utf8;

-- account related data
create table `account` (
	`id` int(11) unsigned not null auto_increment,
	`cnpj` char(14) not null,
	`name` varchar(200) null,
    `site` varchar(160) null,
    `email` varchar(160) null,
    `phone` varchar(160) null,
    `blocked` int(1) unsigned not null default 0,
	`latitude` decimal(12, 9) null default null,
	`longitude` decimal(12, 9) null default null,
	`signature_fee` decimal(7, 2) null default null,
	`timestamp` timestamp not null default now(),
	constraint `pk|account` primary key (`id`),
	constraint `uc|account` unique (`cnpj`)
) engine=innodb default charset=utf8;
alter table `permission` add constraint `fk|permission|account` foreign key (`account_id`) references `account` (`id`);
create table `account_config` (
	`id` int(11) unsigned not null auto_increment,
	`account_id` int(11) unsigned not null,
	`parameter` varchar(60) not null,
	`value` varchar(1000) not null,
	constraint `pk|account_config` primary key (`id`),
	constraint `fk|account_config|account` foreign key (`account_id`) references `account` (`id`),
	constraint `uc|account_config` unique (`account_id`, `parameter`)
) engine=innodb default charset=utf8;
create table `account_contact` (
	`id` int(11) unsigned not null auto_increment,
	`account_id` int(11) unsigned not null,
	`name` varchar(120) not null,
	`email` varchar(60) null,
	`phone` varchar(15) null,
	`cel_phone` varchar(15) null,
	constraint `pk|account_contact` primary key (`id`),
	constraint `fk|account_contact|account` foreign key (`account_id`) references `account` (`id`),
	constraint `uc|account_contact` unique (`account_id`, `email`)
) engine=innodb default charset=utf8;
create table `account_payment_history` (
	`id` int(11) unsigned not null auto_increment,
	`account_id` int(11) unsigned not null,
	`year` int(11) unsigned not null,
	`month` int(11) unsigned not null,
	`payment` decimal(12, 9) unsigned not null,
	constraint `pk|account_payment_history` primary key (`id`),
	constraint `fk|account_payment_history|account` foreign key (`account_id`) references `account` (`id`),
	constraint `uc|account_payment_history` unique (`account_id`, `year`, `month`)
) engine=innodb default charset=utf8;
create table `account_signature_fee_history` (
	`id` int(11) unsigned not null auto_increment,
	`account_id` int(11) unsigned not null,
	`year` int(11) unsigned not null,
	`month` int(11) unsigned not null,
	`signature_fee` decimal(7, 2) unsigned not null,
	constraint `pk|account_signature_fee_history` primary key (`id`),
	constraint `fk|account_signature_fee_history|account` foreign key (`account_id`) references `account` (`id`),
	constraint `uc|account_signature_fee_history` unique (`account_id`, `year`, `month`)
) engine=innodb default charset=utf8;

-- login verification
create table `secure_zone`(
	`id` int(11) unsigned not null auto_increment,
	`account_id` int(11) unsigned not null,
    `name` varchar(60) not null,
	`latitude` decimal(12, 9) not null,
	`longitude` decimal(12, 9) not null,
	constraint `pk|secure_zone` primary key (`id`),
	constraint `fk|secure_zone|account` foreign key (`account_id`) references `account` (`id`),
	constraint `uc|secure_zone` unique (`name`)
) engine=innodb default charset=utf8;
create table `user_white_list`(
	`id` int(11) unsigned not null auto_increment,
    `user_id` int(11) unsigned not null,
	constraint `pk|user_white_list` primary key (`id`),
	constraint `fk|user_white_list|user` foreign key (`user_id`) references `user` (`id`),
	constraint `uc|user_white_list` unique (`user_id`)
) engine=innodb default charset=utf8;

create table `geography_country` (
	`id` int(11) unsigned not null auto_increment,
	`name` varchar(60) not null,
	constraint `pk|geography_country` primary key (`id`),
	constraint `uc|geography_country` unique (`name`)
) engine=innodb default charset=utf8;
create table `geography_state` (
	`id` int(11) unsigned not null auto_increment,
	`country_id` int(11) unsigned not null,
	`name` varchar(60) not null,
	`abbreviation` varchar(60) not null,
	constraint `pk|geography_state` primary key (`id`),
	constraint `fk|geography_state|country` foreign key (`country_id`) references `geography_country` (`id`),
	constraint `uc|geography_state` unique (`country_id`, `name`)
) engine=innodb default charset=utf8;
create table `geography_city` (
	`id` int(11) unsigned not null auto_increment,
	`state_id` int(11) unsigned null,
	`name` varchar(60) not null,
	constraint `pk|geography_city` primary key (`id`),
	constraint `fk|geography_city|state` foreign key (`state_id`) references `geography_state` (`id`),
	constraint `uc|geography_city` unique (`state_id`, `name`)
) engine=innodb default charset=utf8;
create table `geography_neighbourhood` (
	`id` int(11) unsigned not null auto_increment,
	`city_id` int(11) unsigned null,
	`name` varchar(60) not null,
	constraint `pk|geography_neighbourhood` primary key (`id`),
	constraint `fk|geography_neighbourhood|city` foreign key (`city_id`) references `geography_city` (`id`),
	constraint `uc|geography_neighbourhood` unique (`city_id`, `name`)
) engine=innodb default charset=utf8;
create table `geography_zipcode` (
	`id` int(11) unsigned not null auto_increment,
	`zipcode` char(8) not null,
	`city_id` int(11) unsigned null,
	`neighbourhood_id` int(11) unsigned null,
	`location` varchar(240) null,
	constraint `pk|geography_zipcode` primary key (`id`),
	constraint `fk|geography_zipcode|city` foreign key (`city_id`) references `geography_city` (`id`),
	constraint `fk|geography_zipcode|neighbourhood` foreign key (`neighbourhood_id`) references `geography_neighbourhood` (`id`),
	constraint `uc|geography_zipcode` unique (`zipcode`)
) engine=innodb default charset=utf8;

drop table if exists `analytics_stats`;
drop table if exists `analytics_statistic`;
drop table if exists `analytics_statistic_type`;

create table `analytics_statistic_type`
(
	`id` int(11) unsigned not null auto_increment,
	`name` varchar(60) not null,
	constraint `pk|statistic_type` primary key (`id`),
    constraint `uc|statistic_type` unique (`name`)
) engine=innodb default charset=utf8;
create table `analytics_statistic`
(
	`id` int(11) unsigned not null auto_increment,
	`type_id` int(11) unsigned not null,
	`name` varchar(60) not null,
	constraint `pk|statistic` primary key (`id`),
	constraint `fk|statistic|statistic_type` foreign key (`type_id`) references `analytics_statistic_type` (`id`),
    constraint `uc|statistic` unique (`type_id`, `name`)
) engine=innodb default charset=utf8;
create table `analytics_stats`
(
	`id` int(11) unsigned not null auto_increment,
	`statistic_id` int(11) unsigned not null,
	`user_id` int(11) unsigned null,
	`entity_id` int(11) unsigned null,
	`timestamp` timestamp not null default now(),
	constraint `pk|statistics` primary key (`id`),
	constraint `fk|statistics|user` foreign key (`user_id`) references `user` (`id`),
	constraint `fk|statistics|statistic` foreign key (`statistic_id`) references `analytics_statistic` (`id`)
) engine=innodb default charset=utf8;

-- drops
drop table if exists `activity_characteristic_on_conclusion_revoked`;
drop table if exists `activity_characteristic_on_conclusion_awarded`;
drop table if exists `activity_characteristic_must_not`;
drop table if exists `activity_characteristic_must`;
drop table if exists `activity_allowed_user`;
drop table if exists `activity_item_log`;
drop table if exists `activity_item`;
drop table if exists `activity`;
drop table if exists `characteristic_association`;
drop table if exists `characteristic_association_log`;
drop table if exists `characteristic`;

-- characteristics
create table `characteristic`
(
	`id` int(11) unsigned not null auto_increment,
	`account_id` int(11) unsigned not null,
	`name` varchar(60) not null,
	constraint `pk|characteristic_type` primary key (`id`),
	constraint `fk|characteristic|account` foreign key (`account_id`) references `account` (`id`),
    constraint `uc|characteristic_type` unique (`name`)
) engine=innodb default charset=utf8;
create table `characteristic_association`
(
	`id` int(11) unsigned not null auto_increment,
	`characteristic_id` int(11) unsigned not null,
	`entity_id` int(11) unsigned not null,
	`timestamp` timestamp not null,
	constraint `pk|characteristic_association` primary key (`id`),
	constraint `fk|characteristic_association|characteristic` foreign key (`characteristic_id`) references `characteristic` (`id`),
    constraint `uc|characteristic_association` unique (`characteristic_id`, `entity_id`)
) engine=innodb default charset=utf8;
create index `ix|characteristic_association|entity` on `characteristic_association` (`entity_id`);
create table `characteristic_association_log`
(
	`id` int(11) unsigned not null auto_increment,
	`characteristic_id` int(11) unsigned not null,
	`entity_id` int(11) unsigned not null,
	`action` tinyint(1) unsigned not null,
	`timestamp` timestamp not null,
	constraint `pk|characteristic_association_log` primary key (`id`),
	constraint `fk|characteristic_association_log|characteristic` foreign key (`characteristic_id`) references `characteristic` (`id`)
) engine=innodb default charset=utf8;
create index `ix|characteristic_association_log|entity` on `characteristic_association_log` (`entity_id`);

-- activities
create table `activity`
(
	`id` int(11) unsigned not null auto_increment,
	`account_id` int(11) unsigned not null,
	`name` varchar(60) not null,
	`daily_count` int(11) null,
	constraint `pk|activity` primary key (`id`),
	constraint `fk|activity|account` foreign key (`account_id`) references `account` (`id`),
    constraint `uc|activity` unique (`name`)
) engine=innodb default charset=utf8;
create table `activity_item`
(
	`id` int(11) unsigned not null auto_increment,
	`activity_id` int(11) unsigned not null,
	`user_id` int(11) unsigned not null,
	`entity_id` int(11) unsigned not null,
	`finished_in` timestamp null,
	`result` tinyint(1) unsigned null,
	`timestamp` timestamp not null default now(),
	constraint `pk|activity_item` primary key (`id`),
	constraint `fk|activity_item|user` foreign key (`user_id`) references `user` (`id`),
	constraint `fk|activity_item|activity` foreign key (`activity_id`) references `activity` (`id`),
    constraint `uc|activity_item` unique (`activity_id`, `user_id`, `entity_id`)
) engine=innodb default charset=utf8;
create index `ix|activity_item|entity` on `activity_item` (`entity_id`);
create table `activity_item_log`
(
	`id` int(11) unsigned not null auto_increment,
	`activity_id` int(11) unsigned not null,
	`user_id` int(11) unsigned not null,
	`action` int(11) unsigned not null,
	constraint `pk|activity_item_log` primary key (`id`),
	constraint `fk|activity_item_log|user` foreign key (`user_id`) references `user` (`id`),
	constraint `fk|activity_item_log|activity` foreign key (`activity_id`) references `activity` (`id`)
) engine=innodb default charset=utf8;

create table `activity_allowed_user`
(
	`id` int(11) unsigned not null auto_increment,
	`activity_id` int(11) unsigned not null,
	`user_id` int(11) unsigned not null,
	`timestamp` timestamp not null default now(),
	constraint `pk|activity_allowed_user` primary key (`id`),
	constraint `fk|activity_allowed_user|activity_item` foreign key (`activity_id`) references `activity_item` (`id`)
) engine=innodb default charset=utf8;
create table `activity_characteristic_must`
(
	`id` int(11) unsigned not null auto_increment,
	`activity_id` int(11) unsigned not null,
	`characteristic_id` int(11) unsigned not null,
	constraint `pk|activity_characteristic_must` primary key (`id`),
	constraint `fk|activity_characteristic_must|activity` foreign key (`activity_id`) references `activity` (`id`),
	constraint `fk|activity_characteristic_must|characteristic` foreign key (`characteristic_id`) references `characteristic` (`id`),
    constraint `uc|activity_characteristic_must` unique (`activity_id`, `characteristic_id`)
) engine=innodb default charset=utf8;
create table `activity_characteristic_must_not`
(
	`id` int(11) unsigned not null auto_increment,
	`activity_id` int(11) unsigned not null,
	`characteristic_id` int(11) unsigned not null,
	constraint `pk|activity_characteristic_must` primary key (`id`),
	constraint `fk|activity_characteristic_must_not|activity` foreign key (`activity_id`) references `activity` (`id`),
	constraint `fk|activity_characteristic_must_not|characteristic` foreign key (`characteristic_id`) references `characteristic` (`id`),
    constraint `uc|activity_characteristic_must_not` unique (`activity_id`, `characteristic_id`)
) engine=innodb default charset=utf8;
create table `activity_characteristic_on_conclusion_awarded`
(
	`id` int(11) unsigned not null auto_increment,
	`activity_id` int(11) unsigned not null,
	`characteristic_id` int(11) unsigned not null,
	constraint `pk|activity_characteristic_on_conclusion_awarded` primary key (`id`),
	constraint `fk|activity_characteristic_on_conclusion_awarded|activity` foreign key (`activity_id`) references `activity` (`id`),
	constraint `fk|activity_characteristic_on_conclusion_awarded|characteristic` foreign key (`characteristic_id`) references `characteristic` (`id`),
    constraint `uc|activity_characteristic_on_conclusion_awarded` unique (`activity_id`, `characteristic_id`)
) engine=innodb default charset=utf8;
create table `activity_characteristic_on_conclusion_revoked`
(
	`id` int(11) unsigned not null auto_increment,
	`activity_id` int(11) unsigned not null,
	`characteristic_id` int(11) unsigned not null,
	constraint `pk|activity_characteristic_on_conclusion_revoked` primary key (`id`),
	constraint `fk|activity_characteristic_on_conclusion_revoked|activity` foreign key (`activity_id`) references `activity` (`id`),
	constraint `fk|activity_characteristic_on_conclusion_revoked|characteristic` foreign key (`characteristic_id`) references `characteristic` (`id`),
    constraint `uc|activity_characteristic_on_conclusion_revoked` unique (`activity_id`, `characteristic_id`)
) engine=innodb default charset=utf8;