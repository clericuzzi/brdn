drop table if exists `log_property_change`;
drop table if exists `log_dataset_change`;
drop table if exists `log_error`;
drop table if exists `log`;

-- logs
create table `log` (
	`id` int(11) unsigned not null auto_increment,
	`user_id` int(11) unsigned not null,
	`target_id` int(11) unsigned not null,
	`action` varchar(40) not null,
	`timestamp` timestamp not null default now(),
	constraint `pk|log` primary key (`id`),
	constraint `fk|log|user` foreign key (`user_id`) references `user` (`id`)
) engine=innodb default charset=utf8;
create table `log_error` (
	`id` int(11) unsigned not null auto_increment,
	`user_id` int(11) unsigned null,
	`class_name` varchar(60) not null,
	`method_name` varchar(60) not null,
	`exception_type` varchar(60) not null,
	`exception_message` varchar(60) not null,
	`exception_stack_trace` varchar(60) not null,
	`timestamp` timestamp not null default now(),
	constraint `pk|log_error` primary key (`id`),
	constraint `fk|log_error|user` foreign key (`user_id`) references `user` (`id`)
) engine=innodb default charset=utf8;
create table `log_dataset_change` (
	`id` int(11) unsigned not null auto_increment,
	`user_id` int(11) unsigned not null,
	`action` varchar(40) null,
	`entity_type` varchar(60) null,
	`entity_summary` varchar(600) null,
	`timestamp` timestamp not null default now(),
	constraint `pk|log_dataset_change` primary key (`id`),
	constraint `fk|log_dataset_change|user` foreign key (`user_id`) references `user` (`id`)
) engine=innodb default charset=utf8;
create table `log_property_change` (
	`id` int(11) unsigned not null auto_increment,
	`user_id` int(11) unsigned not null,
	`entity_type` varchar(60) null,
	`property_name` varchar(60) null,
	`property_value_old` varchar(120) null,
	`property_value_new` varchar(120) null,
	`timestamp` timestamp not null default now(),
	constraint `pk|log_property_change` primary key (`id`),
	constraint `fk|log_property_change|user` foreign key (`user_id`) references `user` (`id`)
) engine=innodb default charset=utf8;