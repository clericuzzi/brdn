delete from sales_campaign_user;
delete from sales_campaign_macro;
delete from sales_campaign_keyword;
delete from sales_campaign_zipcode;

delete from queue_sale_lines;
delete from queue_sale_review;
delete from queue_sale;

delete from company_address;
delete from company_contact;
delete from company_task;
delete from company;