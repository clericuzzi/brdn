drop schema if exists brdn;
create schema brdn;
use brdn;

-- categories
create table `category` (
	`id` int(11) unsigned not null auto_increment,
	`name` varchar(60) not null,
	constraint `pk|category` primary key (`id`),
	constraint `uc|category` unique (`name`)
) engine=innodb default charset=utf8;
create table `subcategory` (
	`id` int(11) unsigned not null auto_increment,
	`category_id` int(11) unsigned not null,
	`name` varchar(60) not null,
	constraint `pk|subcategory` primary key (`id`),
	constraint `fk|subcategory|category` foreign key (`category_id`) references `category` (`id`),
	constraint `uc|subcategory` unique (`category_id`, `name`)
) engine=innodb default charset=utf8;

-- cities
create table `country` (
	`id` int(11) unsigned not null auto_increment,
	`name` varchar(60) not null,
	constraint `pk|country` primary key (`id`),
	constraint `uc|country` unique (`name`)
) engine=innodb default charset=utf8;
insert into `country` values (null, 'brasil');
create table `state` (
	`id` int(11) unsigned not null auto_increment,
	`country_id` int(11) unsigned not null,
	`name` varchar(60) not null,
	`abbreviation` varchar(60) not null,
	constraint `pk|state` primary key (`id`),
	constraint `fk|state|country` foreign key (`country_id`) references `country` (`id`),
	constraint `uc|state` unique (`name`)
) engine=innodb default charset=utf8;
insert into `state` (`id`, `country_id`, `name`, `abbreviation`) values (null, 1, 'acre', 'ac'), (null, 1, 'alagoas', 'al'), (null, 1, 'amapá', 'ap'), (null, 1, 'amazonas', 'am'), (null, 1, 'bahia', 'ba'), (null, 1, 'ceará', 'ce'), (null, 1, 'distrito federal', 'df'), (null, 1, 'espírito santo', 'es'), (null, 1, 'goiás', 'go'), (null, 1, 'maranhão', 'ma'), (null, 1, 'mato grosso', 'mt'), (null, 1, 'mato grosso do sul', 'ms'), (null, 1, 'minas gerais', 'mg'), (null, 1, 'pará', 'pa'), (null, 1, 'paraíba', 'pb'), (null, 1, 'paraná', 'pr'), (null, 1, 'pernambuco', 'pe'), (null, 1, 'piauí', 'pi'), (null, 1, 'rio de janeiro', 'rj'), (null, 1, 'rio grande do norte', 'rn'), (null, 1, 'rio grande do sul', 'rs'), (null, 1, 'rondônia', 'ro'), (null, 1, 'roraima', 'rr'), (null, 1, 'santa catarina', 'sc'), (null, 1, 'são paulo', 'sp'), (null, 1, 'sergipe', 'se'), (null, 1, 'tocantins', 'to');
create table `city` (
	`id` int(11) unsigned not null auto_increment,
	`state_id` int(11) unsigned null,
	`name` varchar(60) not null,
	constraint `pk|city` primary key (`id`),
	constraint `fk|city|state` foreign key (`state_id`) references `state` (`id`),
	constraint `uc|city` unique (`name`)
) engine=innodb default charset=utf8;

-- statuses
create table `status_type` (
	`id` int(11) unsigned not null auto_increment,
	`name` varchar(60) not null,
	constraint `pk|status_type` primary key (`id`),
	constraint `uc|status_type` unique (`name`)
) engine=innodb default charset=utf8;
create table `status` (
	`id` int(11) unsigned not null auto_increment,
	`status_type_id` int(11) unsigned not null,
	`name` varchar(60) not null,
	constraint `pk|status` primary key (`id`),
	constraint `fk|status|status_type` foreign key (`status_type_id`) references `status_type` (`id`),
	constraint `uc|status` unique (`status_type_id`, `name`)
) engine=innodb default charset=utf8;

-- logs
create table `action_type` (
	`id` int(11) unsigned not null auto_increment,
	`name` varchar(60) not null,
	constraint `pk|action_type` primary key (`id`),
	constraint `uc|action_type` unique (`name`)
) engine=innodb default charset=utf8;
insert into `action_type` values (null, 'crud delete'), (null, 'crud insert'), (null, 'crud update');
create table `log` (
	`id` int(11) unsigned not null auto_increment,
	`user_id` int(11) unsigned not null,
	`action_id` int(11) unsigned not null,
	`message` varchar(400) not null,
	`timestamp` timestamp not null default now(),
	constraint `pk|log` primary key (`id`)
) engine=innodb default charset=utf8;
create table `error` (
	`id` int(11) unsigned not null auto_increment,
	`class_name` varchar(60) not null,
	`method_name` varchar(60) not null,
	`exception_type` varchar(60) not null,
	`exception_message` varchar(60) not null,
	`exception_stack_trace` varchar(60) not null,
	`timestamp` timestamp not null default now(),
	constraint `pk|error` primary key (`id`)
) engine=innodb default charset=utf8;
    
-- system parameters
drop table if exists `system_parameter`;
create table `system_parameter` (
	`id` int(11) unsigned not null auto_increment,
	`parameter` varchar(60) not null,
	`value` varchar(1000) not null,
	constraint `pk|system_parameter` primary key (`id`),
	constraint `uc|system_parameter` unique (`parameter`)
) engine=innodb default charset=utf8;

-- users
create table `permission` (
	`id` int(11) unsigned not null auto_increment,
	`name` varchar(60) not null,
	`priority` int(11) unsigned not null,
	constraint `pk|permission` primary key (`id`),
	constraint `uc|permission` unique (`name`)
) engine=innodb default charset=utf8;
create table `user` (
	`id` int(11) unsigned not null auto_increment,
	`name` varchar(60) not null,
	`phone` varchar(15) null,
	`email` varchar(60) not null,
	`password` varchar(60) not null,
	`active` int (11) unsigned not null default 1,
	`blocked` int (11) unsigned not null default 0,
	`timestamp` timestamp not null default now(),
	constraint `pk|user` primary key (`id`),
	constraint `uc|user` unique (`email`)
) engine=innodb default charset=utf8;
create table `user_permission` (
	`id` int(11) unsigned not null auto_increment,
	`account_id` int(11) unsigned not null,
	`user_id` int(11) unsigned not null,
	`permission_id` int(11) unsigned not null,
	constraint `pk|user_permission` primary key (`id`),
	constraint `fk|user_permission|user` foreign key (`user_id`) references `user` (`id`),
	constraint `fk|user_permission|permission` foreign key (`permission_id`) references `permission` (`id`),
	constraint `uc|user_permission` unique (`user_id`, `permission_id`)
) engine=innodb default charset=utf8;

-- phone types
create table `phone_type` (
	`id` int(11) unsigned not null auto_increment,
	`name` varchar(60) not null,
	constraint `pk|phone_type` primary key (`id`),
	constraint `uc|phone_type` unique (`name`)
) engine=innodb default charset=utf8;
insert into `phone_type` values (null, 'fixo'), (null, 'celular'), (null, 'comercial'), (null, 'residencial');

-- account related data
create table `account` (
	`id` int(11) unsigned not null auto_increment,
	`cnpj` char(14) not null,
	`name` varchar(200) null,
    `site` varchar(160) null,
    `email` varchar(160) null,
    `phone` varchar(160) null,
    `blocked` int(1) unsigned not null default 0,
	`latitude` decimal(12, 9) not null,
	`longitude` decimal(12, 9) not null,
	`signature_fee` decimal(7, 2) not null default 0,
	`timestamp` timestamp not null default now(),
	constraint `pk|account` primary key (`id`),
	constraint `uc|account` unique (`cnpj`)
) engine=innodb default charset=utf8;
alter table `user_permission` add constraint `fk|user_permission|account` foreign key (`account_id`) references `account` (`id`);
create table `account_user` (
	`id` int(11) unsigned not null auto_increment,
	`account_id` int(11) unsigned not null,
	`user_id` int(11) unsigned not null,
	`active` int (11) unsigned not null default 1,
	`blocked` int (11) unsigned not null default 0,
	`timestamp` timestamp not null default now(),
	constraint `pk|account_user` primary key (`id`),
	constraint `fk|account_user|account` foreign key (`account_id`) references `account` (`id`),
	constraint `fk|account_user|user` foreign key (`user_id`) references `user` (`id`),
	constraint `uc|account_user` unique (`account_id`, `user_id`)
) engine=innodb default charset=utf8;
create table `account_config` (
	`id` int(11) unsigned not null auto_increment,
	`account_id` int(11) unsigned not null,
	`parameter` varchar(60) not null,
	`value` varchar(1000) not null,
	constraint `pk|account_config` primary key (`id`),
	constraint `fk|account_config|account` foreign key (`account_id`) references `account` (`id`),
	constraint `uc|account_config` unique (`account_id`, `parameter`)
) engine=innodb default charset=utf8;
create table `account_contact` (
	`id` int(11) unsigned not null auto_increment,
	`account_id` int(11) unsigned not null,
	`person_id` int(11) unsigned not null,
	constraint `pk|account_contact` primary key (`id`),
	constraint `fk|account_contact|account` foreign key (`account_id`) references `account` (`id`),
	constraint `uc|account_contact` unique (`account_id`, `person_id`)
) engine=innodb default charset=utf8;
create table `account_payment_history` (
	`id` int(11) unsigned not null auto_increment,
	`account_id` int(11) unsigned not null,
	`year` int(11) unsigned not null,
	`month` int(11) unsigned not null,
	`payment` decimal(12, 9) unsigned not null,
	constraint `pk|account_payment_history` primary key (`id`),
	constraint `fk|account_payment_history|account` foreign key (`account_id`) references `account` (`id`),
	constraint `uc|account_payment_history` unique (`account_id`, `year`, `month`)
) engine=innodb default charset=utf8;
create table `account_signature_fee_history` (
	`id` int(11) unsigned not null auto_increment,
	`account_id` int(11) unsigned not null,
	`year` int(11) unsigned not null,
	`month` int(11) unsigned not null,
	`signature_fee` decimal(7, 2) unsigned not null,
	constraint `pk|account_signature_fee_history` primary key (`id`),
	constraint `fk|account_signature_fee_history|account` foreign key (`account_id`) references `account` (`id`),
	constraint `uc|account_signature_fee_history` unique (`account_id`, `year`, `month`)
) engine=innodb default charset=utf8;

-- login verification
create table `secure_zone`(
	`id` int(11) unsigned not null auto_increment,
	`account_id` int(11) unsigned not null,
    `name` varchar(60) not null,
	`latitude` decimal(12, 9) not null,
	`longitude` decimal(12, 9) not null,
	constraint `pk|secure_zone` primary key (`id`),
	constraint `fk|secure_zone|account` foreign key (`account_id`) references `account` (`id`),
	constraint `uc|secure_zone` unique (`name`)
) engine=innodb default charset=utf8;
create table `user_white_list`(
	`id` int(11) unsigned not null auto_increment,
    `user_id` int(11) unsigned not null,
	constraint `pk|user_white_list` primary key (`id`),
	constraint `fk|user_white_list|user` foreign key (`user_id`) references `user` (`id`),
	constraint `uc|user_white_list` unique (`user_id`)
) engine=innodb default charset=utf8;

-- account triggers
delimiter |
create trigger `trg_insert_account` after insert
on `account`
for each row
begin

	insert into `account_config` values (null, new.id, 'enable_qr_login', '0');

end|
delimiter ;

-- person tables
create table `person`(
	`id` int(11) unsigned not null auto_increment,
	`account_id` int(11) unsigned not null,
    `sex` varchar(1) null,
    `name` varchar(120) null,
    `birth` date null,
    `document` varchar(20) null,
    `timestamp` timestamp not null default now(),
    `year` int(11) not null,
    `month` int(11) not null,
	constraint `pk|person` primary key (`id`),
	constraint `fk|person|account` foreign key (`account_id`) references `account` (`id`)
) engine=innodb default charset=utf8;
create index `ix|person|name` on `person` (`name`);
create index `ix|person|document` on `person` (`document`);
create index `ix|person|timestamp` on `person` (`timestamp`);
create index `ix|person|year|month` on `person` (`year`, `month`);
alter table `account_contact` add constraint `fk|account_contact|person` foreign key (`person_id`) references `person` (`id`);
create table `person_email`(
	`id` int(11) unsigned not null auto_increment,
    `person_id` int(11) unsigned not null,
    `email` varchar(120) not null,
    `main` int(1) not null default 0,
	constraint `pk|person_email` primary key (`id`),
	constraint `fk|person_email|person` foreign key (`person_id`) references `person` (`id`),
	constraint `uc|person_email` unique (`person_id`, `email`)
) engine=innodb default charset=utf8;
create index `ix|person_email|email` on `person_email` (`email`);
create table `person_phone`(
	`id` int(11) unsigned not null auto_increment,
    `person_id` int(11) unsigned not null,
    `phone_type_id` int(11) unsigned not null,
	`ddd` varchar(2) not null,
	`phone` varchar(12) not null,
    `operator` varchar(20),
    `main` int(1) not null default 0,
	constraint `pk|person_phone` primary key (`id`),
	constraint `fk|person_phone|person` foreign key (`person_id`) references `person` (`id`),
	constraint `fk|person_phone|phone_type` foreign key (`phone_type_id`) references `phone_type` (`id`),
	constraint `uc|person_phone` unique (`person_id`, `ddd`, `phone`)
) engine=innodb default charset=utf8;
alter table `person_phone` add column `operator` varchar(20) null after `phone`;
create table `person_address`(
	`id` int(11) unsigned not null auto_increment,
    `person_id` int(11) unsigned not null,
    `description` varchar(60) not null,
	`latitude` decimal(12, 9) not null,
	`longitude` decimal(12, 9) not null,
	`main` int(1) not null default 0,
	constraint `pk|person_address` primary key (`id`),
	constraint `fk|person_address|person` foreign key (`person_id`) references `person` (`id`),
	constraint `uc|person_address` unique (`person_id`, `description`)
) engine=innodb default charset=utf8;
create index `ix|person_address|description` on `person_address` (`description`);
create table `person_address_full`(
	`id` int(11) unsigned not null auto_increment,
    `person_id` int(11) unsigned not null,
    `street` varchar(160) not null,
    `number` varchar(160) not null,
    `compliment` varchar(160) not null,
    `zip` varchar(8) not null,
    `region` varchar(160) not null,
    `city` varchar(160) not null,
    `state` varchar(2) not null,
	constraint `pk|person_address_full` primary key (`id`),
	constraint `fk|person_address_full|person` foreign key (`person_id`) references `person` (`id`)
) engine=innodb default charset=utf8;

-- ec2
create table `ec2_account` (
	`id` int(11) unsigned not null auto_increment,    
	`account_id` int(11) unsigned not null,
	`name` varchar(200) not null,
	`access_key` varchar(200) not null,
	`secret_key` varchar(200) not null,
	constraint `pk|ec2_account` primary key (`id`),
	constraint `fk|ec2_account|account` foreign key (`account_id`) references `account` (`id`),
	constraint `uc|ec2_account` unique (`name`)
) engine=innodb default charset=utf8;
create index `ix|ec2_account|name` on `ec2_account` (`name`);
create table `ec2_instance_type` (
	`id` int(11) unsigned not null auto_increment,
	`name` varchar(200) not null,
	`hour_rate` decimal(6,4) not null default 0,
	constraint `pk|ec2_instance_type` primary key (`id`),
	constraint `uc|ec2_instance_type` unique (`name`)
) engine=innodb default charset=utf8;
insert into `ec2_instance_type` values (null, 't2.nano', 0), (null, 't2.micro', 0), (null, 't2.small', 0), (null, 't2.medium', 0), (null, 't2.large', 0), (null, 't2.xlarge', 0), (null, 't2.2xlarge', 0);
create table `ec2_instance` (
	`id` int(11) unsigned not null auto_increment,
	`ec2_account_id` int(11) unsigned not null,
	`ec2_instance_type_id` int(11) unsigned not null default 1,
	`instance_id` varchar(200) not null,
	`name` varchar(200) not null,
	`enabled` int(1) not null default 0,
	`macro_setup` varchar(200) null,
    `restarts_in_minutes` int(11) null,
	constraint `pk|ec2_instance` primary key (`id`),
	constraint `fk|ec2_instance|ec2_account` foreign key (`ec2_account_id`) references `ec2_account` (`id`),
	constraint `fk|ec2_instance|ec2_instance_type` foreign key (`ec2_instance_type_id`) references `ec2_instance_type` (`id`),
	constraint `uc|ec2_instance` unique (`instance_id`)
) engine=innodb default charset=utf8;
create table `ec2_instance_up_time` (
	`id` int(11) unsigned not null auto_increment,
	`ec2_instance_id` int(11) unsigned not null,
	`day` tinyint(1) unsigned null,
	`date` date null,
	`start` time not null,
	`finish` time not null,
	constraint `pk|ec2_instance_up_time` primary key (`id`),
	constraint `fk|ec2_instance_up_time|ec2_instance` foreign key (`ec2_instance_id`) references `ec2_instance` (`id`)
) engine=innodb default charset=utf8;

-- macros
create table `macro_login`(
	`id` int(11) unsigned not null auto_increment,
	`account_id` int(11) unsigned not null,
    `macro` varchar(60) not null,
    `url` varchar(160) not null,
    `login` varchar(160) not null,
    `password` varchar(160) not null,
    `additional_info_01` varchar(160) null,
    `additional_info_02` varchar(160) null,
    `additional_info_03` varchar(160) null,
	constraint `pk|macro_login` primary key (`id`),
	constraint `fk|macro_login|account` foreign key (`account_id`) references `account` (`id`),
    constraint `uc|macro_login` unique (`account_id`, `macro`)
) engine=innodb default charset=utf8;
create index `ix|macro_login|macro` on `macro_login` (`macro`);

-- database specifics
create table `product` (
    `id` int(11) unsigned not null auto_increment,
    `account_id` int(11) unsigned not null,
    `name` varchar(120) not null,
    `commission_group` varchar(120) not null,
    `commission_type` varchar(120) not null,
    `service_code` varchar(120) not null,
    `service_description` varchar(120) not null,
    `signature_fee` decimal(6,2) not null default 0,
    `discount_value` decimal(6,2) not null default 0,
    `discount_percentage` decimal(5,2) not null default 0,
	`timestamp` timestamp not null default now(),
    constraint `pk|product` primary key (`id`),
    constraint `fk|product|account` foreign key (`account_id`) references `account` (`id`),
    constraint `uc|product` unique (`account_id`, `name`)
) engine=innodb default charset=utf8;

create table `plan` (
    `id` int(11) unsigned not null auto_increment,
    `account_id` int(11) unsigned not null,
    `name` varchar(120) not null,
    `data` int(11) not null default 0,
    `minutes` int(11) not null default 0,
    `ilimited` int(1) not null default 0,
    `campaign` varchar(120) not null,
    `discount_value` decimal(6,2) not null default 0,
    `discount_percentage` decimal(5,2) not null default 0,
	`timestamp` timestamp not null default now(),
    constraint `pk|plan` primary key (`id`),
    constraint `fk|plan|account` foreign key (`account_id`) references `account` (`id`),
    constraint `uc|plan` unique (`account_id`, `name`)
) engine=innodb default charset=utf8;
create table `plan_product` (
    `id` int(11) unsigned not null auto_increment,
    `account_id` int(11) unsigned not null,
    `plan_id` int(11) unsigned not null,
    `product_id` int(11) unsigned not null,
	`timestamp` timestamp not null default now(),
    constraint `pk|plan_product` primary key (`id`),
    constraint `fk|plan_product|account` foreign key (`account_id`) references `account` (`id`),
    constraint `fk|plan_product|plan` foreign key (`plan_id`) references `plan` (`id`),
    constraint `fk|plan_product|product` foreign key (`product_id`) references `product` (`id`),
    constraint `uc|plan_product` unique (`plan_id`, `product_id`)
) engine=innodb default charset=utf8;

create table `keyword` (
    `id` int(11) unsigned not null auto_increment,
    `account_id` int(11) unsigned not null,
    `name` varchar(120) not null,
    constraint `pk|keyword` primary key (`id`),
    constraint `fk|keyword|account` foreign key (`account_id`) references `account` (`id`)
) engine=innodb default charset=utf8;

create table `sales_campaign` (
    `id` int(11) unsigned not null auto_increment,
    `account_id` int(11) unsigned not null,
    `name` varchar(120) not null,
    constraint `pk|sales_campaign` primary key (`id`),
    constraint `fk|sales_campaign|account` foreign key (`account_id`) references `account` (`id`)
) engine=innodb default charset=utf8;
create table `sales_campaign_user` (
    `id` int(11) unsigned not null auto_increment,
    `account_id` int(11) unsigned not null,
    `sales_campaign_id` int(11) unsigned not null,
    `user_id` int(11) unsigned not null,
    constraint `pk|sales_campaign_user` primary key (`id`),
    constraint `fk|sales_campaign_user|account` foreign key (`account_id`) references `account` (`id`),
    constraint `fk|sales_campaign_user|sales_campaign` foreign key (`sales_campaign_id`) references `sales_campaign` (`id`) on delete cascade,
    constraint `fk|sales_campaign_user|user` foreign key (`user_id`) references `user` (`id`),
    constraint `uc|sales_campaign_user` unique (`sales_campaign_id`, `user_id`)
) engine=innodb default charset=utf8;
create table `sales_campaign_keyword` (
    `id` int(11) unsigned not null auto_increment,
    `sales_campaign_id` int(11) unsigned not null,
    `keyword_id` int(11) unsigned not null,
    constraint `pk|sales_campaign_keyword` primary key (`id`),
    constraint `fk|sales_campaign_keyword|sales_campaign` foreign key (`sales_campaign_id`) references `sales_campaign` (`id`) on delete cascade,
    constraint `fk|sales_campaign_keyword|keyword` foreign key (`keyword_id`) references `keyword` (`id`) on delete cascade,
    constraint `uc|sales_campaign_keyword` unique (`sales_campaign_id`, `keyword_id`)
) engine=innodb default charset=utf8;
create table `sales_campaign_zipcode` (
    `id` int(11) unsigned not null auto_increment,
    `account_id` int(11) unsigned not null,
    `sales_campaign_id` int(11) unsigned not null,
    `zipcode` varchar(8) not null,
    `done` int(11) unsigned not null default 0,
    constraint `pk|sales_campaign_zipcode` primary key (`id`),
    constraint `fk|sales_campaign_zipcode|account` foreign key (`account_id`) references `account` (`id`),
    constraint `fk|sales_campaign_zipcode|sales_campaign` foreign key (`sales_campaign_id`) references `sales_campaign` (`id`) on delete cascade,
    constraint `uc|sales_campaign_zipcode` unique (`sales_campaign_id`, `zipcode`)
) engine=innodb default charset=utf8;
create table `sales_campaign_macro` (
    `id` int(11) unsigned not null auto_increment,
    `account_id` int(11) unsigned not null,
    `sales_campaign_id` int(11) unsigned not null,
    `sales_campaign_keyword_id` int(11) unsigned not null,
    `sales_campaign_zipcode_id` int(11) unsigned not null,
    constraint `pk|sales_campaign_macro` primary key (`id`),
    constraint `fk|sales_campaign_macro|account` foreign key (`account_id`) references `account` (`id`),
    constraint `fk|sales_campaign_macro|sales_campaign` foreign key (`sales_campaign_id`) references `sales_campaign` (`id`) on delete cascade,
    constraint `fk|sales_campaign_macro|sales_campaign_keyword` foreign key (`sales_campaign_keyword_id`) references `sales_campaign_keyword` (`id`) on delete cascade,
    constraint `fk|sales_campaign_macro|sales_campaign_zipcode` foreign key (`sales_campaign_zipcode_id`) references `sales_campaign_zipcode` (`id`) on delete cascade,
    constraint `uc|sales_campaign_macro` unique (`sales_campaign_id`, `sales_campaign_keyword_id`, `sales_campaign_zipcode_id`)
) engine=innodb default charset=utf8;
create index `ix|sales_campaign_macro|id-done` on `sales_campaign_macro` (`id`, `done`);

create table `department` (
    `id` int(11) unsigned not null auto_increment,
    `account_id` int(11) unsigned not null,
    `name` varchar(120) not null,
    constraint `pk|department` primary key (`id`),
    constraint `fk|department|account` foreign key (`account_id`) references `account` (`id`),
    constraint `uc|department` unique (`name`)
) engine=innodb default charset=utf8;
create table `position` (
    `id` int(11) unsigned not null auto_increment,
    `account_id` int(11) unsigned not null,
    `name` varchar(120) not null,
    constraint `pk|position` primary key (`id`),
    constraint `fk|position|account` foreign key (`account_id`) references `account` (`id`),
    constraint `uc|position` unique (`name`)
) engine=innodb default charset=utf8;

create table `company` (
	`id` int(11) unsigned not null auto_increment,
	`account_id` int(11) unsigned not null,
	`user_id` int(11) unsigned null,
	`sales_campaign_id` int(11) unsigned null,
	`cnpj` varchar(16) null,
    `site` varchar(160) null,
    `ddd` varchar(2) null,
    `phone` varchar(10) null,
    `operator` varchar(20) null,
	`description` varchar(200) null,
	`formal_name` varchar(90) null,
	`fantasy_name` varchar(90) not null,
	`state_registry` varchar(60) null,
	`timestamp` timestamp not null default now(),
	`availability` tinyint(1) unsigned not null default 0,
	`availability_test` timestamp null,
	constraint `pk|company` primary key (`id`),
	constraint `fk|company|account` foreign key (`account_id`) references `account` (`id`),
	constraint `fk|company|user` foreign key (`user_id`) references `user` (`id`),
	constraint `fk|company|sales_campaign` foreign key (`sales_campaign_id`) references `sales_campaign` (`id`)
	-- constraint `uc|company` unique (`cnpj`)
) engine=innodb default charset=utf8;
create index `ix|company|fantasy_name` on `company` (`fantasy_name`);
create table `company_task` (
	`id` int(11) unsigned not null auto_increment,
	`user_id` int(11) unsigned not null,
	`worker_id` int(11) unsigned not null,
	`company_id` int(11) unsigned not null,
	`title` varchar(40) not null,
	`description` varchar(400) not null,
	`due_date` date not null,
	`timestamp` timestamp not null default now(),
	`completion` date null,
	constraint `pk|company_task` primary key (`id`),
	constraint `fk|company_task|user_id` foreign key (`user_id`) references `user` (`id`),
	constraint `fk|company_task|worker_id` foreign key (`worker_id`) references `user` (`id`),
	constraint `fk|company_task|company` foreign key (`company_id`) references `company` (`id`)
) engine=innodb default charset=utf8;
create table `company_task_status` (
	`id` int(11) unsigned not null auto_increment,
	`company_task_id` int(11) unsigned not null,
	`user_id` int(11) unsigned not null,
	`situation` varchar(40) not null,
	`comment` varchar(120) not null,
	`timestamp` timestamp not null default now(),
	constraint `pk|company_task_status` primary key (`id`),
	constraint `fk|company_task_status|company_task` foreign key (`company_task_id`) references `company_task` (`id`),
	constraint `fk|company_task_status|user` foreign key (`user_id`) references `user` (`id`)
) engine=innodb default charset=utf8;
create table `company_contact` (
	`id` int(11) unsigned not null auto_increment,
	`person_id` int(11) unsigned not null,
	`company_id` int(11) unsigned not null,
	`position_id` int(11) unsigned null,
    `department_id` int(11) unsigned null,
	`is_main` int(1) not null default 0,
	constraint `pk|company_contact` primary key (`id`),
	constraint `fk|company_contact|person` foreign key (`person_id`) references `person` (`id`),
	constraint `fk|company_contact|company` foreign key (`company_id`) references `company` (`id`),
	constraint `fk|company_contact|position` foreign key (`position_id`) references `position` (`id`),
	constraint `fk|company_contact|department` foreign key (`department_id`) references `department` (`id`),
	constraint `uc|company_contact` unique (`company_id`, `person_id`)
) engine=innodb default charset=utf8;
create table `company_address` (
	`id` int(11) unsigned not null auto_increment,
	`company_id` int(11) unsigned not null,
	`city_id` int(11) unsigned null,
    `street` varchar(120) not null,
	`number` varchar(12) null,
	`compliment` varchar(60) null,
    `neighbourhood` varchar(60) null,
	`zip` varchar(60) null,	
    `message` varchar(60) null,
    `closet_distance` varchar(60) null,
    `lines` varchar(60) null,
    `until_20` varchar(60) null,
    `from_21_up_to_50` varchar(60) null,
    `over_50` varchar(60) null,
	constraint `pk|company_address` primary key (`id`),
	constraint `fk|company_address|city` foreign key (`city_id`) references `city` (`id`),
	constraint `fk|company_address|company` foreign key (`company_id`) references `company` (`id`)
) engine=innodb default charset=utf8;

-- sales
create table `queue_wait` (
	`id` int(11) unsigned not null auto_increment,
	`account_id` int(11) unsigned not null,
	`company_id` int(11) unsigned not null,
	`status_id` int(11) unsigned not null,
	`user_id` int(11) unsigned not null,
	`until` date not null,
	`comment` varchar(120) null,
	`timestamp` timestamp not null default now(),
	constraint `pk|queue_wait` primary key (`id`),
	constraint `fk|queue_wait|account` foreign key (`account_id`) references `account` (`id`),
	constraint `fk|queue_wait|company` foreign key (`company_id`) references `company` (`id`),
	constraint `fk|queue_wait|status` foreign key (`status_id`) references `status` (`id`),
	constraint `fk|queue_wait|user` foreign key (`user_id`) references `user` (`id`),
	constraint `uc|queue_wait` unique (`account_id`, `company_id`)
) engine=innodb default charset=utf8;
create table `queue_sale` (
	`id` int(11) unsigned not null auto_increment,
	`account_id` int(11) unsigned not null,
	`user_id` int(11) unsigned null,
	`company_id` int(11) unsigned not null,
	`plan_id` int(11) unsigned null,
	`soho_plan` varchar(60) null,
	`soho_campaign` varchar(60) null,
	`soho_value` decimal(6,2) unsigned null,
	`company_address_id` int(11) unsigned null,
	`next_contact` date null,
	`next_contact_hour` int(11) unsigned null,
	`sold` int(11) unsigned not null default 0,
	`closed` int(11) unsigned not null default 0,
	`approved` int(11) unsigned not null default 0,
	`closed_by` int(11) unsigned null,
	`timestamp` timestamp not null default now(),
	`first_offered` timestamp null,
	constraint `pk|queue_sale` primary key (`id`),
	constraint `fk|queue_sale|account` foreign key (`account_id`) references `account` (`id`),
	constraint `fk|queue_sale|user` foreign key (`user_id`) references `user` (`id`),
	constraint `fk|queue_sale|company` foreign key (`company_id`) references `company` (`id`),
	constraint `fk|queue_sale|plan` foreign key (`plan_id`) references `plan` (`id`),
	constraint `fk|queue_sale|company_address` foreign key (`company_address_id`) references `company_address` (`id`),
	constraint `fk|queue_sale|user_closer` foreign key (`closed_by`) references `user` (`id`),
	constraint `uc|queue_sale` unique (`account_id`, `company_id`)
) engine=innodb default charset=utf8;
create index `ix|queue_sale|account-user` on `queue_sale` (`account_id`, `user_id`);
create index `ix|queue_sale|user-company` on `queue_sale` (`user_id`, `company_id`);
create index `ix|queue_sale|user-sale-idle` on `queue_sale` (`user_id`, `closed`, `approved`);
create index `ix|queue_sale|company-account` on `queue_sale` (`company_id`, `account_id`);
create index `ix|queue_sale|account-closed-sold` on `queue_sale` (`account_id`, `closed`, `sold`);
create table `queue_sale_review` (
	`id` int(11) unsigned not null auto_increment,
	`queue_sale_id` int(11) unsigned not null,
	`status_id` int(11) unsigned not null,
	`user_id` int(11) unsigned not null,
	`comment` varchar(120) null,
	`timestamp` timestamp not null default now(),
	constraint `pk|queue_sale_review` primary key (`id`),
	constraint `fk|queue_sale_review|queue_sale` foreign key (`queue_sale_id`) references `queue_sale` (`id`),
	constraint `fk|queue_sale_review|status` foreign key (`status_id`) references `status` (`id`),
	constraint `fk|queue_sale_review|user` foreign key (`user_id`) references `user` (`id`)
) engine=innodb default charset=utf8;
create table `queue_sale_lines` (
	`id` int(11) unsigned not null auto_increment,
	`queue_sale_id` int(11) unsigned not null,
	`ddd` varchar(2) not null,
	`phone` varchar(10) not null,
	constraint `pk|queue_sale_lines` primary key (`id`),
	constraint `fk|queue_sale_lines|queue_sale` foreign key (`queue_sale_id`) references `queue_sale` (`id`),
	constraint `uc|queue_sale` unique (`queue_sale_id`, `ddd`, `phone`)
) engine=innodb default charset=utf8;

-- testing priority
create table `testing_priority_address` (
	`id` int(11) unsigned not null auto_increment,
	`user_id` int(11) unsigned not null,
	`company_id` int(11) unsigned not null,
	constraint `pk|testing_priority_address` primary key (`id`),
	constraint `fk|testing_priority_address|user` foreign key (`user_id`) references `user` (`id`),
	constraint `fk|testing_priority_address|company` foreign key (`company_id`) references `company` (`id`),
	constraint `uc|testing_priority_address` unique (`company_id`)
) engine=innodb default charset=utf8;
create table `testing_priority_phone` (
	`id` int(11) unsigned not null auto_increment,
	`user_id` int(11) unsigned not null,
	`person_phone_id` int(11) unsigned not null,
	`test_after` timestamp null,
	constraint `pk|testing_priority_phone` primary key (`id`),
	constraint `fk|testing_priority_phone|user` foreign key (`user_id`) references `user` (`id`),
	constraint `fk|testing_priority_phone|person_phone` foreign key (`person_phone_id`) references `person_phone` (`id`),
	constraint `uc|testing_priority_phone` unique (`person_phone_id`)
) engine=innodb default charset=utf8;

-- buckets
drop table if exists `bucket_profile_approved_company`;
drop table if exists `bucket_profile_rejected_company`;
drop table if exists `bucket_user`;
drop table if exists `bucket_company`;
drop table if exists `bucket`;
drop table if exists `bucket_profile_user`;
drop table if exists `bucket_profile`;
create table `bucket_profile` (
	`id` int(11) unsigned not null auto_increment,
	`account_id` int(11) unsigned not null,
	`user_id` int(11) unsigned not null,
	`name` varchar(120) not null,
    `city_id` int(11) unsigned null,
    `state_id` int(11) unsigned null,
	`description` varchar(200) null,
	`neighbourhood` varchar(60) null,
    `sales_campaign_id` int(11) unsigned null,
	`availability` tinyint(1) unsigned not null default 0,
    `timestamp` timestamp not null default now(),
    
	constraint `pk|bucket_profile` primary key (`id`),
	constraint `fk|bucket_profile|city` foreign key (`city_id`) references `city` (`id`),
	constraint `fk|bucket_profile|user` foreign key (`user_id`) references `user` (`id`),
	constraint `fk|bucket_profile|state` foreign key (`state_id`) references `state` (`id`),
	constraint `fk|bucket_profile|account` foreign key (`account_id`) references `account` (`id`),
	constraint `fk|bucket_profile|sales_campaign` foreign key (`sales_campaign_id`) references `sales_campaign` (`id`),
	constraint `uc|bucket_profile` unique (`account_id`, `name`)
) engine=innodb default charset=utf8;
create table `bucket_profile_user` (
	`id` int(11) unsigned not null auto_increment,
	`created_by` int(11) unsigned not null,
    `bucket_profile_id` int(11) unsigned not null,
	`user_id` int(11) unsigned not null,
    
	constraint `pk|bucket_profile_user` primary key (`id`),
	constraint `fk|bucket_profile_user|user` foreign key (`user_id`) references `user` (`id`),
	constraint `fk|bucket_profile_user|creator` foreign key (`created_by`) references `user` (`id`),
	constraint `fk|bucket_profile_user|bucket_profile` foreign key (`bucket_profile_id`) references `bucket_profile` (`id`),
	constraint `uc|bucket_profile_user` unique (`bucket_profile_id`, `user_id`)
) engine=innodb default charset=utf8;
create table `bucket` (
	`id` int(11) unsigned not null auto_increment,
    `bucket_profile_id` int(11) unsigned not null,
    `name` varchar(120) not null,
    `timestamp` timestamp not null default now(),
    
	constraint `pk|bucket` primary key (`id`),
	constraint `fk|bucket|bucket_profile` foreign key (`bucket_profile_id`) references `bucket_profile` (`id`),
	constraint `uc|bucket` unique (`name`, `bucket_profile_id`)
) engine=innodb default charset=utf8;
create table `bucket_user` (
	`id` int(11) unsigned not null auto_increment,
    `bucket_id` int(11) unsigned not null,
    `user_id` int(11) unsigned not null,
    
	constraint `pk|bucket_user` primary key (`id`),
	constraint `fk|bucket_user|bucket` foreign key (`bucket_id`) references `bucket` (`id`),
	constraint `fk|bucket_user|user` foreign key (`user_id`) references `user` (`id`),
	constraint `uc|bucket_user` unique (`bucket_id`, `user_id`)
) engine=innodb default charset=utf8;
create table `bucket_company` (
	`id` int(11) unsigned not null auto_increment,
    `bucket_id` int(11) unsigned not null,
    `company_id` int(11) unsigned not null,
    
	constraint `pk|bucket_company` primary key (`id`),
	constraint `fk|bucket_company|bucket` foreign key (`bucket_id`) references `bucket` (`id`),
	constraint `fk|bucket_company|company` foreign key (`company_id`) references `company` (`id`),
	constraint `uc|bucket_company` unique (`bucket_id`, `company_id`)
) engine=innodb default charset=utf8;
create table `bucket_profile_approved_company` (
	`id` int(11) unsigned not null auto_increment,
    `bucket_profile_id` int(11) unsigned not null,
	`company_id` int(11) unsigned not null,
	`user_id` int(11) unsigned not null,
    `timestamp` timestamp not null default now(),
    
	constraint `pk|bucket_profile_approved_company` primary key (`id`),
	constraint `fk|bucket_profile_approved_company|user` foreign key (`user_id`) references `user` (`id`),
	constraint `fk|bucket_profile_approved_company|company` foreign key (`company_id`) references `company` (`id`),
	constraint `fk|bucket_profile_approved_company|bucket_profile` foreign key (`bucket_profile_id`) references `bucket_profile` (`id`)
) engine=innodb default charset=utf8;
create table `bucket_profile_rejected_company` (
	`id` int(11) unsigned not null auto_increment,
    `bucket_profile_id` int(11) unsigned not null,
	`company_id` int(11) unsigned not null,
	`user_id` int(11) unsigned not null,
    `timestamp` timestamp not null default now(),
    
	constraint `pk|bucket_profile_rejected_company` primary key (`id`),
	constraint `fk|bucket_profile_rejected_company|user` foreign key (`user_id`) references `user` (`id`),
	constraint `fk|bucket_profile_rejected_company|company` foreign key (`company_id`) references `company` (`id`),
	constraint `fk|bucket_profile_rejected_company|bucket_profile` foreign key (`bucket_profile_id`) references `bucket_profile` (`id`)
) engine=innodb default charset=utf8;

-- database initiation
insert into `permission` values (null, 'Administrador', 100), (null, 'Gestor', 200), (null, 'Colaborador', 300);
insert into `user` values (null, 'Pedro Clericuzzi', 'pedro@clericuzzi.com', '123456', 1, 0, now());
insert into `user` values (null, 'Diogo Mendes', 'diogo@brdn.com.br', '123456', 1, 0, now());

insert into `account` values (null, '11111111111111', 'BrDn', null, null, null, 0, 0, 0, 65, now());
insert into `account` values (null, '11111111111112', 'Tests', null, null, null, 0, 0, 0, 65, now());
insert into `account_user` values (null, 1, 1, 1, 0, now());
insert into `account_user` values (null, 1, 2, 1, 0, now());

insert into `user_permission` values (null, 1, 1, 1);

delete from `queue_sale_review`;
delete from `status`;
delete from `status_type`;
insert into `status_type` values (1, 'aprovada'), (2, 'rejeitada');
insert into `status` values (null, 1, 'proposta enviada'), (null, 1, 'analisando o plano'), (null, 1, 'cliente analisando proposta'), (null, 1, 'venda realizada'), (null, 1, 'fidelidade'), (null, 1, 'retornar ligação');
insert into `status` values (null, 2, 'não atende'), (null, 2, 'paga menos no concorrente'), (null, 2, 'desligado'), (null, 2, 'não existe'), (null, 2, 'sem interesse'), (null, 2, 'já atendido por outro parceiro'), (null, 2, 'insatisfeito com a vivo'), (null, 2, 'residência'), (null, 2, 'pessoa física'), (null, 2, 'reclamação'), (null, 2, 'pós-venda'), (null, 2, 'encerrando as atividades');