select
	(select `name` from sales_campaign c where c.id = i.sales_campaign_id) as `name`
    , count(0) `total`
    , (select count(0) from company cp where cp.sales_campaign_id = i.sales_campaign_id and cp.availability = 1) as `disponíveis`
    , ((select count(0) from company cp where cp.sales_campaign_id = i.sales_campaign_id and cp.availability = 1) / count(0)) * 100 as `% de aproveitamento`
    , (count(0) / (select count(0) from company cp)) * 100 as `% de representatividade`
from
	company i
group by
	i.sales_campaign_id
order by
	4 desc;