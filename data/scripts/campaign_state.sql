select
	s.name as `ação`
    , count(0) as `total empresas`
    , (select count(0) from company y where y.sales_campaign_id = c.sales_campaign_id and y.availability = 1) as `com disponibilidade`
    , (select count(0) from company y where y.sales_campaign_id = c.sales_campaign_id and y.availability = 0) as `sem disponibilidade`
from
	company c join sales_campaign s on c.sales_campaign_id = s.id group by s.name with rollup;

select
	s.`name`
    , count(0)
from
	sales_campaign_macro m
join
	sales_campaign s on s.id = m.sales_campaign_id
where
	sales_campaign_id in (15)
group by
	s.`name`;

select 
	sales_campaign_id as `id campanha`
	, (select `name` from sales_campaign c where c.id = s.sales_campaign_id) as `campanha`
    , count(0) `combinações em processamento`
    , (select count(0) from sales_campaign_zipcode z where z.sales_campaign_id = s.sales_campaign_id) as `total de ceps`
    , (select count(0) from sales_campaign_zipcode z where z.sales_campaign_id = s.sales_campaign_id and z.done = 1) as `ceps processados ou processando`
from
	sales_campaign_macro s 
group by
	sales_campaign_id
order by
	2;

update sales_campaign_zipcode set done = 0 where sales_campaign_id = 15;
delete from sales_campaign_macro where sales_campaign_id = 15;

select * from sales_campaign;
select * from sales_campaign where id = 15;
select * from sales_campaign_macro where sales_campaign_id = 15;
select * from sales_campaign_keyword where sales_campaign_id = 15;
select * from sales_campaign_zipcode where sales_campaign_id = 15;
    
select
	(select `name` from sales_campaign c where c.id = s.sales_campaign_id) as `campanha`
    , count(0) `ceps`
from
	sales_campaign_zipcode s
group by
	s.sales_campaign_id
order by
	1;