update company set user_id = null, cnpj = null, state_registry = null, formal_name = null where user_id = 1;
update queue_sale set user_id = null, sold = 0, closed = 0, approved = 0, next_contact = null, next_contact_hour = null where user_id = 1;
delete from queue_wait where user_id = 1;
delete from queue_sale_review where user_id = 1;