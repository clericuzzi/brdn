drop table if exists `_tmp_company_doubles`;
create table `_tmp_company_doubles` select concat(ifnull(`ddd`, ''), ifnull(`phone`, '')) `full_phone` from `company` group by concat(ifnull(`ddd`, ''), ifnull(`phone`, '')) having count(0) > 1;

drop table if exists `_tmp_company_doubles_ids`;
create table `_tmp_company_doubles_ids` select `id` from `company` where concat(ifnull(`ddd`, ''), ifnull(`phone`, '')) in (select `full_phone` from `_tmp_company_doubles`);
create index `ix|_tmp_company_doubles_ids` on `_tmp_company_doubles_ids` (`id`);

delete from `company_availability` where `company_address_id` in (select `id` from `company_address` where `company_id` in (select id from `_tmp_company_doubles_ids`));
delete from `company_address` where `company_id` in (select `id` from `_tmp_company_doubles_ids`);
delete from `company` where `id` in (select `id` from `_tmp_company_doubles_ids`);

drop table if exists `_tmp_company_doubles_ids`;
drop table if exists `_tmp_company_doubles`;