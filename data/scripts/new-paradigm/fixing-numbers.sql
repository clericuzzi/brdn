drop function if exists `fnc_fix_default_number`;
delimiter |
create function `fnc_fix_default_number`(companyId int(11))
returns tinyint(1)
deterministic
begin

	set @ddd = (select ddd from company_line where company_id = companyId order by 1 limit 1);
	set @phone = (select phone from company_line where company_id = companyId order by 1 limit 1);
	
    update company set ddd = @ddd, phone = @phone where id = companyId;

	return 1;
end|
delimiter ;

drop table if exists _tmp;
create table _tmp select id from company where ddd is null and id in (select company_id from company_line);

select `fnc_fix_default_number` (id) from _tmp;