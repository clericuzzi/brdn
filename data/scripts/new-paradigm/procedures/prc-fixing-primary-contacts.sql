delimiter ;
drop procedure if exists proc_fix_primary_contact;
delimiter |
create procedure proc_fix_primary_contact ()
begin

	DECLARE done INT DEFAULT FALSE;
	DECLARE companyId int(11);
	DECLARE cur CURSOR FOR SELECT `id` from `company` where `state_registry` is not null;
	-- DECLARE cur CURSOR FOR SELECT `id` from `company` where `id` = 454307;    
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

	OPEN cur;

	read_loop: LOOP
	FETCH cur INTO companyId;
    set @lineId = (select id from company_line where company_id = companyId order by rand() limit 1);
    set @ddd = (select ddd from company_line where id = @lineId);
    set @phone = (select phone from company_line where id = @lineId);
    -- select @ddd = ddd, @phone = phone from company_line where id = @lineId;
    
    update company set ddd = @ddd, phone = @phone where id = companyId;
    
    IF done THEN
      LEAVE read_loop;
    END IF;
    
	END LOOP;

	CLOSE cur;
end|
delimiter ;

select count(0) from company where state_registry is not null;
select count(0) from company where state_registry is not null and ddd is null;
-- call proc_fix_primary_contact;
-- select * from company where id = 454307;
-- select * from company_line where company_id = 454307;