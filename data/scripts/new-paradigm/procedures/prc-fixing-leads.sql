delimiter ;
drop procedure if exists `prc_clean_leads`;
delimiter |
create procedure `prc_clean_leads` (activityId int(11))
begin

	declare done int default false;
	declare userId int(11);
	declare cur cursor for select distinct `user_id` from `lead` where `activity_id` = activityId;
	declare continue handler for not found set done = true;

	open cur;
	read_loop: loop
	fetch cur into userId;
    
		drop table if exists `_tbl_prc_clean_leads`;
		create table `_tbl_prc_clean_leads` (`company_id` int(11) unsigned not null);
		create index `ix|_tbl_prc_clean_leads|company` on `_tbl_prc_clean_leads` (`company_id`);
		insert into `_tbl_prc_clean_leads` select distinct `company_id` from `lead` where `user_id` = userId and `activity_id` = activityId;

		delete from `_tbl_prc_clean_leads` where `company_id` in (select `company_id` from (select `company_id`, datediff(max(`timestamp`), now()) as `diff` from `company_note` where `user_id` = userId group by `company_id`)  as `t` where `diff` >= -3);
		delete from `_tbl_prc_clean_leads` where `company_id` in (select `company_id` from (select `company_id`, datediff(max(`started_in`), now()) as `diff` from `company_call` where `user_id` = userId group by `company_id`)  as `t` where `diff` >= -3);
		delete from `_tbl_prc_clean_leads` where `company_id` in (select `company_id` from (select `company_id`, datediff(max(`timestamp`), now()) as `diff` from `company_contact` where `user_id` = userId group by `company_id`)  as `t` where `diff` >= -3);
		delete from `_tbl_prc_clean_leads` where `company_id` in (select `company_id` from (select `company_id`, datediff(max(`call_time`), now()) as `diff` from `company_callback` where `user_id` = userId group by `company_id`) as `t` where `diff` >= -3);

		delete from `lead` where `activity_id` = activityId and `company_id` in (select `company_id` from `_tbl_prc_clean_leads`);
    
    if done then
      leave read_loop;
    end if;
	end loop;
	close cur;
end|
delimiter ;

-- call `prc_clean_leads` (1);