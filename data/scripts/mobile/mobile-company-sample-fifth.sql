select
	distinct
	`mc`.`segment` as `segmento`
    , `mc`.`cnpj` as `cnpj`
    , `mc`.`fantasy_name` as `nome fantasia`
    , `ct`.`contact` as `contato`
from
	`mobile_company` `mc`
join `mobile_company_contact` `ct` on `ct`.`mobile_company_id` = `mc`.`id`
where
	`mc`.`id` in (select `id` from `mobile_company` where `cnpj` in (select `cnpj` from `mobile_company_import` where `filename` = 'upselling-julho-2019-07-02'))
and length(`mc`.`cnpj`) = 14
and `mc`.`cnpj` not in (select `cnpj` from `mobile_company_unavailable_company`)
order by
	`mc`.`fantasy_name`, `ct`.`contact`;

-- select * from `mobile_company_address` limit 15;
-- select distinct `filename` from `mobile_company_import`;