select
	mc.`segment` as `segmento`
    , mc.`cnpj` as `cnpj`
    , mc.`fantasy_name` as `nome fantasia`
    , ct.`contact` as `contato`
from
	`mobile_company` mc
join `mobile_company_contact` ct on ct.`mobile_company_id` = mc.id
where
	mc.`id` in (select * from (select `id` from `mobile_company` order by rand() limit 1000) as t)
and length(mc.`cnpj`) > 11
order by
	mc.`fantasy_name`, ct.`contact`;