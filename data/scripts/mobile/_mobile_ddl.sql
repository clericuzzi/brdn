drop table if exists `mobile_company_evaluation`;
drop table if exists `mobile_company_address`;
drop table if exists `mobile_company_comment`;
drop table if exists `mobile_company_contact`;
drop table if exists `mobile_company_source`;
drop table if exists `mobile_company_import`;
drop table if exists `mobile_company_phone`;
drop table if exists `mobile_company_lock`;
drop table if exists `mobile_company`;

create table `mobile_company_import`
(
  `id` int(11) unsigned not null auto_increment,
  `cnpj` varchar(20) not null,
  `valid` int(1) not null default 0,
  `filename` varchar(60) not null,
  `timestamp` timestamp not null default now(),
  `priority` int(11) unsigned null,
  constraint `pk|mobile_company_import` primary key (`id`)
) engine=innodb default charset=utf8;
create index `ix|mobile_company_import|cnpj` on `mobile_company_source` (`cnpj`);
create index `ix|mobile_company_import|filename` on `mobile_company_source` (`filename`);

create table `mobile_company_source`
(
  `id` int(11) unsigned not null auto_increment,
  `mobile_company_import_id` int(11) unsigned not null,
  `priority` int(11) unsigned null,
  constraint `pk|mobile_company_source` primary key (`id`),
  constraint `fk|mobile_company_source|mobile_company_import` foreign key (`mobile_company_import_id`) references `mobile_company_import` (`id`)
) engine=innodb default charset=utf8;
create index `ix|mobile_company_source|cnpj` on `mobile_company_source` (`cnpj`);
create index `ix|mobile_company_source|done` on `mobile_company_source` (`done`);

create table `mobile_company`
(
  `id` int(11) unsigned not null auto_increment,
  `account_id` int(11) unsigned not null,
  `cnpj` varchar(20) not null,
  `segment` varchar(60) null,
  `description` varchar(200) null,
  `fantasy_name` varchar(120) null,
  `callback` timestamp null,
  `callback_user` int(11) unsigned null,
  `timestamp` timestamp not null default now(),
  constraint `pk|mobile_company` primary key (`id`),
  constraint `fk|mobile_company|account` foreign key (`account_id`) references `account` (`id`),
  constraint `fk|mobile_company|callback_user` foreign key (`callback_user`) references `user` (`id`)
) engine=innodb default charset=utf8;
create index `ix|mobile_company|cnpj` on `mobile_company` (`cnpj`);

create table `mobile_company_address`
(
  `id` int(11) unsigned not null auto_increment,
  `mobile_company_id` int(11) unsigned not null,
  `address` varchar(200) not null,
  constraint `pk|mobile_company_address` primary key (`id`),
  constraint `fk|mobile_company_address|mobile_company` foreign key (`mobile_company_id`) references `mobile_company` (`id`)
) engine=innodb default charset=utf8;
create index `ix|mobile_company_address|address` on `mobile_company_address` (`address`);

create table `mobile_company_contact`
(
  `id` int(11) unsigned not null auto_increment,
  `mobile_company_id` int(11) unsigned not null,
  `contact` varchar(120) null,
  constraint `pk|mobile_company_contact` primary key (`id`),
  constraint `fk|mobile_company_contact|mobile_company` foreign key (`mobile_company_id`) references `mobile_company` (`id`)
) engine=innodb default charset=utf8;
create index `ix|mobile_company_contact|contact` on `mobile_company_contact` (`contact`);

create table `mobile_company_comment`
(
  `id` int(11) unsigned not null auto_increment,
  `mobile_company_id` int(11) unsigned not null,
  `user_id` int(11) unsigned not null,
  `comment` varchar(120) not null,
  `timestamp` timestamp not null default now(),
  constraint `pk|mobile_company_comment` primary key (`id`),
  constraint `fk|mobile_company_comment|user` foreign key (`user_id`) references `user` (`id`),
  constraint `fk|mobile_company_comment|mobile_company` foreign key (`mobile_company_id`) references `mobile_company` (`id`)
) engine=innodb default charset=utf8;

create table `mobile_company_evaluation`
(
  `id` int(11) unsigned not null auto_increment,
  `mobile_company_id` int(11) unsigned not null,
  `user_id` int(11) unsigned not null,
  `evaluation` int(11) unsigned default null,
  `timestamp` timestamp not null default now(),
  constraint `pk|mobile_company_evaluation` primary key (`id`),
  constraint `fk|mobile_company_evaluation|user` foreign key (`user_id`) references `user` (`id`),
  constraint `fk|mobile_company_evaluation|mobile_company` foreign key (`mobile_company_id`) references `mobile_company` (`id`)
) engine=innodb auto_increment=43439 default charset=utf8;

create table `mobile_company_lock`
(
  `id` int(11) unsigned not null auto_increment,
  `user_id` int(11) unsigned not null,
  `mobile_company_id` int(11) unsigned not null,
  `timestamp` timestamp not null default now(),
  constraint `pk|mobile_company_lock` primary key (`id`),
  unique key `uc|mobile_company_lock` (`user_id`,`mobile_company_id`),
  constraint `fk|mobile_company_lock|user` foreign key (`user_id`) references `user` (`id`),
  constraint `fk|mobile_company_lock|mobile_company` foreign key (`mobile_company_id`) references `mobile_company` (`id`)
) engine=innodb default charset=utf8;