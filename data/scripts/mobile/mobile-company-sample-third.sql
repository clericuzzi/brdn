select
	distinct
	mc.`segment` as `segmento`
    , mc.`cnpj` as `cnpj`
    , mc.`fantasy_name` as `nome fantasia`
    , ct.`contact` as `contato`
from
	`mobile_company` mc
join `mobile_company_contact` ct on ct.`mobile_company_id` = mc.id
where
	mc.`id` in (select `id` from (select mc_1.`id` from `mobile_company` mc_1 where mc_1.`cnpj` in (select `cnpj` from `mobile_company_import` where `priority` = 15) order by rand() limit 1000) as t)
and length(mc.cnpj) = 14
order by
	mc.`fantasy_name`, ct.`contact`;

select * from mobile_company_address limit 15;