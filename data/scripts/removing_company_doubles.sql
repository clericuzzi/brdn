drop table if exists _company_double;
create table _company_double select description, fantasy_name, site, ddd, phone, min(id) as `id`from company group by description, fantasy_name, site, ddd, phone having count(0) > 1;
-- select description, fantasy_name, site, ddd, phone, count(0) as `id`from company group by description, fantasy_name, site, ddd, phone having count(0) > 1;
-- select * from _company_double;
-- select * from company where id in (select id from _company_double_id);
-- select * from company where description = 'Advogado' and fantasy_name = 'Ferri e Tochetto Advogados: Dysrael Gergeli Ferri - Carlos Tochetto' and site = 'Adicionar website' and ddd = '49' and phone = '34449529';

drop table if exists _company_double_id;
create table _company_double_id
select
	c.id
from
	_company_double d
join
	company c on ifnull(d.description, '0') = ifnull(c.description, '0')
    and ifnull(d.fantasy_name, '0') = ifnull(c.fantasy_name, '0')
    and ifnull(d.site, '0') = ifnull(c.site, '0')
    and ifnull(d.ddd, '0') = ifnull(c.ddd, '0')
    and ifnull(d.phone, '0') = ifnull(c.phone, '0')
    and c.id <> d.id;

delete from queue_sale_review where queue_sale_id in (select id from queue_sale where company_id in (select id from _company_double_id));
delete from queue_sale_lines where queue_sale_id in (select id from queue_sale where company_id in (select id from _company_double_id));
delete from queue_sale where company_id in (select id from _company_double_id);
delete from queue_wait where company_id in (select id from _company_double_id);
delete from company_address where company_id in (select d.id from _company_double_id d);
delete from company_contact where company_id in (select id from _company_double_id);
delete from company_task where company_id in (select id from _company_double_id);
delete from company where id in (select id from _company_double_id);

drop table if exists _company_double;
drop table if exists _company_double_id;