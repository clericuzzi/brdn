drop table if exists `company_contact`;
CREATE TABLE `company_contact` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) unsigned NOT NULL,
  `position_id` int(11) unsigned DEFAULT NULL,
  `department_id` int(11) unsigned DEFAULT NULL,
  `name` varchar(120) NOT NULL,
  `email` varchar(120) NULL,
  `phone` varchar(12) NULL,
  `registered_by` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc|company_contact` (`company_id`,`name`),
  CONSTRAINT `fk|company_contact|user` FOREIGN KEY (`registered_by`) REFERENCES `user` (`id`),
  CONSTRAINT `fk|company_contact|company` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`),
  CONSTRAINT `fk|company_contact|position` FOREIGN KEY (`position_id`) REFERENCES `position` (`id`),
  CONSTRAINT `fk|company_contact|department` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

drop table if exists `company_evaluation`;
CREATE TABLE `company_evaluation` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `evaluation` int(11) unsigned NULL default null,
  `timestamp` timestamp NOT NULL,
  constraint `pk|company_evaluation` PRIMARY KEY (`id`),
  constraint `fk|company_evaluation|company_id` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`),
  constraint `fk|company_evaluation|user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

drop table if exists `company_keyword`;
CREATE TABLE `company_keyword` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) unsigned NOT NULL,
  `keyword_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc|company_keyword` (`company_id`,`keyword_id`),
  CONSTRAINT `fk|company_keyword|company_id` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`),
  CONSTRAINT `fk|company_keyword|keyword_id` FOREIGN KEY (`keyword_id`) REFERENCES `keyword` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

drop table if exists `mailing_extraction_zipcode`;
drop table if exists `mailing_extraction`;
CREATE TABLE `mailing_extraction` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `states` varchar(1200) DEFAULT NULL,
  `cities` varchar(1200) DEFAULT NULL,
  `neighbourhoods` varchar(1200) DEFAULT NULL,
  `done` int(1) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  CONSTRAINT `uc|mailing_extraction` unique (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
CREATE TABLE `mailing_extraction_zipcode` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `extraction_id` int(11) unsigned NOT NULL,
  `zipcode_id` int(11) unsigned DEFAULT NULL,
  `cleared_items` varchar(120) NOT NULL,
  `done` int(1) not NULL default 0,
  constraint `pk|mailing_extraction_zipcode` PRIMARY KEY (`id`),
  UNIQUE KEY `uc|mailing_extraction_zipcode` (`extraction_id`, `zipcode_id`),
  CONSTRAINT `fk|mailing_extraction_zipcode|extraction` FOREIGN KEY (`extraction_id`) REFERENCES `mailing_extraction` (`id`),
  CONSTRAINT `fk|mailing_extraction_zipcode|zipcode` FOREIGN KEY (`zipcode_id`) REFERENCES `zipcode` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

drop table if exists `user_area`;
CREATE TABLE `user_area` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `state_id` varchar(1000) NULL,
  `city_id` varchar(1000) NULL,
  `neighbourhood_id` varchar(1000) NULL,
  constraint `pk|user_area` PRIMARY KEY (`id`),
  constraint `uc|user_area` unique (`user_id`),
  CONSTRAINT `fk|user_area|user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

drop table if exists `sale`;
CREATE TABLE `sale` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `plan_id` int(11) unsigned not null,
  `company_id` int(11) unsigned not null,
  `company_address_id` int(11) unsigned not null,
  `soho_plan` varchar(60) DEFAULT NULL,
  `soho_campaign` varchar(60) DEFAULT NULL,
  `soho_value` decimal(6,2) unsigned DEFAULT NULL,
  `sold_by` int(11) unsigned DEFAULT NULL,
  `sold_in` timestamp null,
  `registered_in` timestamp null,
  constraint `pk|sale` PRIMARY KEY (`id`),
  constraint `fk|sale|plan` foreign key (`plan_id`) references `plan` (`id`),
  constraint `fk|sale|company` foreign key (`plan_id`) references `plan` (`id`),
  constraint `fk|sale|company_address` foreign key (`company_address_id`) references `company_address` (`id`),
  constraint `fk|sale|sold_by` foreign key (`sold_by`) references `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

drop table if exists `sale_portability`;
CREATE TABLE `sale_portability` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sale_id` int(11) unsigned not null,
  `ddd` varchar(2) NOT NULL,
  `phone` varchar(10) NOT NULL,
  constraint `pk|sale_portability` PRIMARY KEY (`id`),
  constraint `uc|sale_portability` unique (`sale_id`, `ddd`, `phone`),
  constraint `fk|sale_portability|sale` foreign key (`sale_id`) references `sale` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
