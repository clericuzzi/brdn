drop table if exists `web_vendas_testing_cycle`;
create table `web_vendas_testing_cycle`
(
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `company_address_id` int(11) unsigned not null,
  `priority` int(11) unsigned null,
  constraint `pk|web_vendas_testing_cycle` primary key (`id`),
  constraint `fk|web_vendas_testing_cycle|company_address` foreign key (`company_address_id`) references `company_address` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
insert into `web_vendas_testing_cycle`
	select
		null,
        `id` as `company_address_id`,
        case
			when `testing_timestamp` is null then 1000
			when date(`testing_timestamp`) <> date(now()) then datediff(now(), `testing_timestamp`)
            else null
        end as `priority`
	from
		`company_address`
	where
		`testing_timestamp` is null or date(`testing_timestamp`) <> date(now());

-- select * from `web_vendas_testing_cycle` where `company_address_id` = 34039;
-- select count(0) from `company_address` where `testing_timestamp` is null;
-- select *, datediff(now(), `testing_timestamp`) from `company_address` where date(`testing_timestamp`) <> date(now()) order by rand() limit 14;
-- select *, DATEDIFF(now(), `testing_timestamp`) from company_address limit 15;
-- select tecnology, count(0) from company_address group by tecnology;
-- update `web_vendas_testing_cycle` w join `company_address` c on c.`id` = w.`company_address_id` set c.`priority` = DATEDIFF(now(), c.`testing_timestamp`) where c.`testing_timestamp` <> date(now());
-- update `web_vendas_testing_cycle` w join `company_address` c on c.`id` = w.`company_address_id` set w.`priority` = 100 where c.`testing_timestamp` is null;
select `priority`, count(0) from `web_vendas_testing_cycle` group by `priority` order by 1 desc;
delete from `web_vendas_testing_cycle` where `priority` = 1000;