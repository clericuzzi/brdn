drop view if exists `available_company`;
create view `available_company`
as
	select
		c.`id`
	from
		`company` c
	where
		c.`id` not in (select `company_id` from `company_lock`)
	and c.`id` not in (select `company_id` from `company_address` where `zipcode_id` in (select `id` from `zipcode` where `city_id` = 296))
	and c.`id` not in (select `company_id` from `company_address` where `message` like '%preved%');

select count(0) from `available_company`;
-- explain select * from available_company;