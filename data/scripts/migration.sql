drop table if exists keyword_bkp;
drop table if exists bucket_user;
drop table if exists bucket_profile_user;
drop table if exists sales_campaign_macro;
drop table if exists sales_campaign_zipcode;
drop table if exists sales_campaign_keyword;

delete from company_address;
delete from company_contact;
delete from company_task;
delete from company;

drop table if exists `bucket_company`;
drop table if exists `bucket`;
drop table if exists `bucket_profile_run`;
drop table if exists `bucket_profile_city`;
drop table if exists `bucket_profile_state`;
drop table if exists `bucket_profile_keyword`;
drop table if exists `bucket_profile_neighbourhood`;
drop table if exists `bucket_profile`;

create table `bucket_profile`
(
	`id` int(11) unsigned not null auto_increment,
	`account_id` int(11) unsigned not null,
	`user_id` int(11) unsigned not null,
	`name` varchar(40) not null,
	`availability` tinyint(1) unsigned default null,
	`timestamp` timestamp not null default current_timestamp,
	constraint `pk|bucket_profile` primary key (`id`),
	constraint `fk|bucket_profile|account` foreign key (`account_id`) references `account` (`id`),
	constraint `fk|bucket_profile|user` foreign key (`user_id`) references `user` (`id`)
) engine=innodb auto_increment=9 default charset=utf8;
create table `bucket_profile_city`
(
	`id` int(11) unsigned not null auto_increment,
	`bucket_profile_id` int(11) unsigned null,
	`city_id` int(11) unsigned null,
	constraint `pk|bucket_profile_city` primary key (`id`),
	constraint `fk|bucket_profile_city|bucket_profile` foreign key (`bucket_profile_id`) references `bucket_profile` (`id`),
	constraint `fk|bucket_profile_city|city` foreign key (`city_id`) references `city` (`id`)
) engine=innodb auto_increment=9 default charset=utf8;
create table `bucket_profile_state`
(
	`id` int(11) unsigned not null auto_increment,
	`bucket_profile_id` int(11) unsigned null,
	`state_id` int(11) unsigned null,
	constraint `pk|bucket_profile_state` primary key (`id`),
	constraint `fk|bucket_profile_state|bucket_profile` foreign key (`bucket_profile_id`) references `bucket_profile` (`id`),
	constraint `fk|bucket_profile_state|state` foreign key (`state_id`) references `state` (`id`)
) engine=innodb auto_increment=9 default charset=utf8;
create table `bucket_profile_neighbourhood`
(
	`id` int(11) unsigned not null auto_increment,
	`bucket_profile_id` int(11) unsigned null,
	`neighbourhood_id` int(11) unsigned null,
	constraint `pk|bucket_profile_neighbourhood` primary key (`id`),
	constraint `fk|bucket_profile_neighbourhood|bucket_profile` foreign key (`bucket_profile_id`) references `bucket_profile` (`id`),
	constraint `fk|bucket_profile_neighbourhood|neighbourhood` foreign key (`neighbourhood_id`) references `neighbourhood` (`id`)
) engine=innodb auto_increment=9 default charset=utf8;
create table `bucket_profile_keyword`
(
	`id` int(11) unsigned not null auto_increment,
	`bucket_profile_id` int(11) unsigned null,
	`keyword_id` int(11) unsigned null,
	constraint `pk|bucket_profile_keyword` primary key (`id`),
	constraint `fk|bucket_profile_keyword|bucket_profile` foreign key (`bucket_profile_id`) references `bucket_profile` (`id`),
	constraint `fk|bucket_profile_keyword|neighbourhood` foreign key (`keyword_id`) references `keyword` (`id`)
) engine=innodb auto_increment=9 default charset=utf8;
create table `bucket_profile_run` (
	`id` int(11) unsigned not null auto_increment,
	`bucket_profile_id` int(11) unsigned null,
	`start` timestamp not null,
	`started_by` int(11) unsigned not null,
	`finish` timestamp null,
	`finished_by` int(11) unsigned null,
	`running` int(11) unsigned not null default 0,
	constraint `pk|bucket_profile_run` primary key (`id`),
	constraint `fk|bucket_profile_run|bucket_profile` foreign key (`bucket_profile_id`) references `bucket_profile` (`id`),
	constraint `fk|bucket_profile_run|started_by` foreign key (`started_by`) references `user` (`id`),
	constraint `fk|bucket_profile_run|finished_by` foreign key (`finished_by`) references `user` (`id`)
) engine=innodb default charset=utf8;
create table `bucket` (
	`id` int(11) unsigned not null auto_increment,
	`bucket_profile_run_id` int(11) unsigned not null,
	`user_id` int(11) unsigned not null,
	`enabled` int(11) unsigned not null default 1,
	constraint `pk|bucket` primary key (`id`),
	constraint `fk|bucket|bucket_profile_run` foreign key (`bucket_profile_run_id`) references `bucket_profile_run` (`id`),
	constraint `fk|bucket|user` foreign key (`user_id`) references `user` (`id`),
	constraint `uc|bucket` unique (`bucket_profile_run_id`, `user_id`)
) engine=innodb default charset=utf8;
create table `bucket_company` (
	`id` int(11) unsigned not null auto_increment,
	`bucket_id` int(11) unsigned not null,
	`company_id` int(11) unsigned not null,
	`current` int(11) unsigned not null default 0,
	`evaluation_result` int(11) unsigned null default null,
    `evaluation_timestamp` timestamp null default null,
	constraint `pk|bucket_company` primary key (`id`),
	constraint `fk|bucket_company|bucket` foreign key (`bucket_id`) references `bucket` (`id`),
	constraint `fk|bucket_company|company` foreign key (`company_id`) references `company` (`id`),
	constraint `uc|bucket_company` unique (`bucket_id`, `company_id`)
) engine=innodb default charset=utf8;
create index `ix|bucket_company|current` on `bucket_company` (`current`);

drop table if exists queue_sale_review;
drop table if exists queue_sale_lines;
drop table if exists queue_sale;
drop table if exists queue_wait;

drop table if exists company_comment;
drop table if exists company_sale_review;
drop table if exists company_sale_portability;
drop table if exists company_sale;

create table `company_comment` (
	`id` int(11) unsigned not null auto_increment,
	`company_id` int(11) unsigned not null,
	`user_id` int(11) unsigned not null,
	`comment` varchar(120) not null,
	`timestamp` timestamp not null default now(),
	constraint `pk|company_comment` primary key (`id`),
	constraint `fk|company_comment|company` foreign key (`company_id`) references `company` (`id`),
	constraint `fk|company_comment|user` foreign key (`user_id`) references `user` (`id`)
) engine=innodb default charset=utf8;
create table `company_sale` (
	`id` int(11) unsigned not null auto_increment,
	`account_id` int(11) unsigned not null,
	`bucket_company_id` int(11) unsigned null,
	`company_address_id` int(11) unsigned null,
	`plan_id` int(11) unsigned null,
	`soho_plan` varchar(60) null,
	`soho_campaign` varchar(60) null,
	`soho_value` decimal(6,2) unsigned null,
	`next_contact` timestamp null,
	`sold` int(11) unsigned not null default 0,
	`closed` int(11) unsigned not null default 0,
	`approved` int(11) unsigned null default null,
	`closed_by` int(11) unsigned null,
	`timestamp` timestamp not null default now(),
	`timestamp_close` timestamp null default null,
	constraint `pk|company_sale` primary key (`id`),
	constraint `fk|company_sale|account` foreign key (`account_id`) references `account` (`id`),
	constraint `fk|company_sale|bucket_company` foreign key (`bucket_company_id`) references `bucket_company` (`id`),
	constraint `fk|company_sale|plan` foreign key (`plan_id`) references `plan` (`id`),
	constraint `fk|company_sale|company_address` foreign key (`company_address_id`) references `company_address` (`id`),
	constraint `fk|company_sale|user_closer` foreign key (`closed_by`) references `user` (`id`)
) engine=innodb default charset=utf8;
create table `company_sale_review` (
	`id` int(11) unsigned not null auto_increment,
	`company_sale_id` int(11) unsigned not null,
	`status_id` int(11) unsigned not null,
	`user_id` int(11) unsigned not null,
	`comment` varchar(120) null,
	`timestamp` timestamp not null default now(),
	constraint `pk|company_sale_review` primary key (`id`),
	constraint `fk|company_sale_review|company_sale` foreign key (`company_sale_id`) references `company_sale` (`id`),
	constraint `fk|company_sale_review|status` foreign key (`status_id`) references `status` (`id`),
	constraint `fk|company_sale_review|user` foreign key (`user_id`) references `user` (`id`)
) engine=innodb default charset=utf8;
create table `company_sale_portability` (
	`id` int(11) unsigned not null auto_increment,
	`company_sale_id` int(11) unsigned not null,
	`ddd` varchar(2) not null,
	`phone` varchar(10) not null,
	constraint `pk|company_sale_portability` primary key (`id`),
	constraint `fk|company_sale_portability|company_sale` foreign key (`company_sale_id`) references `company_sale` (`id`),
	constraint `uc|company_sale_portability` unique (`company_sale_id`, `ddd`, `phone`)
) engine=innodb default charset=utf8;