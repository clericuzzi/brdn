drop schema if exists brdn;
create schema brdn;
use brdn;

create table `concurrency_manager` (
	`id` int(11) unsigned not null auto_increment,
	`instancia_id` int(11) unsigned not null,
	`entidade_id` int(11) unsigned not null,
	constraint `pk|concurrency_manager` primary key (`id`)
) engine=innodb default charset=utf8;
create table `empresa` (
	`id` int(11) unsigned not null auto_increment,
	`nome_fantasia` varchar(90) not null,
	`data_cadastro` timestamp not null default now(),
	constraint `pk|empresa` primary key (`id`)
	-- constraint `uc|company` unique (`cnpj`)
) engine=innodb default charset=utf8;
create table `endereco` (
	`id` int(11) unsigned not null auto_increment,
	`empresa_id` int(11) unsigned not null,
    `rua` varchar(120) not null,
	`numero` varchar(12) null,
	`zipcode` varchar(8) null,
    `cobertura` tinyint(1) unsigned null default 0,
    `disponibilidade` tinyint(1) unsigned null default 0,
	`data_do_teste` timestamp null,
	constraint `pk|endereco` primary key (`id`),
	constraint `fk|endereco|company` foreign key (`empresa_id`) references `empresa` (`id`)
) engine=innodb default charset=utf8;