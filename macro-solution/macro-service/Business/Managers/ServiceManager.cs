﻿using System;
using System.IO;
using Brdn.MacrosImpl;
using System.Threading;
using System.Diagnostics;
using macro_service.Utils;
using Clericuzzi.Lib.Email;
using System.Threading.Tasks;
using Clericuzzi.Lib.Utils.Core;
using System.Collections.Generic;
using Clericuzzi.Brdn.Macro.Entities;
using Clericuzzi.Lib.Macros.Controller;
using Clericuzzi.Lib.Macros.Definitions;
using Clericuzzi.Lib.Settings.ConfigFile;
using Clericuzzi.Lib.Concurrency.Semaphore;
using Clericuzzi.Lib.Http.Business.Requesters;
using Clericuzzi.Lib.Http.Communication.Request;
using Clericuzzi.Lib.Http.Communication.Response;
using Clericuzzi.Lib.Selenium.Business.CaptchaSolving;

namespace macro_service.Business.Managers
{
    public static class ServiceManager
    {
        const string CURRENT_VERSION_PARAM = "current-version";

        static Timer _Timer;
        static MacroMachine _Config;
        static Dictionary<string, bool> _RunningDict = new Dictionary<string, bool>();

        static string _AppPath;
        static string _AppFolder;
        static string _DownloadPath;
        static string _CurrentVersion;

        public static void SetDomain(string domainPath)
        {
            _AppPath = Path.Combine(domainPath, "clericuzzi-brdn-macros-impl.exe");
            _DownloadPath = Path.Combine(domainPath, "minerador.zip");
            _AppFolder = domainPath;
        }

        public static async Task Run()
        {
            _Init();
        }

        static void _Init()
        {
            try
            {
                EmailManager.Config("Relatório de erros", "dev.clericuzzi@gmail.com", "dev.clericuzzi@gmail.com", "P3DR0C13R1CUZZ1", "smtp.gmail.com", 465);

                RequestMessageDefaults.Crypted = false;
                RequestMessageDefaults.Compressed = false;
                RequestMessageDefaults.MachineName = Environment.MachineName;
                RequestMessageDefaults.RootEndpoint = "http://54.70.216.46:5018/api/";
                //RequestMessageDefaults.RootEndpoint = "http://localhost:58111/api/";
                _Kill();

                PathDefinitions.DownloadChromeDriver("https://chromedriver.storage.googleapis.com/78.0.3904.70/chromedriver_win32.zip");
                PathDefinitions.InitTempFolder();

                CryptUtils.InitSecret("ikKRUxCmHL86nzBim&X0v1M*xm@CtGGx36V7usRrrXfHH#0$i!WuHeEDcJ%s4Wukvv8FU@Z9sOCPdeGj5!nTnLU&0Qd@iAX0i9J");
                CaptchaSolver.Init("97df6a5f5be10a4ad46dac3266e79f05");
                SimpleSemaphore.Init(PathDefinitions.TempFolder);

                ConfigFileManager.Init();
                _CurrentVersion = ConfigFileManager.GetValue<string>(CURRENT_VERSION_PARAM);
                _Timer = new Timer(_Check, null, Constants.CALLBACK_DELAY, Constants.CALLBACK_PERIOD);
            }
            catch (Exception ex)
            {
                WindowsUtils.EventLogWrite($"Inicialização do serviço com falha {_AppFolder}\r\n{_AppPath} \r\n ({ex.Message})", type: EventLogEntryType.Error);
            }
        }

        public static void StopAllTasks()
        {
            _Kill();
        }

        static void _CheckVersion(string currentversion)
        {
            if (string.IsNullOrEmpty(_CurrentVersion) || !currentversion.Equals(_CurrentVersion))
            {
                _Kill();
                _DownloadVersion();
            }

            _CurrentVersion = currentversion;
            ConfigFileManager.Add(CURRENT_VERSION_PARAM, _CurrentVersion);
        }
        static void _DownloadVersion()
        {
            _DownloadPath.DeleteFile();

            WindowsUtils.DownloadFileFromUrl("http://54.70.216.46/minerador.zip", _DownloadPath);
            WindowsUtils.Unzip(_DownloadPath, _AppFolder);
            _DownloadPath.DeleteFile();
        }

        static async Task _LoadOrRegister()
        {
            try
            {
                var request = new RequestMessage(RequestMessageDefaults.GetUrl("macro-config/load-or-register"));
                var response = await request.Post<SingleResponse<MacroMachine>>();
                if (!string.IsNullOrEmpty(response?.ErrorMessage))
                    EmailManager.SimpleSmtp("relatorio@brdn.com.br", "Sistema Brdn", $"Falha no registro da máquina {RequestMessageDefaults.MachineName}", $"Houve um problema durante o início do serviço de mineração:\r\n\r\n{response.ErrorMessage}");
                else
                {
                    _Config = response.Entity;
                    //var currentversion = response.DeserializeTo<string>(CURRENT_VERSION_PARAM);
                    //_CheckVersion(currentversion);
                }
                var currentConfigs = string.Empty;
                foreach (var configItem in _Config.Configurations)
                    currentConfigs += $" {configItem.MacroIdParent.Macro}";

                WindowsUtils.EventLogWrite($"Configurações sincronizadas: {currentConfigs.Trim()}");
            }
            catch (Exception ex)
            {
                EmailManager.SimpleSmtp("relatorio@brdn.com.br", "Sistema Brdn", $"Falha no registro da máquina {RequestMessageDefaults.MachineName}", $"Houve um problema durante o início do serviço de mineração:\r\n\r\n{ex.Message}");
                WindowsUtils.EventLogWrite($"Erro: {ex.Message}", type: EventLogEntryType.Error);
            }
        }

        static void _Check(Object stateInfo)
        {
            Task.Run(async () => await _GetCurrentConfig());
        }
        static async Task _GetCurrentConfig()
        {
            WindowsUtils.EventLogWrite($"Atualizando as configurações...");
            await _LoadOrRegister();
            if (_Config.Updated > 0)
                _Kill();

            if (_Config?.Configurations?.Count > 0)
                foreach (var config in _Config.Configurations)
                    _CheckRunInterval(config);
        }

        static void _Run(MacroMachineConfig config)
        {
            WindowsUtils.EventLogWrite($"Iniciando a macro: {config.MacroIdParent.Macro}, roda em plano de fundo: {(config.FromBackground == 1 ? "sim" : "não")}");
            var actionName = config.MacroIdParent.Macro;
            if (!_RunningDict.ContainsKey(actionName))
                _RunningDict.Add(actionName, true);
            for (int i = 0; i < config.Instances; i++)
            {
                if (config.FromBackground == 1)
                    Task.Run(async () => await MacrosProgram.GetNewAction(config.MacroId));
                //else
                //{
                //    var command = _AppPath;
                //    for (int j = 0; j < config.Instances; j++)
                //        command += $" {config.MacroId}";

                //    WindowsUtils.EventLogWrite($"Comando de execução: {command}");
                //    Process.Start(command);
                //}
            }
        }
        static void _Kill()
        {
            _RunningDict = new Dictionary<string, bool>();
            MacroControl.KillAll();
        }
        static void _CheckRunInterval(MacroMachineConfig config)
        {
            _RunningDict.TryGetValue(config.MacroIdParent.Macro, out bool isRunning);
            var shouldRun = _ShouldRun(config);
            WindowsUtils.EventLogWrite($"Validação periódica {config.MacroIdParent.Macro}, está rodando: {(isRunning ? "sim" : "não")}, deveria estart: {(shouldRun ? "sim" : "não")}");
            if (shouldRun && !isRunning)
                _Run(config);

            if (!shouldRun && isRunning)
                _Kill();
        }
        static bool _ShouldRun(MacroMachineConfig config)
        {
            var shouldRun = false;
            var currentHour = DateTime.Now.Hour;
            if (config.Starting.HasValue && !config.Ending.HasValue)
                shouldRun = currentHour >= config.Starting.Value;
            else if (!config.Starting.HasValue && config.Ending.HasValue)
                shouldRun = currentHour <= config.Ending.Value;
            else if (config.Starting.HasValue && config.Ending.HasValue)
                shouldRun = currentHour >= config.Starting.Value && currentHour <= config.Ending.Value;
            else
                shouldRun = true;

            return shouldRun;
        }
    }
}
