﻿using System.ComponentModel;

namespace macro_service.Business.Enum
{
    public enum RunStatesEnum
    {
        [Description("There ARE macros running")]
        Running = 1,
        [Description("There are NONE macros running")]
        Stopped = 2,
    }
}
