﻿using System;
using System.Reflection;
using System.ServiceProcess;
using System.Configuration.Install;
using Clericuzzi.Lib.Utils.Core;
using macro_service.Utils;
using macro_service.Business.Managers;
using System.Threading.Tasks;

namespace macro_service
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            if (Environment.UserInteractive)
            {
                string parameter = string.Concat(args);
                switch (parameter)
                {
                    case "--i":
                        ManagedInstallerClass.InstallHelper(new[] { Assembly.GetExecutingAssembly().Location });
                        break;
                    case "--u":
                        ManagedInstallerClass.InstallHelper(new[] { "/u", Assembly.GetExecutingAssembly().Location });
                        break;
                    case "--install":
                        ManagedInstallerClass.InstallHelper(new[] { Assembly.GetExecutingAssembly().Location });
                        break;
                    case "--uninstall":
                        ManagedInstallerClass.InstallHelper(new[] { "/u", Assembly.GetExecutingAssembly().Location });
                        break;
                }
            }
            else
                ServiceBase.Run(new ServiceBase[] { new BRDNService() });

            //WindowsUtils.EventLogConfig(Constants.SERVICE_NAME);
            //WindowsUtils.EventLogWrite($"Serviço de mineração INICIADO");
            //ServiceManager.SetDomain(AppDomain.CurrentDomain.BaseDirectory);
            //ServiceManager.StopAllTasks();
            //ServiceManager.Run().Wait();

            //Task.Delay(600000).Wait();
        }
    }
}
