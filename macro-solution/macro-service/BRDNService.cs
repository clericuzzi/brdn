﻿using System;
using macro_service.Utils;
using System.ServiceProcess;
using System.Threading.Tasks;
using Clericuzzi.Lib.Utils.Core;
using macro_service.Business.Managers;

namespace macro_service
{
    public partial class BRDNService : ServiceBase
    {
        public BRDNService()
        {
            InitializeComponent();

            ServiceName = Constants.SERVICE_NAME;
            WindowsUtils.EventLogConfig(Constants.SERVICE_NAME);
            ServiceManager.SetDomain(AppDomain.CurrentDomain.BaseDirectory);
        }

        protected override void OnStart(string[] args)
        {
            WindowsUtils.EventLogWrite($"Serviço de mineração INICIADO v.02");
            ServiceManager.StopAllTasks();
            Task.Run(async () => await ServiceManager.Run());
        }

        protected override void OnStop()
        {
            ServiceManager.StopAllTasks();
            WindowsUtils.EventLogWrite("Serviço de mineração FINALIZADO");
        }
    }
}
