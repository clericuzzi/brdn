﻿using macro_service.Utils;
using System.ComponentModel;
using System.ServiceProcess;
using System.Configuration.Install;

namespace macro_service
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();
            _Installer.ServiceName = Constants.SERVICE_NAME;
        }

        private void _Installer_AfterInstall(object sender, InstallEventArgs e)
        {
            new ServiceController(_Installer.ServiceName).Start();
        }
    }
}
