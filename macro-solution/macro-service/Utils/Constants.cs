﻿namespace macro_service.Utils
{
    public static class Constants
    {
        public const int CALLBACK_DELAY = 5000;
        public const int CALLBACK_PERIOD = 300000;
        public const string APP_DOMAIN = "Application";
        public const string SERVICE_NAME = "BRDN :: Mineração";
    }
}