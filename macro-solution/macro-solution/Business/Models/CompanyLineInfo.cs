﻿using Clericuzzi.Lib.Selenium.Extensions;
using Clericuzzi.Lib.Selenium.Util.Constants;
using Clericuzzi.Lib.Utils.Core;
using OpenQA.Selenium;
using System;

namespace Brdn.MacrosImpl.Business.Models
{
    public class CompanyLineInfo
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public string Operator { get; set; }
        public DateTime? DateSince { get; set; }

        public CompanyLineInfo()
        {

        }
        public CompanyLineInfo(IWebElement tr)
        {
            var tds = tr.GetChildrenByTag(TagConstants.TD);
            Number = tds[0].Text;
            Operator = tds[1].Text;
            if (!string.IsNullOrEmpty(tds[3].Text))
                DateSince = CastUtils.CastTo<DateTime>(tds[3].Text);
        }
    }
}
