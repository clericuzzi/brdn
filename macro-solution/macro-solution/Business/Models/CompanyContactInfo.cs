﻿using System.Collections.Generic;

namespace Brdn.MacrosImpl.Business.Models
{
    public class CompanyContactInfo
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public List<string> Phones { get; set; }
    }
}
