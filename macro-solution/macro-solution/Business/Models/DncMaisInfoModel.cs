﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Brdn.MacrosImpl.Business.Models
{
    public class DncMaisInfoModel
    {
        public int CompanyLineId { get; set; }
        public string Ddd { get; set; }
        public string Phone { get; set; }
        public int CanCall { get; set; }
    }
}
