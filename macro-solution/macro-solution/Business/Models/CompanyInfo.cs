﻿using System.Collections.Generic;
using Clericuzzi.Brdn.Macro.Entities;

namespace Clericuzzi.Brdn.Macro.Impl.Business.Models
{
    public class CompanyInfo : IEqualityComparer<CompanyInfo>
    {
        public bool IsValid
        {
            get
            {
                int.TryParse(CompanyAddress?.Number, out var number);
                return !string.IsNullOrEmpty(Company?.Phone) && CompanyAddress?.ZipcodeIdParent != null && number > 0;
            }
        }
        public string Uid
        {
            get
            {
                var uid = $"{Company.FantasyName}|{Company.Phone}|{Company.Description}|{CompanyAddress.ZipcodeId}|{CompanyAddress.Number}";
                while (uid.Contains("||"))
                    uid = uid.Replace("||", "|");

                return uid;
            }
        }
        public Company Company { get; set; }
        public CompanyAddress CompanyAddress { get; set; }
        public List<CompanyLine> CompanyLines { get; set; }

        public CompanyInfo()
        {
        }

        public bool Equals(CompanyInfo x, CompanyInfo y)
        {
            return x.Uid.Equals(y.Uid);
        }
        public int GetHashCode(CompanyInfo obj)
        {
            return obj.GetHashCode();
        }
        public override string ToString()
        {
            return Uid;
        }
    }
}
