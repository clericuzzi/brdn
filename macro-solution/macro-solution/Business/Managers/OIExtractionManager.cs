﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Clericuzzi.Lib.Utils.Core;

namespace Brdn.MacrosImpl.Business.Managers
{
    public static class OIExtractionManager
    {
        public static string CurrentUf;
        public static string CurrentCity;

        private static string FileDone { get { return Path.Combine(ConsoleUtils.LocationEntry, "targeting", "oi_done.txt"); } }
        private static string FileToDo { get { return Path.Combine(ConsoleUtils.LocationEntry, "targeting", "oi_todo.txt"); } }
        private static string FilesFolder { get { return Path.Combine(ConsoleUtils.LocationEntry, "targeting"); } }

        public static void GetNextUf()
        {
            FilesFolder.CreateIfNotExists();
            FileDone.CreateIfNotExists(true);
            FileToDo.CreateIfNotExists(true);

            CurrentUf = FileToDo.ReadTextFromFile().GetRandomItem();
        }
        public static string GetNextCity(List<string> availableCities)
        {
            var done = _GetDoneCitiesByCurrentUf();
            var notDone = availableCities.Where(i => !done.Contains(i)).ToList();

            return notDone.GetRandomItem();
        }

        static List<string> _GetDoneCitiesByCurrentUf()
        {
            var lines = FileDone.ReadTextFromFile();
            var cities = new List<string>();

            return lines.Where(i => i.StartsWith(CurrentUf)).Select(i => i.Split(';')[1]).ToList();
        }
    }
}
