﻿using System;
using System.Linq;
using Clericuzzi.Lib.Email;
using System.Threading.Tasks;
using Macros.SmartWeb.ByCnpj;
using Clericuzzi.Lib.Utils.Core;
using System.Collections.Generic;
using Clericuzzi.Brdn.Macro.Entities;
using Clericuzzi.Lib.Macros.Controller;
using Clericuzzi.Lib.Macros.Definitions;
using Clericuzzi.Lib.Concurrency.Semaphore;
using Clericuzzi.Lib.Http.Business.Requesters;
using Clericuzzi.Lib.Http.Communication.Request;
using Clericuzzi.Lib.Http.Communication.Response;
using Clericuzzi.Brdn.Macro.Entities.Business.Enums;
using Clericuzzi.Lib.Selenium.Business.CaptchaSolving;
using Macros.Abr.GetOperator;

namespace Brdn.MacrosImpl
{
    public class MacrosProgram
    {
        static MacroMachine _Config;
        public static int RestartsIn = 100;
        public static string MacroLogin;
        static void Main(string[] args)
        {
            MacroControl.KillAll();
            _Init(args);
        }
        static void _Init(string[] args)
        {
            try
            {
                WindowsUtils.EventLogConfig("BRDN :: Mineração");
                EmailManager.Config("Relatório de erros", "dev.clericuzzi@gmail.com", "dev.clericuzzi@gmail.com", "P3DR0C13R1CUZZ1", "smtp.gmail.com", 465);

                RequestMessageDefaults.Crypted = false;
                RequestMessageDefaults.Compressed = false;
                RequestMessageDefaults.MachineName = Environment.MachineName;
                RequestMessageDefaults.RootEndpoint = "http://54.70.216.46:5018/api/";
#if DEBUG
                //RequestMessageDefaults.RootEndpoint = "http://localhost:58111/api/";
#endif

                MacroControl.KillAll();
                ProcessUtils.Kill("chromedriver");

                PathDefinitions.DownloadChromeDriver("https://chromedriver.storage.googleapis.com/78.0.3904.70/chromedriver_win32.zip");
                PathDefinitions.InitTempFolder();

                SimpleSemaphore.Init(PathDefinitions.TempFolder);
                CryptUtils.InitSecret("ikKRUxCmHL86nzBim&X0v1M*xm@CtGGx36V7usRrrXfHH#0$i!WuHeEDcJ%s4Wukvv8FU@Z9sOCPdeGj5!nTnLU&0Qd@iAX0i9J");
                CaptchaSolver.Init("97df6a5f5be10a4ad46dac3266e79f05");

                _GetMachineConfig().Wait();

                var tasks = new List<Task>();
                foreach (var configItem in _Config.Configurations)
                    for (int i = 0; i < configItem.Instances; i++)
                        tasks.Add(GetNewAction(configItem.MacroId, false));

                Task.WhenAll(tasks.ToArray()).Wait();
            }
            catch (Exception ex)
            {
                WindowsUtils.EventLogWrite($"Error: {ex.Message}\r\n\r\n{ex.StackTrace}");
            }
        }

        public static MacroLogin GetLoginInfo(int macroId)
        {
            var macroLogin = _Config.Configurations.FirstOrDefault(i => i.MacroId.Equals(macroId))?.MacroIdParent;

            return macroLogin;
        }
        static async Task _GetMachineConfig()
        {
            var request = new RequestMessage(RequestMessageDefaults.GetUrl("macro-config/load-or-register"));
            try
            {
                var response = await request.Post<SingleResponse<MacroMachine>>();

                _Config = response.Entity;
            }
            catch (Exception ex)
            {
            }
        }

        public static Task GetNewAction(int macroId, bool runHeadless = true)
        {
            var headless = runHeadless;
#if DEBUG
            headless = false;
#endif

            return new AbrSohoController(headless).Run();
            //var action = (MacroNamesEnum)macroId;
            //switch (action)
            //{
            //    case MacroNamesEnum.MiningCatta:
            //        return null;
            //    case MacroNamesEnum.SmartWebByZipAndNumber:
            //        return new SmartWebByZipAndNumberController(headless).Run();
            //    case MacroNamesEnum.SmartWebByCnpj:
            //        return new SmartWebByCnpjController(headless).Run();
            //    default:
            //        return null;
            //    case MacroNamesEnum.Simplifique:
            //        return new SimplifiqueController(headless).Run();
            //    case MacroNamesEnum.MaisInfo:
            //        return new MacroDncMaisInfoController(headless).Run();
            //    case MacroNamesEnum.MiningGoogle:
            //        return new MiningCoogleController(headless).Run();
            //    case MacroNamesEnum.CnpjGoogle:
            //        return new MiningCoogleCnpjController(headless).Run();
            //    case MacroNamesEnum.CnpjCheck:
            //        return new CnpjCheckController(headless).Run();
            //    case MacroNamesEnum.AbrTelecomSoho:
            //        return new AbrSohoController(headless).Run();
            //    default:
            //        return new CnpjCheckController(headless).Run();
            //}
        }
        public static string GetActionName(int macroId)
        {
            var name = string.Empty;
            var action = (MacroNamesEnum)macroId;
            switch (action)
            {
                case MacroNamesEnum.MiningCatta:
                    name = null;
                    break;
                case MacroNamesEnum.SmartWebByZipAndNumber:
                    name = "SmartWebByZipAndNumberController";
                    break;
                case MacroNamesEnum.Simplifique:
                    name = "SimplifiqueController";
                    break;
                case MacroNamesEnum.MaisInfo:
                    name = "MacroDncMaisInfoController";
                    break;
                case MacroNamesEnum.SmartWebByCnpj:
                    name = "SmartWebByCnpjController";
                    break;
                case MacroNamesEnum.MiningGoogle:
                    name = "MiningCoogleController";
                    break;
                case MacroNamesEnum.CnpjGoogle:
                    name = "MiningCoogleCnpjController";
                    break;
                case MacroNamesEnum.CnpjCheck:
                    name = "CnpjCheckController";
                    break;
                case MacroNamesEnum.AbrTelecomSoho:
                    name = "AbrSohoController";
                    break;
                default:
                    name = "CnpjCheckController";
                    break;
            }

            return name;
        }
    }
}