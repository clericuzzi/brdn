using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Clericuzzi.Lib.Utils.Core;
using Clericuzzi.Brdn.Macro.Entities;
using Clericuzzi.Lib.Macros.Controller;
using Clericuzzi.Lib.Macros.Definitions;
using Clericuzzi.Lib.Http.Utils.Constants;
using Clericuzzi.Lib.Selenium.Util.Constants;
using Clericuzzi.Lib.Http.Business.Requesters;
using Clericuzzi.Lib.Http.Communication.Request;
using Clericuzzi.Lib.Http.Communication.Response;
using Brdn.MacrosImpl;
using Clericuzzi.Lib.Selenium.Business.CaptchaSolving;
using Clericuzzi.Lib.Selenium.Extensions;
using System.Collections.Generic;

namespace Macros.Simplifique
{
    public class SimplifiqueControllerImpl : MacroControllerImpl<Company>
    {
        const string CLEAR_PATH = "macro/simplifique/clear";
        const string SOURCE_PATH = "macro/simplifique/source";
        const string CONSOLIDATE_PATH = "macro/simplifique/consolidate";

        private string _CompanyName;
        private string _ConvergingMessage;
        private string _CompanyWalletLand;
        private string _CompanyWalletMobile;
        private List<CompanyBill> _Bills;

        public SimplifiqueControllerImpl()
        {
            LOOPS = true;
            RestartsIn = MacrosProgram.RestartsIn;
        }

        public override void RunSync()
        {
            HasError = true;
            try
            {
                HasError = false;
            }
            catch (Exception ex)
            {
                HasError = true;
                ex.LogDetails();
            }
        }
        public override async Task RunAsync()
        {
            _CompanyName = null;
            _ConvergingMessage = null;
            _CompanyWalletLand = null;
            _CompanyWalletMobile = null;
            _Bills = new List<CompanyBill>();
            HasError = true;
            if (Item != null)
            {
                try
                {
                    Console.WriteLine($"Nova avaliação, {Item}| Restarts in: {RestartsIn}");
                    await Task.Delay(150);

                    _ValidateUrl();
                    await _Search();
                    _GetReult();

                    HasError = false;
                }
                catch (Exception ex)
                {
                    HasError = true;
                    ex.LogDetails();

                    throw ex;
                }
            }
            else
                throw new Exception("Item cannot be null");

            await CheckLoopRestart();
        }

        async Task _Search()
        {
            Driver.SetText("/html/body/form/div[4]/div[1]/div/div[2]/div[1]/input[1]", Item.Cnpj.DigitsOnly().Trim(), 45);
            Driver.ClickByXPath("/html/body/form/div[4]/div[1]/div/div[2]/div[2]/div/a");

            Driver.Timeout(1500);

            Driver.WaitForFunction(750, () => Driver.IsEnabledAndDisplayed("/html/body/form/div[4]/div[6]/div/div/div[3]/a"));
            var siteKey = Driver.GetByTagAndClass(TagConstants.DIV, "g-recaptcha")?.GetAttribute("data-sitekey");
            var url = Driver.WebDriver.Url;

            Console.WriteLine($"Cnpj: {Item.Cnpj}");
            await CaptchaSolver.SolveCaptcha(siteKey, url);

            Driver.TimeoutRandom();
            var responseElement = Driver.GetById("recaptcha-token");
            Driver.TimeoutRandom();
            Driver.Javascript.ExecuteScript($"document.getElementById('g-recaptcha-response').innerHTML='{CaptchaSolver.Token}';");
            Driver.TimeoutRandom();
            Driver.ClickByXPath("/html/body/form/div[4]/div[6]/div/div/div[3]/a");

            Driver.Timeout(1500);
        }
        void _GetReult()
        {
            Driver.WaitForFunction(750, () => Driver.IsEnabledAndDisplayed("/html/body/form/div[4]/div[1]/div/div[3]/div"));
            Driver.Timeout(750);

            _GetBills();
            _GetCompanyInfo();

            var convergingMessageElement = Driver.GetByXPathOrId("/html/body/form/div[4]/div[1]/div/div[3]/div/div[8]/div[1]");
            _ConvergingMessage = Driver.GetText(convergingMessageElement);
        }

        private void _GetCompanyInfo()
        {
            _CompanyName = Driver.GetByXPathOrId("/html/body/form/div[4]/div[1]/div/div[3]/div/div[2]/table/tbody/tr[1]/td[2]")?.Text;

            var landAdabas = Driver.GetByXPathOrId("/html/body/form/div[4]/div[1]/div/div[3]/div/div[4]/div[1]/div[2]/div[2]/div[2]/table/tbody");
            var landSegment = Driver.GetByXPathOrId("/html/body/form/div[4]/div[1]/div/div[3]/div/div[4]/div[1]/div[2]/div[2]/div[1]/table/tbody/tr[1]/td[2]");
            var mobileAdabas = Driver.GetByXPathOrId("/html/body/form/div[4]/div[1]/div/div[3]/div/div[4]/div[1]/div[1]/div[2]/div[1]/table/tbody/tr[2]/td[2]");
            var mobileSegment = Driver.GetByXPathOrId("/html/body/form/div[4]/div[1]/div/div[3]/div/div[4]/div[1]/div[1]/div[2]/div[1]/table/tbody/tr[1]/td[2]");
            _CompanyWalletLand = $"Segmento: {Driver.GetText(landSegment)} - ADABAS: {Driver.GetText(landAdabas)}";
            _CompanyWalletMobile = $"Segmento: {Driver.GetText(mobileSegment)} - ADABAS: {Driver.GetText(mobileAdabas)}";
        }

        void _GetBills()
        {
            var buttonPath = "/html/body/form/div[4]/div[1]/div/div[3]/div/div[10]/div[2]";
            Driver.WaitForFunction(750, () => Driver.IsEnabledAndDisplayed(buttonPath));
            Driver.ClickByXPath(buttonPath);
            Driver.Timeout(750);

            var bodyElementPath = "/html/body/form/div[4]/div[1]/div/div[3]/div/div[10]/div[1]/table/tbody";
            var bodyElement = Driver.GetByXPathOrId(bodyElementPath);

            var rows = bodyElement.GetChildrenByTag(TagConstants.TR);
            foreach (var row in rows)
            {
                var bill = new CompanyBill();

                var tds = row.GetChildrenByTag(TagConstants.TD);
                bill.Billing = "VIVO";
                bill.Due = CastUtils.CastTo<DateTime>(tds[3].Text);
                double.TryParse(tds[2].Text.DigitsOnly(), out double value);
                bill.Value = value / 100;
                bill.Serial = tds[1].Text;

                _Bills.Add(bill);
            }
        }

        void _ValidateUrl()
        {
            Driver.ClickByXPath("/html/body/form/div[10]/div[2]/div/div[4]/p[1]/a");
        }

        public override async Task Loop(bool brokenLoop = false)
        {
            RequestMessage request = null;
            SingleResponse<Company> response = null;
            if (!HasError)
                request = new RequestMessage(RequestMessageDefaults.GetUrl(CONSOLIDATE_PATH));
            else
                request = new RequestMessage(RequestMessageDefaults.GetUrl(CLEAR_PATH));

            try
            {
                request.AddParam("company-id", Item.Id);
                request.AddParam("company-name", _CompanyName);
                request.AddParam("company-bills", _Bills);
                request.AddParam("converging-message", _ConvergingMessage);
                request.AddParam("company-wallet-land", _CompanyWalletLand);
                request.AddParam("company-wallet-mobile", _CompanyWalletMobile);
                response = await request.Post<SingleResponse<Company>>();
                if (response.HasErrors)
                {
                    Console.WriteLine(response.ErrorMessages.FirstOrDefault());
                    await ForceReset<Company>(CLEAR_PATH, SOURCE_PATH);
                }
                else
                    Item = response.Entity;

                if (HasError)
                    Driver.Kill();
            }
            catch (Exception ex)
            {
                await ForceReset<Company>(CLEAR_PATH, SOURCE_PATH);
                HasError = true;
                ex.LogDetails();
                // throw ex;
            }

            if (!brokenLoop)
                await RunAsync();
        }

        public override async Task Consolidate()
        {
            await Task.Delay(150);
            return;

            //if (_LOOPS)
            //    return;

            //StatusResponse response = null;
            //if (!HasError)
            //{
            //    var request = new RequestMessage(RequestMessageDefaults.GetUrl(CONSOLIDATE_PATH));
            //    request.AddParam(FieldConstants.ENTITY, Item);

            //    response = await request.Post<StatusResponse>();
            //    if (response?.Success ?? false)
            //        Console.WriteLine("Consolidation successfull");
            //    else
            //        Console.WriteLine(response.ErrorMessages.FirstOrDefault());
            //}
            //else
            //    Console.WriteLine("Error running the macro");
        }
    }
}