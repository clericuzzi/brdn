using Clericuzzi.Brdn.Macro.Entities;
using Clericuzzi.Brdn.Macro.Impl.Business.Models;
using Clericuzzi.Lib.Selenium.Business.Core;
using Clericuzzi.Lib.Selenium.Extensions;
using Clericuzzi.Lib.Selenium.Util.Constants;
using Clericuzzi.Lib.Utils.Core;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Macros.CnpjCheck
{
    public static class CnpjCheckControllerUtils
    {
        public static bool ValidateCnpjResult(SeleniumDriver driver)
        {
            return driver.IsEnabledAndDisplayed("/html/body/div/div[2]/div/section[1]/div/div/div/div/div/div/div[1]/div[1]/div/a[1]");
        }
        public static CompanyInfo ParseInformation(SeleniumDriver driver)
        {
            var lines = new List<List<string>>();
            var tableElement = driver.GetByXPathOrId("/html/body/div/div[2]/div/section[1]/div/div/div/div/div/div/div[2]/div/div[1]/table");
            var trs = tableElement.GetChildrenByTag(TagConstants.TR);
            foreach (var tr in trs)
            {
                var tds = tr.GetChildrenByTag(TagConstants.TD);
                if (tds.Count == 0)
                    tds = tr.GetChildrenByTag("th");
                lines.Add(tds.Select(i => i.Text).ToList());
            }
            var phones = _GetInformation(driver, lines, "telefone");
            if (string.IsNullOrEmpty(phones))
                return null;

            var companyInfo = new CompanyInfo();
            companyInfo.Company = new Company();
            companyInfo.CompanyLines = new List<CompanyLine>();
            companyInfo.CompanyAddress = new CompanyAddress();
            companyInfo.CompanyAddress.ZipcodeIdParent = new GeographyZipcode();

            companyInfo.CompanyAddress.ZipcodeIdParent.Zipcode = _GetInformation(driver, lines, "cep")?.DigitsOnly();
            var city = _GetInformation(driver, lines, "municipio");
            var state = _GetInformation(driver, lines, "uf");
            companyInfo.CompanyAddress.Street = _GetInformation(driver, lines, "logradouro");
            companyInfo.CompanyAddress.Number = _GetInformation(driver, lines, "numero");
            companyInfo.CompanyAddress.Compliment = _GetInformation(driver, lines, "complemento");
            var neighbourhood = _GetInformation(driver, lines, "bairro");

            var openingDate = _GetInformation(driver, lines, "data de abertura");
            var openingDateValue = CastUtils.CastTo<DateTime>(openingDate.Split(' ')[0]);

            var dateFormat = @"dd \d\e MMMM \d\e yyyy \�\s hh:mm:ss";
            var lastUpdate = _GetInformation(driver, lines, "ultima atualizacao")?.Split('-')[0].Trim();
            var parseResult = DateTime.TryParseExact(lastUpdate, dateFormat, new CultureInfo("pt-BR"), DateTimeStyles.None, out DateTime value);

            _GetPhoneInfo(driver, lines, companyInfo);
            if (parseResult)
                companyInfo.Company.DateLastUpdate = value;

            companyInfo.Company.Cnpj = _GetInformation(driver, lines, "numero de inscricao")?.Substring(0, 20).DigitsOnly().Trim();
            var capital = _GetInformation(driver, lines, "capital social");
            if (!string.IsNullOrEmpty(capital))
            {
                capital = capital.Substring(0, capital.IndexOf("("));
                if (capital.Contains(" "))
                    capital = capital.Substring(capital.IndexOf(" ") + 1);
                companyInfo.Company.Capital = CastUtils.CastTo<double>(capital);
            }
            companyInfo.Company.DateOpened = openingDateValue;
            companyInfo.Company.FormalName = _GetInformation(driver, lines, "nome empresarial");
            companyInfo.Company.FantasyName = _GetInformation(driver, lines, "nome fantasia");
            companyInfo.Company.JuridicNature = _GetInformation(driver, lines, "codigo e descricao da natureza juridica");
            companyInfo.Company.PrimaryActivity = _GetPrimaryActivity(driver, lines, "atividade economica primaria");
            companyInfo.Company.SituationIdParent = new CompanySituation() { Name = _GetInformation(driver, lines, "situacao cadastral") };
            if (companyInfo.Company.SituationIdParent?.Name == "BAIXADA")
                return null;
            else
                return companyInfo;
        }

        public static CompanyInfo GetInfo(SeleniumDriver driver, string cnpj)
        {
            var tries = 0;
            driver.Navigate($"https://consultacnpj.com/cnpj/{cnpj}");
            while (tries++ < 5)
            {
                driver.WaitForFunction(750, () => ValidateCnpjResult(driver), 5, false);
                if (ValidateCnpjResult(driver))
                    return ParseInformation(driver);

                driver.TimeoutRandom(1500, 3000);
                driver.Navigate($"https://consultacnpj.com/cnpj/{cnpj}");
            }

            return null;
        }

        static void _GetPhoneInfo(SeleniumDriver driver, List<List<string>> lines, CompanyInfo companyInfo)
        {
            var phones = _GetInformation(driver, lines, "telefone");
            if (phones.Contains("/"))
            {
                var phoneList = phones.Split('/').ToList();
                for (int i = 0; i < phoneList.Count; i++)
                    phoneList[i] = phoneList[i].DigitsOnly().Trim();

                var companyPhone = phoneList.FirstOrDefault();
                if (!string.IsNullOrEmpty(companyPhone))
                    companyInfo.Company.Phone = companyPhone.RemoveStartingCharacter("0");

                phoneList.RemoveAt(0);
                foreach (var phone in phoneList)
                {
                    var ddd = phone.Substring(0, 2);
                    var number = phone.RemoveFromHead(2);
                    var firstDigit = number.Substring(0, 1);
                    var isMobile = firstDigit == "9" || firstDigit == "8" || firstDigit == "7";
                    companyInfo.CompanyLines.Add(new CompanyLine { Phone = number, Mobile = isMobile ? 1 : 0 });
                }
            }
            else
            {
                phones = phones.DigitsOnly();
                companyInfo.Company.Phone = phones.RemoveStartingCharacter("0");
            }
        }
        static string _GetInformation(SeleniumDriver driver, List<List<string>> input, string field)
        {
            var currentLine = 0;
            foreach (var inputLine in input)
            {
                var position = 0;
                for (position = 0; position < inputLine.Count; position++)
                {
                    if (inputLine[position].ClearSpecialChars(toLower: true).Trim().Equals(field))
                        return input[currentLine + 1][position];
                }
                currentLine++;
            }

            return null;
        }
        static string _GetPrimaryActivity(SeleniumDriver driver, List<List<string>> input, string field)
        {
            var returnValue = string.Empty;
            var currentLine = 0;
            foreach (var inputLine in input)
            {
                var position = 0;
                for (position = 0; position < inputLine.Count; position++)
                {
                    if (inputLine[position].ClearSpecialChars(toLower: true).Trim().Equals(field))
                        return String.Join(" ", input[currentLine + 2]);
                }
                currentLine++;
            }

            return null;
        }
    }
}