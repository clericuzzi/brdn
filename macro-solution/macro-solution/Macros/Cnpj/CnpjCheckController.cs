using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Clericuzzi.Brdn.Macro.Entities;
using Clericuzzi.Lib.Macros.Controller;
using Clericuzzi.Lib.Macros.Definitions;
using Clericuzzi.Lib.Http.Business.Requesters;
using Clericuzzi.Lib.Http.Communication.Request;
using Clericuzzi.Lib.Http.Communication.Response;
using Clericuzzi.Lib.Utils.Core;

namespace Macros.CnpjCheck
{
    public class CnpjCheckController : MacroController<Company, Company>
    {
        const string SOURCE_PATH = "macro/cnpj-check/source";

        public CnpjCheckController(bool headless) : base(null, new CnpjCheckControllerImpl(), null, headless)
        {
            //RunOnce = true;
            IsAsync = true;
            DebugError = false;
            CloseAfterRun = true;
            HasIndivitualDriver = true;
        }

        public override async Task GetSource()
        {
            Restart();
            ParentEntities = null;
            var requestMessage = new RequestMessage(RequestMessageDefaults.GetUrl(SOURCE_PATH));
            try
            {
                var client = await requestMessage.Post<SingleResponse<Company>>();
                if (client.HasErrors)
                    throw new Exception(client.ErrorMessages[0]);

                ParentEntities = new List<Company> { client?.Entity };
            }
            catch (Exception ex)
            {
                WindowsUtils.EventLogWrite($"Falha ao buscar novo item pelo controller\r\n{ex.Message}\r\n\r\n{ex.StackTrace}");
            }
        }
        public override void CreateConsolidation(Company item)
        {
            ConsolidationItem = item;
        }

        public override void HandleGeneralException(Exception ex)
        {
            ConsolidationItem = null;
        }
        public override void HandleMacroException(MacroException ex)
        {
            ConsolidationItem = null;
        }
    }
}