using System;
using System.Linq;
using System.Threading.Tasks;
using Clericuzzi.Lib.Utils.Core;
using Clericuzzi.Brdn.Macro.Entities;
using Clericuzzi.Lib.Macros.Controller;
using Clericuzzi.Lib.Http.Business.Requesters;
using Clericuzzi.Lib.Http.Communication.Request;
using Clericuzzi.Lib.Http.Communication.Response;
using Clericuzzi.Brdn.Macro.Impl.Business.Models;

namespace Macros.CnpjCheck
{
    public class CnpjCheckControllerImpl : MacroControllerImpl<Company>
    {
		const string CLEAR_PATH = "macro/cnpj-check/clear";
		const string SOURCE_PATH = "macro/cnpj-check/source";
		const string CONSOLIDATE_PATH = "macro/cnpj-check/consolidate";

        CompanyInfo _Info;
        public CnpjCheckControllerImpl()
        {
			LOOPS = true;
            RestartsIn = 60;
        }

        public override void RunSync()
        {
			HasError = true;
			try
			{
				HasError = false;
			}
			catch (Exception ex)
			{
				HasError = true;
				ex.LogDetails();
			}
        }
        public override async Task RunAsync()
        {
			HasError = true;
            if (Item != null)
            {
				try
				{
                    Console.WriteLine($"Nova avaliação, {Item}| Restarts in: {RestartsIn}");
                    Driver.TimeoutRandom(1000, 3000);
					await Task.Delay(150);
                    _Info = CnpjCheckControllerUtils.GetInfo(Driver, Item.Cnpj);

                    HasError = false;
				}
				catch (Exception ex)
                {
                    WindowsUtils.EventLogWrite($"Falha na execução\r\n{ex.Message}\r\n\r\n{ex.StackTrace}");
                    HasError = true;
					ex.LogDetails();
					// throw ex;
				}
            }
            else
                throw new Exception("Item cannot be null");

            await CheckLoopRestart();
        }

        public override async Task Loop(bool brokenLoop = false)
        {
            RequestMessage request = null;
            SingleResponse<Company> response = null;
            if (!HasError)
                request = new RequestMessage(RequestMessageDefaults.GetUrl(CONSOLIDATE_PATH));
            else
                request = new RequestMessage(RequestMessageDefaults.GetUrl(CLEAR_PATH));

            try
            {
                request.AddParam("company-id", Item.Id);
                request.AddParam("company-info", _Info);
                response = await request.Post<SingleResponse<Company>>();
                if (response.HasErrors)
                {
                    Console.WriteLine(response.ErrorMessages.FirstOrDefault());
                    await ForceReset<Company>(CLEAR_PATH, SOURCE_PATH);
                }
                else
                    Item = response.Entity;
            }
            catch (Exception ex)
            {
                WindowsUtils.EventLogWrite($"Falha ao buscar novo item pelo em execução\r\n{ex.Message}\r\n\r\n{ex.StackTrace}");
                await ForceReset<Company>(CLEAR_PATH, SOURCE_PATH);
                HasError = true;
				ex.LogDetails();
				// throw ex;
            }
			
            if (!brokenLoop)
				await RunAsync();
        }

        public override async Task Consolidate()
        {
            await Task.Delay(150);
            return;

            //if (_LOOPS)
            //    return;

            //StatusResponse response = null;
            //if (!HasError)
            //{
            //    var request = new RequestMessage(RequestMessageDefaults.GetUrl(CONSOLIDATE_PATH));
            //    request.AddParam(FieldConstants.ENTITY, Item);

            //    response = await request.Post<StatusResponse>();
            //    if (response?.Success ?? false)
            //        Console.WriteLine("Consolidation successfull");
            //    else
            //        Console.WriteLine(response.ErrorMessages.FirstOrDefault());
            //}
            //else
            //    Console.WriteLine("Error running the macro");
        }
    }
}