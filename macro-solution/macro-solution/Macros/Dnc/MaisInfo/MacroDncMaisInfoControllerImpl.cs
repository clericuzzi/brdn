using System;
using System.IO;
using System.Linq;
using Brdn.MacrosImpl;
using System.Threading.Tasks;
using Clericuzzi.Lib.Utils.Core;
using System.Collections.Generic;
using Brdn.MacrosImpl.Business.Models;
using Clericuzzi.Lib.Macros.Controller;
using Clericuzzi.Lib.Macros.Definitions;
using Clericuzzi.Lib.Http.Utils.Constants;
using Clericuzzi.Lib.Http.Business.Requesters;
using Clericuzzi.Lib.Http.Communication.Request;
using Clericuzzi.Lib.Http.Communication.Response;
using Clericuzzi.Lib.Xls.Business.Core;

namespace Macros.Dnc.MaisInfo
{
    public class MacroDncMaisInfoControllerImpl : MacroControllerImpl<List<DncMaisInfoModel>>
    {
        const string CLEAR_PATH = "macro/dnc/mais-info/clear";
        const string SOURCE_PATH = "macro/dnc/mais-info/source";
        const string CONSOLIDATE_PATH = "macro/dnc/mais-info/consolidate";

        public MacroDncMaisInfoControllerImpl()
        {
            LOOPS = true;
            RestartsIn = MacrosProgram.RestartsIn;
        }

        public override void RunSync()
        {
            HasError = true;
            try
            {
                HasError = false;
            }
            catch (Exception ex)
            {
                HasError = true;
                ex.LogDetails();
            }
        }
        public override async Task RunAsync()
        {
            HasError = true;
            if (Item != null)
            {
                try
                {
                    Console.WriteLine($"Nova avaliação, {Item.Count} items | Restarts in: {RestartsIn}");
                    await Task.Delay(150);
                    _ValidateUrl();
                    string filePath = _CreateFile();
                    _Upload(filePath);
                    _Download();
                    _ParseFile();


                    HasError = false;
                }
                catch (Exception ex)
                {
                    HasError = true;
                    ex.LogDetails();
                    // throw ex;
                }
            }
            else
                throw new Exception("Item cannot be null");

            await CheckLoopRestart();
        }
        void _ParseFile()
        {
            var file = PathDefinitions.DownloadFolder.GetFiles().FirstOrDefault(i => i.FileExtension().Equals("xlsx"));
            if (File.Exists(file))
            {
                var linesInfo = XlsReader.ReadXlsx(file, true);
                foreach (var lineInfo in linesInfo)
                {
                    var currentItem = Item.FirstOrDefault(i => lineInfo[0].Equals(i.Ddd + i.Phone));

                    if (currentItem != null)
                        currentItem.CanCall = lineInfo[1].Equals("OK") ? 2 : 0;
                }
            }
        }

        void _Download()
        {
            Driver.WaitForFunction(750, () => _FileDownloaded(), 20, false);
        }
        void _Upload(string filePath)
        {
            Driver.WaitForFunction(750, () => Driver.IsEnabledAndDisplayed("/html/body/div[1]/div/div[3]/div/div[2]/div/form/div[1]/div[3]/button"));
            Driver.ClickByXPath("/html/body/div[1]/div/div[3]/div/div[2]/div/form/div[1]/div[3]/button");

            Driver.WaitForFunction(750, () => Driver.IsEnabledAndDisplayed("/html/body/div[1]/div/div[3]/div/div[3]/div/div/div[2]/div/form/div/div[1]/input"));
            var inputFile = Driver.GetByXPathOrId("/html/body/div[1]/div/div[3]/div/div[3]/div/div/div[2]/div/form/div/div[1]/input");
            inputFile.SendKeys(filePath);

            Driver.WaitForFunction(750, () => Driver.IsEnabledAndDisplayed("/html/body/div[1]/div/div[3]/div/div[3]/div/div/div[2]/div/div/button"));
            Driver.ClickByXPath("/html/body/div[1]/div/div[3]/div/div[3]/div/div/div[2]/div/div/button");
        }
        string _CreateFile()
        {
            var numbers = string.Join(";", Item.Select(i => i.Ddd + i.Phone).ToList());
            PathDefinitions.DownloadFolder.DeleteAllFiles();
            var filePath = Path.Combine(PathDefinitions.DownloadFolder, "lines.txt");
            filePath.DeleteFile();
            File.WriteAllText(filePath, numbers);
            return filePath;
        }
        void _ValidateUrl()
        {
            if (Driver.WebDriver.Url.Contains("Movel/ConsultarNumero"))
                Driver.Refresh();
            else
                Driver.Navigate("https://www.portalmaisinfo.com.br/Movel/ConsultarNumero");
        }

        bool _FileDownloaded()
        {
            var numFiles = PathDefinitions.DownloadFolder.GetFiles().Count();

            return numFiles == 2;
        }

        public override async Task Loop(bool brokenLoop = false)
        {
            RequestMessage request = null;
            SingleResponse<List<DncMaisInfoModel>> response = null;
            if (!HasError)
                request = new RequestMessage(RequestMessageDefaults.GetUrl(CONSOLIDATE_PATH));
            else
                request = new RequestMessage(RequestMessageDefaults.GetUrl(CLEAR_PATH));

            try
            {
                request.AddParam("lines-info", Item);
                response = await request.Post<SingleResponse<List<DncMaisInfoModel>>>();
                if (response.HasErrors)
                {
                    Console.WriteLine(response.ErrorMessages.FirstOrDefault());
                    await ForceReset<string>(CLEAR_PATH, SOURCE_PATH);
                }
                else
                    Item = response.Entity;
            }
            catch (Exception ex)
            {
                await ForceReset<string>(CLEAR_PATH, SOURCE_PATH);
                HasError = true;
                ex.LogDetails();
                // throw ex;
            }

            if (!brokenLoop)
                await RunAsync();
        }

        public override async Task Consolidate()
        {
            await Task.Delay(150);
            return;

            //if (_LOOPS)
            //    return;

            //StatusResponse response = null;
            //if (!HasError)
            //{
            //    var request = new RequestMessage(RequestMessageDefaults.GetUrl(CONSOLIDATE_PATH));
            //    request.AddParam(FieldConstants.ENTITY, Item);

            //    response = await request.Post<StatusResponse>();
            //    if (response?.Success ?? false)
            //        Console.WriteLine("Consolidation successfull");
            //    else
            //        Console.WriteLine(response.ErrorMessages.FirstOrDefault());
            //}
            //else
            //    Console.WriteLine("Error running the macro");
        }
    }
}