using System;
using System.Linq;
using System.Threading.Tasks;
using Clericuzzi.Lib.Utils.Core;
using Clericuzzi.Lib.Macros.Controller;
using Clericuzzi.Lib.Selenium.Extensions;
using Clericuzzi.Lib.Http.Business.Requesters;
using Clericuzzi.Lib.Http.Communication.Request;
using Clericuzzi.Lib.Http.Communication.Response;
using Clericuzzi.Brdn.Macro.Entities;
using Brdn.MacrosImpl;
using Clericuzzi.Lib.Selenium.Util.Constants;
using Clericuzzi.Lib.Selenium.Business.CaptchaSolving;

namespace Macros.Dnc.MaisInfo
{
    public class MacroDncMaisInfoControllerAuth : MacroControllerAuth
    {
        /// <summary>
        /// Singleton implementation for MacroDncMaisInfoControllerAuth
        /// </summary>
        public static MacroDncMaisInfoControllerAuth Instance { get; } = new MacroDncMaisInfoControllerAuth();
        // Explicit static constructor to tell C# compiler
        // not to mark type as beforefieldinit
        static MacroDncMaisInfoControllerAuth()
        {
        }
        private MacroDncMaisInfoControllerAuth()
        {
        }

        const string LOGIN_COMPONENT = "/html/body/div[2]/section/form/div[2]/div/div/input";
        const string PASSWORD_COMPONENT = "/html/body/div[2]/section/form/div[3]/div[1]/div/input";
        public new MacroLogin AuthInformation { get; set; }

        public override async Task Authenticate()
        {
            var iframe = Driver.GetByXPathOrId("/html/body/div[2]/section/form/div[3]/div[2]/div[1]/div/div/div/iframe");
            var siteKey = iframe.GetAttribute("src").GetSection("&k=", "&co");
            var url = Driver.WebDriver.Url;

            var buttonXPath = "/html/body/div[2]/section/form/div[3]/div[2]/div[2]/button";

            await CaptchaSolver.SolveCaptcha(siteKey, url);
            Driver.TimeoutRandom();
            Driver.Javascript.ExecuteScript($"document.getElementById('g-recaptcha-response').innerHTML='{CaptchaSolver.Token}';");
            Driver.TimeoutRandom();

            Driver.ClickByXPath(buttonXPath);
        }
        public override bool AuthenticateScreenReady()
        {
            var loginInput = Driver.GetByXPathOrId(LOGIN_COMPONENT);

            return loginInput != null;
        }
        public override bool CheckFill()
        {
            var loginInput = Driver.GetByXPathOrId(LOGIN_COMPONENT);
            if (loginInput != null)
            {
                var loginText = loginInput.GetAttribute("value");
                if (!loginText.Equals(AuthInformation.Login))
                    return false;
            }

            var passwordInput = Driver.GetByXPathOrId(PASSWORD_COMPONENT);
            if (!passwordInput.IsTextEqualsTo(AuthInformation.Password))
                return false;

            return true;
        }
        public override void FillInfo()
        {
            Driver.SetText(LOGIN_COMPONENT, AuthInformation.Login, 25);
            Driver.SetText(PASSWORD_COMPONENT, AuthInformation.Password, 25);
        }
        public override bool Logged()
        {
            Driver.WaitForFunction(500, () => IsLogged() || IsLoginInvalid(), timeoutMessage: "Could not determine if the user was logged in or not...");

            return IsLogged();
        }
        public bool IsLogged()
        {
            var componentPath = "/html/body/div[1]/div/div[1]/div/div[1]/a/img";
            Driver.WaitForFunction(200, () => Driver.GetByXPath(componentPath)?.Enabled ?? false, 10, true, "Timedout waiting for post login click", "Error waiting for post login click");
            var component = Driver.GetByXPathOrId(componentPath);

            return component != null;
        }
        public bool IsLoginInvalid()
        {
            var alertBarXPath = "";
            var loginButtonXPath = "/html/body/div[2]/section/form/div[3]/div[2]/div[2]/button";

            if (Driver.IsEnabledAndDisplayed(loginButtonXPath) || Driver.IsEnabledAndDisplayed(alertBarXPath))
                return true;
            else
                return false;
        }
        public string GetLoginError()
        {
            var alertBarXPath = "";
            var alertBarElement = Driver.GetByXPathOrId(alertBarXPath);
            var content = alertBarElement?.Text?.ClearSpecialChars(toLower: true);

            return content;
        }
        public override void NavigateTo()
        {
            Driver.GoToUrl(this.AuthInformation.Url);
        }
        public override async Task GetLoginInfo()
        {
            var requestMessage = new RequestMessage(RequestMessageDefaults.GetUrl("crud/macro_login/filter"));
            requestMessage.AddParam("filter", new { Macro = MacrosProgram.MacroLogin });
            var macroLogins = await requestMessage.Post<BatchResponse<MacroLogin>>();
            if (!macroLogins.HasErrors)
                AuthInformation = macroLogins.Entities.FirstOrDefault();

            if (AuthInformation == null)
                throw new Exception($"Sem logins disponíveis para a macro de nome '{MacrosProgram.MacroLogin}'");
        }
        public override async Task StoreLoginInformation()
        {
            var requestMessage = new RequestMessage(RequestMessageDefaults.GetUrl("crud/macro_login/log-error"));
            requestMessage.AddParam("login-id", AuthInformation.Id);
            requestMessage.AddParam("error-message", GetLoginError());

            var response = await requestMessage.Post<StatusResponse>();
        }
    }
}