using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Brdn.MacrosImpl.Business.Models;
using Clericuzzi.Lib.Macros.Controller;
using Clericuzzi.Lib.Macros.Definitions;
using Clericuzzi.Lib.Http.Business.Requesters;
using Clericuzzi.Lib.Http.Communication.Request;
using Clericuzzi.Lib.Http.Communication.Response;

namespace Macros.Dnc.MaisInfo
{
    public class MacroDncMaisInfoController : MacroController<List<DncMaisInfoModel>, List<DncMaisInfoModel>>
    {
        const string SOURCE_PATH = "macro/dnc/mais-info/source";

        public MacroDncMaisInfoController(bool headless) : base(MacroDncMaisInfoControllerAuth.Instance, new MacroDncMaisInfoControllerImpl(), null, headless)
        {
            //RunOnce = true;
            IsAsync = true;
            DebugError = false;
            CloseAfterRun = true;
            HasIndivitualDriver = true;
        }

        public override async Task GetSource()
        {
            Restart();
            ParentEntities = null;
            var requestMessage = new RequestMessage(RequestMessageDefaults.GetUrl(SOURCE_PATH));
            try
            {
                var client = await requestMessage.Post<SingleResponse<List<DncMaisInfoModel>>>();
                if (client.HasErrors)
                    throw new Exception(client.ErrorMessages[0]);

                ParentEntities = new List<List<DncMaisInfoModel>> { client?.Entity };
            }
            catch (Exception ex)
            {
                Console.WriteLine("Datasource FAILED");
                Console.WriteLine(ex.Message);
            }
        }
        public override void CreateConsolidation(List<DncMaisInfoModel> item)
        {
            ConsolidationItem = item;
        }

        public override void HandleGeneralException(Exception ex)
        {
            ConsolidationItem = null;
        }
        public override void HandleMacroException(MacroException ex)
        {
            ConsolidationItem = null;
        }
    }
}