using System;
using System.Threading.Tasks;
using Clericuzzi.Lib.Utils.Core;
using System.Collections.Generic;
using Clericuzzi.Brdn.Macro.Entities;
using Clericuzzi.Lib.Macros.Controller;
using Clericuzzi.Lib.Macros.Definitions;
using Clericuzzi.Lib.Http.Utils.Constants;
using Clericuzzi.Lib.Selenium.Business.Enums;
using Clericuzzi.Lib.Http.Business.Requesters;
using Clericuzzi.Lib.Http.Communication.Request;
using Clericuzzi.Lib.Http.Communication.Response;

namespace Macros.Mining.Google
{
    public class MiningCoogleController : MacroController<MiningGoogle, MiningGoogle>
    {
        const string SOURCE_PATH = "macro/mining-google-source";

        string MachineId;
        public MiningCoogleController(bool headless) : base(null, new MiningCoogleControllerImpl(), null, headless)
        {
            //RunOnce = true;
            IsAsync = true;
            DebugError = false;
            CloseAfterRun = true;
            HasIndivitualDriver = true;
            MachineId = RandomStrings.RandomString(20);
            DriverType = DriverTypeEnum.Chrome;
        }

        public override async Task GetSource()
        {
            Restart();
            ParentEntities = null;
            var requestMessage = new RequestMessage(RequestMessageDefaults.GetUrl(SOURCE_PATH));
            requestMessage.AddParam(FieldConstants.MACHINE_NAME, MachineId, overwrite: true);
            Console.WriteLine($"Id da m�quina: {MachineId}");
            try
            {
                var client = await requestMessage.Post<SingleResponse<MiningGoogle>>();
                if (client.HasErrors)
                    throw new Exception(client.ErrorMessages[0]);

                ParentEntities = new List<MiningGoogle> { client?.Entity };
            }
            catch (Exception ex)
            {
                WindowsUtils.EventLogWrite($"Falha ao buscar novo item pelo controller\r\n{ex.Message}\r\n\r\n{ex.StackTrace}");
            }
        }
        public override void CreateConsolidation(MiningGoogle item)
        {
            ConsolidationItem = item;
        }

        public override void HandleGeneralException(Exception ex)
        {
            ConsolidationItem = null;
        }
        public override void HandleMacroException(MacroException ex)
        {
            ConsolidationItem = null;
        }
    }
}