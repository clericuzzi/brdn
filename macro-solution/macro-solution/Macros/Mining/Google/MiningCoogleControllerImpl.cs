using System;
using System.Linq;
using Brdn.MacrosImpl;
using OpenQA.Selenium;
using System.Threading.Tasks;
using Clericuzzi.Lib.Utils.Core;
using System.Collections.Generic;
using Clericuzzi.Brdn.Macro.Entities;
using Clericuzzi.Lib.Macros.Controller;
using Clericuzzi.Lib.Selenium.Util.Constants;
using Clericuzzi.Lib.Http.Business.Requesters;
using Clericuzzi.Lib.Http.Communication.Request;
using Clericuzzi.Lib.Http.Communication.Response;
using Clericuzzi.Brdn.Macro.Entities.Business.Models;
using Clericuzzi.Lib.Http.Utils.Constants;
using Clericuzzi.Lib.Selenium.Extensions;
using Clericuzzi.Lib.Selenium.Util.Actions;

namespace Macros.Mining.Google
{
    public class MiningCoogleControllerImpl : MacroControllerImpl<MiningGoogle>
    {
        const string SEARCH_QUERY = "https://www.google.com/search?hl=pt-BR&q=__!__,+__@__,+__#__&tbm=lcl";

        const string SEND_PATH = "macro/mining-google-send";
        const string CLEAR_PATH = "macro/mining-google-clear";
        const string SOURCE_PATH = "macro/mining-google-source";
        const string CONSOLIDATE_PATH = "macro/mining-google-consolidate";

        List<CompanyItem> _Companies = null;
        public MiningCoogleControllerImpl()
        {
            LOOPS = true;
            RestartsIn = MacrosProgram.RestartsIn;
        }

        public override void RunSync()
        {
            HasError = true;
            try
            {
                HasError = false;
            }
            catch (Exception ex)
            {
                HasError = true;
                ex.LogDetails();
            }
        }
        public override async Task RunAsync()
        {
            HasError = true;
            _Companies = new List<CompanyItem>();
            if (Item != null)
            {
                try
                {
                    Console.WriteLine($"Nova avaliação, {Item}| Restarts in: {RestartsIn}");
                    var url = SEARCH_QUERY.Replace("__!__", Item.CompanySubtypeIdParent.Subtype.ClearSpecialChars(toLower: true).Trim()).Replace("__@__", Item.CityIdParent.Name.ClearSpecialChars(toLower: true).Trim()).Replace("__#__", Item.CityIdParent.StateIdParent.Name.ClearSpecialChars(toLower: true).Trim());
                    if (url.Contains(" "))
                    {
                        url = url.RemoveDoubleSpaces();
                        url = url.Replace(" ", "+");
                    }
                    Driver.Navigate(url);
                    await _LoopCompanies();

                    await Task.Delay(150);
                    HasError = false;
                }
                catch (Exception ex)
                {
                    WindowsUtils.EventLogWrite($"Falha de execução\r\n{ex.Message}\r\n\r\n{ex.StackTrace}");
                    HasError = true;
                    ex.LogDetails();
                    // throw ex;
                }
            }
            else
                throw new Exception("Item cannot be null");

            await CheckLoopRestart();
        }
        async Task _LoopCompanies()
        {
            await _GetCompaniesInfo();
            while (_HasNext())
            {
                _ClickNext();
                Driver.Timeout(4000);
                await _GetCompaniesInfo();
            }
        }
        async Task _SendCompanies()
        {
            try
            {
                if (_Companies.Count == 0)
                    return;

                var request = new RequestMessage(RequestMessageDefaults.GetUrl(SEND_PATH));
                request.AddParam("companies", _Companies);

                var response = await request.Post<StatusResponse>();
            }
            catch (Exception ex)
            {
                WindowsUtils.EventLogWrite($"Falha ao enviar dados\r\n{ex.Message}\r\n\r\n{ex.StackTrace}");
            }
            finally
            {
                _Companies = new List<CompanyItem>();
            }
        }
        List<IWebElement> _ListCompanies()
        {
            var noResultsPath = "/html/body/div[6]/div[3]/div[9]/div[1]/div[2]/div/div[2]/div[1]/div[2]/div/p[1]";
            var noResultsElement = Driver.GetByXPathOrId(noResultsPath);
            if (Driver.IsEnabledAndDisplayed(noResultsPath) && (noResultsElement?.Text.ClearSpecialChars(toLower: true).Contains("nao ha resultados locais que correspondem a sua pesquisa") ?? false))
                return new List<IWebElement>();

            var listXPath = "//*[@id=\"rl_ist0\"]/div[1]/div[4]";
            var companiesList = Driver.GetByXPathOrId(listXPath);
            var items = companiesList.FindElements(By.XPath("*"))?.ToList();

            Driver.TimeoutRandom();

            return items;
        }
        async Task _GetCompaniesInfo()
        {
            var companies = _ListCompanies();
            foreach (var company in companies)
            {
                _GetCompanyInfo(company);
                Driver.TimeoutRandom();
            }

            await _SendCompanies();
        }
        void _GetCompanyInfo(IWebElement company)
        {
            var panelCloseXPath = "//*[@id=\"rhs_block\"]/div/div[2]/div";

            var siteXPath = "//div/div/div/div/div/div[1]/div/div[1]/div[2]/div[1]/div/div[1]/div/div[2]/div[1]/a";
            try
            {
                if (_ShouldSkip(company))
                    return;

                Driver.ClickElement(company);
                Driver.WaitForFunction(750, () => _WaitPopup());

                var popup = _GetPopup();
                var siteElement = Driver.GetByXPathOrId(siteXPath);
                var site = siteElement?.GetAttribute("href");
                var infoSectionContent = Driver.GetText(popup);
                var lines = infoSectionContent?.SplitByString("\r\n");
                var ddd = _GetDdd(lines);
                var phone = _GetPhone(lines);
                var address = _GetAddress(lines);
                var description = _GetDescription(lines);
                var fantasyName = lines[0];

                _ValidateCompany(ddd, site, phone, address, description, fantasyName);
            }
            catch (Exception ex)
            {
            }
            finally
            {
                Driver.ClickByXPath(panelCloseXPath);
            }
        }
        void _ValidateCompany(string ddd, string site, string phone, string address, string description, string fantasyName)
        {
            if (!string.IsNullOrEmpty(ddd) && !string.IsNullOrEmpty(phone) && !string.IsNullOrEmpty(address) && !string.IsNullOrEmpty(fantasyName))
            {
                var fullNUmber = ddd + phone;
                if (fullNUmber.StartsWith("0800") || fullNUmber.StartsWith("0300"))
                    return;

                _Companies.Add(new CompanyItem(Item.CompanySubtypeId, ddd, site, phone, address, description, fantasyName));
            }
        }
        bool _ShouldSkip(IWebElement company)
        {
            var content = company?.Text?.ClearSpecialChars(toLower: true);
            if (content.Contains("nao consegue encontrar") || content.Contains("procurando algo diferente"))
                return true;
            else if (content.Contains("descobrir mais lugares"))
                return true;
            else
                return false;
        }
        string _GetDdd(List<string> lines)
        {
            var phoneLine = lines.FirstOrDefault(i => i.ToLowerInvariant().StartsWith("telefone"));
            if (!string.IsNullOrEmpty(phoneLine))
            {
                var phone = phoneLine.Substring(phoneLine.IndexOf(" ") + 1)?.DigitsOnly()?.Trim();
                if (!string.IsNullOrEmpty(phone) && phone.Length > 9)
                    return phone?.Substring(0, 2);
            }

            return null;
        }
        string _GetPhone(List<string> lines)
        {
            var phoneLine = lines.FirstOrDefault(i => i.ToLowerInvariant().StartsWith("telefone"));
            if (!string.IsNullOrEmpty(phoneLine))
            {
                var phone = phoneLine.Substring(phoneLine.IndexOf(" ") + 1)?.DigitsOnly()?.Trim();
                if (!string.IsNullOrEmpty(phone))
                {
                    if (phone.Length > 9)
                        return phone?.Substring(2);
                    else
                        return phone;
                }
            }

            return null;
        }
        string _GetAddress(List<string> lines)
        {
            var addressLine = lines.FirstOrDefault(i => i.ToLowerInvariant().StartsWith("endereço"));
            if (!string.IsNullOrEmpty(addressLine))
            {
                var address = addressLine.Substring(addressLine.IndexOf(" ") + 1);
                return address?.Trim();
            }

            return null;
        }
        string _GetDescription(List<string> lines)
        {
            for (int i = 0; i < lines.Count; i++)
            {
                var line = lines[i].ClearSpecialChars(toLower: true).Trim();
                if (line.ToLowerInvariant().StartsWith("endereco"))
                {
                    var city = Item.CityIdParent.Name.ClearSpecialChars(toLower: true).Trim();
                    var state = Item.CityIdParent.StateIdParent.Name.ClearSpecialChars(toLower: true).Trim();
                    var descriptionLine = lines[i - 1].ClearSpecialChars(toLower: true).Trim();

                    var indexCity = descriptionLine.Length;
                    if (descriptionLine.Contains(city) || descriptionLine.Contains(state))
                    {
                        if (descriptionLine.Contains(city))
                            indexCity = descriptionLine.IndexOf(city);
                        var indexState = descriptionLine.Length;
                        if (descriptionLine.Contains(state))
                            indexState = descriptionLine.IndexOf(state);

                        var description = descriptionLine.Substring(0, Math.Min(indexState, indexCity))?.Trim();
                        if (!string.IsNullOrEmpty(description))
                            description = description.Substring(0, description.LastIndexOf(" "));

                        return description;
                    }

                    return descriptionLine;
                }
            }

            return null;
        }
        IWebElement _GetPopup()
        {
            var sliderComponent = Driver.WebDriver.FindElement(By.TagName("async-local-kp"));

            return sliderComponent.FindElement(By.XPath("*"));
        }
        bool _WaitPopup()
        {
            var sliderComponent = Driver.WebDriver.FindElement(By.TagName("async-local-kp"));
            var displayed = Driver.IsEnabledAndDisplayed(sliderComponent);

            return displayed;
        }

        bool _HasNext()
        {
            try
            {
                var navigationBar = Driver.GetByXPathOrId("//*[@id=\"nav\"]/tbody/tr");
                var lastColumn = navigationBar.FindElements(By.XPath("*")).LastOrDefault();
                var lastColumnChild = lastColumn.FindElement(By.XPath("*"));

                return lastColumnChild.TagName.Equals(TagConstants.A);
            }
            catch (Exception)
            {
                return false;
            }
        }
        void _ClickNext()
        {
            var navigationBar = Driver.GetByXPathOrId("//*[@id=\"nav\"]/tbody/tr");
            var lastColumn = navigationBar.FindElements(By.XPath("*")).LastOrDefault();
            var lastColumnChild = lastColumn.FindElement(By.XPath("*"));

            Driver.ClickElement(lastColumnChild);
        }

        public override async Task Loop(bool brokenLoop = false)
        {
            RequestMessage request = null;
            SingleResponse<MiningGoogle> response = null;
            if (!HasError)
                request = new RequestMessage(RequestMessageDefaults.GetUrl(CONSOLIDATE_PATH));
            else
                request = new RequestMessage(RequestMessageDefaults.GetUrl(CLEAR_PATH));

            try
            {
                request.AddParam("mining-id", Item.Id);
                response = await request.Post<SingleResponse<MiningGoogle>>();
                if (response.HasErrors)
                {
                    Console.WriteLine(response.ErrorMessages.FirstOrDefault());
                    await ForceReset<MiningGoogle>(CLEAR_PATH, SOURCE_PATH);
                }
                else
                    Item = response.Entity;
            }
            catch (Exception ex)
            {
                WindowsUtils.EventLogWrite($"Falha ao buscar novo item pelo em execução\r\n{ex.Message}\r\n\r\n{ex.StackTrace}");
                await ForceReset<MiningGoogle>(CLEAR_PATH, SOURCE_PATH);
                HasError = true;
                ex.LogDetails();
                // throw ex;
            }

            if (!brokenLoop)
                await RunAsync();
        }

        public override async Task Consolidate()
        {
            await Task.Delay(150);
            return;

            //if (_LOOPS)
            //    return;

            //StatusResponse response = null;
            //if (!HasError)
            //{
            //    var request = new RequestMessage(RequestMessageDefaults.GetUrl(CONSOLIDATE_PATH));
            //    request.AddParam(FieldConstants.ENTITY, Item);

            //    response = await request.Post<StatusResponse>();
            //    if (response?.Success ?? false)
            //        Console.WriteLine("Consolidation successfull");
            //    else
            //        Console.WriteLine(response.ErrorMessages.FirstOrDefault());
            //}
            //else
            //    Console.WriteLine("Error running the macro");
        }
    }
}