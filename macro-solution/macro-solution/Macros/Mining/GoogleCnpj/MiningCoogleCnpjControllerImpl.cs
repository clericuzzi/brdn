using System;
using System.Linq;
using Brdn.MacrosImpl;
using OpenQA.Selenium;
using System.Globalization;
using System.Threading.Tasks;
using Clericuzzi.Lib.Utils.Core;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Clericuzzi.Brdn.Macro.Entities;
using Clericuzzi.Lib.Macros.Controller;
using Clericuzzi.Lib.Selenium.Extensions;
using Clericuzzi.Lib.Selenium.Util.Constants;
using Clericuzzi.Lib.Http.Business.Requesters;
using Clericuzzi.Lib.Http.Communication.Request;
using Clericuzzi.Lib.Http.Communication.Response;
using Clericuzzi.Brdn.Macro.Impl.Business.Models;

namespace Macros.Mining.GoogleCnpj
{
    public class MiningCoogleCnpjControllerImpl : MacroControllerImpl<Company>
    {
        public delegate bool X(int x, int y);

        const string CLEAR_PATH = "macro/cnpj-google-clear";
        const string SOURCE_PATH = "macro/cnpj-google-source";
        const string CONSOLIDATE_PATH = "macro/cnpj-google-consolidate";

        private List<CompanyInfo> _Companies;
        public MiningCoogleCnpjControllerImpl()
        {
            LOOPS = true;
            RestartsIn = MacrosProgram.RestartsIn;
        }

        public override void RunSync()
        {
            HasError = true;
            try
            {
                HasError = false;
            }
            catch (Exception ex)
            {
                HasError = true;
                ex.LogDetails();
            }
        }
        public override async Task RunAsync()
        {
            HasError = true;
            _Companies = new List<CompanyInfo>();
            if (Item != null)
            {
                try
                {
                    Console.WriteLine($"Nova avaliação, {Item}| Restarts in: {RestartsIn}");
                    await Task.Delay(150);

                    _ValidateUrl();
                    _ActualSearch();
                    _FindOccurrences();

                    HasError = false;
                }
                catch (Exception ex)
                {
                    HasError = true;
                    ex.LogDetails();
                    // throw ex;
                }
            }
            else
                throw new Exception("Item cannot be null");

            await CheckLoopRestart();
        }
        void _ValidateUrl()
        {
            Driver.Navigate("https://www.google.com");
        }
        void _ActualSearch()
        {
            Driver.SetText("/html/body/div/div[4]/form/div[2]/div[1]/div[1]/div/div[2]/input", $"{Item.FantasyName.ClearSpecialChars(toLower: true)} cnpj", 15);
            Driver.SendEnter("/html/body/div/div[4]/form/div[2]/div[1]/div[1]/div/div[2]/input");
        }
        void _FindOccurrences()
        {
            var pageContent = Driver.WebDriver.FindElements(By.TagName("html"));
            var contents = pageContent.Select(i => i.GetAttribute("innerHTML")).ToList();

            var matches = Regex.Matches(String.Join("\r\n", contents), RegexUtils.CNPJ);

            var cnpjs = new List<string>();
            foreach (var match in matches)
                cnpjs.Add(match.ToString().DigitsOnly());

            cnpjs = cnpjs.Where(i => i.IsCnpj()).Distinct().ToList();
            foreach (var cnpj in cnpjs)
            {
                var tries = 0;
                Driver.Navigate($"https://consultacnpj.com/cnpj/{cnpj}");
                while (tries++ < 5)
                {
                    Driver.WaitForFunction(750, () => _ValidateCnpjResult(), 5, false);
                    if (_ValidateCnpjResult())
                    {
                        _ParseInformation();
                        break;
                    }
                    Driver.TimeoutRandom(500, 1000);
                    Driver.Navigate($"https://consultacnpj.com/cnpj/{cnpj}");
                }
                Driver.TimeoutRandom(500, 1000);
            }
            _Companies = _Companies.Where(i => !string.IsNullOrWhiteSpace(i.Company.Phone)).ToList();
        }
        bool _ValidateCnpjResult()
        {
            return Driver.IsEnabledAndDisplayed("/html/body/div/div[2]/div/section[1]/div/div/div/div/div/div/div[1]/div[1]/div/a[1]");
        }

        void _ParseInformation()
        {
            var lines = new List<List<string>>();
            var tableElement = Driver.GetByXPathOrId("/html/body/div/div[2]/div/section[1]/div/div/div/div/div/div/div[2]/div/div[1]/table");
            var trs = tableElement.GetChildrenByTag(TagConstants.TR);
            foreach (var tr in trs)
            {
                var tds = tr.GetChildrenByTag(TagConstants.TD);
                if (tds.Count == 0)
                    tds = tr.GetChildrenByTag("th");
                lines.Add(tds.Select(i => i.Text).ToList());
            }
            var phones = _GetInformation(lines, "telefone");
            if (string.IsNullOrEmpty(phones))
                return;

            var companyInfo = new CompanyInfo();
            companyInfo.Company = new Company();
            companyInfo.CompanyLines = new List<CompanyLine>();
            companyInfo.CompanyAddress = new CompanyAddress();
            companyInfo.CompanyAddress.ZipcodeIdParent = new GeographyZipcode();

            companyInfo.CompanyAddress.ZipcodeIdParent.Zipcode = _GetInformation(lines, "cep")?.DigitsOnly();
            var city = _GetInformation(lines, "municipio");
            var state = _GetInformation(lines, "uf");
            companyInfo.CompanyAddress.Street = _GetInformation(lines, "logradouro");
            companyInfo.CompanyAddress.Number = _GetInformation(lines, "numero");
            companyInfo.CompanyAddress.Compliment = _GetInformation(lines, "complemento");
            var neighbourhood = _GetInformation(lines, "bairro");

            var openingDate = _GetInformation(lines, "data de abertura");
            var openingDateValue = CastUtils.CastTo<DateTime>(openingDate.Split(' ')[0]);

            var dateFormat = @"dd \d\e MMMM \d\e yyyy \à\s hh:mm:ss";
            var lastUpdate = _GetInformation(lines, "ultima atualizacao")?.Split('-')[0].Trim();
            var parseResult = DateTime.TryParseExact(lastUpdate, dateFormat, new CultureInfo("pt-BR"), DateTimeStyles.None, out DateTime value);

            _GetPhoneInfo(lines, companyInfo);
            if (parseResult)
                companyInfo.Company.DateLastUpdate = value;

            companyInfo.Company.Cnpj = _GetInformation(lines, "numero de inscricao")?.Substring(0, 20).DigitsOnly().Trim();
            companyInfo.Company.DateOpened = openingDateValue;
            companyInfo.Company.FormalName = _GetInformation(lines, "nome empresarial");
            companyInfo.Company.FantasyName = _GetInformation(lines, "nome fantasia");
            companyInfo.Company.JuridicNature = _GetInformation(lines, "codigo e descricao da natureza juridica");
            companyInfo.Company.PrimaryActivity = _GetPrimaryActivity(lines, "atividade economica primaria");
            companyInfo.Company.SituationIdParent = new CompanySituation() { Name = _GetInformation(lines, "situacao cadastral") };
            if (companyInfo.Company.SituationIdParent?.Name == "BAIXADA")
                return;
            else
                _Companies.Add(companyInfo);
        }
        void _GetPhoneInfo(List<List<string>> lines, CompanyInfo companyInfo)
        {
            var phones = _GetInformation(lines, "telefone");
            if (phones.Contains("/"))
            {
                var phoneList = phones.Split('/').ToList();
                for (int i = 0; i < phoneList.Count; i++)
                    phoneList[i] = phoneList[i].DigitsOnly().Trim();

                var companyPhone = phoneList.FirstOrDefault();
                if (!string.IsNullOrEmpty(companyPhone))
                    companyInfo.Company.Phone = companyPhone.RemoveStartingCharacter("0");

                phoneList.RemoveAt(0);
                foreach (var phone in phoneList)
                {
                    var ddd = phone.Substring(0, 2);
                    var number = phone.RemoveFromHead(2);
                    var firstDigit = number.Substring(0, 1);
                    var isMobile = firstDigit == "9" || firstDigit == "8" || firstDigit == "7";
                    companyInfo.CompanyLines.Add(new CompanyLine { Phone = number, Mobile = isMobile ? 1 : 0 });
                }
            }
            else
            {
                phones = phones.DigitsOnly();
                companyInfo.Company.Phone = phones.RemoveStartingCharacter("0");
            }
        }
        string _GetInformation(List<List<string>> input, string field)
        {
            var currentLine = 0;
            foreach (var inputLine in input)
            {
                var position = 0;
                for (position = 0; position < inputLine.Count; position++)
                {
                    if (inputLine[position].ClearSpecialChars(toLower: true).Trim().Equals(field))
                        return input[currentLine + 1][position];
                }
                currentLine++;
            }

            return null;
        }
        string _GetPrimaryActivity(List<List<string>> input, string field)
        {
            var returnValue = string.Empty;
            var currentLine = 0;
            foreach (var inputLine in input)
            {
                var position = 0;
                for (position = 0; position < inputLine.Count; position++)
                {
                    if (inputLine[position].ClearSpecialChars(toLower: true).Trim().Equals(field))
                        return String.Join(" ", input[currentLine + 2]);
                }
                currentLine++;
            }

            return null;
        }

        public override async Task Loop(bool brokenLoop = false)
        {
            RequestMessage request = null;
            SingleResponse<Company> response = null;
            if (!HasError)
                request = new RequestMessage(RequestMessageDefaults.GetUrl(CONSOLIDATE_PATH));
            else
                request = new RequestMessage(RequestMessageDefaults.GetUrl(CLEAR_PATH));

            try
            {
                request.AddParam("company-id", Item.Id);
                request.AddParam("companies", _Companies);
                response = await request.Post<SingleResponse<Company>>();
                if (response.HasErrors)
                {
                    Console.WriteLine(response.ErrorMessages.FirstOrDefault());
                    await ForceReset<Company>(CLEAR_PATH, SOURCE_PATH);
                }
                else
                    Item = response.Entity;
            }
            catch (Exception ex)
            {
                HasError = true;
                ex.LogDetails();
                throw ex;
            }

            if (!brokenLoop)
                await RunAsync();
        }

        public override async Task Consolidate()
        {
            await Task.Delay(150);
            return;

            //if (_LOOPS)
            //    return;

            //StatusResponse response = null;
            //if (!HasError)
            //{
            //    var request = new RequestMessage(RequestMessageDefaults.GetUrl(CONSOLIDATE_PATH));
            //    request.AddParam(FieldConstants.ENTITY, Item);

            //    response = await request.Post<StatusResponse>();
            //    if (response?.Success ?? false)
            //        Console.WriteLine("Consolidation successfull");
            //    else
            //        Console.WriteLine(response.ErrorMessages.FirstOrDefault());
            //}
            //else
            //    Console.WriteLine("Error running the macro");
        }
    }
}