using System;
using System.Linq;
using System.Threading.Tasks;
using Clericuzzi.Lib.Utils.Core;
using Clericuzzi.Brdn.Macro.Entities;
using Clericuzzi.Lib.Macros.Controller;
using Clericuzzi.Lib.Selenium.Extensions;
using Clericuzzi.Lib.Http.Business.Requesters;
using Clericuzzi.Lib.Http.Communication.Request;
using Clericuzzi.Lib.Http.Communication.Response;
using Clericuzzi.Brdn.Macro.Entities;

namespace Brdn.MacrosImpl.Macros.Catta.ByZipAndKeyword
{
    public class CattaAuth : MacroControllerAuth
    {
        /// <summary>
        /// Singleton implementation for CattaByZipAndKeywordControllerAuth
        /// </summary>
        public static CattaAuth Instance { get; } = new CattaAuth();
        // Explicit static constructor to tell C# compiler
        // not to mark type as beforefieldinit
        static CattaAuth()
        {
        }
        private CattaAuth()
        {
        }

        const string LOGIN_COMPONENT = "username";
        const string PASSWORD_COMPONENT = "password";
        public new MacroLogin AuthInformation { get; set; }

        public override async Task Authenticate()
        {
            var buttonXPath = "//*[@id=\"entrar\"]/button";
            await Task.Delay(150);

            Driver.ClickByXPath(buttonXPath);
        }
        public override bool AuthenticateScreenReady()
        {
            var loginInput = Driver.GetByXPathOrId(LOGIN_COMPONENT);

            return loginInput != null;
        }
        public override bool CheckFill()
        {
            var loginInput = Driver.GetByXPathOrId(LOGIN_COMPONENT);
            if (loginInput != null)
            {
                var loginText = loginInput.GetAttribute("value");
                if (!loginText.Equals(AuthInformation.Login))
                    return false;
            }

            var passwordInput = Driver.GetByXPathOrId(PASSWORD_COMPONENT);
            if (!passwordInput.IsTextEqualsTo(AuthInformation.Password))
                return false;

            return true;
        }
        public override void FillInfo()
        {
            var loginInput = Driver.GetByXPathOrId(LOGIN_COMPONENT);
            if (Driver.IsEnabledAndDisplayed(loginInput))
            {
                loginInput.Clear();
                loginInput.SendKeys(AuthInformation.Login);
            }

            var passwordInput = Driver.GetByXPathOrId(PASSWORD_COMPONENT);
            if (Driver.IsEnabledAndDisplayed(passwordInput))
            {
                passwordInput.Clear();
                passwordInput.SendKeys(AuthInformation.Password);
            }
        }
        public override bool Logged()
        {
            Driver.WaitForFunction(500, () => IsLogged() || IsLoginInvalid(), timeoutMessage: "Could not determine if the user was logged in or not...");

            return IsLogged();
        }
        public bool IsLogged()
        {
            var componentPath = "/html/body/div[1]/div/div[1]/div[1]/a";
            Driver.WaitForFunction(200, () => Driver.GetByXPath(componentPath)?.Enabled ?? false, 10, true, "Timedout waiting for post login click", "Error waiting for post login click");
            var component = Driver.GetByXPathOrId(componentPath);

            return component != null;
        }
        public bool IsLoginInvalid()
        {
            var alertBarXPath = "//*[@id=\"entrar-mensagem\"]";
            var loginButtonXPath = "//*[@id=\"entrar\"]/button";

            if (Driver.IsEnabledAndDisplayed(loginButtonXPath) || Driver.IsEnabledAndDisplayed(alertBarXPath))
                return true;
            else
                return false;
        }
        public string GetLoginError()
        {
            var alertBarXPath = "";
            var alertBarElement = Driver.GetByXPathOrId(alertBarXPath);
            var content = alertBarElement?.Text?.ClearSpecialChars(toLower: true);

            return content;
        }
        public override void NavigateTo()
        {
            Driver.GoToUrl(this.AuthInformation.Url);
        }
        public override async Task GetLoginInfo()
        {
            try
            {
                var requestMessage = new RequestMessage(RequestMessageDefaults.GetUrl("crud/macro_login/filter"));
                requestMessage.AddParam("filter", new { Macro = MacrosProgram.MacroLogin });
                var macroLogins = await requestMessage.Post<BatchResponse<MacroLogin>>();
                if (!macroLogins.HasErrors)
                    AuthInformation = macroLogins.Entities.FirstOrDefault();

                if (AuthInformation == null)
                    throw new Exception($"Sem logins disponíveis para a macro de nome '{MacrosProgram.MacroLogin}'");
            }
            catch (Exception ex)
            {
            }
        }
        public override async Task StoreLoginInformation()
        {
            var requestMessage = new RequestMessage(RequestMessageDefaults.GetUrl("crud/macro_login/log-error"));
            requestMessage.AddParam("login-id", AuthInformation.Id);
            requestMessage.AddParam("error-message", GetLoginError());

            var response = await requestMessage.Post<StatusResponse>();
        }
    }
}