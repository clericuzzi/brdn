using System;
using System.IO;
using System.Linq;
using Brdn.MacrosImpl;
using System.Threading.Tasks;
using Clericuzzi.Lib.Utils.Core;
using Brdn.MacrosImpl.Macros.Abr;
using System.Collections.Generic;
using Clericuzzi.Brdn.Macro.Entities;
using Brdn.MacrosImpl.Business.Models;
using Clericuzzi.Lib.Macros.Controller;
using Clericuzzi.Lib.Macros.Definitions;
using Clericuzzi.Lib.Selenium.Extensions;
using Clericuzzi.Lib.Selenium.Util.Constants;
using Clericuzzi.Lib.Http.Business.Requesters;
using Clericuzzi.Lib.Http.Communication.Request;
using Clericuzzi.Lib.Http.Communication.Response;
using Clericuzzi.Lib.Selenium.Business.CaptchaSolving;

namespace Macros.Abr.GetOperator
{
    public class AbrSohoControllerImpl : MacroControllerImpl<List<CompanyLine>>
    {
        const string CLEAR_PATH = "macro/abr-soho/clear";
        const string SOURCE_PATH = "macro/abr-soho/source";
        const string CONSOLIDATE_PATH = "macro/abr-soho/consolidate";

        bool _DeleteLines = false;
        string _SessionName = RandomStrings.RandomString(16);
        public List<CompanyLineInfo> _LinesInfo = new List<CompanyLineInfo>();
        public List<CompanyLineInfo> _DeletedLinesInfo = new List<CompanyLineInfo>();
        public string FilePath { get { return Path.Combine(SessionFolder, "upload.txt"); } }
        public string SessionFolder { get { return Path.Combine(PathDefinitions.TempFolder, _SessionName); } }
        public AbrSohoControllerImpl()
        {
            LOOPS = true;
            RestartsIn = MacrosProgram.RestartsIn;
            HasIndivitualDriver = true;
        }

        public override void RunSync()
        {
            HasError = true;
            try
            {
                HasError = false;
            }
            catch (Exception ex)
            {
                HasError = true;
                ex.LogDetails();
            }
        }
        public override async Task RunAsync()
        {
            HasError = true;
            _LinesInfo = new List<CompanyLineInfo>();
            _DeletedLinesInfo = new List<CompanyLineInfo>();
            _DeleteLines = false;
            var urlValidated = false;
            if (Item != null)
            {
                if (Item.Count == 0)
                    throw new Exception("Não há números a testar no momento...");

                try
                {
                    Console.WriteLine($"Nova avaliação, {Item}| Restarts in: {RestartsIn}");
                    while (!urlValidated)
                        urlValidated = _ValidateUrl();
                    await _SolveCaptcha();

                    HasError = false;
                }
                catch (Exception ex)
                {
                    HasError = true;
                    ex.LogDetails();
                    // throw ex;
                }
            }
            else
                throw new Exception("Item cannot be null");

            await CheckLoopRestart();
        }

        void _ValidateFile()
        {
            var numbers = new List<string>();
            Item.ForEach(i => numbers.Add(i.StringValue));
            numbers = numbers.Distinct().ToList();
            File.WriteAllLines(FilePath, numbers);

            _FillNumbers();
        }
        void _FillNumbers()
        {
            var inputElement = Driver.GetByXPathOrId("//*[@id=\"arquivo\"]");
            inputElement.SendKeys(FilePath);
        }
        bool _ValidateUrl()
        {
            try
            {
                Driver.Navigate("https://consultanumero.abrtelecom.com.br/consultanumero/consulta/consultaHistoricoRecenteCtg");
                Driver.WaitForFunction(750, () => Driver.IsEnabledAndDisplayed("//*[@id=\"arquivo\"]"));

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        async Task _SolveCaptcha()
        {
            SessionFolder.CreateIfNotExists();
            SessionFolder.DeleteAllFiles();
            _ValidateFile();

            var buttonXPath = "//*[@id=\"idSubmit\"]";

            var bodyDiv = Driver.GetByXPathOrId("body");
            var initialDiv = bodyDiv.FindElement(OpenQA.Selenium.By.XPath("*"));

            var captchaFrame = Driver.GetByXPathOrId("//*[@id=\"jcap\"]");
            if (Driver.IsEnabledAndDisplayed(captchaFrame))
            {
                var script = $"arguments[0].setAttribute('style', 'display: none;');";
                Driver.RunJavascriptWithArgs(script, Driver.GetByXPathOrId("//*[@id=\"jquery-captcha\"]/p"));
                Driver.RunJavascriptWithArgs(script, Driver.GetByXPathOrId("//*[@id=\"body\"]/div/span[1]"));
                Driver.RunJavascriptWithArgs(script, Driver.GetByXPathOrId("//*[@id=\"body\"]/div/span[2]"));
                Driver.RunJavascriptWithArgs(script, Driver.GetByXPathOrId("//*[@id=\"body\"]/div/div"));
                Driver.RunJavascriptWithArgs(script, Driver.GetByXPathOrId("//*[@id=\"body\"]/div/h3"));
                Driver.RunJavascriptWithArgs(script, Driver.GetByXPathOrId("//*[@id=\"header\"]"));
                Driver.RunJavascriptWithArgs(script, Driver.GetByXPathOrId("//*[@id=\"wrapper\"]/div[2]"));
                Driver.RunJavascriptWithArgs(script, Driver.GetByXPathOrId("//*[@id=\"footer\"]"));

                Driver.RunJavascriptWithArgs(script, Driver.GetByXPathOrId("//*[@id=\"resultado\"]"));
                Driver.RunJavascriptWithArgs(script, Driver.GetByXPathOrId("//*[@id=\"form\"]/table/tbody/tr[1]"));
                Driver.RunJavascriptWithArgs(script, Driver.GetByXPathOrId("//*[@id=\"form\"]/table/tbody/tr[2]"));
                Driver.RunJavascriptWithArgs(script, Driver.GetByXPathOrId("//*[@id=\"form\"]/table/tbody/tr[3]"));
                Driver.RunJavascriptWithArgs(script, Driver.GetByXPathOrId("//*[@id=\"form\"]/table/tbody/tr[4]"));
                Driver.RunJavascriptWithArgs(script, Driver.GetByXPathOrId("//*[@id=\"form\"]/table/tbody/tr[5]"));
                Driver.RunJavascriptWithArgs(script, Driver.GetByXPathOrId("//*[@id=\"form\"]/table/tbody/tr[6]"));
                Driver.RunJavascriptWithArgs(script, Driver.GetByXPathOrId("//*[@id=\"form\"]/table/tbody/tr[8]"));
                Driver.RunJavascriptWithArgs(script, Driver.GetByXPathOrId("//*[@id=\"form\"]/table/tbody/tr[9]"));
                Driver.RunJavascriptWithArgs(script, Driver.GetByXPathOrId("//*[@id=\"form\"]/table/tbody/tr[10]"));
                Driver.RunJavascriptWithArgs(script, Driver.GetByXPathOrId("//*[@id=\"form\"]/table/tbody/tr[11]"));
                Driver.RunJavascriptWithArgs(script, Driver.GetByXPathOrId("//*[@id=\"form\"]/table/tbody/tr[7]/td[1]"));
                Driver.RunJavascriptWithArgs(script, Driver.GetByXPathOrId("//*[@id=\"form\"]/table/tbody/tr[7]/td[2]"));
                Driver.RunJavascriptWithArgs(script, Driver.GetByXPathOrId("//*[@id=\"form\"]/table/tbody/tr[7]/td[3]/input[2]"));

                Driver.ClickElement(captchaFrame);
                Driver.ScrollToElement(captchaFrame);

                Driver.Timeout(750);
                var screenshot = captchaFrame.ScreenShot(SessionFolder, Driver);
                var captchaElementBounds = captchaFrame.FindBounds();

                var captchaBounds = AbrUtils.GetCaptchaBounds(screenshot);
                var croppedFile = ElementExtensions.CropScreenShot(SessionFolder, screenshot, captchaBounds.Value);
                var fileBase64 = FileUtils.AsBase64(croppedFile);

                script = $"arguments[0].setAttribute('style', 'display: block;');";

                Driver.RunJavascriptWithArgs(script, Driver.GetByXPathOrId("//*[@id=\"resultado\"]"));
                Driver.RunJavascriptWithArgs(script, Driver.GetByXPathOrId("//*[@id=\"form\"]/table/tbody/tr[1]"));
                Driver.RunJavascriptWithArgs(script, Driver.GetByXPathOrId("//*[@id=\"form\"]/table/tbody/tr[2]"));
                Driver.RunJavascriptWithArgs(script, Driver.GetByXPathOrId("//*[@id=\"form\"]/table/tbody/tr[3]"));
                Driver.RunJavascriptWithArgs(script, Driver.GetByXPathOrId("//*[@id=\"form\"]/table/tbody/tr[4]"));
                Driver.RunJavascriptWithArgs(script, Driver.GetByXPathOrId("//*[@id=\"form\"]/table/tbody/tr[5]"));
                Driver.RunJavascriptWithArgs(script, Driver.GetByXPathOrId("//*[@id=\"form\"]/table/tbody/tr[6]"));
                Driver.RunJavascriptWithArgs(script, Driver.GetByXPathOrId("//*[@id=\"form\"]/table/tbody/tr[8]"));
                Driver.RunJavascriptWithArgs(script, Driver.GetByXPathOrId("//*[@id=\"form\"]/table/tbody/tr[9]"));
                Driver.RunJavascriptWithArgs(script, Driver.GetByXPathOrId("//*[@id=\"form\"]/table/tbody/tr[10]"));
                Driver.RunJavascriptWithArgs(script, Driver.GetByXPathOrId("//*[@id=\"form\"]/table/tbody/tr[11]"));
                Driver.RunJavascriptWithArgs(script, Driver.GetByXPathOrId("//*[@id=\"form\"]/table/tbody/tr[7]/td[1]"));
                Driver.RunJavascriptWithArgs(script, Driver.GetByXPathOrId("//*[@id=\"form\"]/table/tbody/tr[7]/td[2]"));
                Driver.RunJavascriptWithArgs(script, Driver.GetByXPathOrId("//*[@id=\"form\"]/table/tbody/tr[7]/td[3]/input[2]"));

                await CaptchaSolver.SolveImageCaptcha(fileBase64);
                Driver.SetText("//*[@id=\"form\"]/table/tbody/tr[7]/td[3]/input[2]", CaptchaSolver.Token.ToLowerInvariant(), 25);

                await Task.Delay(150);

                Driver.ClickByXPath(buttonXPath);
                Driver.WaitForFunction(5000, () => _SolveCaptchaResult(), 60);
            }
        }

        bool _SolveCaptchaResult()
        {
            var result = _SolveCaptchaResultAlert() || _SolveCaptchaResultSuccess();

            return result;
        }
        bool _SolveCaptchaResultAlert()
        {
            if (Driver.HasAlerts())
            {
                var message = Driver.GetAlertMessage()?.ClearSpecialChars(toLower: true).Trim();
                if (message.Contains("nao existem dados retornados para a consulta"))
                {
                    _DeleteLines = true;
                    _DeletedLinesInfo = new List<CompanyLineInfo>();
                    Item.ForEach(i => _DeletedLinesInfo.Add(new CompanyLineInfo { Id = i.Id }));

                    return true;
                }
                Driver.CloseAlerts();
                throw new Exception("Captcha problem...");
            }

            return false;
        }
        bool _SolveCaptchaResultSuccess()
        {
            try
            {
                var tableElement = Driver.GetByXPathOrId("resultado");
                var rows = tableElement?.GetChildrenByTag(TagConstants.TR).Skip(1).ToList();

                if (rows?.Count > 0)
                {
                    _LinesInfo = new List<CompanyLineInfo>();
                    rows.ForEach(i => _LinesInfo.Add(new CompanyLineInfo(i)));
                    for (int i = 0; i < _LinesInfo.Count; i++)
                    {
                        if (string.IsNullOrEmpty(_LinesInfo[i].Number))
                            _LinesInfo[i].Number = _LinesInfo[i - 1].Number;

                        _LinesInfo[i].Id = Item.Where(j => j.StringValue.RemoveStartingCharacter("0").Contains(_LinesInfo[i].Number.RemoveStartingCharacter("0"))).Select(j => j.Id).FirstOrDefault();
                    }
                }

                return _LinesInfo.Count > 0;
            }
            catch (Exception ex)
            {
                _LinesInfo = new List<CompanyLineInfo>();
                return false;
            }
        }

        public override async Task Loop(bool brokenLoop = false)
        {
            RequestMessage request = null;
            BatchResponse<CompanyLine> response = null;
            if (!HasError)
                request = new RequestMessage(RequestMessageDefaults.GetUrl(CONSOLIDATE_PATH));
            else
                request = new RequestMessage(RequestMessageDefaults.GetUrl(CLEAR_PATH));

            try
            {
                request.AddParam("line-info", _DeleteLines ? _DeletedLinesInfo : _LinesInfo);
                request.AddParam("delete-lines", _DeleteLines);
                response = await request.Post<BatchResponse<CompanyLine>>(timeoutLimit: 270000);
                $"sending request for {Item}, with {((response?.HasErrors ?? true) ? "failure" : "success")}, session: {_SessionName}".Log();
                if (response.HasErrors)
                {
                    Console.WriteLine(response.ErrorMessages.FirstOrDefault());
                    await ForceReset<string>(CLEAR_PATH, SOURCE_PATH);
                }
                else
                    Item = response.Entities;
            }
            catch (Exception ex)
            {
                await ForceReset<string>(CLEAR_PATH, SOURCE_PATH);
                HasError = true;
                ex.LogDetails();
                // throw ex;
            }

            if (!brokenLoop)
                await RunAsync();
        }

        public override async Task Consolidate()
        {
            await Task.Delay(150);
            return;

            //if (_LOOPS)
            //    return;

            //StatusResponse response = null;
            //if (!HasError)
            //{
            //    var request = new RequestMessage(RequestMessageDefaults.GetUrl(CONSOLIDATE_PATH));
            //    request.AddParam(FieldConstants.ENTITY, Item);

            //    response = await request.Post<StatusResponse>();
            //    if (response?.Success ?? false)
            //        Console.WriteLine("Consolidation successfull");
            //    else
            //        Console.WriteLine(response.ErrorMessages.FirstOrDefault());
            //}
            //else
            //    Console.WriteLine("Error running the macro");
        }
    }
}