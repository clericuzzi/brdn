using System;
using System.Threading.Tasks;
using Clericuzzi.Lib.Utils.Core;
using System.Collections.Generic;
using Clericuzzi.Brdn.Macro.Entities;
using Clericuzzi.Lib.Macros.Controller;
using Clericuzzi.Lib.Macros.Definitions;
using Clericuzzi.Lib.Http.Utils.Constants;
using Clericuzzi.Lib.Http.Business.Requesters;
using Clericuzzi.Lib.Http.Communication.Request;
using Clericuzzi.Lib.Http.Communication.Response;

namespace Macros.Abr.GetOperator
{
    public class AbrSohoController : MacroController<List<CompanyLine>, List<CompanyLine>>
    {
        const string SOURCE_PATH = "macro/abr-soho/source";
        readonly string _SessionName = RandomStrings.RandomString(12);

        public AbrSohoController(bool headless) : base(null, new AbrSohoControllerImpl(), null, headless)
        {
            //RunOnce = true;
            IsAsync = true;
            DebugError = false;
            CloseAfterRun = true;
            HasIndivitualDriver = true;
        }

        public override async Task GetSource()
        {
            Restart();
            ParentEntities = null;
            var requestMessage = new RequestMessage("http://brdn.dyndns.org:81/functions/routine/consult_abr.php");
            try
            {
                requestMessage.AddValue(FieldConstants.MACHINE_NAME, _SessionName, compressed: false, overwrite: true);
                var client = await requestMessage.Post<BatchResponse<CompanyLine>>();
                if (client.HasErrors)
                    throw new Exception(client.ErrorMessages[0]);

                ParentEntities = new List<List<CompanyLine>> { client?.Entities };
            }
            catch (Exception ex)
            {
                Console.WriteLine("Datasource FAILED");
            }
        }
        public override void CreateConsolidation(List<CompanyLine> item)
        {
            ConsolidationItem = item;
        }

        public override void HandleGeneralException(Exception ex)
        {
            ConsolidationItem = null;
        }
        public override void HandleMacroException(MacroException ex)
        {
            ConsolidationItem = null;
        }
    }
}