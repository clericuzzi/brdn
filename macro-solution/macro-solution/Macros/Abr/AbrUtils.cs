﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Brdn.MacrosImpl.Macros.Abr
{
    public static class AbrUtils
    {
        public static Rectangle? GetCaptchaBounds(string captchaImage)
        {
            var image = Image.FromFile(captchaImage);
            var bitmap = new Bitmap(image);
            try
            {
                int xEnd = 0, xStart = 0;
                int yEnd = 0, yStart = 0;

                var white = Color.White.ToArgb();
                for (int i = 0; i < bitmap.Width; i++)
                {
                    for (int j = 0; j < bitmap.Height; j++)
                    {
                        var pixelColor = bitmap.GetPixel(i, j);
                        var namedColor = pixelColor.IsNamedColor;
                        var knownColor = pixelColor.ToKnownColor();

                        var closeToWhite = AreColorsClose(pixelColor.ToArgb(), white, 32);
                        if (!closeToWhite)
                        {
                            if (xStart == 0)
                                xStart = i;

                            if (yStart == 0)
                                yStart = j;
                        }
                        else
                        {
                            if (yEnd > 0 && xStart > 0 && xEnd == 0 && i > xStart && j > yStart && j < yEnd)
                                xEnd = i;

                            if (yStart > 0 && yEnd == 0 && j > yStart)
                                yEnd = j;
                        }
                    }
                }

                return new Rectangle(xStart, yStart, xEnd - xStart, yEnd - yStart);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (image != null)
                    image.Dispose();
                if (bitmap != null)
                    bitmap.Dispose();
            }
        }

        public static bool AreColorsClose(int color, int baseColor, int threshold)
        {
            return AreColorsClose(Color.FromArgb(color), Color.FromArgb(baseColor), threshold);
        }
        public static bool AreColorsClose(Color color, Color baseColor, int threshold)
        {
            var r = (int)color.R - (int)baseColor.R;
            var g = (int)color.G - (int)baseColor.G;
            var b = (int)color.B - (int)baseColor.B;

            var squaredValues = ((r * r) + (g * g) + (b * b));
            var squaredThershold = (threshold * threshold);

            return squaredValues <= squaredThershold;
        }
    }
}
