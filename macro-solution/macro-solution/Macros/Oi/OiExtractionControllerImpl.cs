using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Clericuzzi.Lib.Utils.Core;
using Clericuzzi.Lib.Macros.Controller;
using Clericuzzi.Lib.Macros.Definitions;
using Clericuzzi.Lib.Http.Utils.Constants;
using Clericuzzi.Lib.Selenium.Util.Constants;
using Clericuzzi.Lib.Http.Business.Requesters;
using Clericuzzi.Lib.Http.Communication.Request;
using Clericuzzi.Lib.Http.Communication.Response;
using Brdn.MacrosImpl;
using OpenQA.Selenium;
using Clericuzzi.Lib.Selenium.Extensions;
using OpenQA.Selenium.Support.UI;
using Clericuzzi.Lib.Selenium.Business.CaptchaSolving;
using Brdn.MacrosImpl.Business.Managers;

namespace Macros.Oi.ExtractionOi
{
    public class OiExtractionControllerImpl : MacroControllerImpl<string>
    {
        const string CLEAR_PATH = "prefix/clear";
        const string SOURCE_PATH = "prefix/source";
        const string CONSOLIDATE_PATH = "prefix/consolidate";

        public OiExtractionControllerImpl()
        {
            LOOPS = true;
            RestartsIn = MacrosProgram.RestartsIn;
        }

        public override void RunSync()
        {
            HasError = true;
            try
            {
                HasError = false;
            }
            catch (Exception ex)
            {
                HasError = true;
                ex.LogDetails();
            }
        }
        public override async Task RunAsync()
        {
            _DefineRun();
            HasError = true;
            if (Item != null)
            {
                try
                {
                    Console.WriteLine($"Nova avaliação, {Item}| Restarts in: {RestartsIn}");
                    await Task.Delay(150);
                    HasError = false;

                    Console.WriteLine("Iniciando rodagem...");
                    Driver.Timeout(1500);
                    var boxPath = "//*[@id=\"cliente_oi\"]";
                    Driver.WaitForFunction(500, () => Driver.IsEnabledAndDisplayed(boxPath), 15, false);

                    Console.WriteLine("boxpath avaliado...");
                    Driver.Timeout(1500);
                    if (!Driver.IsEnabledAndDisplayed(boxPath))
                    {
                        Console.WriteLine("Indo para a página...");
                        Driver.Timeout(1500);
                        _GoToPage();
                        _SelectBoxes();
                    }

                    _FindTarget();
                    await _BreakCaptcha();
                    _DownloadFile();
                    await _SendFile();
                }
                catch (Exception ex)
                {
                    HasError = true;
                    ex.LogDetails();
                    // throw ex;
                }
            }
            else
                throw new Exception("Item cannot be null");

            await CheckLoopRestart();
        }

        async Task _SendFile()
        {
            var req = new RequestMessage(ServerUrls.GetUrl("extraction/oi"));
        }
        void _GoToPage()
        {
            Console.WriteLine("going to page...");
            Driver.Timeout(1500);
            var menu1 = "//*[@id=\"menu\"]/ul/li[7]/a/span";
            var menu2 = "//*[@id=\"menu\"]/ul/li[7]/ul/li/a/span";
            var menu3 = "//*[@id=\"menu\"]/ul/li[7]/ul/li/ul/li/a/span";

            Driver.WaitForFunction(500, () => Driver.IsEnabledAndDisplayed(menu1));
            Driver.ClickByXPath(menu1);

            Driver.WaitForFunction(500, () => Driver.IsEnabledAndDisplayed(menu2));
            Driver.ClickByXPath(menu2);

            Driver.WaitForFunction(500, () => Driver.IsEnabledAndDisplayed(menu3));
            Driver.ClickByXPath(menu3);

            Console.WriteLine("page loaded...");
            Driver.Timeout(1500);
        }
        void _DefineRun()
        {
            if (!Driver.WebDriver.Url.Equals("https://portalempresarial.oi.net.br/"))
                Driver.Redirect("https://portalempresarial.oi.net.br/");
        }
        void _FindTarget()
        {
            Console.WriteLine("going for target...");
            Driver.Timeout(1500);
            OIExtractionManager.GetNextUf();

            var comboUf = "//*[@id=\"id_uf\"]";
            var comboCity = "//*[@id=\"cidade\"]";

            Driver.WaitForFunction(500, () => Driver.IsEnabledAndDisplayed(comboUf));
            var comboUfElement = Driver.GetByXPathOrId(comboUf);
            var comboUfSelect = new SelectElement(comboUfElement);
            comboUfSelect.SelectByText(OIExtractionManager.CurrentUf);

            Driver.WaitForFunction(500, () => Driver.IsEnabledAndDisplayed(comboCity));
            var comboCityElement = Driver.GetByXPathOrId(comboCity);
            var comboCitySelect = new SelectElement(comboCityElement);
            _SelectCity(comboCitySelect);
        }
        void _SelectBoxes()
        {
            Console.WriteLine("selecting boxes...");
            Driver.Timeout(1500);
            var boxPath = "//*[@id=\"cliente_oi\"]";
            var radioPath = "//*[@id=\"posseProduto\"]";

            Driver.WaitForFunction(500, () => Driver.IsEnabledAndDisplayed(boxPath));
            Driver.ClickByXPath(boxPath);

            Driver.WaitForFunction(500, () => Driver.IsEnabledAndDisplayed(radioPath));
            Driver.ClickByXPath(radioPath);

            Console.WriteLine("boxes ticked...");
            Driver.Timeout(1500);
        }
        void _DownloadFile()
        {
            var downloadLinkPath = "//*[@id=\"BaixarExcel\"]/font/b";
            var downloadExcelPath = "//*[@id=\"BaixarExcelDownload\"]";

            Driver.WaitForFunction(500, () => Driver.IsEnabledAndDisplayed(downloadLinkPath));
            Driver.ClickByXPath(downloadLinkPath);

            Driver.WaitForFunction(500, () => Driver.IsEnabledAndDisplayed(downloadExcelPath));
            Driver.ClickByXPath(downloadExcelPath);
        }
        async Task _BreakCaptcha()
        {
            var filesFolder = Path.Combine(ConsoleUtils.LocationEntry, "files");

            var captchaImagePath = "/html/body/section/div/div/div[2]/form/div[2]/div[2]/div[1]/div[1]";

            Driver.WaitForFunction(500, () => Driver.IsEnabledAndDisplayed(captchaImagePath));
            var captchaImageElement = Driver.GetByXPathOrId(captchaImagePath);
            Driver.ScrollToElement(captchaImageElement);

            var screenshot = captchaImageElement.ScreenShot(filesFolder, Driver);
            var captchaElementBounds = captchaImageElement.FindBounds();
            var croppedFile = ElementExtensions.CropScreenShot(filesFolder, screenshot, captchaElementBounds);
            var fileBase64 = FileUtils.AsBase64(croppedFile);

            await CaptchaSolver.SolveImageCaptcha(fileBase64);
        }

        void _SelectCity(SelectElement comboCitySelect)
        {
            var availableCities = comboCitySelect.Options.Select(i => i.Text).ToList();
            Console.WriteLine($"availableCities: ", String.Join("-", availableCities));
            comboCitySelect.SelectByText(OIExtractionManager.CurrentCity);
        }

        public override async Task Loop(bool brokenLoop = false)
        {
            RequestMessage request = null;
            SingleResponse<string> response = null;
            if (!HasError)
            {
                request = new RequestMessage(RequestMessageDefaults.GetUrl(CONSOLIDATE_PATH));
                request.AddParam(FieldConstants.ENTITY, Item);
            }
            else
                request = new RequestMessage(RequestMessageDefaults.GetUrl(CLEAR_PATH));

            try
            {
                response = await request.Post<SingleResponse<string>>();
                if (response.HasErrors)
                {
                    Console.WriteLine(response.ErrorMessages.FirstOrDefault());
                    await ForceReset<string>(CLEAR_PATH, SOURCE_PATH);
                }
                else
                    Item = response.Entity;
            }
            catch (Exception ex)
            {
                await ForceReset<string>(CLEAR_PATH, SOURCE_PATH);
                HasError = true;
                ex.LogDetails();
                // throw ex;
            }

            if (!brokenLoop)
                await RunAsync();
        }

        public override async Task Consolidate()
        {
            await Task.Delay(150);
            return;

            //if (_LOOPS)
            //    return;

            //StatusResponse response = null;
            //if (!HasError)
            //{
            //    var request = new RequestMessage(RequestMessageDefaults.GetUrl(CONSOLIDATE_PATH));
            //    request.AddParam(FieldConstants.ENTITY, Item);

            //    response = await request.Post<StatusResponse>();
            //    if (response?.Success ?? false)
            //        Console.WriteLine("Consolidation successfull");
            //    else
            //        Console.WriteLine(response.ErrorMessages.FirstOrDefault());
            //}
            //else
            //    Console.WriteLine("Error running the macro");
        }
    }
}