using System;
using System.Drawing;
using System.Windows.Forms;
using System.Threading.Tasks;
using OpenQA.Selenium.Chrome;
using System.Collections.Generic;
using Clericuzzi.Lib.Macros.Controller;
using Clericuzzi.Lib.Macros.Definitions;

namespace Macros.Oi.ExtractionOi
{
    public class OiExtractionController : MacroController<string, string>
    {
        public OiExtractionController() : base(null, new OiExtractionControllerImpl(), null)
        {
            //RunOnce = true;
            IsAsync = true;
            DebugError = false;
            CloseAfterRun = true;

            var monitor = Screen.FromPoint(new Point(Screen.PrimaryScreen.Bounds.Right + 1, Screen.PrimaryScreen.Bounds.Top));
            DriverController.CustomArguments = new List<string>() { String.Format("--window-position={0},{1}", monitor.Bounds.X, monitor.Bounds.Y), "--start-maximized" };
        }

        public override async Task GetSource()
        {
            var chromeDriver = (ChromeDriver)Driver.WebDriver;
            var executorField = typeof(OpenQA.Selenium.Remote.RemoteWebDriver)
        .GetField("executor",
                  System.Reflection.BindingFlags.NonPublic
                  | System.Reflection.BindingFlags.Instance);

            object executor = executorField.GetValue(chromeDriver);

            var internalExecutorField = executor.GetType()
                .GetField("internalExecutor",
                          System.Reflection.BindingFlags.NonPublic
                          | System.Reflection.BindingFlags.Instance);
            object internalExecutor = internalExecutorField.GetValue(executor);

            //executor.CommandInfoRepository
            var remoteServerUriField = internalExecutor.GetType()
                .GetField("remoteServerUri",
                          System.Reflection.BindingFlags.NonPublic
                          | System.Reflection.BindingFlags.Instance);
            var remoteServerUri = remoteServerUriField.GetValue(internalExecutor) as Uri;

            Driver.WebDriver.Close();

            Restart();
            ParentEntities = null;
            try
            {
                await Task.Delay(150);
                ParentEntities = new List<string> { "" };
            }
            catch (Exception ex)
            {
                Console.WriteLine("Datasource FAILED");
                Console.WriteLine(ex.Message);
            }
        }
        public override void CreateConsolidation(string item)
        {
            ConsolidationItem = item;
        }

        public override void HandleGeneralException(Exception ex)
        {
            ConsolidationItem = null;
        }
        public override void HandleMacroException(MacroException ex)
        {
            ConsolidationItem = null;
        }
    }
}