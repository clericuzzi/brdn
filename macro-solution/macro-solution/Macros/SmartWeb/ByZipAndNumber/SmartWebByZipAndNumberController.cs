using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Clericuzzi.Brdn.Macro.Entities;
using Brdn.MacrosImpl.Macros.SmartWeb;
using Clericuzzi.Lib.Macros.Controller;
using Clericuzzi.Lib.Macros.Definitions;
using Clericuzzi.Lib.Http.Business.Requesters;
using Clericuzzi.Lib.Http.Communication.Request;
using Clericuzzi.Lib.Http.Communication.Response;

namespace Macros.SmartWeb.ByCnpj
{
    public class SmartWebByZipAndNumberController : MacroController<CompanyAddress, CompanyAddress>
    {
        const string SOURCE_PATH = "macro/by-zip-and-number";

        public SmartWebByZipAndNumberController(bool headless) : base(SmartWebAuth.Instance, new SmartWebByZipAndNumberControllerImpl(), null, headless)
        {
            //RunOnce = true;
            IsAsync = true;
            DebugError = false;
            CloseAfterRun = true;
            HasIndivitualDriver = true;
        }

        public override async Task GetSource()
        {
            Restart();
            ParentEntities = null;
            var requestMessage = new RequestMessage(RequestMessageDefaults.GetUrl(SOURCE_PATH));
            try
            {
                var client = await requestMessage.Post<SingleResponse<CompanyAddress>>();
                if (client.HasErrors || client?.Entity == null)
                    throw new Exception(client.ErrorMessages[0]);

                SmartWebUtils.ClickCobertura(Driver);
                ParentEntities = new List<CompanyAddress> { client?.Entity };
            }
            catch (Exception ex)
            {
                Console.WriteLine("Datasource FAILED");
                Console.WriteLine(ex.Message);
            }
        }
        public override void CreateConsolidation(CompanyAddress item)
        {
            ConsolidationItem = item;
        }

        public override void HandleGeneralException(Exception ex)
        {
            ConsolidationItem = null;
        }
        public override void HandleMacroException(MacroException ex)
        {
            ConsolidationItem = null;
        }
    }
}