using System;
using System.Linq;
using Brdn.MacrosImpl;
using System.Threading.Tasks;
using Clericuzzi.Lib.Utils.Core;
using Clericuzzi.Brdn.Macro.Entities;
using Brdn.MacrosImpl.Macros.SmartWeb;
using Clericuzzi.Lib.Macros.Controller;
using Clericuzzi.Lib.Selenium.Extensions;
using Clericuzzi.Lib.Selenium.Business.Core;
using Clericuzzi.Lib.Selenium.Util.Constants;
using Clericuzzi.Lib.Selenium.Business.Enums;
using Clericuzzi.Lib.Http.Business.Requesters;
using Clericuzzi.Lib.Http.Communication.Request;
using Clericuzzi.Lib.Http.Communication.Response;
using OpenQA.Selenium;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Macros.SmartWeb.ByCnpj
{
    public class SmartWebByZipAndNumberControllerImpl : MacroControllerImpl<CompanyAddress>
    {
        const string CLEAR_PATH = "macro/by-zip-and-number-clear";
        const string SOURCE_PATH = "macro/by-zip-and-number";
        const string CONSOLIDATE_PATH = "macro/by-zip-and-number-consolidate";

        bool _Skip = false;
        bool _ShouldDelete = false;
        string _UF = null;
        string _City = null;
        string _Zip = null;
        string _Number = null;
        string _Neighbourhood = null;
        List<string> _Numbers = null;
        CompanyAvailability _Availability;
        public SmartWebByZipAndNumberControllerImpl()
        {
            LOOPS = true;
            RestartsIn = MacrosProgram.RestartsIn;
        }

        public override void RunSync()
        {
            _Skip = false;
            HasError = true;
            try
            {
                HasError = false;
            }
            catch (Exception ex)
            {
                HasError = true;
                ex.LogDetails();
            }
        }
        public override async Task RunAsync()
        {
            HasError = true;
            _Skip = false;
            _ShouldDelete = false;

            _Zip = null;
            _Number = null;
            _Numbers = new List<string>();
            _UF = _City = _Neighbourhood = null;
            _Availability = new CompanyAvailability() { CompanyAddressId = Item.Id };
            if (Item != null)
            {
                try
                {
                    Console.WriteLine($"Nova avaliação, {Item}| Restarts in: {RestartsIn}");
                    await Task.Delay(250);

                    _ValidateNumber();
                    _ValidateZipcode();
                    foreach (var number in _Numbers)
                    {
                        _Search(number);
                        if (!string.IsNullOrEmpty(_Availability.Message))
                            break;
                    }

                    HasError = false;
                }
                catch (Exception ex)
                {
                    Driver.Refresh();
                    SmartWebUtils.ClickCobertura(Driver);
                    HasError = true;
                    ex.LogDetails();
                    // throw ex;
                }
            }
            else
                throw new Exception("Item cannot be null");

            await CheckLoopRestart();
        }
        void _ValidateNumber()
        {
            _Numbers = new List<string>();
            if (string.IsNullOrEmpty(Item.Number))
                _GetCurrentNumber(Item.Street);
            else
                _Numbers.Add(Item.Number);
        }
        void _GetCurrentNumber(string street)
        {
            if (street.Contains("GVT:"))
            {
                street = street.ToLowerInvariant().RemoveDoubleSpaces();
                var remainingAddress = street.Substring(street.IndexOf("), ") + 3);
                remainingAddress = remainingAddress.Substring(0, remainingAddress.IndexOf(" "));

                if (Regex.IsMatch(street, @"\s*s/n\s*") || Regex.IsMatch(street, @"\s*sn\s*"))
                    _Numbers = new List<string>() { "SN" };
                else
                    _Numbers = new List<string>() { remainingAddress.DigitsOnly() };
            }
            else
            {
                var cepRegex = new Regex(@"\d+-\d+");
                var cep = cepRegex.Match(street)?.ToString().DigitsOnly();

                var regex = new Regex(@"([\s,][\d]+)");
                var matches = regex.Matches(street);
                foreach (var match in matches)
                {
                    var item = match?.ToString()?.Trim();
                    if (!string.IsNullOrEmpty(item))
                    {
                        if (!string.IsNullOrEmpty(cep))
                        {
                            if (!cep.Contains(item))
                                _Numbers.Add(match.ToString().Trim());
                        }
                        else
                            _Numbers.Add(match.ToString().Trim());
                    }
                }
            }

            if (_Numbers.Count == 0)
                _Numbers.Add("SN");
        }
        void _ValidateZipcode()
        {
            if (Item.ZipcodeIdParent == null)
                _Zip = _GetCurrentZipcode(Item.Street);
            else
                _Zip = Item.ZipcodeIdParent.Zipcode;
        }
        string _GetCurrentZipcode(string street)
        {
            street = street?.RemoveDoubleSpaces();
            var matches = Regex.Match(street, RegexUtils.CEP);
            if (matches.Success)
            {
                var zip = matches.Value.ToString().DigitsOnly();
                return zip;
            }
            else
                throw new Exception("Endereço sem cep identificável...");
        }
        void _Search(string number)
        {
            var zipPath = "/html/body/div[1]/div[1]/div[7]/div/div[6]/div/div[1]/div/div[2]/div[1]/div/div/div/form/div/span/div[3]/div[2]/div[2]/input";
            var tablePath = "//*[@id=\"s_3_l\"]";
            var numberPath = "/html/body/div[1]/div[1]/div[7]/div/div[6]/div/div[1]/div/div[2]/div[1]/div/div/div/form/div/span/div[3]/div[3]/div[2]/input";
            var searchButtonPath = "//*[@id=\"s_4_1_28_0_Ctrl\"]";

            Driver.WaitForFunction(750, () => Driver.IsEnabledAndDisplayed(zipPath), 30);

            var zipElement = Driver.GetByXPathOrId(zipPath);
            var numberElement = Driver.GetByXPathOrId(numberPath);

            if (string.IsNullOrEmpty(_Zip))
                _Zip = Item.ZipcodeIdParent.Zipcode.DigitsOnly().Trim();

            Driver.SetText(zipElement, _Zip, 25);
            Driver.SetText(numberElement, number, 25);
            Driver.ClickByXPath(searchButtonPath);

            _SearchResult(tablePath, number);

            _UF = Driver.GetText("//*[@id=\"s_S_A4_div\"]/form/div/span/div[3]/div[6]/div[2]/input");
            _City = Driver.GetText("//*[@id=\"s_S_A4_div\"]/form/div/span/div[3]/div[5]/div[2]/input");
        }
        void _SearchResult(string tablePath, string number)
        {
            _Number = number;
            _Availability = new CompanyAvailability() { CompanyAddressId = Item.Id };
            _Skip = false;
            Driver.Timeout(1500);
            _WaitForResult();

            if (_Skip || _ShouldDelete)
                return;

            var resultPath = "/html/body/div[1]/div[1]/div[7]/div/div[6]/div/div[1]/div/div[2]/div[2]/div/div/div/form/div/span/div[3]/div[2]/div[3]/textarea";
            var tableElement = Driver.GetByXPathOrId(tablePath);
            var rows = tableElement.GetChildrenByTag(TagConstants.TR).Skip(1).ToList();
            if (rows?.Count >= 1)
            {
                var row = rows.FirstOrDefault();
                //var firstColumn = rows[1].GetChildrenByTag(TagConstants.TD).FirstOrDefault();
                Driver.ClickElement(row);
                var searchPath = "//*[@id=\"s_3_1_63_0_Ctrl\"]";

                _GetZipInfo(row);

                Driver.WaitForFunction(250, () => Driver.IsEnabledAndDisplayed(searchPath), 30);
                Driver.ClickByXPath(searchPath);

                var tries = 0;
                var result = Driver.GetText(resultPath);
                while (string.IsNullOrEmpty(result))
                {
                    Driver.Timeout(2500);
                    result = Driver.GetText(resultPath);

                    if (++tries > 5)
                        throw new Exception("Maximum result timeout reached");
                }
                _Availability.Parse(result);
            }
        }
        void _GetZipInfo(IWebElement row)
        {
            var tds = row.GetChildrenByTag(TagConstants.TD).ToList();
            _Neighbourhood = tds[7]?.Text;
        }

        void _WaitForResult()
        {
            Driver.WaitForFunction(500, () => _WaitForResultAlert() || _WaitForResultRows(), 30);
        }
        bool _WaitForResultRows()
        {
            var tablePath = "//*[@id=\"s_3_l\"]";
            var tableElement = Driver.GetByXPathOrId(tablePath);
            var rows = tableElement.GetChildrenByTag(TagConstants.TR);
            var hasRows = rows?.Count > 1;
            if (!hasRows)
                _Skip = true;

            return hasRows || _Skip;
        }
        bool _WaitForResultAlert()
        {
            var hasAlerts = Driver.HasAlerts();
            if (hasAlerts)
            {
                var message = Driver.GetAlertMessage()?.ClearSpecialChars(toLower: true);

                _Skip = true;
                _ShouldDelete = message.Contains("nao encontrado");
                Driver.CloseAlerts();
            }

            return Driver.HasAlerts() || _Skip;
        }

        public override async Task Loop(bool brokenLoop = false)
        {
            RequestMessageDefaults.RootEndpoint = "http://localhost:58111/api/";
            RequestMessage request = null;
            SingleResponse<CompanyAddress> response = null;
            if (HasError)
                _Skip = true;
            request = new RequestMessage(RequestMessageDefaults.GetUrl(CONSOLIDATE_PATH));

            try
            {
                request.AddParam("skip", _Skip);
                request.AddParam("uf", _UF);
                request.AddParam("zip", _Zip);
                request.AddParam("city", _City);
                request.AddParam("number", _Number);
                request.AddParam("should-delete", _ShouldDelete);
                request.AddParam("neighbourhood", _Neighbourhood);
                request.AddParam("availability", _Availability);
                request.AddParam("company-address-id", Item.Id);
                response = await request.Post<SingleResponse<CompanyAddress>>(timeoutLimit: 30000);
                if (response.HasErrors)
                {
                    Console.WriteLine(response.ErrorMessages.FirstOrDefault());
                    await ForceReset<CompanyAddress>(CLEAR_PATH, SOURCE_PATH);
                }
                else
                    Item = response.Entity;
            }
            catch (Exception ex)
            {
                Driver.Refresh();
                SmartWebUtils.ClickCobertura(Driver);
                HasError = true;
                ex.LogDetails();
                // throw ex;
            }

            if (!brokenLoop)
                await RunAsync();
        }

        public override async Task Consolidate()
        {
            await Task.Delay(150);
            return;

            //if (_LOOPS)
            //    return;

            //StatusResponse response = null;
            //if (!HasError)
            //{
            //    var request = new RequestMessage(RequestMessageDefaults.GetUrl(CONSOLIDATE_PATH));
            //    request.AddParam(FieldConstants.ENTITY, Item);

            //    response = await request.Post<StatusResponse>();
            //    if (response?.Success ?? false)
            //        Console.WriteLine("Consolidation successfull");
            //    else
            //        Console.WriteLine(response.ErrorMessages.FirstOrDefault());
            //}
            //else
            //    Console.WriteLine("Error running the macro");
        }
    }
}