using System;
using System.Linq;
using Brdn.MacrosImpl;
using OpenQA.Selenium;
using System.Threading.Tasks;
using Clericuzzi.Lib.Utils.Core;
using System.Collections.Generic;
using Clericuzzi.Brdn.Macro.Entities;
using Brdn.MacrosImpl.Macros.SmartWeb;
using Clericuzzi.Lib.Macros.Controller;
using Clericuzzi.Lib.Selenium.Extensions;
using Clericuzzi.Lib.Selenium.Util.Constants;
using Clericuzzi.Lib.Http.Business.Requesters;
using Clericuzzi.Lib.Http.Communication.Request;
using Clericuzzi.Lib.Http.Communication.Response;

namespace Macros.SmartWeb.ByCnpj
{
    public class CompanyContactInfo
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public List<string> Phones { get; set; }
    }

    public class SmartWebByCnpjControllerImpl : MacroControllerImpl<Company>
    {
        const string CLEAR_PATH = "macro/smart-by-cnpj-clear";
        const string SOURCE_PATH = "macro/smart-by-cnpj-source";
        const string CONSOLIDATE_PATH = "macro/smart-by-cnpj-consolidate";

        public List<CompanyContactInfo> _Contacts { get; set; }
        public List<CompanyActiveService> _ActiveServices { get; set; }
        public SmartWebByCnpjControllerImpl()
        {
            LOOPS = true;
            RestartsIn = MacrosProgram.RestartsIn;
        }

        public override void RunSync()
        {
            HasError = true;
            try
            {
                HasError = false;
            }
            catch (Exception ex)
            {
                HasError = true;
                ex.LogDetails();
            }
        }
        public override async Task RunAsync()
        {
            _Contacts = new List<CompanyContactInfo>();
            _ActiveServices = new List<CompanyActiveService>();
            HasError = true;
            if (Item != null)
            {
                try
                {
                    Console.WriteLine($"Nova avaliação, {Item}| Restarts in: {RestartsIn}");
                    await Task.Delay(150);

                    _RunAction();

                    HasError = false;
                }
                catch (Exception ex)
                {
                    HasError = true;
                    ex.LogDetails();
                    // throw ex;
                    if (Driver.IsEnabledAndDisplayed("/html/body/div[1]/div[1]/div[8]/div/div[6]/div/div[1]/div/div[1]/div[1]/div/div/form/div/span/div[1]/div[6]/div[1]/span"))
                        _Justify();
                }
            }
            else
                throw new Exception("Item cannot be null");

            await CheckLoopRestart();
        }
        void _RunAction()
        {
            var divElementPath = "/html/body/div[1]/div[1]/div[7]/div/div[6]/div/div[1]/div/div[1]/div/form/div/span/table/tbody/tr/td[2]/div/div[2]/div[2]/input";
            var justifyDropdown = "/html/body/div[1]/div[1]/div[8]/div/div[6]/div/div[1]/div/div[1]/div[1]/div/div/form/div/span/div[1]/div[6]/div[1]/span";
            Driver.WaitForFunction(250, () => Driver.IsEnabledAndDisplayed(divElementPath) || Driver.IsEnabledAndDisplayed(justifyDropdown), 2, false);

            if (!Driver.IsEnabledAndDisplayed(divElementPath) && !Driver.IsEnabledAndDisplayed(justifyDropdown))
                SmartWebUtils.ClickVenda(Driver);

            Driver.Timeout(1000);
            if (Driver.IsEnabledAndDisplayed(justifyDropdown))
            {
                _Justify();
                SmartWebUtils.ClickVenda(Driver);
            }

            _Search();
            if (_GetActiveServices())
                _GetToContacts();
        }

        void _Search()
        {
            var documentElementPath = "/html/body/div[1]/div[1]/div[7]/div/div[6]/div/div[1]/div/div[1]/div/form/div/span/table/tbody/tr/td[2]/div/div[2]/div[2]/input";
            var documentElement = Driver.GetByXPathOrId(documentElementPath);

            Driver.WaitForFunction(750, () => Driver.GetByXPathOrId(documentElementPath) != null);

            Driver.SetText(documentElement, Item.Cnpj, 25);
            Driver.SendEnter(documentElement);
        }
        void _GetToContacts()
        {
            var buttonReantabilizarPath = "/html/body/div[1]/div[1]/div[7]/div/div[6]/div/div[1]/div/div[1]/div/form/div/span/table/tbody/tr/td[5]/div/div[2]/button/span";
            Driver.WaitForFunction(500, () => Driver.IsEnabledAndDisplayed(buttonReantabilizarPath));
            Driver.ClickByXPath(buttonReantabilizarPath);

            var contactsElementPath = "/html/body/div[1]/div[1]/div[8]/div/div[6]/div/div[1]/div/div[1]/div[2]/div/div/div/form/div/span/div/div[7]/div[2]/span";
            Driver.WaitForFunction(750, () => Driver.IsEnabledAndDisplayed(contactsElementPath));
            var contactsElement = Driver.GetByXPathOrId(contactsElementPath);
            Driver.ClickElement(contactsElement);
            Driver.Timeout(1250);

            var tableBodyPath = "//*[@id=\"s_4_l\"]/tbody";
            Driver.WaitForFunction(750, () => Driver.IsEnabledAndDisplayed(tableBodyPath));

            _GetContactsInfo(tableBodyPath);
            Driver.ClickByXPath("/html/body/div[8]/div[2]/div/div/div/form/div/div[2]/span/button/span");
            Driver.TimeoutRandom();

            _Justify();
        }

        private void _Justify()
        {
            Driver.ClickByXPath("/html/body/div[1]/div[1]/div[8]/div/div[6]/div/div[1]/div/div[1]/div[1]/div/div/form/div/span/div[1]/div[6]/div[1]/span");
            Driver.WaitForFunction(750, () => Driver.IsEnabledAndDisplayed("/html/body/div[1]/div[1]/div[8]/div/div[6]/ul[4]/li[7]"));
            Driver.ClickByXPath("/html/body/div[1]/div[1]/div[8]/div/div[6]/ul[4]/li[7]");
            Driver.TimeoutRandom();
            Driver.ClickByXPath("/html/body/div[1]/div[1]/div[8]/div/div[6]/div/div[1]/div/div[1]/div[1]/div/div/form/div/span/div[1]/div[6]/div[2]/button/span");
        }

        bool _GetActiveServices()
        {
            Driver.WaitForFunction(250, () => Driver.HasAlerts(), 2, false);
            if (Driver.HasAlerts())
            {
                var message = Driver.GetAlertMessage()?.ClearSpecialChars(toLower: true);
                if (message.Contains("cliente nao encontrado"))
                    return false;
            }

            var addedRows = new List<string>();
            var nextItemPath = "/html/body/div[1]/div[1]/div[7]/div/div[6]/div/div[1]/div/div[3]/div[2]/div/form/span/div/div[3]/div/div/div[5]/div/table/tbody/tr/td[2]/table/tbody/tr/td[3]/span";
            var tableContent = "";
            var tablePath = "/html/body/div[1]/div[1]/div[7]/div/div[6]/div/div[1]/div/div[3]/div[2]/div/form/span/div/div[3]/div/div/div[3]/div[3]/div/table";
            Driver.WaitForFunction(500, () => Driver.IsEnabledAndDisplayed(tablePath), 5, false);

            var tableBody = Driver.GetByXPathOrId("/html/body/div[1]/div[1]/div[7]/div/div[6]/div/div[1]/div/div[3]/div[2]/div/form/span/div/div[3]/div/div/div[3]/div[3]/div/table/tbody");
            var rows = tableBody.GetChildrenByTag(TagConstants.TR);
            if (rows.Count == 0)
                return false;

            var rowsData = new List<string>();
            var targetClass = "ui-icon ui-icon-triangle-1-e tree-plus treeclick";
            var tableElement = Driver.GetByXPathOrId(tablePath);
            var breakLoop = 8;
            while (true)
            {
                tableContent = _GetActiveServicesItem(ref addedRows, nextItemPath, targetClass, tableElement, ref breakLoop);

                if (string.IsNullOrEmpty(tableContent))
                    break;
                else
                {
                    if (tableContent.Equals(tableElement?.Text))
                        breakLoop--;
                    if (breakLoop <= 0)
                        break;
                }
            }

            return true;
        }

        string _GetActiveServicesItem(ref List<string> addedRows, string nextItemPath, string targetClass, IWebElement tableElement, ref int breakLoop)
        {
            try
            {
                string tableContent = tableElement.Text;
                var rows = tableElement.GetChildrenByTag(TagConstants.TR);
                foreach (var row in rows)
                {
                    var content = row.Text;
                    var target = row.GetChildrenByTag(TagConstants.DIV).Where(i => i.GetAttribute("class").Equals(targetClass)).FirstOrDefault();
                    if (!string.IsNullOrEmpty(content) && !addedRows.Contains(content))
                    {
                        breakLoop = 8;
                        var tds = row.GetChildrenByTag(TagConstants.TD);
                        _ActiveServices.Add(new CompanyActiveService(tds.Select(i => i.Text).ToList()));
                        addedRows.Add(content);
                        addedRows = addedRows.Distinct().ToList();
                        if (Driver.IsEnabledAndDisplayed(target))
                        {
                            Driver.ClickElement(target);
                            Driver.TimeoutRandom();
                        }

                        Driver.ClickByXPath(nextItemPath);
                        break;
                    }
                }

                return tableContent;
            }
            catch (Exception)
            {
                return null;
            }
        }

        void _GetContactsInfo(string tableBodyPath)
        {
            var tableBody = Driver.GetByXPathOrId(tableBodyPath);
            var lines = tableBody.GetChildrenByTag(TagConstants.TR).Skip(1).ToList();
            foreach (var line in lines)
                _GetContactInfo(line);
        }
        void _GetContactInfo(IWebElement line)
        {
            var contactInfo = new CompanyContactInfo();
            contactInfo.Phones = new List<string>();

            var tds = line.GetChildrenByTag(TagConstants.TD).ToList();
            var inputs = line.GetChildrenByTag(TagConstants.INPUT).ToList();

            Driver.ClickElement(tds[0]);
            Driver.TimeoutRandom();

            var notToCallPhones = new List<string>();

            int forbiddenColumn = 4;
            var inner = tds[forbiddenColumn].GetInnerHtml();
            if (!string.IsNullOrEmpty(inner) && !inner.Equals("&nbsp;"))
            {
                notToCallPhones.Add(tds[forbiddenColumn - 1].Text?.DigitsOnly());
                tds.Remove(tds[forbiddenColumn - 1]);
                forbiddenColumn += 1;
            }
            else
                forbiddenColumn += 2;

            inner = tds[forbiddenColumn].GetInnerHtml();
            if (!string.IsNullOrEmpty(inner) && !inner.Equals("&nbsp;"))
            {
                tds.Remove(tds[forbiddenColumn - 1]);
                forbiddenColumn += 1;
            }
            else
                forbiddenColumn += 2;

            inner = tds[forbiddenColumn].GetInnerHtml();
            if (!string.IsNullOrEmpty(inner) && !inner.Equals("&nbsp;"))
                tds.Remove(tds[forbiddenColumn]);

            contactInfo.Name = tds[2].Text;
            if (string.IsNullOrEmpty(contactInfo.Name))
                contactInfo.Name = inputs[1].GetAttribute("value");

            contactInfo.Email = tds.Where(i => i.Text.Contains("@")).Select(i => i.Text).FirstOrDefault();
            contactInfo.Phones = tds.Where(i => i.Text.Contains("(")).Select(i => i.Text.DigitsOnly()).ToList();
            contactInfo.Phones = contactInfo.Phones.Where(i => !notToCallPhones.Contains(i)).ToList();

            _Contacts.Add(contactInfo);
        }

        public override async Task Loop(bool brokenLoop = false)
        {
            RequestMessage request = null;
            SingleResponse<Company> response = null;
            if (!HasError)
                request = new RequestMessage(RequestMessageDefaults.GetUrl(CONSOLIDATE_PATH));
            else
                request = new RequestMessage(RequestMessageDefaults.GetUrl(CLEAR_PATH));

            try
            {
                request.AddParam("items", _Contacts);
                request.AddParam("services", _ActiveServices);
                request.AddParam("company-id", Item.Id);
                response = await request.Post<SingleResponse<Company>>();
                if (response.HasErrors)
                {
                    Console.WriteLine(response.ErrorMessages.FirstOrDefault());
                    await ForceReset<string>(CLEAR_PATH, SOURCE_PATH);
                }
                else
                    Item = response.Entity;
            }
            catch (Exception ex)
            {
                await ForceReset<string>(CLEAR_PATH, SOURCE_PATH);
                HasError = true;
                ex.LogDetails();
                // throw ex;
            }

            if (!brokenLoop)
                await RunAsync();
        }

        public override async Task Consolidate()
        {
            await Task.Delay(150);
            return;

            //if (_LOOPS)
            //    return;

            //StatusResponse response = null;
            //if (!HasError)
            //{
            //    var request = new RequestMessage(RequestMessageDefaults.GetUrl(CONSOLIDATE_PATH));
            //    request.AddParam(FieldConstants.ENTITY, Item);

            //    response = await request.Post<StatusResponse>();
            //    if (response?.Success ?? false)
            //        Console.WriteLine("Consolidation successfull");
            //    else
            //        Console.WriteLine(response.ErrorMessages.FirstOrDefault());
            //}
            //else
            //    Console.WriteLine("Error running the macro");
        }
    }
}