using System;
using System.Linq;
using System.Drawing;
using Brdn.MacrosImpl;
using System.Threading.Tasks;
using Clericuzzi.Lib.Utils.Core;
using Clericuzzi.Brdn.Macro.Entities;
using Clericuzzi.Lib.Macros.Controller;
using Clericuzzi.Lib.Selenium.Extensions;
using Clericuzzi.Lib.MouseControl.Business;
using Clericuzzi.Lib.Http.Business.Requesters;
using Clericuzzi.Lib.Http.Communication.Request;
using Clericuzzi.Lib.Http.Communication.Response;

namespace Macros.SmartWeb.ByCnpj
{
    public class SmartWebAuth : MacroControllerAuth
    {
        /// <summary>
        /// Singleton implementation for WebVendasNewByCnpjControllerAuth
        /// </summary>
        public static SmartWebAuth Instance { get; } = new SmartWebAuth();
        // Explicit static constructor to tell C# compiler
        // not to mark type as beforefieldinit
        static SmartWebAuth()
        {
        }
        private SmartWebAuth()
        {
        }

        const string LOGIN_COMPONENT = "//*[@id=\"username\"]";
        const string PASSWORD_COMPONENT = "//*[@id=\"password\"]";
        public new MacroLogin AuthInformation { get; set; }

        public override async Task Authenticate()
        {
            var buttonXPath = "//*[@id=\"form\"]/div[4]/div/input";
            await Task.Delay(150);

            Driver.ClickByXPath(buttonXPath);
        }
        public override bool AuthenticateScreenReady()
        {
            Driver.WaitForFunction(750, () => Driver.GetByXPathOrId(LOGIN_COMPONENT) != null, 3, false);
            var loginInput = Driver.GetByXPathOrId(LOGIN_COMPONENT);

            return loginInput != null;
        }
        public override bool CheckFill()
        {
            var loginInput = Driver.GetByXPathOrId(LOGIN_COMPONENT);
            if (loginInput != null)
            {
                var loginText = loginInput.GetAttribute("value");
                if (!loginText.Equals(AuthInformation.Login))
                    return false;
            }

            var passwordInput = Driver.GetByXPathOrId(PASSWORD_COMPONENT);
            if (!passwordInput.IsTextEqualsTo(AuthInformation.Password))
                return false;

            return true;
        }
        public override void FillInfo()
        {
            var loginInput = Driver.GetByXPathOrId(LOGIN_COMPONENT);
            if (Driver.IsEnabledAndDisplayed(loginInput))
            {
                loginInput.Clear();
                loginInput.SendKeys(AuthInformation.Login);
            }

            var passwordInput = Driver.GetByXPathOrId(PASSWORD_COMPONENT);
            if (Driver.IsEnabledAndDisplayed(passwordInput))
            {
                passwordInput.Clear();
                passwordInput.SendKeys(AuthInformation.Password);
            }

            _SliderCaptcha();
        }

        private Point _LastClick;
        private int _TargetValue;
        private int _CurrentValue;
        void _SliderCaptcha()
        {
            _SliderCaptchaGetTargetValue();

            _TryHitTarget();
            Driver.Timeout(550);
            _SliderCaptchaGetCurrentValue();
            var currentValueMatchesTarget = _TargetValue == _CurrentValue;
            while (!currentValueMatchesTarget)
            {
                _HitCorrection();
                Driver.Timeout(550);
                _SliderCaptchaGetCurrentValue();
                currentValueMatchesTarget = _TargetValue == _CurrentValue;
            }
        }

        void _Click()
        {
            Driver.Focus();
            MouseController.MouseMove(_LastClick.X, _LastClick.Y);
        }
        void _TryHitTarget()
        {
            var sliderBodyPath = "//*[@id=\"form\"]/div[3]/div/div";
            var sliderBodyElement = Driver.GetByXPath(sliderBodyPath);

            var clickPosition = (int)((int)Math.Floor(((double)_TargetValue / 100) * 175) + sliderBodyElement.Location.X);
            _LastClick = new Point(clickPosition, sliderBodyElement.Location.Y + 120);
            _Click();
        }
        void _HitCorrection()
        {
            if (_TargetValue > _CurrentValue)
                _LastClick = new Point(_LastClick.X + 1, _LastClick.Y);
            else
                _LastClick = new Point(_LastClick.X - 1, _LastClick.Y);

            _Click();
        }

        void _SliderCaptchaGetTargetValue()
        {
            var targetValuePath = "//*[@id=\"form\"]/span/center[2]";
            var targetValueElement = Driver.GetByXPathOrId(targetValuePath);
            var targetValueElementValue = targetValueElement.GetInnerHtml();
            int.TryParse(targetValueElementValue, out int targetValue);
            _TargetValue = targetValue;
        }
        void _SliderCaptchaGetCurrentValue()
        {
            var currentValuePath = "//*[@id=\"range-1a\"]";
            var currentValueElement = Driver.GetByXPath(currentValuePath);
            var innerContent = currentValueElement.GetInnerHtml();
            innerContent = currentValueElement.GetAttribute("value");

            int.TryParse(innerContent, out int currentValue);

            _CurrentValue = currentValue;
        }

        public override bool Logged()
        {
            Driver.WaitForFunction(500, () => IsLogged() || IsLoginInvalid(), 60, timeoutMessage: "Could not determine if the user was logged in or not...");

            return IsLogged();
        }
        public bool IsLogged()
        {
            var componentPath = "s_sctrl_tabScreen";
            Driver.WaitForFunction(200, () => Driver.GetByXPath(componentPath)?.Enabled ?? false, 4, false, "Timedout waiting for post login click", "Error waiting for post login click");
            var component = Driver.GetByXPathOrId(componentPath);

            return component != null;
        }
        public bool IsLoginInvalid()
        {
            var alertBarXPath = "/html/body/div/h1";
            if (Driver.IsEnabledAndDisplayed(alertBarXPath))
            {
                if (Driver.GetByXPathOrId(alertBarXPath).GetInnerHtml().ClearSpecialChars(toLower: true).Contains("ops tente novamente"))
                    return true;
                else
                    return false;
            }
            else
                return false;
        }
        public string GetLoginError()
        {
            var alertBarXPath = "";
            var alertBarElement = Driver.GetByXPathOrId(alertBarXPath);
            var content = alertBarElement?.Text?.ClearSpecialChars(toLower: true);

            return content;
        }
        public override void NavigateTo()
        {
            Driver.GoToUrl(this.AuthInformation.Url);
        }
        public override async Task GetLoginInfo()
        {
            try
            {
                AuthInformation = MacrosProgram.GetLoginInfo(3);
                if (AuthInformation == null)
                    AuthInformation = MacrosProgram.GetLoginInfo(8);
                if (AuthInformation == null)
                {
                    var requestMessage = new RequestMessage(RequestMessageDefaults.GetUrl("crud/macro_login/filter"));
                    requestMessage.AddParam("filter", new { Macro = MacrosProgram.MacroLogin });
                    var macroLogins = await requestMessage.Post<BatchResponse<MacroLogin>>();
                    if (!macroLogins.HasErrors)
                        AuthInformation = macroLogins.Entities.FirstOrDefault();

                    if (AuthInformation == null)
                        throw new Exception($"Sem logins disponíveis para a macro de nome '{MacrosProgram.MacroLogin}'");
                }
            }
            catch (Exception ex)
            {
            }
        }
        public override async Task StoreLoginInformation()
        {
            var requestMessage = new RequestMessage(RequestMessageDefaults.GetUrl("crud/macro_login/log-error"));
            requestMessage.AddParam("login-id", AuthInformation.Id);
            requestMessage.AddParam("error-message", GetLoginError());

            var response = await requestMessage.Post<StatusResponse>();
        }
    }
}