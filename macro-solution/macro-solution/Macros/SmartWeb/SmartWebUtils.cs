﻿using System.Linq;
using OpenQA.Selenium;
using Macros.SmartWeb.ByCnpj;
using Clericuzzi.Lib.Utils.Core;
using Clericuzzi.Lib.Selenium.Extensions;
using Clericuzzi.Lib.Selenium.Business.Core;
using Clericuzzi.Lib.Selenium.Util.Constants;

namespace Brdn.MacrosImpl.Macros.SmartWeb
{
    public static class SmartWebUtils
    {
        public static void ClickCobertura(SeleniumDriver driver)
        {
            driver.Refresh();
            driver.Timeout(2500);
            var divElementPath = "_swescrnbar";
            driver.WaitForFunction(750, () => driver.IsEnabledAndDisplayed(divElementPath));

            var divElement = driver.GetByXPathOrId(divElementPath);
            driver.WaitForFunction(750, () => _GetButton(divElement, "validar cobertura") != null);
            IWebElement linkElement = _GetButton(divElement, "validar cobertura");
            if (driver.IsEnabledAndDisplayed(linkElement))
                driver.ClickElement(linkElement);

            _ChackLastOrder(driver);
        }
        public static void ClickVenda(SeleniumDriver driver)
        {
            driver.Refresh();
            driver.Timeout(2500);
            var divElementPath = "_swescrnbar";
            driver.WaitForFunction(750, () => driver.IsEnabledAndDisplayed(divElementPath));

            var divElement = driver.GetByXPathOrId(divElementPath);
            driver.WaitForFunction(750, () => _GetButton(divElement, "venda") != null);
            IWebElement linkElement = _GetButton(divElement, "venda");
            if (driver.IsEnabledAndDisplayed(linkElement))
                driver.ClickElement(linkElement);

            _ChackLastOrder(driver);
        }
        static void _ChackLastOrder(SeleniumDriver driver)
        {
            driver.WaitForFunction(250, () => driver.HasAlerts(), 2, false);
            if (driver.HasAlerts())
            {
                var message = driver.GetAlertMessage()?.ClearSpecialChars(toLower: true);
                if (message.Contains("atencao justificativa da nao venda nao informada"))
                    _CancelLastOrder(driver);
            }
        }
        static void _CancelLastOrder(SeleniumDriver driver)
        {
            var retakePath = "/html/body/div[1]/div[1]/div[7]/div/div[6]/div/div[1]/div/div[1]/div/form/span/div/div[1]/div[2]/button[3]/span";
            var lastPedidoPath = "/html/body/div[1]/div/div[7]/div/div[6]/div/div[1]/div/div[1]/div/div/form/div/div/div/div[3]/div[3]/div/table/tbody/tr[2]/td[3]/a";
            driver.WaitForFunction(750, () => driver.IsEnabledAndDisplayed(lastPedidoPath));
            driver.ClickByXPath(lastPedidoPath);

            driver.WaitForFunction(750, () => driver.IsEnabledAndDisplayed(retakePath));
            driver.ClickByXPath(retakePath);

            driver.WaitForFunction(750, () => driver.IsEnabledAndDisplayed("/html/body/div[1]/div[1]/div[8]/div/div[6]/div/div[1]/div/div[1]/div[1]/div/div/form/div/span/div[1]/div[6]/div[1]/span"));
            driver.ClickByXPath("/html/body/div[1]/div[1]/div[8]/div/div[6]/div/div[1]/div/div[1]/div[1]/div/div/form/div/span/div[1]/div[6]/div[1]/span");
            driver.WaitForFunction(750, () => driver.IsEnabledAndDisplayed("/html/body/div[1]/div[1]/div[8]/div/div[6]/ul[4]/li[7]"));
            driver.ClickByXPath("/html/body/div[1]/div[1]/div[8]/div/div[6]/ul[4]/li[7]");
            driver.TimeoutRandom();
            driver.ClickByXPath("/html/body/div[1]/div[1]/div[8]/div/div[6]/div/div[1]/div/div[1]/div[1]/div/div/form/div/span/div[1]/div[6]/div[2]/button/span");

            ClickVenda(driver);
        }

        private static IWebElement _GetButton(IWebElement divElement, string buttonTitle)
        {
            var linkElements = divElement.GetChildrenByTag(TagConstants.A);
            var linkElement = linkElements.Where(i => _ValidateTitle(i, buttonTitle)).FirstOrDefault();
            return linkElement;
        }

        static bool _ValidateLinks(SeleniumDriver driver)
        {
            var listing = driver.GetListByTag(TagConstants.A);

            return listing.Count > 40;
        }
        static bool _ValidateTitle(IWebElement element, string title)
        {
            var elementTitle = element.GetAttribute("title")?.ClearSpecialChars(toLower: true);

            return elementTitle?.Equals(title) ?? false;
        }
    }
}