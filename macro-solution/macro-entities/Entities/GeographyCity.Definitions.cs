﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class GeographyCity
    {
		/// <summary>
		/// The Geography City's class name
		/// </summary>
		public const string CLASS_NAME = "GeographyCity";
		/// <summary>
		/// The GeographyCity's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The GeographyCity's StateId property
		/// </summary>
		public const string PROPERTY_STATE_ID = "StateId";
		/// <summary>
		/// The GeographyCity's Name property
		/// </summary>
		public const string PROPERTY_NAME = "Name";

		public int Id { get; set; }
		public int? StateId { get; set; }
		public string Name { get; set; }

		public GeographyState StateIdParent { get; set; }

        public List<GeographyNeighbourhood> GeographyNeighbourhoodList { get; set; }
        public List<GeographyZipcode> GeographyZipcodeList { get; set; }
        public List<MiningGoogle> MiningGoogleList { get; set; }
	}
}