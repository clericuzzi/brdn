﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class ConcurrencyManager
    {
		/// <summary>
		/// The Concurrency Manager's class name
		/// </summary>
		public const string CLASS_NAME = "ConcurrencyManager";
		/// <summary>
		/// The ConcurrencyManager's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The ConcurrencyManager's Action property
		/// </summary>
		public const string PROPERTY_ACTION = "Action";
		/// <summary>
		/// The ConcurrencyManager's UserId property
		/// </summary>
		public const string PROPERTY_USER_ID = "UserId";
		/// <summary>
		/// The ConcurrencyManager's MachineName property
		/// </summary>
		public const string PROPERTY_MACHINE_NAME = "MachineName";
		/// <summary>
		/// The ConcurrencyManager's EntityId property
		/// </summary>
		public const string PROPERTY_ENTITY_ID = "EntityId";
		/// <summary>
		/// The ConcurrencyManager's EntityValue property
		/// </summary>
		public const string PROPERTY_ENTITY_VALUE = "EntityValue";

		public int Id { get; set; }
		public string Action { get; set; }
		public int? UserId { get; set; }
		public string MachineName { get; set; }
		public int? EntityId { get; set; }
		public string EntityValue { get; set; }

		public User UserIdParent { get; set; }
}
}