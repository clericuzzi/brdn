﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class TblPrcCleanLeads
    {
		/// <summary>
		/// The  Tbl Prc Clean Leads's class name
		/// </summary>
		public const string CLASS_NAME = "TblPrcCleanLeads";
		/// <summary>
		/// The TblPrcCleanLeads's CompanyId property
		/// </summary>
		public const string PROPERTY_COMPANY_ID = "CompanyId";

		public int CompanyId { get; set; }
}
}