﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class VerificationZipCode
    {
		/// <summary>
		/// The Verification Zip Code's class name
		/// </summary>
		public const string CLASS_NAME = "VerificationZipCode";
		/// <summary>
		/// The VerificationZipCode's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The VerificationZipCode's Zipcode property
		/// </summary>
		public const string PROPERTY_ZIPCODE = "Zipcode";

		public int Id { get; set; }
		public string Zipcode { get; set; }
}
}