﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class ZipcodeTestingInterval
    {
		/// <summary>
		/// The Zipcode Testing Interval's class name
		/// </summary>
		public const string CLASS_NAME = "ZipcodeTestingInterval";
		/// <summary>
		/// The ZipcodeTestingInterval's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The ZipcodeTestingInterval's From property
		/// </summary>
		public const string PROPERTY_FROM = "From";
		/// <summary>
		/// The ZipcodeTestingInterval's To property
		/// </summary>
		public const string PROPERTY_TO = "To";
		/// <summary>
		/// The ZipcodeTestingInterval's Current property
		/// </summary>
		public const string PROPERTY_CURRENT = "Current";
		/// <summary>
		/// The ZipcodeTestingInterval's Busy property
		/// </summary>
		public const string PROPERTY_BUSY = "Busy";
		/// <summary>
		/// The ZipcodeTestingInterval's Priority property
		/// </summary>
		public const string PROPERTY_PRIORITY = "Priority";

		public int Id { get; set; }
		public int From { get; set; }
		public int To { get; set; }
		public int? Current { get; set; }
		public int Busy { get; set; }
		public int? Priority { get; set; }
}
}