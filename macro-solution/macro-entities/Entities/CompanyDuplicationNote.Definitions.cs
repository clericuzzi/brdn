﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class CompanyDuplicationNote
    {
		/// <summary>
		/// The  Company Duplication Note's class name
		/// </summary>
		public const string CLASS_NAME = "CompanyDuplicationNote";
		/// <summary>
		/// The CompanyDuplicationNote's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The CompanyDuplicationNote's UserId property
		/// </summary>
		public const string PROPERTY_USER_ID = "UserId";
		/// <summary>
		/// The CompanyDuplicationNote's CompanyId property
		/// </summary>
		public const string PROPERTY_COMPANY_ID = "CompanyId";
		/// <summary>
		/// The CompanyDuplicationNote's ContactId property
		/// </summary>
		public const string PROPERTY_CONTACT_ID = "ContactId";
		/// <summary>
		/// The CompanyDuplicationNote's Comment property
		/// </summary>
		public const string PROPERTY_COMMENT = "Comment";
		/// <summary>
		/// The CompanyDuplicationNote's Timestamp property
		/// </summary>
		public const string PROPERTY_TIMESTAMP = "Timestamp";

		public int Id { get; set; }
		public int UserId { get; set; }
		public int CompanyId { get; set; }
		public int? ContactId { get; set; }
		public string Comment { get; set; }
		public DateTime Timestamp { get; set; }
}
}