﻿using Clericuzzi.Lib.Utils.Core;
using System;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class CompanyAddress
    {
        public override string ToString()
        {
            return $"{Id}|{ZipcodeIdParent?.Zipcode.DigitsOnly()?.Trim()}|{Number?.DigitsOnly()?.Trim()}";
        }
    }
}