﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class CompanyDuplicationLine
    {
		/// <summary>
		/// The  Company Duplication Line's class name
		/// </summary>
		public const string CLASS_NAME = "CompanyDuplicationLine";
		/// <summary>
		/// The CompanyDuplicationLine's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The CompanyDuplicationLine's CompanyId property
		/// </summary>
		public const string PROPERTY_COMPANY_ID = "CompanyId";
		/// <summary>
		/// The CompanyDuplicationLine's Ddd property
		/// </summary>
		public const string PROPERTY_DDD = "Ddd";
		/// <summary>
		/// The CompanyDuplicationLine's Phone property
		/// </summary>
		public const string PROPERTY_PHONE = "Phone";
		/// <summary>
		/// The CompanyDuplicationLine's Mobile property
		/// </summary>
		public const string PROPERTY_MOBILE = "Mobile";
		/// <summary>
		/// The CompanyDuplicationLine's OperatorId property
		/// </summary>
		public const string PROPERTY_OPERATOR_ID = "OperatorId";
		/// <summary>
		/// The CompanyDuplicationLine's OperatorSince property
		/// </summary>
		public const string PROPERTY_OPERATOR_SINCE = "OperatorSince";
		/// <summary>
		/// The CompanyDuplicationLine's TestingTimestamp property
		/// </summary>
		public const string PROPERTY_TESTING_TIMESTAMP = "TestingTimestamp";

		public int Id { get; set; }
		public int CompanyId { get; set; }
		public string Ddd { get; set; }
		public string Phone { get; set; }
		public int Mobile { get; set; }
		public int? OperatorId { get; set; }
		public DateTime? OperatorSince { get; set; }
		public DateTime? TestingTimestamp { get; set; }
}
}