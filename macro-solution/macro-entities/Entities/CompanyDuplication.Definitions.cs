﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class CompanyDuplication
    {
		/// <summary>
		/// The  Company Duplication 's class name
		/// </summary>
		public const string CLASS_NAME = "CompanyDuplication";
		/// <summary>
		/// The CompanyDuplication's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The CompanyDuplication's CompanyId property
		/// </summary>
		public const string PROPERTY_COMPANY_ID = "CompanyId";
		/// <summary>
		/// The CompanyDuplication's Street property
		/// </summary>
		public const string PROPERTY_STREET = "Street";
		/// <summary>
		/// The CompanyDuplication's Number property
		/// </summary>
		public const string PROPERTY_NUMBER = "Number";
		/// <summary>
		/// The CompanyDuplication's Compliment property
		/// </summary>
		public const string PROPERTY_COMPLIMENT = "Compliment";
		/// <summary>
		/// The CompanyDuplication's ZipcodeId property
		/// </summary>
		public const string PROPERTY_ZIPCODE_ID = "ZipcodeId";
		/// <summary>
		/// The CompanyDuplication's Reference property
		/// </summary>
		public const string PROPERTY_REFERENCE = "Reference";
		/// <summary>
		/// The CompanyDuplication's TestingTimestamp property
		/// </summary>
		public const string PROPERTY_TESTING_TIMESTAMP = "TestingTimestamp";

		public int Id { get; set; }
		public int CompanyId { get; set; }
		public string Street { get; set; }
		public string Number { get; set; }
		public string Compliment { get; set; }
		public int? ZipcodeId { get; set; }
		public string Reference { get; set; }
		public DateTime? TestingTimestamp { get; set; }
}
}