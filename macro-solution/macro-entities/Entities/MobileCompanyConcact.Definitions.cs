﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class MobileCompanyConcact
    {
		/// <summary>
		/// The Mobile Company Concact's class name
		/// </summary>
		public const string CLASS_NAME = "MobileCompanyConcact";
		/// <summary>
		/// The MobileCompanyConcact's MobileCompanyId property
		/// </summary>
		public const string PROPERTY_MOBILE_COMPANY_ID = "MobileCompanyId";
		/// <summary>
		/// The MobileCompanyConcact's Contact property
		/// </summary>
		public const string PROPERTY_CONTACT = "Contact";

		public int MobileCompanyId { get; set; }
		public string Contact { get; set; }
}
}