﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class Company
    {
		/// <summary>
		/// The Company's class name
		/// </summary>
		public const string CLASS_NAME = "Company";
		/// <summary>
		/// The Company's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The Company's AccountId property
		/// </summary>
		public const string PROPERTY_ACCOUNT_ID = "AccountId";
		/// <summary>
		/// The Company's UserId property
		/// </summary>
		public const string PROPERTY_USER_ID = "UserId";
		/// <summary>
		/// The Company's CompanySubtypeId property
		/// </summary>
		public const string PROPERTY_COMPANY_SUBTYPE_ID = "CompanySubtypeId";
		/// <summary>
		/// The Company's Cnpj property
		/// </summary>
		public const string PROPERTY_CNPJ = "Cnpj";
		/// <summary>
		/// The Company's Phone property
		/// </summary>
		public const string PROPERTY_PHONE = "Phone";
		/// <summary>
		/// The Company's Description property
		/// </summary>
		public const string PROPERTY_DESCRIPTION = "Description";
		/// <summary>
		/// The Company's FormalName property
		/// </summary>
		public const string PROPERTY_FORMAL_NAME = "FormalName";
		/// <summary>
		/// The Company's FantasyName property
		/// </summary>
		public const string PROPERTY_FANTASY_NAME = "FantasyName";
		/// <summary>
		/// The Company's Latitude property
		/// </summary>
		public const string PROPERTY_LATITUDE = "Latitude";
		/// <summary>
		/// The Company's Longitude property
		/// </summary>
		public const string PROPERTY_LONGITUDE = "Longitude";
		/// <summary>
		/// The Company's Timestamp property
		/// </summary>
		public const string PROPERTY_TIMESTAMP = "Timestamp";
		/// <summary>
		/// The Company's Rank property
		/// </summary>
		public const string PROPERTY_RANK = "Rank";
		/// <summary>
		/// The Company's PrimaryActivity property
		/// </summary>
		public const string PROPERTY_PRIMARY_ACTIVITY = "PrimaryActivity";
		/// <summary>
		/// The Company's JuridicNature property
		/// </summary>
		public const string PROPERTY_JURIDIC_NATURE = "JuridicNature";
		/// <summary>
		/// The Company's ConvergingMessage property
		/// </summary>
		public const string PROPERTY_CONVERGING_MESSAGE = "ConvergingMessage";
		/// <summary>
		/// The Company's WalletMobile property
		/// </summary>
		public const string PROPERTY_WALLET_MOBILE = "WalletMobile";
		/// <summary>
		/// The Company's WalletLand property
		/// </summary>
		public const string PROPERTY_WALLET_LAND = "WalletLand";
		/// <summary>
		/// The Company's Capital property
		/// </summary>
		public const string PROPERTY_CAPITAL = "Capital";
		/// <summary>
		/// The Company's DateOpened property
		/// </summary>
		public const string PROPERTY_DATE_OPENED = "DateOpened";
		/// <summary>
		/// The Company's SituationId property
		/// </summary>
		public const string PROPERTY_SITUATION_ID = "SituationId";
		/// <summary>
		/// The Company's SalesForceId property
		/// </summary>
		public const string PROPERTY_SALES_FORCE_ID = "SalesForceId";
		/// <summary>
		/// The Company's DateLastUpdate property
		/// </summary>
		public const string PROPERTY_DATE_LAST_UPDATE = "DateLastUpdate";

		public int Id { get; set; }
		public int AccountId { get; set; }
		public int? UserId { get; set; }
		public int? CompanySubtypeId { get; set; }
		public string Cnpj { get; set; }
		public string Phone { get; set; }
		public string Description { get; set; }
		public string FormalName { get; set; }
		public string FantasyName { get; set; }
		public double? Latitude { get; set; }
		public double? Longitude { get; set; }
		public DateTime Timestamp { get; set; }
		public string Rank { get; set; }
		public string PrimaryActivity { get; set; }
		public string JuridicNature { get; set; }
		public string ConvergingMessage { get; set; }
		public string WalletMobile { get; set; }
		public string WalletLand { get; set; }
		public double? Capital { get; set; }
		public DateTime? DateOpened { get; set; }
		public int? SituationId { get; set; }
		public string SalesForceId { get; set; }
		public DateTime? DateLastUpdate { get; set; }

		public Account AccountIdParent { get; set; }
		public User UserIdParent { get; set; }
		public CompanySubtype CompanySubtypeIdParent { get; set; }
		public CompanySituation SituationIdParent { get; set; }

        public List<CompanyActiveService> CompanyActiveServiceList { get; set; }
        public List<CompanyAddress> CompanyAddressList { get; set; }
        public List<CompanyBill> CompanyBillList { get; set; }
        public List<CompanyCall> CompanyCallList { get; set; }
        public List<CompanyCallback> CompanyCallbackList { get; set; }
        public List<CompanyCheckRequest> CompanyCheckRequestList { get; set; }
        public List<CompanyContact> CompanyContactList { get; set; }
        public List<CompanyLine> CompanyLineList { get; set; }
        public List<CompanyNote> CompanyNoteList { get; set; }
        public List<CompanyPriority> CompanyPriorityList { get; set; }
        public List<ImportFileCompany> ImportFileCompanyList { get; set; }
        public List<Lead> LeadList { get; set; }
        public List<Sale> SaleList { get; set; }
	}
}