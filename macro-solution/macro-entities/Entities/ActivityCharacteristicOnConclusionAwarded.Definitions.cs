﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class ActivityCharacteristicOnConclusionAwarded
    {
		/// <summary>
		/// The Activity Characteristic On Conclusion Awarded's class name
		/// </summary>
		public const string CLASS_NAME = "ActivityCharacteristicOnConclusionAwarded";
		/// <summary>
		/// The ActivityCharacteristicOnConclusionAwarded's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The ActivityCharacteristicOnConclusionAwarded's ActivityId property
		/// </summary>
		public const string PROPERTY_ACTIVITY_ID = "ActivityId";
		/// <summary>
		/// The ActivityCharacteristicOnConclusionAwarded's CharacteristicId property
		/// </summary>
		public const string PROPERTY_CHARACTERISTIC_ID = "CharacteristicId";

		public int Id { get; set; }
		public int ActivityId { get; set; }
		public int CharacteristicId { get; set; }

		public Activity ActivityIdParent { get; set; }
		public Characteristic CharacteristicIdParent { get; set; }
}
}