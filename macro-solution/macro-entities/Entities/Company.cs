﻿using System;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class Company
    {
        public override string ToString()
        {
            return $"{PhoneFull}|{Id}";
        }
        public string PhoneFull { get { return Phone; } }
    }
}