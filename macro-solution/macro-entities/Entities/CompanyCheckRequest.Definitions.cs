﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class CompanyCheckRequest
    {
		/// <summary>
		/// The Company Check Request's class name
		/// </summary>
		public const string CLASS_NAME = "CompanyCheckRequest";
		/// <summary>
		/// The CompanyCheckRequest's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The CompanyCheckRequest's CheckType property
		/// </summary>
		public const string PROPERTY_CHECK_TYPE = "CheckType";
		/// <summary>
		/// The CompanyCheckRequest's CompanyId property
		/// </summary>
		public const string PROPERTY_COMPANY_ID = "CompanyId";

		public int Id { get; set; }
		public int CheckType { get; set; }
		public int CompanyId { get; set; }

		public Company CompanyIdParent { get; set; }
}
}