﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class LogError
    {
		/// <summary>
		/// The Log Error's class name
		/// </summary>
		public const string CLASS_NAME = "LogError";
		/// <summary>
		/// The LogError's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The LogError's UserId property
		/// </summary>
		public const string PROPERTY_USER_ID = "UserId";
		/// <summary>
		/// The LogError's ClassName property
		/// </summary>
		public const string PROPERTY_CLASS_NAME = "ClassName";
		/// <summary>
		/// The LogError's MethodName property
		/// </summary>
		public const string PROPERTY_METHOD_NAME = "MethodName";
		/// <summary>
		/// The LogError's ExceptionType property
		/// </summary>
		public const string PROPERTY_EXCEPTION_TYPE = "ExceptionType";
		/// <summary>
		/// The LogError's ExceptionMessage property
		/// </summary>
		public const string PROPERTY_EXCEPTION_MESSAGE = "ExceptionMessage";
		/// <summary>
		/// The LogError's ExceptionStackTrace property
		/// </summary>
		public const string PROPERTY_EXCEPTION_STACK_TRACE = "ExceptionStackTrace";
		/// <summary>
		/// The LogError's Timestamp property
		/// </summary>
		public const string PROPERTY_TIMESTAMP = "Timestamp";

		public int Id { get; set; }
		public int? UserId { get; set; }
		public string ClassName { get; set; }
		public string MethodName { get; set; }
		public string ExceptionType { get; set; }
		public string ExceptionMessage { get; set; }
		public string ExceptionStackTrace { get; set; }
		public DateTime Timestamp { get; set; }

		public User UserIdParent { get; set; }
}
}