﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class UserArea
    {
		/// <summary>
		/// The User Area's class name
		/// </summary>
		public const string CLASS_NAME = "UserArea";
		/// <summary>
		/// The UserArea's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The UserArea's UserId property
		/// </summary>
		public const string PROPERTY_USER_ID = "UserId";
		/// <summary>
		/// The UserArea's Cities property
		/// </summary>
		public const string PROPERTY_CITIES = "Cities";
		/// <summary>
		/// The UserArea's Neighbourhoods property
		/// </summary>
		public const string PROPERTY_NEIGHBOURHOODS = "Neighbourhoods";

		public int Id { get; set; }
		public int UserId { get; set; }
		public string Cities { get; set; }
		public string Neighbourhoods { get; set; }

		public User UserIdParent { get; set; }
}
}