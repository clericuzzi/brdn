﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class MacroMachine
    {
		/// <summary>
		/// The Macro Machine's class name
		/// </summary>
		public const string CLASS_NAME = "MacroMachine";
		/// <summary>
		/// The MacroMachine's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The MacroMachine's MachineName property
		/// </summary>
		public const string PROPERTY_MACHINE_NAME = "MachineName";
		/// <summary>
		/// The MacroMachine's IsAlive property
		/// </summary>
		public const string PROPERTY_IS_ALIVE = "IsAlive";
		/// <summary>
		/// The MacroMachine's Updated property
		/// </summary>
		public const string PROPERTY_UPDATED = "Updated";

		public int Id { get; set; }
		public string MachineName { get; set; }
		public int IsAlive { get; set; }
		public int Updated { get; set; }

        public List<MacroMachineConfig> MacroMachineConfigList { get; set; }
	}
}