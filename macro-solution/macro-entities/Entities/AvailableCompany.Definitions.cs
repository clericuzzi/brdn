﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class AvailableCompany
    {
		/// <summary>
		/// The Available Company's class name
		/// </summary>
		public const string CLASS_NAME = "AvailableCompany";
		/// <summary>
		/// The AvailableCompany's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";

		public int Id { get; set; }
}
}