﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class State
    {
		/// <summary>
		/// The State's class name
		/// </summary>
		public const string CLASS_NAME = "State";
		/// <summary>
		/// The State's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The State's CountryId property
		/// </summary>
		public const string PROPERTY_COUNTRY_ID = "CountryId";
		/// <summary>
		/// The State's Name property
		/// </summary>
		public const string PROPERTY_NAME = "Name";
		/// <summary>
		/// The State's Abbreviation property
		/// </summary>
		public const string PROPERTY_ABBREVIATION = "Abbreviation";

		public int Id { get; set; }
		public int CountryId { get; set; }
		public string Name { get; set; }
		public string Abbreviation { get; set; }

		public Country CountryIdParent { get; set; }

        public List<City> CityList { get; set; }
	}
}