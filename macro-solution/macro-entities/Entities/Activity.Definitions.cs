﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class Activity
    {
		/// <summary>
		/// The Activity's class name
		/// </summary>
		public const string CLASS_NAME = "Activity";
		/// <summary>
		/// The Activity's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The Activity's AccountId property
		/// </summary>
		public const string PROPERTY_ACCOUNT_ID = "AccountId";
		/// <summary>
		/// The Activity's Name property
		/// </summary>
		public const string PROPERTY_NAME = "Name";
		/// <summary>
		/// The Activity's DailyCount property
		/// </summary>
		public const string PROPERTY_DAILY_COUNT = "DailyCount";

		public int Id { get; set; }
		public int AccountId { get; set; }
		public string Name { get; set; }
		public int? DailyCount { get; set; }

		public Account AccountIdParent { get; set; }

        public List<ActivityAllowedUser> ActivityAllowedUserList { get; set; }
        public List<ActivityCharacteristicMust> ActivityCharacteristicMustList { get; set; }
        public List<ActivityCharacteristicMustNot> ActivityCharacteristicMustNotList { get; set; }
        public List<ActivityCharacteristicOnConclusionAwarded> ActivityCharacteristicOnConclusionAwardedList { get; set; }
        public List<ActivityCharacteristicOnConclusionRevoked> ActivityCharacteristicOnConclusionRevokedList { get; set; }
        public List<ActivityItem> ActivityItemList { get; set; }
        public List<ActivityItemLog> ActivityItemLogList { get; set; }
        public List<Lead> LeadList { get; set; }
	}
}