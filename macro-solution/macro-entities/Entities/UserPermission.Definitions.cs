﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class UserPermission
    {
		/// <summary>
		/// The User Permission's class name
		/// </summary>
		public const string CLASS_NAME = "UserPermission";
		/// <summary>
		/// The UserPermission's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The UserPermission's UserId property
		/// </summary>
		public const string PROPERTY_USER_ID = "UserId";
		/// <summary>
		/// The UserPermission's PermissionId property
		/// </summary>
		public const string PROPERTY_PERMISSION_ID = "PermissionId";

		public int Id { get; set; }
		public int UserId { get; set; }
		public int PermissionId { get; set; }

		public User UserIdParent { get; set; }
		public Permission PermissionIdParent { get; set; }
}
}