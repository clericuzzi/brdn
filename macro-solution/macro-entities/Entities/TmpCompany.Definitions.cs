﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class TmpCompany
    {
		/// <summary>
		/// The  Tmp Company's class name
		/// </summary>
		public const string CLASS_NAME = "TmpCompany";
		/// <summary>
		/// The TmpCompany's Phone property
		/// </summary>
		public const string PROPERTY_PHONE = "Phone";

		public string Phone { get; set; }
}
}