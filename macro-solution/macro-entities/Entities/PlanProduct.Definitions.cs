﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class PlanProduct
    {
		/// <summary>
		/// The Plan Product's class name
		/// </summary>
		public const string CLASS_NAME = "PlanProduct";
		/// <summary>
		/// The PlanProduct's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The PlanProduct's AccountId property
		/// </summary>
		public const string PROPERTY_ACCOUNT_ID = "AccountId";
		/// <summary>
		/// The PlanProduct's PlanId property
		/// </summary>
		public const string PROPERTY_PLAN_ID = "PlanId";
		/// <summary>
		/// The PlanProduct's ProductId property
		/// </summary>
		public const string PROPERTY_PRODUCT_ID = "ProductId";
		/// <summary>
		/// The PlanProduct's Timestamp property
		/// </summary>
		public const string PROPERTY_TIMESTAMP = "Timestamp";

		public int Id { get; set; }
		public int AccountId { get; set; }
		public int PlanId { get; set; }
		public int ProductId { get; set; }
		public DateTime Timestamp { get; set; }

		public Account AccountIdParent { get; set; }
		public Plan PlanIdParent { get; set; }
		public Product ProductIdParent { get; set; }
}
}