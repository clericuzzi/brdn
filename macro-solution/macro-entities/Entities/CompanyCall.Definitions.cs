﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class CompanyCall
    {
		/// <summary>
		/// The Company Call's class name
		/// </summary>
		public const string CLASS_NAME = "CompanyCall";
		/// <summary>
		/// The CompanyCall's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The CompanyCall's UserId property
		/// </summary>
		public const string PROPERTY_USER_ID = "UserId";
		/// <summary>
		/// The CompanyCall's CompanyId property
		/// </summary>
		public const string PROPERTY_COMPANY_ID = "CompanyId";
		/// <summary>
		/// The CompanyCall's ContactId property
		/// </summary>
		public const string PROPERTY_CONTACT_ID = "ContactId";
		/// <summary>
		/// The CompanyCall's TabulationId property
		/// </summary>
		public const string PROPERTY_TABULATION_ID = "TabulationId";
		/// <summary>
		/// The CompanyCall's StartedIn property
		/// </summary>
		public const string PROPERTY_STARTED_IN = "StartedIn";
		/// <summary>
		/// The CompanyCall's EndedIn property
		/// </summary>
		public const string PROPERTY_ENDED_IN = "EndedIn";

		public int Id { get; set; }
		public int UserId { get; set; }
		public int CompanyId { get; set; }
		public int? ContactId { get; set; }
		public int? TabulationId { get; set; }
		public DateTime StartedIn { get; set; }
		public DateTime? EndedIn { get; set; }

		public User UserIdParent { get; set; }
		public Company CompanyIdParent { get; set; }
		public CompanyContact ContactIdParent { get; set; }
		public Tabulation TabulationIdParent { get; set; }
}
}