﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class Neighbourhood
    {
		/// <summary>
		/// The Neighbourhood's class name
		/// </summary>
		public const string CLASS_NAME = "Neighbourhood";
		/// <summary>
		/// The Neighbourhood's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The Neighbourhood's CityId property
		/// </summary>
		public const string PROPERTY_CITY_ID = "CityId";
		/// <summary>
		/// The Neighbourhood's Name property
		/// </summary>
		public const string PROPERTY_NAME = "Name";

		public int Id { get; set; }
		public int? CityId { get; set; }
		public string Name { get; set; }

		public City CityIdParent { get; set; }

        public List<Zipcode> ZipcodeList { get; set; }
	}
}