﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class CompanySubtype
    {
		/// <summary>
		/// The Company Subtype's class name
		/// </summary>
		public const string CLASS_NAME = "CompanySubtype";
		/// <summary>
		/// The CompanySubtype's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The CompanySubtype's CompanyTypeId property
		/// </summary>
		public const string PROPERTY_COMPANY_TYPE_ID = "CompanyTypeId";
		/// <summary>
		/// The CompanySubtype's Subtype property
		/// </summary>
		public const string PROPERTY_SUBTYPE = "Subtype";

		public int Id { get; set; }
		public int CompanyTypeId { get; set; }
		public string Subtype { get; set; }

		public CompanyType CompanyTypeIdParent { get; set; }

        public List<Company> CompanyList { get; set; }
        public List<MiningGoogle> MiningGoogleList { get; set; }
	}
}