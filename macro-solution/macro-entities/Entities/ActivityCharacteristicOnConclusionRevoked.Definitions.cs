﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class ActivityCharacteristicOnConclusionRevoked
    {
		/// <summary>
		/// The Activity Characteristic On Conclusion Revoked's class name
		/// </summary>
		public const string CLASS_NAME = "ActivityCharacteristicOnConclusionRevoked";
		/// <summary>
		/// The ActivityCharacteristicOnConclusionRevoked's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The ActivityCharacteristicOnConclusionRevoked's ActivityId property
		/// </summary>
		public const string PROPERTY_ACTIVITY_ID = "ActivityId";
		/// <summary>
		/// The ActivityCharacteristicOnConclusionRevoked's CharacteristicId property
		/// </summary>
		public const string PROPERTY_CHARACTERISTIC_ID = "CharacteristicId";

		public int Id { get; set; }
		public int ActivityId { get; set; }
		public int CharacteristicId { get; set; }

		public Activity ActivityIdParent { get; set; }
		public Characteristic CharacteristicIdParent { get; set; }
}
}