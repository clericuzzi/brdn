﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class Povo
    {
		/// <summary>
		/// The Povo's class name
		/// </summary>
		public const string CLASS_NAME = "Povo";
		/// <summary>
		/// The Povo's Nome property
		/// </summary>
		public const string PROPERTY_NOME = "Nome";
		/// <summary>
		/// The Povo's Idada property
		/// </summary>
		public const string PROPERTY_IDADA = "Idada";

		public string Nome { get; set; }
		public int? Idada { get; set; }
}
}