﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class BlockedTabulations
    {
		/// <summary>
		/// The Blocked Tabulations's class name
		/// </summary>
		public const string CLASS_NAME = "BlockedTabulations";
		/// <summary>
		/// The BlockedTabulations's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The BlockedTabulations's TabulationId property
		/// </summary>
		public const string PROPERTY_TABULATION_ID = "TabulationId";

		public int Id { get; set; }
		public int TabulationId { get; set; }

		public Tabulation TabulationIdParent { get; set; }
}
}