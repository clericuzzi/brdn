﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class LogDatasetChange
    {
		/// <summary>
		/// The Log Dataset Change's class name
		/// </summary>
		public const string CLASS_NAME = "LogDatasetChange";
		/// <summary>
		/// The LogDatasetChange's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The LogDatasetChange's UserId property
		/// </summary>
		public const string PROPERTY_USER_ID = "UserId";
		/// <summary>
		/// The LogDatasetChange's Action property
		/// </summary>
		public const string PROPERTY_ACTION = "Action";
		/// <summary>
		/// The LogDatasetChange's EntityType property
		/// </summary>
		public const string PROPERTY_ENTITY_TYPE = "EntityType";
		/// <summary>
		/// The LogDatasetChange's EntitySummary property
		/// </summary>
		public const string PROPERTY_ENTITY_SUMMARY = "EntitySummary";
		/// <summary>
		/// The LogDatasetChange's Timestamp property
		/// </summary>
		public const string PROPERTY_TIMESTAMP = "Timestamp";

		public int Id { get; set; }
		public int UserId { get; set; }
		public string Action { get; set; }
		public string EntityType { get; set; }
		public string EntitySummary { get; set; }
		public DateTime Timestamp { get; set; }

		public User UserIdParent { get; set; }
}
}