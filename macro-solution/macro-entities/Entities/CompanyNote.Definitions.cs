﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class CompanyNote
    {
		/// <summary>
		/// The Company Note's class name
		/// </summary>
		public const string CLASS_NAME = "CompanyNote";
		/// <summary>
		/// The CompanyNote's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The CompanyNote's UserId property
		/// </summary>
		public const string PROPERTY_USER_ID = "UserId";
		/// <summary>
		/// The CompanyNote's CompanyId property
		/// </summary>
		public const string PROPERTY_COMPANY_ID = "CompanyId";
		/// <summary>
		/// The CompanyNote's ContactId property
		/// </summary>
		public const string PROPERTY_CONTACT_ID = "ContactId";
		/// <summary>
		/// The CompanyNote's Comment property
		/// </summary>
		public const string PROPERTY_COMMENT = "Comment";
		/// <summary>
		/// The CompanyNote's Timestamp property
		/// </summary>
		public const string PROPERTY_TIMESTAMP = "Timestamp";

		public int Id { get; set; }
		public int UserId { get; set; }
		public int CompanyId { get; set; }
		public int? ContactId { get; set; }
		public string Comment { get; set; }
		public DateTime Timestamp { get; set; }

		public User UserIdParent { get; set; }
		public Company CompanyIdParent { get; set; }
		public CompanyContact ContactIdParent { get; set; }
}
}