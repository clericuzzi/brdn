﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class NewCnpj
    {
		/// <summary>
		/// The  New Cnpj's class name
		/// </summary>
		public const string CLASS_NAME = "NewCnpj";
		/// <summary>
		/// The NewCnpj's Cnpj property
		/// </summary>
		public const string PROPERTY_CNPJ = "Cnpj";

		public string Cnpj { get; set; }
}
}