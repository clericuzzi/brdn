﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class ActivityAllowedUser
    {
		/// <summary>
		/// The Activity Allowed User's class name
		/// </summary>
		public const string CLASS_NAME = "ActivityAllowedUser";
		/// <summary>
		/// The ActivityAllowedUser's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The ActivityAllowedUser's ActivityId property
		/// </summary>
		public const string PROPERTY_ACTIVITY_ID = "ActivityId";
		/// <summary>
		/// The ActivityAllowedUser's UserId property
		/// </summary>
		public const string PROPERTY_USER_ID = "UserId";
		/// <summary>
		/// The ActivityAllowedUser's Timestamp property
		/// </summary>
		public const string PROPERTY_TIMESTAMP = "Timestamp";

		public int Id { get; set; }
		public int ActivityId { get; set; }
		public int UserId { get; set; }
		public DateTime Timestamp { get; set; }

		public Activity ActivityIdParent { get; set; }
		public User UserIdParent { get; set; }
}
}