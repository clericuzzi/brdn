﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class Account
    {
		/// <summary>
		/// The Account's class name
		/// </summary>
		public const string CLASS_NAME = "Account";
		/// <summary>
		/// The Account's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The Account's Cnpj property
		/// </summary>
		public const string PROPERTY_CNPJ = "Cnpj";
		/// <summary>
		/// The Account's Name property
		/// </summary>
		public const string PROPERTY_NAME = "Name";
		/// <summary>
		/// The Account's Site property
		/// </summary>
		public const string PROPERTY_SITE = "Site";
		/// <summary>
		/// The Account's Email property
		/// </summary>
		public const string PROPERTY_EMAIL = "Email";
		/// <summary>
		/// The Account's Phone property
		/// </summary>
		public const string PROPERTY_PHONE = "Phone";
		/// <summary>
		/// The Account's Blocked property
		/// </summary>
		public const string PROPERTY_BLOCKED = "Blocked";
		/// <summary>
		/// The Account's Latitude property
		/// </summary>
		public const string PROPERTY_LATITUDE = "Latitude";
		/// <summary>
		/// The Account's Longitude property
		/// </summary>
		public const string PROPERTY_LONGITUDE = "Longitude";
		/// <summary>
		/// The Account's SignatureFee property
		/// </summary>
		public const string PROPERTY_SIGNATURE_FEE = "SignatureFee";
		/// <summary>
		/// The Account's Timestamp property
		/// </summary>
		public const string PROPERTY_TIMESTAMP = "Timestamp";

		public int Id { get; set; }
		public string Cnpj { get; set; }
		public string Name { get; set; }
		public string Site { get; set; }
		public string Email { get; set; }
		public string Phone { get; set; }
		public int Blocked { get; set; }
		public double? Latitude { get; set; }
		public double? Longitude { get; set; }
		public double? SignatureFee { get; set; }
		public DateTime Timestamp { get; set; }

        public List<AccountConfig> AccountConfigList { get; set; }
        public List<AccountContact> AccountContactList { get; set; }
        public List<AccountPaymentHistory> AccountPaymentHistoryList { get; set; }
        public List<AccountSignatureFeeHistory> AccountSignatureFeeHistoryList { get; set; }
        public List<Activity> ActivityList { get; set; }
        public List<Characteristic> CharacteristicList { get; set; }
        public List<Company> CompanyList { get; set; }
        public List<ContactDepartment> ContactDepartmentList { get; set; }
        public List<ContactPosition> ContactPositionList { get; set; }
        public List<Permission> PermissionList { get; set; }
        public List<Product> ProductList { get; set; }
        public List<SecureZone> SecureZoneList { get; set; }
	}
}