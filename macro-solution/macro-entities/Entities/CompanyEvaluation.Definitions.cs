﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class CompanyEvaluation
    {
		/// <summary>
		/// The Company Evaluation's class name
		/// </summary>
		public const string CLASS_NAME = "CompanyEvaluation";
		/// <summary>
		/// The CompanyEvaluation's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The CompanyEvaluation's CompanyId property
		/// </summary>
		public const string PROPERTY_COMPANY_ID = "CompanyId";
		/// <summary>
		/// The CompanyEvaluation's UserId property
		/// </summary>
		public const string PROPERTY_USER_ID = "UserId";
		/// <summary>
		/// The CompanyEvaluation's Evaluation property
		/// </summary>
		public const string PROPERTY_EVALUATION = "Evaluation";
		/// <summary>
		/// The CompanyEvaluation's Timestamp property
		/// </summary>
		public const string PROPERTY_TIMESTAMP = "Timestamp";

		public int Id { get; set; }
		public int CompanyId { get; set; }
		public int UserId { get; set; }
		public int? Evaluation { get; set; }
		public DateTime Timestamp { get; set; }

		public Company CompanyIdParent { get; set; }
		public User UserIdParent { get; set; }
}
}