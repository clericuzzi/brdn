﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class GeographyZipcode
    {
		/// <summary>
		/// The Geography Zipcode's class name
		/// </summary>
		public const string CLASS_NAME = "GeographyZipcode";
		/// <summary>
		/// The GeographyZipcode's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The GeographyZipcode's Zipcode property
		/// </summary>
		public const string PROPERTY_ZIPCODE = "Zipcode";
		/// <summary>
		/// The GeographyZipcode's CityId property
		/// </summary>
		public const string PROPERTY_CITY_ID = "CityId";
		/// <summary>
		/// The GeographyZipcode's NeighbourhoodId property
		/// </summary>
		public const string PROPERTY_NEIGHBOURHOOD_ID = "NeighbourhoodId";
		/// <summary>
		/// The GeographyZipcode's Location property
		/// </summary>
		public const string PROPERTY_LOCATION = "Location";

		public int Id { get; set; }
		public string Zipcode { get; set; }
		public int? CityId { get; set; }
		public int? NeighbourhoodId { get; set; }
		public string Location { get; set; }

		public GeographyCity CityIdParent { get; set; }
		public GeographyNeighbourhood NeighbourhoodIdParent { get; set; }

        public List<CompanyAddress> CompanyAddressList { get; set; }
	}
}