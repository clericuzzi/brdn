﻿using Clericuzzi.Lib.Utils.Core;
using System;
using System.Linq;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class CompanyAvailability
    {
        public void Parse(string result)
        {
            var breakline = "\r\n";
            var lines = result.SplitByString(breakline);
            result = result.RemoveSpecialChars(toLower: true);

            Lines = _GetIntValue(result, "quantidade: ", breakline, ";");
            Over50 = _GetIntValue(result, "de 51 a 1 gbps: ", breakline, ";");
            Over50Max = _GetMaxValue(result, "de 51 a 1 gbps: ", breakline);
            Over50Sip = _GetSipValue(result, "de 51 a 1 gbps: ", breakline);
            Until20 = _GetIntValue(result, " ate 20mbps: ", breakline, ";");
            Until20Max = _GetMaxValue(result, " ate 20mbps: ", breakline);
            Until20Sip = _GetSipValue(result, " ate 20mbps: ", breakline);
            From21UpTo50 = _GetIntValue(result, "de 21 a 50mbps: ", breakline, ";");
            From21UpTo50Max = _GetMaxValue(result, "de 21 a 50mbps: ", breakline);
            From21UpTo50Sip = _GetSipValue(result, "de 21 a 50mbps: ", breakline);

            ClosetDistance = _GetIntValue(result, "distancia do armario: ", breakline, ";");
            ClosetTopSpeed = _GetIntValue(result, "velocidade maxima do armario: ", breakline, ";");

            Message = lines[1].ClearSpecialChars(toLower: true)?.Trim();
            Network = _GetValue(result, "tipo armario: ", breakline, ";")?.Trim();
            Tecnology = _GetValue(result, "tecnologia do acesso: ", breakline, ";")?.Trim();
            TecnologyForTv = _GetValue(result, "tecnologia de tv: ", breakline, ";")?.Trim();
        }

        string _GetValue(string result, string sectionHead, params string[] firstOccurrence)
        {
            var section = result.GetSection(sectionHead, firstOccurrence)?.ClearSpecialChars().Trim();

            return section;
        }
        int? _GetIntValue(string result, string sectionHead, params string[] firstOccurrence)
        {
            var section = result.GetSection(sectionHead, firstOccurrence);
            if (int.TryParse(section.DigitsOnly(), out int value))
                return value;
            else
                return null;
        }
        string _GetMaxValue(string result, string sectionHead, params string[] firstOccurrence)
        {
            var section = result.GetSection(sectionHead, firstOccurrence);
            if (section.Contains("velocidade maxima"))
            {
                var sections = section.Split(';');
                section = sections.Where(i => i.Contains("velocidade maxima")).FirstOrDefault();

                return section.Replace("velocidade maxima", string.Empty).Trim();
            }

            return null;
        }
        int? _GetSipValue(string result, string sectionHead, params string[] firstOccurrence)
        {
            var section = result.GetSection(sectionHead, firstOccurrence);
            return ((section?.Contains("tecnologia sip") ?? false) ? 1 : 0);
        }
    }
}