﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class CompanyPriority
    {
		/// <summary>
		/// The Company Priority's class name
		/// </summary>
		public const string CLASS_NAME = "CompanyPriority";
		/// <summary>
		/// The CompanyPriority's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The CompanyPriority's CompanyId property
		/// </summary>
		public const string PROPERTY_COMPANY_ID = "CompanyId";
		/// <summary>
		/// The CompanyPriority's Priority property
		/// </summary>
		public const string PROPERTY_PRIORITY = "Priority";
		/// <summary>
		/// The CompanyPriority's Timestamp property
		/// </summary>
		public const string PROPERTY_TIMESTAMP = "Timestamp";

		public int Id { get; set; }
		public int CompanyId { get; set; }
		public int Priority { get; set; }
		public DateTime Timestamp { get; set; }

		public Company CompanyIdParent { get; set; }
}
}