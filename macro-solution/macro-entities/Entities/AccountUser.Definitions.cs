﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class AccountUser
    {
		/// <summary>
		/// The Account User's class name
		/// </summary>
		public const string CLASS_NAME = "AccountUser";
		/// <summary>
		/// The AccountUser's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The AccountUser's AccountId property
		/// </summary>
		public const string PROPERTY_ACCOUNT_ID = "AccountId";
		/// <summary>
		/// The AccountUser's UserId property
		/// </summary>
		public const string PROPERTY_USER_ID = "UserId";
		/// <summary>
		/// The AccountUser's Active property
		/// </summary>
		public const string PROPERTY_ACTIVE = "Active";
		/// <summary>
		/// The AccountUser's Blocked property
		/// </summary>
		public const string PROPERTY_BLOCKED = "Blocked";
		/// <summary>
		/// The AccountUser's Timestamp property
		/// </summary>
		public const string PROPERTY_TIMESTAMP = "Timestamp";

		public int Id { get; set; }
		public int AccountId { get; set; }
		public int UserId { get; set; }
		public int Active { get; set; }
		public int Blocked { get; set; }
		public DateTime Timestamp { get; set; }

		public Account AccountIdParent { get; set; }
		public User UserIdParent { get; set; }
}
}