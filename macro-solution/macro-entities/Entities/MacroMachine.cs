﻿using Clericuzzi.Lib.Utils.Core;
using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class MacroMachine
    {
        public List<MacroMachineConfig> Configurations { get; set; }
        public string ActiveConfigurations
        {
            get
            {
                var configs = string.Empty;
                if (Configurations?.Count > 0)
                    foreach (var config in Configurations)
                    {
                        var period = "\r\n";
                        if (config.Starting.HasValue && !config.Ending.HasValue)
                            period += $"Inicia em: {config.Starting.Value}h";
                        else if (!config.Starting.HasValue && config.Ending.HasValue)
                            period += $"Finaliza em: {config.Ending.Value}h";
                        else if (!config.Starting.HasValue && !config.Ending.HasValue)
                            period += "enquanto a máquina estiver ativa";
                        else
                            period += $"Entre: {config.Starting.Value}-{config.Ending.Value}h";

                        var aditionalInfo = $"\r\nSegundo Plano: {(config.FromBackground == 0 ? "não" : "sim")}";
                        aditionalInfo += $"\r\nDesliga a máquian ao finalizar: {(config.TurnOffAfter == 0 ? "não" : "sim")}";

                        configs += $"\r\n - {config.MacroIdParent.Macro} ({config.Instances} instâncias){period}{aditionalInfo}".RemoveDoubleSpaces();
                    }

                return configs;
            }
        }
    }
}