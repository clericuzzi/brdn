﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class SystemParameter
    {
		/// <summary>
		/// The System Parameter's class name
		/// </summary>
		public const string CLASS_NAME = "SystemParameter";
		/// <summary>
		/// The SystemParameter's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The SystemParameter's Parameter property
		/// </summary>
		public const string PROPERTY_PARAMETER = "Parameter";
		/// <summary>
		/// The SystemParameter's Value property
		/// </summary>
		public const string PROPERTY_VALUE = "Value";

		public int Id { get; set; }
		public string Parameter { get; set; }
		public string Value { get; set; }
}
}