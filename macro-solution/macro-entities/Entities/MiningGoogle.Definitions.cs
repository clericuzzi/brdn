﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class MiningGoogle
    {
		/// <summary>
		/// The Mining Google's class name
		/// </summary>
		public const string CLASS_NAME = "MiningGoogle";
		/// <summary>
		/// The MiningGoogle's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The MiningGoogle's CityId property
		/// </summary>
		public const string PROPERTY_CITY_ID = "CityId";
		/// <summary>
		/// The MiningGoogle's CompanySubtypeId property
		/// </summary>
		public const string PROPERTY_COMPANY_SUBTYPE_ID = "CompanySubtypeId";
		/// <summary>
		/// The MiningGoogle's Priority property
		/// </summary>
		public const string PROPERTY_PRIORITY = "Priority";

		public int Id { get; set; }
		public int CityId { get; set; }
		public int CompanySubtypeId { get; set; }
		public int Priority { get; set; }

		public GeographyCity CityIdParent { get; set; }
		public CompanySubtype CompanySubtypeIdParent { get; set; }
}
}