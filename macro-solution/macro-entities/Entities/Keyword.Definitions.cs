﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class Keyword
    {
		/// <summary>
		/// The Keyword's class name
		/// </summary>
		public const string CLASS_NAME = "Keyword";
		/// <summary>
		/// The Keyword's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The Keyword's AccountId property
		/// </summary>
		public const string PROPERTY_ACCOUNT_ID = "AccountId";
		/// <summary>
		/// The Keyword's Name property
		/// </summary>
		public const string PROPERTY_NAME = "Name";

		public int Id { get; set; }
		public int AccountId { get; set; }
		public string Name { get; set; }

		public Account AccountIdParent { get; set; }

        public List<CompanyKeyword> CompanyKeywordList { get; set; }
	}
}