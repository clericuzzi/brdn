﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class MacroLogin
    {
		/// <summary>
		/// The Macro Login's class name
		/// </summary>
		public const string CLASS_NAME = "MacroLogin";
		/// <summary>
		/// The MacroLogin's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The MacroLogin's Macro property
		/// </summary>
		public const string PROPERTY_MACRO = "Macro";
		/// <summary>
		/// The MacroLogin's Url property
		/// </summary>
		public const string PROPERTY_URL = "Url";
		/// <summary>
		/// The MacroLogin's Login property
		/// </summary>
		public const string PROPERTY_LOGIN = "Login";
		/// <summary>
		/// The MacroLogin's Password property
		/// </summary>
		public const string PROPERTY_PASSWORD = "Password";
		/// <summary>
		/// The MacroLogin's AdditionalInfo01 property
		/// </summary>
		public const string PROPERTY_ADDITIONAL_INFO_01 = "AdditionalInfo01";
		/// <summary>
		/// The MacroLogin's AdditionalInfo02 property
		/// </summary>
		public const string PROPERTY_ADDITIONAL_INFO_02 = "AdditionalInfo02";
		/// <summary>
		/// The MacroLogin's AdditionalInfo03 property
		/// </summary>
		public const string PROPERTY_ADDITIONAL_INFO_03 = "AdditionalInfo03";
		/// <summary>
		/// The MacroLogin's LastLoginError property
		/// </summary>
		public const string PROPERTY_LAST_LOGIN_ERROR = "LastLoginError";

		public int Id { get; set; }
		public string Macro { get; set; }
		public string Url { get; set; }
		public string Login { get; set; }
		public string Password { get; set; }
		public string AdditionalInfo01 { get; set; }
		public string AdditionalInfo02 { get; set; }
		public string AdditionalInfo03 { get; set; }
		public string LastLoginError { get; set; }

        public List<MacroMachineConfig> MacroMachineConfigList { get; set; }
	}
}