﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class CompanyDuplicationContactLine
    {
		/// <summary>
		/// The  Company Duplication Contact Line's class name
		/// </summary>
		public const string CLASS_NAME = "CompanyDuplicationContactLine";
		/// <summary>
		/// The CompanyDuplicationContactLine's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The CompanyDuplicationContactLine's CompanyContactId property
		/// </summary>
		public const string PROPERTY_COMPANY_CONTACT_ID = "CompanyContactId";
		/// <summary>
		/// The CompanyDuplicationContactLine's CompanyLineId property
		/// </summary>
		public const string PROPERTY_COMPANY_LINE_ID = "CompanyLineId";

		public int Id { get; set; }
		public int CompanyContactId { get; set; }
		public int CompanyLineId { get; set; }
}
}