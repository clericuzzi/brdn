﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class LogPropertyChange
    {
		/// <summary>
		/// The Log Property Change's class name
		/// </summary>
		public const string CLASS_NAME = "LogPropertyChange";
		/// <summary>
		/// The LogPropertyChange's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The LogPropertyChange's UserId property
		/// </summary>
		public const string PROPERTY_USER_ID = "UserId";
		/// <summary>
		/// The LogPropertyChange's EntityType property
		/// </summary>
		public const string PROPERTY_ENTITY_TYPE = "EntityType";
		/// <summary>
		/// The LogPropertyChange's PropertyName property
		/// </summary>
		public const string PROPERTY_PROPERTY_NAME = "PropertyName";
		/// <summary>
		/// The LogPropertyChange's PropertyValueOld property
		/// </summary>
		public const string PROPERTY_PROPERTY_VALUE_OLD = "PropertyValueOld";
		/// <summary>
		/// The LogPropertyChange's PropertyValueNew property
		/// </summary>
		public const string PROPERTY_PROPERTY_VALUE_NEW = "PropertyValueNew";
		/// <summary>
		/// The LogPropertyChange's Timestamp property
		/// </summary>
		public const string PROPERTY_TIMESTAMP = "Timestamp";

		public int Id { get; set; }
		public int UserId { get; set; }
		public string EntityType { get; set; }
		public string PropertyName { get; set; }
		public string PropertyValueOld { get; set; }
		public string PropertyValueNew { get; set; }
		public DateTime Timestamp { get; set; }

		public User UserIdParent { get; set; }
}
}