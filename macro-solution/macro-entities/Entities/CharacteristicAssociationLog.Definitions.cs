﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class CharacteristicAssociationLog
    {
		/// <summary>
		/// The Characteristic Association Log's class name
		/// </summary>
		public const string CLASS_NAME = "CharacteristicAssociationLog";
		/// <summary>
		/// The CharacteristicAssociationLog's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The CharacteristicAssociationLog's CharacteristicId property
		/// </summary>
		public const string PROPERTY_CHARACTERISTIC_ID = "CharacteristicId";
		/// <summary>
		/// The CharacteristicAssociationLog's EntityId property
		/// </summary>
		public const string PROPERTY_ENTITY_ID = "EntityId";
		/// <summary>
		/// The CharacteristicAssociationLog's Action property
		/// </summary>
		public const string PROPERTY_ACTION = "Action";
		/// <summary>
		/// The CharacteristicAssociationLog's Timestamp property
		/// </summary>
		public const string PROPERTY_TIMESTAMP = "Timestamp";

		public int Id { get; set; }
		public int CharacteristicId { get; set; }
		public int EntityId { get; set; }
		public int Action { get; set; }
		public DateTime Timestamp { get; set; }

		public Characteristic CharacteristicIdParent { get; set; }
}
}