﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class Concurrency
    {
		/// <summary>
		/// The Concurrency's class name
		/// </summary>
		public const string CLASS_NAME = "Concurrency";
		/// <summary>
		/// The Concurrency's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The Concurrency's Action property
		/// </summary>
		public const string PROPERTY_ACTION = "Action";
		/// <summary>
		/// The Concurrency's UserId property
		/// </summary>
		public const string PROPERTY_USER_ID = "UserId";
		/// <summary>
		/// The Concurrency's MachineName property
		/// </summary>
		public const string PROPERTY_MACHINE_NAME = "MachineName";
		/// <summary>
		/// The Concurrency's EntityId property
		/// </summary>
		public const string PROPERTY_ENTITY_ID = "EntityId";
		/// <summary>
		/// The Concurrency's EntityValue property
		/// </summary>
		public const string PROPERTY_ENTITY_VALUE = "EntityValue";

		public int Id { get; set; }
		public string Action { get; set; }
		public int? UserId { get; set; }
		public string MachineName { get; set; }
		public int? EntityId { get; set; }
		public string EntityValue { get; set; }

		public User UserIdParent { get; set; }
}
}