﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class CompanyLock
    {
		/// <summary>
		/// The Company Lock's class name
		/// </summary>
		public const string CLASS_NAME = "CompanyLock";
		/// <summary>
		/// The CompanyLock's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The CompanyLock's UserId property
		/// </summary>
		public const string PROPERTY_USER_ID = "UserId";
		/// <summary>
		/// The CompanyLock's CompanyId property
		/// </summary>
		public const string PROPERTY_COMPANY_ID = "CompanyId";
		/// <summary>
		/// The CompanyLock's Timestamp property
		/// </summary>
		public const string PROPERTY_TIMESTAMP = "Timestamp";

		public int Id { get; set; }
		public int UserId { get; set; }
		public int CompanyId { get; set; }
		public DateTime Timestamp { get; set; }

		public User UserIdParent { get; set; }
		public Company CompanyIdParent { get; set; }
}
}