﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class Country
    {
		/// <summary>
		/// The Country's class name
		/// </summary>
		public const string CLASS_NAME = "Country";
		/// <summary>
		/// The Country's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The Country's Name property
		/// </summary>
		public const string PROPERTY_NAME = "Name";

		public int Id { get; set; }
		public string Name { get; set; }

        public List<State> StateList { get; set; }
	}
}