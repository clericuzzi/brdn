﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class CompanyDuplicationContact
    {
		/// <summary>
		/// The  Company Duplication Contact's class name
		/// </summary>
		public const string CLASS_NAME = "CompanyDuplicationContact";
		/// <summary>
		/// The CompanyDuplicationContact's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The CompanyDuplicationContact's UserId property
		/// </summary>
		public const string PROPERTY_USER_ID = "UserId";
		/// <summary>
		/// The CompanyDuplicationContact's CompanyId property
		/// </summary>
		public const string PROPERTY_COMPANY_ID = "CompanyId";
		/// <summary>
		/// The CompanyDuplicationContact's PositionId property
		/// </summary>
		public const string PROPERTY_POSITION_ID = "PositionId";
		/// <summary>
		/// The CompanyDuplicationContact's DepartmentId property
		/// </summary>
		public const string PROPERTY_DEPARTMENT_ID = "DepartmentId";
		/// <summary>
		/// The CompanyDuplicationContact's Name property
		/// </summary>
		public const string PROPERTY_NAME = "Name";
		/// <summary>
		/// The CompanyDuplicationContact's Email property
		/// </summary>
		public const string PROPERTY_EMAIL = "Email";
		/// <summary>
		/// The CompanyDuplicationContact's Timestamp property
		/// </summary>
		public const string PROPERTY_TIMESTAMP = "Timestamp";

		public int Id { get; set; }
		public int UserId { get; set; }
		public int CompanyId { get; set; }
		public int PositionId { get; set; }
		public int DepartmentId { get; set; }
		public string Name { get; set; }
		public string Email { get; set; }
		public DateTime Timestamp { get; set; }
}
}