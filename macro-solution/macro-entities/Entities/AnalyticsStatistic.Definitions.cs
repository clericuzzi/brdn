﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class AnalyticsStatistic
    {
		/// <summary>
		/// The Analytics Statistic's class name
		/// </summary>
		public const string CLASS_NAME = "AnalyticsStatistic";
		/// <summary>
		/// The AnalyticsStatistic's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The AnalyticsStatistic's TypeId property
		/// </summary>
		public const string PROPERTY_TYPE_ID = "TypeId";
		/// <summary>
		/// The AnalyticsStatistic's Name property
		/// </summary>
		public const string PROPERTY_NAME = "Name";

		public int Id { get; set; }
		public int TypeId { get; set; }
		public string Name { get; set; }

		public AnalyticsStatisticType TypeIdParent { get; set; }

        public List<AnalyticsStats> AnalyticsStatsList { get; set; }
	}
}