﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class ImportFileCompany
    {
		/// <summary>
		/// The Import File Company's class name
		/// </summary>
		public const string CLASS_NAME = "ImportFileCompany";
		/// <summary>
		/// The ImportFileCompany's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The ImportFileCompany's ImportFileId property
		/// </summary>
		public const string PROPERTY_IMPORT_FILE_ID = "ImportFileId";
		/// <summary>
		/// The ImportFileCompany's CompanyId property
		/// </summary>
		public const string PROPERTY_COMPANY_ID = "CompanyId";

		public int Id { get; set; }
		public int ImportFileId { get; set; }
		public int CompanyId { get; set; }

		public ImportFile ImportFileIdParent { get; set; }
		public Company CompanyIdParent { get; set; }
}
}