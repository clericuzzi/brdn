﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class MacroMachineConfig
    {
		/// <summary>
		/// The Configuração de mineração's class name
		/// </summary>
		public const string CLASS_NAME = "MacroMachineConfig";
		/// <summary>
		/// The MacroMachineConfig's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The MacroMachineConfig's MachineId property
		/// </summary>
		public const string PROPERTY_MACHINE_ID = "MachineId";
		/// <summary>
		/// The MacroMachineConfig's MacroId property
		/// </summary>
		public const string PROPERTY_MACRO_ID = "MacroId";
		/// <summary>
		/// The MacroMachineConfig's Instances property
		/// </summary>
		public const string PROPERTY_INSTANCES = "Instances";
		/// <summary>
		/// The MacroMachineConfig's Starting property
		/// </summary>
		public const string PROPERTY_STARTING = "Starting";
		/// <summary>
		/// The MacroMachineConfig's Ending property
		/// </summary>
		public const string PROPERTY_ENDING = "Ending";
		/// <summary>
		/// The MacroMachineConfig's IsAlive property
		/// </summary>
		public const string PROPERTY_IS_ALIVE = "IsAlive";
		/// <summary>
		/// The MacroMachineConfig's TurnOffAfter property
		/// </summary>
		public const string PROPERTY_TURN_OFF_AFTER = "TurnOffAfter";
		/// <summary>
		/// The MacroMachineConfig's FromBackground property
		/// </summary>
		public const string PROPERTY_FROM_BACKGROUND = "FromBackground";

		public int Id { get; set; }
		public int MachineId { get; set; }
		public int MacroId { get; set; }
		public int Instances { get; set; }
		public int? Starting { get; set; }
		public int? Ending { get; set; }
		public int IsAlive { get; set; }
		public int TurnOffAfter { get; set; }
		public int FromBackground { get; set; }

		public MacroMachine MachineIdParent { get; set; }
		public MacroLogin MacroIdParent { get; set; }
}
}