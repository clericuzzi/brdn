﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class ActivityItemLog
    {
		/// <summary>
		/// The Activity Item Log's class name
		/// </summary>
		public const string CLASS_NAME = "ActivityItemLog";
		/// <summary>
		/// The ActivityItemLog's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The ActivityItemLog's ActivityId property
		/// </summary>
		public const string PROPERTY_ACTIVITY_ID = "ActivityId";
		/// <summary>
		/// The ActivityItemLog's UserId property
		/// </summary>
		public const string PROPERTY_USER_ID = "UserId";
		/// <summary>
		/// The ActivityItemLog's Action property
		/// </summary>
		public const string PROPERTY_ACTION = "Action";

		public int Id { get; set; }
		public int ActivityId { get; set; }
		public int UserId { get; set; }
		public int Action { get; set; }

		public Activity ActivityIdParent { get; set; }
		public User UserIdParent { get; set; }
}
}