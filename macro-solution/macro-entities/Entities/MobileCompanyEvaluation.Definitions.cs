﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class MobileCompanyEvaluation
    {
		/// <summary>
		/// The Mobile Company Evaluation's class name
		/// </summary>
		public const string CLASS_NAME = "MobileCompanyEvaluation";
		/// <summary>
		/// The MobileCompanyEvaluation's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The MobileCompanyEvaluation's MobileCompanyId property
		/// </summary>
		public const string PROPERTY_MOBILE_COMPANY_ID = "MobileCompanyId";
		/// <summary>
		/// The MobileCompanyEvaluation's UserId property
		/// </summary>
		public const string PROPERTY_USER_ID = "UserId";
		/// <summary>
		/// The MobileCompanyEvaluation's Evaluation property
		/// </summary>
		public const string PROPERTY_EVALUATION = "Evaluation";
		/// <summary>
		/// The MobileCompanyEvaluation's Timestamp property
		/// </summary>
		public const string PROPERTY_TIMESTAMP = "Timestamp";

		public int Id { get; set; }
		public int MobileCompanyId { get; set; }
		public int UserId { get; set; }
		public int? Evaluation { get; set; }
		public DateTime Timestamp { get; set; }

		public MobileCompany MobileCompanyIdParent { get; set; }
		public User UserIdParent { get; set; }
}
}