﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class MobileCompanyContact
    {
		/// <summary>
		/// The Mobile Company Contact's class name
		/// </summary>
		public const string CLASS_NAME = "MobileCompanyContact";
		/// <summary>
		/// The MobileCompanyContact's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The MobileCompanyContact's MobileCompanyId property
		/// </summary>
		public const string PROPERTY_MOBILE_COMPANY_ID = "MobileCompanyId";
		/// <summary>
		/// The MobileCompanyContact's Contact property
		/// </summary>
		public const string PROPERTY_CONTACT = "Contact";

		public int Id { get; set; }
		public int MobileCompanyId { get; set; }
		public string Contact { get; set; }

		public MobileCompany MobileCompanyIdParent { get; set; }
}
}