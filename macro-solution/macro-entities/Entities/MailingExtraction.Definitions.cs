﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class MailingExtraction
    {
		/// <summary>
		/// The Mailing Extraction's class name
		/// </summary>
		public const string CLASS_NAME = "MailingExtraction";
		/// <summary>
		/// The MailingExtraction's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The MailingExtraction's Name property
		/// </summary>
		public const string PROPERTY_NAME = "Name";
		/// <summary>
		/// The MailingExtraction's States property
		/// </summary>
		public const string PROPERTY_STATES = "States";
		/// <summary>
		/// The MailingExtraction's Cities property
		/// </summary>
		public const string PROPERTY_CITIES = "Cities";
		/// <summary>
		/// The MailingExtraction's Neighbourhoods property
		/// </summary>
		public const string PROPERTY_NEIGHBOURHOODS = "Neighbourhoods";
		/// <summary>
		/// The MailingExtraction's Done property
		/// </summary>
		public const string PROPERTY_DONE = "Done";

		public int Id { get; set; }
		public string Name { get; set; }
		public string States { get; set; }
		public string Cities { get; set; }
		public string Neighbourhoods { get; set; }
		public int Done { get; set; }

        public List<MailingExtractionZipcode> MailingExtractionZipcodeList { get; set; }
	}
}