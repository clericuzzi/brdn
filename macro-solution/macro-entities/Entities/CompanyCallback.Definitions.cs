﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class CompanyCallback
    {
		/// <summary>
		/// The Company Callback's class name
		/// </summary>
		public const string CLASS_NAME = "CompanyCallback";
		/// <summary>
		/// The CompanyCallback's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The CompanyCallback's UserId property
		/// </summary>
		public const string PROPERTY_USER_ID = "UserId";
		/// <summary>
		/// The CompanyCallback's CompanyId property
		/// </summary>
		public const string PROPERTY_COMPANY_ID = "CompanyId";
		/// <summary>
		/// The CompanyCallback's ContactId property
		/// </summary>
		public const string PROPERTY_CONTACT_ID = "ContactId";
		/// <summary>
		/// The CompanyCallback's CallTime property
		/// </summary>
		public const string PROPERTY_CALL_TIME = "CallTime";
		/// <summary>
		/// The CompanyCallback's Done property
		/// </summary>
		public const string PROPERTY_DONE = "Done";
		/// <summary>
		/// The CompanyCallback's Timestamp property
		/// </summary>
		public const string PROPERTY_TIMESTAMP = "Timestamp";

		public int Id { get; set; }
		public int UserId { get; set; }
		public int CompanyId { get; set; }
		public int ContactId { get; set; }
		public DateTime CallTime { get; set; }
		public int Done { get; set; }
		public DateTime Timestamp { get; set; }

		public User UserIdParent { get; set; }
		public Company CompanyIdParent { get; set; }
		public CompanyContact ContactIdParent { get; set; }
}
}