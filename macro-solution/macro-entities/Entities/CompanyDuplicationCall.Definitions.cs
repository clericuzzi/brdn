﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class CompanyDuplicationCall
    {
		/// <summary>
		/// The  Company Duplication Call's class name
		/// </summary>
		public const string CLASS_NAME = "CompanyDuplicationCall";
		/// <summary>
		/// The CompanyDuplicationCall's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The CompanyDuplicationCall's UserId property
		/// </summary>
		public const string PROPERTY_USER_ID = "UserId";
		/// <summary>
		/// The CompanyDuplicationCall's CompanyId property
		/// </summary>
		public const string PROPERTY_COMPANY_ID = "CompanyId";
		/// <summary>
		/// The CompanyDuplicationCall's ContactId property
		/// </summary>
		public const string PROPERTY_CONTACT_ID = "ContactId";
		/// <summary>
		/// The CompanyDuplicationCall's TabulationId property
		/// </summary>
		public const string PROPERTY_TABULATION_ID = "TabulationId";
		/// <summary>
		/// The CompanyDuplicationCall's StartedIn property
		/// </summary>
		public const string PROPERTY_STARTED_IN = "StartedIn";
		/// <summary>
		/// The CompanyDuplicationCall's EndedIn property
		/// </summary>
		public const string PROPERTY_ENDED_IN = "EndedIn";

		public int Id { get; set; }
		public int UserId { get; set; }
		public int CompanyId { get; set; }
		public int? ContactId { get; set; }
		public int? TabulationId { get; set; }
		public DateTime StartedIn { get; set; }
		public DateTime? EndedIn { get; set; }
}
}