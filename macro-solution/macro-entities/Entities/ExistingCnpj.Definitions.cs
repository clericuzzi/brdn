﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class ExistingCnpj
    {
		/// <summary>
		/// The  Existing Cnpj's class name
		/// </summary>
		public const string CLASS_NAME = "ExistingCnpj";
		/// <summary>
		/// The ExistingCnpj's Cnpj property
		/// </summary>
		public const string PROPERTY_CNPJ = "Cnpj";

		public string Cnpj { get; set; }
}
}