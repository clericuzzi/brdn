﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class CompanyActiveService
    {
        public CompanyActiveService()
        {
        }
        public CompanyActiveService(List<string> values)
        {
            var i = 0;
            Instance = _ValidatePosition(values, i++);
            ServiceAddress = _ValidatePosition(values, i++);
            AssociatedProduct = _ValidatePosition(values, i++);
            Status = _ValidatePosition(values, i++);
            Order = _ValidatePosition(values, i++);
            Offer = _ValidatePosition(values, i++);
            Invalid = _ValidatePosition(values, i++);
            TecnologyVoice = _ValidatePosition(values, i++);
            TecnologyAccess = _ValidatePosition(values, i++);
            ChargeProfile = _ValidatePosition(values, i++);
            CreatedId = _ValidatePosition(values, i++);
            ActivationDate = _ValidatePosition(values, i++);
            Benefit = _ValidatePosition(values, i++);
            Portability = _ValidatePosition(values, i++);
            Rpon = _ValidatePosition(values, i++);
            Tv = _ValidatePosition(values, i++);
            Ficticious = _ValidatePosition(values, i++);
            DoubleAccess = _ValidatePosition(values, i++);
            Qtd = _ValidatePosition(values, i++);
            Product = _ValidatePosition(values, i++);
        }
        private string _ValidatePosition(List<string> values, int index)
        {
            if (values.Count > index)
                return values[index];
            else
                return null;
        }
    }
}