﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class Log
    {
		/// <summary>
		/// The Log's class name
		/// </summary>
		public const string CLASS_NAME = "Log";
		/// <summary>
		/// The Log's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The Log's UserId property
		/// </summary>
		public const string PROPERTY_USER_ID = "UserId";
		/// <summary>
		/// The Log's TargetId property
		/// </summary>
		public const string PROPERTY_TARGET_ID = "TargetId";
		/// <summary>
		/// The Log's Action property
		/// </summary>
		public const string PROPERTY_ACTION = "Action";
		/// <summary>
		/// The Log's Timestamp property
		/// </summary>
		public const string PROPERTY_TIMESTAMP = "Timestamp";

		public int Id { get; set; }
		public int UserId { get; set; }
		public int TargetId { get; set; }
		public string Action { get; set; }
		public DateTime Timestamp { get; set; }

		public User UserIdParent { get; set; }
}
}