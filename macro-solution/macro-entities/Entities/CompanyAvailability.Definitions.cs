﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class CompanyAvailability
    {
		/// <summary>
		/// The Company Availability's class name
		/// </summary>
		public const string CLASS_NAME = "CompanyAvailability";
		/// <summary>
		/// The CompanyAvailability's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The CompanyAvailability's CompanyAddressId property
		/// </summary>
		public const string PROPERTY_COMPANY_ADDRESS_ID = "CompanyAddressId";
		/// <summary>
		/// The CompanyAvailability's Message property
		/// </summary>
		public const string PROPERTY_MESSAGE = "Message";
		/// <summary>
		/// The CompanyAvailability's ClosetDistance property
		/// </summary>
		public const string PROPERTY_CLOSET_DISTANCE = "ClosetDistance";
		/// <summary>
		/// The CompanyAvailability's Lines property
		/// </summary>
		public const string PROPERTY_LINES = "Lines";
		/// <summary>
		/// The CompanyAvailability's Until20 property
		/// </summary>
		public const string PROPERTY_UNTIL_20 = "Until20";
		/// <summary>
		/// The CompanyAvailability's Until20Max property
		/// </summary>
		public const string PROPERTY_UNTIL_20_MAX = "Until20Max";
		/// <summary>
		/// The CompanyAvailability's Until20Sip property
		/// </summary>
		public const string PROPERTY_UNTIL_20_SIP = "Until20Sip";
		/// <summary>
		/// The CompanyAvailability's From21UpTo50 property
		/// </summary>
		public const string PROPERTY_FROM_21_UP_TO_50 = "From21UpTo50";
		/// <summary>
		/// The CompanyAvailability's From21UpTo50Max property
		/// </summary>
		public const string PROPERTY_FROM_21_UP_TO_50_MAX = "From21UpTo50Max";
		/// <summary>
		/// The CompanyAvailability's From21UpTo50Sip property
		/// </summary>
		public const string PROPERTY_FROM_21_UP_TO_50_SIP = "From21UpTo50Sip";
		/// <summary>
		/// The CompanyAvailability's Over50 property
		/// </summary>
		public const string PROPERTY_OVER_50 = "Over50";
		/// <summary>
		/// The CompanyAvailability's Over50Max property
		/// </summary>
		public const string PROPERTY_OVER_50_MAX = "Over50Max";
		/// <summary>
		/// The CompanyAvailability's Over50Sip property
		/// </summary>
		public const string PROPERTY_OVER_50_SIP = "Over50Sip";
		/// <summary>
		/// The CompanyAvailability's Network property
		/// </summary>
		public const string PROPERTY_NETWORK = "Network";
		/// <summary>
		/// The CompanyAvailability's Tecnology property
		/// </summary>
		public const string PROPERTY_TECNOLOGY = "Tecnology";
		/// <summary>
		/// The CompanyAvailability's ClosetTopSpeed property
		/// </summary>
		public const string PROPERTY_CLOSET_TOP_SPEED = "ClosetTopSpeed";
		/// <summary>
		/// The CompanyAvailability's TecnologyForTv property
		/// </summary>
		public const string PROPERTY_TECNOLOGY_FOR_TV = "TecnologyForTv";
		/// <summary>
		/// The CompanyAvailability's Timestamp property
		/// </summary>
		public const string PROPERTY_TIMESTAMP = "Timestamp";

		public int Id { get; set; }
		public int CompanyAddressId { get; set; }
		public string Message { get; set; }
		public int? ClosetDistance { get; set; }
		public int? Lines { get; set; }
		public int? Until20 { get; set; }
		public string Until20Max { get; set; }
		public int? Until20Sip { get; set; }
		public int? From21UpTo50 { get; set; }
		public string From21UpTo50Max { get; set; }
		public int? From21UpTo50Sip { get; set; }
		public int? Over50 { get; set; }
		public string Over50Max { get; set; }
		public int? Over50Sip { get; set; }
		public string Network { get; set; }
		public string Tecnology { get; set; }
		public int? ClosetTopSpeed { get; set; }
		public string TecnologyForTv { get; set; }
		public DateTime Timestamp { get; set; }

		public CompanyAddress CompanyAddressIdParent { get; set; }
}
}