﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class User
    {
		/// <summary>
		/// The User's class name
		/// </summary>
		public const string CLASS_NAME = "User";
		/// <summary>
		/// The User's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The User's Name property
		/// </summary>
		public const string PROPERTY_NAME = "Name";
		/// <summary>
		/// The User's Phone property
		/// </summary>
		public const string PROPERTY_PHONE = "Phone";
		/// <summary>
		/// The User's Email property
		/// </summary>
		public const string PROPERTY_EMAIL = "Email";
		/// <summary>
		/// The User's Password property
		/// </summary>
		public const string PROPERTY_PASSWORD = "Password";
		/// <summary>
		/// The User's Active property
		/// </summary>
		public const string PROPERTY_ACTIVE = "Active";
		/// <summary>
		/// The User's Blocked property
		/// </summary>
		public const string PROPERTY_BLOCKED = "Blocked";
		/// <summary>
		/// The User's Timestamp property
		/// </summary>
		public const string PROPERTY_TIMESTAMP = "Timestamp";

		public int Id { get; set; }
		public string Name { get; set; }
		public string Phone { get; set; }
		public string Email { get; set; }
		public string Password { get; set; }
		public int Active { get; set; }
		public int Blocked { get; set; }
		public DateTime Timestamp { get; set; }

        public List<ActivityAllowedUser> ActivityAllowedUserList { get; set; }
        public List<ActivityItem> ActivityItemList { get; set; }
        public List<ActivityItemLog> ActivityItemLogList { get; set; }
        public List<AnalyticsStats> AnalyticsStatsList { get; set; }
        public List<Company> CompanyList { get; set; }
        public List<CompanyCall> CompanyCallList { get; set; }
        public List<CompanyCallback> CompanyCallbackList { get; set; }
        public List<CompanyContact> CompanyContactList { get; set; }
        public List<CompanyNote> CompanyNoteList { get; set; }
        public List<Concurrency> ConcurrencyList { get; set; }
        public List<ImportFile> ImportFileList { get; set; }
        public List<Lead> LeadList { get; set; }
        public List<Log> LogList { get; set; }
        public List<LogDatasetChange> LogDatasetChangeList { get; set; }
        public List<LogError> LogErrorList { get; set; }
        public List<LogPropertyChange> LogPropertyChangeList { get; set; }
        public List<Sale> SaleList { get; set; }
        public List<UserArea> UserAreaList { get; set; }
        public List<UserPermission> UserPermissionList { get; set; }
        public List<UserWhiteList> UserWhiteListList { get; set; }
	}
}