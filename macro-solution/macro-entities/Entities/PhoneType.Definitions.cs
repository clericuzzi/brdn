﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class PhoneType
    {
		/// <summary>
		/// The Phone Type's class name
		/// </summary>
		public const string CLASS_NAME = "PhoneType";
		/// <summary>
		/// The PhoneType's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The PhoneType's Name property
		/// </summary>
		public const string PROPERTY_NAME = "Name";

		public int Id { get; set; }
		public string Name { get; set; }
}
}