﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class CompanyDuplicationCallback
    {
		/// <summary>
		/// The  Company Duplication Callback's class name
		/// </summary>
		public const string CLASS_NAME = "CompanyDuplicationCallback";
		/// <summary>
		/// The CompanyDuplicationCallback's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The CompanyDuplicationCallback's UserId property
		/// </summary>
		public const string PROPERTY_USER_ID = "UserId";
		/// <summary>
		/// The CompanyDuplicationCallback's CompanyId property
		/// </summary>
		public const string PROPERTY_COMPANY_ID = "CompanyId";
		/// <summary>
		/// The CompanyDuplicationCallback's ContactId property
		/// </summary>
		public const string PROPERTY_CONTACT_ID = "ContactId";
		/// <summary>
		/// The CompanyDuplicationCallback's CallTime property
		/// </summary>
		public const string PROPERTY_CALL_TIME = "CallTime";
		/// <summary>
		/// The CompanyDuplicationCallback's Done property
		/// </summary>
		public const string PROPERTY_DONE = "Done";
		/// <summary>
		/// The CompanyDuplicationCallback's Timestamp property
		/// </summary>
		public const string PROPERTY_TIMESTAMP = "Timestamp";

		public int Id { get; set; }
		public int UserId { get; set; }
		public int CompanyId { get; set; }
		public int ContactId { get; set; }
		public DateTime CallTime { get; set; }
		public int Done { get; set; }
		public DateTime Timestamp { get; set; }
}
}