﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class CompanyLineOperator
    {
		/// <summary>
		/// The Company Line Operator's class name
		/// </summary>
		public const string CLASS_NAME = "CompanyLineOperator";
		/// <summary>
		/// The CompanyLineOperator's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The CompanyLineOperator's CompanyLineId property
		/// </summary>
		public const string PROPERTY_COMPANY_LINE_ID = "CompanyLineId";
		/// <summary>
		/// The CompanyLineOperator's OperatorId property
		/// </summary>
		public const string PROPERTY_OPERATOR_ID = "OperatorId";
		/// <summary>
		/// The CompanyLineOperator's OperatorSince property
		/// </summary>
		public const string PROPERTY_OPERATOR_SINCE = "OperatorSince";

		public int Id { get; set; }
		public int CompanyLineId { get; set; }
		public int? OperatorId { get; set; }
		public DateTime? OperatorSince { get; set; }

		public CompanyLine CompanyLineIdParent { get; set; }
		public PhoneOperator OperatorIdParent { get; set; }
}
}