﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class MobileCompanyImport
    {
		/// <summary>
		/// The Mobile Company Import's class name
		/// </summary>
		public const string CLASS_NAME = "MobileCompanyImport";
		/// <summary>
		/// The MobileCompanyImport's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The MobileCompanyImport's Cnpj property
		/// </summary>
		public const string PROPERTY_CNPJ = "Cnpj";
		/// <summary>
		/// The MobileCompanyImport's Valid property
		/// </summary>
		public const string PROPERTY_VALID = "Valid";
		/// <summary>
		/// The MobileCompanyImport's Filename property
		/// </summary>
		public const string PROPERTY_FILENAME = "Filename";
		/// <summary>
		/// The MobileCompanyImport's Timestamp property
		/// </summary>
		public const string PROPERTY_TIMESTAMP = "Timestamp";
		/// <summary>
		/// The MobileCompanyImport's Priority property
		/// </summary>
		public const string PROPERTY_PRIORITY = "Priority";

		public int Id { get; set; }
		public string Cnpj { get; set; }
		public int Valid { get; set; }
		public string Filename { get; set; }
		public DateTime Timestamp { get; set; }
		public int? Priority { get; set; }

        public List<MobileCompanySource> MobileCompanySourceList { get; set; }
	}
}