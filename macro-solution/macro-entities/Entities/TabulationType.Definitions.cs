﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class TabulationType
    {
		/// <summary>
		/// The Tabulation Type's class name
		/// </summary>
		public const string CLASS_NAME = "TabulationType";
		/// <summary>
		/// The TabulationType's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The TabulationType's Type property
		/// </summary>
		public const string PROPERTY_TYPE = "Type";

		public int Id { get; set; }
		public string Type { get; set; }

        public List<Tabulation> TabulationList { get; set; }
	}
}