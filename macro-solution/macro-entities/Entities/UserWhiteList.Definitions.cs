﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class UserWhiteList
    {
		/// <summary>
		/// The User White List's class name
		/// </summary>
		public const string CLASS_NAME = "UserWhiteList";
		/// <summary>
		/// The UserWhiteList's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The UserWhiteList's UserId property
		/// </summary>
		public const string PROPERTY_USER_ID = "UserId";

		public int Id { get; set; }
		public int UserId { get; set; }

		public User UserIdParent { get; set; }
}
}