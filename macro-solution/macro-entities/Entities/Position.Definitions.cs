﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class Position
    {
		/// <summary>
		/// The Position's class name
		/// </summary>
		public const string CLASS_NAME = "Position";
		/// <summary>
		/// The Position's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The Position's AccountId property
		/// </summary>
		public const string PROPERTY_ACCOUNT_ID = "AccountId";
		/// <summary>
		/// The Position's Name property
		/// </summary>
		public const string PROPERTY_NAME = "Name";

		public int Id { get; set; }
		public int AccountId { get; set; }
		public string Name { get; set; }

		public Account AccountIdParent { get; set; }

        public List<CompanyContact> CompanyContactList { get; set; }
	}
}