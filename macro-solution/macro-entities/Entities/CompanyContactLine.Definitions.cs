﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class CompanyContactLine
    {
		/// <summary>
		/// The Company Contact Line's class name
		/// </summary>
		public const string CLASS_NAME = "CompanyContactLine";
		/// <summary>
		/// The CompanyContactLine's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The CompanyContactLine's CompanyContactId property
		/// </summary>
		public const string PROPERTY_COMPANY_CONTACT_ID = "CompanyContactId";
		/// <summary>
		/// The CompanyContactLine's CompanyLineId property
		/// </summary>
		public const string PROPERTY_COMPANY_LINE_ID = "CompanyLineId";

		public int Id { get; set; }
		public int CompanyContactId { get; set; }
		public int CompanyLineId { get; set; }

		public CompanyContact CompanyContactIdParent { get; set; }
		public CompanyLine CompanyLineIdParent { get; set; }
}
}