﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class Characteristic
    {
		/// <summary>
		/// The Characteristic's class name
		/// </summary>
		public const string CLASS_NAME = "Characteristic";
		/// <summary>
		/// The Characteristic's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The Characteristic's AccountId property
		/// </summary>
		public const string PROPERTY_ACCOUNT_ID = "AccountId";
		/// <summary>
		/// The Characteristic's Name property
		/// </summary>
		public const string PROPERTY_NAME = "Name";

		public int Id { get; set; }
		public int AccountId { get; set; }
		public string Name { get; set; }

		public Account AccountIdParent { get; set; }

        public List<ActivityCharacteristicMust> ActivityCharacteristicMustList { get; set; }
        public List<ActivityCharacteristicMustNot> ActivityCharacteristicMustNotList { get; set; }
        public List<ActivityCharacteristicOnConclusionAwarded> ActivityCharacteristicOnConclusionAwardedList { get; set; }
        public List<ActivityCharacteristicOnConclusionRevoked> ActivityCharacteristicOnConclusionRevokedList { get; set; }
        public List<CharacteristicAssociation> CharacteristicAssociationList { get; set; }
        public List<CharacteristicAssociationLog> CharacteristicAssociationLogList { get; set; }
	}
}