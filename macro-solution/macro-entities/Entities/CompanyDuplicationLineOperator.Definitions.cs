﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class CompanyDuplicationLineOperator
    {
		/// <summary>
		/// The  Company Duplication Line Operator's class name
		/// </summary>
		public const string CLASS_NAME = "CompanyDuplicationLineOperator";
		/// <summary>
		/// The CompanyDuplicationLineOperator's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The CompanyDuplicationLineOperator's CompanyLineId property
		/// </summary>
		public const string PROPERTY_COMPANY_LINE_ID = "CompanyLineId";
		/// <summary>
		/// The CompanyDuplicationLineOperator's OperatorId property
		/// </summary>
		public const string PROPERTY_OPERATOR_ID = "OperatorId";
		/// <summary>
		/// The CompanyDuplicationLineOperator's OperatorSince property
		/// </summary>
		public const string PROPERTY_OPERATOR_SINCE = "OperatorSince";

		public int Id { get; set; }
		public int CompanyLineId { get; set; }
		public int? OperatorId { get; set; }
		public DateTime? OperatorSince { get; set; }
}
}