﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class CompanyComment
    {
		/// <summary>
		/// The Company Comment's class name
		/// </summary>
		public const string CLASS_NAME = "CompanyComment";
		/// <summary>
		/// The CompanyComment's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The CompanyComment's CompanyId property
		/// </summary>
		public const string PROPERTY_COMPANY_ID = "CompanyId";
		/// <summary>
		/// The CompanyComment's UserId property
		/// </summary>
		public const string PROPERTY_USER_ID = "UserId";
		/// <summary>
		/// The CompanyComment's Comment property
		/// </summary>
		public const string PROPERTY_COMMENT = "Comment";
		/// <summary>
		/// The CompanyComment's Timestamp property
		/// </summary>
		public const string PROPERTY_TIMESTAMP = "Timestamp";

		public int Id { get; set; }
		public int CompanyId { get; set; }
		public int UserId { get; set; }
		public string Comment { get; set; }
		public DateTime Timestamp { get; set; }

		public Company CompanyIdParent { get; set; }
		public User UserIdParent { get; set; }
}
}