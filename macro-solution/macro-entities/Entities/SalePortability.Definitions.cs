﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class SalePortability
    {
		/// <summary>
		/// The Sale Portability's class name
		/// </summary>
		public const string CLASS_NAME = "SalePortability";
		/// <summary>
		/// The SalePortability's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The SalePortability's SaleId property
		/// </summary>
		public const string PROPERTY_SALE_ID = "SaleId";
		/// <summary>
		/// The SalePortability's Ddd property
		/// </summary>
		public const string PROPERTY_DDD = "Ddd";
		/// <summary>
		/// The SalePortability's Phone property
		/// </summary>
		public const string PROPERTY_PHONE = "Phone";

		public int Id { get; set; }
		public int SaleId { get; set; }
		public string Ddd { get; set; }
		public string Phone { get; set; }

		public Sale SaleIdParent { get; set; }
}
}