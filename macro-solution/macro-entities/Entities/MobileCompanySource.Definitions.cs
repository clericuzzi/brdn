﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class MobileCompanySource
    {
		/// <summary>
		/// The Mobile Company Source's class name
		/// </summary>
		public const string CLASS_NAME = "MobileCompanySource";
		/// <summary>
		/// The MobileCompanySource's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The MobileCompanySource's MobileCompanyImportId property
		/// </summary>
		public const string PROPERTY_MOBILE_COMPANY_IMPORT_ID = "MobileCompanyImportId";
		/// <summary>
		/// The MobileCompanySource's Priority property
		/// </summary>
		public const string PROPERTY_PRIORITY = "Priority";

		public int Id { get; set; }
		public int MobileCompanyImportId { get; set; }
		public int? Priority { get; set; }

		public MobileCompanyImport MobileCompanyImportIdParent { get; set; }
}
}