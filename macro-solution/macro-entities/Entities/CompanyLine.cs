﻿using Clericuzzi.Lib.Utils.Core;
using System;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class CompanyLine
    {
        public string StringValue
        {
            get
            {
                return Phone.DigitsOnly();
            }
        }
        public bool IsProblematic
        {
            get
            {
                var zeroOut = StringValue.Replace("0", string.Empty);
                var localStringValue = Phone;
                var isLessThanLimit = StringValue.Length < 10;
                var isGreaterThanLimit = StringValue.Length > 11;

                return isGreaterThanLimit || isLessThanLimit || string.IsNullOrEmpty(zeroOut) || !StringValue.Equals(localStringValue);
            }
        }
        public bool IsInvalid
        {
            get
            {
                var isZeroOut = StringValue.Replace("0", string.Empty).Length == 0;
                var isLessThanLimit = StringValue.Length < 10;
                var isGreaterThanLimit = StringValue.Length > 11;

                return isGreaterThanLimit || isLessThanLimit || isZeroOut;
            }
        }
    }
}