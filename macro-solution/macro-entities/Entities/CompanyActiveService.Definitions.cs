﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class CompanyActiveService
    {
		/// <summary>
		/// The Company Active Service's class name
		/// </summary>
		public const string CLASS_NAME = "CompanyActiveService";
		/// <summary>
		/// The CompanyActiveService's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The CompanyActiveService's CompanyId property
		/// </summary>
		public const string PROPERTY_COMPANY_ID = "CompanyId";
		/// <summary>
		/// The CompanyActiveService's Instance property
		/// </summary>
		public const string PROPERTY_INSTANCE = "Instance";
		/// <summary>
		/// The CompanyActiveService's ServiceAddress property
		/// </summary>
		public const string PROPERTY_SERVICE_ADDRESS = "ServiceAddress";
		/// <summary>
		/// The CompanyActiveService's AssociatedProduct property
		/// </summary>
		public const string PROPERTY_ASSOCIATED_PRODUCT = "AssociatedProduct";
		/// <summary>
		/// The CompanyActiveService's Status property
		/// </summary>
		public const string PROPERTY_STATUS = "Status";
		/// <summary>
		/// The CompanyActiveService's Order property
		/// </summary>
		public const string PROPERTY_ORDER = "Order";
		/// <summary>
		/// The CompanyActiveService's Offer property
		/// </summary>
		public const string PROPERTY_OFFER = "Offer";
		/// <summary>
		/// The CompanyActiveService's Invalid property
		/// </summary>
		public const string PROPERTY_INVALID = "Invalid";
		/// <summary>
		/// The CompanyActiveService's TecnologyVoice property
		/// </summary>
		public const string PROPERTY_TECNOLOGY_VOICE = "TecnologyVoice";
		/// <summary>
		/// The CompanyActiveService's TecnologyAccess property
		/// </summary>
		public const string PROPERTY_TECNOLOGY_ACCESS = "TecnologyAccess";
		/// <summary>
		/// The CompanyActiveService's ChargeProfile property
		/// </summary>
		public const string PROPERTY_CHARGE_PROFILE = "ChargeProfile";
		/// <summary>
		/// The CompanyActiveService's CreatedId property
		/// </summary>
		public const string PROPERTY_CREATED_ID = "CreatedId";
		/// <summary>
		/// The CompanyActiveService's ActivationDate property
		/// </summary>
		public const string PROPERTY_ACTIVATION_DATE = "ActivationDate";
		/// <summary>
		/// The CompanyActiveService's Benefit property
		/// </summary>
		public const string PROPERTY_BENEFIT = "Benefit";
		/// <summary>
		/// The CompanyActiveService's Portability property
		/// </summary>
		public const string PROPERTY_PORTABILITY = "Portability";
		/// <summary>
		/// The CompanyActiveService's Rpon property
		/// </summary>
		public const string PROPERTY_RPON = "Rpon";
		/// <summary>
		/// The CompanyActiveService's Tv property
		/// </summary>
		public const string PROPERTY_TV = "Tv";
		/// <summary>
		/// The CompanyActiveService's Ficticious property
		/// </summary>
		public const string PROPERTY_FICTICIOUS = "Ficticious";
		/// <summary>
		/// The CompanyActiveService's DoubleAccess property
		/// </summary>
		public const string PROPERTY_DOUBLE_ACCESS = "DoubleAccess";
		/// <summary>
		/// The CompanyActiveService's Qtd property
		/// </summary>
		public const string PROPERTY_QTD = "Qtd";
		/// <summary>
		/// The CompanyActiveService's Product property
		/// </summary>
		public const string PROPERTY_PRODUCT = "Product";

		public int Id { get; set; }
		public int CompanyId { get; set; }
		public string Instance { get; set; }
		public string ServiceAddress { get; set; }
		public string AssociatedProduct { get; set; }
		public string Status { get; set; }
		public string Order { get; set; }
		public string Offer { get; set; }
		public string Invalid { get; set; }
		public string TecnologyVoice { get; set; }
		public string TecnologyAccess { get; set; }
		public string ChargeProfile { get; set; }
		public string CreatedId { get; set; }
		public string ActivationDate { get; set; }
		public string Benefit { get; set; }
		public string Portability { get; set; }
		public string Rpon { get; set; }
		public string Tv { get; set; }
		public string Ficticious { get; set; }
		public string DoubleAccess { get; set; }
		public string Qtd { get; set; }
		public string Product { get; set; }

		public Company CompanyIdParent { get; set; }
}
}