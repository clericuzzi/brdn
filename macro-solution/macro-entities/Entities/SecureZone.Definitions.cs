﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class SecureZone
    {
		/// <summary>
		/// The Secure Zone's class name
		/// </summary>
		public const string CLASS_NAME = "SecureZone";
		/// <summary>
		/// The SecureZone's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The SecureZone's AccountId property
		/// </summary>
		public const string PROPERTY_ACCOUNT_ID = "AccountId";
		/// <summary>
		/// The SecureZone's Name property
		/// </summary>
		public const string PROPERTY_NAME = "Name";
		/// <summary>
		/// The SecureZone's Latitude property
		/// </summary>
		public const string PROPERTY_LATITUDE = "Latitude";
		/// <summary>
		/// The SecureZone's Longitude property
		/// </summary>
		public const string PROPERTY_LONGITUDE = "Longitude";

		public int Id { get; set; }
		public int AccountId { get; set; }
		public string Name { get; set; }
		public double Latitude { get; set; }
		public double Longitude { get; set; }

		public Account AccountIdParent { get; set; }
}
}