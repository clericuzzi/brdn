﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class CompanyAddress
    {
		/// <summary>
		/// The Company Address's class name
		/// </summary>
		public const string CLASS_NAME = "CompanyAddress";
		/// <summary>
		/// The CompanyAddress's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The CompanyAddress's CompanyId property
		/// </summary>
		public const string PROPERTY_COMPANY_ID = "CompanyId";
		/// <summary>
		/// The CompanyAddress's Street property
		/// </summary>
		public const string PROPERTY_STREET = "Street";
		/// <summary>
		/// The CompanyAddress's Number property
		/// </summary>
		public const string PROPERTY_NUMBER = "Number";
		/// <summary>
		/// The CompanyAddress's Compliment property
		/// </summary>
		public const string PROPERTY_COMPLIMENT = "Compliment";
		/// <summary>
		/// The CompanyAddress's ZipcodeId property
		/// </summary>
		public const string PROPERTY_ZIPCODE_ID = "ZipcodeId";
		/// <summary>
		/// The CompanyAddress's Reference property
		/// </summary>
		public const string PROPERTY_REFERENCE = "Reference";
		/// <summary>
		/// The CompanyAddress's TestingTimestamp property
		/// </summary>
		public const string PROPERTY_TESTING_TIMESTAMP = "TestingTimestamp";
		/// <summary>
		/// The CompanyAddress's IsValid property
		/// </summary>
		public const string PROPERTY_IS_VALID = "IsValid";

		public int Id { get; set; }
		public int CompanyId { get; set; }
		public string Street { get; set; }
		public string Number { get; set; }
		public string Compliment { get; set; }
		public int? ZipcodeId { get; set; }
		public string Reference { get; set; }
		public DateTime? TestingTimestamp { get; set; }
		public int? IsValid { get; set; }

		public Company CompanyIdParent { get; set; }
		public GeographyZipcode ZipcodeIdParent { get; set; }

        public List<CompanyAvailability> CompanyAvailabilityList { get; set; }
	}
}