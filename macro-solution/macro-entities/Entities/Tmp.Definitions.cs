﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class Tmp
    {
		/// <summary>
		/// The  Tmp's class name
		/// </summary>
		public const string CLASS_NAME = "Tmp";
		/// <summary>
		/// The Tmp's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The Tmp's AccountId property
		/// </summary>
		public const string PROPERTY_ACCOUNT_ID = "AccountId";
		/// <summary>
		/// The Tmp's CompanySubtypeId property
		/// </summary>
		public const string PROPERTY_COMPANY_SUBTYPE_ID = "CompanySubtypeId";
		/// <summary>
		/// The Tmp's Cnpj property
		/// </summary>
		public const string PROPERTY_CNPJ = "Cnpj";
		/// <summary>
		/// The Tmp's Site property
		/// </summary>
		public const string PROPERTY_SITE = "Site";
		/// <summary>
		/// The Tmp's Ddd property
		/// </summary>
		public const string PROPERTY_DDD = "Ddd";
		/// <summary>
		/// The Tmp's Phone property
		/// </summary>
		public const string PROPERTY_PHONE = "Phone";
		/// <summary>
		/// The Tmp's Description property
		/// </summary>
		public const string PROPERTY_DESCRIPTION = "Description";
		/// <summary>
		/// The Tmp's FormalName property
		/// </summary>
		public const string PROPERTY_FORMAL_NAME = "FormalName";
		/// <summary>
		/// The Tmp's FantasyName property
		/// </summary>
		public const string PROPERTY_FANTASY_NAME = "FantasyName";
		/// <summary>
		/// The Tmp's StateRegistry property
		/// </summary>
		public const string PROPERTY_STATE_REGISTRY = "StateRegistry";
		/// <summary>
		/// The Tmp's ImagesUrl property
		/// </summary>
		public const string PROPERTY_IMAGES_URL = "ImagesUrl";
		/// <summary>
		/// The Tmp's Latitude property
		/// </summary>
		public const string PROPERTY_LATITUDE = "Latitude";
		/// <summary>
		/// The Tmp's Longitude property
		/// </summary>
		public const string PROPERTY_LONGITUDE = "Longitude";
		/// <summary>
		/// The Tmp's Timestamp property
		/// </summary>
		public const string PROPERTY_TIMESTAMP = "Timestamp";
		/// <summary>
		/// The Tmp's CnpjTestingTimestamp property
		/// </summary>
		public const string PROPERTY_CNPJ_TESTING_TIMESTAMP = "CnpjTestingTimestamp";
		/// <summary>
		/// The Tmp's Rank property
		/// </summary>
		public const string PROPERTY_RANK = "Rank";
		/// <summary>
		/// The Tmp's DateOpened property
		/// </summary>
		public const string PROPERTY_DATE_OPENED = "DateOpened";
		/// <summary>
		/// The Tmp's SituationId property
		/// </summary>
		public const string PROPERTY_SITUATION_ID = "SituationId";
		/// <summary>
		/// The Tmp's SalesForceId property
		/// </summary>
		public const string PROPERTY_SALES_FORCE_ID = "SalesForceId";
		/// <summary>
		/// The Tmp's CnpjCandidates property
		/// </summary>
		public const string PROPERTY_CNPJ_CANDIDATES = "CnpjCandidates";
		/// <summary>
		/// The Tmp's DateLastUpdate property
		/// </summary>
		public const string PROPERTY_DATE_LAST_UPDATE = "DateLastUpdate";

		public int Id { get; set; }
		public int AccountId { get; set; }
		public int? CompanySubtypeId { get; set; }
		public string Cnpj { get; set; }
		public string Site { get; set; }
		public string Ddd { get; set; }
		public string Phone { get; set; }
		public string Description { get; set; }
		public string FormalName { get; set; }
		public string FantasyName { get; set; }
		public string StateRegistry { get; set; }
		public string ImagesUrl { get; set; }
		public double? Latitude { get; set; }
		public double? Longitude { get; set; }
		public DateTime Timestamp { get; set; }
		public DateTime? CnpjTestingTimestamp { get; set; }
		public string Rank { get; set; }
		public DateTime? DateOpened { get; set; }
		public int? SituationId { get; set; }
		public string SalesForceId { get; set; }
		public string CnpjCandidates { get; set; }
		public DateTime? DateLastUpdate { get; set; }
}
}