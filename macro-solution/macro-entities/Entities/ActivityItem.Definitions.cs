﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class ActivityItem
    {
		/// <summary>
		/// The Activity Item's class name
		/// </summary>
		public const string CLASS_NAME = "ActivityItem";
		/// <summary>
		/// The ActivityItem's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The ActivityItem's ActivityId property
		/// </summary>
		public const string PROPERTY_ACTIVITY_ID = "ActivityId";
		/// <summary>
		/// The ActivityItem's UserId property
		/// </summary>
		public const string PROPERTY_USER_ID = "UserId";
		/// <summary>
		/// The ActivityItem's EntityId property
		/// </summary>
		public const string PROPERTY_ENTITY_ID = "EntityId";
		/// <summary>
		/// The ActivityItem's FinishedIn property
		/// </summary>
		public const string PROPERTY_FINISHED_IN = "FinishedIn";
		/// <summary>
		/// The ActivityItem's Result property
		/// </summary>
		public const string PROPERTY_RESULT = "Result";
		/// <summary>
		/// The ActivityItem's Timestamp property
		/// </summary>
		public const string PROPERTY_TIMESTAMP = "Timestamp";

		public int Id { get; set; }
		public int ActivityId { get; set; }
		public int UserId { get; set; }
		public int EntityId { get; set; }
		public DateTime? FinishedIn { get; set; }
		public int? Result { get; set; }
		public DateTime Timestamp { get; set; }

		public Activity ActivityIdParent { get; set; }
		public User UserIdParent { get; set; }
}
}