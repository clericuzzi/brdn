﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class AccountConfig
    {
		/// <summary>
		/// The Account Config's class name
		/// </summary>
		public const string CLASS_NAME = "AccountConfig";
		/// <summary>
		/// The AccountConfig's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The AccountConfig's AccountId property
		/// </summary>
		public const string PROPERTY_ACCOUNT_ID = "AccountId";
		/// <summary>
		/// The AccountConfig's Parameter property
		/// </summary>
		public const string PROPERTY_PARAMETER = "Parameter";
		/// <summary>
		/// The AccountConfig's Value property
		/// </summary>
		public const string PROPERTY_VALUE = "Value";

		public int Id { get; set; }
		public int AccountId { get; set; }
		public string Parameter { get; set; }
		public string Value { get; set; }

		public Account AccountIdParent { get; set; }
}
}