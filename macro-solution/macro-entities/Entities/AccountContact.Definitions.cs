﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class AccountContact
    {
		/// <summary>
		/// The Account Contact's class name
		/// </summary>
		public const string CLASS_NAME = "AccountContact";
		/// <summary>
		/// The AccountContact's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The AccountContact's AccountId property
		/// </summary>
		public const string PROPERTY_ACCOUNT_ID = "AccountId";
		/// <summary>
		/// The AccountContact's Name property
		/// </summary>
		public const string PROPERTY_NAME = "Name";
		/// <summary>
		/// The AccountContact's Email property
		/// </summary>
		public const string PROPERTY_EMAIL = "Email";
		/// <summary>
		/// The AccountContact's Phone property
		/// </summary>
		public const string PROPERTY_PHONE = "Phone";
		/// <summary>
		/// The AccountContact's CelPhone property
		/// </summary>
		public const string PROPERTY_CEL_PHONE = "CelPhone";

		public int Id { get; set; }
		public int AccountId { get; set; }
		public string Name { get; set; }
		public string Email { get; set; }
		public string Phone { get; set; }
		public string CelPhone { get; set; }

		public Account AccountIdParent { get; set; }
}
}