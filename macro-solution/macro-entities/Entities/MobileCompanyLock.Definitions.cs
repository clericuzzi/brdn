﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class MobileCompanyLock
    {
		/// <summary>
		/// The Mobile Company Lock's class name
		/// </summary>
		public const string CLASS_NAME = "MobileCompanyLock";
		/// <summary>
		/// The MobileCompanyLock's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The MobileCompanyLock's UserId property
		/// </summary>
		public const string PROPERTY_USER_ID = "UserId";
		/// <summary>
		/// The MobileCompanyLock's MobileCompanyId property
		/// </summary>
		public const string PROPERTY_MOBILE_COMPANY_ID = "MobileCompanyId";
		/// <summary>
		/// The MobileCompanyLock's Timestamp property
		/// </summary>
		public const string PROPERTY_TIMESTAMP = "Timestamp";

		public int Id { get; set; }
		public int UserId { get; set; }
		public int MobileCompanyId { get; set; }
		public DateTime Timestamp { get; set; }

		public User UserIdParent { get; set; }
		public MobileCompany MobileCompanyIdParent { get; set; }
}
}