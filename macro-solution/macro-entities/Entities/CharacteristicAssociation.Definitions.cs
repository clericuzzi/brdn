﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class CharacteristicAssociation
    {
		/// <summary>
		/// The Characteristic Association's class name
		/// </summary>
		public const string CLASS_NAME = "CharacteristicAssociation";
		/// <summary>
		/// The CharacteristicAssociation's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The CharacteristicAssociation's CharacteristicId property
		/// </summary>
		public const string PROPERTY_CHARACTERISTIC_ID = "CharacteristicId";
		/// <summary>
		/// The CharacteristicAssociation's EntityId property
		/// </summary>
		public const string PROPERTY_ENTITY_ID = "EntityId";
		/// <summary>
		/// The CharacteristicAssociation's Timestamp property
		/// </summary>
		public const string PROPERTY_TIMESTAMP = "Timestamp";

		public int Id { get; set; }
		public int CharacteristicId { get; set; }
		public int EntityId { get; set; }
		public DateTime Timestamp { get; set; }

		public Characteristic CharacteristicIdParent { get; set; }
}
}