﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class PhoneOperator
    {
		/// <summary>
		/// The Phone Operator's class name
		/// </summary>
		public const string CLASS_NAME = "PhoneOperator";
		/// <summary>
		/// The PhoneOperator's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The PhoneOperator's Name property
		/// </summary>
		public const string PROPERTY_NAME = "Name";

		public int Id { get; set; }
		public string Name { get; set; }

        public List<CompanyLine> CompanyLineList { get; set; }
        public List<CompanyLineOperator> CompanyLineOperatorList { get; set; }
	}
}