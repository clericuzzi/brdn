﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class Sale
    {
		/// <summary>
		/// The Sale's class name
		/// </summary>
		public const string CLASS_NAME = "Sale";
		/// <summary>
		/// The Sale's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The Sale's CompanyId property
		/// </summary>
		public const string PROPERTY_COMPANY_ID = "CompanyId";
		/// <summary>
		/// The Sale's UserId property
		/// </summary>
		public const string PROPERTY_USER_ID = "UserId";
		/// <summary>
		/// The Sale's ProductId property
		/// </summary>
		public const string PROPERTY_PRODUCT_ID = "ProductId";
		/// <summary>
		/// The Sale's Timestamp property
		/// </summary>
		public const string PROPERTY_TIMESTAMP = "Timestamp";
		/// <summary>
		/// The Sale's ApprovedBy property
		/// </summary>
		public const string PROPERTY_APPROVED_BY = "ApprovedBy";
		/// <summary>
		/// The Sale's ApprovedIn property
		/// </summary>
		public const string PROPERTY_APPROVED_IN = "ApprovedIn";

		public int Id { get; set; }
		public int CompanyId { get; set; }
		public int UserId { get; set; }
		public int ProductId { get; set; }
		public DateTime Timestamp { get; set; }
		public int ApprovedBy { get; set; }
		public DateTime? ApprovedIn { get; set; }

		public Company CompanyIdParent { get; set; }
		public User UserIdParent { get; set; }
		public Product ProductIdParent { get; set; }
		public User ApprovedByParent { get; set; }
}
}