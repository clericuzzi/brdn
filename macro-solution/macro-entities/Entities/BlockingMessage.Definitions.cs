﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class BlockingMessage
    {
		/// <summary>
		/// The Blocking Message's class name
		/// </summary>
		public const string CLASS_NAME = "BlockingMessage";
		/// <summary>
		/// The BlockingMessage's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The BlockingMessage's Text property
		/// </summary>
		public const string PROPERTY_TEXT = "Text";

		public int Id { get; set; }
		public string Text { get; set; }
}
}