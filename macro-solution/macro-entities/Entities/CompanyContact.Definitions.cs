﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class CompanyContact
    {
		/// <summary>
		/// The Company Contact's class name
		/// </summary>
		public const string CLASS_NAME = "CompanyContact";
		/// <summary>
		/// The CompanyContact's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The CompanyContact's UserId property
		/// </summary>
		public const string PROPERTY_USER_ID = "UserId";
		/// <summary>
		/// The CompanyContact's CompanyId property
		/// </summary>
		public const string PROPERTY_COMPANY_ID = "CompanyId";
		/// <summary>
		/// The CompanyContact's PositionId property
		/// </summary>
		public const string PROPERTY_POSITION_ID = "PositionId";
		/// <summary>
		/// The CompanyContact's DepartmentId property
		/// </summary>
		public const string PROPERTY_DEPARTMENT_ID = "DepartmentId";
		/// <summary>
		/// The CompanyContact's Name property
		/// </summary>
		public const string PROPERTY_NAME = "Name";
		/// <summary>
		/// The CompanyContact's Email property
		/// </summary>
		public const string PROPERTY_EMAIL = "Email";
		/// <summary>
		/// The CompanyContact's Timestamp property
		/// </summary>
		public const string PROPERTY_TIMESTAMP = "Timestamp";

		public int Id { get; set; }
		public int UserId { get; set; }
		public int CompanyId { get; set; }
		public int PositionId { get; set; }
		public int DepartmentId { get; set; }
		public string Name { get; set; }
		public string Email { get; set; }
		public DateTime Timestamp { get; set; }

		public User UserIdParent { get; set; }
		public Company CompanyIdParent { get; set; }
		public ContactPosition PositionIdParent { get; set; }
		public ContactDepartment DepartmentIdParent { get; set; }

        public List<CompanyCall> CompanyCallList { get; set; }
        public List<CompanyCallback> CompanyCallbackList { get; set; }
        public List<CompanyContactLine> CompanyContactLineList { get; set; }
        public List<CompanyNote> CompanyNoteList { get; set; }
	}
}