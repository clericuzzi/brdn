﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class Permission
    {
		/// <summary>
		/// The Permission's class name
		/// </summary>
		public const string CLASS_NAME = "Permission";
		/// <summary>
		/// The Permission's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The Permission's AccountId property
		/// </summary>
		public const string PROPERTY_ACCOUNT_ID = "AccountId";
		/// <summary>
		/// The Permission's Name property
		/// </summary>
		public const string PROPERTY_NAME = "Name";
		/// <summary>
		/// The Permission's Priority property
		/// </summary>
		public const string PROPERTY_PRIORITY = "Priority";

		public int Id { get; set; }
		public int? AccountId { get; set; }
		public string Name { get; set; }
		public int Priority { get; set; }

		public Account AccountIdParent { get; set; }

        public List<UserPermission> UserPermissionList { get; set; }
	}
}