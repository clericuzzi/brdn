﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class CompanySituation
    {
		/// <summary>
		/// The Company Situation's class name
		/// </summary>
		public const string CLASS_NAME = "CompanySituation";
		/// <summary>
		/// The CompanySituation's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The CompanySituation's Name property
		/// </summary>
		public const string PROPERTY_NAME = "Name";

		public int Id { get; set; }
		public string Name { get; set; }

        public List<Company> CompanyList { get; set; }
	}
}