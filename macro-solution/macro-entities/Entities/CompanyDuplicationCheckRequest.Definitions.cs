﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class CompanyDuplicationCheckRequest
    {
		/// <summary>
		/// The  Company Duplication Check Request's class name
		/// </summary>
		public const string CLASS_NAME = "CompanyDuplicationCheckRequest";
		/// <summary>
		/// The CompanyDuplicationCheckRequest's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The CompanyDuplicationCheckRequest's CheckType property
		/// </summary>
		public const string PROPERTY_CHECK_TYPE = "CheckType";
		/// <summary>
		/// The CompanyDuplicationCheckRequest's CompanyId property
		/// </summary>
		public const string PROPERTY_COMPANY_ID = "CompanyId";

		public int Id { get; set; }
		public int CheckType { get; set; }
		public int CompanyId { get; set; }
}
}