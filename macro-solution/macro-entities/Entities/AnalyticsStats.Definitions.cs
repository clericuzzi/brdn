﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class AnalyticsStats
    {
		/// <summary>
		/// The Analytics Stats's class name
		/// </summary>
		public const string CLASS_NAME = "AnalyticsStats";
		/// <summary>
		/// The AnalyticsStats's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The AnalyticsStats's StatisticId property
		/// </summary>
		public const string PROPERTY_STATISTIC_ID = "StatisticId";
		/// <summary>
		/// The AnalyticsStats's UserId property
		/// </summary>
		public const string PROPERTY_USER_ID = "UserId";
		/// <summary>
		/// The AnalyticsStats's EntityId property
		/// </summary>
		public const string PROPERTY_ENTITY_ID = "EntityId";
		/// <summary>
		/// The AnalyticsStats's Amount property
		/// </summary>
		public const string PROPERTY_AMOUNT = "Amount";
		/// <summary>
		/// The AnalyticsStats's Timestamp property
		/// </summary>
		public const string PROPERTY_TIMESTAMP = "Timestamp";

		public int Id { get; set; }
		public int StatisticId { get; set; }
		public int? UserId { get; set; }
		public int? EntityId { get; set; }
		public int Amount { get; set; }
		public DateTime Timestamp { get; set; }

		public AnalyticsStatistic StatisticIdParent { get; set; }
		public User UserIdParent { get; set; }
}
}