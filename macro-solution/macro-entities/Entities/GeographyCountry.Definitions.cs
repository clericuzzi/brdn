﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class GeographyCountry
    {
		/// <summary>
		/// The Geography Country's class name
		/// </summary>
		public const string CLASS_NAME = "GeographyCountry";
		/// <summary>
		/// The GeographyCountry's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The GeographyCountry's Name property
		/// </summary>
		public const string PROPERTY_NAME = "Name";

		public int Id { get; set; }
		public string Name { get; set; }

        public List<GeographyState> GeographyStateList { get; set; }
	}
}