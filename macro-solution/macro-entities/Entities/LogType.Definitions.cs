﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class LogType
    {
		/// <summary>
		/// The Log Type's class name
		/// </summary>
		public const string CLASS_NAME = "LogType";
		/// <summary>
		/// The LogType's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The LogType's Name property
		/// </summary>
		public const string PROPERTY_NAME = "Name";

		public int Id { get; set; }
		public string Name { get; set; }

        public List<Log> LogList { get; set; }
	}
}