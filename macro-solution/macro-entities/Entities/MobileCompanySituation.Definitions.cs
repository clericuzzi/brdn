﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class MobileCompanySituation
    {
		/// <summary>
		/// The Mobile Company Situation's class name
		/// </summary>
		public const string CLASS_NAME = "MobileCompanySituation";
		/// <summary>
		/// The MobileCompanySituation's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The MobileCompanySituation's MobileCompanyId property
		/// </summary>
		public const string PROPERTY_MOBILE_COMPANY_ID = "MobileCompanyId";
		/// <summary>
		/// The MobileCompanySituation's SituationId property
		/// </summary>
		public const string PROPERTY_SITUATION_ID = "SituationId";
		/// <summary>
		/// The MobileCompanySituation's UserId property
		/// </summary>
		public const string PROPERTY_USER_ID = "UserId";
		/// <summary>
		/// The MobileCompanySituation's Obs property
		/// </summary>
		public const string PROPERTY_OBS = "Obs";
		/// <summary>
		/// The MobileCompanySituation's Timestamp property
		/// </summary>
		public const string PROPERTY_TIMESTAMP = "Timestamp";

		public int Id { get; set; }
		public int MobileCompanyId { get; set; }
		public int SituationId { get; set; }
		public int UserId { get; set; }
		public string Obs { get; set; }
		public DateTime Timestamp { get; set; }

		public MobileCompany MobileCompanyIdParent { get; set; }
		public Status SituationIdParent { get; set; }
		public User UserIdParent { get; set; }
}
}