﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class City
    {
		/// <summary>
		/// The City's class name
		/// </summary>
		public const string CLASS_NAME = "City";
		/// <summary>
		/// The City's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The City's StateId property
		/// </summary>
		public const string PROPERTY_STATE_ID = "StateId";
		/// <summary>
		/// The City's Name property
		/// </summary>
		public const string PROPERTY_NAME = "Name";

		public int Id { get; set; }
		public int? StateId { get; set; }
		public string Name { get; set; }

		public State StateIdParent { get; set; }

        public List<Neighbourhood> NeighbourhoodList { get; set; }
        public List<Zipcode> ZipcodeList { get; set; }
	}
}