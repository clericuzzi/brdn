﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class AnalyticsStatisticType
    {
		/// <summary>
		/// The Analytics Statistic Type's class name
		/// </summary>
		public const string CLASS_NAME = "AnalyticsStatisticType";
		/// <summary>
		/// The AnalyticsStatisticType's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The AnalyticsStatisticType's Name property
		/// </summary>
		public const string PROPERTY_NAME = "Name";

		public int Id { get; set; }
		public string Name { get; set; }

        public List<AnalyticsStatistic> AnalyticsStatisticList { get; set; }
	}
}