﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class CompanyInstallationAddress
    {
		/// <summary>
		/// The Company Installation Address's class name
		/// </summary>
		public const string CLASS_NAME = "CompanyInstallationAddress";
		/// <summary>
		/// The CompanyInstallationAddress's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The CompanyInstallationAddress's UserId property
		/// </summary>
		public const string PROPERTY_USER_ID = "UserId";
		/// <summary>
		/// The CompanyInstallationAddress's CompanyId property
		/// </summary>
		public const string PROPERTY_COMPANY_ID = "CompanyId";
		/// <summary>
		/// The CompanyInstallationAddress's Reference property
		/// </summary>
		public const string PROPERTY_REFERENCE = "Reference";
		/// <summary>
		/// The CompanyInstallationAddress's Street property
		/// </summary>
		public const string PROPERTY_STREET = "Street";
		/// <summary>
		/// The CompanyInstallationAddress's Number property
		/// </summary>
		public const string PROPERTY_NUMBER = "Number";
		/// <summary>
		/// The CompanyInstallationAddress's ZipcodeId property
		/// </summary>
		public const string PROPERTY_ZIPCODE_ID = "ZipcodeId";
		/// <summary>
		/// The CompanyInstallationAddress's Timestamp property
		/// </summary>
		public const string PROPERTY_TIMESTAMP = "Timestamp";

		public int Id { get; set; }
		public int UserId { get; set; }
		public int CompanyId { get; set; }
		public string Reference { get; set; }
		public string Street { get; set; }
		public string Number { get; set; }
		public int ZipcodeId { get; set; }
		public DateTime Timestamp { get; set; }

		public User UserIdParent { get; set; }
		public Company CompanyIdParent { get; set; }
		public GeographyZipcode ZipcodeIdParent { get; set; }
}
}