﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class CompanyLine
    {
		/// <summary>
		/// The Company Line's class name
		/// </summary>
		public const string CLASS_NAME = "CompanyLine";
		/// <summary>
		/// The CompanyLine's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The CompanyLine's CompanyId property
		/// </summary>
		public const string PROPERTY_COMPANY_ID = "CompanyId";
		/// <summary>
		/// The CompanyLine's Phone property
		/// </summary>
		public const string PROPERTY_PHONE = "Phone";
		/// <summary>
		/// The CompanyLine's Mobile property
		/// </summary>
		public const string PROPERTY_MOBILE = "Mobile";
		/// <summary>
		/// The CompanyLine's OperatorId property
		/// </summary>
		public const string PROPERTY_OPERATOR_ID = "OperatorId";
		/// <summary>
		/// The CompanyLine's OperatorSince property
		/// </summary>
		public const string PROPERTY_OPERATOR_SINCE = "OperatorSince";
		/// <summary>
		/// The CompanyLine's TestingTimestamp property
		/// </summary>
		public const string PROPERTY_TESTING_TIMESTAMP = "TestingTimestamp";
		/// <summary>
		/// The CompanyLine's TestedTodayDnc property
		/// </summary>
		public const string PROPERTY_TESTED_TODAY_DNC = "TestedTodayDnc";
		/// <summary>
		/// The CompanyLine's TestedTodayProcon property
		/// </summary>
		public const string PROPERTY_TESTED_TODAY_PROCON = "TestedTodayProcon";
		/// <summary>
		/// The CompanyLine's IsValid property
		/// </summary>
		public const string PROPERTY_IS_VALID = "IsValid";

		public int Id { get; set; }
		public int CompanyId { get; set; }
		public string Phone { get; set; }
		public int Mobile { get; set; }
		public int? OperatorId { get; set; }
		public DateTime? OperatorSince { get; set; }
		public DateTime? TestingTimestamp { get; set; }
		public int TestedTodayDnc { get; set; }
		public int TestedTodayProcon { get; set; }
		public int IsValid { get; set; }

		public Company CompanyIdParent { get; set; }
		public PhoneOperator OperatorIdParent { get; set; }

        public List<CompanyContactLine> CompanyContactLineList { get; set; }
        public List<CompanyLineOperator> CompanyLineOperatorList { get; set; }
	}
}