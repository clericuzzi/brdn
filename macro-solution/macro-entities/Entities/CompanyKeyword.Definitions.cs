﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class CompanyKeyword
    {
		/// <summary>
		/// The Company Keyword's class name
		/// </summary>
		public const string CLASS_NAME = "CompanyKeyword";
		/// <summary>
		/// The CompanyKeyword's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The CompanyKeyword's CompanyId property
		/// </summary>
		public const string PROPERTY_COMPANY_ID = "CompanyId";
		/// <summary>
		/// The CompanyKeyword's KeywordId property
		/// </summary>
		public const string PROPERTY_KEYWORD_ID = "KeywordId";

		public int Id { get; set; }
		public int CompanyId { get; set; }
		public int KeywordId { get; set; }

		public Company CompanyIdParent { get; set; }
		public Keyword KeywordIdParent { get; set; }
}
}