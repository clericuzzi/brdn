﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class AccountSignatureFeeHistory
    {
		/// <summary>
		/// The Account Signature Fee History's class name
		/// </summary>
		public const string CLASS_NAME = "AccountSignatureFeeHistory";
		/// <summary>
		/// The AccountSignatureFeeHistory's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The AccountSignatureFeeHistory's AccountId property
		/// </summary>
		public const string PROPERTY_ACCOUNT_ID = "AccountId";
		/// <summary>
		/// The AccountSignatureFeeHistory's Year property
		/// </summary>
		public const string PROPERTY_YEAR = "Year";
		/// <summary>
		/// The AccountSignatureFeeHistory's Month property
		/// </summary>
		public const string PROPERTY_MONTH = "Month";
		/// <summary>
		/// The AccountSignatureFeeHistory's SignatureFee property
		/// </summary>
		public const string PROPERTY_SIGNATURE_FEE = "SignatureFee";

		public int Id { get; set; }
		public int AccountId { get; set; }
		public int Year { get; set; }
		public int Month { get; set; }
		public double SignatureFee { get; set; }

		public Account AccountIdParent { get; set; }
}
}