﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class StatusType
    {
		/// <summary>
		/// The Status Type's class name
		/// </summary>
		public const string CLASS_NAME = "StatusType";
		/// <summary>
		/// The StatusType's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The StatusType's Name property
		/// </summary>
		public const string PROPERTY_NAME = "Name";

		public int Id { get; set; }
		public string Name { get; set; }

        public List<Status> StatusList { get; set; }
	}
}