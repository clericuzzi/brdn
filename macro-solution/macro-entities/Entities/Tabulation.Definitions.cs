﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class Tabulation
    {
		/// <summary>
		/// The Tabulation's class name
		/// </summary>
		public const string CLASS_NAME = "Tabulation";
		/// <summary>
		/// The Tabulation's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The Tabulation's TabulationTypeId property
		/// </summary>
		public const string PROPERTY_TABULATION_TYPE_ID = "TabulationTypeId";
		/// <summary>
		/// The Tabulation's Text property
		/// </summary>
		public const string PROPERTY_TEXT = "Text";

		public int Id { get; set; }
		public int TabulationTypeId { get; set; }
		public string Text { get; set; }

		public TabulationType TabulationTypeIdParent { get; set; }

        public List<BlockedTabulations> BlockedTabulationsList { get; set; }
        public List<CompanyCall> CompanyCallList { get; set; }
	}
}