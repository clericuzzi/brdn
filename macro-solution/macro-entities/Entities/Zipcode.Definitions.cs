﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class Zipcode
    {
		/// <summary>
		/// The Zipcode's class name
		/// </summary>
		public const string CLASS_NAME = "Zipcode";
		/// <summary>
		/// The Zipcode's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The Zipcode's Zip property
		/// </summary>
		public const string PROPERTY_ZIP = "Zip";
		/// <summary>
		/// The Zipcode's NeighbourhoodId property
		/// </summary>
		public const string PROPERTY_NEIGHBOURHOOD_ID = "NeighbourhoodId";
		/// <summary>
		/// The Zipcode's CityId property
		/// </summary>
		public const string PROPERTY_CITY_ID = "CityId";

		public int Id { get; set; }
		public string Zip { get; set; }
		public int? NeighbourhoodId { get; set; }
		public int? CityId { get; set; }

		public Neighbourhood NeighbourhoodIdParent { get; set; }
		public City CityIdParent { get; set; }

        public List<CompanyAddress> CompanyAddressList { get; set; }
        public List<MailingExtractionZipcode> MailingExtractionZipcodeList { get; set; }
	}
}