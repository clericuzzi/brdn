﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class ActionType
    {
		/// <summary>
		/// The Action Type's class name
		/// </summary>
		public const string CLASS_NAME = "ActionType";
		/// <summary>
		/// The ActionType's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The ActionType's Name property
		/// </summary>
		public const string PROPERTY_NAME = "Name";

		public int Id { get; set; }
		public string Name { get; set; }
}
}