﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class Lead
    {
		/// <summary>
		/// The Lead's class name
		/// </summary>
		public const string CLASS_NAME = "Lead";
		/// <summary>
		/// The Lead's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The Lead's UserId property
		/// </summary>
		public const string PROPERTY_USER_ID = "UserId";
		/// <summary>
		/// The Lead's CompanyId property
		/// </summary>
		public const string PROPERTY_COMPANY_ID = "CompanyId";
		/// <summary>
		/// The Lead's ActivityId property
		/// </summary>
		public const string PROPERTY_ACTIVITY_ID = "ActivityId";
		/// <summary>
		/// The Lead's Timestamp property
		/// </summary>
		public const string PROPERTY_TIMESTAMP = "Timestamp";

		public int Id { get; set; }
		public int? UserId { get; set; }
		public int CompanyId { get; set; }
		public int ActivityId { get; set; }
		public DateTime? Timestamp { get; set; }

		public User UserIdParent { get; set; }
		public Company CompanyIdParent { get; set; }
		public Activity ActivityIdParent { get; set; }
}
}