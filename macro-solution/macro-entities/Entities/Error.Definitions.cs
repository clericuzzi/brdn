﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class Error
    {
		/// <summary>
		/// The Error's class name
		/// </summary>
		public const string CLASS_NAME = "Error";
		/// <summary>
		/// The Error's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The Error's ClassName property
		/// </summary>
		public const string PROPERTY_CLASS_NAME = "ClassName";
		/// <summary>
		/// The Error's MethodName property
		/// </summary>
		public const string PROPERTY_METHOD_NAME = "MethodName";
		/// <summary>
		/// The Error's ExceptionType property
		/// </summary>
		public const string PROPERTY_EXCEPTION_TYPE = "ExceptionType";
		/// <summary>
		/// The Error's ExceptionMessage property
		/// </summary>
		public const string PROPERTY_EXCEPTION_MESSAGE = "ExceptionMessage";
		/// <summary>
		/// The Error's ExceptionStackTrace property
		/// </summary>
		public const string PROPERTY_EXCEPTION_STACK_TRACE = "ExceptionStackTrace";
		/// <summary>
		/// The Error's Timestamp property
		/// </summary>
		public const string PROPERTY_TIMESTAMP = "Timestamp";

		public int Id { get; set; }
		public string ClassName { get; set; }
		public string MethodName { get; set; }
		public string ExceptionType { get; set; }
		public string ExceptionMessage { get; set; }
		public string ExceptionStackTrace { get; set; }
		public DateTime Timestamp { get; set; }
}
}