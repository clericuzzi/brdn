﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class ImportFile
    {
		/// <summary>
		/// The Import File's class name
		/// </summary>
		public const string CLASS_NAME = "ImportFile";
		/// <summary>
		/// The ImportFile's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The ImportFile's UserId property
		/// </summary>
		public const string PROPERTY_USER_ID = "UserId";
		/// <summary>
		/// The ImportFile's CampaignName property
		/// </summary>
		public const string PROPERTY_CAMPAIGN_NAME = "CampaignName";
		/// <summary>
		/// The ImportFile's Active property
		/// </summary>
		public const string PROPERTY_ACTIVE = "Active";
		/// <summary>
		/// The ImportFile's Timestamp property
		/// </summary>
		public const string PROPERTY_TIMESTAMP = "Timestamp";

		public int Id { get; set; }
		public int UserId { get; set; }
		public string CampaignName { get; set; }
		public int Active { get; set; }
		public DateTime Timestamp { get; set; }

		public User UserIdParent { get; set; }

        public List<ImportFileCompany> ImportFileCompanyList { get; set; }
	}
}