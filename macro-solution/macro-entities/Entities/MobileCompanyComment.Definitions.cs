﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class MobileCompanyComment
    {
		/// <summary>
		/// The Mobile Company Comment's class name
		/// </summary>
		public const string CLASS_NAME = "MobileCompanyComment";
		/// <summary>
		/// The MobileCompanyComment's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The MobileCompanyComment's MobileCompanyId property
		/// </summary>
		public const string PROPERTY_MOBILE_COMPANY_ID = "MobileCompanyId";
		/// <summary>
		/// The MobileCompanyComment's UserId property
		/// </summary>
		public const string PROPERTY_USER_ID = "UserId";
		/// <summary>
		/// The MobileCompanyComment's Comment property
		/// </summary>
		public const string PROPERTY_COMMENT = "Comment";
		/// <summary>
		/// The MobileCompanyComment's Timestamp property
		/// </summary>
		public const string PROPERTY_TIMESTAMP = "Timestamp";

		public int Id { get; set; }
		public int MobileCompanyId { get; set; }
		public int UserId { get; set; }
		public string Comment { get; set; }
		public DateTime Timestamp { get; set; }

		public MobileCompany MobileCompanyIdParent { get; set; }
		public User UserIdParent { get; set; }
}
}