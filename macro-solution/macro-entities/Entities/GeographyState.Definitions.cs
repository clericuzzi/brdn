﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class GeographyState
    {
		/// <summary>
		/// The Geography State's class name
		/// </summary>
		public const string CLASS_NAME = "GeographyState";
		/// <summary>
		/// The GeographyState's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The GeographyState's CountryId property
		/// </summary>
		public const string PROPERTY_COUNTRY_ID = "CountryId";
		/// <summary>
		/// The GeographyState's Name property
		/// </summary>
		public const string PROPERTY_NAME = "Name";
		/// <summary>
		/// The GeographyState's Abbreviation property
		/// </summary>
		public const string PROPERTY_ABBREVIATION = "Abbreviation";

		public int Id { get; set; }
		public int CountryId { get; set; }
		public string Name { get; set; }
		public string Abbreviation { get; set; }

		public GeographyCountry CountryIdParent { get; set; }

        public List<GeographyCity> GeographyCityList { get; set; }
	}
}