﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class AccountPaymentHistory
    {
		/// <summary>
		/// The Account Payment History's class name
		/// </summary>
		public const string CLASS_NAME = "AccountPaymentHistory";
		/// <summary>
		/// The AccountPaymentHistory's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The AccountPaymentHistory's AccountId property
		/// </summary>
		public const string PROPERTY_ACCOUNT_ID = "AccountId";
		/// <summary>
		/// The AccountPaymentHistory's Year property
		/// </summary>
		public const string PROPERTY_YEAR = "Year";
		/// <summary>
		/// The AccountPaymentHistory's Month property
		/// </summary>
		public const string PROPERTY_MONTH = "Month";
		/// <summary>
		/// The AccountPaymentHistory's Payment property
		/// </summary>
		public const string PROPERTY_PAYMENT = "Payment";

		public int Id { get; set; }
		public int AccountId { get; set; }
		public int Year { get; set; }
		public int Month { get; set; }
		public double Payment { get; set; }

		public Account AccountIdParent { get; set; }
}
}