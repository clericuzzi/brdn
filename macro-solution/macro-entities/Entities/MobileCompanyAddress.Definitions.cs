﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class MobileCompanyAddress
    {
		/// <summary>
		/// The Mobile Company Address's class name
		/// </summary>
		public const string CLASS_NAME = "MobileCompanyAddress";
		/// <summary>
		/// The MobileCompanyAddress's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The MobileCompanyAddress's MobileCompanyId property
		/// </summary>
		public const string PROPERTY_MOBILE_COMPANY_ID = "MobileCompanyId";
		/// <summary>
		/// The MobileCompanyAddress's Address property
		/// </summary>
		public const string PROPERTY_ADDRESS = "Address";

		public int Id { get; set; }
		public int MobileCompanyId { get; set; }
		public string Address { get; set; }

		public MobileCompany MobileCompanyIdParent { get; set; }
}
}