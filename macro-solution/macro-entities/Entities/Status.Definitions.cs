﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class Status
    {
		/// <summary>
		/// The Status's class name
		/// </summary>
		public const string CLASS_NAME = "Status";
		/// <summary>
		/// The Status's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The Status's StatusTypeId property
		/// </summary>
		public const string PROPERTY_STATUS_TYPE_ID = "StatusTypeId";
		/// <summary>
		/// The Status's Name property
		/// </summary>
		public const string PROPERTY_NAME = "Name";

		public int Id { get; set; }
		public int StatusTypeId { get; set; }
		public string Name { get; set; }

		public StatusType StatusTypeIdParent { get; set; }
}
}