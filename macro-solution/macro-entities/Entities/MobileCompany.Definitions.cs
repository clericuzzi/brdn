﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class MobileCompany
    {
		/// <summary>
		/// The Mobile Company's class name
		/// </summary>
		public const string CLASS_NAME = "MobileCompany";
		/// <summary>
		/// The MobileCompany's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The MobileCompany's AccountId property
		/// </summary>
		public const string PROPERTY_ACCOUNT_ID = "AccountId";
		/// <summary>
		/// The MobileCompany's Cnpj property
		/// </summary>
		public const string PROPERTY_CNPJ = "Cnpj";
		/// <summary>
		/// The MobileCompany's Segment property
		/// </summary>
		public const string PROPERTY_SEGMENT = "Segment";
		/// <summary>
		/// The MobileCompany's Description property
		/// </summary>
		public const string PROPERTY_DESCRIPTION = "Description";
		/// <summary>
		/// The MobileCompany's FantasyName property
		/// </summary>
		public const string PROPERTY_FANTASY_NAME = "FantasyName";
		/// <summary>
		/// The MobileCompany's Callback property
		/// </summary>
		public const string PROPERTY_CALLBACK = "Callback";
		/// <summary>
		/// The MobileCompany's CallbackUser property
		/// </summary>
		public const string PROPERTY_CALLBACK_USER = "CallbackUser";
		/// <summary>
		/// The MobileCompany's Timestamp property
		/// </summary>
		public const string PROPERTY_TIMESTAMP = "Timestamp";

		public int Id { get; set; }
		public int AccountId { get; set; }
		public string Cnpj { get; set; }
		public string Segment { get; set; }
		public string Description { get; set; }
		public string FantasyName { get; set; }
		public DateTime? Callback { get; set; }
		public int? CallbackUser { get; set; }
		public DateTime Timestamp { get; set; }

		public Account AccountIdParent { get; set; }
		public User CallbackUserParent { get; set; }

        public List<MobileCompanyAddress> MobileCompanyAddressList { get; set; }
        public List<MobileCompanyComment> MobileCompanyCommentList { get; set; }
        public List<MobileCompanyContact> MobileCompanyContactList { get; set; }
        public List<MobileCompanyEvaluation> MobileCompanyEvaluationList { get; set; }
        public List<MobileCompanyLock> MobileCompanyLockList { get; set; }
        public List<MobileCompanySituation> MobileCompanySituationList { get; set; }
	}
}