﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class Plan
    {
		/// <summary>
		/// The Plan's class name
		/// </summary>
		public const string CLASS_NAME = "Plan";
		/// <summary>
		/// The Plan's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The Plan's AccountId property
		/// </summary>
		public const string PROPERTY_ACCOUNT_ID = "AccountId";
		/// <summary>
		/// The Plan's Name property
		/// </summary>
		public const string PROPERTY_NAME = "Name";
		/// <summary>
		/// The Plan's Data property
		/// </summary>
		public const string PROPERTY_DATA = "Data";
		/// <summary>
		/// The Plan's Minutes property
		/// </summary>
		public const string PROPERTY_MINUTES = "Minutes";
		/// <summary>
		/// The Plan's Ilimited property
		/// </summary>
		public const string PROPERTY_ILIMITED = "Ilimited";
		/// <summary>
		/// The Plan's Campaign property
		/// </summary>
		public const string PROPERTY_CAMPAIGN = "Campaign";
		/// <summary>
		/// The Plan's DiscountValue property
		/// </summary>
		public const string PROPERTY_DISCOUNT_VALUE = "DiscountValue";
		/// <summary>
		/// The Plan's DiscountPercentage property
		/// </summary>
		public const string PROPERTY_DISCOUNT_PERCENTAGE = "DiscountPercentage";
		/// <summary>
		/// The Plan's Timestamp property
		/// </summary>
		public const string PROPERTY_TIMESTAMP = "Timestamp";

		public int Id { get; set; }
		public int AccountId { get; set; }
		public string Name { get; set; }
		public int Data { get; set; }
		public int Minutes { get; set; }
		public int Ilimited { get; set; }
		public string Campaign { get; set; }
		public double DiscountValue { get; set; }
		public double DiscountPercentage { get; set; }
		public DateTime Timestamp { get; set; }

		public Account AccountIdParent { get; set; }

        public List<PlanProduct> PlanProductList { get; set; }
        public List<Sale> SaleList { get; set; }
	}
}