﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class CompanyType
    {
		/// <summary>
		/// The Company Type's class name
		/// </summary>
		public const string CLASS_NAME = "CompanyType";
		/// <summary>
		/// The CompanyType's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The CompanyType's Type property
		/// </summary>
		public const string PROPERTY_TYPE = "Type";

		public int Id { get; set; }
		public string Type { get; set; }

        public List<CompanySubtype> CompanySubtypeList { get; set; }
	}
}