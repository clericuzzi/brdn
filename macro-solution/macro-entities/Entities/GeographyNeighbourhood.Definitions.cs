﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class GeographyNeighbourhood
    {
		/// <summary>
		/// The Geography Neighbourhood's class name
		/// </summary>
		public const string CLASS_NAME = "GeographyNeighbourhood";
		/// <summary>
		/// The GeographyNeighbourhood's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The GeographyNeighbourhood's CityId property
		/// </summary>
		public const string PROPERTY_CITY_ID = "CityId";
		/// <summary>
		/// The GeographyNeighbourhood's Name property
		/// </summary>
		public const string PROPERTY_NAME = "Name";

		public int Id { get; set; }
		public int? CityId { get; set; }
		public string Name { get; set; }

		public GeographyCity CityIdParent { get; set; }

        public List<GeographyZipcode> GeographyZipcodeList { get; set; }
	}
}