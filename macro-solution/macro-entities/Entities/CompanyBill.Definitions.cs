﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class CompanyBill
    {
		/// <summary>
		/// The Company Bill's class name
		/// </summary>
		public const string CLASS_NAME = "CompanyBill";
		/// <summary>
		/// The CompanyBill's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The CompanyBill's CompanyId property
		/// </summary>
		public const string PROPERTY_COMPANY_ID = "CompanyId";
		/// <summary>
		/// The CompanyBill's Billing property
		/// </summary>
		public const string PROPERTY_BILLING = "Billing";
		/// <summary>
		/// The CompanyBill's Serial property
		/// </summary>
		public const string PROPERTY_SERIAL = "Serial";
		/// <summary>
		/// The CompanyBill's Value property
		/// </summary>
		public const string PROPERTY_VALUE = "Value";
		/// <summary>
		/// The CompanyBill's Due property
		/// </summary>
		public const string PROPERTY_DUE = "Due";

		public int Id { get; set; }
		public int CompanyId { get; set; }
		public string Billing { get; set; }
		public string Serial { get; set; }
		public double? Value { get; set; }
		public DateTime? Due { get; set; }

		public Company CompanyIdParent { get; set; }
}
}