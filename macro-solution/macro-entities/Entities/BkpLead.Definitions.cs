﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class BkpLead
    {
		/// <summary>
		/// The  Bkp Lead's class name
		/// </summary>
		public const string CLASS_NAME = "BkpLead";
		/// <summary>
		/// The BkpLead's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The BkpLead's UserId property
		/// </summary>
		public const string PROPERTY_USER_ID = "UserId";
		/// <summary>
		/// The BkpLead's CompanyId property
		/// </summary>
		public const string PROPERTY_COMPANY_ID = "CompanyId";
		/// <summary>
		/// The BkpLead's ActivityId property
		/// </summary>
		public const string PROPERTY_ACTIVITY_ID = "ActivityId";
		/// <summary>
		/// The BkpLead's Timestamp property
		/// </summary>
		public const string PROPERTY_TIMESTAMP = "Timestamp";

		public int Id { get; set; }
		public int? UserId { get; set; }
		public int CompanyId { get; set; }
		public int ActivityId { get; set; }
		public DateTime? Timestamp { get; set; }
}
}