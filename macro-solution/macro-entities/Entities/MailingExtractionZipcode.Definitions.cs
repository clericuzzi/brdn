﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class MailingExtractionZipcode
    {
		/// <summary>
		/// The Mailing Extraction Zipcode's class name
		/// </summary>
		public const string CLASS_NAME = "MailingExtractionZipcode";
		/// <summary>
		/// The MailingExtractionZipcode's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The MailingExtractionZipcode's ExtractionId property
		/// </summary>
		public const string PROPERTY_EXTRACTION_ID = "ExtractionId";
		/// <summary>
		/// The MailingExtractionZipcode's ZipcodeId property
		/// </summary>
		public const string PROPERTY_ZIPCODE_ID = "ZipcodeId";
		/// <summary>
		/// The MailingExtractionZipcode's ClearedItems property
		/// </summary>
		public const string PROPERTY_CLEARED_ITEMS = "ClearedItems";
		/// <summary>
		/// The MailingExtractionZipcode's Done property
		/// </summary>
		public const string PROPERTY_DONE = "Done";
		/// <summary>
		/// The MailingExtractionZipcode's Priority property
		/// </summary>
		public const string PROPERTY_PRIORITY = "Priority";

		public int Id { get; set; }
		public int ExtractionId { get; set; }
		public int? ZipcodeId { get; set; }
		public string ClearedItems { get; set; }
		public int Done { get; set; }
		public int Priority { get; set; }

		public MailingExtraction ExtractionIdParent { get; set; }
		public Zipcode ZipcodeIdParent { get; set; }
}
}