﻿using System;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.Macro.Entities
{
    public partial class WebVendasTestingCycle
    {
		/// <summary>
		/// The Web Vendas Testing Cycle's class name
		/// </summary>
		public const string CLASS_NAME = "WebVendasTestingCycle";
		/// <summary>
		/// The WebVendasTestingCycle's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The WebVendasTestingCycle's CompanyAddressId property
		/// </summary>
		public const string PROPERTY_COMPANY_ADDRESS_ID = "CompanyAddressId";
		/// <summary>
		/// The WebVendasTestingCycle's Priority property
		/// </summary>
		public const string PROPERTY_PRIORITY = "Priority";

		public int Id { get; set; }
		public int CompanyAddressId { get; set; }
		public int? Priority { get; set; }

		public CompanyAddress CompanyAddressIdParent { get; set; }
}
}