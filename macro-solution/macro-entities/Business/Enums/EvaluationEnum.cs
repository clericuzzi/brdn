﻿namespace Clericuzzi.Brdn.Macro.Entities.Business.Enums
{
    public enum EvaluationEnum
    {
        ClientNotFound = 2,
        ClientCallback = 3,
        ClientInterested = 1,
        ClientNotInterested = 4,
    }
}