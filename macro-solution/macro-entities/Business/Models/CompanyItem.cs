﻿using Clericuzzi.Lib.Utils.Core;

namespace Clericuzzi.Brdn.Macro.Entities.Business.Models
{
    public class CompanyItem
    {
        public string Address { get; set; }
        public Company Company { get; set; }

        public CompanyItem(int companySubtype, string ddd, string site, string phone, string address, string description, string fantasyName)
        {
            Address = address;
            Company = new Company
            {
                CompanySubtypeId = companySubtype,

                Phone = (ddd + phone).RemoveStartingCharacter("0"),

                Description = description,
                FantasyName = fantasyName,
            };
        }
    }
}