﻿using System;

namespace Clericuzzi.Brdn.Macro.Entities.Business.Exceptions
{
    public class DifferentCityExeption : Exception
    {
        public DifferentCityExeption(string message) : base(message)
        {
        }
    }
}