﻿using System.IO;
using System.Linq;
using Clericuzzi.Lib.Utils.Core;
using System.Collections.Generic;

namespace cep_fixer
{
    class Program
    {
        static void Main(string[] args)
        {
            var ceps = File.ReadAllLines("data.txt").ToList();
            var fixedCeps = new List<string>();

            ceps.Where(i => i.DigitsOnly().Length > 0).Distinct().ToList().ForEach(i => fixedCeps.Add(_FixCep(i)));
            fixedCeps = fixedCeps.OrderBy(i => i).ToList();
            File.WriteAllLines("data_fixed.txt", fixedCeps);
        }
        private static string _FixCep(string cep)
        {
            cep = cep.DigitsOnly();
            while (cep.Length < 8)
                cep = $"{cep}0";

            return cep;
        }
    }
}