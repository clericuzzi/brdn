﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mobile_company_info_import.Entities
{
    public class InputModel
    {
        public int MobileCompanyId { get; set; }
        public List<InputModelInfo> ContactInfo { get; set; }

        public InputModel()
        {
        }
        public InputModel(int companyId, string line)
        {
            MobileCompanyId = companyId;
            var parts = line.Split(';')[1].Split(':');
            ContactInfo = new List<InputModelInfo>();
            ContactInfo.Add(new InputModelInfo(companyId, parts));
        }
    }
}
