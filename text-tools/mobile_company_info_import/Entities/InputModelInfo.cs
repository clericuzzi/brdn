﻿using Clericuzzi.Lib.Utils.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mobile_company_info_import.Entities
{
    public class InputModelInfo
    {
        public int MobileCompanyId { get; set; }
        public string ContactName { get; set; }
        public string ContactEmail { get; set; }
        public List<string> ContactPhone { get; set; }

        public InputModelInfo()
        {
        }
        public InputModelInfo(int companyId, string[] parts)
        {
            MobileCompanyId = companyId;
            ContactName = parts[0];
            ContactPhone = _GetPhones(parts);
            ContactEmail = parts.Where(i => i.Contains("@")).FirstOrDefault();
        }
        List<string> _GetPhones(string[] parts)
        {
            var phones = parts.Where(i => i.Contains("(") && i.Contains(")") && i.Contains("-")).ToList();
            for (int i = 0; i < phones.Count; i++)
                phones[i] = phones[i].DigitsOnly();

            return phones;
        }
    }
}
