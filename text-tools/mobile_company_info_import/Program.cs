﻿using System.IO;
using System.Linq;
using Clericuzzi.Lib.Utils.Core;
using System.Collections.Generic;
using mobile_company_info_import.Entities;
using System;

namespace mobile_company_info_import
{
    class Program
    {
        static string InputFile { get { return Path.Combine(ConsoleUtils.LocationEntry, "input.txt"); } }
        static List<InputModel> _Models = new List<InputModel>();
        static void Main(string[] args)
        {
            var lines = InputFile.ReadTextFromFile();
            foreach (var line in lines)
            {
                if (!string.IsNullOrEmpty(line))
                {
                    var parts = line.Split(';');
                    if (int.TryParse(parts[0], out int companyId))
                    {
                        var existingModel = _Models.Where(i => i.MobileCompanyId.Equals(companyId)).FirstOrDefault();
                        if (existingModel == null)
                        {
                            existingModel = new InputModel(companyId, line);
                            _Models.Add(existingModel);
                        }
                        else
                            existingModel.ContactInfo.Add(new InputModelInfo(existingModel.MobileCompanyId, parts[1].Split(':')));
                    }
                }
            }

            var sqlCommand = "insert into `_tmp_notes` values ";
            var sqlCommandItem = "('!', '@', null)";
            var sqlCommandItems = new List<string>();
            foreach (var model in _Models)
            {
                var notes = new List<string>();
                var names = model.ContactInfo.Select(i=>i.ContactName).Distinct().ToList();
                var emails = model.ContactInfo.Select(i => i.ContactEmail).Distinct().ToList();
                var phones = new List<string>();
                foreach (var info in model.ContactInfo)
                    emails.AddRange(info.ContactPhone);

                sqlCommandItems.Add(sqlCommandItem.Replace("!", model.MobileCompanyId.ToString()).Replace("@", String.Join(" - ", notes)));
            }

            var finalCommand = sqlCommand + string.Join("\r\n, ", sqlCommandItems) + ";";
            File.WriteAllText("out-notes.txt", finalCommand);

            _WriteContact();
            _WriteLineOut();
        }

        private static void _WriteContact()
        {
            var sqlCommand = "insert into `_tmp_contacts` values ";
            var sqlCommandItem = "('!', '@', '$', null)";
            var sqlCommandItems = new List<string>();
            foreach (var model in _Models)
            {
                var names = new List<string>();
                foreach (var info in model.ContactInfo)
                    names.Add(info.ContactName);

                names = names.Distinct().ToList();
                foreach (var name in names)
                {
                    var emails = model.ContactInfo.Where(i => i.ContactName.Equals(name)).Select(i => i.ContactEmail).Distinct().ToList();
                    var emailString = String.Join(" - ", emails);
                    sqlCommandItems.Add(sqlCommandItem.Replace("!", model.MobileCompanyId.ToString()).Replace("@", name).Replace("$", emailString));
                }
            }

            var finalCommand = sqlCommand + string.Join("\r\n, ", sqlCommandItems) + ";";
            File.WriteAllText("out-contact.txt", finalCommand);
        }

        private static void _WriteLineOut()
        {
            var sqlCommand = "insert into `company_line` values ";
            var sqlCommandItem = "('!', '@', '$', null)";
            var sqlCommandItems = new List<string>();
            foreach (var model in _Models)
            {
                var phones = new List<string>();
                foreach (var info in model.ContactInfo)
                    phones.AddRange(info.ContactPhone);

                phones = phones.Distinct().ToList();
                foreach (var phone in phones)
                    sqlCommandItems.Add(sqlCommandItem.Replace("!", model.MobileCompanyId.ToString()).Replace("@", phone.Substring(0, 2)).Replace("$", phone.Substring(2)));
            }

            var finalCommand = sqlCommand + string.Join("\r\n, ", sqlCommandItems) + ";";
            File.WriteAllText("out.txt", finalCommand);
        }
    }
}