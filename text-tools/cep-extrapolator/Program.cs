﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Clericuzzi.Lib.Utils.Core;
using System.Collections.Generic;
using Clericuzzi.Lib.Http.Business.Requesters;
using Clericuzzi.Lib.Http.Communication.Request;
using Clericuzzi.Lib.Http.Communication.Response;
using Clericuzzi.Lib.Google.Business.ZipcodeValidation;

namespace Clericuzzi.Brdn.TextTools.Cep.Extrapolator
{
    class Program
    {
        const string IN = @"files\in.txt";
        const string DONE = @"files\done.txt";

        static int Failure = 0;
        static int Success = 0;
        static int Accumulated = 0;

        static void Main(string[] args)
        {
            RequestMessageDefaults.Compressed = RequestMessageDefaults.Crypted = false;
            if (ConsoleUtils.IsDevEnvironment)
                RequestMessageDefaults.RootEndpoint = "http://localhost:58111/api/";
            else
                RequestMessageDefaults.RootEndpoint = "http://50.112.18.243:5018/api/";
            RequestMessageDefaults.RootEndpoint = "http://50.112.18.243:5018/api/";

            var done = File.ReadAllLines(DONE);
            var input = File.ReadAllLines(IN);
            var currentTarget = input.Where(i => !done.Contains(i)).ToList().GetRandomItem();
            while (currentTarget != null)
            {
                var cepList = new List<string>();
                for (int i = 0; i < 1000; i++)
                    cepList.Add((currentTarget + i.ToString().LeadingChars("0", 3)).LeadingChars("0", 8));

                _TestCeps(cepList).Wait();

                File.AppendAllLines(DONE, new List<string> { currentTarget });
                currentTarget = input.Where(i => !done.Contains(i)).ToList().GetRandomItem();
            }
        }
        static async Task _TestCeps(List<string> cepList)
        {
            Console.Clear();
            var i = 1;
            var j = 1;

            foreach (var cep in cepList)
            {
                try
                {
                    var address = await ZipcodeValidation.ValidateBr(cep);
                    if (address?.City != null)
                    {
                        var request = new RequestMessage(RequestMessageDefaults.GetUrl("zipcodes/import-cep"));
                        request.AddValue("cep-info", address, null, false);

                        var response = await request.Post<StatusResponse>();
                        if (response.Success)
                            Success++;
                        else
                            Failure++;

                        Console.WriteLine($"{cep}, {response.Success} {j.ToString().LeadingChars("0", 3)}|{Success}/{Failure}|{Accumulated}");
                    }
                    else
                        Console.WriteLine($"{cep} {j.ToString().LeadingChars("0", 3)}|{Success}/{Failure}|{Accumulated}");

                    if (i++ > 100)
                    {
                        i = 1;
                        Console.Clear();
                    }
                    j++;
                    Accumulated++;
                }
                catch (Exception ex)
                {
                    File.AppendAllText(@"files\error.txt", $"Error validating {cep}\n" + ex.Message + "\n" + ex.StackTrace + "\n_______________________");
                }
            }
        }
    }
}