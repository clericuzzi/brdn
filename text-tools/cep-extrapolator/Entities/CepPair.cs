﻿using System.IO;
using Clericuzzi.Lib.Utils.Core;
using System.Collections.Generic;

namespace Clericuzzi.Brdn.TextTools.Cep.Extrapolator.Entities
{
    public class CepPair
    {
        public int CepStart { get; set; }
        public int CepFinish { get; set; }
        public List<string> CepList { get; set; }

        public CepPair() { }
        public CepPair(string pair)
        {
            var parts = pair.Split(';');
            CepStart = int.Parse(parts[0]);
            CepFinish = int.Parse(parts[1]);

            CepList = new List<string>();
            var currentCep = CepStart;
            while (currentCep <= CepFinish)
            {
                //for (int i = 0; i < 1000; i++)
                //    CepList.Add($"{currentCep.ToString()}{i.ToString().LeadingChars("0", 3)}".LeadingChars("0", 8));

                CepList.Add((currentCep++).ToString().LeadingChars("0", 5));
            }
        }

        public override string ToString()
        {
            return $"CepPair: {CepStart}-{CepFinish}, {CepList.Count} items";
        }
    }
}