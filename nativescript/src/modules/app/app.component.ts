import { Component, OnInit, ComponentFactoryResolver } from '@angular/core';
import { ServerUrls } from '@src/external/plugin-utils/utils/server-urls';
import { RequestPaginationDefaults } from '@src/external/plugin-utils/business/models/request/pagination/request-pagination.model';
import { CrudableTableColumnDefinitionModel } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';
import { UserDataService } from '@src/clericuzzi-lib/services/user-data.service';
import { ComponentGeneratorService } from '@src/clericuzzi-lib/services/component-generator.service';
import { DomSanitizer } from '@angular/platform-browser';
import { ModuleBasicDefinitions } from '@src/module-basic/business/constants/module-basic.definitions';
import { ProjectRoutingService } from '@src/services/project-routing.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit
{
  constructor(
    private _Routes: ProjectRoutingService,
    private _UserData: UserDataService,
    private _Generator: ComponentGeneratorService,

    public factory: ComponentFactoryResolver,
    public sanitizer: DomSanitizer,
  )
  {
  }
  ngOnInit()
  {
    const serverRoot: string = `http://54.70.216.46:5018/api/`;
    if (window.location.href.indexOf(`localhost`) < 0)
      ServerUrls.SERVER_ADDRESS = serverRoot;
    else
    {
      ServerUrls.SERVER_ADDRESS = serverRoot;
      ServerUrls.SERVER_ADDRESS = ServerUrls.LOCAL_ROOT;
    }
    RequestPaginationDefaults.PageSize = 100;
    CrudableTableColumnDefinitionModel.Init();
    ModuleBasicDefinitions.Init(ServerUrls.SERVER_ROOT);
  }
}
