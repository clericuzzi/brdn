import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from '@src/modules/app/app-routing.module';
import { AppComponent } from '@src/modules/app/app.component';
import { HomeComponent } from '@src/modules/app/home/home.component';
import { ProjectRoutingService } from '@src/services/project-routing.service';
import { SiteModule } from '../site/site.module';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
  ],
  imports: [
    // SiteModule,
    BrowserModule,
    AppRoutingModule,
  ],
  providers: [
    ProjectRoutingService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
