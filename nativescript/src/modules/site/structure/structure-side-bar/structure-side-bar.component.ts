import { MatSnackBar } from '@angular/material';
import { Component, OnInit, ViewChild, ElementRef, ViewContainerRef, ComponentFactoryResolver } from '@angular/core';

// utils
import { SitePaths } from '@src/modules/site/site-paths.routes';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';
import { InputDecoration } from '@src/external/plugin-utils/utils/input-decoration';

// models

// services
import { UserDataService } from 'clericuzzi-lib/services/user-data.service';
import { ProjectRoutingService } from '@src/services/project-routing.service';

// components
import { AccordionPanelComponent } from 'clericuzzi-lib/modules/shared/accordion-panel/accordion-panel.component';
import { AccordionPanelController } from 'clericuzzi-lib/modules/shared/accordion-panel/accordion-panel.controller';
import { SubjectListenerComponent } from 'clericuzzi-lib/modules/shared/subject-listener/subject-listener.component';
import { AccordionPanelItemComponent } from 'clericuzzi-lib/modules/shared/accordion-panel/accordion-panel-item/accordion-panel-item.component';
import { CompanyActionsService } from '@src/services/company-actions.service';

@Component({
  selector: 'app-structure-side-bar',
  templateUrl: './structure-side-bar.component.html',
  styleUrls: ['./structure-side-bar.component.css']
})
export class StructureSideBarComponent extends SubjectListenerComponent implements OnInit
{
  @ViewChild(`items`) Items: ElementRef;
  @ViewChild('itemPanel', { read: ViewContainerRef }) _ContainerItems: ViewContainerRef;

  public Busy: boolean = true;
  private PanelItems: any = {};
  private PanelSubItems: any = {};

  constructor(
    public _Snack: MatSnackBar,
    public _Routing: ProjectRoutingService,
    private _UserData: UserDataService,
    public ComponentFactoryResolver: ComponentFactoryResolver,

    private _CompanyActions: CompanyActionsService,
  )
  {
    super();
  }

  private _AccordionController: AccordionPanelController;
  public get CurrentAccount(): string
  {
    return this._UserData.CurrentAccountName;
  }
  public get MultipleAccounts(): boolean
  {
    return this._UserData.HasMultipleAccounts;
  }

  ngOnInit()
  {
    this._AccordionController = new AccordionPanelController(this._ContainerItems, this.ComponentFactoryResolver);
    this._AccordionController.SingleSelection = false;

    this.ListenTo(this.OnDestroyed, () => this._AccordionController.IsAlive = false);
    this.ListenTo(this._UserData.Accounts.OnAccountChanged, () => this._Draw());
    this.ListenTo(this._UserData.LoginService.UserValidated, () => this._Draw());

    this._Draw();
  }

  private _Draw(): void
  {
    this.PanelItems = {};
    this.PanelSubItems = {};
    this._AccordionController.Clear();

    if (this._UserData.IsManagementLevel)
      this._InitManagement();
    else
      this._InitUser();
  }

  private _NewLeadSoho(): void
  {
    this._CompanyActions.CampaignId = 1;
    this._Routing.ToSiteRoute(SitePaths.LEAD_NEW);
  }
  private _NewLeadMobile(): void
  {
    this._CompanyActions.CampaignId = 3;
    this._Routing.ToSiteRoute(SitePaths.LEAD_NEW);
  }
  private _NewLeadHighSpeed(): void
  {
    this._CompanyActions.CampaignId = 4;
    this._Routing.ToSiteRoute(SitePaths.LEAD_NEW);
  }
  private _NewLeadSfa(): void
  {
    this._CompanyActions.CampaignId = 5;
    this._Routing.ToSiteRoute(SitePaths.LEAD_NEW);
  }

  private _InitUser(): void
  {
    this._AccordionController.New(`Início`, () => this._Routing.ToSiteRoute(SitePaths.START));

    const leads: AccordionPanelComponent = this._AccordionController.New(`Leads`, () => this._Routing.ToSiteRoute(SitePaths.START));
    leads.AddItem(`Agendamentos`, () => this._Routing.ToSiteRoute(SitePaths.LEAD_CALLBACK), null, null);
    leads.AddItem(`Novo Lead Móvel`, () => this._NewLeadMobile(), null, null);
    leads.AddItem(`Novo Lead Soho`, () => this._NewLeadSoho(), null, null);
    leads.AddItem(`Novo Lead Soho Alta Velocidade`, () => this._NewLeadHighSpeed(), null, null);
    leads.AddItem(`Novo Lead Sfa`, () => this._NewLeadSfa(), null, null);
    leads.AddItem(`Meus leads`, () => this._Routing.ToSiteRoute(SitePaths.LEAD_MINE), null, null);

    // let sales: AccordionPanelComponent = this._AccordionController.New(`Vendas`, () => this._Routing.ToSiteRoute(SitePaths.START));
    // sales.AddItem(`Minha esteira`, () => this.AwaitingValidation(), null, null);
    // sales.AddItem(`Minhas vendas`, () => this.AwaitingValidation(), null, null);
  }
  private _InitManagement(): void
  {
    this._AccordionController.New(`Início`, () => this._Routing.ToSiteRoute(SitePaths.START));
    // const activities: AccordionPanelComponent = this._AccordionController.New(`Atividades`, () => this._Routing.ToSiteRoute(SitePaths.CRUD));
    // activities.AddItem(`Cadastro`, () => this.AwaitingValidation(), null, null);
    // activities.AddItem(`Características`, () => this.AwaitingValidation(), null, null);
    // activities.AddItem(`Gerenciar Atividades`, () => this.AwaitingValidation(), null, null);
    // activities.AddItem(`Minhas Atividades`, () => this.AwaitingValidation(), null, null);

    const leads: AccordionPanelComponent = this._AccordionController.New(`Leads`, () => this._Routing.ToSiteRoute(SitePaths.START));
    leads.AddItem(`Agendamentos`, () => this._Routing.ToSiteRoute(SitePaths.LEAD_CALLBACK), null, null);
    leads.AddItem(`Novo Lead Móvel`, () => this._NewLeadMobile(), null, null);
    leads.AddItem(`Novo Lead Soho`, () => this._NewLeadSoho(), null, null);
    leads.AddItem(`Novo Lead Soho Alta Velocidade`, () => this._NewLeadHighSpeed(), null, null);
    leads.AddItem(`Novo Lead Sfa`, () => this._NewLeadSfa(), null, null);
    leads.AddItem(`Meus leads`, () => this._Routing.ToSiteRoute(SitePaths.LEAD_MINE), null, null);

    const crud: AccordionPanelComponent = this._AccordionController.New(`Cadastros`, () => this._Routing.ToSiteRoute(SitePaths.CRUD));
    // crud.AddItem(`Áreas de Atuação`, () => this.AwaitingValidation(), null, null);
    // crud.AddItem(`Contas`, () => this.AwaitingValidation(), null, null);
    // crud.AddItem(`Departamentos`, () => this.AwaitingValidation(), null, null);
    // crud.AddItem(`Mensagens de Bloqueio`, () => this.AwaitingValidation(), null, null);
    // crud.AddItem(`Permissões`, () => this.AwaitingValidation(), null, null);
    // crud.AddItem(`Posições`, () => this.AwaitingValidation(), null, null);
    // crud.AddItem(`Usuários`, () => this.AwaitingValidation(), null, null);

    const campaigns: AccordionPanelComponent = this._AccordionController.New(`Campanhas`, () => this._Routing.ToSiteRoute(SitePaths.CAMPAIGN_SCREEN));

    // const importing: AccordionPanelComponent = this._AccordionController.New(`Importação`, () => this._Routing.ToSiteRoute(SitePaths.START));
    // importing.AddItem(`Arquivo para base SFA`, () => this._Routing.ToSiteRoute(SitePaths.IMPORTING_COMPANIES), null, null);

    // const companies: AccordionPanelComponent = this._AccordionController.New(`Empresas`, () => this._Routing.ToSiteRoute(SitePaths.START));
    // companies.AddItem(`Lista`, () => this.AwaitingValidation(), null, null);
    // companies.AddItem(`A tabular`, () => this.AwaitingValidation(), null, null);
    // companies.AddItem(`Tabuladas`, () => this.AwaitingValidation(), null, null);

    const reports: AccordionPanelComponent = this._AccordionController.New(`Relatórios`);
    reports.AddItem(`Avaliações`, () => this._Routing.ToSiteRoute(SitePaths.REPORTS_EVALUATIONS));
    reports.AddItem(`Tabulações`, () => this._Routing.ToSiteRoute(SitePaths.REPORTS_TABULATIONS));
    reports.AddItem(`Mineração`, () => this._Routing.ToSiteRoute(SitePaths.REPORTS_MINING));
    reports.AddItem(`Ligações`, () => this._Routing.ToSiteRoute(SitePaths.REPORTS_CALLS));
    reports.AddItem(`Vendas`, () => this._Routing.ToSiteRoute(SitePaths.REPORTS_SELLS));

    const sales: AccordionPanelComponent = this._AccordionController.New(`Vendas`, () => this._Routing.ToSiteRoute(SitePaths.START));
    // sales.AddItem(`Não fechadas`, () => this.AwaitingValidation(), null, null);
  }

  public _GetPanel(panelName: string): AccordionPanelItemComponent
  {
    if (!ObjectUtils.NullOrUndefined(this.PanelItems[panelName]))
      return this.PanelItems[panelName];
    else
      return this.PanelSubItems[panelName];
  }

  public AwaitingValidation(): void
  {
    this._Snack.open(`Aguardando validação desta funcionalidade...`, `Aviso`);
  }

  public Hide(): void
  {
    InputDecoration.ElementFadeOut(this.Items, 50);
    InputDecoration.ElementClassAdd(`side-bar-body-component`, `side-bar-body-hidden`);
  }
  public Show(): void
  {
    InputDecoration.ElementFadeIn(this.Items);
    InputDecoration.ElementClassRemove(`side-bar-body-component`, `side-bar-body-hidden`);
  }
}
