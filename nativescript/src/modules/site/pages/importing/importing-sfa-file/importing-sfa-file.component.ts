import { Component, OnInit, ChangeDetectorRef, ViewChild, ElementRef, Input } from '@angular/core';
import { HostBindedComponent } from 'clericuzzi-lib/modules/shared/util-components/host-binded/host-binded.component';
import { PageTitleService } from 'clericuzzi-lib/services/page-title.service';
import { ComponentGeneratorService } from 'clericuzzi-lib/services/component-generator.service';
import { UserDataService } from 'clericuzzi-lib/services/user-data.service';
import { MatSnackBar } from '@angular/material';

import * as XLSX from 'xlsx';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

@Component({
  selector: 'app-importing-sfa-file',
  templateUrl: './importing-sfa-file.component.html',
})
export class ImportingSfaFileComponent extends HostBindedComponent implements OnInit
{
  constructor(
    public _Snack: MatSnackBar,
    public _UserData: UserDataService,
    private _Generator: ComponentGeneratorService,
    private _ChangeDetector: ChangeDetectorRef,
    public pageTitle: PageTitleService,
  )
  {
    super(pageTitle);
    this.InitFactories(this._Generator.Factory, this._Generator.Sanitizer);
    this.SetTitle(`Importar lista de empresas`);
  }

  ngOnInit()
  {
  }

  public UploadedFile(e): void
  {
     /* wire up file reader */
     const target: DataTransfer = <DataTransfer>(e.target);
     if (target.files.length !== 1) throw new Error('Cannot use multiple files');
     const reader: FileReader = new FileReader();
     reader.onload = (e: any) => {
       /* read workbook */
       const bstr: string = e.target.result;
       const wb: XLSX.WorkBook = XLSX.read(bstr, {type: 'binary'});

       /* grab first sheet */
       const wsname: string = wb.SheetNames[0];
       const ws: XLSX.WorkSheet = wb.Sheets[wsname];

       /* save data */
       console.log(XLSX.utils.sheet_to_json(ws, {header: 1}));
     };
     reader.readAsBinaryString(target.files[0]);
  }
  }
