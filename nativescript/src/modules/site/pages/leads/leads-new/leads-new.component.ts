import { MatSnackBar } from '@angular/material';
import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
// utils
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';
// models
import { SnackUtils } from '@src/external/plugin-utils/utils/snack-utils';
import { CompanyItem } from '@src/business/models/company-item.model';
// services
import { UserDataService } from 'clericuzzi-lib/services/user-data.service';
import { PageTitleService } from 'clericuzzi-lib/services/page-title.service';
import { CompanyActionsService } from '@src/services/company-actions.service';
import { ComponentGeneratorService } from 'clericuzzi-lib/services/component-generator.service';
// components
import { HostBindedComponent } from 'clericuzzi-lib/modules/shared/util-components/host-binded/host-binded.component';
import { CompanyInfoComponent } from '@src/modules/site/components/company/company-info/company-info.component';
// behaviours

@Component({
  selector: 'app-leads-new',
  templateUrl: './leads-new.component.html'
})
export class LeadsNewComponent extends HostBindedComponent implements OnInit
{
  private _Model: CompanyItem;
  private _InfoPanelLoaded: boolean = false;
  @ViewChild(`info`) private _InfoPanel: CompanyInfoComponent;
  constructor(
    public _Snack: MatSnackBar,
    public _UserData: UserDataService,
    private _Generator: ComponentGeneratorService,
    private _ChangeDetector: ChangeDetectorRef,
    public pageTitle: PageTitleService,

    private _CompanyActions: CompanyActionsService,
  )
  {
    super(pageTitle);
    this.InitFactories(this._Generator.Factory, this._Generator.Sanitizer);
    this.SetTitle(`Carregando cliente...`);
    this.AddClasses(`full-size`);
  }

  ngOnInit()
  {
    this._LoadLead();
  }
  private _LoadLead(): void
  {
    this._CompanyActions.NewLead(this._NewClient.bind(this));
  }

  public get ModelLoaded(): boolean
  {
    return !ObjectUtils.NullOrUndefined(this._Model);
  }

  private _NewClient(): void
  {
    let pageTitle: string = ``;
    if (this._CompanyActions.CampaignId === 1)
      pageTitle = `Cliente Soho`;
    else if (this._CompanyActions.CampaignId === 3)
      pageTitle = `Cliente Móvel`;
    else if (this._CompanyActions.CampaignId === 4)
      pageTitle = `Cliente Alta Velocidade`;
    this.SetTitle(pageTitle);
    this._Model = this._CompanyActions.CurrentModel;

    if (!this._InfoPanelLoaded)
    {
      this._ChangeDetector.detectChanges();
      this._InfoPanelLoaded = true;
      this._InfoPanel.ShowsNext();
      this.ListenTo(this._InfoPanel.OnNextLeadResquested, () => this._LoadLead());
    }

    if (!this.ModelLoaded)
      SnackUtils.OpenError(this._Snack, `Falha no carregamento do lead...`);
    else
      this._InfoPanel.InitModel();
  }
}
