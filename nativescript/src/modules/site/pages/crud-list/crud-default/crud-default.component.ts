import { MatDialog } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
// utils
import { SitePaths } from '@src/modules/site/site-paths.routes';
import { ServerUrls } from '@src/external/plugin-utils/utils/server-urls';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';
// models
import { Crudable } from '@src/external/plugin-utils/business/models/crud/crudable.model';
import { CompanyItem } from '@src/business/models/company-item.model';
import { CustomField } from '@src/external/plugin-utils/business/models/crud/custom-field.model';
import { ConfigUserItemComponent } from 'module-basic/modules/basic/components/config-user-item/config-user-item.component';
import { User, UserFilter, UserColumnsFilter, UserTable } from '@src/external/plugin-utils/business/models/models/user.model';
import { Company, CompanyFilter, CompanyColumnsFilter, CompanyColumnsUpdate, CompanyColumnsInsert } from '@src/entities/company.model';
import { UserArea, UserAreaFilter, UserAreaColumnsUpdate, UserAreaColumnsInsert, UserAreaColumnsFilter, UserAreaColumns } from '@src/entities/userArea.model';
import { Account, AccountFilter, AccountColumnsInsert, AccountColumnsFilter, AccountColumnsUpdate, AccountTable } from '@src/external/plugin-utils/business/models/models/account.model';
import { Tabulation, TabulationFilter, TabulationColumnsUpdate, TabulationColumnsInsert, TabulationColumnsFilter } from '@src/entities/tabulation.model';
import { Activity, ActivityFilter, ActivityColumnsUpdate, ActivityColumnsInsert, ActivityColumnsFilter, ActivityColumns } from '@src/external/plugin-utils/business/models/models/activity.model';
import { Permission, PermissionFilter, PermissionColumnsUpdate, PermissionColumnsInsert, PermissionColumnsFilter, PermissionColumns } from '@src/external/plugin-utils/business/models/models/permission.model';
import { BlockingMessage, BlockingMessageFilter, BlockingMessageColumnsInsert, BlockingMessageColumnsFilter, BlockingMessageColumnsUpdate } from '@src/entities/blockingMessage.model';
import { Characteristic, CharacteristicFilter, CharacteristicColumnsUpdate, CharacteristicColumnsInsert, CharacteristicColumnsFilter, CharacteristicColumns } from '@src/external/plugin-utils/business/models/models/characteristic.model';
// services
import { RoutingService } from 'clericuzzi-lib/services/routing.service';
import { EntitiesService } from 'clericuzzi-lib/services/entities.service';
import { UserDataService } from 'clericuzzi-lib/services/user-data.service';
import { PageTitleService } from 'clericuzzi-lib/services/page-title.service';
import { AutoCompleteFetchService } from 'clericuzzi-lib/services/auto-complete-fetch.service';
import { ComponentGeneratorService } from 'clericuzzi-lib/services/component-generator.service';
// components
import { CrudPopupComponent } from 'clericuzzi-lib/modules/shared/crud/crud-popup/crud-popup.component';
import { ItemCompanyComponent } from '@src/modules/site/components/list-items/item-company/item-company.component';
import { ItemUserAreaComponent } from '@src/modules/site/components/list-items/item-user-area/item-user-area.component';
import { ItemTabulationComponent } from '@src/modules/site/components/list-items/item-tabulation/item-tabulation.component';
import { AutocompleteInputComponent } from 'clericuzzi-lib/modules/shared/form-controls/autocomplete-input/autocomplete-input.component';
import { ConfigActivityItemComponent } from 'module-basic/modules/basic/components/config-activity-item/config-activity-item.component';
import { ItemBlockingMessageComponent } from '@src/modules/site/components/list-items/item-blocking-message/item-blocking-message.component';
import { ConfigPermissionItemComponent } from 'module-basic/modules/basic/components/config-permission-item/config-permission-item.component';
import { ConfigAccountListItemComponent } from 'module-basic/modules/basic/components/config-account-list-item/config-account-list-item.component';
import { ConfigCharacteristicItemComponent } from 'module-basic/modules/basic/components/config-characteristic-item/config-characteristic-item.component';
// behaviours
import { ListPage } from 'clericuzzi-lib/business/behaviours/list-page.behaviour';
import { GeographyStateTable } from '@src/external/plugin-utils/business/models/models-geo/geographyState.model';
import { ComponentTypeEnum } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';
import { GeographyCityTable } from '@src/external/plugin-utils/business/models/models-geo/geographyCity.model';
import { GeographyNeighbourhoodTable } from '@src/external/plugin-utils/business/models/models-geo/geographyNeighbourhood.model';
import { UserAreaInsertComponent } from '@src/modules/site/components/user-area/user-area-insert/user-area-insert.component';

@Component({
  selector: 'app-crud-default',
  templateUrl: './crud-default.component.html'
})
export class CrudDefaultComponent extends ListPage<Crudable, Crudable> implements OnInit
{
  private _LoadsOnStart: boolean = true;
  constructor(
    public diag: MatDialog,
    public userData: UserDataService,
    public generator: ComponentGeneratorService,
    public autoCompleteFetch: AutoCompleteFetchService,
    private _Route: ActivatedRoute,
    private _Routing: RoutingService,
    private _ModelActions: EntitiesService,

    public pageTitle: PageTitleService,
  )
  {
    super(diag, userData, generator, autoCompleteFetch, pageTitle);
    this.InitFactories(this.generator.Factory, this.generator.Sanitizer);
  }

  private _CurrentRoute: string;
  ngOnInit()
  {
    this._CurrentRoute = this._Routing.GetLastSegment(this._Route, 2);
    this._HandleRoute();

    if (!ObjectUtils.NullOrUndefined(this.BaseObject))
    {
      this.BaseObject.TableDefinition.CanDelete = this.CanDelete;
      this.Initialize(this.BaseObject, this.FilterObject);
      this.Component.Init(this.BaseObject, this.FilterObject, this.ItemComponent, false, this._LoadsOnStart);
      this.Component.InitFilters(... this.ColumnsFilter);

      this._FetchExternalData();
      this._CustomFilterActions();

      if (this.CanInsert)
        this.ActionNewItem();
      if (this.CanUpdate)
        this.ActionUpdateItem();

      this.ListenTo(this._ModelActions.OnRefreshRequested, () => this.Component.Reload());
      this.SetTitle(this.Title);
      this._CustomActions();
      if (!ObjectUtils.NullUndefinedOrEmpty(this.ListTitle))
        this.Component.PanelSelect.Title = this.ListTitle;
    }
  }

  private _FetchExternalData(): void
  {
    if (this._CurrentRoute == SitePaths.CRUD_PERMISSION)
    {
      this.Component.LoadExternalData(ServerUrls.GetUrl(`crud/${AccountTable}/listing`), new CustomField(AccountTable, PermissionColumns.AccountId));
      // this.Component.LoadExternalData(ServerUrls.GetUrl(`autocomplete/clients/list`), new CustomField(CharacteristicTable, `CharacteristicsWith`), new CustomField(CharacteristicTable, `CharacteristicsWithout`))
      // let componentWith: AutocompleteInputComponent<number> = this.Component.PanelFilter.GetComponent(`CharacteristicsWith`);
      // let componentWithout: AutocompleteInputComponent<number> = this.Component.PanelFilter.GetComponent(`CharacteristicsWithout`);
      // componentWith.MultiSelect = componentWithout.MultiSelect = true;
      // componentWith.Placeholder = `Com as características`;
      // componentWithout.Placeholder = `Sem as características`;
    }
    else if (this._CurrentRoute == SitePaths.CRUD_CHARACTERISTIC)
      this.Component.LoadExternalData(ServerUrls.GetUrl(`crud/${AccountTable}/listing`), new CustomField(AccountTable, CharacteristicColumns.AccountId));
    else if (this._CurrentRoute == SitePaths.USER_AREA)
      this.Component.LoadExternalData(ServerUrls.GetUrl(`listing/crud-user-area`), new CustomField(GeographyStateTable, `StateId`), new CustomField(UserTable, UserAreaColumns.UserId));
  }
  private _CustomFilterActions(): void
  {
    if (this._CurrentRoute == SitePaths.USER_AREA)
    {
      let cityComponent: AutocompleteInputComponent<number> = this.Component.PanelFilter.GetComponent(`CityId`);
      let userComponent: AutocompleteInputComponent<number> = this.Component.PanelFilter.GetComponent(`UserId`);
      let stateComponent: AutocompleteInputComponent<number> = this.Component.PanelFilter.GetComponent(`StateId`);
      let neighbourhoodComponent: AutocompleteInputComponent<number> = this.Component.PanelFilter.GetComponent(`NeighbourhoodId`);
      cityComponent.MultiSelect = userComponent.MultiSelect = stateComponent.MultiSelect = neighbourhoodComponent.MultiSelect = true;

      this.ListenTo(cityComponent.OnValueChanged, () => this._UserAreaCitySelected());
      this.ListenTo(stateComponent.OnValueChanged, () => this._UserAreaStateSelected());
    }
  }
  private _CustomActions(): void
  {
  }
  private _HandleRoute(): void
  {
    if (this._CurrentRoute == SitePaths.CRUD_USER)
    {
      this.BaseObject = new User();
      this.FilterObject = new UserFilter();
      this.ItemComponent = ConfigUserItemComponent;
      this.ColumnsFilter = UserColumnsFilter;
      this.ColumnsInsert = this.ColumnsUpdate = [];

      this.Component.PanelSelect.ShowsLoader(true);

      this.Title = `Cadastro de Usuários`;
      this.ListTitle = `Usuários`;

      this.CanDelete = this.CanInsert = this.CanUpdate = true;
    }
    else if (this._CurrentRoute == SitePaths.CRUD_ACCOUNT)
    {
      this.BaseObject = new Account();
      this.FilterObject = new AccountFilter();
      this.ItemComponent = ConfigAccountListItemComponent;
      this.ColumnsFilter = AccountColumnsFilter;
      this.ColumnsInsert = AccountColumnsInsert;
      this.ColumnsUpdate = AccountColumnsUpdate;

      this.Component.PanelSelect.ShowsLoader(true);

      this.Title = `Cadastro de Contas`;
      this.ListTitle = `Contas`;

      this.CanDelete = this.CanInsert = this.CanUpdate = true;
    }
    else if (this._CurrentRoute == SitePaths.CRUD_ACTIVITY)
    {
      this.BaseObject = new Activity();
      this.FilterObject = new ActivityFilter();
      this.ItemComponent = ConfigActivityItemComponent;
      this.ColumnsFilter = ActivityColumnsFilter;
      this.ColumnsInsert = ActivityColumnsInsert;
      this.ColumnsUpdate = ActivityColumnsUpdate;

      this.Component.PanelSelect.ShowsLoader(true);

      this.Title = `Cadastro de Atividades`;
      this.ListTitle = `Atividades`;

      this.CanDelete = this.CanInsert = this.CanUpdate = true;
    }
    else if (this._CurrentRoute == SitePaths.CRUD_PERMISSION)
    {
      this.BaseObject = new Permission();
      this.FilterObject = new PermissionFilter();
      this.ItemComponent = ConfigPermissionItemComponent;
      this.ColumnsFilter = PermissionColumnsFilter;
      this.ColumnsInsert = PermissionColumnsInsert;
      this.ColumnsUpdate = PermissionColumnsUpdate;

      this.Component.PanelSelect.ShowsLoader(true);

      this.Title = `Cadastro de Permissões`;
      this.ListTitle = `Permissões`;

      this.CanDelete = this.CanInsert = this.CanUpdate = true;
    }
    else if (this._CurrentRoute == SitePaths.CRUD_CHARACTERISTIC)
    {
      this.BaseObject = new Characteristic();
      this.FilterObject = new CharacteristicFilter();
      this.ItemComponent = ConfigCharacteristicItemComponent;
      this.ColumnsFilter = CharacteristicColumnsFilter;
      this.ColumnsInsert = CharacteristicColumnsInsert;
      this.ColumnsUpdate = CharacteristicColumnsUpdate;

      this.Component.PanelSelect.ShowsLoader(true);

      this.Title = `Cadastro de Características`;
      this.ListTitle = `Características`;

      this.CanDelete = this.CanInsert = this.CanUpdate = true;
    }

    else if (this._CurrentRoute == SitePaths.COMPANIES)
    {
      this.BaseObject = new CompanyItem();
      this.FilterObject = new CompanyFilter();
      this.ItemComponent = ItemCompanyComponent;
      this.ColumnsFilter = CompanyColumnsFilter;
      this.ColumnsInsert = CompanyColumnsInsert;
      this.ColumnsUpdate = CompanyColumnsUpdate;

      this.Component.PanelSelect.SortFuncion = function (x: CompanyItem, y: CompanyItem): number { return x.FantasyName > y.FantasyName ? 1 : -1; };
      this.Component.PanelSelect.ShowsLoader(true);

      this.Title = `Cadastro de Empresas`;
      this.ListTitle = `Empresas`;

      this.Component.PanelSelect.CustomSelect = ServerUrls.GetUrl(`companies/listing`);
      this.CanDelete = this.CanInsert = this.CanUpdate = false;
    }
    else if (this._CurrentRoute == SitePaths.TABULATION)
    {
      this.BaseObject = new Tabulation();
      this.FilterObject = new TabulationFilter();
      this.ItemComponent = ItemTabulationComponent;
      this.ColumnsFilter = TabulationColumnsFilter;
      this.ColumnsInsert = TabulationColumnsInsert;
      this.ColumnsUpdate = TabulationColumnsUpdate;

      this.Component.PanelSelect.SortFuncion = function (x: Tabulation, y: Tabulation): number { return x.Text > y.Text ? 1 : -1; };
      this.Component.PanelSelect.ShowsLoader(true);

      this.Title = `Cadastro de Tabulações`;
      this.ListTitle = `Tabulações`;

      this.CanDelete = this.CanInsert = this.CanUpdate = true;
    }
    else if (this._CurrentRoute == SitePaths.BLOCKING_MESSAGES)
    {
      this.BaseObject = new BlockingMessage();
      this.FilterObject = new BlockingMessageFilter();
      this.ItemComponent = ItemBlockingMessageComponent;
      this.ColumnsFilter = BlockingMessageColumnsFilter;
      this.ColumnsInsert = BlockingMessageColumnsInsert;
      this.ColumnsUpdate = BlockingMessageColumnsUpdate;

      this.Component.PanelSelect.SortFuncion = function (x: BlockingMessage, y: BlockingMessage): number { return x.Text > y.Text ? 1 : -1; };
      this.Component.PanelSelect.ShowsLoader(true);

      this.Title = `Cadastro de Mensagens de Bloqueio`;
      this.ListTitle = `Mensagens de Bloqueio`;

      this.CanDelete = this.CanInsert = this.CanUpdate = true;
    }
    else if (this._CurrentRoute == SitePaths.USER_AREA)
    {
      this.BaseObject = new UserArea();
      this.FilterObject = new UserAreaFilter();
      this.ItemComponent = ItemUserAreaComponent;
      this.ColumnsFilter = [UserAreaColumns.UserId, `StateId`, `CityId`, `NeighbourhoodId`];
      this.ColumnsInsert = UserAreaColumnsInsert;
      this.ColumnsUpdate = UserAreaColumnsUpdate;

      this.FilterObject.SetColumnDefinition(`StateId`, `state_id`, `StateId`, `Estado`, `Estados`, ComponentTypeEnum.AutoComplete);
      this.FilterObject.SetColumnDefinition(`CityId`, `city_id`, `CityId`, `Cidade`, `Cidades`, ComponentTypeEnum.AutoComplete);
      this.FilterObject.SetColumnDefinition(`NeighbourhoodId`, `neighbourhood_id`, `NeighbourhoodId`, `Bairro`, `Bairros`, ComponentTypeEnum.AutoComplete);

      // console.log(this.BaseObject.ColumnDefinitions.All);
      this.Component.PanelSelect.ShowsLoader(true);

      this.Title = `Cadastro de Áreas de Atuação`;
      this.ListTitle = `Áreas de Atuação`;

      this.CanDelete = this.CanInsert = this.CanUpdate = true;
      this.InsertItemComponent = UserAreaInsertComponent;
    }
  }

  private _UserAreaCitySelected(): void
  {
    let cityComponent: AutocompleteInputComponent<number> = this.Component.PanelFilter.GetComponent(`CityId`);
    let neighbourhoodComponent: AutocompleteInputComponent<number> = this.Component.PanelFilter.GetComponent(`NeighbourhoodId`);

    if (ObjectUtils.NullOrUndefined(cityComponent.Values) || ObjectUtils.NullUndefinedOrEmptyArray(cityComponent.Values))
    neighbourhoodComponent.ClearItems();
    else
      this.autoCompleteFetch.FetchData(ServerUrls.GetUrl(`geography/listing/neighbourhoods-by-cities`), neighbourhoodComponent, GeographyNeighbourhoodTable, new CustomField(`city-ids`, cityComponent.Values));
  }
  private _UserAreaStateSelected(): void
  {
    let cityComponent: AutocompleteInputComponent<number> = this.Component.PanelFilter.GetComponent(`CityId`);
    let stateComponent: AutocompleteInputComponent<number> = this.Component.PanelFilter.GetComponent(`StateId`);

    if (ObjectUtils.NullOrUndefined(stateComponent.Values) || ObjectUtils.NullUndefinedOrEmptyArray(stateComponent.Values))
      cityComponent.ClearItems();
    else
      this.autoCompleteFetch.FetchData(ServerUrls.GetUrl(`geography/listing/cities-by-states`), cityComponent, GeographyCityTable, new CustomField(`state-ids`, stateComponent.Values));
  }

  // these methods should be deleted if not used
  public AddItemAfterOpen(component: CrudPopupComponent): void
  {
    if (this._CurrentRoute == SitePaths.CRUD_ACTIVITY)
    {
      let accountComponent: AutocompleteInputComponent<number> = component.GetComponent(ActivityColumns.AccountId);
      if (!ObjectUtils.NullOrUndefined(accountComponent))
        component.LoadExternalData(ServerUrls.GetUrl(`crud/${AccountTable}/listing`), new CustomField(AccountTable, ActivityColumns.AccountId));
    }
    else if (this._CurrentRoute == SitePaths.CRUD_CHARACTERISTIC)
    {
      let accountComponent: AutocompleteInputComponent<number> = component.GetComponent(CharacteristicColumns.AccountId);
      if (!ObjectUtils.NullOrUndefined(accountComponent))
        component.LoadExternalData(ServerUrls.GetUrl(`crud/${AccountTable}/listing`), new CustomField(AccountTable, CharacteristicColumns.AccountId));
    }
  }
  public AddItemAfterClose(): void
  {
    this.Component.Reload();
  }
  public UpdateItemAfterOpen(component: CrudPopupComponent): void
  {
    if (this._CurrentRoute == SitePaths.CRUD_ACCOUNT)
    {
      let accountComponent: AutocompleteInputComponent<number> = component.GetComponent(ActivityColumns.AccountId);
      if (!ObjectUtils.NullOrUndefined(accountComponent))
        component.LoadExternalData(ServerUrls.GetUrl(`crud/${AccountTable}/listing`), new CustomField(AccountTable, ActivityColumns.AccountId));
    }
    else if (this._CurrentRoute == SitePaths.CRUD_ACCOUNT)
    {
      let accountComponent: AutocompleteInputComponent<number> = component.GetComponent(CharacteristicColumns.AccountId);
      if (!ObjectUtils.NullOrUndefined(accountComponent))
        component.LoadExternalData(ServerUrls.GetUrl(`crud/${AccountTable}/listing`), new CustomField(AccountTable, CharacteristicColumns.AccountId));
    }
  }
  public UpdateItemAfterClose(): void
  {
    this.Component.Reload();
  }
}