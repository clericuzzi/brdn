import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { HostBindedComponent } from 'clericuzzi-lib/modules/shared/util-components/host-binded/host-binded.component';
import { BiChartComponent } from 'clericuzzi-bi/modules/bi-components/bi-chart/bi-chart.component';
import { CrudFilterComponent } from 'clericuzzi-lib/modules/shared/crud/crud-filter/crud-filter.component';
import { EmployeeFilter } from '../../../../../business/filters/employee.filter';
import { MatSnackBar } from '@angular/material';
import { ReportService } from '../../../../../services/reports.service';
import { ComponentGeneratorService } from 'clericuzzi-lib/services/component-generator.service';
import { PageTitleService } from 'clericuzzi-lib/services/page-title.service';
import { CrudableTableColumnDefinitionModel, ComponentTypeEnum } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';
import { DateInputComponent } from 'clericuzzi-lib/modules/shared/form-controls/date-input/date-input.component';
import { SnackUtils } from '@src/external/plugin-utils/utils/snack-utils';
import { InputDecoration } from '@src/external/plugin-utils/utils/input-decoration';
import { ResponseMessage } from '@src/external/plugin-utils/business/models/response/response-message.model';
import { ReportEvaluation } from '@src/business/models/report-evaluations.model';
import { BiChartDataSet } from 'clericuzzi-bi/modules/bi-components/bi-chart/bi-chart-dataset.model';
import { BiChartDataSetItem } from 'clericuzzi-bi/modules/bi-components/bi-chart/bi-chart-dataset-item.model';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';
import { DateFilter } from '@src/business/filters/date.filter';
import { ReportCallsOrSales } from '@src/business/models/reports-calls-or-sales.model';
import { AutocompleteInputComponent } from 'clericuzzi-lib/modules/shared/form-controls/autocomplete-input/autocomplete-input.component';
import { ServerUrls } from '@src/external/plugin-utils/utils/server-urls';
import { AutoCompleteFetchService } from 'clericuzzi-lib/services/auto-complete-fetch.service';

@Component({
  selector: 'app-reports-tabulations',
  templateUrl: './reports-tabulations.component.html'
})
export class ReportsTabulationsComponent extends HostBindedComponent implements OnInit
{
  @ViewChild(`filter`) private _FilterContainer: CrudFilterComponent;
  @ViewChild(`container`) private _Container: ElementRef;

  @ViewChild(`chartPie`) private _ChartPie: BiChartComponent;
  @ViewChild(`chartLines`) private _ChartLines: BiChartComponent;

  private _Filter: EmployeeFilter = new EmployeeFilter();
  constructor(
    private _Snack: MatSnackBar,
    private _Reports: ReportService,
    private _Generator: ComponentGeneratorService,
    private _AutoComplete: AutoCompleteFetchService,
    public pageTitle: PageTitleService,
  )
  {
    super(pageTitle);
    this.InitFactories(this._Generator.Factory, this._Generator.Sanitizer);
    this.SetTitle(`Relatórios de tabulações por período`);
  }

  ngOnInit()
  {
    this._InitFilter();
  }
  private _InitFilter(): void
  {
    const toColumn: CrudableTableColumnDefinitionModel = new CrudableTableColumnDefinitionModel(`date_to`, `DateTo`, `Até`, `Atés`, ComponentTypeEnum.Date);
    const fromColumn: CrudableTableColumnDefinitionModel = new CrudableTableColumnDefinitionModel(`date_from`, `DateFrom`, `De`, `Des`, ComponentTypeEnum.Date);
    const employeeColumn: CrudableTableColumnDefinitionModel = new CrudableTableColumnDefinitionModel(`employee`, `EmployeeIds`, `Colaborador`, `Colaboradores`, ComponentTypeEnum.AutoComplete);

    const fieldFrom: DateInputComponent = this._FilterContainer.Addfilter<DateInputComponent>(fromColumn, fromColumn.ReadableName);
    const fieldTo: DateInputComponent = this._FilterContainer.Addfilter<DateInputComponent>(toColumn, toColumn.ReadableName);
    const fieldEmployee: AutocompleteInputComponent<number> = this._FilterContainer.Addfilter<AutocompleteInputComponent<number>>(employeeColumn, employeeColumn.ReadableName);
    fieldEmployee.MultiSelect = true;

    const date: Date = new Date();
    this._Filter = new EmployeeFilter(new Date(), new Date(date.getFullYear(), date.getMonth(), 1));

    fieldTo.Update(this._Filter);
    fieldFrom.Update(this._Filter);
    fieldEmployee.Update(this._Filter);

    this._AutoComplete.FetchData(ServerUrls.GetUrl(`listing/employees`), fieldEmployee, `user`);
    this.ListenTo(fieldTo.OnValueChanged, () => this._RequestReport());
    this.ListenTo(fieldFrom.OnValueChanged, () => this._RequestReport());
    this.ListenTo(this._FilterContainer.OnFilterRequested, () => this._RequestReport());
    this._RequestReport();
  }
  private _RequestReport(): void
  {
    if (this.Busy)
      SnackUtils.Open(this._Snack, `Aguarde o retorno do último relatório solicitado...`, `Aviso`);
    else
    {
      this.Busy = true;
      this._FilterContainer.Busy = true;
      this._Reports.Tabulations(this._Filter, this._ReportCallback.bind(this));
    }
  }
  private _ReportCallback(response: ResponseMessage<ReportCallsOrSales>): void
  {
    this.Busy = false;
    this._FilterContainer.Busy = false;
    InputDecoration.ElementFadeIn(this._Container);

    const reportData: ReportCallsOrSales = response.Parse(new ReportCallsOrSales());
    reportData.Parse();

    const colorData: string[] = [];
    for (let i = 0; i < reportData.Data.length; i++)
      colorData[i] = '#' + Math.floor(Math.random() * 16777215).toString(16);

    let colorIndex: number = 0;
    const datasetNames: string[] = reportData.Data.map(i => i.Item1);
    const datasets: BiChartDataSet[] = [];
    for (const datasetName of datasetNames)
    {
      let shouldAdd: boolean = false;
      const dataset: BiChartDataSet = new BiChartDataSet();
      dataset.Title = datasetName;
      dataset.borderColor = dataset.backgroundColor = [colorData[colorIndex++]];
      dataset.Items = [];
      const items: any[] = reportData.Data.find(i => i.Item1 === datasetName).Item2;
      for (const item of items)
      {
        if (!shouldAdd && item.Item2 > 0)
          shouldAdd = true;
        dataset.Items.push(new BiChartDataSetItem(item.Item1, item.Item2));
      }
      dataset.Update();

      if (shouldAdd)
        datasets.push(dataset);
    }

    this._ChartLines.Type = `line`;
    this._ChartLines.SetAxesNames(`Dias`, `qtd`);
    this._ChartLines.SetLabelDateRange(this._Filter.DateFrom, this._Filter.DateTo);
    this._ChartLines.SetTitle(`Relatório de ligações entre ${StringUtils.TransformDateToBr(this._Filter.DateFrom)} e ${StringUtils.TransformDateToBr(this._Filter.DateTo)}`);
    this._ChartLines.DataSets = datasets;
    this._ChartLines.Draw();

    colorIndex = 0;
    const distributionPieData: BiChartDataSet = new BiChartDataSet();
    distributionPieData.Items = [];
    distributionPieData.backgroundColor = [];
    for (const distribution of reportData.Distribution)
    {
      distributionPieData.backgroundColor.push(colorData[colorIndex++]);
      distributionPieData.Items.push(new BiChartDataSetItem(distribution.Item1, distribution.Item2));
    }

    this._ChartPie.HideAxes();
    this._ChartPie.HideLegend();
    this._ChartPie.PieAddDataset(`Distribuição`, distributionPieData);
  }
}
