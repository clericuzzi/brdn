import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { HostBindedComponent } from 'clericuzzi-lib/modules/shared/util-components/host-binded/host-binded.component';
import { BiChartComponent } from 'clericuzzi-bi/modules/bi-components/bi-chart/bi-chart.component';
import { CrudFilterComponent } from 'clericuzzi-lib/modules/shared/crud/crud-filter/crud-filter.component';
import { EmployeeFilter } from '../../../../../business/filters/employee.filter';
import { MatSnackBar } from '@angular/material';
import { ReportService } from '../../../../../services/reports.service';
import { ComponentGeneratorService } from 'clericuzzi-lib/services/component-generator.service';
import { PageTitleService } from 'clericuzzi-lib/services/page-title.service';
import { CrudableTableColumnDefinitionModel, ComponentTypeEnum } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';
import { DateInputComponent } from 'clericuzzi-lib/modules/shared/form-controls/date-input/date-input.component';
import { SnackUtils } from '@src/external/plugin-utils/utils/snack-utils';
import { InputDecoration } from '@src/external/plugin-utils/utils/input-decoration';
import { ResponseMessage } from '@src/external/plugin-utils/business/models/response/response-message.model';
import { ReportEvaluation } from '@src/business/models/report-evaluations.model';
import { BiChartDataSet } from 'clericuzzi-bi/modules/bi-components/bi-chart/bi-chart-dataset.model';
import { BiChartDataSetItem } from 'clericuzzi-bi/modules/bi-components/bi-chart/bi-chart-dataset-item.model';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';
import { DateFilter } from '@src/business/filters/date.filter';

@Component({
  selector: 'app-reports-smart',
  templateUrl: './reports-smart.component.html'
})
export class ReportsSmartComponent extends HostBindedComponent implements OnInit
{
  @ViewChild(`filter`) private _FilterContainer: CrudFilterComponent;
  @ViewChild(`container`) private _Container: ElementRef;

  @ViewChild(`chartAbr`) private _ChartAbr: BiChartComponent;
  @ViewChild(`chartCnpj`) private _ChartCnpj: BiChartComponent;
  @ViewChild(`chartLines`) private _ChartLines: BiChartComponent;
  @ViewChild(`chartZipNumber`) private _ChartZipNumber: BiChartComponent;

  private _Filter: DateFilter = new DateFilter();
  constructor(
    private _Snack: MatSnackBar,
    private _Reports: ReportService,
    private _Generator: ComponentGeneratorService,
    public pageTitle: PageTitleService,
  )
  {
    super(pageTitle);
    this.InitFactories(this._Generator.Factory, this._Generator.Sanitizer);
    this.SetTitle(`Relatórios por tipo de avaliação`);
  }

  ngOnInit()
  {
    this._InitFilter();
  }

  private _InitFilter(): void
  {
    const toColumn: CrudableTableColumnDefinitionModel = new CrudableTableColumnDefinitionModel(`date_to`, `DateTo`, `Até`, `Atés`, ComponentTypeEnum.Date);
    const fromColumn: CrudableTableColumnDefinitionModel = new CrudableTableColumnDefinitionModel(`date_from`, `DateFrom`, `De`, `Des`, ComponentTypeEnum.Date);

    const fieldFrom: DateInputComponent = this._FilterContainer.Addfilter<DateInputComponent>(fromColumn, fromColumn.ReadableName);
    const fieldTo: DateInputComponent = this._FilterContainer.Addfilter<DateInputComponent>(toColumn, toColumn.ReadableName);

    const date: Date = new Date();
    this._Filter = new EmployeeFilter(new Date(), new Date(date.getFullYear(), date.getMonth(), 1));

    fieldTo.Update(this._Filter);
    fieldFrom.Update(this._Filter);

    this.ListenTo(fieldTo.OnValueChanged, () => this._RequestReport());
    this.ListenTo(fieldFrom.OnValueChanged, () => this._RequestReport());
    this.ListenTo(this._FilterContainer.OnFilterRequested, () => this._RequestReport());
    this._RequestReport();
  }
  private _RequestReport(): void
  {
    if (this.Busy)
      SnackUtils.Open(this._Snack, `Aguarde o retorno do último relatório solicitado...`, `Aviso`);
    else
    {
      this.Busy = true;
      this._FilterContainer.Busy = true;
      this._Reports.Evaluations(this._Filter, this._ReportCallback.bind(this));
    }
  }
  private _ReportCallback(response: ResponseMessage<ReportEvaluation>): void
  {
    this.Busy = false;
    this._FilterContainer.Busy = false;
    InputDecoration.ElementFadeIn(this._Container);

    const reportData: ReportEvaluation = response.Parse(new ReportEvaluation());
    reportData.Parse();

    const abrPiedata: BiChartDataSet = new BiChartDataSet();
    abrPiedata.Items = [new BiChartDataSetItem(`Testadas`, reportData.AbrTested), new BiChartDataSetItem(`Não testadas`, reportData.AbrUntested)];
    abrPiedata.backgroundColor = [`#8CE78C`, `#FF3D67`];
    this._ChartAbr.HideGridLines();
    this._ChartAbr.PieAddDataset(`Linhas testadas`, abrPiedata);

    const cnpjPiedata: BiChartDataSet = new BiChartDataSet();
    cnpjPiedata.Items = [new BiChartDataSetItem(`Testadas`, reportData.ByCnpjTested), new BiChartDataSetItem(`Não testadas`, reportData.ByCnpjUntested)];
    cnpjPiedata.backgroundColor = [`#8CE78C`, `#FF3D67`];
    this._ChartCnpj.HideGridLines();
    this._ChartCnpj.PieAddDataset(`Empresas availiadas por cnpj`, cnpjPiedata);

    const zipNumberPiedata: BiChartDataSet = new BiChartDataSet();
    zipNumberPiedata.Items = [new BiChartDataSetItem(`Testados`, reportData.ByZipNumberTested), new BiChartDataSetItem(`Não testados`, reportData.ByZipNumberUntested)];
    zipNumberPiedata.backgroundColor = [`#8CE78C`, `#FF3D67`];
    this._ChartZipNumber.HideGridLines();
    this._ChartZipNumber.PieAddDataset(`Endereços availiados por cep e número`, zipNumberPiedata);

    const abrLine: BiChartDataSet = new BiChartDataSet();
    abrLine.Title = `# linhas testadas`;
    abrLine.borderColor = abrLine.backgroundColor = [`#EFBAF8`];
    abrLine.Items = [];
    for (const abrItem of reportData.Abr)
      abrLine.Items.push(new BiChartDataSetItem(abrItem.Item1, abrItem.Item2));
    abrLine.Update();

    const cnpjLine: BiChartDataSet = new BiChartDataSet();
    cnpjLine.Title = `# cnpjs testados`;
    cnpjLine.borderColor = cnpjLine.backgroundColor = [`#36A2EB`];
    cnpjLine.Items = [];
    for (const cnpjItem of reportData.ByCnpj)
      cnpjLine.Items.push(new BiChartDataSetItem(cnpjItem.Item1, cnpjItem.Item2));
    cnpjLine.Update();

    const zipNumberLine: BiChartDataSet = new BiChartDataSet();
    zipNumberLine.Title = `# endereços`;
    zipNumberLine.borderColor = zipNumberLine.backgroundColor = [`#8CE78C`];
    zipNumberLine.Items = [];
    for (const zipNumberItem of reportData.ByZipNumber)
      zipNumberLine.Items.push(new BiChartDataSetItem(zipNumberItem.Item1, zipNumberItem.Item2));
    zipNumberLine.Update();

    this._ChartLines.Type = `line`;
    this._ChartLines.SetAxesNames(`Dias`, `qtd`);
    this._ChartLines.SetLabelDateRange(this._Filter.DateFrom, this._Filter.DateTo);
    this._ChartLines.SetTitle(`Relatório da avaliações entre ${StringUtils.TransformDateToBr(this._Filter.DateFrom)} e ${StringUtils.TransformDateToBr(this._Filter.DateTo)}`);
    this._ChartLines.DataSets = [abrLine, cnpjLine, zipNumberLine];
    this._ChartLines.Draw();
  }
}
