import { Component, OnInit, ViewContainerRef, ViewChild } from '@angular/core';
import { HostBindedComponent } from 'clericuzzi-lib/modules/shared/util-components/host-binded/host-binded.component';
import { MatSnackBar } from '@angular/material';
import { ComponentGeneratorService } from 'clericuzzi-lib/services/component-generator.service';
import { AutoCompleteFetchService } from 'clericuzzi-lib/services/auto-complete-fetch.service';
import { PageTitleService } from 'clericuzzi-lib/services/page-title.service';
import { CampaignScreenModel } from '@src/business/models/campaign-screen.model';
import { CrudableTableColumnDefinitionModel, ComponentTypeEnum } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';
import { AutocompleteInputComponent } from 'clericuzzi-lib/modules/shared/form-controls/autocomplete-input/autocomplete-input.component';
import { BiCardComponent } from 'clericuzzi-bi/modules/bi-components/bi-card/bi-card.component';
import { ComboInputComponent } from 'clericuzzi-lib/modules/shared/form-controls/custom-inputs/combo-input/combo-input.component';
import { ServerUrls } from '@src/external/plugin-utils/utils/server-urls';
import { BatchResponseMessageList } from '@src/external/plugin-utils/business/models/response/batch-response-message.model';

@Component({
  selector: 'app-campaign-screen',
  templateUrl: './campaign-screen.component.html'
})
export class CampaignScreenComponent extends HostBindedComponent implements OnInit
{
  @ViewChild(`biCardToDo`) private _CardToDo: BiCardComponent;
  @ViewChild(`biCardTotal`) private _CardTotal: BiCardComponent;

  @ViewChild('filterCampaign', { read: ViewContainerRef }) _ContainerCampaign: ViewContainerRef;
  @ViewChild('filterCharacteristics', { read: ViewContainerRef }) _ContainerCharacteristics: ViewContainerRef;
  @ViewChild('filterCharacteristicsNot', { read: ViewContainerRef }) _ContainerCharacteristicsNot: ViewContainerRef;

  private _Campaign: AutocompleteInputComponent<number>;
  private _CharacteristicMust: AutocompleteInputComponent<number>;
  private _CharacteristicMustNot: AutocompleteInputComponent<number>;

  public Model: CampaignScreenModel = new CampaignScreenModel();
  constructor(
    private _Snack: MatSnackBar,
    private _Generator: ComponentGeneratorService,
    private _AutoComplete: AutoCompleteFetchService,
    public pageTitle: PageTitleService,
  )
  {
    super(pageTitle);
    this.InitFactories(this._Generator.Factory, this._Generator.Sanitizer);
    this.SetTitle(`Gerenciamento de campanhas`);
  }

  ngOnInit()
  {
    this._InitAutoCompletes();
  }
  private _InitAutoCompletes()
  {
    const campaignColumn: CrudableTableColumnDefinitionModel = new CrudableTableColumnDefinitionModel(`campaignId`, `CampaignId`, `Campanha`, `Campaigns`, ComponentTypeEnum.AutoComplete);
    const characteristicMustColumn: CrudableTableColumnDefinitionModel = new CrudableTableColumnDefinitionModel(`characteristicMust`, `CharacteristicsMust`, `Características não permitidas`, `Características não permitidas`, ComponentTypeEnum.AutoComplete);
    const characteristicMustNotColumn: CrudableTableColumnDefinitionModel = new CrudableTableColumnDefinitionModel(`characteristicMustNot`, `CharacteristicsMustNot`, `Características permitidas`, `Características permitidas`, ComponentTypeEnum.AutoComplete);

    this._Campaign = campaignColumn.CreateComponent(this._ContainerCampaign, this._Generator.Factory, this.Model, null, this._Generator.Sanitizer);
    this._CharacteristicMust = characteristicMustColumn.CreateComponent(this._ContainerCharacteristics, this._Generator.Factory, this.Model, null, this._Generator.Sanitizer);
    this._CharacteristicMustNot = characteristicMustNotColumn.CreateComponent(this._ContainerCharacteristicsNot, this._Generator.Factory, this.Model, null, this._Generator.Sanitizer);

    this._Campaign.MultiSelect = false;
    this._CharacteristicMust.MultiSelect = this._CharacteristicMustNot.MultiSelect = true;

    this._AutoComplete.FetchAutocompleteData(ServerUrls.GetUrl(`listing/campaign-screen`), this._FillCharacteristicsInfo.bind(this));
  }
  private _FillCharacteristicsInfo(response: BatchResponseMessageList): void
  {
    this._Campaign.SetItems(this._AutoComplete.GetAutocompleteData(`activity`, response));
    this._CharacteristicMust.SetItems(this._AutoComplete.GetAutocompleteData(`characteristic`, response));
    this._CharacteristicMustNot.SetItems(this._AutoComplete.GetAutocompleteData(`characteristic`, response));
  }
}
