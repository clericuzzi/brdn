import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
// utils
// models
// services
// components
import { CrudPopupComponent } from 'clericuzzi-lib/modules/shared/crud/crud-popup/crud-popup.component';
import { MatDialog, MatSnackBar } from '@angular/material';
import { HttpService } from 'clericuzzi-lib/services/http.service';
import { LoaderService } from 'clericuzzi-lib/services/loader.service';
import { ComponentGeneratorService } from 'clericuzzi-lib/services/component-generator.service';
import { AutoCompleteFetchService } from 'clericuzzi-lib/services/auto-complete-fetch.service';
import { ComponentTypeEnum, CrudableTableColumnDefinitionModel } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';
import { UserArea, UserAreaColumns } from '@src/entities/userArea.model';
import { UserTable } from '@src/external/plugin-utils/business/models/models/user.model';
import { CustomField } from '@src/external/plugin-utils/business/models/crud/custom-field.model';
import { GeographyStateTable } from '@src/external/plugin-utils/business/models/models-geo/geographyState.model';
import { ServerUrls } from '@src/external/plugin-utils/utils/server-urls';
import { AutocompleteInputComponent } from 'clericuzzi-lib/modules/shared/form-controls/autocomplete-input/autocomplete-input.component';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';
import { GeographyNeighbourhoodTable } from '@src/external/plugin-utils/business/models/models-geo/geographyNeighbourhood.model';
import { GeographyCityTable } from '@src/external/plugin-utils/business/models/models-geo/geographyCity.model';
// behaviours

@Component({
  selector: 'app-user-area-insert',
  templateUrl: './user-area-insert.component.html'
})
export class UserAreaInsertComponent extends CrudPopupComponent implements OnInit
{
  @ViewChild(`_container`, { read: ViewContainerRef }) private _Container: ViewContainerRef;
  constructor(
    private diag: MatDialog,
    private snack: MatSnackBar,

    private http: HttpService,
    private loader: LoaderService,
    private Generator: ComponentGeneratorService,
    private autoCompleteFetch: AutoCompleteFetchService,
  )
  {
    super(null, diag, null, snack, loader, Generator, autoCompleteFetch);
  }

  ngOnInit()
  {
    this.InitFactories(this.Generator.Factory, this.Generator.Sanitizer);
    this._InitComponents();
  }
  private _InitComponents(): void
  {
    let cityColumn: CrudableTableColumnDefinitionModel = new CrudableTableColumnDefinitionModel(`city_id`, `CityId`, `Cidade`, `Cidades`, ComponentTypeEnum.AutoComplete);
    let stateColumn: CrudableTableColumnDefinitionModel = new CrudableTableColumnDefinitionModel(`state_id`, `StateId`, `Estado`, `Estados`, ComponentTypeEnum.AutoComplete);
    let neighbourhoodColumn: CrudableTableColumnDefinitionModel = new CrudableTableColumnDefinitionModel(`neighbourhood_id`, `NeighbourhoodId`, `Bairro`, `Bairros`, ComponentTypeEnum.AutoComplete);
    neighbourhoodColumn.IsNullable = true;

    this.CurrentModel = new UserArea();
    this.AddComponent(this._Container, this.CurrentModel.GetColumnDefinition(UserAreaColumns.UserId));
    this.AddComponent(this._Container, stateColumn);
    this.AddComponent(this._Container, cityColumn);
    this.AddComponent(this._Container, neighbourhoodColumn);

    let cityComponent: AutocompleteInputComponent<number> = this.GetComponent<AutocompleteInputComponent<number>>(`CityId`);
    let stateComponent: AutocompleteInputComponent<number> = this.GetComponent<AutocompleteInputComponent<number>>(`StateId`);
    let neighbourhoodComponent: AutocompleteInputComponent<number> = this.GetComponent<AutocompleteInputComponent<number>>(`NeighbourhoodId`);
    cityComponent.MultiSelect = stateComponent.MultiSelect = neighbourhoodComponent.MultiSelect = true;

    this.ListenTo(cityComponent.OnValueChanged, () => this._UserAreaCitySelected());
    this.ListenTo(stateComponent.OnValueChanged, () => this._UserAreaStateSelected());

    this.LoadExternalData(ServerUrls.GetUrl(`listing/crud-user-area`), new CustomField(GeographyStateTable, `StateId`), new CustomField(UserTable, UserAreaColumns.UserId));
  }
  private _UserAreaCitySelected(): void
  {
    let cityComponent: AutocompleteInputComponent<number> = this.GetComponent(`CityId`);
    let neighbourhoodComponent: AutocompleteInputComponent<number> = this.GetComponent(`NeighbourhoodId`);

    if (ObjectUtils.NullOrUndefined(cityComponent.Values) || ObjectUtils.NullUndefinedOrEmptyArray(cityComponent.Values))
    neighbourhoodComponent.ClearItems();
    else
      this.autoCompleteFetch.FetchData(ServerUrls.GetUrl(`geography/listing/neighbourhoods-by-cities`), neighbourhoodComponent, GeographyNeighbourhoodTable, new CustomField(`city-ids`, cityComponent.Values));
  }
  private _UserAreaStateSelected(): void
  {
    let cityComponent: AutocompleteInputComponent<number> = this.GetComponent(`CityId`);
    let stateComponent: AutocompleteInputComponent<number> = this.GetComponent(`StateId`);

    if (ObjectUtils.NullOrUndefined(stateComponent.Values) || ObjectUtils.NullUndefinedOrEmptyArray(stateComponent.Values))
      cityComponent.ClearItems();
    else
      this.autoCompleteFetch.FetchData(ServerUrls.GetUrl(`geography/listing/cities-by-states`), cityComponent, GeographyCityTable, new CustomField(`state-ids`, stateComponent.Values));
  }
}