import { Component, OnInit } from '@angular/core';
import { MatDialog, MatSnackBar } from '@angular/material';
// utils
// models
import { CompanyItem } from '@src/business/models/company-item.model';
import { CompanyActionsService } from '@src/services/company-actions.service';
import { StatusResponseMessage } from '@src/external/plugin-utils/business/models/response/status-response-message.model';
import { SnackUtils } from '@src/external/plugin-utils/utils/snack-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';
import { MaterialPopupComponent } from 'clericuzzi-lib/modules/material/components/material-popup/material-popup.component';
import { CompanyAvailability } from '@src/entities/companyAvailability.model';
import { CompanyInfoComponent } from '../../../company/company-info/company-info.component';
// services
// components
// behaviours

@Component({
  selector: 'app-item-company-actions',
  templateUrl: './item-company-actions.component.html'
})
export class ItemCompanyActionsComponent implements OnInit
{
  public Model: CompanyItem;
  constructor(
    private _Diag: MatDialog,
    private _Snack: MatSnackBar,
    private _CompanyActions: CompanyActionsService,
  )
  {
  }

  ngOnInit()
  {
  }

  public Call(): void
  {
  }
  public ViewDetails(): void
  {
    this._CompanyActions.CurrentModel = this.Model;
    MaterialPopupComponent.Popup(this._Diag, CompanyInfoComponent, this.Model, component => this._ViewDetailsLoadModel(component), () => this._CompanyActions.Clear());
  }
  private _ViewDetailsLoadModel(component: CompanyInfoComponent): void
  {
    component.LoadModel();
  }

  public RequestCheck(): void
  {
    MaterialPopupComponent.Confirmation(this._Diag, this._RequestCheckAction.bind(this), null, `Solicitar teste de endereço`, `Deseja realmente solicitar um teste para a empresa selecionada?`);
  }
  private _RequestCheckAction(): void
  {
  }
  private _RequestCheckCallback(response: StatusResponseMessage): void
  {
    if (ObjectUtils.NullOrUndefined(response, response.Success))
    {
      SnackUtils.OpenError(this._Snack, `Falha no envio da solicitação`);
      this._ClearAvailability();
    }
    else
    {
      if (!ObjectUtils.NullUndefinedOrEmpty(response.ErrorMessage))
      {
        SnackUtils.OpenError(this._Snack, response.ErrorMessage);
        this._ClearAvailability();
      }
      else if (response.Success)
        SnackUtils.OpenSuccess(this._Snack, `Solicitação feita com sucesso`);
      else
        SnackUtils.OpenSuccess(this._Snack, `Falha no envio da solicitação`);
    }
  }

  private _ClearAvailability(): void
  {
  }
}
