import { MatDialog } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { Component, OnInit, ComponentFactoryResolver, ViewChild } from '@angular/core';
// utils
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';
// models
import { CompanyItem } from '@src/business/models/company-item.model';
// services
// components
import { ItemCompanyActionsComponent } from './item-company-actions/item-company-actions.component';
// behaviours
import { SelectListItem } from 'clericuzzi-lib/business/behaviours/select-list-item.behaviour';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';

@Component({
  selector: 'app-item-company',
  templateUrl: './item-company.component.html'
})
export class ItemCompanyComponent extends SelectListItem<CompanyItem> implements OnInit
{
  @ViewChild(`actions`) _ActionsPanel: ItemCompanyActionsComponent;
  constructor(
    private _Diag: MatDialog,

    public factory: ComponentFactoryResolver,
    public sanitizer: DomSanitizer,
  )
  {
    super(null);
    this.NonSelectable = true;
  }

  ngOnInit()
  {
    this._ActionsPanel.Model = this.Model;
  }

  public get Cnpj(): string
  {
    if (!ObjectUtils.NullOrUndefined(this.Model))
      return this.Model.Cnpj;
    else
      return null;
  }
  public get Name(): string
  {
    if (!ObjectUtils.NullOrUndefined(this.Model))
      return this.Model.FantasyName;
    else
      return null;
  }
  public get Phone(): string
  {
    if (!ObjectUtils.NullOrUndefined(this.Model))
      return `(${this.Model.Ddd}) ${this.Model.Phone}`;
    else
      return null;
  }
  public get Separator(): string
  {
    if (!ObjectUtils.NullOrUndefined(this.Model, this.Model.Cnpj))
    {
      if (ObjectUtils.NullUndefinedOrEmpty(this.Model.Cnpj))
        return null;
      else
        return ' - Cnpj:';
    }
    else
      return null;
  }
}
