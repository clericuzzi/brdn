import { Component, OnInit } from '@angular/core';
import { SelectListItem } from 'clericuzzi-lib/business/behaviours/select-list-item.behaviour';
import { CompanyContact } from '@src/entities/companyContact.model';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';

@Component({
  selector: 'app-item-company-contact',
  templateUrl: './item-company-contact.component.html'
})
export class ItemCompanyContactComponent extends SelectListItem<CompanyContact> implements OnInit
{
  constructor()
  {
    super(null);
  }

  ngOnInit()
  {
  }

  public get Name(): string
  {
    return ObjectUtils.ReturnIfNotNullNorUndefined(this.Model, this.Model.Name);
  }
  public get Email(): string
  {
    return ObjectUtils.ReturnIfNotNullNorUndefined(this.Model, this.Model.Email);
  }
  public get Line(): string
  {
    if (!ObjectUtils.NullOrUndefined(this.Model, this.Model.CompanyLineIdParent))
      return StringUtils.ToPhoneBr(this.Model.CompanyLineIdParent.Ddd + this.Model.CompanyLineIdParent.Phone);
    else
      return null;
  }
}
