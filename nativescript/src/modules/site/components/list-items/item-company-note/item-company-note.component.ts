import { Component, OnInit } from '@angular/core';
// utils
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';
// models
import { CompanyNote } from '@src/entities/companyNote.model';
// services
// components
// behaviours
import { SelectListItem } from 'clericuzzi-lib/business/behaviours/select-list-item.behaviour';

@Component({
  selector: 'app-item-company-note',
  templateUrl: './item-company-note.component.html'
})
export class ItemCompanyNoteComponent extends SelectListItem<CompanyNote> implements OnInit
{
  constructor()
  {
    super(null);
  }

  ngOnInit()
  {
  }

  public get DateTime(): string
  {
    var date: Date = ObjectUtils.ReturnIfNotNullNorUndefined(this.Model, this.Model.Timestamp);
    return StringUtils.TransformDateTimeToBr(date);
  }
  public get Creator(): string
  {
    return ObjectUtils.ReturnIfNotNullNorUndefined(this.Model, this.Model.UserIdParent, this.Model.UserIdParent.Name);
  }
  public get Comment(): string
  {
    let contact: string = ObjectUtils.ReturnIfNotNullNorUndefined(this.Model, this.Model.ContactIdParent, this.Model.ContactIdParent.Name);
    let comment: string = ObjectUtils.ReturnIfNotNullNorUndefined(this.Model, this.Model.Comment);

    if (!ObjectUtils.NullUndefinedOrEmpty(contact))
      return `${contact.toUpperCase()}: ${comment}`;
    else
      return comment;
  }
}