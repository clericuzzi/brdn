import { Component, OnInit } from '@angular/core';
// utils
// models
import { LineItem } from '@src/business/models/line-item.model';
// services
// components
// behaviours
import { SelectListItem } from 'clericuzzi-lib/business/behaviours/select-list-item.behaviour';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';

@Component({
  selector: 'app-item-company-line',
  templateUrl: './item-company-line.component.html'
})
export class ItemCompanyLineComponent extends SelectListItem<LineItem> implements OnInit
{
  constructor()
  {
    super(null);
  }
  ngOnInit()
  {
  }

  public get Number(): string
  {
    if (ObjectUtils.NullOrUndefined(this.Model))
      return null;
    else
      return StringUtils.ToPhoneBr(this.PhoneString);
  }
  public get Operator(): string
  {
    if (ObjectUtils.NullOrUndefined(this.Model.OperatorIdParent))
      return null;
    else
      return this.Model.OperatorIdParent.Name;
  }
  public get PhoneString(): string
  {
    if (ObjectUtils.NullUndefinedOrEmpty(this.Model.Ddd))
      return this.Model.Phone;
    else
    {
      if (ObjectUtils.NullUndefinedOrEmpty(this.Model.Phone))
        return this.Model.Ddd;
      else
        return this.Model.Ddd + this.Model.Phone;
    }
  }

  public get PortabilityDate(): string
  {
    try
    {
      return `desde ${StringUtils.TransformDateToBr(this.Model.OperatorSince)}`;
    }
    catch(err)
    {
      return null;
    }
  }
  public get HasPortabilityDate(): boolean
  {
    try
    {
      return !ObjectUtils.NullOrUndefined(this.Model, this.Model.OperatorSince);
    }
    catch(err)
    {
      return false;
    }
  }
}
