import { Component, OnInit } from '@angular/core';
// utils
// models
import { CompanyCallback } from '@src/entities/companyCallback.model';
import { ResponseMessage } from '@src/external/plugin-utils/business/models/response/response-message.model';
// services
import { CompanyActionsService } from '@src/services/company-actions.service';
// components
import { HostBindedComponent } from 'clericuzzi-lib/modules/shared/util-components/host-binded/host-binded.component';
import { UserDataService } from 'clericuzzi-lib/services/user-data.service';
import { MatSnackBar } from '@angular/material';
import { BatchResponseMessage } from '@src/external/plugin-utils/business/models/response/batch-response-message.model';
// behaviours

@Component({
  selector: 'app-comany-callback',
  templateUrl: './comany-callback.component.html'
})
export class ComanyCallbackComponent extends HostBindedComponent implements OnInit
{
  public Models: CompanyCallback[] = [];
  constructor(
    private _Snack: MatSnackBar,
    private _UserData: UserDataService,
    private _CompanyActions: CompanyActionsService,
  )
  {
    super(null);
  }

  ngOnInit()
  {
    this.ClearComponentClass();
    this.AddClasses(`full-width`);

    this.ListenTo(this._UserData.LoginService.UserValidated, () => this._Load());
    this.ListenTo(this._CompanyActions.OnCompanyCallbackAdded, callbackItem => this._Add(callbackItem));
  }
  private _Add(model: CompanyCallback): void
  {
    this.Models.push(model);
  }
  private _Load(): void
  {
    this.Busy = true;
    this.Models = [];
    this._CompanyActions.LoadUserCallbacks(this._LoadCallback.bind(this));
  }
  private _LoadCallback(response: BatchResponseMessage<CompanyCallback>): void
  {
    this.Busy = false;
    this.Models = ResponseMessage.HandleResponseList<CompanyCallback>(response, new CompanyCallback(), this._Snack);
  }

  public get HasModels(): boolean
  {
    try
    {
      return this.Models.length >= 1;
    }
    catch(err)
    {
      return false;
    }
  }
  public get HasNoCallbacks(): boolean
  {
    try
    {
      return this.Models.length == 0;
    }
    catch(err)
    {
      return false;
    }
  }
}
