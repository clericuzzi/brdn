import { Component, OnInit, Input } from '@angular/core';
import { HostBindedComponent } from 'clericuzzi-lib/modules/shared/util-components/host-binded/host-binded.component';
import { CompanyCallback } from '@src/entities/companyCallback.model';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';
import { CompanyActionsService } from '@src/services/company-actions.service';
import { MatDialog } from '@angular/material';
import { CompanyInfoComponent } from '../../company-info/company-info.component';
import { MaterialPopupComponent } from 'clericuzzi-lib/modules/material/components/material-popup/material-popup.component';
import { CompanyItem } from '@src/business/models/company-item.model';
import { CompanyCallbackLabelComponent } from '../../company-callback-label/company-callback-label.component';

@Component({
  selector: 'app-comany-callback-item',
  templateUrl: './comany-callback-item.component.html'
})
export class ComanyCallbackItemComponent extends HostBindedComponent implements OnInit
{
  @Input() public Model: CompanyCallback;
  constructor(
    private _Diag: MatDialog,
    private _CompanyActions: CompanyActionsService
  )
  {
    super(null);
  }

  ngOnInit()
  {
  }
  public get CompanyName(): string
  {
    try
    {
      return this.Model.CompanyIdParent.FantasyName;
    }
    catch (err)
    {
      return null;
    }
  }
  public get CallTime(): string
  {
    try
    {
      return StringUtils.TransformDateTimeToBr(this.Model.CallTime);
    }
    catch (err)
    {
      return null;
    }
  }
  public get DisplayClass(): string
  {
    try
    {
      return CompanyCallbackLabelComponent.GetCallbackClass(this.Model.CallTime) + ` flex-column`;
    }
    catch (err)
    {
      return `flex-column`;
    }
  }

  public ViewCompany(): void
  {
    const companyItem = new CompanyItem;
    companyItem.Id = this.Model.CompanyId;
    this._CompanyActions.CurrentModel = companyItem;
    MaterialPopupComponent.Popup(this._Diag, CompanyInfoComponent, this.Model, component => this._ViewDetailsLoadModel(component), () => this._CompanyActions.Clear());
  }
  private _ViewDetailsLoadModel(component: CompanyInfoComponent): void
  {
    component.LoadModel();
  }
}
