import { MatDialog, MatSnackBar } from '@angular/material';
import { Component, OnInit, ViewChild, ViewContainerRef, ElementRef } from '@angular/core';
// utils
import { ServerUrls } from '@src/external/plugin-utils/utils/server-urls';
import { SnackUtils } from '@src/external/plugin-utils/utils/snack-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';
import { InputDecoration } from '@src/external/plugin-utils/utils/input-decoration';
// models
import { Crudable } from '@src/external/plugin-utils/business/models/crud/crudable.model';
import { AddressItem } from '@src/business/models/address-item.model';
import { CustomField } from '@src/external/plugin-utils/business/models/crud/custom-field.model';
import { CompanyItem } from '@src/business/models/company-item.model';
import { TabulationTable } from '@src/entities/tabulation.model';
import { BatchResponseMessage } from '@src/external/plugin-utils/business/models/response/batch-response-message.model';
import { ContactPositionTable } from '@src/entities/contactPosition.model';
import { ContactDepartmentTable } from '@src/entities/contactDepartment.model';
import { CompanyAddressFilter, CompanyAddressColumnsUpdate, CompanyAddressColumnsInsert } from '@src/entities/companyAddress.model';
import { CompanyLine, CompanyLineFilter, CompanyLineColumnsInsert, CompanyLineColumnsUpdate, CompanyLineTable } from '@src/entities/companyLine.model';
import { CompanyCall, CompanyCallFilter, CompanyCallColumnsInsert, CompanyCallColumns, CompanyCallColumnsUpdate } from '@src/entities/companyCall.model';
import { CompanyNote, CompanyNoteFilter, CompanyNoteColumnsInsert, CompanyNoteColumns, CompanyNoteColumnsUpdate } from '@src/entities/companyNote.model';
import { CompanyContact, CompanyContactFilter, CompanyContactColumnsInsert, CompanyContactColumns, CompanyContactTable, CompanyContactColumnsUpdate } from '@src/entities/companyContact.model';
// services
import { UserDataService } from 'clericuzzi-lib/services/user-data.service';
import { CompanyActionsService } from '@src/services/company-actions.service';
// components
import { CrudPopupComponent } from 'clericuzzi-lib/modules/shared/crud/crud-popup/crud-popup.component';
import { HostBindedComponent } from 'clericuzzi-lib/modules/shared/util-components/host-binded/host-binded.component';
import { MaterialPopupComponent } from 'clericuzzi-lib/modules/material/components/material-popup/material-popup.component';
import { CrudPopupComponentModel } from 'clericuzzi-lib/modules/shared/crud/crud-popup/crud-popup.model';
import { ItemCompanyCallComponent } from '../../../list-items/item-company-call/item-company-call.component';
import { ItemCompanyLineComponent } from '../../../list-items/item-company-line/item-company-line.component';
import { ItemCompanyNoteComponent } from '../../../list-items/item-company-note/item-company-note.component';
import { ItemCompanyAddressComponent } from '../../../list-items/item-company-address/item-company-address.component';
import { ItemCompanyContactComponent } from '../../../list-items/item-company-contact/item-company-contact.component';
import { DefaultSelectWithFilterComponent } from 'module-basic/modules/basic/components/default-select-with-filter/default-select-with-filter.component';
import { LineItem } from '@src/business/models/line-item.model';
// behaviours

@Component({
  selector: 'app-company-info-relationships',
  templateUrl: './company-info-relationships.component.html'
})
export class CompanyInfoRelationshipsComponent extends HostBindedComponent implements OnInit
{
  public ComponentsReady: boolean = false;

  @ViewChild(`container`) private _Container: ElementRef;
  @ViewChild(`componentCall`) private _Calls: DefaultSelectWithFilterComponent<CompanyCall, CompanyCallFilter>;
  @ViewChild(`componentNote`) private _Notes: DefaultSelectWithFilterComponent<CompanyNote, CompanyNoteFilter>;
  @ViewChild(`componentLines`) private _Lines: DefaultSelectWithFilterComponent<LineItem, CompanyLineFilter>;
  @ViewChild(`componentContact`) private _Contacts: DefaultSelectWithFilterComponent<CompanyContact, CompanyContactFilter>;
  @ViewChild(`componentAddress`) private _Addresses: DefaultSelectWithFilterComponent<AddressItem, CompanyAddressFilter>;
  constructor(
    private _Diag: MatDialog,
    private _Snack: MatSnackBar,
    private _UserData: UserDataService,
    private _CompanyActions: CompanyActionsService,
  )
  {
    super(null);
  }

  private get _Model(): CompanyItem
  {
    return this._CompanyActions.CurrentModel;
  }
  ngOnInit()
  {
    this.Busy = true;
    this.AddClasses(`full-size`);
  }

  public Initialize(): void
  {
    if (this.ComponentsReady)
      return;

    this._InitCalls();
    this._InitNotes();
    this._InitLines();
    this._InitContacts();
    this._InitAddresses();

    this.ComponentsReady = true;
  }

  public SetModel(): void
  {
    this.Busy = false;
    InputDecoration.RemoveClass(this._Container, `visibiliity-fade-out`, `visibiliity-fade-removed`);

    this._Calls.PanelSelect.BaseObject.CompanyId = this._Model.Id;
    this._Notes.PanelSelect.BaseObject.CompanyId = this._Model.Id;
    this._Lines.PanelSelect.BaseObject.CompanyId = this._Model.Id;
    this._Contacts.PanelSelect.BaseObject.CompanyId = this._Model.Id;
    this._Addresses.PanelSelect.BaseObject.CompanyId = this._Model.Id;

    this._Calls.SetFilter(new CompanyCallFilter(null, null, [this._Model.Id]));
    this._Notes.SetFilter(new CompanyNoteFilter(null, null, [this._Model.Id]));
    this._Lines.SetFilter(new CompanyLineFilter(null, [ this._Model.Id ]));
    this._Contacts.SetFilter(new CompanyContactFilter(null, null, [this._Model.Id]));
    this._Addresses.SetFilter(new CompanyAddressFilter(null, [ this._Model.Id ]));

    this._Calls.SetItems(this._Model.Calls);
    this._Notes.SetItems(this._Model.Notes);
    this._Lines.SetItems(this._Model.Lines);
    this._Contacts.SetItems(this._Model.Contacts);
    this._Addresses.SetItems(this._Model.Addresses);
  }
  private _InitCalls(): void
  {
    const baseObject: CompanyCall = new CompanyCall();
    baseObject.UserId = this._UserData.Id;
    baseObject.CompanyId = this._Model.Id;
    baseObject.TableDefinition.CanDelete = false;
    const filterObject: CompanyCallFilter = new CompanyCallFilter(null, null, [this._Model.Id]);

    this._Calls.Init(baseObject, filterObject, ItemCompanyCallComponent, false, false);
    this._Calls.PanelSelect.HidePagination();
    this._Calls.PanelSelect.Title = `Últimas Ligações`;
    this._Calls.PanelSelect.ShowsFilter(false);
    this._Calls.PanelSelect.ShowsNoResultsMessage = false;

    this._Calls.AddAction(`hang`, `Tabular ligação`, () => this._HangUp(CrudPopupComponentModel.NewUpdate(this._Calls.PanelSelect.SelectedItem, ... CompanyCallColumnsUpdate), () => this._Calls.PanelSelect.Reload(), component => this._ListContactAndTabulation(component), `800px`, `350px`), false, `font-bold font-large fas fa-phone-slash color-delete`, null, 1);
    this._Calls.AddAction(`call`, `Nova ligação`, () => this._NewCall(), true, `font-bold font-large fas fa-phone color-ok`, null, 0);
  }
  private _InitNotes(): void
  {
    const baseObject: CompanyNote = new CompanyNote();
    baseObject.UserId = this._UserData.Id;
    baseObject.CompanyId = this._Model.Id;
    baseObject.TableDefinition.CanUpdate = true;
    baseObject.TableDefinition.CanDelete = false;
    const filterObject: CompanyNoteFilter = new CompanyNoteFilter(null, null, [this._Model.Id]);

    this._Notes.Init(baseObject, filterObject, ItemCompanyNoteComponent, false, false);
    this._Notes.PanelSelect.HidePagination();
    this._Notes.PanelSelect.Title = `Anotações`;
    this._Notes.PanelSelect.ShowsFilter(false);
    this._Notes.PanelSelect.ShowsNoResultsMessage = false;
    this._Notes.AddAction(`add`, baseObject.TableDefinition.NewItemTitle, () => this._AddItem(CrudPopupComponentModel.NewInsert(baseObject, ...CompanyNoteColumnsInsert), () => this._Notes.PanelSelect.Reload(), component => this._ListContactsForNotes(component)), true, `material-icons`, `add`, 0);
    this._Notes.AddAction(`edit`, baseObject.TableDefinition.NewItemTitle, () => this._UpdateItem(CrudPopupComponentModel.NewUpdate(this._Notes.PanelSelect.SelectedItem, ...CompanyNoteColumnsUpdate), () => this._Notes.PanelSelect.Reload(), component => this._ListContactsForCalls(component)), false, `material-icons`, `edit`, 1);
  }
  private _InitLines(): void
  {
    const baseObject: LineItem = new LineItem();
    baseObject.CompanyId = this._Model.Id;
    baseObject.TableDefinition.CanUpdate = true;
    baseObject.TableDefinition.CanDelete = false;
    const filterObject: CompanyLineFilter = new CompanyLineFilter(null, [ this._Model.Id ]);

    this._Lines.AddClasses(`l-margin`);
    this._Lines.Init(baseObject, filterObject, ItemCompanyLineComponent, false, false);
    this._Lines.PanelSelect.HidePagination();
    this._Lines.PanelSelect.Title = `Linhas`;
    this._Lines.PanelSelect.ShowsFilter(false);
    this._Lines.PanelSelect.ShowsNoResultsMessage = false;
    this._Lines.AddAction(`add`, baseObject.TableDefinition.NewItemTitle, () => this._AddItem(CrudPopupComponentModel.NewInsert(baseObject, ...CompanyLineColumnsInsert), () => this._Lines.PanelSelect.Reload(), component => this._ListContactsForNotes(component)), true, `material-icons`, `add`, 0);
    this._Lines.AddAction(`edit`, baseObject.TableDefinition.NewItemTitle, () => this._UpdateItem(CrudPopupComponentModel.NewUpdate(this._Lines.PanelSelect.SelectedItem, ...CompanyLineColumnsUpdate), () => this._Lines.PanelSelect.Reload(), component => this._ListContactsForCalls(component)), false, `material-icons`, `edit`, 1);
  }
  private _InitContacts(): void
  {
    const baseObject: CompanyContact = new CompanyContact();
    baseObject.UserId = this._UserData.Id;
    baseObject.CompanyId = this._Model.Id;
    baseObject.TableDefinition.CanUpdate = true;
    baseObject.TableDefinition.CanDelete = false;
    const filterObject: CompanyContactFilter = new CompanyContactFilter(null, null, [this._Model.Id]);

    this._Contacts.Init(baseObject, filterObject, ItemCompanyContactComponent, false, false);
    this._Contacts.PanelSelect.HidePagination();
    this._Contacts.PanelSelect.Title = `Contatos`;
    this._Contacts.PanelSelect.ShowsFilter(false);
    this._Contacts.PanelSelect.ShowsNoResultsMessage = false;

    this._Contacts.AddAction(`add`, baseObject.TableDefinition.NewItemTitle, () => this._AddItem(CrudPopupComponentModel.NewInsert(baseObject, ...CompanyContactColumnsInsert), () => this._Contacts.PanelSelect.Reload(), component => this._ListPositionDepartmentAndLines(component), `800px`, `400px`), true, `material-icons`, `add`, 0);
    this._Contacts.AddAction(`edit`, baseObject.TableDefinition.NewItemTitle, () => this._AddItem(CrudPopupComponentModel.NewUpdate(this._Contacts.PanelSelect.SelectedItem, ...CompanyContactColumnsUpdate), () => this._Contacts.PanelSelect.Reload(), component => this._ListPositionDepartmentAndLines(component), `800px`, `400px`), true, `material-icons`, `edit`, 1);
  }
  private _InitAddresses(): void
  {
    const baseObject: AddressItem = new AddressItem();
    baseObject.CompanyId = this._Model.Id;
    baseObject.TableDefinition.CanUpdate = true;
    baseObject.TableDefinition.CanDelete = false;
    const filterObject: CompanyAddressFilter = new CompanyAddressFilter(null, [ this._Model.Id ]);

    this._Addresses.RemoveClasses(`full-size`, `flex-column`);
    this._Addresses.Init(baseObject, filterObject, ItemCompanyAddressComponent, false, false);
    this._Addresses.PanelSelect.HidePagination();
    this._Addresses.PanelSelect.Title = `Endereços`;
    this._Addresses.PanelSelect.ShowsFilter(false);
    this._Addresses.PanelSelect.ShowsNoResultsMessage = false;

    this._Addresses.AddAction(`add`, baseObject.TableDefinition.NewItemTitle, () => this._AddItem(CrudPopupComponentModel.NewInsert(baseObject, ...CompanyAddressColumnsInsert), () => this._Addresses.PanelSelect.Reload(), component => this._ListPositionDepartmentAndLines(component), `800px`, `400px`), true, `material-icons`, `add`, 0);
    this._Addresses.AddAction(`edit`, baseObject.TableDefinition.NewItemTitle, () => this._AddItem(CrudPopupComponentModel.NewUpdate(this._Addresses.PanelSelect.SelectedItem, ...CompanyAddressColumnsUpdate), () => this._Addresses.PanelSelect.Reload(), component => this._ListPositionDepartmentAndLines(component), `800px`, `400px`), true, `material-icons`, `edit`, 1);
  }

  private _HangUp(popupModel: CrudPopupComponentModel, refreshAction: Function, initAction: Function = null, width: string = `600px`, height: string = `300px`): void
  {
    const call: CompanyCall = this._Calls.PanelSelect.SelectedItem as CompanyCall;
    if (call.UserId !== this._UserData.Id)
      SnackUtils.Open(this._Snack, `Não é possível alterar uma ligação que você não fez`, `Aviso`);
    else
    {
      popupModel.Title = `Tabular ligação`;
      popupModel.TitleActionConfirm = `Definir tabulação`;
      MaterialPopupComponent.Popup(this._Diag, CrudPopupComponent, popupModel, initAction, refreshAction, width, height);
    }
  }
  private _AddItem(popupModel: CrudPopupComponentModel, refreshAction: Function, initAction: Function = null, width: string = `600px`, height: string = `300px`): void
  {
    MaterialPopupComponent.Popup(this._Diag, CrudPopupComponent, popupModel, initAction, refreshAction, width, height);
  }
  private _UpdateItem(popupModel: CrudPopupComponentModel, refreshAction: Function, initAction: Function = null, width: string = `600px`, height: string = `300px`): void
  {
    if (!this._UpdateItemValidation(popupModel.BaseObject))
      SnackUtils.OpenError(this._Snack, `Não é possível alterar um item que você não criou...`);
    else
      MaterialPopupComponent.Popup(this._Diag, CrudPopupComponent, popupModel, initAction, refreshAction, width, height);
  }
  /**
   * checks if the given item was created by the current user
   * @param model the item to be updated
   */
  private _UpdateItemValidation(model: Crudable): boolean
  {
    const propertyName: string = `UserId`;
    if (ObjectUtils.NullOrUndefined(model[propertyName]))
      return true;
    else
    {
      const itemCreator: number = model[propertyName];
      return itemCreator === this._UserData.Id;
    }
  }

  private _ListContactsForCalls(component: CrudPopupComponent): void
  {
    component.LoadExternalDataWithParams(ServerUrls.GetUrl(`crud/${CompanyContactTable}/listing`), [new CustomField(`filter`, { CompanyId: [this._Model.Id] })], new CustomField(CompanyContactTable, CompanyCallColumns.ContactId));
  }
  private _ListContactsForNotes(component: CrudPopupComponent): void
  {
    component.LoadExternalDataWithParams(ServerUrls.GetUrl(`crud/${CompanyContactTable}/listing`), [new CustomField(`filter`, { CompanyId: [this._Model.Id] })], new CustomField(CompanyContactTable, CompanyNoteColumns.ContactId));
  }
  private _ListContactAndTabulation(component: CrudPopupComponent): void
  {
    component.LoadExternalDataWithParams(ServerUrls.GetUrl(`listing/contact-and-tabulation`), [new CustomField(`company-id`, this._Model.Id)], new CustomField(CompanyContactTable, CompanyCallColumns.ContactId), new CustomField(TabulationTable, CompanyCallColumns.TabulationId));
  }
  private _ListPositionDepartmentAndLines(component: CrudPopupComponent): void
  {
    component.LoadExternalDataWithParams(ServerUrls.GetUrl(`listing/crud-position-department-and-lines`), [ new CustomField(`company-id`, this._Model.Id) ], new CustomField(ContactPositionTable, CompanyContactColumns.PositionId), new CustomField(ContactDepartmentTable, CompanyContactColumns.DepartmentId), new CustomField(CompanyLineTable, CompanyContactColumns.CompanyLineId));
  }

  private _NewCall(): void
  {
    MaterialPopupComponent.Confirmation(this._Diag, this._NewCallAction.bind(this), null, `Ligar para o cliente`, `Iniciar uma nova ligação?`);
  }
  private _NewCallAction(): void
  {
    this._CompanyActions.NewCall(this._NewCallActionCallback.bind(this));
  }
  private _NewCallActionCallback(response: BatchResponseMessage<CompanyCall>): void
  {
    response.Parse(new CompanyCall());
    this._Calls.SetItems(response.Entities);
  }
}
