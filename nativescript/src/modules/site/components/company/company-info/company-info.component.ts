import { Subject } from 'rxjs';
import { Component, OnInit, ViewChild } from '@angular/core';
// utils
// Models
import { CompanyItem } from '@src/business/models/company-item.model';
// services
import { CompanyActionsService } from '@src/services/company-actions.service';
// components
import { HostBindedComponent } from 'clericuzzi-lib/modules/shared/util-components/host-binded/host-binded.component';
import { CompanyInfoHeaderComponent } from './company-info-header/company-info-header.component';
import { CompanyInfoRelationshipsComponent } from './company-info-relationships/company-info-relationships.component';
// behaviours

@Component({
  selector: 'app-company-info',
  templateUrl: './company-info.component.html'
})
export class CompanyInfoComponent extends HostBindedComponent implements OnInit
{
  public OnNextLeadResquested: Subject<void> = new Subject<void>();

  private _Model: CompanyItem;
  @ViewChild(`header`) _HeaderPanel: CompanyInfoHeaderComponent;
  @ViewChild(`relationships`) _RelationshipsPanel: CompanyInfoRelationshipsComponent;
  constructor(
    private _CompanyActions: CompanyActionsService,
  )
  {
    super(null);
    this.AddClasses(`full-size`);
  }

  ngOnInit()
  {
    this.ListenTo(this._CompanyActions.OnCompanyLoaded, () => this._ModelLoaded());
    this.ListenTo(this._HeaderPanel.OnNextLeadResquested, () => this.OnNextLeadResquested.next());
  }
  public ShowsNext(): void
  {
    this._HeaderPanel.ShowsNext = true;
  }
  public InitModel(): void
  {
    this._ModelLoaded();
  }
  public LoadModel(): void
  {
    this._CompanyActions.LoadModel();
  }
  private _InitModel(): void
  {
    this._RelationshipsPanel.Initialize();

    this._HeaderPanel.SetModel(this._Model);
    this._RelationshipsPanel.SetModel();
  }
  private _ModelLoaded(): void
  {
    this._HeaderPanel.Busy = false;
    this._Model = this._CompanyActions.CurrentModel;
    this._InitModel();
  }
}
