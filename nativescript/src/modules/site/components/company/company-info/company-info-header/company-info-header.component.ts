import { Subject } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatSnackBar } from '@angular/material';
// utils
import { ServerUrls } from '@src/external/plugin-utils/utils/server-urls';
import { CustomField } from '@src/external/plugin-utils/business/models/crud/custom-field.model';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';
// models
import { CompanyItem } from '@src/business/models/company-item.model';
import { CompanyContactTable } from '@src/entities/companyContact.model';
import { CompanyCallback, CompanyCallbackColumnsInsert, CompanyCallbackColumns } from '@src/entities/companyCallback.model';
// services
import { UserDataService } from 'clericuzzi-lib/services/user-data.service';
import { CompanyActionsService } from '@src/services/company-actions.service';
// components
import { DateInputComponent } from 'clericuzzi-lib/modules/shared/form-controls/date-input/date-input.component';
import { CrudPopupComponent } from 'clericuzzi-lib/modules/shared/crud/crud-popup/crud-popup.component';
import { MaterialPopupComponent } from 'clericuzzi-lib/modules/material/components/material-popup/material-popup.component';
import { CrudPopupComponentModel } from 'clericuzzi-lib/modules/shared/crud/crud-popup/crud-popup.model';
// behaviours

@Component({
  selector: 'app-company-info-header',
  templateUrl: './company-info-header.component.html'
})
export class CompanyInfoHeaderComponent implements OnInit
{
  public OnNextLeadResquested: Subject<void> = new Subject<void>();
  public ShowsNext: boolean = false;
  private _Model: CompanyItem;
  public Busy: boolean = true;
  public CheckRequestPending: boolean = false;
  constructor(
    private _Diag: MatDialog,
    private _Snack: MatSnackBar,
    private _UserData: UserDataService,
    private _CompanyActions: CompanyActionsService,
  )
  {
  }

  ngOnInit()
  {
  }

  public SetModel(model: CompanyItem): void
  {
    this._Model = model;
  }

  public NewCallback(): void
  {
    const baseObject: CompanyCallback = new CompanyCallback();
    baseObject.UserId = this._UserData.Id;
    baseObject.ProductId = 1;
    baseObject.CompanyId = this._Model.Id;

    MaterialPopupComponent.Popup(this._Diag, CrudPopupComponent, CrudPopupComponentModel.NewInsert(baseObject, ...CompanyCallbackColumnsInsert), component => this._ListContactsForCallback(component), null, `800px`, `240px`);
  }
  private _ListContactsForCallback(component: CrudPopupComponent): void
  {
    const callTimeComponent: DateInputComponent = component.GetComponent(CompanyCallbackColumns.CallTime);
    callTimeComponent.ShowHour(`hora`, 8, 21, `h`);
    callTimeComponent.ShowMinute(`minuto`, 0, 59, `m`);
    component.LoadExternalDataWithParams(ServerUrls.GetUrl(`crud/${CompanyContactTable}/listing`), [new CustomField(`filter`, { CompanyId: [this._Model.Id] })], new CustomField(CompanyContactTable, CompanyCallbackColumns.ContactId));
    component.Definitions.SendArray = false;
    component.Definitions.RequestUrl = ServerUrls.GetUrl(`companies/new-callback`);
  }
  public NextLead(): void
  {
    MaterialPopupComponent.Confirmation(this._Diag, () => this.OnNextLeadResquested.next(), null, `Próximo lead`, `Deseja realmente passar para o próximo lead?`);
  }

  public get CompanyName(): string
  {
    try
    {
      if (!ObjectUtils.NullOrUndefined(this._Model, this._Model.FantasyName))
        return this._Model.FantasyName;
      else
        return null;
    }
    catch (err)
    {
      return null;
    }
  }
  public get Phone(): string
  {
    try
    {
      if (!ObjectUtils.NullOrUndefined(this._Model))
        return StringUtils.ToPhoneBr(`${this._Model.Ddd}${this._Model.Phone}`);
      else
        return null;
    }
    catch (err)
    {
      return null;
    }
  }
  public get Cnpj(): string
  {
    try
    {
      if (!ObjectUtils.NullOrUndefined(this._Model, this._Model.Cnpj))
        return this._Model.Cnpj;
      else
        return null;
    }
    catch (err)
    {
      return null;
    }
  }

  public get HasCnpj(): boolean
  {
    try
    {
      return !ObjectUtils.NullOrUndefined(this._Model, this._Model.Cnpj);
    }
    catch(err)
    {
      return null;
    }
  }
  public get HasPhone(): boolean
  {
    try
    {
      return !ObjectUtils.NullOrUndefined(this.Phone);
    }
    catch(err)
    {
      return null;
    }
  }

  public get HasCallback(): boolean
  {
    try
    {
      return !ObjectUtils.NullOrUndefined(this._Model, this._Model.Callback);
    }
    catch (err)
    {
      return true;
    }
  }
  public get Callback(): CompanyCallback
  {
    try
    {
      if (!ObjectUtils.NullOrUndefined(this._Model, this._Model.Callback, this._Model.Callback.CallTime))
        return this._Model.Callback;
      else
        return null;
    }
    catch(err)
    {
      return null;
    }
  }
}
