import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

// modules
import { SharedModule } from 'clericuzzi-lib/modules/shared/shared.module';
import { BiComponentsModule } from 'clericuzzi-bi/modules/bi-components/bi-components.module';
import { ClericuzziModuleBasic } from 'module-basic/modules/basic/clericuzzi-module-basic.module';

// utils
import { SitePaths } from './site-paths.routes';
import { SiteComponent } from './site.component';

// services
import { RoleGuardService } from '@src/services/auth/role-guard.service';

// project components
import { StructureHeaderComponent } from './structure/structure-header/structure-header.component';
import { StructureSideBarComponent } from './structure/structure-side-bar/structure-side-bar.component';
import { StructureSideBarAccountPickerComponent } from './structure/structure-side-bar/structure-side-bar-account-picker/structure-side-bar-account-picker.component';

// config

// project items
import { CrudListComponent } from './pages/crud-list/crud-list.component';
import { CrudDefaultComponent } from './pages/crud-list/crud-default/crud-default.component';
import { ItemCompanyComponent } from './components/list-items/item-company/item-company.component';
import { ItemCompanyActionsComponent } from './components/list-items/item-company/item-company-actions/item-company-actions.component';
import { ItemTabulationComponent } from './components/list-items/item-tabulation/item-tabulation.component';
import { ItemBlockingMessageComponent } from './components/list-items/item-blocking-message/item-blocking-message.component';
import { ItemUserAreaComponent } from './components/list-items/item-user-area/item-user-area.component';
import { UserAreaInsertComponent } from './components/user-area/user-area-insert/user-area-insert.component';
import { UserAreaUpdateComponent } from './components/user-area/user-area-update/user-area-update.component';
import { CompanyInfoComponent } from './components/company/company-info/company-info.component';
import { CompanyCallComponent } from './components/company/company-call/company-call.component';
import { ItemCompanyCallComponent } from './components/list-items/item-company-call/item-company-call.component';
import { ItemCompanyContactComponent } from './components/list-items/item-company-contact/item-company-contact.component';
import { ItemCompanyNoteComponent } from './components/list-items/item-company-note/item-company-note.component';
import { CompanyInfoHeaderComponent } from './components/company/company-info/company-info-header/company-info-header.component';
import { CompanyInfoRelationshipsComponent } from './components/company/company-info/company-info-relationships/company-info-relationships.component';
import { LeadsNewComponent } from './pages/leads/leads-new/leads-new.component';
import { LeadsMineComponent } from './pages/leads/leads-mine/leads-mine.component';
import { LeadsCallbackComponent } from './pages/leads/leads-callback/leads-callback.component';
import { ReportsMiningComponent } from './pages/reports/reports-mining/reports-mining.component';
import { ReportsCallsComponent } from './pages/reports/reports-calls/reports-calls.component';
import { ItemCompanyAddressComponent } from './components/list-items/item-company-address/item-company-address.component';
import { ItemCompanyLineComponent } from './components/list-items/item-company-line/item-company-line.component';
import { ComanyCallbackComponent } from './components/company/comany-callback/comany-callback.component';
import { ComanyCallbackItemComponent } from './components/company/comany-callback/comany-callback-item/comany-callback-item.component';
import { CompanyCallbackLabelComponent } from './components/company/company-callback-label/company-callback-label.component';
import { ImportingSfaFileComponent } from './pages/importing/importing-sfa-file/importing-sfa-file.component';
import { ReportsSmartComponent } from './pages/reports/reports-smart/reports-smart.component';
import { ReportsSellsComponent } from './pages/reports/reports-sells/reports-sells.component';
import { CampaignScreenComponent } from './pages/campaign/campaign-screen/campaign-screen.component';
import { ReportsTabulationsComponent } from './pages/reports/reports-tabulations/reports-tabulations.component';

// Routing
export const routes =
[
  {
    path: SitePaths.INDEX, component: SiteComponent
    , children:
    [
      { path: SitePaths.START, component: CrudListComponent },

      // campaigns
      { path: SitePaths.CAMPAIGN_SCREEN, component: CampaignScreenComponent },

      // importing companies
      { path: SitePaths.IMPORTING_COMPANIES, component: ImportingSfaFileComponent },

      // company pages
      { path: SitePaths.COMPANIES, component: CrudDefaultComponent },
      { path: SitePaths.USER_AREA, component: CrudDefaultComponent },
      { path: SitePaths.TABULATION, component: CrudDefaultComponent },
      { path: SitePaths.BLOCKING_MESSAGES, component: CrudDefaultComponent },

      // leads
      { path: SitePaths.LEAD_NEW, component: LeadsNewComponent },
      { path: SitePaths.LEAD_MINE, component: LeadsMineComponent },
      { path: SitePaths.LEAD_CALLBACK, component: LeadsCallbackComponent },

      // REPORTS
      { path: SitePaths.REPORTS_CALLS, component: ReportsCallsComponent },
      { path: SitePaths.REPORTS_SELLS, component: ReportsSellsComponent },
      { path: SitePaths.REPORTS_MINING, component: ReportsMiningComponent },
      { path: SitePaths.REPORTS_EVALUATIONS, component: ReportsSmartComponent },
      { path: SitePaths.REPORTS_TABULATIONS, component: ReportsTabulationsComponent },

      // crud pages
      { path: SitePaths.CRUD_USER, component: CrudDefaultComponent },
      { path: SitePaths.CRUD_ACCOUNT, component: CrudDefaultComponent },
      { path: SitePaths.CRUD_ACTIVITY, component: CrudDefaultComponent },
      { path: SitePaths.CRUD_PERMISSION, component: CrudDefaultComponent },
      { path: SitePaths.CRUD_CHARACTERISTIC, component: CrudDefaultComponent },
    ]
  },
];

@NgModule({
  imports: [
    SharedModule,
    BiComponentsModule,
    ClericuzziModuleBasic,

    RouterModule.forChild(routes)
  ],
  exports: [
    SiteComponent,
  ],
  declarations: [
    SiteComponent,

    StructureHeaderComponent,
    StructureSideBarComponent,
    StructureSideBarAccountPickerComponent,

    CrudListComponent,
    CrudDefaultComponent,
    ItemUserAreaComponent,
    ItemTabulationComponent,
    ItemBlockingMessageComponent,

    ItemCompanyComponent,
    ItemCompanyCallComponent,
    ItemCompanyLineComponent,
    ItemCompanyNoteComponent,
    ItemCompanyActionsComponent,
    ItemCompanyAddressComponent,
    ItemCompanyContactComponent,

    UserAreaInsertComponent,
    UserAreaUpdateComponent,

    CompanyInfoComponent,
    CompanyCallComponent,
    CompanyInfoHeaderComponent,
    CompanyInfoRelationshipsComponent,
    // leads
    LeadsNewComponent,
    LeadsMineComponent,
    LeadsCallbackComponent,
    // reports
    ReportsMiningComponent,
    ReportsCallsComponent,

    // callback
    ComanyCallbackComponent,
    ComanyCallbackItemComponent,
    CompanyCallbackLabelComponent,
    ImportingSfaFileComponent,
    ReportsSmartComponent,
    ReportsSellsComponent,
    CampaignScreenComponent,
    ReportsTabulationsComponent,
  ],
  entryComponents: [
    ItemCompanyComponent,
    ItemUserAreaComponent,
    ItemTabulationComponent,
    ItemCompanyCallComponent,
    ItemCompanyLineComponent,
    ItemCompanyNoteComponent,
    ItemCompanyAddressComponent,
    ItemCompanyContactComponent,
    ItemBlockingMessageComponent,

    CompanyInfoComponent,
    CompanyCallComponent,

    UserAreaInsertComponent,
    UserAreaUpdateComponent,
  ]
})
export class SiteModule { }
