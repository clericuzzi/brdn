import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';
import { PropertyConverter } from 'nativescript-ui-dataform';

export class DecimalToStringConverter implements PropertyConverter
{
    constructor()
    {
    }

    public convertFrom(value: number): string
    {
        if (ObjectUtils.NullOrUndefined(value))
            return null;
        else
            return value.toString();
    }
    public convertTo(value: string): number
    {
        try
        {
            if (ObjectUtils.NullOrUndefined(value))
                return 0;
            else
            {
                const numberValue: number = Number.parseFloat(value);

                return numberValue;
            }
        }
        catch (err)
        {
            return 0;
        }
    }
}