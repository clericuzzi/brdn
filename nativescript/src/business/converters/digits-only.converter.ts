import { PropertyConverter } from 'nativescript-ui-dataform';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';

export class DigitsOnlyConverter implements PropertyConverter
{
    constructor(
        public Value: string = null,
    )
    {
    }

    public convertFrom(value: string): string
    {
        return StringUtils.DigitsOnly(value);
    }
    public convertTo(value: string): string
    {
        return StringUtils.DigitsOnly(value);
    }
}