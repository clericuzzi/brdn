import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';
import { PropertyConverter } from 'nativescript-ui-dataform';

export class NumberToStringConverter implements PropertyConverter
{
    constructor(input: number = 0)
    {
    }

    public convertFrom(value: string): number
    {
        try
        {
            if (ObjectUtils.NullOrUndefined(value))
                return 0;
            else
            {
                const numberValue: number = Number.parseInt(value, 10);

                return numberValue;
            }
        }
        catch (err)
        {
            return 0;
        }
    }
    public convertTo(value: number): string
    {
        if (ObjectUtils.NullOrUndefined(value))
            return null;
        else
            return value.toString();
    }
}