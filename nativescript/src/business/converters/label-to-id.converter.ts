import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';
import { PropertyConverter } from 'nativescript-ui-dataform';

const ID_PROPERTY: string = `key`;
const LABEL_PROPERTY: string = `label`;
export class LabelToIdConverter implements PropertyConverter
{
    constructor(
        public Items: any[],
        public IdProperty: string,
        public LabelProperty: string,
    )
    {
        this.Items = this.Items.map(i => ({ key: Number.parseInt(i[this.IdProperty], 10), label: i[this.LabelProperty] }));
    }

    public convertFrom(value: number): string
    {
        try
        {
            const selectedValue: any = this.Items.find(i => i[ID_PROPERTY] === value);
            if (!ObjectUtils.NullOrUndefined(selectedValue))
                return selectedValue[LABEL_PROPERTY];
            else
                return `valor não encontrado`;
        }
        catch (err)
        {
            return `valor não encontrado`;
        }
    }
    public convertTo(value: string): number
    {
        try
        {
            const selectedValue: any = this.Items.find(i => i[LABEL_PROPERTY] === value);
            if (!ObjectUtils.NullOrUndefined(selectedValue))
                return selectedValue[ID_PROPERTY];
            else
                return null;
        }
        catch (err)
        {
            return null;
        }
    }
}