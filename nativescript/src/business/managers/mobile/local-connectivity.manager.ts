import { Subject } from 'rxjs';
import { NamedSubscriptionManager } from '@src/external/plugin-utils/business/models/subscriptions/named-subscription-manager.model';
import { PluginUtilsDialogManager } from '@src/external/plugin-utils/business/managers/dialogs.manager';
import { PluginUtilsConnectivityManager } from '@src/external/plugin-utils/business/managers/connectivity.manager';

export class LocalConnectivityManager
{
    public static readonly OFFLINE_MESSAGE: string = `Você está offline`;

    private static _Unsubscribe: Subject<void> = new Subject<void>();
    private static _SubscriptionManager: NamedSubscriptionManager;
    private static _EventFiringCooldown: boolean = false;

    public static Init(): void
    {
        this._SubscriptionManager = new NamedSubscriptionManager();
        this._SubscriptionManager.Subscribe(`went-online`, PluginUtilsConnectivityManager.OnWentOnline, this._Unsubscribe, () => this._ConnectivityChanged());
        this._SubscriptionManager.Subscribe(`went-offline`, PluginUtilsConnectivityManager.OnWentOffline, this._Unsubscribe, () => this._ConnectivityChanged());
    }
    private static _ConnectivityChanged(): void
    {
        if (!this._EventFiringCooldown)
        {
            const shouldAppear: boolean = !this._EventFiringCooldown;
            const connType: string = PluginUtilsConnectivityManager.GetConnectionType();

            this._EventFiringCooldown = true;
            setTimeout(() => this._EventFiringCooldown = false, 1250);
        }
    }

    public static Check(): boolean
    {
        if (!PluginUtilsConnectivityManager.Online)
            this.ToastOffline();

        return PluginUtilsConnectivityManager.Online;
    }
    public static ToastOffline(): void
    {
        PluginUtilsDialogManager.Toast(this.OFFLINE_MESSAGE);
    }
}