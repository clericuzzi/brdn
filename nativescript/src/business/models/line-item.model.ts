import { CompanyLine } from '@src/entities/companyLine.model';
import { CompanyLineOperator } from '@src/entities/companyLineOperator.model';
import { DateUtils } from '@src/external/plugin-utils/utils/date-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';
import { Company } from '@src/entities/company.model';
import { PhoneOperator } from '@src/entities/phoneOperator.model';
import { JsonUtils } from '@src/external/plugin-utils/utils/json-utils';

export class LineItem extends CompanyLine
{
  constructor(
    public Id: number = null,
    public CompanyId: number = null,
    public Ddd: string = null,
    public Phone: string = null,
    public OperatorId: number = null,
    public OperatorSince: Date = null,
    public TestingTimestamp: Date = null,

    public Operators: CompanyLineOperator[] = null,
  )
  {
    super();
  }
  public ParseFromJson(json: any): void
  {
    if (json)
    {
      this.Id = json['Id'];
      this.Ddd = json['Ddd'];
      this.Phone = json['Phone'];
      this.CompanyId = json['CompanyId'];
      this.OperatorId = json['OperatorId'];
      this.OperatorSince = DateUtils.FromDateServer(json['OperatorSince']);
      this.TestingTimestamp = DateUtils.FromDateServer(json['TestingTimestamp']);

      if (!ObjectUtils.NullOrUndefined(json['CompanyIdParent']))
      {
        this.CompanyIdParent = new Company();
        this.CompanyIdParent.ParseFromJson(json['CompanyIdParent']);
      }
      if (!ObjectUtils.NullOrUndefined(json['OperatorIdParent']))
      {
        this.OperatorIdParent = new PhoneOperator();
        this.OperatorIdParent.ParseFromJson(json['OperatorIdParent']);
      }

      this.Operators = JsonUtils.ParseList(json['Operators'], new CompanyLineOperator());
    }
  }
}
