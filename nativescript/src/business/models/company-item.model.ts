import { Company } from '@src/entities/company.model';
// utils
import { DateUtils } from '@src/external/plugin-utils/utils/date-utils';
// models
import { Account } from '@src/external/plugin-utils/business/models/models/account.model';
import { CompanyCall } from '@src/entities/companyCall.model';
import { CompanyNote } from '@src/entities/companyNote.model';
import { PhoneOperator } from '@src/entities/phoneOperator.model';
import { CompanyAddress } from '@src/entities/companyAddress.model';
import { CompanyContact } from '@src/entities/companyContact.model';
import { CompanyCallback } from '@src/entities/companyCallback.model';
import { CompanyAvailability } from '@src/entities/companyAvailability.model';
import { JsonUtils } from '@src/external/plugin-utils/utils/json-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';
import { AddressItem } from './address-item.model';
import { CompanyLine } from '@src/entities/companyLine.model';
import { LineItem } from './line-item.model';

export class CompanyItem extends Company
{
  constructor(
    public Id: number = null,
    public AccountId: number = null,
    public CompanySubtypeId: number = null,
    public Cnpj: string = null,
    public Site: string = null,
    public Ddd: string = null,
    public Phone: string = null,
    public OperatorId: number = null,
    public Description: string = null,
    public FormalName: string = null,
    public FantasyName: string = null,
    public StateRegistry: string = null,
    public ImagesUrl: string = null,
    public Timestamp: Date = null,

    public HasPendingTest: boolean = false,

    public Calls: CompanyCall[] = null,
    public Notes: CompanyNote[] = null,
    public Contacts: CompanyContact[] = null,

    public Lines: LineItem[] = null,
    public Addresses: AddressItem[] = null,

    public Callback: CompanyCallback = null,
  )
  {
    super(Id, AccountId, CompanySubtypeId, Cnpj, Site, Ddd, Phone, OperatorId, Description, FormalName, FantasyName, StateRegistry, ImagesUrl, Timestamp);
  }

  public ParseFromJson(json: any): void
  {
    if (json)
    {
      this.Id = json['Id'];
      this.Ddd = json['Ddd'];
      this.Cnpj = json['Cnpj'];
      this.Site = json['Site'];
      this.Phone = json['Phone'];
      this.AccountId = json['AccountId'];
      this.ImagesUrl = json['ImagesUrl'];
      this.OperatorId = json['OperatorId'];
      this.FormalName = json['FormalName'];
      this.Description = json['Description'];
      this.FantasyName = json['FantasyName'];
      this.StateRegistry = json['StateRegistry'];
      this.Timestamp = DateUtils.FromDateServer(json['Timestamp']);

      this.HasPendingTest = json['HasPendingTest'];

      if (!ObjectUtils.NullOrUndefined(json['AccountIdParent']))
      {
        this.AccountIdParent = new Account();
        this.AccountIdParent.ParseFromJson(json['AccountIdParent']);
      }
      if (!ObjectUtils.NullOrUndefined(json['OperatorIdParent']))
      {
        this.OperatorIdParent = new PhoneOperator();
        this.OperatorIdParent.ParseFromJson(json['OperatorIdParent']);
      }

      if (!ObjectUtils.NullOrUndefined(json['Callback']))
      {
        this.Callback = new CompanyCallback();
        this.Callback.ParseFromJson(json['Callback']);
      }

      this.Calls = JsonUtils.ParseList(json['Calls'], new CompanyCall());
      this.Notes = JsonUtils.ParseList(json['Notes'], new CompanyNote());
      this.Contacts = JsonUtils.ParseList(json['Contacts'], new CompanyContact());

      this.Lines = JsonUtils.ParseList(json['Lines'], new LineItem());
      this.Addresses = JsonUtils.ParseList(json['Addresses'], new AddressItem());
    }
  }
}
