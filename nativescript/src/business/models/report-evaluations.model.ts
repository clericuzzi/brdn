export class ReportEvaluation
{
  constructor(
    public Abr: any[] = null,
    public ByCnpj: any[] = null,
    public ByZipNumber: any[] = null,
    public AbrTested: number = null,
    public AbrUntested: number = null,
    public ByCnpjTested: number = null,
    public ByCnpjUntested: number = null,
    public ByZipNumberTested: number = null,
    public ByZipNumberUntested: number = null,
  )
  {
  }

  public Parse(): void
  {
  }
}
