import { Crudable } from '@src/external/plugin-utils/business/models/crud/crudable.model';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

export class CampaignScreenModel extends Crudable
{
  constructor(
    public CampaignId: number = null,
    public CharacteristicsMust: number[] = null,
    public CharacteristicsMustNot: number[] = null,
  )
  {
    super();
  }

  public Clone(): Crudable
  {
    return this;
  }
  public ToJson(): any
  {
    const json: any = {};
    if (!ObjectUtils.NullOrUndefined(this.CampaignId))
      json['CampaignId'] = this.CampaignId;
    if (!ObjectUtils.NullOrUndefined(this.CharacteristicsMust))
      json['CharacteristicsMust'] = this.CharacteristicsMust;
    if (!ObjectUtils.NullOrUndefined(this.CharacteristicsMustNot))
      json['CharacteristicsMustNot'] = this.CharacteristicsMustNot;

    return json;
  }
  public ToString(): string
  {
    return null;
  }
  public ParseToJson(): string
  {
    const json: any = this.ToJson();

    return JSON.stringify(json);
  }
  public ParseFromJson(json: Object): void
  {
    if (json)
    {
      this.CampaignId = json['CampaignId'];
      this.CharacteristicsMust = json['CharacteristicsMust'];
      this.CharacteristicsMustNot = json['CharacteristicsMustNot'];
    }
  }
}
