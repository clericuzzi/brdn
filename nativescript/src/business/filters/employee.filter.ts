import { DateFilter } from './date.filter';

export class EmployeeFilter extends DateFilter
{
  constructor(
    public DateTo: Date = null,
    public DateFrom: Date = null,
    public EmployeeIds: number[] = null,
  )
  {
    super(DateTo, DateFrom);
  }
}
