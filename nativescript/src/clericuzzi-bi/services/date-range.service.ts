import { Injectable } from '@angular/core';

// bi models
import { YearMonths } from 'clericuzzi-bi/entities/yearmonths.model';

// base models
import { PropertyFilter } from '@src/external/plugin-utils/business/models/crud/property-filter.model';
import { RequestMessage } from '@src/external/plugin-utils/business/models/request/request-message.model';
import { BatchResponseMessage } from '@src/external/plugin-utils/business/models/response/batch-response-message.model';

// base services
import { HttpService } from 'clericuzzi-lib/services/http.service';

@Injectable()
export class DateRangeService
{
  public YearMonthRangeUrl: string;
  public YearMonthRangeByYearMonthUrl: string;
  public DefineBaseUrl(baseUrl: string): void
  {
    this.YearMonthRangeUrl = `${baseUrl}bi/range/all`;
    this.YearMonthRangeByYearMonthUrl = `${baseUrl}bi/range/by-year-month`;
  }

  constructor(
    private _Http: HttpService,
  ) { }

  public GetRange(typeFullName: string, dateField: string, callback: Function, filters: PropertyFilter[]): void
  {
      let request: RequestMessage = new RequestMessage(this.YearMonthRangeUrl);
      // if (filters != null && filters != undefined && filters.length > 0)
      //   request.AddRangeFilters(filters);

      // request.AddData({ TypeFullName: typeFullName });
      // request.AddRangeField(dateField);
      
      var response: BatchResponseMessage<YearMonths> = new BatchResponseMessage<YearMonths>();
      this._Http.Post(request, response, () => callback(response));
  }
  public GetCustomRange(url: string, callback: Function, filters: PropertyFilter[]): void
  {
      let request: RequestMessage = new RequestMessage(url);
      // if (filters != null && filters != undefined && filters.length > 0)
      //   request.AddRangeFilters(filters);

      // var response: BatchResponseMessage<YearMonths> = new BatchResponseMessage<YearMonths>();
      // this._Http.Post(request, response, () => callback(response));
  }
  public GetCustomByYearMonth(typeFullName: string, callback: Function): void
  {
      let request: RequestMessage = new RequestMessage(this.YearMonthRangeByYearMonthUrl);
      // request.AddData({ TypeFullName: typeFullName });

      var response: BatchResponseMessage<YearMonths> = new BatchResponseMessage<YearMonths>();
      this._Http.Post(request, response, () => callback(response));
  }
}
