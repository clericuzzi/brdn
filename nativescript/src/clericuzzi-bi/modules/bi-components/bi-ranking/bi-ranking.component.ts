import { Subject } from 'rxjs';
import { Component, OnInit, Input, HostBinding } from '@angular/core';

// utils
import { JsonUtils } from '@src/external/plugin-utils/utils/json-utils';

// models
import { RankingItem } from 'clericuzzi-bi/entities/ranking-item.model';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

@Component({
  selector: 'bi-ranking',
  templateUrl: './bi-ranking.component.html'
})
export class BiRankingComponent implements OnInit
{
  public OnItemSelected: Subject<RankingItem> = new Subject<RankingItem>();
  
  public get HasSummary(): boolean { return !ObjectUtils.NullOrUndefined(this.SummaryItem); }

  public Items: RankingItem[];
  public SummaryItem: RankingItem;

  @Input() public Label: string;
  @Input() public Title: string;
  @Input() public ValueLabel: string;
  
  @HostBinding(`class`) ContainerClass: string = `full-size`;

  constructor() { }
  ngOnInit()
  {
  }

  public Clear(): void
  {
    this.Items = null;
    this.SummaryItem = null;
  }
  public Update(data: any[]): void
  {
    this.Items = JsonUtils.ParseList<RankingItem>(data['Items'], new RankingItem());
  }
  public UpdateItens(items: RankingItem[], summary: RankingItem = null): void
  {
    this.Items = items;
    this.SummaryItem = summary;
  }
}