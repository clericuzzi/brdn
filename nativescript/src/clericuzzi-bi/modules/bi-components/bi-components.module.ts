import { NgModule } from '@angular/core';

// clericuzzi angular
import { MaterialModule } from 'clericuzzi-lib/modules/material/material.module';

// components
import { BiCardComponent } from './bi-card/bi-card.component';
import { BiChartComponent } from './bi-chart/bi-chart.component';
import { BiRankingComponent } from './bi-ranking/bi-ranking.component';
import { BiYearMonthPickerComponent } from './bi-year-month-picker/bi-year-month-picker.component';

@NgModule({
  imports: [
    MaterialModule,
  ],
  exports: [       
    BiCardComponent,
    BiChartComponent,
    BiRankingComponent,
    BiYearMonthPickerComponent,
  ],
  declarations: [    
    BiCardComponent,
    BiChartComponent,
    BiRankingComponent,
    BiYearMonthPickerComponent,
  ],
  entryComponents: [    
  ]
})
export class BiComponentsModule { }
