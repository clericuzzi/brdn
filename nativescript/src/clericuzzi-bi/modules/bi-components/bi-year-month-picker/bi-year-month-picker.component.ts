import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

// bi services
import { DateRangeService } from 'clericuzzi-bi/services/date-range.service';

// bi models
import { YearMonths } from 'clericuzzi-bi/entities/yearmonths.model';
// base models
import { BatchResponseMessage } from '@src/external/plugin-utils/business/models/response/batch-response-message.model';
import { MatSelectChange } from '@angular/material';
import { PropertyFilter } from '@src/external/plugin-utils/business/models/crud/property-filter.model';
import { Subject } from 'rxjs';

@Component({
  selector: 'bi-year-month-picker',
  templateUrl: './bi-year-month-picker.component.html',
  styleUrls: ['./bi-year-month-picker.component.css']
})
export class BiYearMonthPickerComponent implements OnInit
{
  @Input() public AllLabel: string = `Todos`;
  @Input() public YearHasAll: boolean = false;
  @Input() public MonthHasAll: boolean = false;
  
  public OnRangeGotten: Subject<void> = new Subject<void>();
  public OnYearSelected: Subject<number> = new Subject<number>();
  public OnMonthSelected: Subject<number> = new Subject<number>();

  public Year: any;
  public Month: any;
  private _LoadedValues: boolean = false;

  public Years: any[];
  public Months: any[];
  private Period: YearMonths[];
  private CurrentPeriod: YearMonths;
  constructor(private _RangeService: DateRangeService) { }
  ngOnInit()
  {
  }
  
  public GetRange(typeFullName: string, dateProperty: string, ... filters: PropertyFilter[]): void
  {
    this._RangeService.GetRange(typeFullName, dateProperty, this._GetRangeCallback.bind(this), filters);
  }
  public GetCustomRange(url: string, ... filters: PropertyFilter[]): void
  {
    this._RangeService.GetCustomRange(url, this._GetRangeCallback.bind(this), filters);
  }
  public GetRangeByYearMonth(typeFullName: string): void
  {
    this._RangeService.GetCustomByYearMonth(typeFullName, this._GetRangeCallback.bind(this));
  }
  _GetRangeCallback(response: BatchResponseMessage<YearMonths>): void
  {
    if (!response.HasErrors)
    {
      response.ParseJSOnContent(new YearMonths());
      this.Period = response.Entities;
      if (this.Period != null && this.Period != undefined && this.Period.length > 0)
        this._LoadYears();
    }

    this.OnRangeGotten.next();
  }
  _InitValues(): void
  {
    this.Years = [];
    this.Months = [];
    
    if (this.YearHasAll)
      this.Years.push(this.AllLabel);
      
    if (this.MonthHasAll)
      this.Months.push(this.AllLabel);
  }
  _LoadYears(): void
  {
    this._InitValues();
    for (let currentYear of this.Period.map(p => p.Year))
      this.Years.push(currentYear);

    let currentYear: number = new Date().getFullYear();
    if (this.Years.indexOf(currentYear) >= 0)
      this.SelectPeriod(currentYear);
    else
      this.SelectPeriod(this.Period[0].Year);
  }
  public SelectPeriod(year: number): void
  {
    this._InitValues();
    this.CurrentPeriod = this.Period.filter(p => p.Year == year)[0];
    for (let currentYear of this.Period.map(p => p.Year))
      this.Years.push(currentYear);
    for (let month of this.CurrentPeriod.Months)
      this.Months.push(month);

    this.Year = year;
    if (!this._LoadedValues)
    {
      if (this.Year == new Date().getFullYear())
      {
        let currentMonth = new Date().getMonth() + 1;
        if (this.Months.indexOf(currentMonth) >= 0)
          this.Month = currentMonth;
        else
          this.Month = this.Months[0];
      }
      else
        this.Month = this.Months[0];
      this._LoadedValues = true;
    }
    else
      this.Month = this.Months[0];
  }
  public ChangeYear(selection: MatSelectChange): void
  {
    if (typeof(selection.value) == "number")
    {
      this.SelectPeriod(selection.value as number);
      this.OnYearSelected.next(this.Year);
    }
    else
    {
      this.Months = [ `Todos` ];
      this.OnYearSelected.next(null);
    }
  }
  public ChangeMonth(selection: MatSelectChange): void
  {
    if (typeof(selection.value) == "number")
    {
      this.Month = selection.value;
      this.OnMonthSelected.next(this.Month);
    }
    else
      this.OnMonthSelected.next(null);
  }
}