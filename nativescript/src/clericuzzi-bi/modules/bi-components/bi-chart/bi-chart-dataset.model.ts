import { BiChartDataSetItem } from './bi-chart-dataset-item.model';

export class BiChartDataSet
{
  public Title: string;
  public Items: BiChartDataSetItem[];

  public data: any[];

  public fill: boolean = false;
  public label: string;
  public borderColor: string[];
  public backgroundColor: string[];
  public Update(): void
  {
    this.data = this.Items.map(i => i.Value);
    this.label = this.Title;
  }
}
