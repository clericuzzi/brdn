import { Chart } from 'chart.js';
import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';

// models

// models
import { BiChartDataSet } from './bi-chart-dataset.model';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';

@Component({
  selector: 'bi-chart',
  templateUrl: './bi-chart.component.html'
})
export class BiChartComponent implements OnInit
{
  @ViewChild(`canvas`) public Canvas: ElementRef;

  @Input() public Type: string;
  @Input() public Title: string;
  @Input() public Labels: string[];
  @Input() public Options: any = { responsive: true, maintainAspectRatio: false, scales: { xAxes: [{}], yAxes: [{ ticks: { beginAtZero: true } }] } };
  @Input() public DataSets: BiChartDataSet[];
  @Input() public BackgroundColors: string[];

  public Chart: Chart;

  constructor()
  {
  }

  ngOnInit()
  {
    this._InitChart();
  }
  _InitChart(): void
  {
    this.Draw();
  }
  public Clear(): void
  {
    const context = this.Canvas.nativeElement.getContext('2d');
    context.clearRect(0, 0, this.Canvas.nativeElement.width, this.Canvas.nativeElement.height);

    if (this.Chart != null)
      this.Chart.destroy();
  }
  public Draw(): void
  {
    this.Clear();
    const chartObject: any = {
      type: this.Type,
      Title: this.Title,
      data: {
        labels: this.Labels,
        datasets: this.DataSets,
      },
      options: this.Options,
      backgroundColor: this.BackgroundColors,
    };
    this.Chart = new Chart(this.Canvas.nativeElement.getContext('2d'), chartObject);
  }
  public StartAtZero(): void
  {
    const yAxes: any = this.Options[`scales`][`yAxes`][0];
    delete yAxes[`ticks`];
    yAxes[`ticks`] = { ticks: { beginAtZero: true } };
  }
  public HideAxes(): void
  {
    this.Options[`scales`] = { xAxes: { display: false }, yAxes: { display: false } };
  }
  public HideLegend(): void
  {
    let legend: any = this.Options[`legend`];
    if (ObjectUtils.NullOrUndefined(legend))
    {
      this.Options[`legend`] = {};
      legend = this.Options[`legend`];
    }
    delete legend[`display`];
    legend[`display`] = false;
  }
  public HideGridLines(): void
  {
    const xAxes: any = this.Options[`scales`][`xAxes`][0];
    delete xAxes[`gridLines`];
    xAxes[`gridLines`] = { display: false };

    const yAxes: any = this.Options[`scales`][`yAxes`][0];
    delete yAxes[`gridLines`];
    yAxes[`gridLines`] = { display: false };
  }
  public SetTitle(title: string): void
  {
    this.Options[`title`] = { display: true, text: title };
  }
  public SetAxesNames(xAxesName: string, yAxesName: string): void
  {
    const xAxes: any = this.Options[`scales`][`xAxes`][0];
    delete xAxes[`scaleLabel`];
    xAxes[`scaleLabel`] = { display: true, labelString: xAxesName };

    const yAxes: any = this.Options[`scales`][`yAxes`][0];
    delete yAxes[`scaleLabel`];
    yAxes[`scaleLabel`] = { display: true, labelString: yAxesName };
  }

  public PieAddDataset(title: string, ...datasets: BiChartDataSet[]): void
  {
    if (ObjectUtils.NullUndefinedOrEmptyArray(datasets))
      return;

    const labels: string[] = [];
    for (const dataset of datasets)
    {
      const datasetLabels: string[] = dataset.Items.map(i => i.Key);
      for (const datasetLabel of datasetLabels)
        labels.push(datasetLabel);

      dataset.Update();
    }

    this.Type = `pie`;
    this.Labels = labels;
    this.DataSets = datasets;
    this.SetTitle(title);

    this.Draw();
  }
  public SetLabelDateRange(from: Date, to: Date): void
  {
    const labels: string[] = [];
    const upperLimit: Date = new Date(to.getFullYear(), to.getMonth(), to.getDate(), 23, 59, 59, 999);
    const currentDate: Date = new Date(from.getFullYear(), from.getMonth(), from.getDate(), 0, 0, 0, 0);
    while (currentDate < upperLimit)
    {
      labels.push(StringUtils.TransformDateToFormat(currentDate, `DD/MM`));
      currentDate.setDate(currentDate.getDate() + 1);
    }

    this.Labels = labels;
  }
}
