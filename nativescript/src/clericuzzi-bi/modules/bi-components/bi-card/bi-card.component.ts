import { Component, OnInit, Input } from '@angular/core';

// utils
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';
import { HostBindedComponent } from 'clericuzzi-lib/modules/shared/util-components/host-binded/host-binded.component';

@Component({
  selector: 'bi-card',
  templateUrl: './bi-card.component.html',
  styleUrls: ['./bi-card.component.css']
})
export class BiCardComponent extends HostBindedComponent implements OnInit
{
  @Input() public Title: string;
  @Input() public GradientTo: string;
  @Input() public GradientFrom: string;
  @Input() public MaterialIcon: string;
  @Input() public FontAwesomeIcon: string;

  public Icon: string;
  public Value: any;
  public get GradientClass(): string
  {
    return `linear-gradient(to top right, #${this.GradientFrom}, #${this.GradientTo})`;
  }

  constructor()
  {
    super(null);
  }

  ngOnInit()
  {
  }
  public SetValue(value: any): void
  {
    this.Value = StringUtils.NumberWithCommas(value);
  }
}
