﻿import { Crudable } from '@src/external/plugin-utils/business/models/crud/crudable.model';

// utils
import { DateUtils } from '@src/external/plugin-utils/utils/date-utils';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// Navigation Properties
import { User } from '@src/external/plugin-utils/business/models/models/user.model';
import { Company } from './company.model';
import { CompanyContact } from './companyContact.model';

// components helpers
import { ComponentTypeEnum } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-definition.model';

export const CompanyCallbackClass: string = `CompanyCallback`;
export const CompanyCallbackTable: string = `company_callback`;

/**
 * Column definitions for the `CompanyCallback` class
 */
export enum CompanyCallbackColumns
{
	Id = 'Id',
	Done = 'Done',
	UserId = 'UserId',
	CallTime = 'CallTime',
	CompanyId = 'CompanyId',
	ContactId = 'ContactId',
	Timestamp = 'Timestamp',

	UserIdParent = 'UserIdParent',
	CompanyIdParent = 'CompanyIdParent',
	ContactIdParent = 'ContactIdParent',
}
export const CompanyCallbackColumnsFilter: string[] = [];
export const CompanyCallbackColumnsInsert: string[] = [CompanyCallbackColumns.UserId, CompanyCallbackColumns.CompanyId, CompanyCallbackColumns.ContactId, CompanyCallbackColumns.CallTime, CompanyCallbackColumns.Done, CompanyCallbackColumns.Timestamp];
export const CompanyCallbackColumnsUpdate: string[] = [CompanyCallbackColumns.UserId, CompanyCallbackColumns.CompanyId, CompanyCallbackColumns.ContactId, CompanyCallbackColumns.CallTime, CompanyCallbackColumns.Done, CompanyCallbackColumns.Timestamp];

/**
 * Implementations of the `CompanyCallback` class
 */
export class CompanyCallback extends Crudable
{
	public UserIdParent: User;
	public CompanyIdParent: Company;
	public ContactIdParent: CompanyContact;

	public static FromJson(json: any): CompanyCallback
	{
		const item: CompanyCallback = new CompanyCallback();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public UserId: number = null,
		public CompanyId: number = null,
		public ContactId: number = null,
		public CallTime: Date = null,
		public Done: number = null,
		public Timestamp: Date = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`CompanyCallback`, `company_callback`, CrudableTableDefinitionGender.Male, `Company Callback`, `Company Callbacks`, true, true, true, true, true, true);

		this.SetColumnDefinition(CompanyCallbackColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyCallbackColumns.UserId, `user_id`, `UserId`, `User Id`, `User Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyCallbackColumns.CompanyId, `company_id`, `CompanyId`, `Company Id`, `Company Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyCallbackColumns.ContactId, `contact_id`, `ContactId`, `Contact Id`, `Contact Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyCallbackColumns.CallTime, `call_time`, `CallTime`, `Call Time`, `Call Times`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyCallbackColumns.Done, `done`, `Done`, `Done`, `Dones`, ComponentTypeEnum.Number, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyCallbackColumns.Timestamp, `timestamp`, `Timestamp`, `Timestamp`, `Timestamps`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
	}

	public Clone(): CompanyCallback
	{
		const clone: CompanyCallback = new CompanyCallback();
		clone.Id = this.Id;
		clone.Done = this.Done;
		clone.UserId = this.UserId;
		clone.CallTime = this.CallTime;
		clone.CompanyId = this.CompanyId;
		clone.ContactId = this.ContactId;
		clone.Timestamp = this.Timestamp;

		clone.UserIdParent = this.UserIdParent;
		clone.CompanyIdParent = this.CompanyIdParent;
		clone.ContactIdParent = this.ContactIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.UserIdParent == null ? '' : this.UserIdParent.ToString()}`;
	}

	public ToJson(navigationProperties: boolean = false): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Done))
			json['Done'] = this.Done;
		if (!ObjectUtils.NullOrUndefined(this.CallTime))
			json['CallTime'] = DateUtils.ToDateServer(this.CallTime);
		if (!ObjectUtils.NullOrUndefined(this.Timestamp))
			json['Timestamp'] = DateUtils.ToDateServer(this.Timestamp);
		if (navigationProperties)
		{
			if (!ObjectUtils.NullOrUndefined(this.UserIdParent))
				json['UserIdParent'] = this.UserIdParent.ToJson();
			if (!ObjectUtils.NullOrUndefined(this.CompanyIdParent))
				json['CompanyIdParent'] = this.CompanyIdParent.ToJson();
			if (!ObjectUtils.NullOrUndefined(this.ContactIdParent))
				json['ContactIdParent'] = this.ContactIdParent.ToJson();
		}

		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: any): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Done = json['Done'];
			this.UserId = json['UserId'];
			this.CompanyId = json['CompanyId'];
			this.ContactId = json['ContactId'];
			this.CallTime = DateUtils.FromDateServer(json['CallTime']);
			this.Timestamp = DateUtils.FromDateServer(json['Timestamp']);

			if (!ObjectUtils.NullOrUndefined(json['UserIdParent']))
			{
				this.UserIdParent = new User();
				this.UserIdParent.ParseFromJson(json['UserIdParent']);
			}
			if (!ObjectUtils.NullOrUndefined(json['CompanyIdParent']))
			{
				this.CompanyIdParent = new Company();
				this.CompanyIdParent.ParseFromJson(json['CompanyIdParent']);
			}
			if (!ObjectUtils.NullOrUndefined(json['ContactIdParent']))
			{
				this.ContactIdParent = new CompanyContact();
				this.ContactIdParent.ParseFromJson(json['ContactIdParent']);
			}
		}
	}
}

/**
 * Implementations of the `CompanyCallbackFilter` class
 */
export class CompanyCallbackFilter extends Crudable
{
	public static FromJson(json: Object): CompanyCallbackFilter
	{
		const item: CompanyCallbackFilter = new CompanyCallbackFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public UserId: number[] = null,
		public CompanyId: number[] = null,
		public ContactId: number[] = null,
		public CallTime: Date = null,
		public Done: number = null,
		public Timestamp: Date = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`CompanyCallbackFilter`, `company_callbackFilter`, CrudableTableDefinitionGender.Male, `Company Callback`, `Company Callbacks`, true, true, true, true, true, true);

		this.SetColumnDefinition(CompanyCallbackColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyCallbackColumns.UserId, `user_id`, `UserId`, `User Id`, `User Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyCallbackColumns.CompanyId, `company_id`, `CompanyId`, `Company Id`, `Company Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyCallbackColumns.ContactId, `contact_id`, `ContactId`, `Contact Id`, `Contact Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyCallbackColumns.CallTime, `call_time`, `CallTime`, `Call Time`, `Call Times`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyCallbackColumns.Done, `done`, `Done`, `Done`, `Dones`, ComponentTypeEnum.Number, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyCallbackColumns.Timestamp, `timestamp`, `Timestamp`, `Timestamp`, `Timestamps`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
	}

	public Clone(): CompanyCallbackFilter
	{
		const clone: CompanyCallbackFilter = new CompanyCallbackFilter();
		clone.Id = this.Id;
		clone.Done = this.Done;
		clone.UserId = this.UserId;
		clone.CallTime = this.CallTime;
		clone.CompanyId = this.CompanyId;
		clone.ContactId = this.ContactId;
		clone.Timestamp = this.Timestamp;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Done))
			json['Done'] = this.Done;
		if (!ObjectUtils.NullOrUndefined(this.CallTime))
			json['CallTime'] = DateUtils.ToDateServer(this.CallTime);
		if (!ObjectUtils.NullOrUndefined(this.Timestamp))
			json['Timestamp'] = DateUtils.ToDateServer(this.Timestamp);
		if (!ObjectUtils.NullOrUndefined(this.UserId))
			json['UserId'] = Array.isArray(this.UserId) ? this.UserId : [this.UserId];
		if (!ObjectUtils.NullOrUndefined(this.CompanyId))
			json['CompanyId'] = Array.isArray(this.CompanyId) ? this.CompanyId : [this.CompanyId];
		if (!ObjectUtils.NullOrUndefined(this.ContactId))
			json['ContactId'] = Array.isArray(this.ContactId) ? this.ContactId : [this.ContactId];


		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Done = json['Done'];
			this.UserId = json['UserId'];
			this.CompanyId = json['CompanyId'];
			this.ContactId = json['ContactId'];
			this.CallTime = DateUtils.FromDateServer(json['CallTime']);
			this.Timestamp = DateUtils.FromDateServer(json['Timestamp']);
		}
	}
}