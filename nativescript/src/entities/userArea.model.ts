﻿import { Crudable } from '@src/external/plugin-utils/business/models/crud/crudable.model';

// utils
import { DateUtils } from '@src/external/plugin-utils/utils/date-utils';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// Navigation Properties
import { User } from '@src/external/plugin-utils/business/models/models/user.model';

// components helpers
import { ComponentTypeEnum } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-definition.model';

export const UserAreaClass: string = `UserArea`;
export const UserAreaTable: string = `user_area`;

/**
 * Column definitions for the `UserArea` class
 */
export enum UserAreaColumns
{
	Id = 'Id',
	UserId = 'UserId',
	Cities = 'Cities',
	Neighbourhoods = 'Neighbourhoods',

	UserIdParent = 'UserIdParent',
}
export const UserAreaColumnsFilter: string[] = [];
export const UserAreaColumnsInsert: string[] = [UserAreaColumns.UserId, UserAreaColumns.Cities, UserAreaColumns.Neighbourhoods];
export const UserAreaColumnsUpdate: string[] = [UserAreaColumns.UserId, UserAreaColumns.Cities, UserAreaColumns.Neighbourhoods];

/**
 * Implementations of the `UserArea` class
 */
export class UserArea extends Crudable
{
	public UserIdParent: User;

	public static FromJson(json: any): UserArea
	{
		const item: UserArea = new UserArea();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public UserId: number = null,
		public Cities: string = null,
		public Neighbourhoods: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`UserArea`, `user_area`, CrudableTableDefinitionGender.Male, `User Area`, `User Areas`, true, true, true, true, true, true);

		this.SetColumnDefinition(UserAreaColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(UserAreaColumns.UserId, `user_id`, `UserId`, `User Id`, `User Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(UserAreaColumns.Cities, `cities`, `Cities`, `Cities`, `Citiess`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(UserAreaColumns.Neighbourhoods, `neighbourhoods`, `Neighbourhoods`, `Neighbourhoods`, `Neighbourhoodss`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
	}

	public Clone(): UserArea
	{
		const clone: UserArea = new UserArea();
		clone.Id = this.Id;
		clone.UserId = this.UserId;
		clone.Cities = this.Cities;
		clone.Neighbourhoods = this.Neighbourhoods;

		clone.UserIdParent = this.UserIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.UserIdParent == null ? '' : this.UserIdParent.ToString()}`;
	}

	public ToJson(navigationProperties: boolean = false): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Cities))
			json['Cities'] = this.Cities;
		if (!ObjectUtils.NullOrUndefined(this.Neighbourhoods))
			json['Neighbourhoods'] = this.Neighbourhoods;
		if (navigationProperties)
		{
			if (!ObjectUtils.NullOrUndefined(this.UserIdParent))
				json['UserIdParent'] = this.UserIdParent.ToJson();
		}

		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: any): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.UserId = json['UserId'];
			this.Cities = json['Cities'];
			this.Neighbourhoods = json['Neighbourhoods'];

			if (!ObjectUtils.NullOrUndefined(json['UserIdParent']))
			{
				this.UserIdParent = new User();
				this.UserIdParent.ParseFromJson(json['UserIdParent']);
			}
		}
	}
}

/**
 * Implementations of the `UserAreaFilter` class
 */
export class UserAreaFilter extends Crudable
{
	public static FromJson(json: Object): UserAreaFilter
	{
		const item: UserAreaFilter = new UserAreaFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public UserId: number[] = null,
		public Cities: string = null,
		public Neighbourhoods: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`UserAreaFilter`, `user_areaFilter`, CrudableTableDefinitionGender.Male, `User Area`, `User Areas`, true, true, true, true, true, true);

		this.SetColumnDefinition(UserAreaColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(UserAreaColumns.UserId, `user_id`, `UserId`, `User Id`, `User Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(UserAreaColumns.Cities, `cities`, `Cities`, `Cities`, `Citiess`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(UserAreaColumns.Neighbourhoods, `neighbourhoods`, `Neighbourhoods`, `Neighbourhoods`, `Neighbourhoodss`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
	}

	public Clone(): UserAreaFilter
	{
		const clone: UserAreaFilter = new UserAreaFilter();
		clone.Id = this.Id;
		clone.UserId = this.UserId;
		clone.Cities = this.Cities;
		clone.Neighbourhoods = this.Neighbourhoods;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Cities))
			json['Cities'] = this.Cities;
		if (!ObjectUtils.NullOrUndefined(this.Neighbourhoods))
			json['Neighbourhoods'] = this.Neighbourhoods;
		if (!ObjectUtils.NullOrUndefined(this.UserId))
			json['UserId'] = Array.isArray(this.UserId) ? this.UserId : [this.UserId];


		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.UserId = json['UserId'];
			this.Cities = json['Cities'];
			this.Neighbourhoods = json['Neighbourhoods'];
		}
	}
}