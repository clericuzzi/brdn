﻿import { Crudable } from '@src/external/plugin-utils/business/models/crud/crudable.model';

// utils
import { DateUtils } from '@src/external/plugin-utils/utils/date-utils';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// Navigation Properties
import { Account } from '@src/external/plugin-utils/business/models/models/account.model';
import { CompanySubtype } from './companySubtype.model';
import { CompanySituation } from './companySituation.model';

// components helpers
import { ComponentTypeEnum } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-definition.model';

export const CompanyClass: string = `Company`;
export const CompanyTable: string = `company`;

/**
 * Column definitions for the `Company` class
 */
export enum CompanyColumns
{
	Id = 'Id',
	Ddd = 'Ddd',
	Cnpj = 'Cnpj',
	Site = 'Site',
	Rank = 'Rank',
	Phone = 'Phone',
	Capital = 'Capital',
	Latitude = 'Latitude',
	AccountId = 'AccountId',
	ImagesUrl = 'ImagesUrl',
	Longitude = 'Longitude',
	Timestamp = 'Timestamp',
	FormalName = 'FormalName',
	DateOpened = 'DateOpened',
	Description = 'Description',
	FantasyName = 'FantasyName',
	SituationId = 'SituationId',
	SalesForceId = 'SalesForceId',
	StateRegistry = 'StateRegistry',
	DateLastUpdate = 'DateLastUpdate',
	CompanySubtypeId = 'CompanySubtypeId',
	CnpjTestingTimestamp = 'CnpjTestingTimestamp',

	AccountIdParent = 'AccountIdParent',
	SituationIdParent = 'SituationIdParent',
	CompanySubtypeIdParent = 'CompanySubtypeIdParent',
}
export const CompanyColumnsFilter: string[] = [];
export const CompanyColumnsInsert: string[] = [CompanyColumns.AccountId, CompanyColumns.CompanySubtypeId, CompanyColumns.Cnpj, CompanyColumns.Site, CompanyColumns.Ddd, CompanyColumns.Phone, CompanyColumns.Description, CompanyColumns.FormalName, CompanyColumns.FantasyName, CompanyColumns.StateRegistry, CompanyColumns.ImagesUrl, CompanyColumns.Latitude, CompanyColumns.Longitude, CompanyColumns.Timestamp, CompanyColumns.CnpjTestingTimestamp, CompanyColumns.Rank, CompanyColumns.Capital, CompanyColumns.DateOpened, CompanyColumns.SituationId, CompanyColumns.SalesForceId, CompanyColumns.DateLastUpdate];
export const CompanyColumnsUpdate: string[] = [CompanyColumns.AccountId, CompanyColumns.CompanySubtypeId, CompanyColumns.Cnpj, CompanyColumns.Site, CompanyColumns.Ddd, CompanyColumns.Phone, CompanyColumns.Description, CompanyColumns.FormalName, CompanyColumns.FantasyName, CompanyColumns.StateRegistry, CompanyColumns.ImagesUrl, CompanyColumns.Latitude, CompanyColumns.Longitude, CompanyColumns.Timestamp, CompanyColumns.CnpjTestingTimestamp, CompanyColumns.Rank, CompanyColumns.Capital, CompanyColumns.DateOpened, CompanyColumns.SituationId, CompanyColumns.SalesForceId, CompanyColumns.DateLastUpdate];

/**
 * Implementations of the `Company` class
 */
export class Company extends Crudable
{
	public AccountIdParent: Account;
	public CompanySubtypeIdParent: CompanySubtype;
	public SituationIdParent: CompanySituation;

	public static FromJson(json: any): Company
	{
		const item: Company = new Company();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public AccountId: number = null,
		public CompanySubtypeId: number = null,
		public Cnpj: string = null,
		public Site: string = null,
		public Ddd: string = null,
		public Phone: string = null,
		public Description: string = null,
		public FormalName: string = null,
		public FantasyName: string = null,
		public StateRegistry: string = null,
		public ImagesUrl: string = null,
		public Latitude: number = null,
		public Longitude: number = null,
		public Timestamp: Date = null,
		public CnpjTestingTimestamp: Date = null,
		public Rank: string = null,
		public Capital: number = null,
		public DateOpened: Date = null,
		public SituationId: number = null,
		public SalesForceId: string = null,
		public DateLastUpdate: Date = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`Company`, `company`, CrudableTableDefinitionGender.Male, `Company`, `Companys`, true, true, true, true, true, true);

		this.SetColumnDefinition(CompanyColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyColumns.AccountId, `account_id`, `AccountId`, `Account Id`, `Account Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyColumns.CompanySubtypeId, `company_subtype_id`, `CompanySubtypeId`, `Company Subtype Id`, `Company Subtype Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyColumns.Cnpj, `cnpj`, `Cnpj`, `Cnpj`, `Cnpjs`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyColumns.Site, `site`, `Site`, `Site`, `Sites`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyColumns.Ddd, `ddd`, `Ddd`, `Ddd`, `Ddds`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyColumns.Phone, `phone`, `Phone`, `Phone`, `Phones`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyColumns.Description, `description`, `Description`, `Description`, `Descriptions`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyColumns.FormalName, `formal_name`, `FormalName`, `Formal Name`, `Formal Names`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyColumns.FantasyName, `fantasy_name`, `FantasyName`, `Fantasy Name`, `Fantasy Names`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyColumns.StateRegistry, `state_registry`, `StateRegistry`, `State Registry`, `State Registrys`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyColumns.ImagesUrl, `images_url`, `ImagesUrl`, `Images Url`, `Images Urls`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyColumns.Latitude, `latitude`, `Latitude`, `Latitude`, `Latitudes`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyColumns.Longitude, `longitude`, `Longitude`, `Longitude`, `Longitudes`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyColumns.Timestamp, `timestamp`, `Timestamp`, `Timestamp`, `Timestamps`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyColumns.CnpjTestingTimestamp, `cnpj_testing_timestamp`, `CnpjTestingTimestamp`, `Cnpj Testing Timestamp`, `Cnpj Testing Timestamps`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyColumns.Rank, `rank`, `Rank`, `Rank`, `Ranks`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyColumns.Capital, `capital`, `Capital`, `Capital`, `Capitals`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyColumns.DateOpened, `date_opened`, `DateOpened`, `Date Opened`, `Date Openeds`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyColumns.SituationId, `situation_id`, `SituationId`, `Situation Id`, `Situation Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyColumns.SalesForceId, `sales_force_id`, `SalesForceId`, `Sales Force Id`, `Sales Force Ids`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyColumns.DateLastUpdate, `date_last_update`, `DateLastUpdate`, `Date Last Update`, `Date Last Updates`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, true);
	}

	public Clone(): Company
	{
		const clone: Company = new Company();
		clone.Id = this.Id;
		clone.Ddd = this.Ddd;
		clone.Cnpj = this.Cnpj;
		clone.Site = this.Site;
		clone.Rank = this.Rank;
		clone.Phone = this.Phone;
		clone.Capital = this.Capital;
		clone.Latitude = this.Latitude;
		clone.AccountId = this.AccountId;
		clone.ImagesUrl = this.ImagesUrl;
		clone.Longitude = this.Longitude;
		clone.Timestamp = this.Timestamp;
		clone.FormalName = this.FormalName;
		clone.DateOpened = this.DateOpened;
		clone.Description = this.Description;
		clone.FantasyName = this.FantasyName;
		clone.SituationId = this.SituationId;
		clone.SalesForceId = this.SalesForceId;
		clone.StateRegistry = this.StateRegistry;
		clone.DateLastUpdate = this.DateLastUpdate;
		clone.CompanySubtypeId = this.CompanySubtypeId;
		clone.CnpjTestingTimestamp = this.CnpjTestingTimestamp;

		clone.AccountIdParent = this.AccountIdParent;
		clone.SituationIdParent = this.SituationIdParent;
		clone.CompanySubtypeIdParent = this.CompanySubtypeIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.AccountIdParent == null ? '' : this.AccountIdParent.ToString()}`;
	}

	public ToJson(navigationProperties: boolean = false): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Ddd))
			json['Ddd'] = this.Ddd;
		if (!ObjectUtils.NullOrUndefined(this.Cnpj))
			json['Cnpj'] = this.Cnpj;
		if (!ObjectUtils.NullOrUndefined(this.Site))
			json['Site'] = this.Site;
		if (!ObjectUtils.NullOrUndefined(this.Rank))
			json['Rank'] = this.Rank;
		if (!ObjectUtils.NullOrUndefined(this.Phone))
			json['Phone'] = this.Phone;
		if (!ObjectUtils.NullOrUndefined(this.Capital))
			json['Capital'] = this.Capital;
		if (!ObjectUtils.NullOrUndefined(this.Latitude))
			json['Latitude'] = this.Latitude;
		if (!ObjectUtils.NullOrUndefined(this.ImagesUrl))
			json['ImagesUrl'] = this.ImagesUrl;
		if (!ObjectUtils.NullOrUndefined(this.Longitude))
			json['Longitude'] = this.Longitude;
		if (!ObjectUtils.NullOrUndefined(this.FormalName))
			json['FormalName'] = this.FormalName;
		if (!ObjectUtils.NullOrUndefined(this.Description))
			json['Description'] = this.Description;
		if (!ObjectUtils.NullOrUndefined(this.FantasyName))
			json['FantasyName'] = this.FantasyName;
		if (!ObjectUtils.NullOrUndefined(this.SalesForceId))
			json['SalesForceId'] = this.SalesForceId;
		if (!ObjectUtils.NullOrUndefined(this.StateRegistry))
			json['StateRegistry'] = this.StateRegistry;
		if (!ObjectUtils.NullOrUndefined(this.Timestamp))
			json['Timestamp'] = DateUtils.ToDateServer(this.Timestamp);
		if (!ObjectUtils.NullOrUndefined(this.DateOpened))
			json['DateOpened'] = DateUtils.ToDateServer(this.DateOpened);
		if (!ObjectUtils.NullOrUndefined(this.DateLastUpdate))
			json['DateLastUpdate'] = DateUtils.ToDateServer(this.DateLastUpdate);
		if (!ObjectUtils.NullOrUndefined(this.CnpjTestingTimestamp))
			json['CnpjTestingTimestamp'] = DateUtils.ToDateServer(this.CnpjTestingTimestamp);
		if (navigationProperties)
		{
			if (!ObjectUtils.NullOrUndefined(this.AccountIdParent))
				json['AccountIdParent'] = this.AccountIdParent.ToJson();
			if (!ObjectUtils.NullOrUndefined(this.CompanySubtypeIdParent))
				json['CompanySubtypeIdParent'] = this.CompanySubtypeIdParent.ToJson();
			if (!ObjectUtils.NullOrUndefined(this.SituationIdParent))
				json['SituationIdParent'] = this.SituationIdParent.ToJson();
		}

		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: any): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Ddd = json['Ddd'];
			this.Cnpj = json['Cnpj'];
			this.Site = json['Site'];
			this.Rank = json['Rank'];
			this.Phone = json['Phone'];
			this.Capital = json['Capital'];
			this.Latitude = json['Latitude'];
			this.AccountId = json['AccountId'];
			this.ImagesUrl = json['ImagesUrl'];
			this.Longitude = json['Longitude'];
			this.FormalName = json['FormalName'];
			this.Description = json['Description'];
			this.FantasyName = json['FantasyName'];
			this.SituationId = json['SituationId'];
			this.SalesForceId = json['SalesForceId'];
			this.StateRegistry = json['StateRegistry'];
			this.CompanySubtypeId = json['CompanySubtypeId'];
			this.Timestamp = DateUtils.FromDateServer(json['Timestamp']);
			this.DateOpened = DateUtils.FromDateServer(json['DateOpened']);
			this.DateLastUpdate = DateUtils.FromDateServer(json['DateLastUpdate']);
			this.CnpjTestingTimestamp = DateUtils.FromDateServer(json['CnpjTestingTimestamp']);

			if (!ObjectUtils.NullOrUndefined(json['AccountIdParent']))
			{
				this.AccountIdParent = new Account();
				this.AccountIdParent.ParseFromJson(json['AccountIdParent']);
			}
			if (!ObjectUtils.NullOrUndefined(json['CompanySubtypeIdParent']))
			{
				this.CompanySubtypeIdParent = new CompanySubtype();
				this.CompanySubtypeIdParent.ParseFromJson(json['CompanySubtypeIdParent']);
			}
			if (!ObjectUtils.NullOrUndefined(json['SituationIdParent']))
			{
				this.SituationIdParent = new CompanySituation();
				this.SituationIdParent.ParseFromJson(json['SituationIdParent']);
			}
		}
	}
}

/**
 * Implementations of the `CompanyFilter` class
 */
export class CompanyFilter extends Crudable
{
	public static FromJson(json: Object): CompanyFilter
	{
		const item: CompanyFilter = new CompanyFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public AccountId: number[] = null,
		public CompanySubtypeId: number[] = null,
		public Cnpj: string = null,
		public Site: string = null,
		public Ddd: string = null,
		public Phone: string = null,
		public Description: string = null,
		public FormalName: string = null,
		public FantasyName: string = null,
		public StateRegistry: string = null,
		public ImagesUrl: string = null,
		public Latitude: number = null,
		public Longitude: number = null,
		public Timestamp: Date = null,
		public CnpjTestingTimestamp: Date = null,
		public Rank: string = null,
		public Capital: number = null,
		public DateOpened: Date = null,
		public SituationId: number[] = null,
		public SalesForceId: string = null,
		public DateLastUpdate: Date = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`CompanyFilter`, `companyFilter`, CrudableTableDefinitionGender.Male, `Company`, `Companys`, true, true, true, true, true, true);

		this.SetColumnDefinition(CompanyColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyColumns.AccountId, `account_id`, `AccountId`, `Account Id`, `Account Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyColumns.CompanySubtypeId, `company_subtype_id`, `CompanySubtypeId`, `Company Subtype Id`, `Company Subtype Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyColumns.Cnpj, `cnpj`, `Cnpj`, `Cnpj`, `Cnpjs`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyColumns.Site, `site`, `Site`, `Site`, `Sites`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyColumns.Ddd, `ddd`, `Ddd`, `Ddd`, `Ddds`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyColumns.Phone, `phone`, `Phone`, `Phone`, `Phones`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyColumns.Description, `description`, `Description`, `Description`, `Descriptions`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyColumns.FormalName, `formal_name`, `FormalName`, `Formal Name`, `Formal Names`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyColumns.FantasyName, `fantasy_name`, `FantasyName`, `Fantasy Name`, `Fantasy Names`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyColumns.StateRegistry, `state_registry`, `StateRegistry`, `State Registry`, `State Registrys`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyColumns.ImagesUrl, `images_url`, `ImagesUrl`, `Images Url`, `Images Urls`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyColumns.Latitude, `latitude`, `Latitude`, `Latitude`, `Latitudes`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyColumns.Longitude, `longitude`, `Longitude`, `Longitude`, `Longitudes`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyColumns.Timestamp, `timestamp`, `Timestamp`, `Timestamp`, `Timestamps`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyColumns.CnpjTestingTimestamp, `cnpj_testing_timestamp`, `CnpjTestingTimestamp`, `Cnpj Testing Timestamp`, `Cnpj Testing Timestamps`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyColumns.Rank, `rank`, `Rank`, `Rank`, `Ranks`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyColumns.Capital, `capital`, `Capital`, `Capital`, `Capitals`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyColumns.DateOpened, `date_opened`, `DateOpened`, `Date Opened`, `Date Openeds`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyColumns.SituationId, `situation_id`, `SituationId`, `Situation Id`, `Situation Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyColumns.SalesForceId, `sales_force_id`, `SalesForceId`, `Sales Force Id`, `Sales Force Ids`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyColumns.DateLastUpdate, `date_last_update`, `DateLastUpdate`, `Date Last Update`, `Date Last Updates`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, true);
	}

	public Clone(): CompanyFilter
	{
		const clone: CompanyFilter = new CompanyFilter();
		clone.Id = this.Id;
		clone.Ddd = this.Ddd;
		clone.Cnpj = this.Cnpj;
		clone.Site = this.Site;
		clone.Rank = this.Rank;
		clone.Phone = this.Phone;
		clone.Capital = this.Capital;
		clone.Latitude = this.Latitude;
		clone.AccountId = this.AccountId;
		clone.ImagesUrl = this.ImagesUrl;
		clone.Longitude = this.Longitude;
		clone.Timestamp = this.Timestamp;
		clone.FormalName = this.FormalName;
		clone.DateOpened = this.DateOpened;
		clone.Description = this.Description;
		clone.FantasyName = this.FantasyName;
		clone.SituationId = this.SituationId;
		clone.SalesForceId = this.SalesForceId;
		clone.StateRegistry = this.StateRegistry;
		clone.DateLastUpdate = this.DateLastUpdate;
		clone.CompanySubtypeId = this.CompanySubtypeId;
		clone.CnpjTestingTimestamp = this.CnpjTestingTimestamp;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Ddd))
			json['Ddd'] = this.Ddd;
		if (!ObjectUtils.NullOrUndefined(this.Cnpj))
			json['Cnpj'] = this.Cnpj;
		if (!ObjectUtils.NullOrUndefined(this.Site))
			json['Site'] = this.Site;
		if (!ObjectUtils.NullOrUndefined(this.Rank))
			json['Rank'] = this.Rank;
		if (!ObjectUtils.NullOrUndefined(this.Phone))
			json['Phone'] = this.Phone;
		if (!ObjectUtils.NullOrUndefined(this.Capital))
			json['Capital'] = this.Capital;
		if (!ObjectUtils.NullOrUndefined(this.Latitude))
			json['Latitude'] = this.Latitude;
		if (!ObjectUtils.NullOrUndefined(this.ImagesUrl))
			json['ImagesUrl'] = this.ImagesUrl;
		if (!ObjectUtils.NullOrUndefined(this.Longitude))
			json['Longitude'] = this.Longitude;
		if (!ObjectUtils.NullOrUndefined(this.FormalName))
			json['FormalName'] = this.FormalName;
		if (!ObjectUtils.NullOrUndefined(this.Description))
			json['Description'] = this.Description;
		if (!ObjectUtils.NullOrUndefined(this.FantasyName))
			json['FantasyName'] = this.FantasyName;
		if (!ObjectUtils.NullOrUndefined(this.SalesForceId))
			json['SalesForceId'] = this.SalesForceId;
		if (!ObjectUtils.NullOrUndefined(this.StateRegistry))
			json['StateRegistry'] = this.StateRegistry;
		if (!ObjectUtils.NullOrUndefined(this.Timestamp))
			json['Timestamp'] = DateUtils.ToDateServer(this.Timestamp);
		if (!ObjectUtils.NullOrUndefined(this.DateOpened))
			json['DateOpened'] = DateUtils.ToDateServer(this.DateOpened);
		if (!ObjectUtils.NullOrUndefined(this.DateLastUpdate))
			json['DateLastUpdate'] = DateUtils.ToDateServer(this.DateLastUpdate);
		if (!ObjectUtils.NullOrUndefined(this.AccountId))
			json['AccountId'] = Array.isArray(this.AccountId) ? this.AccountId : [this.AccountId];
		if (!ObjectUtils.NullOrUndefined(this.CnpjTestingTimestamp))
			json['CnpjTestingTimestamp'] = DateUtils.ToDateServer(this.CnpjTestingTimestamp);
		if (!ObjectUtils.NullOrUndefined(this.SituationId))
			json['SituationId'] = Array.isArray(this.SituationId) ? this.SituationId : [this.SituationId];
		if (!ObjectUtils.NullOrUndefined(this.CompanySubtypeId))
			json['CompanySubtypeId'] = Array.isArray(this.CompanySubtypeId) ? this.CompanySubtypeId : [this.CompanySubtypeId];


		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Ddd = json['Ddd'];
			this.Cnpj = json['Cnpj'];
			this.Site = json['Site'];
			this.Rank = json['Rank'];
			this.Phone = json['Phone'];
			this.Capital = json['Capital'];
			this.Latitude = json['Latitude'];
			this.AccountId = json['AccountId'];
			this.ImagesUrl = json['ImagesUrl'];
			this.Longitude = json['Longitude'];
			this.FormalName = json['FormalName'];
			this.Description = json['Description'];
			this.FantasyName = json['FantasyName'];
			this.SituationId = json['SituationId'];
			this.SalesForceId = json['SalesForceId'];
			this.StateRegistry = json['StateRegistry'];
			this.CompanySubtypeId = json['CompanySubtypeId'];
			this.Timestamp = DateUtils.FromDateServer(json['Timestamp']);
			this.DateOpened = DateUtils.FromDateServer(json['DateOpened']);
			this.DateLastUpdate = DateUtils.FromDateServer(json['DateLastUpdate']);
			this.CnpjTestingTimestamp = DateUtils.FromDateServer(json['CnpjTestingTimestamp']);
		}
	}
}