﻿import { Crudable } from '@src/external/plugin-utils/business/models/crud/crudable.model';

// utils
import { DateUtils } from '@src/external/plugin-utils/utils/date-utils';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// Navigation Properties
import { CompanyType } from './companyType.model';

// components helpers
import { ComponentTypeEnum } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-definition.model';

export const CompanySubtypeClass: string = `CompanySubtype`;
export const CompanySubtypeTable: string = `company_subtype`;

/**
 * Column definitions for the `CompanySubtype` class
 */
export enum CompanySubtypeColumns
{
	Id = 'Id',
	Subtype = 'Subtype',
	CompanyTypeId = 'CompanyTypeId',

	CompanyTypeIdParent = 'CompanyTypeIdParent',
}
export const CompanySubtypeColumnsFilter: string[] = [];
export const CompanySubtypeColumnsInsert: string[] = [CompanySubtypeColumns.CompanyTypeId, CompanySubtypeColumns.Subtype];
export const CompanySubtypeColumnsUpdate: string[] = [CompanySubtypeColumns.CompanyTypeId, CompanySubtypeColumns.Subtype];

/**
 * Implementations of the `CompanySubtype` class
 */
export class CompanySubtype extends Crudable
{
	public CompanyTypeIdParent: CompanyType;

	public static FromJson(json: any): CompanySubtype
	{
		const item: CompanySubtype = new CompanySubtype();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public CompanyTypeId: number = null,
		public Subtype: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`CompanySubtype`, `company_subtype`, CrudableTableDefinitionGender.Male, `Company Subtype`, `Company Subtypes`, true, true, true, true, true, true);

		this.SetColumnDefinition(CompanySubtypeColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanySubtypeColumns.CompanyTypeId, `company_type_id`, `CompanyTypeId`, `Company Type Id`, `Company Type Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanySubtypeColumns.Subtype, `subtype`, `Subtype`, `Subtype`, `Subtypes`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): CompanySubtype
	{
		const clone: CompanySubtype = new CompanySubtype();
		clone.Id = this.Id;
		clone.Subtype = this.Subtype;
		clone.CompanyTypeId = this.CompanyTypeId;

		clone.CompanyTypeIdParent = this.CompanyTypeIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.CompanyTypeIdParent == null ? '' : this.CompanyTypeIdParent.ToString()}`;
	}

	public ToJson(navigationProperties: boolean = false): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Subtype))
			json['Subtype'] = this.Subtype;
		if (navigationProperties)
		{
			if (!ObjectUtils.NullOrUndefined(this.CompanyTypeIdParent))
				json['CompanyTypeIdParent'] = this.CompanyTypeIdParent.ToJson();
		}

		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: any): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Subtype = json['Subtype'];
			this.CompanyTypeId = json['CompanyTypeId'];

			if (!ObjectUtils.NullOrUndefined(json['CompanyTypeIdParent']))
			{
				this.CompanyTypeIdParent = new CompanyType();
				this.CompanyTypeIdParent.ParseFromJson(json['CompanyTypeIdParent']);
			}
		}
	}
}

/**
 * Implementations of the `CompanySubtypeFilter` class
 */
export class CompanySubtypeFilter extends Crudable
{
	public static FromJson(json: Object): CompanySubtypeFilter
	{
		const item: CompanySubtypeFilter = new CompanySubtypeFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public CompanyTypeId: number[] = null,
		public Subtype: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`CompanySubtypeFilter`, `company_subtypeFilter`, CrudableTableDefinitionGender.Male, `Company Subtype`, `Company Subtypes`, true, true, true, true, true, true);

		this.SetColumnDefinition(CompanySubtypeColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanySubtypeColumns.CompanyTypeId, `company_type_id`, `CompanyTypeId`, `Company Type Id`, `Company Type Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanySubtypeColumns.Subtype, `subtype`, `Subtype`, `Subtype`, `Subtypes`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): CompanySubtypeFilter
	{
		const clone: CompanySubtypeFilter = new CompanySubtypeFilter();
		clone.Id = this.Id;
		clone.Subtype = this.Subtype;
		clone.CompanyTypeId = this.CompanyTypeId;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Subtype))
			json['Subtype'] = this.Subtype;
		if (!ObjectUtils.NullOrUndefined(this.CompanyTypeId))
			json['CompanyTypeId'] = Array.isArray(this.CompanyTypeId) ? this.CompanyTypeId : [this.CompanyTypeId];


		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Subtype = json['Subtype'];
			this.CompanyTypeId = json['CompanyTypeId'];
		}
	}
}