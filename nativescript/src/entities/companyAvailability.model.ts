﻿import { Crudable } from '@src/external/plugin-utils/business/models/crud/crudable.model';

// utils
import { DateUtils } from '@src/external/plugin-utils/utils/date-utils';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// Navigation Properties
import { CompanyAddress } from './companyAddress.model';

// components helpers
import { ComponentTypeEnum } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-definition.model';

export const CompanyAvailabilityClass: string = `CompanyAvailability`;
export const CompanyAvailabilityTable: string = `company_availability`;

/**
 * Column definitions for the `CompanyAvailability` class
 */
export enum CompanyAvailabilityColumns
{
	Id = 'Id',
	Lines = 'Lines',
	Over50 = 'Over50',
	Message = 'Message',
	Until20 = 'Until20',
	Network = 'Network',
	Over50Max = 'Over50Max',
	Over50Sip = 'Over50Sip',
	Tecnology = 'Tecnology',
	Timestamp = 'Timestamp',
	Until20Max = 'Until20Max',
	Until20Sip = 'Until20Sip',
	From21UpTo50 = 'From21UpTo50',
	ClosetDistance = 'ClosetDistance',
	ClosetTopSpeed = 'ClosetTopSpeed',
	TecnologyForTv = 'TecnologyForTv',
	From21UpTo50Max = 'From21UpTo50Max',
	From21UpTo50Sip = 'From21UpTo50Sip',
	CompanyAddressId = 'CompanyAddressId',

	CompanyAddressIdParent = 'CompanyAddressIdParent',
}
export const CompanyAvailabilityColumnsFilter: string[] = [];
export const CompanyAvailabilityColumnsInsert: string[] = [CompanyAvailabilityColumns.CompanyAddressId, CompanyAvailabilityColumns.Message, CompanyAvailabilityColumns.ClosetDistance, CompanyAvailabilityColumns.Lines, CompanyAvailabilityColumns.Until20, CompanyAvailabilityColumns.Until20Max, CompanyAvailabilityColumns.Until20Sip, CompanyAvailabilityColumns.From21UpTo50, CompanyAvailabilityColumns.From21UpTo50Max, CompanyAvailabilityColumns.From21UpTo50Sip, CompanyAvailabilityColumns.Over50, CompanyAvailabilityColumns.Over50Max, CompanyAvailabilityColumns.Over50Sip, CompanyAvailabilityColumns.Network, CompanyAvailabilityColumns.Tecnology, CompanyAvailabilityColumns.ClosetTopSpeed, CompanyAvailabilityColumns.TecnologyForTv, CompanyAvailabilityColumns.Timestamp];
export const CompanyAvailabilityColumnsUpdate: string[] = [CompanyAvailabilityColumns.CompanyAddressId, CompanyAvailabilityColumns.Message, CompanyAvailabilityColumns.ClosetDistance, CompanyAvailabilityColumns.Lines, CompanyAvailabilityColumns.Until20, CompanyAvailabilityColumns.Until20Max, CompanyAvailabilityColumns.Until20Sip, CompanyAvailabilityColumns.From21UpTo50, CompanyAvailabilityColumns.From21UpTo50Max, CompanyAvailabilityColumns.From21UpTo50Sip, CompanyAvailabilityColumns.Over50, CompanyAvailabilityColumns.Over50Max, CompanyAvailabilityColumns.Over50Sip, CompanyAvailabilityColumns.Network, CompanyAvailabilityColumns.Tecnology, CompanyAvailabilityColumns.ClosetTopSpeed, CompanyAvailabilityColumns.TecnologyForTv, CompanyAvailabilityColumns.Timestamp];

/**
 * Implementations of the `CompanyAvailability` class
 */
export class CompanyAvailability extends Crudable
{
	public CompanyAddressIdParent: CompanyAddress;

	public static FromJson(json: any): CompanyAvailability
	{
		const item: CompanyAvailability = new CompanyAvailability();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public CompanyAddressId: number = null,
		public Message: string = null,
		public ClosetDistance: number = null,
		public Lines: number = null,
		public Until20: number = null,
		public Until20Max: string = null,
		public Until20Sip: number = null,
		public From21UpTo50: number = null,
		public From21UpTo50Max: string = null,
		public From21UpTo50Sip: number = null,
		public Over50: number = null,
		public Over50Max: string = null,
		public Over50Sip: number = null,
		public Network: string = null,
		public Tecnology: string = null,
		public ClosetTopSpeed: number = null,
		public TecnologyForTv: string = null,
		public Timestamp: Date = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`CompanyAvailability`, `company_availability`, CrudableTableDefinitionGender.Male, `Company Availability`, `Company Availabilitys`, true, true, true, true, true, true);

		this.SetColumnDefinition(CompanyAvailabilityColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyAvailabilityColumns.CompanyAddressId, `company_address_id`, `CompanyAddressId`, `Company Address Id`, `Company Address Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyAvailabilityColumns.Message, `message`, `Message`, `Message`, `Messages`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyAvailabilityColumns.ClosetDistance, `closet_distance`, `ClosetDistance`, `Closet Distance`, `Closet Distances`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyAvailabilityColumns.Lines, `lines`, `Lines`, `Lines`, `Liness`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyAvailabilityColumns.Until20, `until_20`, `Until20`, `Until 20`, `Until 20s`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyAvailabilityColumns.Until20Max, `until_20_max`, `Until20Max`, `Until 20 Max`, `Until 20 Maxs`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyAvailabilityColumns.Until20Sip, `until_20_sip`, `Until20Sip`, `Until 20 Sip`, `Until 20 Sips`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyAvailabilityColumns.From21UpTo50, `from_21_up_to_50`, `From21UpTo50`, `From 21 Up To 50`, `From 21 Up To 50s`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyAvailabilityColumns.From21UpTo50Max, `from_21_up_to_50_max`, `From21UpTo50Max`, `From 21 Up To 50 Max`, `From 21 Up To 50 Maxs`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyAvailabilityColumns.From21UpTo50Sip, `from_21_up_to_50_sip`, `From21UpTo50Sip`, `From 21 Up To 50 Sip`, `From 21 Up To 50 Sips`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyAvailabilityColumns.Over50, `over_50`, `Over50`, `Over 50`, `Over 50s`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyAvailabilityColumns.Over50Max, `over_50_max`, `Over50Max`, `Over 50 Max`, `Over 50 Maxs`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyAvailabilityColumns.Over50Sip, `over_50_sip`, `Over50Sip`, `Over 50 Sip`, `Over 50 Sips`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyAvailabilityColumns.Network, `network`, `Network`, `Network`, `Networks`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyAvailabilityColumns.Tecnology, `tecnology`, `Tecnology`, `Tecnology`, `Tecnologys`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyAvailabilityColumns.ClosetTopSpeed, `closet_top_speed`, `ClosetTopSpeed`, `Closet Top Speed`, `Closet Top Speeds`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyAvailabilityColumns.TecnologyForTv, `tecnology_for_tv`, `TecnologyForTv`, `Tecnology For Tv`, `Tecnology For Tvs`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyAvailabilityColumns.Timestamp, `timestamp`, `Timestamp`, `Timestamp`, `Timestamps`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
	}

	public Clone(): CompanyAvailability
	{
		const clone: CompanyAvailability = new CompanyAvailability();
		clone.Id = this.Id;
		clone.Lines = this.Lines;
		clone.Over50 = this.Over50;
		clone.Message = this.Message;
		clone.Until20 = this.Until20;
		clone.Network = this.Network;
		clone.Over50Max = this.Over50Max;
		clone.Over50Sip = this.Over50Sip;
		clone.Tecnology = this.Tecnology;
		clone.Timestamp = this.Timestamp;
		clone.Until20Max = this.Until20Max;
		clone.Until20Sip = this.Until20Sip;
		clone.From21UpTo50 = this.From21UpTo50;
		clone.ClosetDistance = this.ClosetDistance;
		clone.ClosetTopSpeed = this.ClosetTopSpeed;
		clone.TecnologyForTv = this.TecnologyForTv;
		clone.From21UpTo50Max = this.From21UpTo50Max;
		clone.From21UpTo50Sip = this.From21UpTo50Sip;
		clone.CompanyAddressId = this.CompanyAddressId;

		clone.CompanyAddressIdParent = this.CompanyAddressIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.CompanyAddressIdParent == null ? '' : this.CompanyAddressIdParent.ToString()}`;
	}

	public ToJson(navigationProperties: boolean = false): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Lines))
			json['Lines'] = this.Lines;
		if (!ObjectUtils.NullOrUndefined(this.Over50))
			json['Over50'] = this.Over50;
		if (!ObjectUtils.NullOrUndefined(this.Message))
			json['Message'] = this.Message;
		if (!ObjectUtils.NullOrUndefined(this.Until20))
			json['Until20'] = this.Until20;
		if (!ObjectUtils.NullOrUndefined(this.Network))
			json['Network'] = this.Network;
		if (!ObjectUtils.NullOrUndefined(this.Over50Max))
			json['Over50Max'] = this.Over50Max;
		if (!ObjectUtils.NullOrUndefined(this.Over50Sip))
			json['Over50Sip'] = this.Over50Sip;
		if (!ObjectUtils.NullOrUndefined(this.Tecnology))
			json['Tecnology'] = this.Tecnology;
		if (!ObjectUtils.NullOrUndefined(this.Until20Max))
			json['Until20Max'] = this.Until20Max;
		if (!ObjectUtils.NullOrUndefined(this.Until20Sip))
			json['Until20Sip'] = this.Until20Sip;
		if (!ObjectUtils.NullOrUndefined(this.From21UpTo50))
			json['From21UpTo50'] = this.From21UpTo50;
		if (!ObjectUtils.NullOrUndefined(this.ClosetDistance))
			json['ClosetDistance'] = this.ClosetDistance;
		if (!ObjectUtils.NullOrUndefined(this.ClosetTopSpeed))
			json['ClosetTopSpeed'] = this.ClosetTopSpeed;
		if (!ObjectUtils.NullOrUndefined(this.TecnologyForTv))
			json['TecnologyForTv'] = this.TecnologyForTv;
		if (!ObjectUtils.NullOrUndefined(this.From21UpTo50Max))
			json['From21UpTo50Max'] = this.From21UpTo50Max;
		if (!ObjectUtils.NullOrUndefined(this.From21UpTo50Sip))
			json['From21UpTo50Sip'] = this.From21UpTo50Sip;
		if (!ObjectUtils.NullOrUndefined(this.Timestamp))
			json['Timestamp'] = DateUtils.ToDateServer(this.Timestamp);
		if (navigationProperties)
		{
			if (!ObjectUtils.NullOrUndefined(this.CompanyAddressIdParent))
				json['CompanyAddressIdParent'] = this.CompanyAddressIdParent.ToJson();
		}

		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: any): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Lines = json['Lines'];
			this.Over50 = json['Over50'];
			this.Message = json['Message'];
			this.Until20 = json['Until20'];
			this.Network = json['Network'];
			this.Over50Max = json['Over50Max'];
			this.Over50Sip = json['Over50Sip'];
			this.Tecnology = json['Tecnology'];
			this.Until20Max = json['Until20Max'];
			this.Until20Sip = json['Until20Sip'];
			this.From21UpTo50 = json['From21UpTo50'];
			this.ClosetDistance = json['ClosetDistance'];
			this.ClosetTopSpeed = json['ClosetTopSpeed'];
			this.TecnologyForTv = json['TecnologyForTv'];
			this.From21UpTo50Max = json['From21UpTo50Max'];
			this.From21UpTo50Sip = json['From21UpTo50Sip'];
			this.CompanyAddressId = json['CompanyAddressId'];
			this.Timestamp = DateUtils.FromDateServer(json['Timestamp']);

			if (!ObjectUtils.NullOrUndefined(json['CompanyAddressIdParent']))
			{
				this.CompanyAddressIdParent = new CompanyAddress();
				this.CompanyAddressIdParent.ParseFromJson(json['CompanyAddressIdParent']);
			}
		}
	}
}

/**
 * Implementations of the `CompanyAvailabilityFilter` class
 */
export class CompanyAvailabilityFilter extends Crudable
{
	public static FromJson(json: Object): CompanyAvailabilityFilter
	{
		const item: CompanyAvailabilityFilter = new CompanyAvailabilityFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public CompanyAddressId: number[] = null,
		public Message: string = null,
		public ClosetDistance: number = null,
		public Lines: number = null,
		public Until20: number = null,
		public Until20Max: string = null,
		public Until20Sip: number = null,
		public From21UpTo50: number = null,
		public From21UpTo50Max: string = null,
		public From21UpTo50Sip: number = null,
		public Over50: number = null,
		public Over50Max: string = null,
		public Over50Sip: number = null,
		public Network: string = null,
		public Tecnology: string = null,
		public ClosetTopSpeed: number = null,
		public TecnologyForTv: string = null,
		public Timestamp: Date = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`CompanyAvailabilityFilter`, `company_availabilityFilter`, CrudableTableDefinitionGender.Male, `Company Availability`, `Company Availabilitys`, true, true, true, true, true, true);

		this.SetColumnDefinition(CompanyAvailabilityColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyAvailabilityColumns.CompanyAddressId, `company_address_id`, `CompanyAddressId`, `Company Address Id`, `Company Address Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyAvailabilityColumns.Message, `message`, `Message`, `Message`, `Messages`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyAvailabilityColumns.ClosetDistance, `closet_distance`, `ClosetDistance`, `Closet Distance`, `Closet Distances`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyAvailabilityColumns.Lines, `lines`, `Lines`, `Lines`, `Liness`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyAvailabilityColumns.Until20, `until_20`, `Until20`, `Until 20`, `Until 20s`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyAvailabilityColumns.Until20Max, `until_20_max`, `Until20Max`, `Until 20 Max`, `Until 20 Maxs`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyAvailabilityColumns.Until20Sip, `until_20_sip`, `Until20Sip`, `Until 20 Sip`, `Until 20 Sips`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyAvailabilityColumns.From21UpTo50, `from_21_up_to_50`, `From21UpTo50`, `From 21 Up To 50`, `From 21 Up To 50s`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyAvailabilityColumns.From21UpTo50Max, `from_21_up_to_50_max`, `From21UpTo50Max`, `From 21 Up To 50 Max`, `From 21 Up To 50 Maxs`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyAvailabilityColumns.From21UpTo50Sip, `from_21_up_to_50_sip`, `From21UpTo50Sip`, `From 21 Up To 50 Sip`, `From 21 Up To 50 Sips`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyAvailabilityColumns.Over50, `over_50`, `Over50`, `Over 50`, `Over 50s`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyAvailabilityColumns.Over50Max, `over_50_max`, `Over50Max`, `Over 50 Max`, `Over 50 Maxs`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyAvailabilityColumns.Over50Sip, `over_50_sip`, `Over50Sip`, `Over 50 Sip`, `Over 50 Sips`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyAvailabilityColumns.Network, `network`, `Network`, `Network`, `Networks`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyAvailabilityColumns.Tecnology, `tecnology`, `Tecnology`, `Tecnology`, `Tecnologys`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyAvailabilityColumns.ClosetTopSpeed, `closet_top_speed`, `ClosetTopSpeed`, `Closet Top Speed`, `Closet Top Speeds`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyAvailabilityColumns.TecnologyForTv, `tecnology_for_tv`, `TecnologyForTv`, `Tecnology For Tv`, `Tecnology For Tvs`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyAvailabilityColumns.Timestamp, `timestamp`, `Timestamp`, `Timestamp`, `Timestamps`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
	}

	public Clone(): CompanyAvailabilityFilter
	{
		const clone: CompanyAvailabilityFilter = new CompanyAvailabilityFilter();
		clone.Id = this.Id;
		clone.Lines = this.Lines;
		clone.Over50 = this.Over50;
		clone.Message = this.Message;
		clone.Until20 = this.Until20;
		clone.Network = this.Network;
		clone.Over50Max = this.Over50Max;
		clone.Over50Sip = this.Over50Sip;
		clone.Tecnology = this.Tecnology;
		clone.Timestamp = this.Timestamp;
		clone.Until20Max = this.Until20Max;
		clone.Until20Sip = this.Until20Sip;
		clone.From21UpTo50 = this.From21UpTo50;
		clone.ClosetDistance = this.ClosetDistance;
		clone.ClosetTopSpeed = this.ClosetTopSpeed;
		clone.TecnologyForTv = this.TecnologyForTv;
		clone.From21UpTo50Max = this.From21UpTo50Max;
		clone.From21UpTo50Sip = this.From21UpTo50Sip;
		clone.CompanyAddressId = this.CompanyAddressId;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Lines))
			json['Lines'] = this.Lines;
		if (!ObjectUtils.NullOrUndefined(this.Over50))
			json['Over50'] = this.Over50;
		if (!ObjectUtils.NullOrUndefined(this.Message))
			json['Message'] = this.Message;
		if (!ObjectUtils.NullOrUndefined(this.Until20))
			json['Until20'] = this.Until20;
		if (!ObjectUtils.NullOrUndefined(this.Network))
			json['Network'] = this.Network;
		if (!ObjectUtils.NullOrUndefined(this.Over50Max))
			json['Over50Max'] = this.Over50Max;
		if (!ObjectUtils.NullOrUndefined(this.Over50Sip))
			json['Over50Sip'] = this.Over50Sip;
		if (!ObjectUtils.NullOrUndefined(this.Tecnology))
			json['Tecnology'] = this.Tecnology;
		if (!ObjectUtils.NullOrUndefined(this.Until20Max))
			json['Until20Max'] = this.Until20Max;
		if (!ObjectUtils.NullOrUndefined(this.Until20Sip))
			json['Until20Sip'] = this.Until20Sip;
		if (!ObjectUtils.NullOrUndefined(this.From21UpTo50))
			json['From21UpTo50'] = this.From21UpTo50;
		if (!ObjectUtils.NullOrUndefined(this.ClosetDistance))
			json['ClosetDistance'] = this.ClosetDistance;
		if (!ObjectUtils.NullOrUndefined(this.ClosetTopSpeed))
			json['ClosetTopSpeed'] = this.ClosetTopSpeed;
		if (!ObjectUtils.NullOrUndefined(this.TecnologyForTv))
			json['TecnologyForTv'] = this.TecnologyForTv;
		if (!ObjectUtils.NullOrUndefined(this.From21UpTo50Max))
			json['From21UpTo50Max'] = this.From21UpTo50Max;
		if (!ObjectUtils.NullOrUndefined(this.From21UpTo50Sip))
			json['From21UpTo50Sip'] = this.From21UpTo50Sip;
		if (!ObjectUtils.NullOrUndefined(this.Timestamp))
			json['Timestamp'] = DateUtils.ToDateServer(this.Timestamp);
		if (!ObjectUtils.NullOrUndefined(this.CompanyAddressId))
			json['CompanyAddressId'] = Array.isArray(this.CompanyAddressId) ? this.CompanyAddressId : [this.CompanyAddressId];


		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Lines = json['Lines'];
			this.Over50 = json['Over50'];
			this.Message = json['Message'];
			this.Until20 = json['Until20'];
			this.Network = json['Network'];
			this.Over50Max = json['Over50Max'];
			this.Over50Sip = json['Over50Sip'];
			this.Tecnology = json['Tecnology'];
			this.Until20Max = json['Until20Max'];
			this.Until20Sip = json['Until20Sip'];
			this.From21UpTo50 = json['From21UpTo50'];
			this.ClosetDistance = json['ClosetDistance'];
			this.ClosetTopSpeed = json['ClosetTopSpeed'];
			this.TecnologyForTv = json['TecnologyForTv'];
			this.From21UpTo50Max = json['From21UpTo50Max'];
			this.From21UpTo50Sip = json['From21UpTo50Sip'];
			this.CompanyAddressId = json['CompanyAddressId'];
			this.Timestamp = DateUtils.FromDateServer(json['Timestamp']);
		}
	}
}