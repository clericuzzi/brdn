﻿import { Crudable } from '@src/external/plugin-utils/business/models/crud/crudable.model';

// utils
import { DateUtils } from '@src/external/plugin-utils/utils/date-utils';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// Navigation Properties
import { Company } from './company.model';
import { GeographyZipcode } from '@src/external/plugin-utils/business/models/models-geo/geographyZipcode.model';

// components helpers
import { ComponentTypeEnum } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-definition.model';

export const CompanyAddressClass: string = `CompanyAddress`;
export const CompanyAddressTable: string = `company_address`;

/**
 * Column definitions for the `CompanyAddress` class
 */
export enum CompanyAddressColumns
{
	Id = 'Id',
	Street = 'Street',
	Number = 'Number',
	CompanyId = 'CompanyId',
	ZipcodeId = 'ZipcodeId',
	Reference = 'Reference',
	Compliment = 'Compliment',
	TestingTimestamp = 'TestingTimestamp',

	CompanyIdParent = 'CompanyIdParent',
	ZipcodeIdParent = 'ZipcodeIdParent',
}
export const CompanyAddressColumnsFilter: string[] = [];
export const CompanyAddressColumnsInsert: string[] = [CompanyAddressColumns.CompanyId, CompanyAddressColumns.Street, CompanyAddressColumns.Number, CompanyAddressColumns.Compliment, CompanyAddressColumns.ZipcodeId, CompanyAddressColumns.Reference, CompanyAddressColumns.TestingTimestamp];
export const CompanyAddressColumnsUpdate: string[] = [CompanyAddressColumns.CompanyId, CompanyAddressColumns.Street, CompanyAddressColumns.Number, CompanyAddressColumns.Compliment, CompanyAddressColumns.ZipcodeId, CompanyAddressColumns.Reference, CompanyAddressColumns.TestingTimestamp];

/**
 * Implementations of the `CompanyAddress` class
 */
export class CompanyAddress extends Crudable
{
	public CompanyIdParent: Company;
	public ZipcodeIdParent: GeographyZipcode;

	public static FromJson(json: any): CompanyAddress
	{
		const item: CompanyAddress = new CompanyAddress();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public CompanyId: number = null,
		public Street: string = null,
		public Number: string = null,
		public Compliment: string = null,
		public ZipcodeId: number = null,
		public Reference: string = null,
		public TestingTimestamp: Date = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`CompanyAddress`, `company_address`, CrudableTableDefinitionGender.Male, `Company Address`, `Company Addresss`, true, true, true, true, true, true);

		this.SetColumnDefinition(CompanyAddressColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyAddressColumns.CompanyId, `company_id`, `CompanyId`, `Company Id`, `Company Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyAddressColumns.Street, `street`, `Street`, `Street`, `Streets`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyAddressColumns.Number, `number`, `Number`, `Number`, `Numbers`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyAddressColumns.Compliment, `compliment`, `Compliment`, `Compliment`, `Compliments`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyAddressColumns.ZipcodeId, `zipcode_id`, `ZipcodeId`, `Zipcode Id`, `Zipcode Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyAddressColumns.Reference, `reference`, `Reference`, `Reference`, `References`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyAddressColumns.TestingTimestamp, `testing_timestamp`, `TestingTimestamp`, `Testing Timestamp`, `Testing Timestamps`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, true);
	}

	public Clone(): CompanyAddress
	{
		const clone: CompanyAddress = new CompanyAddress();
		clone.Id = this.Id;
		clone.Street = this.Street;
		clone.Number = this.Number;
		clone.CompanyId = this.CompanyId;
		clone.ZipcodeId = this.ZipcodeId;
		clone.Reference = this.Reference;
		clone.Compliment = this.Compliment;
		clone.TestingTimestamp = this.TestingTimestamp;

		clone.CompanyIdParent = this.CompanyIdParent;
		clone.ZipcodeIdParent = this.ZipcodeIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.CompanyIdParent == null ? '' : this.CompanyIdParent.ToString()}`;
	}

	public ToJson(navigationProperties: boolean = false): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Street))
			json['Street'] = this.Street;
		if (!ObjectUtils.NullOrUndefined(this.Number))
			json['Number'] = this.Number;
		if (!ObjectUtils.NullOrUndefined(this.Reference))
			json['Reference'] = this.Reference;
		if (!ObjectUtils.NullOrUndefined(this.Compliment))
			json['Compliment'] = this.Compliment;
		if (!ObjectUtils.NullOrUndefined(this.TestingTimestamp))
			json['TestingTimestamp'] = DateUtils.ToDateServer(this.TestingTimestamp);
		if (navigationProperties)
		{
			if (!ObjectUtils.NullOrUndefined(this.CompanyIdParent))
				json['CompanyIdParent'] = this.CompanyIdParent.ToJson();
			if (!ObjectUtils.NullOrUndefined(this.ZipcodeIdParent))
				json['ZipcodeIdParent'] = this.ZipcodeIdParent.ToJson();
		}

		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: any): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Street = json['Street'];
			this.Number = json['Number'];
			this.CompanyId = json['CompanyId'];
			this.ZipcodeId = json['ZipcodeId'];
			this.Reference = json['Reference'];
			this.Compliment = json['Compliment'];
			this.TestingTimestamp = DateUtils.FromDateServer(json['TestingTimestamp']);

			if (!ObjectUtils.NullOrUndefined(json['CompanyIdParent']))
			{
				this.CompanyIdParent = new Company();
				this.CompanyIdParent.ParseFromJson(json['CompanyIdParent']);
			}
			if (!ObjectUtils.NullOrUndefined(json['ZipcodeIdParent']))
			{
				this.ZipcodeIdParent = new GeographyZipcode();
				this.ZipcodeIdParent.ParseFromJson(json['ZipcodeIdParent']);
			}
		}
	}
}

/**
 * Implementations of the `CompanyAddressFilter` class
 */
export class CompanyAddressFilter extends Crudable
{
	public static FromJson(json: Object): CompanyAddressFilter
	{
		const item: CompanyAddressFilter = new CompanyAddressFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public CompanyId: number[] = null,
		public Street: string = null,
		public Number: string = null,
		public Compliment: string = null,
		public ZipcodeId: number[] = null,
		public Reference: string = null,
		public TestingTimestamp: Date = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`CompanyAddressFilter`, `company_addressFilter`, CrudableTableDefinitionGender.Male, `Company Address`, `Company Addresss`, true, true, true, true, true, true);

		this.SetColumnDefinition(CompanyAddressColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyAddressColumns.CompanyId, `company_id`, `CompanyId`, `Company Id`, `Company Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyAddressColumns.Street, `street`, `Street`, `Street`, `Streets`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyAddressColumns.Number, `number`, `Number`, `Number`, `Numbers`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyAddressColumns.Compliment, `compliment`, `Compliment`, `Compliment`, `Compliments`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyAddressColumns.ZipcodeId, `zipcode_id`, `ZipcodeId`, `Zipcode Id`, `Zipcode Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyAddressColumns.Reference, `reference`, `Reference`, `Reference`, `References`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyAddressColumns.TestingTimestamp, `testing_timestamp`, `TestingTimestamp`, `Testing Timestamp`, `Testing Timestamps`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, true);
	}

	public Clone(): CompanyAddressFilter
	{
		const clone: CompanyAddressFilter = new CompanyAddressFilter();
		clone.Id = this.Id;
		clone.Street = this.Street;
		clone.Number = this.Number;
		clone.CompanyId = this.CompanyId;
		clone.ZipcodeId = this.ZipcodeId;
		clone.Reference = this.Reference;
		clone.Compliment = this.Compliment;
		clone.TestingTimestamp = this.TestingTimestamp;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Street))
			json['Street'] = this.Street;
		if (!ObjectUtils.NullOrUndefined(this.Number))
			json['Number'] = this.Number;
		if (!ObjectUtils.NullOrUndefined(this.Reference))
			json['Reference'] = this.Reference;
		if (!ObjectUtils.NullOrUndefined(this.Compliment))
			json['Compliment'] = this.Compliment;
		if (!ObjectUtils.NullOrUndefined(this.TestingTimestamp))
			json['TestingTimestamp'] = DateUtils.ToDateServer(this.TestingTimestamp);
		if (!ObjectUtils.NullOrUndefined(this.CompanyId))
			json['CompanyId'] = Array.isArray(this.CompanyId) ? this.CompanyId : [this.CompanyId];
		if (!ObjectUtils.NullOrUndefined(this.ZipcodeId))
			json['ZipcodeId'] = Array.isArray(this.ZipcodeId) ? this.ZipcodeId : [this.ZipcodeId];


		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Street = json['Street'];
			this.Number = json['Number'];
			this.CompanyId = json['CompanyId'];
			this.ZipcodeId = json['ZipcodeId'];
			this.Reference = json['Reference'];
			this.Compliment = json['Compliment'];
			this.TestingTimestamp = DateUtils.FromDateServer(json['TestingTimestamp']);
		}
	}
}