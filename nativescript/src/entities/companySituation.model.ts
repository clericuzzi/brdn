﻿import { Crudable } from '@src/external/plugin-utils/business/models/crud/crudable.model';

// utils
import { DateUtils } from '@src/external/plugin-utils/utils/date-utils';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// components helpers
import { ComponentTypeEnum } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-definition.model';

export const CompanySituationClass: string = `CompanySituation`;
export const CompanySituationTable: string = `company_situation`;

/**
 * Column definitions for the `CompanySituation` class
 */
export enum CompanySituationColumns
{
	Id = 'Id',
	Name = 'Name',
}
export const CompanySituationColumnsFilter: string[] = [];
export const CompanySituationColumnsInsert: string[] = [CompanySituationColumns.Name];
export const CompanySituationColumnsUpdate: string[] = [CompanySituationColumns.Name];

/**
 * Implementations of the `CompanySituation` class
 */
export class CompanySituation extends Crudable
{
	public static FromJson(json: any): CompanySituation
	{
		const item: CompanySituation = new CompanySituation();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public Name: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`CompanySituation`, `company_situation`, CrudableTableDefinitionGender.Male, `Company Situation`, `Company Situations`, true, true, true, true, true, true);

		this.SetColumnDefinition(CompanySituationColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanySituationColumns.Name, `name`, `Name`, `Name`, `Names`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
	}

	public Clone(): CompanySituation
	{
		const clone: CompanySituation = new CompanySituation();
		clone.Id = this.Id;
		clone.Name = this.Name;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.Name}`;
	}

	public ToJson(navigationProperties: boolean = false): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json['Name'] = this.Name;
		if (navigationProperties)
		{
		}

		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: any): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Name = json['Name'];
		}
	}
}

/**
 * Implementations of the `CompanySituationFilter` class
 */
export class CompanySituationFilter extends Crudable
{
	public static FromJson(json: Object): CompanySituationFilter
	{
		const item: CompanySituationFilter = new CompanySituationFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public Name: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`CompanySituationFilter`, `company_situationFilter`, CrudableTableDefinitionGender.Male, `Company Situation`, `Company Situations`, true, true, true, true, true, true);

		this.SetColumnDefinition(CompanySituationColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanySituationColumns.Name, `name`, `Name`, `Name`, `Names`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
	}

	public Clone(): CompanySituationFilter
	{
		const clone: CompanySituationFilter = new CompanySituationFilter();
		clone.Id = this.Id;
		clone.Name = this.Name;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json['Name'] = this.Name;


		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Name = json['Name'];
		}
	}
}