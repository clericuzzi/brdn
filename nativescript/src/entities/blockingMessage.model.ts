﻿import { Crudable } from '@src/external/plugin-utils/business/models/crud/crudable.model';

// utils
import { DateUtils } from '@src/external/plugin-utils/utils/date-utils';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// components helpers
import { ComponentTypeEnum } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-definition.model';

export const BlockingMessageClass: string = `BlockingMessage`;
export const BlockingMessageTable: string = `blocking_message`;

/**
 * Column definitions for the `BlockingMessage` class
 */
export enum BlockingMessageColumns
{
	Id = 'Id',
	Text = 'Text',
}
export const BlockingMessageColumnsFilter: string[] = [];
export const BlockingMessageColumnsInsert: string[] = [BlockingMessageColumns.Text];
export const BlockingMessageColumnsUpdate: string[] = [BlockingMessageColumns.Text];

/**
 * Implementations of the `BlockingMessage` class
 */
export class BlockingMessage extends Crudable
{
	public static FromJson(json: any): BlockingMessage
	{
		const item: BlockingMessage = new BlockingMessage();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public Text: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`BlockingMessage`, `blocking_message`, CrudableTableDefinitionGender.Male, `Blocking Message`, `Blocking Messages`, true, true, true, true, true, true);

		this.SetColumnDefinition(BlockingMessageColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(BlockingMessageColumns.Text, `text`, `Text`, `Text`, `Texts`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): BlockingMessage
	{
		const clone: BlockingMessage = new BlockingMessage();
		clone.Id = this.Id;
		clone.Text = this.Text;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.Text}`;
	}

	public ToJson(navigationProperties: boolean = false): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Text))
			json['Text'] = this.Text;
		if (navigationProperties)
		{
		}

		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: any): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Text = json['Text'];
		}
	}
}

/**
 * Implementations of the `BlockingMessageFilter` class
 */
export class BlockingMessageFilter extends Crudable
{
	public static FromJson(json: Object): BlockingMessageFilter
	{
		const item: BlockingMessageFilter = new BlockingMessageFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public Text: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`BlockingMessageFilter`, `blocking_messageFilter`, CrudableTableDefinitionGender.Male, `Blocking Message`, `Blocking Messages`, true, true, true, true, true, true);

		this.SetColumnDefinition(BlockingMessageColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(BlockingMessageColumns.Text, `text`, `Text`, `Text`, `Texts`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): BlockingMessageFilter
	{
		const clone: BlockingMessageFilter = new BlockingMessageFilter();
		clone.Id = this.Id;
		clone.Text = this.Text;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Text))
			json['Text'] = this.Text;


		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Text = json['Text'];
		}
	}
}