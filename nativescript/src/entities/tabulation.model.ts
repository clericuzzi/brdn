﻿import { Crudable } from '@src/external/plugin-utils/business/models/crud/crudable.model';

// utils
import { DateUtils } from '@src/external/plugin-utils/utils/date-utils';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// Navigation Properties
import { TabulationType } from './tabulationType.model';

// components helpers
import { ComponentTypeEnum } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-definition.model';

export const TabulationClass: string = `Tabulation`;
export const TabulationTable: string = `tabulation`;

/**
 * Column definitions for the `Tabulation` class
 */
export enum TabulationColumns
{
	Id = 'Id',
	Text = 'Text',
	TabulationTypeId = 'TabulationTypeId',

	TabulationTypeIdParent = 'TabulationTypeIdParent',
}
export const TabulationColumnsFilter: string[] = [];
export const TabulationColumnsInsert: string[] = [TabulationColumns.TabulationTypeId, TabulationColumns.Text];
export const TabulationColumnsUpdate: string[] = [TabulationColumns.TabulationTypeId, TabulationColumns.Text];

/**
 * Implementations of the `Tabulation` class
 */
export class Tabulation extends Crudable
{
	public TabulationTypeIdParent: TabulationType;

	public static FromJson(json: any): Tabulation
	{
		const item: Tabulation = new Tabulation();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public TabulationTypeId: number = null,
		public Text: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`Tabulation`, `tabulation`, CrudableTableDefinitionGender.Male, `Tabulation`, `Tabulations`, true, true, true, true, true, true);

		this.SetColumnDefinition(TabulationColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(TabulationColumns.TabulationTypeId, `tabulation_type_id`, `TabulationTypeId`, `Tabulation Type Id`, `Tabulation Type Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(TabulationColumns.Text, `text`, `Text`, `Text`, `Texts`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): Tabulation
	{
		const clone: Tabulation = new Tabulation();
		clone.Id = this.Id;
		clone.Text = this.Text;
		clone.TabulationTypeId = this.TabulationTypeId;

		clone.TabulationTypeIdParent = this.TabulationTypeIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.TabulationTypeIdParent == null ? '' : this.TabulationTypeIdParent.ToString()}`;
	}

	public ToJson(navigationProperties: boolean = false): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Text))
			json['Text'] = this.Text;
		if (navigationProperties)
		{
			if (!ObjectUtils.NullOrUndefined(this.TabulationTypeIdParent))
				json['TabulationTypeIdParent'] = this.TabulationTypeIdParent.ToJson();
		}

		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: any): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Text = json['Text'];
			this.TabulationTypeId = json['TabulationTypeId'];

			if (!ObjectUtils.NullOrUndefined(json['TabulationTypeIdParent']))
			{
				this.TabulationTypeIdParent = new TabulationType();
				this.TabulationTypeIdParent.ParseFromJson(json['TabulationTypeIdParent']);
			}
		}
	}
}

/**
 * Implementations of the `TabulationFilter` class
 */
export class TabulationFilter extends Crudable
{
	public static FromJson(json: Object): TabulationFilter
	{
		const item: TabulationFilter = new TabulationFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public TabulationTypeId: number[] = null,
		public Text: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`TabulationFilter`, `tabulationFilter`, CrudableTableDefinitionGender.Male, `Tabulation`, `Tabulations`, true, true, true, true, true, true);

		this.SetColumnDefinition(TabulationColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(TabulationColumns.TabulationTypeId, `tabulation_type_id`, `TabulationTypeId`, `Tabulation Type Id`, `Tabulation Type Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(TabulationColumns.Text, `text`, `Text`, `Text`, `Texts`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): TabulationFilter
	{
		const clone: TabulationFilter = new TabulationFilter();
		clone.Id = this.Id;
		clone.Text = this.Text;
		clone.TabulationTypeId = this.TabulationTypeId;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Text))
			json['Text'] = this.Text;
		if (!ObjectUtils.NullOrUndefined(this.TabulationTypeId))
			json['TabulationTypeId'] = Array.isArray(this.TabulationTypeId) ? this.TabulationTypeId : [this.TabulationTypeId];


		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Text = json['Text'];
			this.TabulationTypeId = json['TabulationTypeId'];
		}
	}
}