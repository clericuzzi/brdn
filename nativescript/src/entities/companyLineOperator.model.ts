﻿import { Crudable } from '@src/external/plugin-utils/business/models/crud/crudable.model';

// utils
import { DateUtils } from '@src/external/plugin-utils/utils/date-utils';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// Navigation Properties
import { CompanyLine } from './companyLine.model';
import { PhoneOperator } from './phoneOperator.model';

// components helpers
import { ComponentTypeEnum } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-definition.model';

export const CompanyLineOperatorClass: string = `CompanyLineOperator`;
export const CompanyLineOperatorTable: string = `company_line_operator`;

/**
 * Column definitions for the `CompanyLineOperator` class
 */
export enum CompanyLineOperatorColumns
{
	Id = 'Id',
	OperatorId = 'OperatorId',
	CompanyLineId = 'CompanyLineId',
	OperatorSince = 'OperatorSince',

	OperatorIdParent = 'OperatorIdParent',
	CompanyLineIdParent = 'CompanyLineIdParent',
}
export const CompanyLineOperatorColumnsFilter: string[] = [];
export const CompanyLineOperatorColumnsInsert: string[] = [CompanyLineOperatorColumns.CompanyLineId, CompanyLineOperatorColumns.OperatorId, CompanyLineOperatorColumns.OperatorSince];
export const CompanyLineOperatorColumnsUpdate: string[] = [CompanyLineOperatorColumns.CompanyLineId, CompanyLineOperatorColumns.OperatorId, CompanyLineOperatorColumns.OperatorSince];

/**
 * Implementations of the `CompanyLineOperator` class
 */
export class CompanyLineOperator extends Crudable
{
	public CompanyLineIdParent: CompanyLine;
	public OperatorIdParent: PhoneOperator;

	public static FromJson(json: any): CompanyLineOperator
	{
		const item: CompanyLineOperator = new CompanyLineOperator();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public CompanyLineId: number = null,
		public OperatorId: number = null,
		public OperatorSince: Date = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`CompanyLineOperator`, `company_line_operator`, CrudableTableDefinitionGender.Male, `Company Line Operator`, `Company Line Operators`, true, true, true, true, true, true);

		this.SetColumnDefinition(CompanyLineOperatorColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyLineOperatorColumns.CompanyLineId, `company_line_id`, `CompanyLineId`, `Company Line Id`, `Company Line Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyLineOperatorColumns.OperatorId, `operator_id`, `OperatorId`, `Operator Id`, `Operator Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyLineOperatorColumns.OperatorSince, `operator_since`, `OperatorSince`, `Operator Since`, `Operator Sinces`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, true);
	}

	public Clone(): CompanyLineOperator
	{
		const clone: CompanyLineOperator = new CompanyLineOperator();
		clone.Id = this.Id;
		clone.OperatorId = this.OperatorId;
		clone.CompanyLineId = this.CompanyLineId;
		clone.OperatorSince = this.OperatorSince;

		clone.OperatorIdParent = this.OperatorIdParent;
		clone.CompanyLineIdParent = this.CompanyLineIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.CompanyLineIdParent == null ? '' : this.CompanyLineIdParent.ToString()}`;
	}

	public ToJson(navigationProperties: boolean = false): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.OperatorSince))
			json['OperatorSince'] = DateUtils.ToDateServer(this.OperatorSince);
		if (navigationProperties)
		{
			if (!ObjectUtils.NullOrUndefined(this.CompanyLineIdParent))
				json['CompanyLineIdParent'] = this.CompanyLineIdParent.ToJson();
			if (!ObjectUtils.NullOrUndefined(this.OperatorIdParent))
				json['OperatorIdParent'] = this.OperatorIdParent.ToJson();
		}

		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: any): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.OperatorId = json['OperatorId'];
			this.CompanyLineId = json['CompanyLineId'];
			this.OperatorSince = DateUtils.FromDateServer(json['OperatorSince']);

			if (!ObjectUtils.NullOrUndefined(json['CompanyLineIdParent']))
			{
				this.CompanyLineIdParent = new CompanyLine();
				this.CompanyLineIdParent.ParseFromJson(json['CompanyLineIdParent']);
			}
			if (!ObjectUtils.NullOrUndefined(json['OperatorIdParent']))
			{
				this.OperatorIdParent = new PhoneOperator();
				this.OperatorIdParent.ParseFromJson(json['OperatorIdParent']);
			}
		}
	}
}

/**
 * Implementations of the `CompanyLineOperatorFilter` class
 */
export class CompanyLineOperatorFilter extends Crudable
{
	public static FromJson(json: Object): CompanyLineOperatorFilter
	{
		const item: CompanyLineOperatorFilter = new CompanyLineOperatorFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public CompanyLineId: number[] = null,
		public OperatorId: number[] = null,
		public OperatorSince: Date = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`CompanyLineOperatorFilter`, `company_line_operatorFilter`, CrudableTableDefinitionGender.Male, `Company Line Operator`, `Company Line Operators`, true, true, true, true, true, true);

		this.SetColumnDefinition(CompanyLineOperatorColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyLineOperatorColumns.CompanyLineId, `company_line_id`, `CompanyLineId`, `Company Line Id`, `Company Line Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyLineOperatorColumns.OperatorId, `operator_id`, `OperatorId`, `Operator Id`, `Operator Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyLineOperatorColumns.OperatorSince, `operator_since`, `OperatorSince`, `Operator Since`, `Operator Sinces`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, true);
	}

	public Clone(): CompanyLineOperatorFilter
	{
		const clone: CompanyLineOperatorFilter = new CompanyLineOperatorFilter();
		clone.Id = this.Id;
		clone.OperatorId = this.OperatorId;
		clone.CompanyLineId = this.CompanyLineId;
		clone.OperatorSince = this.OperatorSince;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.OperatorSince))
			json['OperatorSince'] = DateUtils.ToDateServer(this.OperatorSince);
		if (!ObjectUtils.NullOrUndefined(this.OperatorId))
			json['OperatorId'] = Array.isArray(this.OperatorId) ? this.OperatorId : [this.OperatorId];
		if (!ObjectUtils.NullOrUndefined(this.CompanyLineId))
			json['CompanyLineId'] = Array.isArray(this.CompanyLineId) ? this.CompanyLineId : [this.CompanyLineId];


		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.OperatorId = json['OperatorId'];
			this.CompanyLineId = json['CompanyLineId'];
			this.OperatorSince = DateUtils.FromDateServer(json['OperatorSince']);
		}
	}
}