﻿import { Crudable } from '@src/external/plugin-utils/business/models/crud/crudable.model';

// utils
import { DateUtils } from '@src/external/plugin-utils/utils/date-utils';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// Navigation Properties
import { User } from '@src/external/plugin-utils/business/models/models/user.model';

// components helpers
import { ComponentTypeEnum } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-definition.model';

export const ImportFileClass: string = `ImportFile`;
export const ImportFileTable: string = `import_file`;

/**
 * Column definitions for the `ImportFile` class
 */
export enum ImportFileColumns
{
	Id = 'Id',
	UserId = 'UserId',
	Active = 'Active',
	Timestamp = 'Timestamp',
	CampaignName = 'CampaignName',

	UserIdParent = 'UserIdParent',
}
export const ImportFileColumnsFilter: string[] = [];
export const ImportFileColumnsInsert: string[] = [ImportFileColumns.UserId, ImportFileColumns.CampaignName, ImportFileColumns.Active, ImportFileColumns.Timestamp];
export const ImportFileColumnsUpdate: string[] = [ImportFileColumns.UserId, ImportFileColumns.CampaignName, ImportFileColumns.Active, ImportFileColumns.Timestamp];

/**
 * Implementations of the `ImportFile` class
 */
export class ImportFile extends Crudable
{
	public UserIdParent: User;

	public static FromJson(json: any): ImportFile
	{
		const item: ImportFile = new ImportFile();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public UserId: number = null,
		public CampaignName: string = null,
		public Active: number = null,
		public Timestamp: Date = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`ImportFile`, `import_file`, CrudableTableDefinitionGender.Male, `Import File`, `Import Files`, true, true, true, true, true, true);

		this.SetColumnDefinition(ImportFileColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ImportFileColumns.UserId, `user_id`, `UserId`, `User Id`, `User Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ImportFileColumns.CampaignName, `campaign_name`, `CampaignName`, `Campaign Name`, `Campaign Names`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ImportFileColumns.Active, `active`, `Active`, `Active`, `Actives`, ComponentTypeEnum.Number, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ImportFileColumns.Timestamp, `timestamp`, `Timestamp`, `Timestamp`, `Timestamps`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
	}

	public Clone(): ImportFile
	{
		const clone: ImportFile = new ImportFile();
		clone.Id = this.Id;
		clone.UserId = this.UserId;
		clone.Active = this.Active;
		clone.Timestamp = this.Timestamp;
		clone.CampaignName = this.CampaignName;

		clone.UserIdParent = this.UserIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.UserIdParent == null ? '' : this.UserIdParent.ToString()}`;
	}

	public ToJson(navigationProperties: boolean = false): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Active))
			json['Active'] = this.Active;
		if (!ObjectUtils.NullOrUndefined(this.CampaignName))
			json['CampaignName'] = this.CampaignName;
		if (!ObjectUtils.NullOrUndefined(this.Timestamp))
			json['Timestamp'] = DateUtils.ToDateServer(this.Timestamp);
		if (navigationProperties)
		{
			if (!ObjectUtils.NullOrUndefined(this.UserIdParent))
				json['UserIdParent'] = this.UserIdParent.ToJson();
		}

		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: any): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.UserId = json['UserId'];
			this.Active = json['Active'];
			this.CampaignName = json['CampaignName'];
			this.Timestamp = DateUtils.FromDateServer(json['Timestamp']);

			if (!ObjectUtils.NullOrUndefined(json['UserIdParent']))
			{
				this.UserIdParent = new User();
				this.UserIdParent.ParseFromJson(json['UserIdParent']);
			}
		}
	}
}

/**
 * Implementations of the `ImportFileFilter` class
 */
export class ImportFileFilter extends Crudable
{
	public static FromJson(json: Object): ImportFileFilter
	{
		const item: ImportFileFilter = new ImportFileFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public UserId: number[] = null,
		public CampaignName: string = null,
		public Active: number = null,
		public Timestamp: Date = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`ImportFileFilter`, `import_fileFilter`, CrudableTableDefinitionGender.Male, `Import File`, `Import Files`, true, true, true, true, true, true);

		this.SetColumnDefinition(ImportFileColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ImportFileColumns.UserId, `user_id`, `UserId`, `User Id`, `User Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ImportFileColumns.CampaignName, `campaign_name`, `CampaignName`, `Campaign Name`, `Campaign Names`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ImportFileColumns.Active, `active`, `Active`, `Active`, `Actives`, ComponentTypeEnum.Number, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ImportFileColumns.Timestamp, `timestamp`, `Timestamp`, `Timestamp`, `Timestamps`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
	}

	public Clone(): ImportFileFilter
	{
		const clone: ImportFileFilter = new ImportFileFilter();
		clone.Id = this.Id;
		clone.UserId = this.UserId;
		clone.Active = this.Active;
		clone.Timestamp = this.Timestamp;
		clone.CampaignName = this.CampaignName;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Active))
			json['Active'] = this.Active;
		if (!ObjectUtils.NullOrUndefined(this.CampaignName))
			json['CampaignName'] = this.CampaignName;
		if (!ObjectUtils.NullOrUndefined(this.Timestamp))
			json['Timestamp'] = DateUtils.ToDateServer(this.Timestamp);
		if (!ObjectUtils.NullOrUndefined(this.UserId))
			json['UserId'] = Array.isArray(this.UserId) ? this.UserId : [this.UserId];


		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.UserId = json['UserId'];
			this.Active = json['Active'];
			this.CampaignName = json['CampaignName'];
			this.Timestamp = DateUtils.FromDateServer(json['Timestamp']);
		}
	}
}