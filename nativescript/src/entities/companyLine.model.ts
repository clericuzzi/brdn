﻿import { Crudable } from '@src/external/plugin-utils/business/models/crud/crudable.model';

// utils
import { DateUtils } from '@src/external/plugin-utils/utils/date-utils';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// Navigation Properties
import { Company } from './company.model';
import { PhoneOperator } from './phoneOperator.model';

// components helpers
import { ComponentTypeEnum } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-definition.model';

export const CompanyLineClass: string = `CompanyLine`;
export const CompanyLineTable: string = `company_line`;

/**
 * Column definitions for the `CompanyLine` class
 */
export enum CompanyLineColumns
{
	Id = 'Id',
	Ddd = 'Ddd',
	Phone = 'Phone',
	Mobile = 'Mobile',
	CompanyId = 'CompanyId',
	OperatorId = 'OperatorId',
	OperatorSince = 'OperatorSince',
	TestingTimestamp = 'TestingTimestamp',

	CompanyIdParent = 'CompanyIdParent',
	OperatorIdParent = 'OperatorIdParent',
}
export const CompanyLineColumnsFilter: string[] = [];
export const CompanyLineColumnsInsert: string[] = [CompanyLineColumns.CompanyId, CompanyLineColumns.Ddd, CompanyLineColumns.Phone, CompanyLineColumns.Mobile, CompanyLineColumns.OperatorId, CompanyLineColumns.OperatorSince, CompanyLineColumns.TestingTimestamp];
export const CompanyLineColumnsUpdate: string[] = [CompanyLineColumns.CompanyId, CompanyLineColumns.Ddd, CompanyLineColumns.Phone, CompanyLineColumns.Mobile, CompanyLineColumns.OperatorId, CompanyLineColumns.OperatorSince, CompanyLineColumns.TestingTimestamp];

/**
 * Implementations of the `CompanyLine` class
 */
export class CompanyLine extends Crudable
{
	public CompanyIdParent: Company;
	public OperatorIdParent: PhoneOperator;

	public static FromJson(json: any): CompanyLine
	{
		const item: CompanyLine = new CompanyLine();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public CompanyId: number = null,
		public Ddd: string = null,
		public Phone: string = null,
		public Mobile: number = null,
		public OperatorId: number = null,
		public OperatorSince: Date = null,
		public TestingTimestamp: Date = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`CompanyLine`, `company_line`, CrudableTableDefinitionGender.Male, `Company Line`, `Company Lines`, true, true, true, true, true, true);

		this.SetColumnDefinition(CompanyLineColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyLineColumns.CompanyId, `company_id`, `CompanyId`, `Company Id`, `Company Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyLineColumns.Ddd, `ddd`, `Ddd`, `Ddd`, `Ddds`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyLineColumns.Phone, `phone`, `Phone`, `Phone`, `Phones`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyLineColumns.Mobile, `mobile`, `Mobile`, `Mobile`, `Mobiles`, ComponentTypeEnum.Number, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyLineColumns.OperatorId, `operator_id`, `OperatorId`, `Operator Id`, `Operator Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyLineColumns.OperatorSince, `operator_since`, `OperatorSince`, `Operator Since`, `Operator Sinces`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyLineColumns.TestingTimestamp, `testing_timestamp`, `TestingTimestamp`, `Testing Timestamp`, `Testing Timestamps`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, true);
	}

	public Clone(): CompanyLine
	{
		const clone: CompanyLine = new CompanyLine();
		clone.Id = this.Id;
		clone.Ddd = this.Ddd;
		clone.Phone = this.Phone;
		clone.Mobile = this.Mobile;
		clone.CompanyId = this.CompanyId;
		clone.OperatorId = this.OperatorId;
		clone.OperatorSince = this.OperatorSince;
		clone.TestingTimestamp = this.TestingTimestamp;

		clone.CompanyIdParent = this.CompanyIdParent;
		clone.OperatorIdParent = this.OperatorIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.CompanyIdParent == null ? '' : this.CompanyIdParent.ToString()}`;
	}

	public ToJson(navigationProperties: boolean = false): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Ddd))
			json['Ddd'] = this.Ddd;
		if (!ObjectUtils.NullOrUndefined(this.Phone))
			json['Phone'] = this.Phone;
		if (!ObjectUtils.NullOrUndefined(this.Mobile))
			json['Mobile'] = this.Mobile;
		if (!ObjectUtils.NullOrUndefined(this.OperatorSince))
			json['OperatorSince'] = DateUtils.ToDateServer(this.OperatorSince);
		if (!ObjectUtils.NullOrUndefined(this.TestingTimestamp))
			json['TestingTimestamp'] = DateUtils.ToDateServer(this.TestingTimestamp);
		if (navigationProperties)
		{
			if (!ObjectUtils.NullOrUndefined(this.CompanyIdParent))
				json['CompanyIdParent'] = this.CompanyIdParent.ToJson();
			if (!ObjectUtils.NullOrUndefined(this.OperatorIdParent))
				json['OperatorIdParent'] = this.OperatorIdParent.ToJson();
		}

		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: any): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Ddd = json['Ddd'];
			this.Phone = json['Phone'];
			this.Mobile = json['Mobile'];
			this.CompanyId = json['CompanyId'];
			this.OperatorId = json['OperatorId'];
			this.OperatorSince = DateUtils.FromDateServer(json['OperatorSince']);
			this.TestingTimestamp = DateUtils.FromDateServer(json['TestingTimestamp']);

			if (!ObjectUtils.NullOrUndefined(json['CompanyIdParent']))
			{
				this.CompanyIdParent = new Company();
				this.CompanyIdParent.ParseFromJson(json['CompanyIdParent']);
			}
			if (!ObjectUtils.NullOrUndefined(json['OperatorIdParent']))
			{
				this.OperatorIdParent = new PhoneOperator();
				this.OperatorIdParent.ParseFromJson(json['OperatorIdParent']);
			}
		}
	}
}

/**
 * Implementations of the `CompanyLineFilter` class
 */
export class CompanyLineFilter extends Crudable
{
	public static FromJson(json: Object): CompanyLineFilter
	{
		const item: CompanyLineFilter = new CompanyLineFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public CompanyId: number[] = null,
		public Ddd: string = null,
		public Phone: string = null,
		public Mobile: number = null,
		public OperatorId: number[] = null,
		public OperatorSince: Date = null,
		public TestingTimestamp: Date = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`CompanyLineFilter`, `company_lineFilter`, CrudableTableDefinitionGender.Male, `Company Line`, `Company Lines`, true, true, true, true, true, true);

		this.SetColumnDefinition(CompanyLineColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyLineColumns.CompanyId, `company_id`, `CompanyId`, `Company Id`, `Company Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyLineColumns.Ddd, `ddd`, `Ddd`, `Ddd`, `Ddds`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyLineColumns.Phone, `phone`, `Phone`, `Phone`, `Phones`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyLineColumns.Mobile, `mobile`, `Mobile`, `Mobile`, `Mobiles`, ComponentTypeEnum.Number, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyLineColumns.OperatorId, `operator_id`, `OperatorId`, `Operator Id`, `Operator Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyLineColumns.OperatorSince, `operator_since`, `OperatorSince`, `Operator Since`, `Operator Sinces`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, true);
		this.SetColumnDefinition(CompanyLineColumns.TestingTimestamp, `testing_timestamp`, `TestingTimestamp`, `Testing Timestamp`, `Testing Timestamps`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, true);
	}

	public Clone(): CompanyLineFilter
	{
		const clone: CompanyLineFilter = new CompanyLineFilter();
		clone.Id = this.Id;
		clone.Ddd = this.Ddd;
		clone.Phone = this.Phone;
		clone.Mobile = this.Mobile;
		clone.CompanyId = this.CompanyId;
		clone.OperatorId = this.OperatorId;
		clone.OperatorSince = this.OperatorSince;
		clone.TestingTimestamp = this.TestingTimestamp;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Ddd))
			json['Ddd'] = this.Ddd;
		if (!ObjectUtils.NullOrUndefined(this.Phone))
			json['Phone'] = this.Phone;
		if (!ObjectUtils.NullOrUndefined(this.Mobile))
			json['Mobile'] = this.Mobile;
		if (!ObjectUtils.NullOrUndefined(this.OperatorSince))
			json['OperatorSince'] = DateUtils.ToDateServer(this.OperatorSince);
		if (!ObjectUtils.NullOrUndefined(this.TestingTimestamp))
			json['TestingTimestamp'] = DateUtils.ToDateServer(this.TestingTimestamp);
		if (!ObjectUtils.NullOrUndefined(this.CompanyId))
			json['CompanyId'] = Array.isArray(this.CompanyId) ? this.CompanyId : [this.CompanyId];
		if (!ObjectUtils.NullOrUndefined(this.OperatorId))
			json['OperatorId'] = Array.isArray(this.OperatorId) ? this.OperatorId : [this.OperatorId];


		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Ddd = json['Ddd'];
			this.Phone = json['Phone'];
			this.Mobile = json['Mobile'];
			this.CompanyId = json['CompanyId'];
			this.OperatorId = json['OperatorId'];
			this.OperatorSince = DateUtils.FromDateServer(json['OperatorSince']);
			this.TestingTimestamp = DateUtils.FromDateServer(json['TestingTimestamp']);
		}
	}
}