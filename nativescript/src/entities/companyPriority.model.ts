﻿import { Crudable } from '@src/external/plugin-utils/business/models/crud/crudable.model';

// utils
import { DateUtils } from '@src/external/plugin-utils/utils/date-utils';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// Navigation Properties
import { Company } from './company.model';

// components helpers
import { ComponentTypeEnum } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-definition.model';

export const CompanyPriorityClass: string = `CompanyPriority`;
export const CompanyPriorityTable: string = `company_priority`;

/**
 * Column definitions for the `CompanyPriority` class
 */
export enum CompanyPriorityColumns
{
	Id = 'Id',
	Priority = 'Priority',
	CompanyId = 'CompanyId',
	Timestamp = 'Timestamp',

	CompanyIdParent = 'CompanyIdParent',
}
export const CompanyPriorityColumnsFilter: string[] = [];
export const CompanyPriorityColumnsInsert: string[] = [CompanyPriorityColumns.CompanyId, CompanyPriorityColumns.Priority, CompanyPriorityColumns.Timestamp];
export const CompanyPriorityColumnsUpdate: string[] = [CompanyPriorityColumns.CompanyId, CompanyPriorityColumns.Priority, CompanyPriorityColumns.Timestamp];

/**
 * Implementations of the `CompanyPriority` class
 */
export class CompanyPriority extends Crudable
{
	public CompanyIdParent: Company;

	public static FromJson(json: any): CompanyPriority
	{
		const item: CompanyPriority = new CompanyPriority();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public CompanyId: number = null,
		public Priority: number = null,
		public Timestamp: Date = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`CompanyPriority`, `company_priority`, CrudableTableDefinitionGender.Male, `Company Priority`, `Company Prioritys`, true, true, true, true, true, true);

		this.SetColumnDefinition(CompanyPriorityColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyPriorityColumns.CompanyId, `company_id`, `CompanyId`, `Company Id`, `Company Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyPriorityColumns.Priority, `priority`, `Priority`, `Priority`, `Prioritys`, ComponentTypeEnum.Number, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyPriorityColumns.Timestamp, `timestamp`, `Timestamp`, `Timestamp`, `Timestamps`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
	}

	public Clone(): CompanyPriority
	{
		const clone: CompanyPriority = new CompanyPriority();
		clone.Id = this.Id;
		clone.Priority = this.Priority;
		clone.CompanyId = this.CompanyId;
		clone.Timestamp = this.Timestamp;

		clone.CompanyIdParent = this.CompanyIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.CompanyIdParent == null ? '' : this.CompanyIdParent.ToString()}`;
	}

	public ToJson(navigationProperties: boolean = false): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Priority))
			json['Priority'] = this.Priority;
		if (!ObjectUtils.NullOrUndefined(this.Timestamp))
			json['Timestamp'] = DateUtils.ToDateServer(this.Timestamp);
		if (navigationProperties)
		{
			if (!ObjectUtils.NullOrUndefined(this.CompanyIdParent))
				json['CompanyIdParent'] = this.CompanyIdParent.ToJson();
		}

		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: any): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Priority = json['Priority'];
			this.CompanyId = json['CompanyId'];
			this.Timestamp = DateUtils.FromDateServer(json['Timestamp']);

			if (!ObjectUtils.NullOrUndefined(json['CompanyIdParent']))
			{
				this.CompanyIdParent = new Company();
				this.CompanyIdParent.ParseFromJson(json['CompanyIdParent']);
			}
		}
	}
}

/**
 * Implementations of the `CompanyPriorityFilter` class
 */
export class CompanyPriorityFilter extends Crudable
{
	public static FromJson(json: Object): CompanyPriorityFilter
	{
		const item: CompanyPriorityFilter = new CompanyPriorityFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public CompanyId: number[] = null,
		public Priority: number = null,
		public Timestamp: Date = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`CompanyPriorityFilter`, `company_priorityFilter`, CrudableTableDefinitionGender.Male, `Company Priority`, `Company Prioritys`, true, true, true, true, true, true);

		this.SetColumnDefinition(CompanyPriorityColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyPriorityColumns.CompanyId, `company_id`, `CompanyId`, `Company Id`, `Company Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyPriorityColumns.Priority, `priority`, `Priority`, `Priority`, `Prioritys`, ComponentTypeEnum.Number, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyPriorityColumns.Timestamp, `timestamp`, `Timestamp`, `Timestamp`, `Timestamps`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
	}

	public Clone(): CompanyPriorityFilter
	{
		const clone: CompanyPriorityFilter = new CompanyPriorityFilter();
		clone.Id = this.Id;
		clone.Priority = this.Priority;
		clone.CompanyId = this.CompanyId;
		clone.Timestamp = this.Timestamp;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Priority))
			json['Priority'] = this.Priority;
		if (!ObjectUtils.NullOrUndefined(this.Timestamp))
			json['Timestamp'] = DateUtils.ToDateServer(this.Timestamp);
		if (!ObjectUtils.NullOrUndefined(this.CompanyId))
			json['CompanyId'] = Array.isArray(this.CompanyId) ? this.CompanyId : [this.CompanyId];


		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Priority = json['Priority'];
			this.CompanyId = json['CompanyId'];
			this.Timestamp = DateUtils.FromDateServer(json['Timestamp']);
		}
	}
}