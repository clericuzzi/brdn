﻿import { Crudable } from '@src/external/plugin-utils/business/models/crud/crudable.model';

// utils
import { DateUtils } from '@src/external/plugin-utils/utils/date-utils';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// Navigation Properties
import { Company } from './company.model';

// components helpers
import { ComponentTypeEnum } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-definition.model';

export const CompanyCheckRequestClass: string = `CompanyCheckRequest`;
export const CompanyCheckRequestTable: string = `company_check_request`;

/**
 * Column definitions for the `CompanyCheckRequest` class
 */
export enum CompanyCheckRequestColumns
{
	Id = 'Id',
	CheckType = 'CheckType',
	CompanyId = 'CompanyId',

	CompanyIdParent = 'CompanyIdParent',
}
export const CompanyCheckRequestColumnsFilter: string[] = [];
export const CompanyCheckRequestColumnsInsert: string[] = [CompanyCheckRequestColumns.CheckType, CompanyCheckRequestColumns.CompanyId];
export const CompanyCheckRequestColumnsUpdate: string[] = [CompanyCheckRequestColumns.CheckType, CompanyCheckRequestColumns.CompanyId];

/**
 * Implementations of the `CompanyCheckRequest` class
 */
export class CompanyCheckRequest extends Crudable
{
	public CompanyIdParent: Company;

	public static FromJson(json: any): CompanyCheckRequest
	{
		const item: CompanyCheckRequest = new CompanyCheckRequest();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public CheckType: number = null,
		public CompanyId: number = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`CompanyCheckRequest`, `company_check_request`, CrudableTableDefinitionGender.Male, `Company Check Request`, `Company Check Requests`, true, true, true, true, true, true);

		this.SetColumnDefinition(CompanyCheckRequestColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyCheckRequestColumns.CheckType, `check_type`, `CheckType`, `Check Type`, `Check Types`, ComponentTypeEnum.Number, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyCheckRequestColumns.CompanyId, `company_id`, `CompanyId`, `Company Id`, `Company Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): CompanyCheckRequest
	{
		const clone: CompanyCheckRequest = new CompanyCheckRequest();
		clone.Id = this.Id;
		clone.CheckType = this.CheckType;
		clone.CompanyId = this.CompanyId;

		clone.CompanyIdParent = this.CompanyIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.CheckType}`;
	}

	public ToJson(navigationProperties: boolean = false): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.CheckType))
			json['CheckType'] = this.CheckType;
		if (navigationProperties)
		{
			if (!ObjectUtils.NullOrUndefined(this.CompanyIdParent))
				json['CompanyIdParent'] = this.CompanyIdParent.ToJson();
		}

		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: any): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.CheckType = json['CheckType'];
			this.CompanyId = json['CompanyId'];

			if (!ObjectUtils.NullOrUndefined(json['CompanyIdParent']))
			{
				this.CompanyIdParent = new Company();
				this.CompanyIdParent.ParseFromJson(json['CompanyIdParent']);
			}
		}
	}
}

/**
 * Implementations of the `CompanyCheckRequestFilter` class
 */
export class CompanyCheckRequestFilter extends Crudable
{
	public static FromJson(json: Object): CompanyCheckRequestFilter
	{
		const item: CompanyCheckRequestFilter = new CompanyCheckRequestFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public CheckType: number = null,
		public CompanyId: number[] = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`CompanyCheckRequestFilter`, `company_check_requestFilter`, CrudableTableDefinitionGender.Male, `Company Check Request`, `Company Check Requests`, true, true, true, true, true, true);

		this.SetColumnDefinition(CompanyCheckRequestColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyCheckRequestColumns.CheckType, `check_type`, `CheckType`, `Check Type`, `Check Types`, ComponentTypeEnum.Number, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CompanyCheckRequestColumns.CompanyId, `company_id`, `CompanyId`, `Company Id`, `Company Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): CompanyCheckRequestFilter
	{
		const clone: CompanyCheckRequestFilter = new CompanyCheckRequestFilter();
		clone.Id = this.Id;
		clone.CheckType = this.CheckType;
		clone.CompanyId = this.CompanyId;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.CheckType))
			json['CheckType'] = this.CheckType;
		if (!ObjectUtils.NullOrUndefined(this.CompanyId))
			json['CompanyId'] = Array.isArray(this.CompanyId) ? this.CompanyId : [this.CompanyId];


		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.CheckType = json['CheckType'];
			this.CompanyId = json['CompanyId'];
		}
	}
}