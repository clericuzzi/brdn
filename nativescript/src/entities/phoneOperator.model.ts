﻿import { Crudable } from '@src/external/plugin-utils/business/models/crud/crudable.model';

// utils
import { DateUtils } from '@src/external/plugin-utils/utils/date-utils';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// components helpers
import { ComponentTypeEnum } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-definition.model';

export const PhoneOperatorClass: string = `PhoneOperator`;
export const PhoneOperatorTable: string = `phone_operator`;

/**
 * Column definitions for the `PhoneOperator` class
 */
export enum PhoneOperatorColumns
{
	Id = 'Id',
	Name = 'Name',
}
export const PhoneOperatorColumnsFilter: string[] = [];
export const PhoneOperatorColumnsInsert: string[] = [PhoneOperatorColumns.Name];
export const PhoneOperatorColumnsUpdate: string[] = [PhoneOperatorColumns.Name];

/**
 * Implementations of the `PhoneOperator` class
 */
export class PhoneOperator extends Crudable
{
	public static FromJson(json: any): PhoneOperator
	{
		const item: PhoneOperator = new PhoneOperator();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public Name: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`PhoneOperator`, `phone_operator`, CrudableTableDefinitionGender.Male, `Phone Operator`, `Phone Operators`, true, true, true, true, true, true);

		this.SetColumnDefinition(PhoneOperatorColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(PhoneOperatorColumns.Name, `name`, `Name`, `Name`, `Names`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
	}

	public Clone(): PhoneOperator
	{
		const clone: PhoneOperator = new PhoneOperator();
		clone.Id = this.Id;
		clone.Name = this.Name;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.Name}`;
	}

	public ToJson(navigationProperties: boolean = false): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json['Name'] = this.Name;
		if (navigationProperties)
		{
		}

		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: any): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Name = json['Name'];
		}
	}
}

/**
 * Implementations of the `PhoneOperatorFilter` class
 */
export class PhoneOperatorFilter extends Crudable
{
	public static FromJson(json: Object): PhoneOperatorFilter
	{
		const item: PhoneOperatorFilter = new PhoneOperatorFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public Name: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`PhoneOperatorFilter`, `phone_operatorFilter`, CrudableTableDefinitionGender.Male, `Phone Operator`, `Phone Operators`, true, true, true, true, true, true);

		this.SetColumnDefinition(PhoneOperatorColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(PhoneOperatorColumns.Name, `name`, `Name`, `Name`, `Names`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
	}

	public Clone(): PhoneOperatorFilter
	{
		const clone: PhoneOperatorFilter = new PhoneOperatorFilter();
		clone.Id = this.Id;
		clone.Name = this.Name;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json['Name'] = this.Name;


		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Name = json['Name'];
		}
	}
}