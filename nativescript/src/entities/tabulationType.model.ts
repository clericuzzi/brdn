﻿import { Crudable } from '@src/external/plugin-utils/business/models/crud/crudable.model';

// utils
import { DateUtils } from '@src/external/plugin-utils/utils/date-utils';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// components helpers
import { ComponentTypeEnum } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-definition.model';

export const TabulationTypeClass: string = `TabulationType`;
export const TabulationTypeTable: string = `tabulation_type`;

/**
 * Column definitions for the `TabulationType` class
 */
export enum TabulationTypeColumns
{
	Id = 'Id',
	Type = 'Type',
}
export const TabulationTypeColumnsFilter: string[] = [];
export const TabulationTypeColumnsInsert: string[] = [TabulationTypeColumns.Type];
export const TabulationTypeColumnsUpdate: string[] = [TabulationTypeColumns.Type];

/**
 * Implementations of the `TabulationType` class
 */
export class TabulationType extends Crudable
{
	public static FromJson(json: any): TabulationType
	{
		const item: TabulationType = new TabulationType();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public Type: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`TabulationType`, `tabulation_type`, CrudableTableDefinitionGender.Male, `Tabulation Type`, `Tabulation Types`, true, true, true, true, true, true);

		this.SetColumnDefinition(TabulationTypeColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(TabulationTypeColumns.Type, `type`, `Type`, `Type`, `Types`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): TabulationType
	{
		const clone: TabulationType = new TabulationType();
		clone.Id = this.Id;
		clone.Type = this.Type;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.Type}`;
	}

	public ToJson(navigationProperties: boolean = false): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Type))
			json['Type'] = this.Type;
		if (navigationProperties)
		{
		}

		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: any): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Type = json['Type'];
		}
	}
}

/**
 * Implementations of the `TabulationTypeFilter` class
 */
export class TabulationTypeFilter extends Crudable
{
	public static FromJson(json: Object): TabulationTypeFilter
	{
		const item: TabulationTypeFilter = new TabulationTypeFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public Type: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`TabulationTypeFilter`, `tabulation_typeFilter`, CrudableTableDefinitionGender.Male, `Tabulation Type`, `Tabulation Types`, true, true, true, true, true, true);

		this.SetColumnDefinition(TabulationTypeColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(TabulationTypeColumns.Type, `type`, `Type`, `Type`, `Types`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): TabulationTypeFilter
	{
		const clone: TabulationTypeFilter = new TabulationTypeFilter();
		clone.Id = this.Id;
		clone.Type = this.Type;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Type))
			json['Type'] = this.Type;


		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Type = json['Type'];
		}
	}
}