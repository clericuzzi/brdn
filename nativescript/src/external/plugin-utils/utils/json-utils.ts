import { Crudable } from '../business/models/crud/crudable.model';

const _ = require('lodash');

export class JsonUtils
{
    public static Stringify(obj: any): any
    {
        if (typeof obj === 'string' || obj instanceof String)
            return JSON.stringify(obj);
        else if (typeof obj === 'number' || obj instanceof Number)
            return JSON.stringify(obj);
        else if (Array.isArray(obj))
            return JSON.stringify(obj);
        else
        {
            let json: any = {};
            let hasProperties: boolean = false;
            for (const propName of obj)
            {
                if (!hasProperties)
                    hasProperties = true;

                if (obj[propName] != null && obj[propName] !== undefined)
                    json[propName] = obj[propName];
            }

            if (!hasProperties)
                json = obj;

            return JSON.stringify(json);
        }
    }

    public static Clone(obj: any): any
    {
        return _.extend({}, obj);
    }
    public static CloneDeep(obj: any): any
    {
        return _.extend(true, {}, obj);
    }

    /**
     * parses a dataset into an object ot type T
     * @param objectTemplate the object template
     * @param dataset the object to be parsed
     */
    public static ToInstanceOf<T>(objectTemplate: T, dataset: any): T
    {
        if (objectTemplate['ParseFromJson'] != null)
            objectTemplate['ParseFromJson'](dataset);
        else
        {
            for (const propName in dataset)
                objectTemplate[propName] = dataset[propName];
        }

        return objectTemplate;
    }
    /**
     * parses a list of objects into a list of items of type T
     * @param list a list of objects to be returned as a list of type T
     * @param baseObject the template that will be followed
     */
    public static ParseList<T>(list: any[], baseObject: T): T[]
    {
        const result: T[] = [];
        for (const item of list)
            result.push(this.ToInstanceOf<T>(JsonUtils.Clone(baseObject), item));

        return result;
    }
    /**
     * parses a list of objects into a list of items of type T
     * @param list a list of objects to be returned as a list of type T
     * @param baseObject the template that will be followed
     */
    public static ParseCrudList<T extends Crudable>(list: any[], baseObject: T): T[]
    {
        const result: T[] = [];
        for (const item of list)
        {
            const clone: T = JsonUtils.Clone(baseObject);
            clone.ParseFromJson(item);
            result.push(clone);
        }

        return result;
    }
}