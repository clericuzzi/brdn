import { ObjectUtils } from './object-utils';

export interface IDictionary<T>
{
    [Key: string]: T;
}

export class Dictionary<T>
{
    private _Keys: string[] = [];
    private _Dictionary: IDictionary<T> = {};

    public get Keys(): string[]
    {
        return this._Keys;
    }

    public All(): T[]
    {
        const data: T[] = [];
        if (!ObjectUtils.NullOrUndefined(this._Keys) && this._Keys.length > 0)
            for (const key of this._Keys)
                data.push(this._Dictionary[key]);

        return data;
    }
    public Add(key: string, item: T): void
    {
        if (this._Keys.indexOf(key) < 0)
            this._Keys.push(key);

        this._Dictionary[key] = item;
    }
    public Get(key: string): T
    {
        return this._Dictionary[key];
    }
    public Remove(key: string): void
    {
        if (this._Keys.indexOf(key) >= 0)
        {
            delete (this._Dictionary[key]);
            this._Keys.splice(this._Keys.indexOf(key), 1);
        }
    }
}