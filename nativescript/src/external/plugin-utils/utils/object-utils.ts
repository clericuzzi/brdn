import { JsonUtils } from './json-utils';

/**
 * This class manages null or undefined comparisos as well as "LINQ" utilities
 */
export class ObjectUtils
{
    /** Logs a large object to the console */
    public static Log(obj: any): void
    {
        const maxConsoleStringLength = 900; // The actual max length isn't clear.
        const value = JSON.stringify(obj);
        const length = value.length;

        if (length < maxConsoleStringLength)
            console.log(value);
        else
        {
            console.log(value.substring(0, maxConsoleStringLength));
            this.Log(value.substring(maxConsoleStringLength));
        }
    }

    /**
     * Evaluates a list of objects and returns TRUE if any of them are null OR undefined
     * @param objs the list of objects to be evaluated
     */
    public static NullOrUndefined(...objs: any[]): boolean
    {
        try
        {
            for (const obj of objs)
                if (obj === null || obj === undefined)
                    return true;

            return false;
        }
        catch (e)
        {
            return true;
        }
    }
    /**
     * Evaluates a list of string and returns true if ALL OF THEM are null, undefined or zero length
     * @param objs the list of objects to be evaluated
     */
    public static NullUndefinedOrEmpty(...objs: string[]): boolean
    {
        try
        {
            for (const obj of objs)
                if (obj === null || obj === undefined || obj.length === 0)
                    return true;

            return false;
        }
        catch (e)
        {
            return true;
        }
    }
    /**
     * Evaluates an array and returns TRUE if it is null, undefined or has no elements
     * @param array the array to be avaluated
     */
    public static NullUndefinedOrEmptyArray(array: any[]): boolean
    {
        try
        {
            if (ObjectUtils.NullOrUndefined(array))
                return true;
            else
                return array.length === 0;
        }
        catch (e)
        {
            return true;
        }
    }

    /**
     * Returns the given value if valid
     * Otherwise returns the given default value (if any)
     * @param value the value to be evaluated, if it cannot be acessed, the default value will be returned
     */
    public static GetValue<T>(obj: any, defaultValue: T = null, ...properties: string[]): T
    {
        try
        {
            if (properties[0] === `File`)
                console.log(obj);

            if (properties.length === 1 && properties[0].indexOf(`.`) > 0)
                properties = properties[0].split(`.`);

            if (ObjectUtils.NullOrUndefined(obj))
                return defaultValue as T;
            else
            {
                if (properties.length === 0)
                    return obj;
                else
                {
                    const property: string = properties.shift();
                    return ObjectUtils.GetValue(obj[property], defaultValue, ...properties);
                }
            }
        }
        catch (e)
        {
            if (ObjectUtils.NullOrUndefined(defaultValue))
                return null;
            else
                return defaultValue;
        }
    }

    public static CreateObjectArray<T>(...values: T[]): T[]
    {
        const array: T[] = [];
        for (const value of values)
            array.push(value);

        return array;
    }

    public static Assign<T>(baseObject: T, item: any): T
    {
        const properties: string[] = Object.keys(item);
        const newObject: T = JsonUtils.Clone(baseObject);
        for (const property of properties)
            newObject[this.ColumnToPropertyName(property)] = item[property];

        return newObject;
    }
    public static ColumnToPropertyName(columnName: string): string
    {
        let propertyName: string = columnName.substr(0, 1).toUpperCase() + columnName.substr(1);
        while (propertyName.indexOf(`_`) >= 0)
        {
            const index: number = propertyName.indexOf(`_`);
            let trailing = propertyName.substr(index + 1);
            trailing = trailing.substr(0, 1).toUpperCase() + trailing.substr(1);

            propertyName = propertyName.substr(0, index) + trailing;
        }

        return propertyName;
    }
    public static PropertyNameToColumn(columnName: string): string
    {
        let propertyName: string = columnName.substr(0, 1).toUpperCase() + columnName.substr(1);
        while (propertyName.indexOf(`_`) >= 0)
        {
            const index: number = propertyName.indexOf(`_`);
            let trailing = propertyName.substr(index + 1);
            trailing = trailing.substr(0, 1).toUpperCase() + trailing.substr(1);

            propertyName = propertyName.substr(0, index) + trailing;
        }

        return propertyName;
    }
}