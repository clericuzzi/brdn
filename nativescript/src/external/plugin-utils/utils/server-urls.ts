﻿export class ServerUrls
{
    public static SERVER_ADDRESS: string;

    // Server root endpoint
    public static get LOCAL_ROOT(): string { return `http://localhost:58111/api/`; }
    public static get SERVER_ROOT(): string { return ServerUrls.SERVER_ADDRESS; }

    public static GetUrl(url: string): string { return `${this.SERVER_ROOT}${url}`; }
    public static GetLocalUrl(url: string): string { return `${this.LOCAL_ROOT}${url}`; }
}