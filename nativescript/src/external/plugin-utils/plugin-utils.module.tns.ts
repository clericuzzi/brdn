import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';

// tns modules
import { NativeScriptFormsModule } from 'nativescript-angular/forms';
import { NativeScriptHttpClientModule } from 'nativescript-angular/http-client';

// services

@NgModule({
  declarations: [
  ],
  imports: [
    NativeScriptFormsModule,
    NativeScriptHttpClientModule,
  ],
  exports: [
    NativeScriptFormsModule,
    NativeScriptHttpClientModule,
  ],
  providers: [
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class PluginUtilsModule { }