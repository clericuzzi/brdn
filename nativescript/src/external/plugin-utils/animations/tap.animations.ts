import { View } from 'tns-core-modules/ui/core/view';
import { ObjectUtils } from '../utils/object-utils';
import { AnimationCurve } from 'tns-core-modules/ui/enums';

export class TapAnimations
{
  private static _Busy: boolean = false;
  public static SimpleTap(view: View, onComplete: () => void = null, animated: boolean = true, scale: number = 0.95): void
  {
    if (ObjectUtils.NullOrUndefined(view) || this._Busy)
      return;

    if (animated)
    {
      const delay: number = 100;
      const duration: number = 75;
      if (!ObjectUtils.NullOrUndefined(onComplete))
        view.animate({ scale: { x: scale, y: scale }, duration: duration, curve: AnimationCurve.easeIn })
          .then(() => view.animate({ scale: { x: 1, y: 1 }, duration: duration * 5, delay: delay, curve: AnimationCurve.easeOut }))
          .then(() => onComplete());
      else
        view.animate({ scale: { x: scale, y: scale }, duration: duration, curve: AnimationCurve.easeIn })
          .then(() => view.animate({ scale: { x: 1, y: 1 }, duration: duration * 5, delay: delay, curve: AnimationCurve.easeOut }));
    }
    else
    {
      if (!ObjectUtils.NullOrUndefined(onComplete))
        onComplete();
    }

    this._Busy = true;
    setTimeout(() => this._Busy = false, 1000);
  }
}
