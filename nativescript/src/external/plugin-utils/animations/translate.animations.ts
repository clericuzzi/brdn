import { View } from 'tns-core-modules/ui/core/view';
import { ObjectUtils } from '../utils/object-utils';

export class TranslateAnimations
{
    public static TranslateTo(view: View, x: number, y: number, curve: string, duration: number = 250, onComplete: () => void = null, animated: boolean = true): void
    {
        if (ObjectUtils.NullOrUndefined(view))
            return;

        if (animated)
        {
            if (!ObjectUtils.NullOrUndefined(onComplete))
                view.animate({ translate: { x: x, y: y }, duration: duration, curve: curve })
                    .then(() => onComplete());
            else
                view.animate({ translate: { x: x, y: y }, duration: duration, curve: curve });
        }
        else
        {
            if (!ObjectUtils.NullOrUndefined(onComplete))
                onComplete();
        }
    }
}
