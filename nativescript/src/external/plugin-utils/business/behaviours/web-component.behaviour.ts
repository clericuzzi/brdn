import { Subject } from 'rxjs';
import { MatSnackBar } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { OnDestroy, HostBinding, ComponentFactoryResolver } from '@angular/core';
// models
import { SnackUtils } from '../../utils/snack-utils';
import { ObjectUtils } from '../../utils/object-utils';
import { BaseMessage } from '../models/response/base-message.model';
// managers
import { NamedSubscriptionManager } from '../models/subscriptions/named-subscription-manager.model';

export class WebComponent implements OnDestroy
{
    @HostBinding(`class`) ComponentClass: string = `ng-star-inserted full-size flex-column`;

    private _Unsubscribe: Subject<void> = new Subject<void>();
    private _SubscriptionManager: NamedSubscriptionManager = new NamedSubscriptionManager();

    public IsAlive: Boolean = true;

    public CurrentModel: any;

    public Busy: boolean;
    public Factory: ComponentFactoryResolver;
    public Sanitizer: DomSanitizer;

    public readonly ClassBinding: string = `ComponentClass`;
    ngOnDestroy()
    {
        this.IsAlive = false;
        this._Unsubscribe.next();
        this._Unsubscribe.complete();


        this.OnDestroy();
        this._SubscriptionManager.Clear();
    }
    public OnDestroy(): void
    {
    }
    public ListenTo<T>(name: string, subject: Subject<T>, next: (value: T) => void, error: () => void): void
    {
        this._SubscriptionManager.Subscribe(name, subject, this._Unsubscribe, next, error, null);
    }

    private _ValidateFactories(): void
    {
        if (ObjectUtils.NullOrUndefined(this.Factory))
            throw new Error(`A fábrica de componentes não foi inicializada...`);

        if (ObjectUtils.NullOrUndefined(this.Sanitizer))
            throw new Error(`O limitador do DOM não foi inicializado...`);
    }

    public InitFactories(factory: ComponentFactoryResolver, sanitizer: DomSanitizer): void
    {
        this.Factory = factory;
        this.Sanitizer = sanitizer;
    }
    public InitModel(model: any): void
    {
        this.CurrentModel = model;
    }

    public HandleResponse(response: BaseMessage, snackBar: MatSnackBar, errorMessage: string = null): boolean
    {
        let returnValue: boolean = false;
        if (ObjectUtils.NullOrUndefined(response) || ObjectUtils.NullOrUndefined(response.HasErrors))
            SnackUtils.OpenError(snackBar, ObjectUtils.NullUndefinedOrEmpty(errorMessage) ? `Falha no envio da requisição` : errorMessage);
        else
        {
            if (response.HasErrors)
                SnackUtils.OpenError(snackBar, ObjectUtils.NullUndefinedOrEmpty(errorMessage) ? `Falha no envio da requisição` : errorMessage);
            else
                returnValue = true;
        }

        return returnValue;
    }
}