import { Subject } from 'rxjs';
import { OnDestroy } from '@angular/core';

// nativescript
import { Page } from 'tns-core-modules/ui/page';
import * as application from 'tns-core-modules/application';

// utils
import { ObjectUtils } from '../../utils/object-utils';
import { RouterExtensions } from 'nativescript-angular/router';
import { PluginUtilsDialogManager } from '../managers/dialogs.manager';
import { NamedSubscriptionManager } from '../models/subscriptions/named-subscription-manager.model';

export enum TransitionNames
{
    CurlUp = 'curlUp',
    CurlDown = 'curlDown',
    Rxplode = 'explode',
    Fade = 'fade',
    FlipRight = 'flipRight',
    FlipLeft = 'flipLeft',
    SlideTop = 'slideTop',
    SlideLeft = 'slideLeft',
    SlideRight = 'slideRight',
    SlideBottom = 'slideBottom',
}

export abstract class NavBarPage implements OnDestroy
{
    private static _DefaultTransition: string = `slideLeft`;
    private static _DefaultTransitionCurve: string = `easeOut`;
    private static _DefaultTransitionDuration: number = 250;
    private static _PhysicalBackTimesFired: number = 0;

    private _Unsubscribe: Subject<void> = new Subject<void>();
    private _CanSuspend: boolean = false;

    public Busy: boolean = false;
    public Title: string = ``;
    public PageName: string = ``;

    public IsAlive: Boolean = true;
    public HasBack: Boolean = false;
    public HasTitle: Boolean = true;
    public HasActions: Boolean = false;
    public HasActionBar: boolean = true;

    /** for Android only, tells the abstract class if a subs class is overriding the default back button pressed behaviour */
    public OverridesPhysicalBackButton: boolean = false;

    private _SubscriptionManager: NamedSubscriptionManager = new NamedSubscriptionManager();

    constructor(
        public Page: Page,
        public Router: RouterExtensions,
        public HasNavigationEvents: boolean,
    )
    {
        if (!ObjectUtils.NullOrUndefined(this.Page))
            this.Page.actionBarHidden = true;

        if (this.HasNavigationEvents)
        {
            this.Page.on(`navigatedTo`, args => this._NavigationStarted());
            this.Page.on(`navigatingTo`, args => this._NavigationFinished());
        }
    }

    private _NavigationStarted(): void
    {
        application.android.off(application.AndroidApplication.activityBackPressedEvent);
        application.android.on(application.AndroidApplication.activityBackPressedEvent, (args: any) => this._PhysicalBack(args));

        this.NavigationIn();
    }
    private _NavigationFinished(): void
    {
        application.android.off(application.AndroidApplication.activityBackPressedEvent);

        this.NavigationOut();
    }

    public NavigationIn(): void { }
    public NavigationOut(): void { }

    ngOnDestroy()
    {
        this.IsAlive = false;
        this._Unsubscribe.next();
        this._Unsubscribe.complete();

        if (!ObjectUtils.NullOrUndefined(this.Page))
        {
            this.Page.off(`navigatedTo`);
            this.Page.off(`navigatingTo`);
        }

        this.OnDestroy();
        this._SubscriptionManager.Clear();
    }
    public OnDestroy(): void
    {
    }

    public ListenTo<T>(name: string, subject: Subject<T>, next: (value: T) => void, error: () => void): void
    {
        this._SubscriptionManager.Subscribe(name, subject, this._Unsubscribe, next, error, null);
    }

    public Back(): void
    {
        if (this.OverridesPhysicalBackButton)
            return;

        if (this.Router.canGoBack())
            this.Router.backToPreviousPage();
    }
    public NavigateTo(route: string, clearNavigationFlag: boolean = false): void
    {
        console.log(`NavigateTo -> `, route);
        this.Router.navigateByUrl(route, { clearHistory: clearNavigationFlag });
    }
    public TransitionTo(route: string, clearNavigationFlag: boolean = false, transitionName: string = null, transitionDuration: number = null, transitionCurve: string = null): void
    {
        const transition: any = {
            name: ObjectUtils.NullOrUndefined(transitionName) ? NavBarPage._DefaultTransition : transitionName,
            curve: ObjectUtils.NullOrUndefined(transitionCurve) ? NavBarPage._DefaultTransitionCurve : transitionCurve,
            duration: ObjectUtils.NullOrUndefined(transitionDuration) ? NavBarPage._DefaultTransitionDuration : transitionDuration,
        };

        console.log(`TransitionTo -> `, route, transition);
        this.Router.navigateByUrl(route, { transition: transition, clearHistory: clearNavigationFlag });
    }

    private _PhysicalBackEndSupress(): void
    {
        NavBarPage._PhysicalBackTimesFired = 0;
    }
    private _PhysicalBack(args: any): void
    {
        args.cancel = true;
        if (NavBarPage._PhysicalBackTimesFired > 0)
            return;

        try
        {
            if (this.OverridesPhysicalBackButton)
                this.Back();
            else if (this.Router.canGoBack())
                this.Back();
            else
            {
                if (this._CanSuspend)
                    application.android.foregroundActivity.moveTaskToBack(true);
                else
                    this._PhysicalForceSuspendAsk();
            }
        }
        catch (err)
        {
            console.log(err);
        }

        NavBarPage._PhysicalBackTimesFired++;
        setTimeout(() => this._PhysicalBackEndSupress(), 1000);
    }
    private _PhysicalForceSuspendAsk()
    {
        this._CanSuspend = true;
        PluginUtilsDialogManager.Toast(`Pressione novamente pra sair`);

        setTimeout(() => this._CanSuspend = false, 2500);
    }

    public ErrorMessageAndBack(error: string): void
    {
        PluginUtilsDialogManager.Alert(() => this.Back(), error);
    }
}
