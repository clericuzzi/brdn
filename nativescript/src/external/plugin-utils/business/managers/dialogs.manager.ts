import { alert, AlertOptions } from 'tns-core-modules/ui/dialogs/dialogs';
import { action, ActionOptions } from 'tns-core-modules/ui/dialogs/dialogs';
import { prompt, PromptOptions } from 'tns-core-modules/ui/dialogs/dialogs';
import { confirm, ConfirmOptions } from 'tns-core-modules/ui/dialogs/dialogs';

// utils
import { ObjectUtils } from '../../utils/object-utils';

const Toast = require('nativescript-toast');
export class PluginUtilsDialogManager
{
  public static Ok: string = `ok`;
  public static No: string = `não`;
  public static Yes: string = `sim`;
  public static Close: string = `fechar`;
  public static AlertTitle: string = `Aviso`;
  public static ConfirmationTitle: string = `Confirmação`;
  public static WarningOrErrorTitle: string = `Alerta`;

  public static Toast(text: string, long: boolean = false): void
  {
    if (long)
      Toast.makeText(text, `long`).show();
    else
      Toast.makeText(text).show();
  }

  public static Alert(callback: () => void, message: string, title?: string, closeButton?: string, cancelable: boolean = true): void
  {
    const options: AlertOptions = {
      title: ObjectUtils.NullUndefinedOrEmpty(title) ? this.AlertTitle : title,
      message: message,
      cancelable: cancelable,
      okButtonText: ObjectUtils.NullUndefinedOrEmpty(closeButton) ? this.Close : closeButton,
    };

    if (ObjectUtils.NullOrUndefined(callback))
      alert(options);
    else
      alert(options).then(result => callback());
  }
  public static Action(callback: (result: string) => void, message: string, title?: string, cancelButton?: string, cancelable: boolean = true, ...actions: string[]): void
  {
    const options: ActionOptions = {
      title: title,
      message: message,
      cancelable: cancelable,
      cancelButtonText: cancelButton,
      actions: actions,
    };

    if (ObjectUtils.NullOrUndefined(callback))
      action(options);
    else
      action(options).then(result => callback(result));
  }

  public static PromptText(callback: (result: boolean, text: string) => void, message: string, title?: string, okButton?: string, cancelButton?: string, neutralButton?: string, cancelable: boolean = true, defaultText?: string, capitalizationType?: string): void
  {
    PluginUtilsDialogManager._Prompt(callback, title, message, okButton, cancelButton, neutralButton, cancelable, `plain text`, defaultText, capitalizationType);
  }
  public static PromptEmail(callback: (result: boolean, text: string) => void, message: string, title?: string, okButton?: string, cancelButton?: string, neutralButton?: string, cancelable: boolean = true, defaultText?: string, capitalizationType?: string): void
  {
    PluginUtilsDialogManager._Prompt(callback, title, message, okButton, cancelButton, neutralButton, cancelable, `email`, defaultText, capitalizationType);
  }
  public static PromptPassword(callback: (result: boolean, text: string) => void, message: string, title?: string, okButton?: string, cancelButton?: string, neutralButton?: string, cancelable: boolean = true, defaultText?: string, capitalizationType?: string): void
  {
    PluginUtilsDialogManager._Prompt(callback, title, message, okButton, cancelButton, neutralButton, cancelable, `password`, defaultText, capitalizationType);
  }
  private static _Prompt(callback: (result: boolean, text: string) => void, message: string, title?: string, okButton?: string, cancelButton?: string, neutralButton?: string, cancelable: boolean = true, inputType?: string, defaultText?: string, capitalizationType?: string): void
  {
    const options: PromptOptions = {
      title: title,
      message: message,
      cancelable: cancelable,
      okButtonText: okButton,
      cancelButtonText: cancelButton,
      neutralButtonText: neutralButton,
      inputType: inputType,
      defaultText: defaultText,
      capitalizationType: capitalizationType,
    };

    if (ObjectUtils.NullOrUndefined(callback))
      prompt(options);
    else
      prompt(options).then(result => callback(result.result, result.text));
  }
  public static Confirm(callback: (result: boolean) => void, message: string, title?: string, okButton?: string, cancelButton?: string, neutralButton?: string, cancelable: boolean = true): void
  {
    const options: ConfirmOptions = {
      title: ObjectUtils.NullUndefinedOrEmpty(title) ? this.ConfirmationTitle : title,
      message: message,
      cancelable: cancelable,
      okButtonText: ObjectUtils.NullUndefinedOrEmpty(okButton) ? this.Yes : okButton,
      cancelButtonText: ObjectUtils.NullUndefinedOrEmpty(cancelButton) ? this.No : cancelButton,
      neutralButtonText: neutralButton,
    };

    if (ObjectUtils.NullOrUndefined(callback))
      confirm(options);
    else
      confirm(options).then(result => callback(result));
  }
}
