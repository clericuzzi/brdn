import { ObjectUtils } from '../../utils/object-utils';

const platformModule = require('tns-core-modules/platform');

export class PluginUtilsPlatformManager
{
    public static get Manufacturer(): string
    {
        try
        {
            if (ObjectUtils.NullOrUndefined(platformModule))
                return `platform not initialized`;
            else
                return platformModule.device.manufacturer;
        }
        catch (err)
        {
            return null;
        }
    }

    public static get DeviceXPixels(): number
    {
        try
        {
            if (ObjectUtils.NullOrUndefined(platformModule))
                return 0;
            else
                return platformModule.screen.mainScreen.widthPixels;
        }
        catch (err)
        {
            return -1;
        }
    }
    public static get DeviceYPixels(): number
    {
        try
        {
            if (ObjectUtils.NullOrUndefined(platformModule))
                return 0;
            else
                return platformModule.screen.mainScreen.heightPixels;
        }
        catch (err)
        {
            return -1;
        }
    }
    public static get DeviceXDpi(): number
    {
        try
        {
            if (ObjectUtils.NullOrUndefined(platformModule))
                return 0;
            else
                return platformModule.screen.mainScreen.widthDIPs;
        }
        catch (err)
        {
            return -1;
        }
    }
    public static get DeviceYDpi(): number
    {
        try
        {
            if (ObjectUtils.NullOrUndefined(platformModule))
                return 0;
            else
                return platformModule.screen.mainScreen.heightDIPs;
        }
        catch (err)
        {
            return -1;
        }
    }
}