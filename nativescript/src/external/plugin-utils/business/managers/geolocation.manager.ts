import * as geolocation from 'nativescript-geolocation';

import { Subject } from 'rxjs';
import { ObjectUtils } from '../../utils/object-utils';
import { Accuracy } from 'tns-core-modules/ui/enums/enums';

export class PluginUtilsGeolocationLocation
{
    constructor(
        public Latitude: number = 0,
        public Longitude: number = 0,
        public Altitude: number = 0,
        public Direction: number = 0,
        public Speed: number = 0,
    )
    {
    }
}

export class PluginUtilsGeolocationManager
{
    private static _IsEnabled: boolean = null;
    public static OnLocationRetrieved: Subject<PluginUtilsGeolocationLocation> = new Subject<PluginUtilsGeolocationLocation>();
    public static OnLocationNotRetrieved: Subject<void> = new Subject<void>();

    public static OnLocationEnabled: Subject<void> = new Subject<void>();
    public static OnLocationNotEnabled: Subject<void> = new Subject<void>();

    public static CurrentLocation: Location;
    public static Enable(): void
    {
        geolocation.isEnabled().then(enabled => this._IsLocationEnabled(enabled), error => this.OnLocationNotEnabled.next());
    }
    public static isEnabled(): boolean
    {
        if (ObjectUtils.NullOrUndefined(this._IsEnabled))
            return false;
        else
            return this._IsEnabled;
    }
    private static _IsLocationEnabled(isEnabled: boolean): void
    {
        if (!isEnabled)
            geolocation.enableLocationRequest().then(() => { this._IsEnabled = true; this.OnLocationNotEnabled.next(); }, error => { this._IsEnabled = false; this.OnLocationNotEnabled.next(); });
        else
        {
            this._IsEnabled = true;
            this.OnLocationEnabled.next();
        }
    }

    public static GetLocation(): void
    {
        geolocation.getCurrentLocation({ desiredAccuracy: Accuracy.high, maximumAge: 5000 })
            .then(location => this._LocationGotten(location), err => this.OnLocationNotRetrieved.next());
    }
    private static _LocationGotten(location: geolocation.Location): void
    {
        this.OnLocationRetrieved.next(new PluginUtilsGeolocationLocation(location.latitude, location.longitude, location.altitude, location.direction, location.speed));
    }
}