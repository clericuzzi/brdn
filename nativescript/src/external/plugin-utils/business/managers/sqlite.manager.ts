import { ObjectUtils } from '../../utils/object-utils';
import { PluginUtilsStorageManager } from './storage.manager';

const sqlite = require('nativescript-sqlite');
const databasePath: string = `db.sqlite`;
const TEMP_ID_KEY: string = `temp-id`;

export class PluginUtilsSqliteManager
{
    private static _Database;
    private static _CurrentLocalId: number;
    private static _DatabasePromise: Promise<any>;
    private static _InitDatabase(db: any): void
    {
        this._Database = db;
        this._Database.resultType(sqlite.RESULTSASOBJECT);
    }
    public static LoadDatabase(): void
    {
        if (!sqlite.exists(databasePath))
            sqlite.copyDatabase(databasePath);

        this._DatabasePromise = new sqlite(databasePath);
        this._DatabasePromise.then(db => this._InitDatabase(db), error => console.log(`DATABASE START ERROR: `, error));
    }

    public static LocalTempId(): number
    {
        this._CurrentLocalId = PluginUtilsStorageManager.GetNumber(TEMP_ID_KEY, -1);
        PluginUtilsStorageManager.SetNumber(TEMP_ID_KEY, this._CurrentLocalId - 1);

        return this._CurrentLocalId;
    }

    public static All<T>(table: string, template: T, success: (response: T[]) => void, error: (err: any) => void): void
    {
        if (ObjectUtils.NullUndefinedOrEmpty(table))
            throw new Error(`please define a table`);

        if (ObjectUtils.NullOrUndefined(template))
            throw new Error(`please define a template`);

        table = `select * from ${table}`;
        this._Database.all(table).then(rows => success(this._FetchResult(template, rows)), err => error(err));
    }
    public static Get<T>(query: string, params: any[], template: T, success: (response: any) => void, error: (err: any) => void): void
    {
        if (ObjectUtils.NullUndefinedOrEmpty(query))
            throw new Error(`please define a query`);

        this._Database.get(query, params).then(result => success(this._FetchResult(template, result)), err => error(err));
    }
    public static GetById<T>(table: string, template: T, id: number, success: (response: T) => void, error: (err: any) => void, idField: string = `id`): void
    {
        if (ObjectUtils.NullUndefinedOrEmpty(table))
            throw new Error(`please define a table`);

        if (ObjectUtils.NullOrUndefined(template))
            throw new Error(`please define a template`);

        if (ObjectUtils.NullOrUndefined(id))
            throw new Error(`please define an id`);

        const query: string = `select * from ${table} where id = ?`;
        this._Database.get(query, [id]).then(rows => success(this._FetchFirstResult(template, rows)), err => error(err));
    }
    public static GetSingle<T>(query: string, params: any[], template: T, success: (response: any) => void, error: (err: any) => void): void
    {
        if (ObjectUtils.NullUndefinedOrEmpty(query))
            throw new Error(`please define a query`);

        this._Database.get(query, params).then(result => success(this._FetchFirstResult(template, result)), err => error(err));
    }

    private static _FetchResult<T>(baseObject: T, rows: any): T[]
    {
        const returnValues: T[] = [];
        for (const row of rows)
            returnValues.push(ObjectUtils.Assign(baseObject, row));

        return returnValues;
    }
    private static _FetchFirstResult<T>(baseObject: T, rows: any): T
    {
        if (ObjectUtils.NullOrUndefined(rows))
            return null;

        const returnValues: T[] = [];
        if (Array.isArray(rows))
        {
            for (const row of rows)
                returnValues.push(ObjectUtils.Assign(baseObject, row));
        }
        else
            returnValues.push(ObjectUtils.Assign(baseObject, rows));

        if (ObjectUtils.NullUndefinedOrEmptyArray(returnValues))
            return null;
        else
            return returnValues[0];
    }

    public static Insert<T>(tableName: string, params: T[], success: (response: number[]) => void = null, error: (err: any) => void = null): void
    {
        if (ObjectUtils.NullUndefinedOrEmpty(tableName))
            throw new Error(`please define a table`);

        if (!ObjectUtils.NullUndefinedOrEmptyArray(params))
        {
            const values: any[] = [];
            const columns: string[] = this._InsertPropertiesToColumns(Object.keys(params[0]));

            let query: string = `insert into ${tableName} (` + columns + `) values `;
            for (const item of params)
            {
                const itemValues: any = this._InsertValues(columns, item);
                query = query + this._InsertQuery(columns, item);
                values.push(...itemValues);
            }

            query = query.substr(0, query.length - 1);
            if (ObjectUtils.NullOrUndefined(success))
                this._Database.execSQL(query, values);
            else
                this._Database.execSQL(query, values).then(ids => success(ids), err => error(err));
        }
    }
    private static _InsertQuery<T>(columns: string[], item: T): string
    {
        let query: string = ``;
        for (const column of columns)
            query = query + `?,`;

        query = `(` + query.substr(0, query.length - 1) + `),`;

        return query;
    }
    private static _InsertValues<T>(columns: string[], item: T): any[]
    {
        const values: any[] = [];
        for (const column of columns)
            values.push(item[ObjectUtils.ColumnToPropertyName(column)]);

        return values;
    }
    private static _InsertPropertiesToColumns(keys: string[]): string[]
    {
        const forbiddenColumns: string[] = [`PkProperty`, `CustomProperties`, `ColumnDefinitions`, `TableDefinition`];
        const columns: string[] = [];
        keys = keys.filter(i => !forbiddenColumns.find(j => j === i));
        for (const key of keys)
        {
            const chars: string[] = key.split(``);
            let column: string = ``;
            if (key.indexOf(`Parent`) < 0)
            {
                for (const char of chars)
                {
                    if (char !== char.toLocaleLowerCase() && column.length > 0)
                        column = column + `_`;

                    column = column + char.toLocaleLowerCase();
                }

                columns.push(column);
            }
        }

        return columns;
    }

    public static Update<T>(tableName: string, params: T[], success: (response: number[]) => void = null, error: (err: any) => void = null, idField: string = `Id`): void
    {
        const ids: number[] = params.map(i => i[idField]);
        const query: string = `delete from ${tableName} where ${idField} in (?)`;

        if (ObjectUtils.NullOrUndefined(success))
            this._Database.execSQL(query, ids);
        else
            this._Database.execSQL(query, ids).then(result => this.Insert(tableName, params, success, error), error => console.log(`UPDATE ERROR: `, error));
    }

    public static ClearTable(tableName: string, success: (response: any) => void = null, error: (err: any) => void = null): void
    {
        const query: string = `delete from ${tableName}`;
        if (ObjectUtils.NullOrUndefined(success))
            this._Database.execSQL(query);
        else
            this._Database.execSQL(query).then(result => success(result), err => error(err));
    }
    public static ResetTable(tableName: string, params: any[], success: (response: any) => void = null, error: (err: any) => void = null): void
    {
        const query: string = `delete from ${tableName}`;
        if (ObjectUtils.NullOrUndefined(success))
            this._Database.execSQL(query).then(result => this.Insert(tableName, params));
        else
            this._Database.execSQL(query).then(result => this.Insert(tableName, params, success, error));
    }
}