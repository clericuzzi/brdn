import { Subject } from 'rxjs';
import { connectionType, getConnectionType, startMonitoring, stopMonitoring } from 'tns-core-modules/connectivity';

export enum ConnectivityType
{
    None = 'none',
    WiFi = 'wifi',
    Mobile = 'mobile',
    Ethernet = 'ethernet',
    Bluetooth = 'bluetooth',
}

export class PluginUtilsConnectivityManager
{
    private static _CurrentConnectivity: string;
    public static Online: boolean = true;
    private static _LastOnlineState: boolean = false;

    public static OnWentOnline: Subject<void> = new Subject<void>();
    public static OnWentOffline: Subject<void> = new Subject<void>();
    public static OnConnectivityChanged: Subject<ConnectivityType> = new Subject<ConnectivityType>();

    private static _MonitoringEvent(connectivity: number): void
    {
        this.Online = true;
        switch (connectivity)
        {
            case connectionType.none:
                this.Online = false;
                this.OnConnectivityChanged.next(ConnectivityType.None);
                break;
            case connectionType.wifi:
                this.OnConnectivityChanged.next(ConnectivityType.WiFi);
                break;
            case connectionType.mobile:
                this.OnConnectivityChanged.next(ConnectivityType.Mobile);
                break;
            case connectionType.ethernet:
                this.OnConnectivityChanged.next(ConnectivityType.Ethernet);
                break;
            case connectionType.bluetooth:
                this.OnConnectivityChanged.next(ConnectivityType.Bluetooth);
                break;
        }

        if (this.Online && !this._LastOnlineState)
            this.OnWentOnline.next();
        if (this._LastOnlineState && !this.Online)
            this.OnWentOffline.next();

        this._LastOnlineState = this.Online;
    }

    public static StopMonitoring(): void
    {
        stopMonitoring();
    }
    public static StartMonitoring(): void
    {
        startMonitoring(this._MonitoringEvent.bind(this));
    }
    public static GetConnectionType(): string
    {
        const currentConnection = getConnectionType();
        switch (currentConnection)
        {
            case connectionType.none:
                if (this._CurrentConnectivity !== ConnectivityType.None)
                    this.OnConnectivityChanged.next(ConnectivityType.None);

                this._CurrentConnectivity = ConnectivityType.None;
                break;

            case connectionType.wifi:
                if (this._CurrentConnectivity !== ConnectivityType.WiFi)
                    this.OnConnectivityChanged.next(ConnectivityType.WiFi);

                this._CurrentConnectivity = ConnectivityType.WiFi;
                break;

            case connectionType.mobile:
                if (this._CurrentConnectivity !== ConnectivityType.Mobile)
                    this.OnConnectivityChanged.next(ConnectivityType.Mobile);

                this._CurrentConnectivity = ConnectivityType.Mobile;
                break;

            case connectionType.ethernet:
                if (this._CurrentConnectivity !== ConnectivityType.Ethernet)
                    this.OnConnectivityChanged.next(ConnectivityType.Ethernet);

                this._CurrentConnectivity = ConnectivityType.Ethernet;
                break;

            case connectionType.bluetooth:
                if (this._CurrentConnectivity !== ConnectivityType.Bluetooth)
                    this.OnConnectivityChanged.next(ConnectivityType.Bluetooth);

                this._CurrentConnectivity = ConnectivityType.Bluetooth;
                break;
        }

        return this._CurrentConnectivity;
    }
}