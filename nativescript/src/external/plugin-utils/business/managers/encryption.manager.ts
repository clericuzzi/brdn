import { ObjectUtils } from '../../utils/object-utils';

const AES = require('crypto-js/aes');
const SHA256 = require('crypto-js/sha256');
const cryptoJS = require('crypto-js');

export class PluginUtilsEncriptionManager
{
    private static _Secret: string;

    public static InitSecret(secret: string): void { this._Secret = secret; }

    public static AesEncrypt(value: string): string
    {
        if (ObjectUtils.NullUndefinedOrEmpty(this._Secret))
            throw new Error(`Please initialize the SECRET`);

        return AES.encrypt(JSON.stringify(value), this._Secret).toString();
    }
    public static AesDecrypt(value: any): string
    {
        return JSON.parse(this.AesDecryptString(value));
    }

    public static AesEncryptString(value: string): string
    {
        if (ObjectUtils.NullUndefinedOrEmpty(this._Secret))
            throw new Error(`Please initialize the SECRET`);

        return AES.encrypt(value, this._Secret).toString();
    }
    public static AesDecryptString(value: any): string
    {
        if (ObjectUtils.NullUndefinedOrEmpty(this._Secret))
            throw new Error(`Please initialize the SECRET`);

        const bytes = AES.decrypt(value.toString(), this._Secret);
        return bytes.toString(cryptoJS.enc.Utf8);
    }

    public static Hash(value: string): string
    {
        const hashedValue = SHA256(value.toString()).toString();
        return hashedValue;
    }
}