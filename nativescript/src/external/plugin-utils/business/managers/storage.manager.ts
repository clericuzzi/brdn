// utils
import { DateUtils } from '../../utils/date-utils';
import { ObjectUtils } from '../../utils/object-utils';

// constants
import * as AppSettings from 'tns-core-modules/application-settings';
import { JsonUtils } from '../../utils/json-utils';

export class PluginUtilsStorageManager
{
  public static GetBool(key: string, defaultValue?: boolean): boolean
  {
    let value: boolean = null;
    if (!ObjectUtils.NullOrUndefined(defaultValue))
      value = AppSettings.getBoolean(key, defaultValue);
    else
    {
      value = AppSettings.getBoolean(key);
      if (ObjectUtils.NullOrUndefined(value))
        this._KeyNotInitializedError(key);
    }

    return value;
  }
  public static GetDate(key: string, defaultValue?: Date): Date
  {
    let value: string = null;
    if (!ObjectUtils.NullOrUndefined(defaultValue))
      value = AppSettings.getString(key, this._DateToString(defaultValue));
    else
    {
      value = AppSettings.getString(key);
      if (ObjectUtils.NullOrUndefined(value))
        this._KeyNotInitializedError(key);
    }

    return DateUtils.FromDateServer(value);
  }
  public static GetNumber(key: string, defaultValue?: number): number
  {
    let value: number = null;
    if (!ObjectUtils.NullOrUndefined(defaultValue))
      value = AppSettings.getNumber(key, defaultValue);
    else
    {
      value = AppSettings.getNumber(key);
      if (ObjectUtils.NullOrUndefined(value) || Number.isNaN(value))
        this._KeyNotInitializedError(key);
    }

    return value;
  }
  public static GetObject<T>(baseObject: T, key: string): T
  {
    let value: string = null;
    if (!this.Check(key))
      return baseObject;
    else
    {
      value = AppSettings.getString(key);
      return JsonUtils.ToInstanceOf<T>(baseObject, JSON.parse(value));
    }
  }
  public static GetString(key: string, defaultValue?: string): string
  {
    let value: string = null;
    if (!ObjectUtils.NullOrUndefined(defaultValue))
      value = AppSettings.getString(key, defaultValue);
    else
    {
      value = AppSettings.getString(key);
      if (ObjectUtils.NullOrUndefined(value))
        this._KeyNotInitializedError(key);
    }

    return value;
  }

  public static SetBool(key: string, value: boolean): void
  {
    this._CheckStoringValue(value);
    AppSettings.setBoolean(key, value);
  }
  public static SetDate(key: string, value: Date): void
  {
    this._CheckStoringValue(value);
    const stringValue: string = this._DateToString(value);

    this.SetString(key, stringValue);
  }
  public static SetNumber(key: string, value: number): void
  {
    this._CheckStoringValue(value);
    AppSettings.setNumber(key, value);
  }
  public static SetObject(key: string, value: any): void
  {
    this._CheckStoringValue(value);
    AppSettings.setString(key, JsonUtils.Stringify(value));
  }
  public static SetString(key: string, value: string): void
  {
    this._CheckStoringValue(value);
    AppSettings.setString(key, value);
  }

  private static _KeyNotInitializedError(key: string): void
  {
    throw new Error(`No value stored in the key '${key}'. If you want to initialize a variable, pass its default value`);
  }
  private static _CheckStoringValue(value: any)
  {
    if (ObjectUtils.NullUndefinedOrEmpty(value))
      throw new Error(`You should not store an empty value. For deleting a setting please call 'ClearKey'`);
  }

  public static Check(key: string): boolean
  {
    return AppSettings.hasKey(key);
  }
  public static Clear(): void
  {
    AppSettings.clear();
  }
  public static ClearKey(key: string): void
  {
    AppSettings.remove(key);
  }

  private static _DateToString(value: Date)
  {
    let hours: string = value.getHours().toString();
    let seconds: string = value.getSeconds().toString();
    let minutes: string = value.getMinutes().toString();
    let day: string = value.getDate().toString();
    let month: string = (value.getMonth() + 1).toString();
    const year: string = value.getFullYear().toString();
    if (day.length === 1)
      day = `0${day}`;
    if (month.length === 1)
      month = `0${month}`;
    if (hours.length === 1)
      hours = `0${hours}`;
    if (minutes.length === 1)
      minutes = `0${minutes}`;
    if (seconds.length === 1)
      seconds = `0${seconds}`;
    const stringValue: string = `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`;
    return stringValue;
  }
}
