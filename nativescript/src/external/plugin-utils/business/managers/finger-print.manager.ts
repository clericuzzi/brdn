import { Subject } from 'rxjs';
import { FingerprintAuth, BiometricIDAvailableResult } from 'nativescript-fingerprint-auth';

export class PluginUtilsFingerPrintManager
{
    public static OnAuthenticationFailed: Subject<void> = new Subject<void>();
    public static OnAuthenticationSuccessfull: Subject<void> = new Subject<void>();

    private static _Auth: FingerprintAuth = new FingerprintAuth();
    public static Auth(title: string, message: string, durtion: number, nicerUI: boolean): void
    {
        const options: any = { title: title, message: message, authenticationValidityDuration: durtion, useCustomAndroidUI: nicerUI };
        this._Auth.verifyFingerprint(options).then((enteredPassword?: string) => this._AuthSuccess(enteredPassword)).catch(err => this._AuthFailed(err));
    }
    private static _AuthFailed(err: Error): void
    {
        this.OnAuthenticationFailed.next();
    }
    private static _AuthSuccess(enteredPassword?: string): void
    {
        this.OnAuthenticationSuccessfull.next();
    }
    public static IsAvailable(): boolean
    {
        this._Auth.available().then((result: BiometricIDAvailableResult) =>
        {
            console.log(`Biometric ID available? ${result.any}`);
            console.log(`Touch? ${result.touch}`);
            console.log(`Face? ${result.face}`);
        });

        return false;
    }
}
