import { Subject, Subscription } from 'rxjs';
import { NamedSubscription } from './named-subscription.model';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';
import { takeUntil } from 'rxjs/operators';

export class NamedSubscriptionManager
{
    public Subscriptions: NamedSubscription[] = [];

    public Clear(): void
    {
        for (const subscription of this.Subscriptions)
            subscription.Unsubscribe();
    }
    public Add(subscription: NamedSubscription): void
    {
        if (!this.Subscriptions.find(i => i.Name === subscription.Name))
            this.Subscriptions.push(subscription);
        else
            throw new Error(`subscription '${subscription.Name}' already exists`);
    }
    public Remove(subscription: NamedSubscription): void
    {
        if (!this.Subscriptions.find(i => i.Name === subscription.Name))
        {
            const index: number = this.Subscriptions.indexOf(subscription);
            this.Subscriptions.splice(index, 1);
        }
    }
    public Subscribe<T>(name: string, subject: Subject<T>, until: Subject<void>, next: (value: T) => void, error?: (value: T) => void, complete?: () => void): NamedSubscription
    {
        if (!this.Subscriptions.find(i => i.Name === name))
        {
            const subscription: Subscription = subject.pipe(takeUntil(until)).subscribe(next, error, complete);

            const namedSubscription: NamedSubscription = new NamedSubscription();
            namedSubscription.Name = name;
            namedSubscription.Subscription = subscription;

            this.Add(namedSubscription);
            return namedSubscription;
        }
        return null;
    }
    public Unsubscribe(name: string): void
    {
        const namedSubscription = this.Subscriptions.find(i => i.Name === name);
        if (!ObjectUtils.NullOrUndefined(namedSubscription))
        {
            namedSubscription.Unsubscribe();
            this.Remove(namedSubscription);
        }
    }
}