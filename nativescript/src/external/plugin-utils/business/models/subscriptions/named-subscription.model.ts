import { Subscription } from 'rxjs';

export class NamedSubscription
{
    public Name: string;
    public Subscription: Subscription;
    constructor()
    {
    }

    public Unsubscribe(): void
    {
        this.Subscription.unsubscribe();
    }
}