﻿export class CustomField
{
    public Key: string;
    public Value: any;

    constructor(key: string, value: any)
    {
        this.Key = key;
        this.Value = value;
    }
}