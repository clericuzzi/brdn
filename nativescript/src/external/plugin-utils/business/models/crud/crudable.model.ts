﻿
// utils
import { Dictionary } from '@src/external/plugin-utils/utils/dictionary-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// models
import { CrudableTableColumnDefinitionModel, ComponentTypeEnum } from './definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionModel, CrudableTableDefinitionGender } from './definitions/crudable-table-definition.model';

export abstract class Crudable
{
    public PkProperty: string = `Id`;
    public TableDefinition: CrudableTableDefinitionModel;
    public CustomProperties: string[] = [];
    public ColumnDefinitions: Dictionary<CrudableTableColumnDefinitionModel> = new Dictionary<CrudableTableColumnDefinitionModel>();

    constructor()
    {
    }

    public GetColumnDefinition(columnName: string): CrudableTableColumnDefinitionModel
    {
        return this.ColumnDefinitions.Get(columnName);
    }

    public SetTableDefinition(className: string, tableName: string, tableGender: CrudableTableDefinitionGender, readableName: string, readableNamePlural: string, canFilter: boolean = true, canSearch: boolean = true, canInsert: boolean = true, canDelete: boolean = true, canUpdate: boolean = true, hasSelectionBox: boolean = true): void
    {
        this.TableDefinition = new CrudableTableDefinitionModel(className, tableName, tableGender, readableName, readableNamePlural, canFilter, canSearch, canInsert, canDelete, canUpdate, hasSelectionBox);
    }

    public SetColumnDefinition(key: string, columnName: string, propertyName: string, readableName: string, readableNamePlural: string, componentType: ComponentTypeEnum, canSort: boolean = true, canFilter: boolean = false, canInsert: boolean = true, canSelect: boolean = true, canUpdate: boolean = true, hasShadow: boolean = true, componentSubcategory: string = null, transform: Function = null, maxWidth: number = null, widthPixels: number = null, widthPercentage: number = null, textAlign: string = `center`, isNullable: boolean = false): void
    {
        this.ColumnDefinitions.Add(key, new CrudableTableColumnDefinitionModel(columnName, propertyName, readableName, readableNamePlural, componentType, canSort, canFilter, canInsert, canSelect, canUpdate, hasShadow, componentSubcategory, transform, maxWidth, widthPixels, widthPercentage, textAlign, isNullable));
    }

    public get InsertTitle(): string
    {
        if (ObjectUtils.NullOrUndefined(this.TableDefinition))
            return null;
        else
            return `Nov${this.TableDefinition.Article()} ${this.TableDefinition.ReadableName}`;
    }
    public get UpdateTitle(): string
    {
        if (ObjectUtils.NullOrUndefined(this.TableDefinition))
            return null;
        else
            return `Atualizar ${this.TableDefinition.ReadableName}`;
    }
    public get SearchTitle(): string
    {
        if (ObjectUtils.NullOrUndefined(this.TableDefinition))
            return null;
        else
            return `Pesquisar ${this.TableDefinition.ReadableName}`;
    }

    public get Key(): number
    {
        if (ObjectUtils.NullOrUndefined(this[this.PkProperty]))
            return null;
        else
            return this[this.PkProperty];
    }
    public GetKey(): number
    {
        if (ObjectUtils.NullOrUndefined(this[this.PkProperty]))
            return null;
        else
            return this[this.PkProperty];
    }

    public Filter(filter: string): boolean
    {
        filter = filter.toLocaleLowerCase();

        const json: any = this.ToJson();
        const properties: string[] = Object.keys(json);
        for (const property of properties)
        {
            const value: any = ObjectUtils.GetValue(json, null, property);
            if (!ObjectUtils.NullOrUndefined(value))
            {
                if (typeof (value) === `object`)
                {
                    const innerValues: string[] = Object.values(value);
                    for (const innerValue of innerValues)
                        if (this._FilterValue(innerValue, filter))
                            return true;
                }
                else
                {
                    if (this._FilterValue(value, filter))
                        return true;
                }
            }
        }

        return false;
    }
    private _FilterValue(value: any, filterString: string): boolean
    {
        if (!ObjectUtils.NullOrUndefined(value))
        {
            let stringValue: string = value.toString();
            stringValue = stringValue.toLocaleLowerCase();
            if (stringValue.indexOf(filterString) >= 0)
                return true;
        }
        return false;
    }

    // ICRUDABLE IMPLEMENTATION
    public abstract Clone(): Crudable;
    public abstract ToJson(): string;
    public abstract ToString(): string;
    public abstract ParseToJson(): string;
    public abstract ParseFromJson(json: Object): void;
}