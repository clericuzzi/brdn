﻿// models
import { CustomField } from './custom-field.model';

// utils
import { JsonUtils } from '@src/external/plugin-utils/utils/json-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';
import { ɵConsole } from '@angular/core';

/**
 * This class represents an object that can extend it's properties with custom fields
 */
export class CustomFieldable
{
    public CustomFields: CustomField[] = [];

    /**
     * Clears the custom fields array
     */
    public Clear(): void
    {
        this.CustomFields = [];
    }
    /**
     * Gets the count of the custom fields currently in the collection
     */
    public get Count(): number
    {
        if (ObjectUtils.NullUndefinedOrEmptyArray(this.CustomFields))
            return 0;
        else
            return this.CustomFields.length;
    }
    private _CheckFields(): void
    {
        if (this.CustomFields === null)
            this.CustomFields = new Array<CustomField>();
    }

    /**
     * Adds a new parameter to the collection
     * @param key the param's name
     * @param value the param's value
     */
    public Add(key: string, value: any): boolean
    {
        if (ObjectUtils.NullOrUndefined(value))
            value = null;

        this._CheckFields();
        if (this.Contains(key))
            this.Remove(key);

        this.CustomFields.push(new CustomField(key, JsonUtils.Stringify(value)));

        return false;
    }
    /**
     * Adds a new parameter to the collection without serializing it
     * @param key the param's name
     * @param value the param's value
     */
    public AddRaw(key: string, value: any): boolean
    {
        if (ObjectUtils.NullOrUndefined(value))
            value = null;

        this._CheckFields();
        if (this.Contains(key))
            this.Remove(key);

        this.CustomFields.push(new CustomField(key, value));

        return true;
    }
    /**
     * Removes a parameter from the collection
     * @param key the param's name
     */
    public Remove(key: string): boolean
    {
        this._CheckFields();
        if (this.Contains(key))
        {
            for (let i = 0; i < this.CustomFields.length; i++)
            {
                const field: CustomField = this.CustomFields[i];
                if (field.Key === key)
                {
                    this.CustomFields.splice(i, 1);
                    return true;
                }
            }
        }

        return false;
    }
    /**
     * Checks if the given parameter exists in the collection
     * @param key the param's name
     */
    public Contains(key: string): boolean
    {
        this._CheckFields();
        if (this.CustomFields === null || this.CustomFields.length === 0)
            return false;

        for (let i = 0; i < this.CustomFields.length; i++)
            if (this.CustomFields[i].Key === key)
                return true;

        return false;
    }
    /**
     * Returns the custom field that matchs with the provided param name
     * @param key the param's name
     */
    private _GetField(key: string): CustomField
    {
        this._CheckFields();
        if (this.CustomFields === null || this.CustomFields.length === 0)
            return null;

        for (let i = 0; i < this.CustomFields.length; i++)
            if (this.CustomFields[i].Key === key)
                return this.CustomFields[i];

        return null;
    }
    /**
     * Returns a value stored in the collection
     * @param key the param's name
     * @param reviver the reviver class
     */
    public GetValue<T>(key: string, template: T): T
    {
        try
        {
            if (this.Contains(key))
            {
                const field: CustomField = this._GetField(key);
                if (field.Value != null && field !== undefined)
                {
                    if (ObjectUtils.NullOrUndefined(template))
                        return JSON.parse(field.Value) as T;
                    else
                        return ObjectUtils.Assign(template, field.Value);
                }
            }
        }
        catch (e)
        {
        }

        return null;
    }

    public ParseFieldsFrom(obj: any): void
    {
        this.CustomFields = [];
        const fields: any = obj[`CustomFields`];
        if (!ObjectUtils.NullUndefinedOrEmptyArray(fields))
        {
            for (const prop in fields)
                this.CustomFields.push(new CustomField(prop, fields[prop]));
        }
    }
}