﻿// models
import { CustomFieldable } from '../crud/custom-fieldable.model';
import { RequestPagination } from './pagination/request-pagination.model';
import { RequestIdentifier } from '../response/request-identifier.model';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';
import { CustomField } from '../crud/custom-field.model';
import { JsonUtils } from '@src/external/plugin-utils/utils/json-utils';
import { PluginUtilsEncriptionManager } from '../../managers/encryption.manager';

// services

/**
 * A set of parameters to rule all default requests
 * All of them can be overidden
 */
export class RequestMessageDefaults
{
    /**
     * A flag indicating if the default behaviour is to encrypt the requests
     */
    public static Crypted: boolean = true;

    /**
     * A flag indicating if the default behaviour is to compress the requests
     */
    public static Compressed: boolean = false;

    /**
     * The public key for a particular request
     */
    public static PublicKey: string;
    /**
     * The currently logged user id
     */
    public static UserId: number;
    /**
     * The currently logged account id
     */
    public static AccountId: number;
}

/**
 * This class represents a web request
 */
export class RequestMessage extends CustomFieldable
{
    /**
     * The identifier object containing auth information
     */
    public Identifier: RequestIdentifier;

    /**
     * Request's pagination properties
     */
    public Pagination: RequestPagination;

    /**
     * creates a new request message based on the given parameters
     * @param url the request url
     * @param fields the request params
     */
    public static New(url: string, ...fields: CustomField[]): RequestMessage
    {
        const req: RequestMessage = new RequestMessage(url);
        for (const field of fields)
            req.AddParam(field.Key, field.Value);

        return req;
    }

    /**
     * Default constructor
     * @param url The reuqest's url
     * @param startClean if TRUE we will not initialize the listing, sorting, filtering, pagination and navigation properties
     */
    constructor(public Url?: string, startClean: boolean = true)
    {
        super();

        this.Add('crypt-data', RequestMessageDefaults.Crypted);
        this.Add('compressed', RequestMessageDefaults.Compressed);

        if (!ObjectUtils.NullOrUndefined(RequestMessageDefaults.UserId))
            this.AddId(RequestMessageDefaults.UserId);
        if (!ObjectUtils.NullOrUndefined(RequestMessageDefaults.AccountId))
            this.AddAccountId(RequestMessageDefaults.AccountId);

        if (!startClean)
            this.Pagination = new RequestPagination();
        else
            this.Pagination = null;
    }

    /**
     * Adds the user id to the outgoing request
     * @param id the reqeuster's id
     */
    public AddId(id: number): void { this.AddParam('user-id', id); }
    /**
     * Adds a list of object ids to the outgoing request
     * @param ids a list of ids to be handled by the server
     */
    public AddIds(ids: number[]): void { this.AddEntities(ids); }
    /**
     * Adds an object to the outgoing request
     * @param entity the entity to be added to the outgoing request
     */
    public AddEntity(entity: any): void { this.AddParam('entity', entity); }
    /**
     * Adds a list of entities to the outgoing request
     * @param entities the list of entities to the outgoing request
     */
    public AddEntities(entities: any[]): void { this.AddParam('entities', entities); }
    /**
     * Adds an account id to the outgoing request
     * @param accountId the account id to be added to the outgoing request
     */
    public AddAccountId(accountId: number): void { this.AddParam('account-id', accountId); }

    /**
     * Adds a new parameter to the collection
     * @param key the param's name
     * @param value the param's value
     */
    public AddParam(key: string, value: any): boolean
    {
        if (this.CustomFields === null)
            this.CustomFields = new Array<CustomField>();

        if (ObjectUtils.NullOrUndefined(value))
            value = null;

        const encryptedKey = PluginUtilsEncriptionManager.AesEncryptString(key);
        if (this.Contains(key) || this.Contains(encryptedKey))
        {
            this.Remove(key);
            this.Remove(encryptedKey);
        }

        value = JsonUtils.Stringify(value);
        if (RequestMessageDefaults.Crypted)
        {
            key = PluginUtilsEncriptionManager.AesEncryptString(key);
            value = PluginUtilsEncriptionManager.AesEncryptString(value);

            return this.AddRaw(key, value);
        }
        else
            return this.Add(key, value);
    }

    public Clear(): void
    {
        this.Pagination = null;
    }
}