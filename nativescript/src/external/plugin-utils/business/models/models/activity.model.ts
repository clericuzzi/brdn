﻿import { Crudable } from '@src/external/plugin-utils/business/models/crud/crudable.model';

// utils
import { DateUtils } from '@src/external/plugin-utils/utils/date-utils';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// Navigation Properties
import { Account } from './account.model';

// components helpers
import { ComponentTypeEnum } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-definition.model';

export const ActivityClass: string = `Activity`;
export const ActivityTable: string = `activity`;

/**
 * Column definitions for the `Activity` class
 */
export enum ActivityColumns
{
	Id = 'Id',
	Name = 'Name',
	AccountId = 'AccountId',
	DailyCount = 'DailyCount',

	AccountIdParent = 'AccountIdParent',
}
export const ActivityColumnsFilter: string[] = [];
export const ActivityColumnsInsert: string[] = [ActivityColumns.AccountId, ActivityColumns.Name, ActivityColumns.DailyCount];
export const ActivityColumnsUpdate: string[] = [ActivityColumns.AccountId, ActivityColumns.Name, ActivityColumns.DailyCount];

/**
 * Implementations of the `Activity` class
 */
export class Activity extends Crudable
{
	public AccountIdParent: Account;

	public static FromJson(json: any): Activity
	{
		const item: Activity = new Activity();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public AccountId: number = null,
		public Name: string = null,
		public DailyCount: number = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`Activity`, `activity`, CrudableTableDefinitionGender.Male, `Activity`, `Activitys`, true, true, true, true, true, true);

		this.SetColumnDefinition(ActivityColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ActivityColumns.AccountId, `account_id`, `AccountId`, `Account Id`, `Account Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ActivityColumns.Name, `name`, `Name`, `Name`, `Names`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ActivityColumns.DailyCount, `daily_count`, `DailyCount`, `Daily Count`, `Daily Counts`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
	}

	public Clone(): Activity
	{
		const clone: Activity = new Activity();
		clone.Id = this.Id;
		clone.Name = this.Name;
		clone.AccountId = this.AccountId;
		clone.DailyCount = this.DailyCount;

		clone.AccountIdParent = this.AccountIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.AccountIdParent == null ? '' : this.AccountIdParent.ToString()}`;
	}

	public ToJson(navigationProperties: boolean = false): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json['Name'] = this.Name;
		if (!ObjectUtils.NullOrUndefined(this.DailyCount))
			json['DailyCount'] = this.DailyCount;
		if (navigationProperties)
		{
			if (!ObjectUtils.NullOrUndefined(this.AccountIdParent))
				json['AccountIdParent'] = this.AccountIdParent.ToJson();
		}

		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: any): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Name = json['Name'];
			this.AccountId = json['AccountId'];
			this.DailyCount = json['DailyCount'];

			if (!ObjectUtils.NullOrUndefined(json['AccountIdParent']))
			{
				this.AccountIdParent = new Account();
				this.AccountIdParent.ParseFromJson(json['AccountIdParent']);
			}
		}
	}
}

/**
 * Implementations of the `ActivityFilter` class
 */
export class ActivityFilter extends Crudable
{
	public static FromJson(json: Object): ActivityFilter
	{
		const item: ActivityFilter = new ActivityFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public AccountId: number[] = null,
		public Name: string = null,
		public DailyCount: number = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`ActivityFilter`, `activityFilter`, CrudableTableDefinitionGender.Male, `Activity`, `Activitys`, true, true, true, true, true, true);

		this.SetColumnDefinition(ActivityColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ActivityColumns.AccountId, `account_id`, `AccountId`, `Account Id`, `Account Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ActivityColumns.Name, `name`, `Name`, `Name`, `Names`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ActivityColumns.DailyCount, `daily_count`, `DailyCount`, `Daily Count`, `Daily Counts`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
	}

	public Clone(): ActivityFilter
	{
		const clone: ActivityFilter = new ActivityFilter();
		clone.Id = this.Id;
		clone.Name = this.Name;
		clone.AccountId = this.AccountId;
		clone.DailyCount = this.DailyCount;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json['Name'] = this.Name;
		if (!ObjectUtils.NullOrUndefined(this.DailyCount))
			json['DailyCount'] = this.DailyCount;
		if (!ObjectUtils.NullOrUndefined(this.AccountId))
			json['AccountId'] = Array.isArray(this.AccountId) ? this.AccountId : [this.AccountId];


		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Name = json['Name'];
			this.AccountId = json['AccountId'];
			this.DailyCount = json['DailyCount'];
		}
	}
}