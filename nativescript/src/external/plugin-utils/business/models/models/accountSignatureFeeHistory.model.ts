﻿import { Crudable } from '@src/external/plugin-utils/business/models/crud/crudable.model';

// utils
import { DateUtils } from '@src/external/plugin-utils/utils/date-utils';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// Navigation Properties
import { Account } from './account.model';

// components helpers
import { ComponentTypeEnum } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-definition.model';

export const AccountSignatureFeeHistoryClass: string = `AccountSignatureFeeHistory`;
export const AccountSignatureFeeHistoryTable: string = `account_signature_fee_history`;

/**
 * Column definitions for the `AccountSignatureFeeHistory` class
 */
export enum AccountSignatureFeeHistoryColumns
{
	Id = 'Id',
	Year = 'Year',
	Month = 'Month',
	AccountId = 'AccountId',
	SignatureFee = 'SignatureFee',

	AccountIdParent = 'AccountIdParent',
}
export const AccountSignatureFeeHistoryColumnsFilter: string[] = [];
export const AccountSignatureFeeHistoryColumnsInsert: string[] = [AccountSignatureFeeHistoryColumns.AccountId, AccountSignatureFeeHistoryColumns.Year, AccountSignatureFeeHistoryColumns.Month, AccountSignatureFeeHistoryColumns.SignatureFee];
export const AccountSignatureFeeHistoryColumnsUpdate: string[] = [AccountSignatureFeeHistoryColumns.AccountId, AccountSignatureFeeHistoryColumns.Year, AccountSignatureFeeHistoryColumns.Month, AccountSignatureFeeHistoryColumns.SignatureFee];

/**
 * Implementations of the `AccountSignatureFeeHistory` class
 */
export class AccountSignatureFeeHistory extends Crudable
{
	public AccountIdParent: Account;

	public static FromJson(json: any): AccountSignatureFeeHistory
	{
		const item: AccountSignatureFeeHistory = new AccountSignatureFeeHistory();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public AccountId: number = null,
		public Year: number = null,
		public Month: number = null,
		public SignatureFee: number = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`AccountSignatureFeeHistory`, `account_signature_fee_history`, CrudableTableDefinitionGender.Male, `Account Signature Fee History`, `Account Signature Fee Historys`, true, true, true, true, true, true);

		this.SetColumnDefinition(AccountSignatureFeeHistoryColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountSignatureFeeHistoryColumns.AccountId, `account_id`, `AccountId`, `Account Id`, `Account Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountSignatureFeeHistoryColumns.Year, `year`, `Year`, `Year`, `Years`, ComponentTypeEnum.Number, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountSignatureFeeHistoryColumns.Month, `month`, `Month`, `Month`, `Months`, ComponentTypeEnum.Number, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountSignatureFeeHistoryColumns.SignatureFee, `signature_fee`, `SignatureFee`, `Signature Fee`, `Signature Fees`, ComponentTypeEnum.Decimal, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): AccountSignatureFeeHistory
	{
		const clone: AccountSignatureFeeHistory = new AccountSignatureFeeHistory();
		clone.Id = this.Id;
		clone.Year = this.Year;
		clone.Month = this.Month;
		clone.AccountId = this.AccountId;
		clone.SignatureFee = this.SignatureFee;

		clone.AccountIdParent = this.AccountIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.AccountIdParent == null ? '' : this.AccountIdParent.ToString()}`;
	}

	public ToJson(navigationProperties: boolean = false): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Year))
			json['Year'] = this.Year;
		if (!ObjectUtils.NullOrUndefined(this.Month))
			json['Month'] = this.Month;
		if (!ObjectUtils.NullOrUndefined(this.SignatureFee))
			json['SignatureFee'] = this.SignatureFee;
		if (navigationProperties)
		{
			if (!ObjectUtils.NullOrUndefined(this.AccountIdParent))
				json['AccountIdParent'] = this.AccountIdParent.ToJson();
		}

		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: any): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Year = json['Year'];
			this.Month = json['Month'];
			this.AccountId = json['AccountId'];
			this.SignatureFee = json['SignatureFee'];

			if (!ObjectUtils.NullOrUndefined(json['AccountIdParent']))
			{
				this.AccountIdParent = new Account();
				this.AccountIdParent.ParseFromJson(json['AccountIdParent']);
			}
		}
	}
}

/**
 * Implementations of the `AccountSignatureFeeHistoryFilter` class
 */
export class AccountSignatureFeeHistoryFilter extends Crudable
{
	public static FromJson(json: Object): AccountSignatureFeeHistoryFilter
	{
		const item: AccountSignatureFeeHistoryFilter = new AccountSignatureFeeHistoryFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public AccountId: number[] = null,
		public Year: number = null,
		public Month: number = null,
		public SignatureFee: number = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`AccountSignatureFeeHistoryFilter`, `account_signature_fee_historyFilter`, CrudableTableDefinitionGender.Male, `Account Signature Fee History`, `Account Signature Fee Historys`, true, true, true, true, true, true);

		this.SetColumnDefinition(AccountSignatureFeeHistoryColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountSignatureFeeHistoryColumns.AccountId, `account_id`, `AccountId`, `Account Id`, `Account Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountSignatureFeeHistoryColumns.Year, `year`, `Year`, `Year`, `Years`, ComponentTypeEnum.Number, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountSignatureFeeHistoryColumns.Month, `month`, `Month`, `Month`, `Months`, ComponentTypeEnum.Number, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountSignatureFeeHistoryColumns.SignatureFee, `signature_fee`, `SignatureFee`, `Signature Fee`, `Signature Fees`, ComponentTypeEnum.Decimal, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): AccountSignatureFeeHistoryFilter
	{
		const clone: AccountSignatureFeeHistoryFilter = new AccountSignatureFeeHistoryFilter();
		clone.Id = this.Id;
		clone.Year = this.Year;
		clone.Month = this.Month;
		clone.AccountId = this.AccountId;
		clone.SignatureFee = this.SignatureFee;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Year))
			json['Year'] = this.Year;
		if (!ObjectUtils.NullOrUndefined(this.Month))
			json['Month'] = this.Month;
		if (!ObjectUtils.NullOrUndefined(this.SignatureFee))
			json['SignatureFee'] = this.SignatureFee;
		if (!ObjectUtils.NullOrUndefined(this.AccountId))
			json['AccountId'] = Array.isArray(this.AccountId) ? this.AccountId : [this.AccountId];


		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Year = json['Year'];
			this.Month = json['Month'];
			this.AccountId = json['AccountId'];
			this.SignatureFee = json['SignatureFee'];
		}
	}
}