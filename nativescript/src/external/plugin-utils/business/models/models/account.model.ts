﻿import { Crudable } from '@src/external/plugin-utils/business/models/crud/crudable.model';

// utils
import { DateUtils } from '@src/external/plugin-utils/utils/date-utils';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// components helpers
import { ComponentTypeEnum } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-definition.model';

export const AccountClass: string = `Account`;
export const AccountTable: string = `account`;

/**
 * Column definitions for the `Account` class
 */
export enum AccountColumns
{
	Id = 'Id',
	Cnpj = 'Cnpj',
	Name = 'Name',
	Site = 'Site',
	Email = 'Email',
	Phone = 'Phone',
	Blocked = 'Blocked',
	Latitude = 'Latitude',
	Longitude = 'Longitude',
	Timestamp = 'Timestamp',
	SignatureFee = 'SignatureFee',
}
export const AccountColumnsFilter: string[] = [];
export const AccountColumnsInsert: string[] = [AccountColumns.Cnpj, AccountColumns.Name, AccountColumns.Site, AccountColumns.Email, AccountColumns.Phone, AccountColumns.Blocked, AccountColumns.Latitude, AccountColumns.Longitude, AccountColumns.SignatureFee, AccountColumns.Timestamp];
export const AccountColumnsUpdate: string[] = [AccountColumns.Cnpj, AccountColumns.Name, AccountColumns.Site, AccountColumns.Email, AccountColumns.Phone, AccountColumns.Blocked, AccountColumns.Latitude, AccountColumns.Longitude, AccountColumns.SignatureFee, AccountColumns.Timestamp];

/**
 * Implementations of the `Account` class
 */
export class Account extends Crudable
{
	public static FromJson(json: any): Account
	{
		const item: Account = new Account();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public Cnpj: string = null,
		public Name: string = null,
		public Site: string = null,
		public Email: string = null,
		public Phone: string = null,
		public Blocked: number = null,
		public Latitude: number = null,
		public Longitude: number = null,
		public SignatureFee: number = null,
		public Timestamp: Date = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`Account`, `account`, CrudableTableDefinitionGender.Male, `Account`, `Accounts`, true, true, true, true, true, true);

		this.SetColumnDefinition(AccountColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountColumns.Cnpj, `cnpj`, `Cnpj`, `Cnpj`, `Cnpjs`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountColumns.Name, `name`, `Name`, `Name`, `Names`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(AccountColumns.Site, `site`, `Site`, `Site`, `Sites`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(AccountColumns.Email, `email`, `Email`, `Email`, `Emails`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(AccountColumns.Phone, `phone`, `Phone`, `Phone`, `Phones`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(AccountColumns.Blocked, `blocked`, `Blocked`, `Blocked`, `Blockeds`, ComponentTypeEnum.Number, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountColumns.Latitude, `latitude`, `Latitude`, `Latitude`, `Latitudes`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(AccountColumns.Longitude, `longitude`, `Longitude`, `Longitude`, `Longitudes`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(AccountColumns.SignatureFee, `signature_fee`, `SignatureFee`, `Signature Fee`, `Signature Fees`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(AccountColumns.Timestamp, `timestamp`, `Timestamp`, `Timestamp`, `Timestamps`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
	}

	public Clone(): Account
	{
		const clone: Account = new Account();
		clone.Id = this.Id;
		clone.Cnpj = this.Cnpj;
		clone.Name = this.Name;
		clone.Site = this.Site;
		clone.Email = this.Email;
		clone.Phone = this.Phone;
		clone.Blocked = this.Blocked;
		clone.Latitude = this.Latitude;
		clone.Longitude = this.Longitude;
		clone.Timestamp = this.Timestamp;
		clone.SignatureFee = this.SignatureFee;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.Cnpj}`;
	}

	public ToJson(navigationProperties: boolean = false): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Cnpj))
			json['Cnpj'] = this.Cnpj;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json['Name'] = this.Name;
		if (!ObjectUtils.NullOrUndefined(this.Site))
			json['Site'] = this.Site;
		if (!ObjectUtils.NullOrUndefined(this.Email))
			json['Email'] = this.Email;
		if (!ObjectUtils.NullOrUndefined(this.Phone))
			json['Phone'] = this.Phone;
		if (!ObjectUtils.NullOrUndefined(this.Blocked))
			json['Blocked'] = this.Blocked;
		if (!ObjectUtils.NullOrUndefined(this.Latitude))
			json['Latitude'] = this.Latitude;
		if (!ObjectUtils.NullOrUndefined(this.Longitude))
			json['Longitude'] = this.Longitude;
		if (!ObjectUtils.NullOrUndefined(this.SignatureFee))
			json['SignatureFee'] = this.SignatureFee;
		if (!ObjectUtils.NullOrUndefined(this.Timestamp))
			json['Timestamp'] = DateUtils.ToDateServer(this.Timestamp);
		if (navigationProperties)
		{
		}

		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: any): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Cnpj = json['Cnpj'];
			this.Name = json['Name'];
			this.Site = json['Site'];
			this.Email = json['Email'];
			this.Phone = json['Phone'];
			this.Blocked = json['Blocked'];
			this.Latitude = json['Latitude'];
			this.Longitude = json['Longitude'];
			this.SignatureFee = json['SignatureFee'];
			this.Timestamp = DateUtils.FromDateServer(json['Timestamp']);
		}
	}
}

/**
 * Implementations of the `AccountFilter` class
 */
export class AccountFilter extends Crudable
{
	public static FromJson(json: Object): AccountFilter
	{
		const item: AccountFilter = new AccountFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public Cnpj: string = null,
		public Name: string = null,
		public Site: string = null,
		public Email: string = null,
		public Phone: string = null,
		public Blocked: number = null,
		public Latitude: number = null,
		public Longitude: number = null,
		public SignatureFee: number = null,
		public Timestamp: Date = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`AccountFilter`, `accountFilter`, CrudableTableDefinitionGender.Male, `Account`, `Accounts`, true, true, true, true, true, true);

		this.SetColumnDefinition(AccountColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountColumns.Cnpj, `cnpj`, `Cnpj`, `Cnpj`, `Cnpjs`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountColumns.Name, `name`, `Name`, `Name`, `Names`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(AccountColumns.Site, `site`, `Site`, `Site`, `Sites`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(AccountColumns.Email, `email`, `Email`, `Email`, `Emails`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(AccountColumns.Phone, `phone`, `Phone`, `Phone`, `Phones`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(AccountColumns.Blocked, `blocked`, `Blocked`, `Blocked`, `Blockeds`, ComponentTypeEnum.Number, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountColumns.Latitude, `latitude`, `Latitude`, `Latitude`, `Latitudes`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(AccountColumns.Longitude, `longitude`, `Longitude`, `Longitude`, `Longitudes`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(AccountColumns.SignatureFee, `signature_fee`, `SignatureFee`, `Signature Fee`, `Signature Fees`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(AccountColumns.Timestamp, `timestamp`, `Timestamp`, `Timestamp`, `Timestamps`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
	}

	public Clone(): AccountFilter
	{
		const clone: AccountFilter = new AccountFilter();
		clone.Id = this.Id;
		clone.Cnpj = this.Cnpj;
		clone.Name = this.Name;
		clone.Site = this.Site;
		clone.Email = this.Email;
		clone.Phone = this.Phone;
		clone.Blocked = this.Blocked;
		clone.Latitude = this.Latitude;
		clone.Longitude = this.Longitude;
		clone.Timestamp = this.Timestamp;
		clone.SignatureFee = this.SignatureFee;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Cnpj))
			json['Cnpj'] = this.Cnpj;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json['Name'] = this.Name;
		if (!ObjectUtils.NullOrUndefined(this.Site))
			json['Site'] = this.Site;
		if (!ObjectUtils.NullOrUndefined(this.Email))
			json['Email'] = this.Email;
		if (!ObjectUtils.NullOrUndefined(this.Phone))
			json['Phone'] = this.Phone;
		if (!ObjectUtils.NullOrUndefined(this.Blocked))
			json['Blocked'] = this.Blocked;
		if (!ObjectUtils.NullOrUndefined(this.Latitude))
			json['Latitude'] = this.Latitude;
		if (!ObjectUtils.NullOrUndefined(this.Longitude))
			json['Longitude'] = this.Longitude;
		if (!ObjectUtils.NullOrUndefined(this.SignatureFee))
			json['SignatureFee'] = this.SignatureFee;
		if (!ObjectUtils.NullOrUndefined(this.Timestamp))
			json['Timestamp'] = DateUtils.ToDateServer(this.Timestamp);


		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Cnpj = json['Cnpj'];
			this.Name = json['Name'];
			this.Site = json['Site'];
			this.Email = json['Email'];
			this.Phone = json['Phone'];
			this.Blocked = json['Blocked'];
			this.Latitude = json['Latitude'];
			this.Longitude = json['Longitude'];
			this.SignatureFee = json['SignatureFee'];
			this.Timestamp = DateUtils.FromDateServer(json['Timestamp']);
		}
	}
}