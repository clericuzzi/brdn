﻿import { Crudable } from '@src/external/plugin-utils/business/models/crud/crudable.model';

// utils
import { DateUtils } from '@src/external/plugin-utils/utils/date-utils';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// Navigation Properties
import { Activity } from './activity.model';
import { User } from './user.model';

// components helpers
import { ComponentTypeEnum } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-definition.model';

export const ActivityItemLogClass: string = `ActivityItemLog`;
export const ActivityItemLogTable: string = `activity_item_log`;

/**
 * Column definitions for the `ActivityItemLog` class
 */
export enum ActivityItemLogColumns
{
	Id = 'Id',
	UserId = 'UserId',
	Action = 'Action',
	ActivityId = 'ActivityId',

	UserIdParent = 'UserIdParent',
	ActivityIdParent = 'ActivityIdParent',
}
export const ActivityItemLogColumnsFilter: string[] = [];
export const ActivityItemLogColumnsInsert: string[] = [ActivityItemLogColumns.ActivityId, ActivityItemLogColumns.UserId, ActivityItemLogColumns.Action];
export const ActivityItemLogColumnsUpdate: string[] = [ActivityItemLogColumns.ActivityId, ActivityItemLogColumns.UserId, ActivityItemLogColumns.Action];

/**
 * Implementations of the `ActivityItemLog` class
 */
export class ActivityItemLog extends Crudable
{
	public ActivityIdParent: Activity;
	public UserIdParent: User;

	public static FromJson(json: any): ActivityItemLog
	{
		const item: ActivityItemLog = new ActivityItemLog();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public ActivityId: number = null,
		public UserId: number = null,
		public Action: number = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`ActivityItemLog`, `activity_item_log`, CrudableTableDefinitionGender.Male, `Activity Item Log`, `Activity Item Logs`, true, true, true, true, true, true);

		this.SetColumnDefinition(ActivityItemLogColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ActivityItemLogColumns.ActivityId, `activity_id`, `ActivityId`, `Activity Id`, `Activity Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ActivityItemLogColumns.UserId, `user_id`, `UserId`, `User Id`, `User Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ActivityItemLogColumns.Action, `action`, `Action`, `Action`, `Actions`, ComponentTypeEnum.Number, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): ActivityItemLog
	{
		const clone: ActivityItemLog = new ActivityItemLog();
		clone.Id = this.Id;
		clone.UserId = this.UserId;
		clone.Action = this.Action;
		clone.ActivityId = this.ActivityId;

		clone.UserIdParent = this.UserIdParent;
		clone.ActivityIdParent = this.ActivityIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.ActivityIdParent == null ? '' : this.ActivityIdParent.ToString()}`;
	}

	public ToJson(navigationProperties: boolean = false): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Action))
			json['Action'] = this.Action;
		if (navigationProperties)
		{
			if (!ObjectUtils.NullOrUndefined(this.ActivityIdParent))
				json['ActivityIdParent'] = this.ActivityIdParent.ToJson();
			if (!ObjectUtils.NullOrUndefined(this.UserIdParent))
				json['UserIdParent'] = this.UserIdParent.ToJson();
		}

		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: any): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.UserId = json['UserId'];
			this.Action = json['Action'];
			this.ActivityId = json['ActivityId'];

			if (!ObjectUtils.NullOrUndefined(json['ActivityIdParent']))
			{
				this.ActivityIdParent = new Activity();
				this.ActivityIdParent.ParseFromJson(json['ActivityIdParent']);
			}
			if (!ObjectUtils.NullOrUndefined(json['UserIdParent']))
			{
				this.UserIdParent = new User();
				this.UserIdParent.ParseFromJson(json['UserIdParent']);
			}
		}
	}
}

/**
 * Implementations of the `ActivityItemLogFilter` class
 */
export class ActivityItemLogFilter extends Crudable
{
	public static FromJson(json: Object): ActivityItemLogFilter
	{
		const item: ActivityItemLogFilter = new ActivityItemLogFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public ActivityId: number[] = null,
		public UserId: number[] = null,
		public Action: number = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`ActivityItemLogFilter`, `activity_item_logFilter`, CrudableTableDefinitionGender.Male, `Activity Item Log`, `Activity Item Logs`, true, true, true, true, true, true);

		this.SetColumnDefinition(ActivityItemLogColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ActivityItemLogColumns.ActivityId, `activity_id`, `ActivityId`, `Activity Id`, `Activity Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ActivityItemLogColumns.UserId, `user_id`, `UserId`, `User Id`, `User Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ActivityItemLogColumns.Action, `action`, `Action`, `Action`, `Actions`, ComponentTypeEnum.Number, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): ActivityItemLogFilter
	{
		const clone: ActivityItemLogFilter = new ActivityItemLogFilter();
		clone.Id = this.Id;
		clone.UserId = this.UserId;
		clone.Action = this.Action;
		clone.ActivityId = this.ActivityId;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Action))
			json['Action'] = this.Action;
		if (!ObjectUtils.NullOrUndefined(this.UserId))
			json['UserId'] = Array.isArray(this.UserId) ? this.UserId : [this.UserId];
		if (!ObjectUtils.NullOrUndefined(this.ActivityId))
			json['ActivityId'] = Array.isArray(this.ActivityId) ? this.ActivityId : [this.ActivityId];


		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.UserId = json['UserId'];
			this.Action = json['Action'];
			this.ActivityId = json['ActivityId'];
		}
	}
}