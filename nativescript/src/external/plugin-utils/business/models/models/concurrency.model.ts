﻿import { Crudable } from '@src/external/plugin-utils/business/models/crud/crudable.model';

// utils
import { DateUtils } from '@src/external/plugin-utils/utils/date-utils';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// Navigation Properties
import { User } from './user.model';

// components helpers
import { ComponentTypeEnum } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-definition.model';

export const ConcurrencyClass: string = `Concurrency`;
export const ConcurrencyTable: string = `concurrency`;

/**
 * Column definitions for the `Concurrency` class
 */
export enum ConcurrencyColumns
{
	Id = 'Id',
	Action = 'Action',
	UserId = 'UserId',
	EntityId = 'EntityId',
	MachineName = 'MachineName',
	EntityValue = 'EntityValue',

	UserIdParent = 'UserIdParent',
}
export const ConcurrencyColumnsFilter: string[] = [];
export const ConcurrencyColumnsInsert: string[] = [ConcurrencyColumns.Action, ConcurrencyColumns.UserId, ConcurrencyColumns.MachineName, ConcurrencyColumns.EntityId, ConcurrencyColumns.EntityValue];
export const ConcurrencyColumnsUpdate: string[] = [ConcurrencyColumns.Action, ConcurrencyColumns.UserId, ConcurrencyColumns.MachineName, ConcurrencyColumns.EntityId, ConcurrencyColumns.EntityValue];

/**
 * Implementations of the `Concurrency` class
 */
export class Concurrency extends Crudable
{
	public UserIdParent: User;

	public static FromJson(json: any): Concurrency
	{
		const item: Concurrency = new Concurrency();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public Action: string = null,
		public UserId: number = null,
		public MachineName: string = null,
		public EntityId: number = null,
		public EntityValue: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`Concurrency`, `concurrency`, CrudableTableDefinitionGender.Male, `Concurrency`, `Concurrencys`, true, true, true, true, true, true);

		this.SetColumnDefinition(ConcurrencyColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ConcurrencyColumns.Action, `action`, `Action`, `Action`, `Actions`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ConcurrencyColumns.UserId, `user_id`, `UserId`, `User Id`, `User Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(ConcurrencyColumns.MachineName, `machine_name`, `MachineName`, `Machine Name`, `Machine Names`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(ConcurrencyColumns.EntityId, `entity_id`, `EntityId`, `Entity Id`, `Entity Ids`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(ConcurrencyColumns.EntityValue, `entity_value`, `EntityValue`, `Entity Value`, `Entity Values`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
	}

	public Clone(): Concurrency
	{
		const clone: Concurrency = new Concurrency();
		clone.Id = this.Id;
		clone.Action = this.Action;
		clone.UserId = this.UserId;
		clone.EntityId = this.EntityId;
		clone.MachineName = this.MachineName;
		clone.EntityValue = this.EntityValue;

		clone.UserIdParent = this.UserIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.Action}`;
	}

	public ToJson(navigationProperties: boolean = false): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Action))
			json['Action'] = this.Action;
		if (!ObjectUtils.NullOrUndefined(this.EntityId))
			json['EntityId'] = this.EntityId;
		if (!ObjectUtils.NullOrUndefined(this.MachineName))
			json['MachineName'] = this.MachineName;
		if (!ObjectUtils.NullOrUndefined(this.EntityValue))
			json['EntityValue'] = this.EntityValue;
		if (navigationProperties)
		{
			if (!ObjectUtils.NullOrUndefined(this.UserIdParent))
				json['UserIdParent'] = this.UserIdParent.ToJson();
		}

		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: any): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Action = json['Action'];
			this.UserId = json['UserId'];
			this.EntityId = json['EntityId'];
			this.MachineName = json['MachineName'];
			this.EntityValue = json['EntityValue'];

			if (!ObjectUtils.NullOrUndefined(json['UserIdParent']))
			{
				this.UserIdParent = new User();
				this.UserIdParent.ParseFromJson(json['UserIdParent']);
			}
		}
	}
}

/**
 * Implementations of the `ConcurrencyFilter` class
 */
export class ConcurrencyFilter extends Crudable
{
	public static FromJson(json: Object): ConcurrencyFilter
	{
		const item: ConcurrencyFilter = new ConcurrencyFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public Action: string = null,
		public UserId: number[] = null,
		public MachineName: string = null,
		public EntityId: number = null,
		public EntityValue: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`ConcurrencyFilter`, `concurrencyFilter`, CrudableTableDefinitionGender.Male, `Concurrency`, `Concurrencys`, true, true, true, true, true, true);

		this.SetColumnDefinition(ConcurrencyColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ConcurrencyColumns.Action, `action`, `Action`, `Action`, `Actions`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ConcurrencyColumns.UserId, `user_id`, `UserId`, `User Id`, `User Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(ConcurrencyColumns.MachineName, `machine_name`, `MachineName`, `Machine Name`, `Machine Names`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(ConcurrencyColumns.EntityId, `entity_id`, `EntityId`, `Entity Id`, `Entity Ids`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(ConcurrencyColumns.EntityValue, `entity_value`, `EntityValue`, `Entity Value`, `Entity Values`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
	}

	public Clone(): ConcurrencyFilter
	{
		const clone: ConcurrencyFilter = new ConcurrencyFilter();
		clone.Id = this.Id;
		clone.Action = this.Action;
		clone.UserId = this.UserId;
		clone.EntityId = this.EntityId;
		clone.MachineName = this.MachineName;
		clone.EntityValue = this.EntityValue;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Action))
			json['Action'] = this.Action;
		if (!ObjectUtils.NullOrUndefined(this.EntityId))
			json['EntityId'] = this.EntityId;
		if (!ObjectUtils.NullOrUndefined(this.MachineName))
			json['MachineName'] = this.MachineName;
		if (!ObjectUtils.NullOrUndefined(this.EntityValue))
			json['EntityValue'] = this.EntityValue;
		if (!ObjectUtils.NullOrUndefined(this.UserId))
			json['UserId'] = Array.isArray(this.UserId) ? this.UserId : [this.UserId];


		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Action = json['Action'];
			this.UserId = json['UserId'];
			this.EntityId = json['EntityId'];
			this.MachineName = json['MachineName'];
			this.EntityValue = json['EntityValue'];
		}
	}
}