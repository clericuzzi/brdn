﻿import { Crudable } from '@src/external/plugin-utils/business/models/crud/crudable.model';

// utils
import { DateUtils } from '@src/external/plugin-utils/utils/date-utils';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// components helpers
import { ComponentTypeEnum } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-definition.model';

export const UserClass: string = `User`;
export const UserTable: string = `user`;

/**
 * Column definitions for the `User` class
 */
export enum UserColumns
{
	Id = 'Id',
	Name = 'Name',
	Phone = 'Phone',
	Email = 'Email',
	Active = 'Active',
	Blocked = 'Blocked',
	Password = 'Password',
	Timestamp = 'Timestamp',
}
export const UserColumnsFilter: string[] = [];
export const UserColumnsInsert: string[] = [UserColumns.Name, UserColumns.Phone, UserColumns.Email, UserColumns.Password, UserColumns.Active, UserColumns.Blocked, UserColumns.Timestamp];
export const UserColumnsUpdate: string[] = [UserColumns.Name, UserColumns.Phone, UserColumns.Email, UserColumns.Password, UserColumns.Active, UserColumns.Blocked, UserColumns.Timestamp];

/**
 * Implementations of the `User` class
 */
export class User extends Crudable
{
	public static FromJson(json: any): User
	{
		const item: User = new User();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public Name: string = null,
		public Phone: string = null,
		public Email: string = null,
		public Password: string = null,
		public Active: number = null,
		public Blocked: number = null,
		public Timestamp: Date = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`User`, `user`, CrudableTableDefinitionGender.Male, `User`, `Users`, true, true, true, true, true, true);

		this.SetColumnDefinition(UserColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(UserColumns.Name, `name`, `Name`, `Name`, `Names`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(UserColumns.Phone, `phone`, `Phone`, `Phone`, `Phones`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(UserColumns.Email, `email`, `Email`, `Email`, `Emails`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(UserColumns.Password, `password`, `Password`, `Password`, `Passwords`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(UserColumns.Active, `active`, `Active`, `Active`, `Actives`, ComponentTypeEnum.Number, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(UserColumns.Blocked, `blocked`, `Blocked`, `Blocked`, `Blockeds`, ComponentTypeEnum.Number, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(UserColumns.Timestamp, `timestamp`, `Timestamp`, `Timestamp`, `Timestamps`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
	}

	public Clone(): User
	{
		const clone: User = new User();
		clone.Id = this.Id;
		clone.Name = this.Name;
		clone.Phone = this.Phone;
		clone.Email = this.Email;
		clone.Active = this.Active;
		clone.Blocked = this.Blocked;
		clone.Password = this.Password;
		clone.Timestamp = this.Timestamp;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.Name}`;
	}

	public ToJson(navigationProperties: boolean = false): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json['Name'] = this.Name;
		if (!ObjectUtils.NullOrUndefined(this.Phone))
			json['Phone'] = this.Phone;
		if (!ObjectUtils.NullOrUndefined(this.Email))
			json['Email'] = this.Email;
		if (!ObjectUtils.NullOrUndefined(this.Active))
			json['Active'] = this.Active;
		if (!ObjectUtils.NullOrUndefined(this.Blocked))
			json['Blocked'] = this.Blocked;
		if (!ObjectUtils.NullOrUndefined(this.Password))
			json['Password'] = this.Password;
		if (!ObjectUtils.NullOrUndefined(this.Timestamp))
			json['Timestamp'] = DateUtils.ToDateServer(this.Timestamp);
		if (navigationProperties)
		{
		}

		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: any): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Name = json['Name'];
			this.Phone = json['Phone'];
			this.Email = json['Email'];
			this.Active = json['Active'];
			this.Blocked = json['Blocked'];
			this.Password = json['Password'];
			this.Timestamp = DateUtils.FromDateServer(json['Timestamp']);
		}
	}
}

/**
 * Implementations of the `UserFilter` class
 */
export class UserFilter extends Crudable
{
	public static FromJson(json: Object): UserFilter
	{
		const item: UserFilter = new UserFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public Name: string = null,
		public Phone: string = null,
		public Email: string = null,
		public Password: string = null,
		public Active: number = null,
		public Blocked: number = null,
		public Timestamp: Date = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`UserFilter`, `userFilter`, CrudableTableDefinitionGender.Male, `User`, `Users`, true, true, true, true, true, true);

		this.SetColumnDefinition(UserColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(UserColumns.Name, `name`, `Name`, `Name`, `Names`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(UserColumns.Phone, `phone`, `Phone`, `Phone`, `Phones`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(UserColumns.Email, `email`, `Email`, `Email`, `Emails`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(UserColumns.Password, `password`, `Password`, `Password`, `Passwords`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(UserColumns.Active, `active`, `Active`, `Active`, `Actives`, ComponentTypeEnum.Number, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(UserColumns.Blocked, `blocked`, `Blocked`, `Blocked`, `Blockeds`, ComponentTypeEnum.Number, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(UserColumns.Timestamp, `timestamp`, `Timestamp`, `Timestamp`, `Timestamps`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
	}

	public Clone(): UserFilter
	{
		const clone: UserFilter = new UserFilter();
		clone.Id = this.Id;
		clone.Name = this.Name;
		clone.Phone = this.Phone;
		clone.Email = this.Email;
		clone.Active = this.Active;
		clone.Blocked = this.Blocked;
		clone.Password = this.Password;
		clone.Timestamp = this.Timestamp;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json['Name'] = this.Name;
		if (!ObjectUtils.NullOrUndefined(this.Phone))
			json['Phone'] = this.Phone;
		if (!ObjectUtils.NullOrUndefined(this.Email))
			json['Email'] = this.Email;
		if (!ObjectUtils.NullOrUndefined(this.Active))
			json['Active'] = this.Active;
		if (!ObjectUtils.NullOrUndefined(this.Blocked))
			json['Blocked'] = this.Blocked;
		if (!ObjectUtils.NullOrUndefined(this.Password))
			json['Password'] = this.Password;
		if (!ObjectUtils.NullOrUndefined(this.Timestamp))
			json['Timestamp'] = DateUtils.ToDateServer(this.Timestamp);


		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Name = json['Name'];
			this.Phone = json['Phone'];
			this.Email = json['Email'];
			this.Active = json['Active'];
			this.Blocked = json['Blocked'];
			this.Password = json['Password'];
			this.Timestamp = DateUtils.FromDateServer(json['Timestamp']);
		}
	}
}