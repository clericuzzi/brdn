﻿import { Crudable } from '@src/external/plugin-utils/business/models/crud/crudable.model';

// utils
import { DateUtils } from '@src/external/plugin-utils/utils/date-utils';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// Navigation Properties
import { User } from './user.model';

// components helpers
import { ComponentTypeEnum } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-definition.model';

export const LogErrorClass: string = `LogError`;
export const LogErrorTable: string = `log_error`;

/**
 * Column definitions for the `LogError` class
 */
export enum LogErrorColumns
{
	Id = 'Id',
	UserId = 'UserId',
	ClassName = 'ClassName',
	Timestamp = 'Timestamp',
	MethodName = 'MethodName',
	ExceptionType = 'ExceptionType',
	ExceptionMessage = 'ExceptionMessage',
	ExceptionStackTrace = 'ExceptionStackTrace',

	UserIdParent = 'UserIdParent',
}
export const LogErrorColumnsFilter: string[] = [];
export const LogErrorColumnsInsert: string[] = [LogErrorColumns.UserId, LogErrorColumns.ClassName, LogErrorColumns.MethodName, LogErrorColumns.ExceptionType, LogErrorColumns.ExceptionMessage, LogErrorColumns.ExceptionStackTrace, LogErrorColumns.Timestamp];
export const LogErrorColumnsUpdate: string[] = [LogErrorColumns.UserId, LogErrorColumns.ClassName, LogErrorColumns.MethodName, LogErrorColumns.ExceptionType, LogErrorColumns.ExceptionMessage, LogErrorColumns.ExceptionStackTrace, LogErrorColumns.Timestamp];

/**
 * Implementations of the `LogError` class
 */
export class LogError extends Crudable
{
	public UserIdParent: User;

	public static FromJson(json: any): LogError
	{
		const item: LogError = new LogError();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public UserId: number = null,
		public ClassName: string = null,
		public MethodName: string = null,
		public ExceptionType: string = null,
		public ExceptionMessage: string = null,
		public ExceptionStackTrace: string = null,
		public Timestamp: Date = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`LogError`, `log_error`, CrudableTableDefinitionGender.Male, `Log Error`, `Log Errors`, true, true, true, true, true, true);

		this.SetColumnDefinition(LogErrorColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(LogErrorColumns.UserId, `user_id`, `UserId`, `User Id`, `User Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(LogErrorColumns.ClassName, `class_name`, `ClassName`, `Class Name`, `Class Names`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(LogErrorColumns.MethodName, `method_name`, `MethodName`, `Method Name`, `Method Names`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(LogErrorColumns.ExceptionType, `exception_type`, `ExceptionType`, `Exception Type`, `Exception Types`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(LogErrorColumns.ExceptionMessage, `exception_message`, `ExceptionMessage`, `Exception Message`, `Exception Messages`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(LogErrorColumns.ExceptionStackTrace, `exception_stack_trace`, `ExceptionStackTrace`, `Exception Stack Trace`, `Exception Stack Traces`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(LogErrorColumns.Timestamp, `timestamp`, `Timestamp`, `Timestamp`, `Timestamps`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
	}

	public Clone(): LogError
	{
		const clone: LogError = new LogError();
		clone.Id = this.Id;
		clone.UserId = this.UserId;
		clone.ClassName = this.ClassName;
		clone.Timestamp = this.Timestamp;
		clone.MethodName = this.MethodName;
		clone.ExceptionType = this.ExceptionType;
		clone.ExceptionMessage = this.ExceptionMessage;
		clone.ExceptionStackTrace = this.ExceptionStackTrace;

		clone.UserIdParent = this.UserIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.UserIdParent == null ? '' : this.UserIdParent.ToString()}`;
	}

	public ToJson(navigationProperties: boolean = false): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.ClassName))
			json['ClassName'] = this.ClassName;
		if (!ObjectUtils.NullOrUndefined(this.MethodName))
			json['MethodName'] = this.MethodName;
		if (!ObjectUtils.NullOrUndefined(this.ExceptionType))
			json['ExceptionType'] = this.ExceptionType;
		if (!ObjectUtils.NullOrUndefined(this.ExceptionMessage))
			json['ExceptionMessage'] = this.ExceptionMessage;
		if (!ObjectUtils.NullOrUndefined(this.Timestamp))
			json['Timestamp'] = DateUtils.ToDateServer(this.Timestamp);
		if (!ObjectUtils.NullOrUndefined(this.ExceptionStackTrace))
			json['ExceptionStackTrace'] = this.ExceptionStackTrace;
		if (navigationProperties)
		{
			if (!ObjectUtils.NullOrUndefined(this.UserIdParent))
				json['UserIdParent'] = this.UserIdParent.ToJson();
		}

		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: any): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.UserId = json['UserId'];
			this.ClassName = json['ClassName'];
			this.MethodName = json['MethodName'];
			this.ExceptionType = json['ExceptionType'];
			this.ExceptionMessage = json['ExceptionMessage'];
			this.ExceptionStackTrace = json['ExceptionStackTrace'];
			this.Timestamp = DateUtils.FromDateServer(json['Timestamp']);

			if (!ObjectUtils.NullOrUndefined(json['UserIdParent']))
			{
				this.UserIdParent = new User();
				this.UserIdParent.ParseFromJson(json['UserIdParent']);
			}
		}
	}
}

/**
 * Implementations of the `LogErrorFilter` class
 */
export class LogErrorFilter extends Crudable
{
	public static FromJson(json: Object): LogErrorFilter
	{
		const item: LogErrorFilter = new LogErrorFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public UserId: number[] = null,
		public ClassName: string = null,
		public MethodName: string = null,
		public ExceptionType: string = null,
		public ExceptionMessage: string = null,
		public ExceptionStackTrace: string = null,
		public Timestamp: Date = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`LogErrorFilter`, `log_errorFilter`, CrudableTableDefinitionGender.Male, `Log Error`, `Log Errors`, true, true, true, true, true, true);

		this.SetColumnDefinition(LogErrorColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(LogErrorColumns.UserId, `user_id`, `UserId`, `User Id`, `User Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(LogErrorColumns.ClassName, `class_name`, `ClassName`, `Class Name`, `Class Names`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(LogErrorColumns.MethodName, `method_name`, `MethodName`, `Method Name`, `Method Names`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(LogErrorColumns.ExceptionType, `exception_type`, `ExceptionType`, `Exception Type`, `Exception Types`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(LogErrorColumns.ExceptionMessage, `exception_message`, `ExceptionMessage`, `Exception Message`, `Exception Messages`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(LogErrorColumns.ExceptionStackTrace, `exception_stack_trace`, `ExceptionStackTrace`, `Exception Stack Trace`, `Exception Stack Traces`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(LogErrorColumns.Timestamp, `timestamp`, `Timestamp`, `Timestamp`, `Timestamps`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
	}

	public Clone(): LogErrorFilter
	{
		const clone: LogErrorFilter = new LogErrorFilter();
		clone.Id = this.Id;
		clone.UserId = this.UserId;
		clone.ClassName = this.ClassName;
		clone.Timestamp = this.Timestamp;
		clone.MethodName = this.MethodName;
		clone.ExceptionType = this.ExceptionType;
		clone.ExceptionMessage = this.ExceptionMessage;
		clone.ExceptionStackTrace = this.ExceptionStackTrace;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.ClassName))
			json['ClassName'] = this.ClassName;
		if (!ObjectUtils.NullOrUndefined(this.MethodName))
			json['MethodName'] = this.MethodName;
		if (!ObjectUtils.NullOrUndefined(this.ExceptionType))
			json['ExceptionType'] = this.ExceptionType;
		if (!ObjectUtils.NullOrUndefined(this.ExceptionMessage))
			json['ExceptionMessage'] = this.ExceptionMessage;
		if (!ObjectUtils.NullOrUndefined(this.Timestamp))
			json['Timestamp'] = DateUtils.ToDateServer(this.Timestamp);
		if (!ObjectUtils.NullOrUndefined(this.ExceptionStackTrace))
			json['ExceptionStackTrace'] = this.ExceptionStackTrace;
		if (!ObjectUtils.NullOrUndefined(this.UserId))
			json['UserId'] = Array.isArray(this.UserId) ? this.UserId : [this.UserId];


		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.UserId = json['UserId'];
			this.ClassName = json['ClassName'];
			this.MethodName = json['MethodName'];
			this.ExceptionType = json['ExceptionType'];
			this.ExceptionMessage = json['ExceptionMessage'];
			this.ExceptionStackTrace = json['ExceptionStackTrace'];
			this.Timestamp = DateUtils.FromDateServer(json['Timestamp']);
		}
	}
}