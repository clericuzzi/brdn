﻿import { Crudable } from '@src/external/plugin-utils/business/models/crud/crudable.model';

// utils
import { DateUtils } from '@src/external/plugin-utils/utils/date-utils';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// Navigation Properties
import { Account } from './account.model';

// components helpers
import { ComponentTypeEnum } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-definition.model';

export const AccountPaymentHistoryClass: string = `AccountPaymentHistory`;
export const AccountPaymentHistoryTable: string = `account_payment_history`;

/**
 * Column definitions for the `AccountPaymentHistory` class
 */
export enum AccountPaymentHistoryColumns
{
	Id = 'Id',
	Year = 'Year',
	Month = 'Month',
	Payment = 'Payment',
	AccountId = 'AccountId',

	AccountIdParent = 'AccountIdParent',
}
export const AccountPaymentHistoryColumnsFilter: string[] = [];
export const AccountPaymentHistoryColumnsInsert: string[] = [AccountPaymentHistoryColumns.AccountId, AccountPaymentHistoryColumns.Year, AccountPaymentHistoryColumns.Month, AccountPaymentHistoryColumns.Payment];
export const AccountPaymentHistoryColumnsUpdate: string[] = [AccountPaymentHistoryColumns.AccountId, AccountPaymentHistoryColumns.Year, AccountPaymentHistoryColumns.Month, AccountPaymentHistoryColumns.Payment];

/**
 * Implementations of the `AccountPaymentHistory` class
 */
export class AccountPaymentHistory extends Crudable
{
	public AccountIdParent: Account;

	public static FromJson(json: any): AccountPaymentHistory
	{
		const item: AccountPaymentHistory = new AccountPaymentHistory();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public AccountId: number = null,
		public Year: number = null,
		public Month: number = null,
		public Payment: number = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`AccountPaymentHistory`, `account_payment_history`, CrudableTableDefinitionGender.Male, `Account Payment History`, `Account Payment Historys`, true, true, true, true, true, true);

		this.SetColumnDefinition(AccountPaymentHistoryColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountPaymentHistoryColumns.AccountId, `account_id`, `AccountId`, `Account Id`, `Account Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountPaymentHistoryColumns.Year, `year`, `Year`, `Year`, `Years`, ComponentTypeEnum.Number, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountPaymentHistoryColumns.Month, `month`, `Month`, `Month`, `Months`, ComponentTypeEnum.Number, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountPaymentHistoryColumns.Payment, `payment`, `Payment`, `Payment`, `Payments`, ComponentTypeEnum.Decimal, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): AccountPaymentHistory
	{
		const clone: AccountPaymentHistory = new AccountPaymentHistory();
		clone.Id = this.Id;
		clone.Year = this.Year;
		clone.Month = this.Month;
		clone.Payment = this.Payment;
		clone.AccountId = this.AccountId;

		clone.AccountIdParent = this.AccountIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.AccountIdParent == null ? '' : this.AccountIdParent.ToString()}`;
	}

	public ToJson(navigationProperties: boolean = false): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Year))
			json['Year'] = this.Year;
		if (!ObjectUtils.NullOrUndefined(this.Month))
			json['Month'] = this.Month;
		if (!ObjectUtils.NullOrUndefined(this.Payment))
			json['Payment'] = this.Payment;
		if (navigationProperties)
		{
			if (!ObjectUtils.NullOrUndefined(this.AccountIdParent))
				json['AccountIdParent'] = this.AccountIdParent.ToJson();
		}

		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: any): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Year = json['Year'];
			this.Month = json['Month'];
			this.Payment = json['Payment'];
			this.AccountId = json['AccountId'];

			if (!ObjectUtils.NullOrUndefined(json['AccountIdParent']))
			{
				this.AccountIdParent = new Account();
				this.AccountIdParent.ParseFromJson(json['AccountIdParent']);
			}
		}
	}
}

/**
 * Implementations of the `AccountPaymentHistoryFilter` class
 */
export class AccountPaymentHistoryFilter extends Crudable
{
	public static FromJson(json: Object): AccountPaymentHistoryFilter
	{
		const item: AccountPaymentHistoryFilter = new AccountPaymentHistoryFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public AccountId: number[] = null,
		public Year: number = null,
		public Month: number = null,
		public Payment: number = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`AccountPaymentHistoryFilter`, `account_payment_historyFilter`, CrudableTableDefinitionGender.Male, `Account Payment History`, `Account Payment Historys`, true, true, true, true, true, true);

		this.SetColumnDefinition(AccountPaymentHistoryColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountPaymentHistoryColumns.AccountId, `account_id`, `AccountId`, `Account Id`, `Account Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountPaymentHistoryColumns.Year, `year`, `Year`, `Year`, `Years`, ComponentTypeEnum.Number, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountPaymentHistoryColumns.Month, `month`, `Month`, `Month`, `Months`, ComponentTypeEnum.Number, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountPaymentHistoryColumns.Payment, `payment`, `Payment`, `Payment`, `Payments`, ComponentTypeEnum.Decimal, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): AccountPaymentHistoryFilter
	{
		const clone: AccountPaymentHistoryFilter = new AccountPaymentHistoryFilter();
		clone.Id = this.Id;
		clone.Year = this.Year;
		clone.Month = this.Month;
		clone.Payment = this.Payment;
		clone.AccountId = this.AccountId;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Year))
			json['Year'] = this.Year;
		if (!ObjectUtils.NullOrUndefined(this.Month))
			json['Month'] = this.Month;
		if (!ObjectUtils.NullOrUndefined(this.Payment))
			json['Payment'] = this.Payment;
		if (!ObjectUtils.NullOrUndefined(this.AccountId))
			json['AccountId'] = Array.isArray(this.AccountId) ? this.AccountId : [this.AccountId];


		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Year = json['Year'];
			this.Month = json['Month'];
			this.Payment = json['Payment'];
			this.AccountId = json['AccountId'];
		}
	}
}