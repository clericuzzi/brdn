﻿import { Crudable } from '@src/external/plugin-utils/business/models/crud/crudable.model';

// utils
import { DateUtils } from '@src/external/plugin-utils/utils/date-utils';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// Navigation Properties
import { Account } from './account.model';

// components helpers
import { ComponentTypeEnum } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-definition.model';

export const SecureZoneClass: string = `SecureZone`;
export const SecureZoneTable: string = `secure_zone`;

/**
 * Column definitions for the `SecureZone` class
 */
export enum SecureZoneColumns
{
	Id = 'Id',
	Name = 'Name',
	Latitude = 'Latitude',
	AccountId = 'AccountId',
	Longitude = 'Longitude',

	AccountIdParent = 'AccountIdParent',
}
export const SecureZoneColumnsFilter: string[] = [];
export const SecureZoneColumnsInsert: string[] = [SecureZoneColumns.AccountId, SecureZoneColumns.Name, SecureZoneColumns.Latitude, SecureZoneColumns.Longitude];
export const SecureZoneColumnsUpdate: string[] = [SecureZoneColumns.AccountId, SecureZoneColumns.Name, SecureZoneColumns.Latitude, SecureZoneColumns.Longitude];

/**
 * Implementations of the `SecureZone` class
 */
export class SecureZone extends Crudable
{
	public AccountIdParent: Account;

	public static FromJson(json: any): SecureZone
	{
		const item: SecureZone = new SecureZone();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public AccountId: number = null,
		public Name: string = null,
		public Latitude: number = null,
		public Longitude: number = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`SecureZone`, `secure_zone`, CrudableTableDefinitionGender.Male, `Secure Zone`, `Secure Zones`, true, true, true, true, true, true);

		this.SetColumnDefinition(SecureZoneColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(SecureZoneColumns.AccountId, `account_id`, `AccountId`, `Account Id`, `Account Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(SecureZoneColumns.Name, `name`, `Name`, `Name`, `Names`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(SecureZoneColumns.Latitude, `latitude`, `Latitude`, `Latitude`, `Latitudes`, ComponentTypeEnum.Decimal, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(SecureZoneColumns.Longitude, `longitude`, `Longitude`, `Longitude`, `Longitudes`, ComponentTypeEnum.Decimal, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): SecureZone
	{
		const clone: SecureZone = new SecureZone();
		clone.Id = this.Id;
		clone.Name = this.Name;
		clone.Latitude = this.Latitude;
		clone.AccountId = this.AccountId;
		clone.Longitude = this.Longitude;

		clone.AccountIdParent = this.AccountIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.AccountIdParent == null ? '' : this.AccountIdParent.ToString()}`;
	}

	public ToJson(navigationProperties: boolean = false): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json['Name'] = this.Name;
		if (!ObjectUtils.NullOrUndefined(this.Latitude))
			json['Latitude'] = this.Latitude;
		if (!ObjectUtils.NullOrUndefined(this.Longitude))
			json['Longitude'] = this.Longitude;
		if (navigationProperties)
		{
			if (!ObjectUtils.NullOrUndefined(this.AccountIdParent))
				json['AccountIdParent'] = this.AccountIdParent.ToJson();
		}

		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: any): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Name = json['Name'];
			this.Latitude = json['Latitude'];
			this.AccountId = json['AccountId'];
			this.Longitude = json['Longitude'];

			if (!ObjectUtils.NullOrUndefined(json['AccountIdParent']))
			{
				this.AccountIdParent = new Account();
				this.AccountIdParent.ParseFromJson(json['AccountIdParent']);
			}
		}
	}
}

/**
 * Implementations of the `SecureZoneFilter` class
 */
export class SecureZoneFilter extends Crudable
{
	public static FromJson(json: Object): SecureZoneFilter
	{
		const item: SecureZoneFilter = new SecureZoneFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public AccountId: number[] = null,
		public Name: string = null,
		public Latitude: number = null,
		public Longitude: number = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`SecureZoneFilter`, `secure_zoneFilter`, CrudableTableDefinitionGender.Male, `Secure Zone`, `Secure Zones`, true, true, true, true, true, true);

		this.SetColumnDefinition(SecureZoneColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(SecureZoneColumns.AccountId, `account_id`, `AccountId`, `Account Id`, `Account Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(SecureZoneColumns.Name, `name`, `Name`, `Name`, `Names`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(SecureZoneColumns.Latitude, `latitude`, `Latitude`, `Latitude`, `Latitudes`, ComponentTypeEnum.Decimal, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(SecureZoneColumns.Longitude, `longitude`, `Longitude`, `Longitude`, `Longitudes`, ComponentTypeEnum.Decimal, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): SecureZoneFilter
	{
		const clone: SecureZoneFilter = new SecureZoneFilter();
		clone.Id = this.Id;
		clone.Name = this.Name;
		clone.Latitude = this.Latitude;
		clone.AccountId = this.AccountId;
		clone.Longitude = this.Longitude;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json['Name'] = this.Name;
		if (!ObjectUtils.NullOrUndefined(this.Latitude))
			json['Latitude'] = this.Latitude;
		if (!ObjectUtils.NullOrUndefined(this.Longitude))
			json['Longitude'] = this.Longitude;
		if (!ObjectUtils.NullOrUndefined(this.AccountId))
			json['AccountId'] = Array.isArray(this.AccountId) ? this.AccountId : [this.AccountId];


		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Name = json['Name'];
			this.Latitude = json['Latitude'];
			this.AccountId = json['AccountId'];
			this.Longitude = json['Longitude'];
		}
	}
}