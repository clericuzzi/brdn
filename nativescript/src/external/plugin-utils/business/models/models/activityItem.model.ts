﻿import { Crudable } from '@src/external/plugin-utils/business/models/crud/crudable.model';

// utils
import { DateUtils } from '@src/external/plugin-utils/utils/date-utils';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// Navigation Properties
import { Activity } from './activity.model';
import { User } from './user.model';

// components helpers
import { ComponentTypeEnum } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-definition.model';

export const ActivityItemClass: string = `ActivityItem`;
export const ActivityItemTable: string = `activity_item`;

/**
 * Column definitions for the `ActivityItem` class
 */
export enum ActivityItemColumns
{
	Id = 'Id',
	UserId = 'UserId',
	Result = 'Result',
	EntityId = 'EntityId',
	Timestamp = 'Timestamp',
	ActivityId = 'ActivityId',
	FinishedIn = 'FinishedIn',

	UserIdParent = 'UserIdParent',
	ActivityIdParent = 'ActivityIdParent',
}
export const ActivityItemColumnsFilter: string[] = [];
export const ActivityItemColumnsInsert: string[] = [ActivityItemColumns.ActivityId, ActivityItemColumns.UserId, ActivityItemColumns.EntityId, ActivityItemColumns.FinishedIn, ActivityItemColumns.Result, ActivityItemColumns.Timestamp];
export const ActivityItemColumnsUpdate: string[] = [ActivityItemColumns.ActivityId, ActivityItemColumns.UserId, ActivityItemColumns.EntityId, ActivityItemColumns.FinishedIn, ActivityItemColumns.Result, ActivityItemColumns.Timestamp];

/**
 * Implementations of the `ActivityItem` class
 */
export class ActivityItem extends Crudable
{
	public ActivityIdParent: Activity;
	public UserIdParent: User;

	public static FromJson(json: any): ActivityItem
	{
		const item: ActivityItem = new ActivityItem();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public ActivityId: number = null,
		public UserId: number = null,
		public EntityId: number = null,
		public FinishedIn: Date = null,
		public Result: number = null,
		public Timestamp: Date = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`ActivityItem`, `activity_item`, CrudableTableDefinitionGender.Male, `Activity Item`, `Activity Items`, true, true, true, true, true, true);

		this.SetColumnDefinition(ActivityItemColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ActivityItemColumns.ActivityId, `activity_id`, `ActivityId`, `Activity Id`, `Activity Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ActivityItemColumns.UserId, `user_id`, `UserId`, `User Id`, `User Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ActivityItemColumns.EntityId, `entity_id`, `EntityId`, `Entity Id`, `Entity Ids`, ComponentTypeEnum.Number, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ActivityItemColumns.FinishedIn, `finished_in`, `FinishedIn`, `Finished In`, `Finished Ins`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, true);
		this.SetColumnDefinition(ActivityItemColumns.Result, `result`, `Result`, `Result`, `Results`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(ActivityItemColumns.Timestamp, `timestamp`, `Timestamp`, `Timestamp`, `Timestamps`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
	}

	public Clone(): ActivityItem
	{
		const clone: ActivityItem = new ActivityItem();
		clone.Id = this.Id;
		clone.UserId = this.UserId;
		clone.Result = this.Result;
		clone.EntityId = this.EntityId;
		clone.Timestamp = this.Timestamp;
		clone.ActivityId = this.ActivityId;
		clone.FinishedIn = this.FinishedIn;

		clone.UserIdParent = this.UserIdParent;
		clone.ActivityIdParent = this.ActivityIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.ActivityIdParent == null ? '' : this.ActivityIdParent.ToString()}`;
	}

	public ToJson(navigationProperties: boolean = false): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Result))
			json['Result'] = this.Result;
		if (!ObjectUtils.NullOrUndefined(this.EntityId))
			json['EntityId'] = this.EntityId;
		if (!ObjectUtils.NullOrUndefined(this.Timestamp))
			json['Timestamp'] = DateUtils.ToDateServer(this.Timestamp);
		if (!ObjectUtils.NullOrUndefined(this.FinishedIn))
			json['FinishedIn'] = DateUtils.ToDateServer(this.FinishedIn);
		if (navigationProperties)
		{
			if (!ObjectUtils.NullOrUndefined(this.ActivityIdParent))
				json['ActivityIdParent'] = this.ActivityIdParent.ToJson();
			if (!ObjectUtils.NullOrUndefined(this.UserIdParent))
				json['UserIdParent'] = this.UserIdParent.ToJson();
		}

		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: any): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.UserId = json['UserId'];
			this.Result = json['Result'];
			this.EntityId = json['EntityId'];
			this.ActivityId = json['ActivityId'];
			this.Timestamp = DateUtils.FromDateServer(json['Timestamp']);
			this.FinishedIn = DateUtils.FromDateServer(json['FinishedIn']);

			if (!ObjectUtils.NullOrUndefined(json['ActivityIdParent']))
			{
				this.ActivityIdParent = new Activity();
				this.ActivityIdParent.ParseFromJson(json['ActivityIdParent']);
			}
			if (!ObjectUtils.NullOrUndefined(json['UserIdParent']))
			{
				this.UserIdParent = new User();
				this.UserIdParent.ParseFromJson(json['UserIdParent']);
			}
		}
	}
}

/**
 * Implementations of the `ActivityItemFilter` class
 */
export class ActivityItemFilter extends Crudable
{
	public static FromJson(json: Object): ActivityItemFilter
	{
		const item: ActivityItemFilter = new ActivityItemFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public ActivityId: number[] = null,
		public UserId: number[] = null,
		public EntityId: number = null,
		public FinishedIn: Date = null,
		public Result: number = null,
		public Timestamp: Date = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`ActivityItemFilter`, `activity_itemFilter`, CrudableTableDefinitionGender.Male, `Activity Item`, `Activity Items`, true, true, true, true, true, true);

		this.SetColumnDefinition(ActivityItemColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ActivityItemColumns.ActivityId, `activity_id`, `ActivityId`, `Activity Id`, `Activity Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ActivityItemColumns.UserId, `user_id`, `UserId`, `User Id`, `User Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ActivityItemColumns.EntityId, `entity_id`, `EntityId`, `Entity Id`, `Entity Ids`, ComponentTypeEnum.Number, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ActivityItemColumns.FinishedIn, `finished_in`, `FinishedIn`, `Finished In`, `Finished Ins`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, true);
		this.SetColumnDefinition(ActivityItemColumns.Result, `result`, `Result`, `Result`, `Results`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(ActivityItemColumns.Timestamp, `timestamp`, `Timestamp`, `Timestamp`, `Timestamps`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
	}

	public Clone(): ActivityItemFilter
	{
		const clone: ActivityItemFilter = new ActivityItemFilter();
		clone.Id = this.Id;
		clone.UserId = this.UserId;
		clone.Result = this.Result;
		clone.EntityId = this.EntityId;
		clone.Timestamp = this.Timestamp;
		clone.ActivityId = this.ActivityId;
		clone.FinishedIn = this.FinishedIn;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Result))
			json['Result'] = this.Result;
		if (!ObjectUtils.NullOrUndefined(this.EntityId))
			json['EntityId'] = this.EntityId;
		if (!ObjectUtils.NullOrUndefined(this.Timestamp))
			json['Timestamp'] = DateUtils.ToDateServer(this.Timestamp);
		if (!ObjectUtils.NullOrUndefined(this.FinishedIn))
			json['FinishedIn'] = DateUtils.ToDateServer(this.FinishedIn);
		if (!ObjectUtils.NullOrUndefined(this.UserId))
			json['UserId'] = Array.isArray(this.UserId) ? this.UserId : [this.UserId];
		if (!ObjectUtils.NullOrUndefined(this.ActivityId))
			json['ActivityId'] = Array.isArray(this.ActivityId) ? this.ActivityId : [this.ActivityId];


		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.UserId = json['UserId'];
			this.Result = json['Result'];
			this.EntityId = json['EntityId'];
			this.ActivityId = json['ActivityId'];
			this.Timestamp = DateUtils.FromDateServer(json['Timestamp']);
			this.FinishedIn = DateUtils.FromDateServer(json['FinishedIn']);
		}
	}
}