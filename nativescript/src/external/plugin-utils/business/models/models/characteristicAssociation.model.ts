﻿import { Crudable } from '@src/external/plugin-utils/business/models/crud/crudable.model';

// utils
import { DateUtils } from '@src/external/plugin-utils/utils/date-utils';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// Navigation Properties
import { Characteristic } from './characteristic.model';

// components helpers
import { ComponentTypeEnum } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-definition.model';

export const CharacteristicAssociationClass: string = `CharacteristicAssociation`;
export const CharacteristicAssociationTable: string = `characteristic_association`;

/**
 * Column definitions for the `CharacteristicAssociation` class
 */
export enum CharacteristicAssociationColumns
{
	Id = 'Id',
	EntityId = 'EntityId',
	Timestamp = 'Timestamp',
	CharacteristicId = 'CharacteristicId',

	CharacteristicIdParent = 'CharacteristicIdParent',
}
export const CharacteristicAssociationColumnsFilter: string[] = [];
export const CharacteristicAssociationColumnsInsert: string[] = [CharacteristicAssociationColumns.CharacteristicId, CharacteristicAssociationColumns.EntityId, CharacteristicAssociationColumns.Timestamp];
export const CharacteristicAssociationColumnsUpdate: string[] = [CharacteristicAssociationColumns.CharacteristicId, CharacteristicAssociationColumns.EntityId, CharacteristicAssociationColumns.Timestamp];

/**
 * Implementations of the `CharacteristicAssociation` class
 */
export class CharacteristicAssociation extends Crudable
{
	public CharacteristicIdParent: Characteristic;

	public static FromJson(json: any): CharacteristicAssociation
	{
		const item: CharacteristicAssociation = new CharacteristicAssociation();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public CharacteristicId: number = null,
		public EntityId: number = null,
		public Timestamp: Date = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`CharacteristicAssociation`, `characteristic_association`, CrudableTableDefinitionGender.Male, `Characteristic Association`, `Characteristic Associations`, true, true, true, true, true, true);

		this.SetColumnDefinition(CharacteristicAssociationColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CharacteristicAssociationColumns.CharacteristicId, `characteristic_id`, `CharacteristicId`, `Characteristic Id`, `Characteristic Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CharacteristicAssociationColumns.EntityId, `entity_id`, `EntityId`, `Entity Id`, `Entity Ids`, ComponentTypeEnum.Number, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CharacteristicAssociationColumns.Timestamp, `timestamp`, `Timestamp`, `Timestamp`, `Timestamps`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
	}

	public Clone(): CharacteristicAssociation
	{
		const clone: CharacteristicAssociation = new CharacteristicAssociation();
		clone.Id = this.Id;
		clone.EntityId = this.EntityId;
		clone.Timestamp = this.Timestamp;
		clone.CharacteristicId = this.CharacteristicId;

		clone.CharacteristicIdParent = this.CharacteristicIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.CharacteristicIdParent == null ? '' : this.CharacteristicIdParent.ToString()}`;
	}

	public ToJson(navigationProperties: boolean = false): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.EntityId))
			json['EntityId'] = this.EntityId;
		if (!ObjectUtils.NullOrUndefined(this.Timestamp))
			json['Timestamp'] = DateUtils.ToDateServer(this.Timestamp);
		if (navigationProperties)
		{
			if (!ObjectUtils.NullOrUndefined(this.CharacteristicIdParent))
				json['CharacteristicIdParent'] = this.CharacteristicIdParent.ToJson();
		}

		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: any): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.EntityId = json['EntityId'];
			this.CharacteristicId = json['CharacteristicId'];
			this.Timestamp = DateUtils.FromDateServer(json['Timestamp']);

			if (!ObjectUtils.NullOrUndefined(json['CharacteristicIdParent']))
			{
				this.CharacteristicIdParent = new Characteristic();
				this.CharacteristicIdParent.ParseFromJson(json['CharacteristicIdParent']);
			}
		}
	}
}

/**
 * Implementations of the `CharacteristicAssociationFilter` class
 */
export class CharacteristicAssociationFilter extends Crudable
{
	public static FromJson(json: Object): CharacteristicAssociationFilter
	{
		const item: CharacteristicAssociationFilter = new CharacteristicAssociationFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public CharacteristicId: number[] = null,
		public EntityId: number = null,
		public Timestamp: Date = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`CharacteristicAssociationFilter`, `characteristic_associationFilter`, CrudableTableDefinitionGender.Male, `Characteristic Association`, `Characteristic Associations`, true, true, true, true, true, true);

		this.SetColumnDefinition(CharacteristicAssociationColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CharacteristicAssociationColumns.CharacteristicId, `characteristic_id`, `CharacteristicId`, `Characteristic Id`, `Characteristic Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CharacteristicAssociationColumns.EntityId, `entity_id`, `EntityId`, `Entity Id`, `Entity Ids`, ComponentTypeEnum.Number, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CharacteristicAssociationColumns.Timestamp, `timestamp`, `Timestamp`, `Timestamp`, `Timestamps`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
	}

	public Clone(): CharacteristicAssociationFilter
	{
		const clone: CharacteristicAssociationFilter = new CharacteristicAssociationFilter();
		clone.Id = this.Id;
		clone.EntityId = this.EntityId;
		clone.Timestamp = this.Timestamp;
		clone.CharacteristicId = this.CharacteristicId;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.EntityId))
			json['EntityId'] = this.EntityId;
		if (!ObjectUtils.NullOrUndefined(this.Timestamp))
			json['Timestamp'] = DateUtils.ToDateServer(this.Timestamp);
		if (!ObjectUtils.NullOrUndefined(this.CharacteristicId))
			json['CharacteristicId'] = Array.isArray(this.CharacteristicId) ? this.CharacteristicId : [this.CharacteristicId];


		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.EntityId = json['EntityId'];
			this.CharacteristicId = json['CharacteristicId'];
			this.Timestamp = DateUtils.FromDateServer(json['Timestamp']);
		}
	}
}