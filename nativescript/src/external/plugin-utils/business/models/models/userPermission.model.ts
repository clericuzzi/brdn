﻿import { Crudable } from '@src/external/plugin-utils/business/models/crud/crudable.model';

// utils
import { DateUtils } from '@src/external/plugin-utils/utils/date-utils';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// Navigation Properties
import { User } from './user.model';
import { Permission } from './permission.model';

// components helpers
import { ComponentTypeEnum } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-definition.model';

export const UserPermissionClass: string = `UserPermission`;
export const UserPermissionTable: string = `user_permission`;

/**
 * Column definitions for the `UserPermission` class
 */
export enum UserPermissionColumns
{
	Id = 'Id',
	UserId = 'UserId',
	PermissionId = 'PermissionId',

	UserIdParent = 'UserIdParent',
	PermissionIdParent = 'PermissionIdParent',
}
export const UserPermissionColumnsFilter: string[] = [];
export const UserPermissionColumnsInsert: string[] = [UserPermissionColumns.UserId, UserPermissionColumns.PermissionId];
export const UserPermissionColumnsUpdate: string[] = [UserPermissionColumns.UserId, UserPermissionColumns.PermissionId];

/**
 * Implementations of the `UserPermission` class
 */
export class UserPermission extends Crudable
{
	public UserIdParent: User;
	public PermissionIdParent: Permission;

	public static FromJson(json: any): UserPermission
	{
		const item: UserPermission = new UserPermission();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public UserId: number = null,
		public PermissionId: number = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`UserPermission`, `user_permission`, CrudableTableDefinitionGender.Male, `User Permission`, `User Permissions`, true, true, true, true, true, true);

		this.SetColumnDefinition(UserPermissionColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(UserPermissionColumns.UserId, `user_id`, `UserId`, `User Id`, `User Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(UserPermissionColumns.PermissionId, `permission_id`, `PermissionId`, `Permission Id`, `Permission Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): UserPermission
	{
		const clone: UserPermission = new UserPermission();
		clone.Id = this.Id;
		clone.UserId = this.UserId;
		clone.PermissionId = this.PermissionId;

		clone.UserIdParent = this.UserIdParent;
		clone.PermissionIdParent = this.PermissionIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.UserIdParent == null ? '' : this.UserIdParent.ToString()}`;
	}

	public ToJson(navigationProperties: boolean = false): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (navigationProperties)
		{
			if (!ObjectUtils.NullOrUndefined(this.UserIdParent))
				json['UserIdParent'] = this.UserIdParent.ToJson();
			if (!ObjectUtils.NullOrUndefined(this.PermissionIdParent))
				json['PermissionIdParent'] = this.PermissionIdParent.ToJson();
		}

		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: any): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.UserId = json['UserId'];
			this.PermissionId = json['PermissionId'];

			if (!ObjectUtils.NullOrUndefined(json['UserIdParent']))
			{
				this.UserIdParent = new User();
				this.UserIdParent.ParseFromJson(json['UserIdParent']);
			}
			if (!ObjectUtils.NullOrUndefined(json['PermissionIdParent']))
			{
				this.PermissionIdParent = new Permission();
				this.PermissionIdParent.ParseFromJson(json['PermissionIdParent']);
			}
		}
	}
}

/**
 * Implementations of the `UserPermissionFilter` class
 */
export class UserPermissionFilter extends Crudable
{
	public static FromJson(json: Object): UserPermissionFilter
	{
		const item: UserPermissionFilter = new UserPermissionFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public UserId: number[] = null,
		public PermissionId: number[] = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`UserPermissionFilter`, `user_permissionFilter`, CrudableTableDefinitionGender.Male, `User Permission`, `User Permissions`, true, true, true, true, true, true);

		this.SetColumnDefinition(UserPermissionColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(UserPermissionColumns.UserId, `user_id`, `UserId`, `User Id`, `User Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(UserPermissionColumns.PermissionId, `permission_id`, `PermissionId`, `Permission Id`, `Permission Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): UserPermissionFilter
	{
		const clone: UserPermissionFilter = new UserPermissionFilter();
		clone.Id = this.Id;
		clone.UserId = this.UserId;
		clone.PermissionId = this.PermissionId;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.UserId))
			json['UserId'] = Array.isArray(this.UserId) ? this.UserId : [this.UserId];
		if (!ObjectUtils.NullOrUndefined(this.PermissionId))
			json['PermissionId'] = Array.isArray(this.PermissionId) ? this.PermissionId : [this.PermissionId];


		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.UserId = json['UserId'];
			this.PermissionId = json['PermissionId'];
		}
	}
}