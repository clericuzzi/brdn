﻿import { Crudable } from '@src/external/plugin-utils/business/models/crud/crudable.model';

// utils
import { DateUtils } from '@src/external/plugin-utils/utils/date-utils';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// Navigation Properties
import { User } from './user.model';

// components helpers
import { ComponentTypeEnum } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-definition.model';

export const LogDatasetChangeClass: string = `LogDatasetChange`;
export const LogDatasetChangeTable: string = `log_dataset_change`;

/**
 * Column definitions for the `LogDatasetChange` class
 */
export enum LogDatasetChangeColumns
{
	Id = 'Id',
	UserId = 'UserId',
	Action = 'Action',
	Timestamp = 'Timestamp',
	EntityType = 'EntityType',
	EntitySummary = 'EntitySummary',

	UserIdParent = 'UserIdParent',
}
export const LogDatasetChangeColumnsFilter: string[] = [];
export const LogDatasetChangeColumnsInsert: string[] = [LogDatasetChangeColumns.UserId, LogDatasetChangeColumns.Action, LogDatasetChangeColumns.EntityType, LogDatasetChangeColumns.EntitySummary, LogDatasetChangeColumns.Timestamp];
export const LogDatasetChangeColumnsUpdate: string[] = [LogDatasetChangeColumns.UserId, LogDatasetChangeColumns.Action, LogDatasetChangeColumns.EntityType, LogDatasetChangeColumns.EntitySummary, LogDatasetChangeColumns.Timestamp];

/**
 * Implementations of the `LogDatasetChange` class
 */
export class LogDatasetChange extends Crudable
{
	public UserIdParent: User;

	public static FromJson(json: any): LogDatasetChange
	{
		const item: LogDatasetChange = new LogDatasetChange();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public UserId: number = null,
		public Action: string = null,
		public EntityType: string = null,
		public EntitySummary: string = null,
		public Timestamp: Date = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`LogDatasetChange`, `log_dataset_change`, CrudableTableDefinitionGender.Male, `Log Dataset Change`, `Log Dataset Changes`, true, true, true, true, true, true);

		this.SetColumnDefinition(LogDatasetChangeColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(LogDatasetChangeColumns.UserId, `user_id`, `UserId`, `User Id`, `User Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(LogDatasetChangeColumns.Action, `action`, `Action`, `Action`, `Actions`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(LogDatasetChangeColumns.EntityType, `entity_type`, `EntityType`, `Entity Type`, `Entity Types`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(LogDatasetChangeColumns.EntitySummary, `entity_summary`, `EntitySummary`, `Entity Summary`, `Entity Summarys`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(LogDatasetChangeColumns.Timestamp, `timestamp`, `Timestamp`, `Timestamp`, `Timestamps`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
	}

	public Clone(): LogDatasetChange
	{
		const clone: LogDatasetChange = new LogDatasetChange();
		clone.Id = this.Id;
		clone.UserId = this.UserId;
		clone.Action = this.Action;
		clone.Timestamp = this.Timestamp;
		clone.EntityType = this.EntityType;
		clone.EntitySummary = this.EntitySummary;

		clone.UserIdParent = this.UserIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.UserIdParent == null ? '' : this.UserIdParent.ToString()}`;
	}

	public ToJson(navigationProperties: boolean = false): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Action))
			json['Action'] = this.Action;
		if (!ObjectUtils.NullOrUndefined(this.EntityType))
			json['EntityType'] = this.EntityType;
		if (!ObjectUtils.NullOrUndefined(this.EntitySummary))
			json['EntitySummary'] = this.EntitySummary;
		if (!ObjectUtils.NullOrUndefined(this.Timestamp))
			json['Timestamp'] = DateUtils.ToDateServer(this.Timestamp);
		if (navigationProperties)
		{
			if (!ObjectUtils.NullOrUndefined(this.UserIdParent))
				json['UserIdParent'] = this.UserIdParent.ToJson();
		}

		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: any): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.UserId = json['UserId'];
			this.Action = json['Action'];
			this.EntityType = json['EntityType'];
			this.EntitySummary = json['EntitySummary'];
			this.Timestamp = DateUtils.FromDateServer(json['Timestamp']);

			if (!ObjectUtils.NullOrUndefined(json['UserIdParent']))
			{
				this.UserIdParent = new User();
				this.UserIdParent.ParseFromJson(json['UserIdParent']);
			}
		}
	}
}

/**
 * Implementations of the `LogDatasetChangeFilter` class
 */
export class LogDatasetChangeFilter extends Crudable
{
	public static FromJson(json: Object): LogDatasetChangeFilter
	{
		const item: LogDatasetChangeFilter = new LogDatasetChangeFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public UserId: number[] = null,
		public Action: string = null,
		public EntityType: string = null,
		public EntitySummary: string = null,
		public Timestamp: Date = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`LogDatasetChangeFilter`, `log_dataset_changeFilter`, CrudableTableDefinitionGender.Male, `Log Dataset Change`, `Log Dataset Changes`, true, true, true, true, true, true);

		this.SetColumnDefinition(LogDatasetChangeColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(LogDatasetChangeColumns.UserId, `user_id`, `UserId`, `User Id`, `User Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(LogDatasetChangeColumns.Action, `action`, `Action`, `Action`, `Actions`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(LogDatasetChangeColumns.EntityType, `entity_type`, `EntityType`, `Entity Type`, `Entity Types`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(LogDatasetChangeColumns.EntitySummary, `entity_summary`, `EntitySummary`, `Entity Summary`, `Entity Summarys`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(LogDatasetChangeColumns.Timestamp, `timestamp`, `Timestamp`, `Timestamp`, `Timestamps`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
	}

	public Clone(): LogDatasetChangeFilter
	{
		const clone: LogDatasetChangeFilter = new LogDatasetChangeFilter();
		clone.Id = this.Id;
		clone.UserId = this.UserId;
		clone.Action = this.Action;
		clone.Timestamp = this.Timestamp;
		clone.EntityType = this.EntityType;
		clone.EntitySummary = this.EntitySummary;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Action))
			json['Action'] = this.Action;
		if (!ObjectUtils.NullOrUndefined(this.EntityType))
			json['EntityType'] = this.EntityType;
		if (!ObjectUtils.NullOrUndefined(this.EntitySummary))
			json['EntitySummary'] = this.EntitySummary;
		if (!ObjectUtils.NullOrUndefined(this.Timestamp))
			json['Timestamp'] = DateUtils.ToDateServer(this.Timestamp);
		if (!ObjectUtils.NullOrUndefined(this.UserId))
			json['UserId'] = Array.isArray(this.UserId) ? this.UserId : [this.UserId];


		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.UserId = json['UserId'];
			this.Action = json['Action'];
			this.EntityType = json['EntityType'];
			this.EntitySummary = json['EntitySummary'];
			this.Timestamp = DateUtils.FromDateServer(json['Timestamp']);
		}
	}
}