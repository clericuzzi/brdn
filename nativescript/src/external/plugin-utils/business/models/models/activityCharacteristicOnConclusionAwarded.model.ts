﻿import { Crudable } from '@src/external/plugin-utils/business/models/crud/crudable.model';

// utils
import { DateUtils } from '@src/external/plugin-utils/utils/date-utils';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// Navigation Properties
import { Activity } from './activity.model';
import { Characteristic } from './characteristic.model';

// components helpers
import { ComponentTypeEnum } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-definition.model';

export const ActivityCharacteristicOnConclusionAwardedClass: string = `ActivityCharacteristicOnConclusionAwarded`;
export const ActivityCharacteristicOnConclusionAwardedTable: string = `activity_characteristic_on_conclusion_awarded`;

/**
 * Column definitions for the `ActivityCharacteristicOnConclusionAwarded` class
 */
export enum ActivityCharacteristicOnConclusionAwardedColumns
{
	Id = 'Id',
	ActivityId = 'ActivityId',
	CharacteristicId = 'CharacteristicId',

	ActivityIdParent = 'ActivityIdParent',
	CharacteristicIdParent = 'CharacteristicIdParent',
}
export const ActivityCharacteristicOnConclusionAwardedColumnsFilter: string[] = [];
export const ActivityCharacteristicOnConclusionAwardedColumnsInsert: string[] = [ActivityCharacteristicOnConclusionAwardedColumns.ActivityId, ActivityCharacteristicOnConclusionAwardedColumns.CharacteristicId];
export const ActivityCharacteristicOnConclusionAwardedColumnsUpdate: string[] = [ActivityCharacteristicOnConclusionAwardedColumns.ActivityId, ActivityCharacteristicOnConclusionAwardedColumns.CharacteristicId];

/**
 * Implementations of the `ActivityCharacteristicOnConclusionAwarded` class
 */
export class ActivityCharacteristicOnConclusionAwarded extends Crudable
{
	public ActivityIdParent: Activity;
	public CharacteristicIdParent: Characteristic;

	public static FromJson(json: any): ActivityCharacteristicOnConclusionAwarded
	{
		const item: ActivityCharacteristicOnConclusionAwarded = new ActivityCharacteristicOnConclusionAwarded();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public ActivityId: number = null,
		public CharacteristicId: number = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`ActivityCharacteristicOnConclusionAwarded`, `activity_characteristic_on_conclusion_awarded`, CrudableTableDefinitionGender.Male, `Activity Characteristic On Conclusion Awarded`, `Activity Characteristic On Conclusion Awardeds`, true, true, true, true, true, true);

		this.SetColumnDefinition(ActivityCharacteristicOnConclusionAwardedColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ActivityCharacteristicOnConclusionAwardedColumns.ActivityId, `activity_id`, `ActivityId`, `Activity Id`, `Activity Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ActivityCharacteristicOnConclusionAwardedColumns.CharacteristicId, `characteristic_id`, `CharacteristicId`, `Characteristic Id`, `Characteristic Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): ActivityCharacteristicOnConclusionAwarded
	{
		const clone: ActivityCharacteristicOnConclusionAwarded = new ActivityCharacteristicOnConclusionAwarded();
		clone.Id = this.Id;
		clone.ActivityId = this.ActivityId;
		clone.CharacteristicId = this.CharacteristicId;

		clone.ActivityIdParent = this.ActivityIdParent;
		clone.CharacteristicIdParent = this.CharacteristicIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.ActivityIdParent == null ? '' : this.ActivityIdParent.ToString()}`;
	}

	public ToJson(navigationProperties: boolean = false): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (navigationProperties)
		{
			if (!ObjectUtils.NullOrUndefined(this.ActivityIdParent))
				json['ActivityIdParent'] = this.ActivityIdParent.ToJson();
			if (!ObjectUtils.NullOrUndefined(this.CharacteristicIdParent))
				json['CharacteristicIdParent'] = this.CharacteristicIdParent.ToJson();
		}

		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: any): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.ActivityId = json['ActivityId'];
			this.CharacteristicId = json['CharacteristicId'];

			if (!ObjectUtils.NullOrUndefined(json['ActivityIdParent']))
			{
				this.ActivityIdParent = new Activity();
				this.ActivityIdParent.ParseFromJson(json['ActivityIdParent']);
			}
			if (!ObjectUtils.NullOrUndefined(json['CharacteristicIdParent']))
			{
				this.CharacteristicIdParent = new Characteristic();
				this.CharacteristicIdParent.ParseFromJson(json['CharacteristicIdParent']);
			}
		}
	}
}

/**
 * Implementations of the `ActivityCharacteristicOnConclusionAwardedFilter` class
 */
export class ActivityCharacteristicOnConclusionAwardedFilter extends Crudable
{
	public static FromJson(json: Object): ActivityCharacteristicOnConclusionAwardedFilter
	{
		const item: ActivityCharacteristicOnConclusionAwardedFilter = new ActivityCharacteristicOnConclusionAwardedFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public ActivityId: number[] = null,
		public CharacteristicId: number[] = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`ActivityCharacteristicOnConclusionAwardedFilter`, `activity_characteristic_on_conclusion_awardedFilter`, CrudableTableDefinitionGender.Male, `Activity Characteristic On Conclusion Awarded`, `Activity Characteristic On Conclusion Awardeds`, true, true, true, true, true, true);

		this.SetColumnDefinition(ActivityCharacteristicOnConclusionAwardedColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ActivityCharacteristicOnConclusionAwardedColumns.ActivityId, `activity_id`, `ActivityId`, `Activity Id`, `Activity Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ActivityCharacteristicOnConclusionAwardedColumns.CharacteristicId, `characteristic_id`, `CharacteristicId`, `Characteristic Id`, `Characteristic Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): ActivityCharacteristicOnConclusionAwardedFilter
	{
		const clone: ActivityCharacteristicOnConclusionAwardedFilter = new ActivityCharacteristicOnConclusionAwardedFilter();
		clone.Id = this.Id;
		clone.ActivityId = this.ActivityId;
		clone.CharacteristicId = this.CharacteristicId;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.ActivityId))
			json['ActivityId'] = Array.isArray(this.ActivityId) ? this.ActivityId : [this.ActivityId];
		if (!ObjectUtils.NullOrUndefined(this.CharacteristicId))
			json['CharacteristicId'] = Array.isArray(this.CharacteristicId) ? this.CharacteristicId : [this.CharacteristicId];


		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.ActivityId = json['ActivityId'];
			this.CharacteristicId = json['CharacteristicId'];
		}
	}
}