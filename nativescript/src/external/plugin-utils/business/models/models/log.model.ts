﻿import { Crudable } from '@src/external/plugin-utils/business/models/crud/crudable.model';

// utils
import { DateUtils } from '@src/external/plugin-utils/utils/date-utils';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// Navigation Properties
import { User } from './user.model';

// components helpers
import { ComponentTypeEnum } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-definition.model';

export const LogClass: string = `Log`;
export const LogTable: string = `log`;

/**
 * Column definitions for the `Log` class
 */
export enum LogColumns
{
	Id = 'Id',
	UserId = 'UserId',
	Action = 'Action',
	TargetId = 'TargetId',
	Timestamp = 'Timestamp',

	UserIdParent = 'UserIdParent',
}
export const LogColumnsFilter: string[] = [];
export const LogColumnsInsert: string[] = [LogColumns.UserId, LogColumns.TargetId, LogColumns.Action, LogColumns.Timestamp];
export const LogColumnsUpdate: string[] = [LogColumns.UserId, LogColumns.TargetId, LogColumns.Action, LogColumns.Timestamp];

/**
 * Implementations of the `Log` class
 */
export class Log extends Crudable
{
	public UserIdParent: User;

	public static FromJson(json: any): Log
	{
		const item: Log = new Log();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public UserId: number = null,
		public TargetId: number = null,
		public Action: string = null,
		public Timestamp: Date = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`Log`, `log`, CrudableTableDefinitionGender.Male, `Log`, `Logs`, true, true, true, true, true, true);

		this.SetColumnDefinition(LogColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(LogColumns.UserId, `user_id`, `UserId`, `User Id`, `User Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(LogColumns.TargetId, `target_id`, `TargetId`, `Target Id`, `Target Ids`, ComponentTypeEnum.Number, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(LogColumns.Action, `action`, `Action`, `Action`, `Actions`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(LogColumns.Timestamp, `timestamp`, `Timestamp`, `Timestamp`, `Timestamps`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
	}

	public Clone(): Log
	{
		const clone: Log = new Log();
		clone.Id = this.Id;
		clone.UserId = this.UserId;
		clone.Action = this.Action;
		clone.TargetId = this.TargetId;
		clone.Timestamp = this.Timestamp;

		clone.UserIdParent = this.UserIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.UserIdParent == null ? '' : this.UserIdParent.ToString()}`;
	}

	public ToJson(navigationProperties: boolean = false): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Action))
			json['Action'] = this.Action;
		if (!ObjectUtils.NullOrUndefined(this.TargetId))
			json['TargetId'] = this.TargetId;
		if (!ObjectUtils.NullOrUndefined(this.Timestamp))
			json['Timestamp'] = DateUtils.ToDateServer(this.Timestamp);
		if (navigationProperties)
		{
			if (!ObjectUtils.NullOrUndefined(this.UserIdParent))
				json['UserIdParent'] = this.UserIdParent.ToJson();
		}

		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: any): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.UserId = json['UserId'];
			this.Action = json['Action'];
			this.TargetId = json['TargetId'];
			this.Timestamp = DateUtils.FromDateServer(json['Timestamp']);

			if (!ObjectUtils.NullOrUndefined(json['UserIdParent']))
			{
				this.UserIdParent = new User();
				this.UserIdParent.ParseFromJson(json['UserIdParent']);
			}
		}
	}
}

/**
 * Implementations of the `LogFilter` class
 */
export class LogFilter extends Crudable
{
	public static FromJson(json: Object): LogFilter
	{
		const item: LogFilter = new LogFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public UserId: number[] = null,
		public TargetId: number = null,
		public Action: string = null,
		public Timestamp: Date = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`LogFilter`, `logFilter`, CrudableTableDefinitionGender.Male, `Log`, `Logs`, true, true, true, true, true, true);

		this.SetColumnDefinition(LogColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(LogColumns.UserId, `user_id`, `UserId`, `User Id`, `User Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(LogColumns.TargetId, `target_id`, `TargetId`, `Target Id`, `Target Ids`, ComponentTypeEnum.Number, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(LogColumns.Action, `action`, `Action`, `Action`, `Actions`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(LogColumns.Timestamp, `timestamp`, `Timestamp`, `Timestamp`, `Timestamps`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
	}

	public Clone(): LogFilter
	{
		const clone: LogFilter = new LogFilter();
		clone.Id = this.Id;
		clone.UserId = this.UserId;
		clone.Action = this.Action;
		clone.TargetId = this.TargetId;
		clone.Timestamp = this.Timestamp;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Action))
			json['Action'] = this.Action;
		if (!ObjectUtils.NullOrUndefined(this.TargetId))
			json['TargetId'] = this.TargetId;
		if (!ObjectUtils.NullOrUndefined(this.Timestamp))
			json['Timestamp'] = DateUtils.ToDateServer(this.Timestamp);
		if (!ObjectUtils.NullOrUndefined(this.UserId))
			json['UserId'] = Array.isArray(this.UserId) ? this.UserId : [this.UserId];


		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.UserId = json['UserId'];
			this.Action = json['Action'];
			this.TargetId = json['TargetId'];
			this.Timestamp = DateUtils.FromDateServer(json['Timestamp']);
		}
	}
}