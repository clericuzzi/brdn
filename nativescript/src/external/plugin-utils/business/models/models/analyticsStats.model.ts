﻿import { Crudable } from '@src/external/plugin-utils/business/models/crud/crudable.model';

// utils
import { DateUtils } from '@src/external/plugin-utils/utils/date-utils';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// Navigation Properties
import { AnalyticsStatistic } from './analyticsStatistic.model';
import { User } from './user.model';

// components helpers
import { ComponentTypeEnum } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-definition.model';

export const AnalyticsStatsClass: string = `AnalyticsStats`;
export const AnalyticsStatsTable: string = `analytics_stats`;

/**
 * Column definitions for the `AnalyticsStats` class
 */
export enum AnalyticsStatsColumns
{
	Id = 'Id',
	UserId = 'UserId',
	Amount = 'Amount',
	EntityId = 'EntityId',
	Timestamp = 'Timestamp',
	StatisticId = 'StatisticId',

	UserIdParent = 'UserIdParent',
	StatisticIdParent = 'StatisticIdParent',
}
export const AnalyticsStatsColumnsFilter: string[] = [];
export const AnalyticsStatsColumnsInsert: string[] = [AnalyticsStatsColumns.StatisticId, AnalyticsStatsColumns.UserId, AnalyticsStatsColumns.EntityId, AnalyticsStatsColumns.Amount, AnalyticsStatsColumns.Timestamp];
export const AnalyticsStatsColumnsUpdate: string[] = [AnalyticsStatsColumns.StatisticId, AnalyticsStatsColumns.UserId, AnalyticsStatsColumns.EntityId, AnalyticsStatsColumns.Amount, AnalyticsStatsColumns.Timestamp];

/**
 * Implementations of the `AnalyticsStats` class
 */
export class AnalyticsStats extends Crudable
{
	public StatisticIdParent: AnalyticsStatistic;
	public UserIdParent: User;

	public static FromJson(json: any): AnalyticsStats
	{
		const item: AnalyticsStats = new AnalyticsStats();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public StatisticId: number = null,
		public UserId: number = null,
		public EntityId: number = null,
		public Amount: number = null,
		public Timestamp: Date = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`AnalyticsStats`, `analytics_stats`, CrudableTableDefinitionGender.Male, `Analytics Stats`, `Analytics Statss`, true, true, true, true, true, true);

		this.SetColumnDefinition(AnalyticsStatsColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AnalyticsStatsColumns.StatisticId, `statistic_id`, `StatisticId`, `Statistic Id`, `Statistic Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AnalyticsStatsColumns.UserId, `user_id`, `UserId`, `User Id`, `User Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(AnalyticsStatsColumns.EntityId, `entity_id`, `EntityId`, `Entity Id`, `Entity Ids`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(AnalyticsStatsColumns.Amount, `amount`, `Amount`, `Amount`, `Amounts`, ComponentTypeEnum.Number, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AnalyticsStatsColumns.Timestamp, `timestamp`, `Timestamp`, `Timestamp`, `Timestamps`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
	}

	public Clone(): AnalyticsStats
	{
		const clone: AnalyticsStats = new AnalyticsStats();
		clone.Id = this.Id;
		clone.UserId = this.UserId;
		clone.Amount = this.Amount;
		clone.EntityId = this.EntityId;
		clone.Timestamp = this.Timestamp;
		clone.StatisticId = this.StatisticId;

		clone.UserIdParent = this.UserIdParent;
		clone.StatisticIdParent = this.StatisticIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.StatisticIdParent == null ? '' : this.StatisticIdParent.ToString()}`;
	}

	public ToJson(navigationProperties: boolean = false): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Amount))
			json['Amount'] = this.Amount;
		if (!ObjectUtils.NullOrUndefined(this.EntityId))
			json['EntityId'] = this.EntityId;
		if (!ObjectUtils.NullOrUndefined(this.Timestamp))
			json['Timestamp'] = DateUtils.ToDateServer(this.Timestamp);
		if (navigationProperties)
		{
			if (!ObjectUtils.NullOrUndefined(this.StatisticIdParent))
				json['StatisticIdParent'] = this.StatisticIdParent.ToJson();
			if (!ObjectUtils.NullOrUndefined(this.UserIdParent))
				json['UserIdParent'] = this.UserIdParent.ToJson();
		}

		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: any): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.UserId = json['UserId'];
			this.Amount = json['Amount'];
			this.EntityId = json['EntityId'];
			this.StatisticId = json['StatisticId'];
			this.Timestamp = DateUtils.FromDateServer(json['Timestamp']);

			if (!ObjectUtils.NullOrUndefined(json['StatisticIdParent']))
			{
				this.StatisticIdParent = new AnalyticsStatistic();
				this.StatisticIdParent.ParseFromJson(json['StatisticIdParent']);
			}
			if (!ObjectUtils.NullOrUndefined(json['UserIdParent']))
			{
				this.UserIdParent = new User();
				this.UserIdParent.ParseFromJson(json['UserIdParent']);
			}
		}
	}
}

/**
 * Implementations of the `AnalyticsStatsFilter` class
 */
export class AnalyticsStatsFilter extends Crudable
{
	public static FromJson(json: Object): AnalyticsStatsFilter
	{
		const item: AnalyticsStatsFilter = new AnalyticsStatsFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public StatisticId: number[] = null,
		public UserId: number[] = null,
		public EntityId: number = null,
		public Amount: number = null,
		public Timestamp: Date = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`AnalyticsStatsFilter`, `analytics_statsFilter`, CrudableTableDefinitionGender.Male, `Analytics Stats`, `Analytics Statss`, true, true, true, true, true, true);

		this.SetColumnDefinition(AnalyticsStatsColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AnalyticsStatsColumns.StatisticId, `statistic_id`, `StatisticId`, `Statistic Id`, `Statistic Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AnalyticsStatsColumns.UserId, `user_id`, `UserId`, `User Id`, `User Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(AnalyticsStatsColumns.EntityId, `entity_id`, `EntityId`, `Entity Id`, `Entity Ids`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(AnalyticsStatsColumns.Amount, `amount`, `Amount`, `Amount`, `Amounts`, ComponentTypeEnum.Number, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AnalyticsStatsColumns.Timestamp, `timestamp`, `Timestamp`, `Timestamp`, `Timestamps`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
	}

	public Clone(): AnalyticsStatsFilter
	{
		const clone: AnalyticsStatsFilter = new AnalyticsStatsFilter();
		clone.Id = this.Id;
		clone.UserId = this.UserId;
		clone.Amount = this.Amount;
		clone.EntityId = this.EntityId;
		clone.Timestamp = this.Timestamp;
		clone.StatisticId = this.StatisticId;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Amount))
			json['Amount'] = this.Amount;
		if (!ObjectUtils.NullOrUndefined(this.EntityId))
			json['EntityId'] = this.EntityId;
		if (!ObjectUtils.NullOrUndefined(this.Timestamp))
			json['Timestamp'] = DateUtils.ToDateServer(this.Timestamp);
		if (!ObjectUtils.NullOrUndefined(this.UserId))
			json['UserId'] = Array.isArray(this.UserId) ? this.UserId : [this.UserId];
		if (!ObjectUtils.NullOrUndefined(this.StatisticId))
			json['StatisticId'] = Array.isArray(this.StatisticId) ? this.StatisticId : [this.StatisticId];


		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.UserId = json['UserId'];
			this.Amount = json['Amount'];
			this.EntityId = json['EntityId'];
			this.StatisticId = json['StatisticId'];
			this.Timestamp = DateUtils.FromDateServer(json['Timestamp']);
		}
	}
}