﻿import { Crudable } from '@src/external/plugin-utils/business/models/crud/crudable.model';

// utils
import { DateUtils } from '@src/external/plugin-utils/utils/date-utils';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// Navigation Properties
import { User } from './user.model';

// components helpers
import { ComponentTypeEnum } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-definition.model';

export const LogPropertyChangeClass: string = `LogPropertyChange`;
export const LogPropertyChangeTable: string = `log_property_change`;

/**
 * Column definitions for the `LogPropertyChange` class
 */
export enum LogPropertyChangeColumns
{
	Id = 'Id',
	UserId = 'UserId',
	Timestamp = 'Timestamp',
	EntityType = 'EntityType',
	PropertyName = 'PropertyName',
	PropertyValueOld = 'PropertyValueOld',
	PropertyValueNew = 'PropertyValueNew',

	UserIdParent = 'UserIdParent',
}
export const LogPropertyChangeColumnsFilter: string[] = [];
export const LogPropertyChangeColumnsInsert: string[] = [LogPropertyChangeColumns.UserId, LogPropertyChangeColumns.EntityType, LogPropertyChangeColumns.PropertyName, LogPropertyChangeColumns.PropertyValueOld, LogPropertyChangeColumns.PropertyValueNew, LogPropertyChangeColumns.Timestamp];
export const LogPropertyChangeColumnsUpdate: string[] = [LogPropertyChangeColumns.UserId, LogPropertyChangeColumns.EntityType, LogPropertyChangeColumns.PropertyName, LogPropertyChangeColumns.PropertyValueOld, LogPropertyChangeColumns.PropertyValueNew, LogPropertyChangeColumns.Timestamp];

/**
 * Implementations of the `LogPropertyChange` class
 */
export class LogPropertyChange extends Crudable
{
	public UserIdParent: User;

	public static FromJson(json: any): LogPropertyChange
	{
		const item: LogPropertyChange = new LogPropertyChange();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public UserId: number = null,
		public EntityType: string = null,
		public PropertyName: string = null,
		public PropertyValueOld: string = null,
		public PropertyValueNew: string = null,
		public Timestamp: Date = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`LogPropertyChange`, `log_property_change`, CrudableTableDefinitionGender.Male, `Log Property Change`, `Log Property Changes`, true, true, true, true, true, true);

		this.SetColumnDefinition(LogPropertyChangeColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(LogPropertyChangeColumns.UserId, `user_id`, `UserId`, `User Id`, `User Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(LogPropertyChangeColumns.EntityType, `entity_type`, `EntityType`, `Entity Type`, `Entity Types`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(LogPropertyChangeColumns.PropertyName, `property_name`, `PropertyName`, `Property Name`, `Property Names`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(LogPropertyChangeColumns.PropertyValueOld, `property_value_old`, `PropertyValueOld`, `Property Value Old`, `Property Value Olds`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(LogPropertyChangeColumns.PropertyValueNew, `property_value_new`, `PropertyValueNew`, `Property Value New`, `Property Value News`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(LogPropertyChangeColumns.Timestamp, `timestamp`, `Timestamp`, `Timestamp`, `Timestamps`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
	}

	public Clone(): LogPropertyChange
	{
		const clone: LogPropertyChange = new LogPropertyChange();
		clone.Id = this.Id;
		clone.UserId = this.UserId;
		clone.Timestamp = this.Timestamp;
		clone.EntityType = this.EntityType;
		clone.PropertyName = this.PropertyName;
		clone.PropertyValueOld = this.PropertyValueOld;
		clone.PropertyValueNew = this.PropertyValueNew;

		clone.UserIdParent = this.UserIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.UserIdParent == null ? '' : this.UserIdParent.ToString()}`;
	}

	public ToJson(navigationProperties: boolean = false): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.EntityType))
			json['EntityType'] = this.EntityType;
		if (!ObjectUtils.NullOrUndefined(this.PropertyName))
			json['PropertyName'] = this.PropertyName;
		if (!ObjectUtils.NullOrUndefined(this.PropertyValueOld))
			json['PropertyValueOld'] = this.PropertyValueOld;
		if (!ObjectUtils.NullOrUndefined(this.PropertyValueNew))
			json['PropertyValueNew'] = this.PropertyValueNew;
		if (!ObjectUtils.NullOrUndefined(this.Timestamp))
			json['Timestamp'] = DateUtils.ToDateServer(this.Timestamp);
		if (navigationProperties)
		{
			if (!ObjectUtils.NullOrUndefined(this.UserIdParent))
				json['UserIdParent'] = this.UserIdParent.ToJson();
		}

		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: any): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.UserId = json['UserId'];
			this.EntityType = json['EntityType'];
			this.PropertyName = json['PropertyName'];
			this.PropertyValueOld = json['PropertyValueOld'];
			this.PropertyValueNew = json['PropertyValueNew'];
			this.Timestamp = DateUtils.FromDateServer(json['Timestamp']);

			if (!ObjectUtils.NullOrUndefined(json['UserIdParent']))
			{
				this.UserIdParent = new User();
				this.UserIdParent.ParseFromJson(json['UserIdParent']);
			}
		}
	}
}

/**
 * Implementations of the `LogPropertyChangeFilter` class
 */
export class LogPropertyChangeFilter extends Crudable
{
	public static FromJson(json: Object): LogPropertyChangeFilter
	{
		const item: LogPropertyChangeFilter = new LogPropertyChangeFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public UserId: number[] = null,
		public EntityType: string = null,
		public PropertyName: string = null,
		public PropertyValueOld: string = null,
		public PropertyValueNew: string = null,
		public Timestamp: Date = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`LogPropertyChangeFilter`, `log_property_changeFilter`, CrudableTableDefinitionGender.Male, `Log Property Change`, `Log Property Changes`, true, true, true, true, true, true);

		this.SetColumnDefinition(LogPropertyChangeColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(LogPropertyChangeColumns.UserId, `user_id`, `UserId`, `User Id`, `User Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(LogPropertyChangeColumns.EntityType, `entity_type`, `EntityType`, `Entity Type`, `Entity Types`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(LogPropertyChangeColumns.PropertyName, `property_name`, `PropertyName`, `Property Name`, `Property Names`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(LogPropertyChangeColumns.PropertyValueOld, `property_value_old`, `PropertyValueOld`, `Property Value Old`, `Property Value Olds`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(LogPropertyChangeColumns.PropertyValueNew, `property_value_new`, `PropertyValueNew`, `Property Value New`, `Property Value News`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(LogPropertyChangeColumns.Timestamp, `timestamp`, `Timestamp`, `Timestamp`, `Timestamps`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
	}

	public Clone(): LogPropertyChangeFilter
	{
		const clone: LogPropertyChangeFilter = new LogPropertyChangeFilter();
		clone.Id = this.Id;
		clone.UserId = this.UserId;
		clone.Timestamp = this.Timestamp;
		clone.EntityType = this.EntityType;
		clone.PropertyName = this.PropertyName;
		clone.PropertyValueOld = this.PropertyValueOld;
		clone.PropertyValueNew = this.PropertyValueNew;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.EntityType))
			json['EntityType'] = this.EntityType;
		if (!ObjectUtils.NullOrUndefined(this.PropertyName))
			json['PropertyName'] = this.PropertyName;
		if (!ObjectUtils.NullOrUndefined(this.PropertyValueOld))
			json['PropertyValueOld'] = this.PropertyValueOld;
		if (!ObjectUtils.NullOrUndefined(this.PropertyValueNew))
			json['PropertyValueNew'] = this.PropertyValueNew;
		if (!ObjectUtils.NullOrUndefined(this.Timestamp))
			json['Timestamp'] = DateUtils.ToDateServer(this.Timestamp);
		if (!ObjectUtils.NullOrUndefined(this.UserId))
			json['UserId'] = Array.isArray(this.UserId) ? this.UserId : [this.UserId];


		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.UserId = json['UserId'];
			this.EntityType = json['EntityType'];
			this.PropertyName = json['PropertyName'];
			this.PropertyValueOld = json['PropertyValueOld'];
			this.PropertyValueNew = json['PropertyValueNew'];
			this.Timestamp = DateUtils.FromDateServer(json['Timestamp']);
		}
	}
}