﻿import { Crudable } from '@src/external/plugin-utils/business/models/crud/crudable.model';

// utils
import { DateUtils } from '@src/external/plugin-utils/utils/date-utils';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// Navigation Properties
import { Account } from './account.model';

// components helpers
import { ComponentTypeEnum } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-definition.model';

export const AccountContactClass: string = `AccountContact`;
export const AccountContactTable: string = `account_contact`;

/**
 * Column definitions for the `AccountContact` class
 */
export enum AccountContactColumns
{
	Id = 'Id',
	Name = 'Name',
	Email = 'Email',
	Phone = 'Phone',
	CelPhone = 'CelPhone',
	AccountId = 'AccountId',

	AccountIdParent = 'AccountIdParent',
}
export const AccountContactColumnsFilter: string[] = [];
export const AccountContactColumnsInsert: string[] = [AccountContactColumns.AccountId, AccountContactColumns.Name, AccountContactColumns.Email, AccountContactColumns.Phone, AccountContactColumns.CelPhone];
export const AccountContactColumnsUpdate: string[] = [AccountContactColumns.AccountId, AccountContactColumns.Name, AccountContactColumns.Email, AccountContactColumns.Phone, AccountContactColumns.CelPhone];

/**
 * Implementations of the `AccountContact` class
 */
export class AccountContact extends Crudable
{
	public AccountIdParent: Account;

	public static FromJson(json: any): AccountContact
	{
		const item: AccountContact = new AccountContact();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public AccountId: number = null,
		public Name: string = null,
		public Email: string = null,
		public Phone: string = null,
		public CelPhone: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`AccountContact`, `account_contact`, CrudableTableDefinitionGender.Male, `Account Contact`, `Account Contacts`, true, true, true, true, true, true);

		this.SetColumnDefinition(AccountContactColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountContactColumns.AccountId, `account_id`, `AccountId`, `Account Id`, `Account Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountContactColumns.Name, `name`, `Name`, `Name`, `Names`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountContactColumns.Email, `email`, `Email`, `Email`, `Emails`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(AccountContactColumns.Phone, `phone`, `Phone`, `Phone`, `Phones`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(AccountContactColumns.CelPhone, `cel_phone`, `CelPhone`, `Cel Phone`, `Cel Phones`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
	}

	public Clone(): AccountContact
	{
		const clone: AccountContact = new AccountContact();
		clone.Id = this.Id;
		clone.Name = this.Name;
		clone.Email = this.Email;
		clone.Phone = this.Phone;
		clone.CelPhone = this.CelPhone;
		clone.AccountId = this.AccountId;

		clone.AccountIdParent = this.AccountIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.AccountIdParent == null ? '' : this.AccountIdParent.ToString()}`;
	}

	public ToJson(navigationProperties: boolean = false): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json['Name'] = this.Name;
		if (!ObjectUtils.NullOrUndefined(this.Email))
			json['Email'] = this.Email;
		if (!ObjectUtils.NullOrUndefined(this.Phone))
			json['Phone'] = this.Phone;
		if (!ObjectUtils.NullOrUndefined(this.CelPhone))
			json['CelPhone'] = this.CelPhone;
		if (navigationProperties)
		{
			if (!ObjectUtils.NullOrUndefined(this.AccountIdParent))
				json['AccountIdParent'] = this.AccountIdParent.ToJson();
		}

		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: any): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Name = json['Name'];
			this.Email = json['Email'];
			this.Phone = json['Phone'];
			this.CelPhone = json['CelPhone'];
			this.AccountId = json['AccountId'];

			if (!ObjectUtils.NullOrUndefined(json['AccountIdParent']))
			{
				this.AccountIdParent = new Account();
				this.AccountIdParent.ParseFromJson(json['AccountIdParent']);
			}
		}
	}
}

/**
 * Implementations of the `AccountContactFilter` class
 */
export class AccountContactFilter extends Crudable
{
	public static FromJson(json: Object): AccountContactFilter
	{
		const item: AccountContactFilter = new AccountContactFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public AccountId: number[] = null,
		public Name: string = null,
		public Email: string = null,
		public Phone: string = null,
		public CelPhone: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`AccountContactFilter`, `account_contactFilter`, CrudableTableDefinitionGender.Male, `Account Contact`, `Account Contacts`, true, true, true, true, true, true);

		this.SetColumnDefinition(AccountContactColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountContactColumns.AccountId, `account_id`, `AccountId`, `Account Id`, `Account Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountContactColumns.Name, `name`, `Name`, `Name`, `Names`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountContactColumns.Email, `email`, `Email`, `Email`, `Emails`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(AccountContactColumns.Phone, `phone`, `Phone`, `Phone`, `Phones`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(AccountContactColumns.CelPhone, `cel_phone`, `CelPhone`, `Cel Phone`, `Cel Phones`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
	}

	public Clone(): AccountContactFilter
	{
		const clone: AccountContactFilter = new AccountContactFilter();
		clone.Id = this.Id;
		clone.Name = this.Name;
		clone.Email = this.Email;
		clone.Phone = this.Phone;
		clone.CelPhone = this.CelPhone;
		clone.AccountId = this.AccountId;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json['Name'] = this.Name;
		if (!ObjectUtils.NullOrUndefined(this.Email))
			json['Email'] = this.Email;
		if (!ObjectUtils.NullOrUndefined(this.Phone))
			json['Phone'] = this.Phone;
		if (!ObjectUtils.NullOrUndefined(this.CelPhone))
			json['CelPhone'] = this.CelPhone;
		if (!ObjectUtils.NullOrUndefined(this.AccountId))
			json['AccountId'] = Array.isArray(this.AccountId) ? this.AccountId : [this.AccountId];


		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Name = json['Name'];
			this.Email = json['Email'];
			this.Phone = json['Phone'];
			this.CelPhone = json['CelPhone'];
			this.AccountId = json['AccountId'];
		}
	}
}