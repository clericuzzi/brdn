﻿import { Crudable } from '@src/external/plugin-utils/business/models/crud/crudable.model';

// utils
import { DateUtils } from '@src/external/plugin-utils/utils/date-utils';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// Navigation Properties
import { StatusType } from './statusType.model';

// components helpers
import { ComponentTypeEnum } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-definition.model';

export const StatusClass: string = `Status`;
export const StatusTable: string = `status`;

/**
 * Column definitions for the `Status` class
 */
export enum StatusColumns
{
	Id = 'Id',
	Name = 'Name',
	StatusTypeId = 'StatusTypeId',

	StatusTypeIdParent = 'StatusTypeIdParent',
}
export const StatusColumnsFilter: string[] = [];
export const StatusColumnsInsert: string[] = [StatusColumns.StatusTypeId, StatusColumns.Name];
export const StatusColumnsUpdate: string[] = [StatusColumns.StatusTypeId, StatusColumns.Name];

/**
 * Implementations of the `Status` class
 */
export class Status extends Crudable
{
	public StatusTypeIdParent: StatusType;

	public static FromJson(json: any): Status
	{
		const item: Status = new Status();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public StatusTypeId: number = null,
		public Name: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`Status`, `status`, CrudableTableDefinitionGender.Male, `Status`, `Statuss`, true, true, true, true, true, true);

		this.SetColumnDefinition(StatusColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(StatusColumns.StatusTypeId, `status_type_id`, `StatusTypeId`, `Status Type Id`, `Status Type Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(StatusColumns.Name, `name`, `Name`, `Name`, `Names`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): Status
	{
		const clone: Status = new Status();
		clone.Id = this.Id;
		clone.Name = this.Name;
		clone.StatusTypeId = this.StatusTypeId;

		clone.StatusTypeIdParent = this.StatusTypeIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.StatusTypeIdParent == null ? '' : this.StatusTypeIdParent.ToString()}`;
	}

	public ToJson(navigationProperties: boolean = false): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json['Name'] = this.Name;
		if (navigationProperties)
		{
			if (!ObjectUtils.NullOrUndefined(this.StatusTypeIdParent))
				json['StatusTypeIdParent'] = this.StatusTypeIdParent.ToJson();
		}

		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: any): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Name = json['Name'];
			this.StatusTypeId = json['StatusTypeId'];

			if (!ObjectUtils.NullOrUndefined(json['StatusTypeIdParent']))
			{
				this.StatusTypeIdParent = new StatusType();
				this.StatusTypeIdParent.ParseFromJson(json['StatusTypeIdParent']);
			}
		}
	}
}

/**
 * Implementations of the `StatusFilter` class
 */
export class StatusFilter extends Crudable
{
	public static FromJson(json: Object): StatusFilter
	{
		const item: StatusFilter = new StatusFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public StatusTypeId: number[] = null,
		public Name: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`StatusFilter`, `statusFilter`, CrudableTableDefinitionGender.Male, `Status`, `Statuss`, true, true, true, true, true, true);

		this.SetColumnDefinition(StatusColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(StatusColumns.StatusTypeId, `status_type_id`, `StatusTypeId`, `Status Type Id`, `Status Type Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(StatusColumns.Name, `name`, `Name`, `Name`, `Names`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): StatusFilter
	{
		const clone: StatusFilter = new StatusFilter();
		clone.Id = this.Id;
		clone.Name = this.Name;
		clone.StatusTypeId = this.StatusTypeId;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json['Name'] = this.Name;
		if (!ObjectUtils.NullOrUndefined(this.StatusTypeId))
			json['StatusTypeId'] = Array.isArray(this.StatusTypeId) ? this.StatusTypeId : [this.StatusTypeId];


		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Name = json['Name'];
			this.StatusTypeId = json['StatusTypeId'];
		}
	}
}