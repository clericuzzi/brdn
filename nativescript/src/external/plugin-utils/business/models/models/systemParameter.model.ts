﻿import { Crudable } from '@src/external/plugin-utils/business/models/crud/crudable.model';

// utils
import { DateUtils } from '@src/external/plugin-utils/utils/date-utils';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// components helpers
import { ComponentTypeEnum } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-definition.model';

export const SystemParameterClass: string = `SystemParameter`;
export const SystemParameterTable: string = `system_parameter`;

/**
 * Column definitions for the `SystemParameter` class
 */
export enum SystemParameterColumns
{
	Id = 'Id',
	Value = 'Value',
	Parameter = 'Parameter',
}
export const SystemParameterColumnsFilter: string[] = [];
export const SystemParameterColumnsInsert: string[] = [SystemParameterColumns.Parameter, SystemParameterColumns.Value];
export const SystemParameterColumnsUpdate: string[] = [SystemParameterColumns.Parameter, SystemParameterColumns.Value];

/**
 * Implementations of the `SystemParameter` class
 */
export class SystemParameter extends Crudable
{
	public static FromJson(json: any): SystemParameter
	{
		const item: SystemParameter = new SystemParameter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public Parameter: string = null,
		public Value: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`SystemParameter`, `system_parameter`, CrudableTableDefinitionGender.Male, `System Parameter`, `System Parameters`, true, true, true, true, true, true);

		this.SetColumnDefinition(SystemParameterColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(SystemParameterColumns.Parameter, `parameter`, `Parameter`, `Parameter`, `Parameters`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(SystemParameterColumns.Value, `value`, `Value`, `Value`, `Values`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): SystemParameter
	{
		const clone: SystemParameter = new SystemParameter();
		clone.Id = this.Id;
		clone.Value = this.Value;
		clone.Parameter = this.Parameter;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.Parameter}`;
	}

	public ToJson(navigationProperties: boolean = false): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Value))
			json['Value'] = this.Value;
		if (!ObjectUtils.NullOrUndefined(this.Parameter))
			json['Parameter'] = this.Parameter;
		if (navigationProperties)
		{
		}

		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: any): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Value = json['Value'];
			this.Parameter = json['Parameter'];
		}
	}
}

/**
 * Implementations of the `SystemParameterFilter` class
 */
export class SystemParameterFilter extends Crudable
{
	public static FromJson(json: Object): SystemParameterFilter
	{
		const item: SystemParameterFilter = new SystemParameterFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public Parameter: string = null,
		public Value: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`SystemParameterFilter`, `system_parameterFilter`, CrudableTableDefinitionGender.Male, `System Parameter`, `System Parameters`, true, true, true, true, true, true);

		this.SetColumnDefinition(SystemParameterColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(SystemParameterColumns.Parameter, `parameter`, `Parameter`, `Parameter`, `Parameters`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(SystemParameterColumns.Value, `value`, `Value`, `Value`, `Values`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): SystemParameterFilter
	{
		const clone: SystemParameterFilter = new SystemParameterFilter();
		clone.Id = this.Id;
		clone.Value = this.Value;
		clone.Parameter = this.Parameter;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Value))
			json['Value'] = this.Value;
		if (!ObjectUtils.NullOrUndefined(this.Parameter))
			json['Parameter'] = this.Parameter;


		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Value = json['Value'];
			this.Parameter = json['Parameter'];
		}
	}
}