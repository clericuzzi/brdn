﻿import { Crudable } from '@src/external/plugin-utils/business/models/crud/crudable.model';

// utils
import { DateUtils } from '@src/external/plugin-utils/utils/date-utils';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// Navigation Properties
import { Account } from './account.model';

// components helpers
import { ComponentTypeEnum } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-definition.model';

export const AccountConfigClass: string = `AccountConfig`;
export const AccountConfigTable: string = `account_config`;

/**
 * Column definitions for the `AccountConfig` class
 */
export enum AccountConfigColumns
{
	Id = 'Id',
	Value = 'Value',
	AccountId = 'AccountId',
	Parameter = 'Parameter',

	AccountIdParent = 'AccountIdParent',
}
export const AccountConfigColumnsFilter: string[] = [];
export const AccountConfigColumnsInsert: string[] = [AccountConfigColumns.AccountId, AccountConfigColumns.Parameter, AccountConfigColumns.Value];
export const AccountConfigColumnsUpdate: string[] = [AccountConfigColumns.AccountId, AccountConfigColumns.Parameter, AccountConfigColumns.Value];

/**
 * Implementations of the `AccountConfig` class
 */
export class AccountConfig extends Crudable
{
	public AccountIdParent: Account;

	public static FromJson(json: any): AccountConfig
	{
		const item: AccountConfig = new AccountConfig();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public AccountId: number = null,
		public Parameter: string = null,
		public Value: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`AccountConfig`, `account_config`, CrudableTableDefinitionGender.Male, `Account Config`, `Account Configs`, true, true, true, true, true, true);

		this.SetColumnDefinition(AccountConfigColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountConfigColumns.AccountId, `account_id`, `AccountId`, `Account Id`, `Account Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountConfigColumns.Parameter, `parameter`, `Parameter`, `Parameter`, `Parameters`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountConfigColumns.Value, `value`, `Value`, `Value`, `Values`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): AccountConfig
	{
		const clone: AccountConfig = new AccountConfig();
		clone.Id = this.Id;
		clone.Value = this.Value;
		clone.AccountId = this.AccountId;
		clone.Parameter = this.Parameter;

		clone.AccountIdParent = this.AccountIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.AccountIdParent == null ? '' : this.AccountIdParent.ToString()}`;
	}

	public ToJson(navigationProperties: boolean = false): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Value))
			json['Value'] = this.Value;
		if (!ObjectUtils.NullOrUndefined(this.Parameter))
			json['Parameter'] = this.Parameter;
		if (navigationProperties)
		{
			if (!ObjectUtils.NullOrUndefined(this.AccountIdParent))
				json['AccountIdParent'] = this.AccountIdParent.ToJson();
		}

		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: any): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Value = json['Value'];
			this.AccountId = json['AccountId'];
			this.Parameter = json['Parameter'];

			if (!ObjectUtils.NullOrUndefined(json['AccountIdParent']))
			{
				this.AccountIdParent = new Account();
				this.AccountIdParent.ParseFromJson(json['AccountIdParent']);
			}
		}
	}
}

/**
 * Implementations of the `AccountConfigFilter` class
 */
export class AccountConfigFilter extends Crudable
{
	public static FromJson(json: Object): AccountConfigFilter
	{
		const item: AccountConfigFilter = new AccountConfigFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public AccountId: number[] = null,
		public Parameter: string = null,
		public Value: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`AccountConfigFilter`, `account_configFilter`, CrudableTableDefinitionGender.Male, `Account Config`, `Account Configs`, true, true, true, true, true, true);

		this.SetColumnDefinition(AccountConfigColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountConfigColumns.AccountId, `account_id`, `AccountId`, `Account Id`, `Account Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountConfigColumns.Parameter, `parameter`, `Parameter`, `Parameter`, `Parameters`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AccountConfigColumns.Value, `value`, `Value`, `Value`, `Values`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): AccountConfigFilter
	{
		const clone: AccountConfigFilter = new AccountConfigFilter();
		clone.Id = this.Id;
		clone.Value = this.Value;
		clone.AccountId = this.AccountId;
		clone.Parameter = this.Parameter;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Value))
			json['Value'] = this.Value;
		if (!ObjectUtils.NullOrUndefined(this.Parameter))
			json['Parameter'] = this.Parameter;
		if (!ObjectUtils.NullOrUndefined(this.AccountId))
			json['AccountId'] = Array.isArray(this.AccountId) ? this.AccountId : [this.AccountId];


		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Value = json['Value'];
			this.AccountId = json['AccountId'];
			this.Parameter = json['Parameter'];
		}
	}
}