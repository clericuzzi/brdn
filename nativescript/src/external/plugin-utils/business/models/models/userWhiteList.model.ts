﻿import { Crudable } from '@src/external/plugin-utils/business/models/crud/crudable.model';

// utils
import { DateUtils } from '@src/external/plugin-utils/utils/date-utils';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// Navigation Properties
import { User } from './user.model';

// components helpers
import { ComponentTypeEnum } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-definition.model';

export const UserWhiteListClass: string = `UserWhiteList`;
export const UserWhiteListTable: string = `user_white_list`;

/**
 * Column definitions for the `UserWhiteList` class
 */
export enum UserWhiteListColumns
{
	Id = 'Id',
	UserId = 'UserId',

	UserIdParent = 'UserIdParent',
}
export const UserWhiteListColumnsFilter: string[] = [];
export const UserWhiteListColumnsInsert: string[] = [UserWhiteListColumns.UserId];
export const UserWhiteListColumnsUpdate: string[] = [UserWhiteListColumns.UserId];

/**
 * Implementations of the `UserWhiteList` class
 */
export class UserWhiteList extends Crudable
{
	public UserIdParent: User;

	public static FromJson(json: any): UserWhiteList
	{
		const item: UserWhiteList = new UserWhiteList();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public UserId: number = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`UserWhiteList`, `user_white_list`, CrudableTableDefinitionGender.Male, `User White List`, `User White Lists`, true, true, true, true, true, true);

		this.SetColumnDefinition(UserWhiteListColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(UserWhiteListColumns.UserId, `user_id`, `UserId`, `User Id`, `User Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): UserWhiteList
	{
		const clone: UserWhiteList = new UserWhiteList();
		clone.Id = this.Id;
		clone.UserId = this.UserId;

		clone.UserIdParent = this.UserIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.UserIdParent == null ? '' : this.UserIdParent.ToString()}`;
	}

	public ToJson(navigationProperties: boolean = false): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (navigationProperties)
		{
			if (!ObjectUtils.NullOrUndefined(this.UserIdParent))
				json['UserIdParent'] = this.UserIdParent.ToJson();
		}

		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: any): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.UserId = json['UserId'];

			if (!ObjectUtils.NullOrUndefined(json['UserIdParent']))
			{
				this.UserIdParent = new User();
				this.UserIdParent.ParseFromJson(json['UserIdParent']);
			}
		}
	}
}

/**
 * Implementations of the `UserWhiteListFilter` class
 */
export class UserWhiteListFilter extends Crudable
{
	public static FromJson(json: Object): UserWhiteListFilter
	{
		const item: UserWhiteListFilter = new UserWhiteListFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public UserId: number[] = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`UserWhiteListFilter`, `user_white_listFilter`, CrudableTableDefinitionGender.Male, `User White List`, `User White Lists`, true, true, true, true, true, true);

		this.SetColumnDefinition(UserWhiteListColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(UserWhiteListColumns.UserId, `user_id`, `UserId`, `User Id`, `User Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): UserWhiteListFilter
	{
		const clone: UserWhiteListFilter = new UserWhiteListFilter();
		clone.Id = this.Id;
		clone.UserId = this.UserId;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.UserId))
			json['UserId'] = Array.isArray(this.UserId) ? this.UserId : [this.UserId];


		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.UserId = json['UserId'];
		}
	}
}