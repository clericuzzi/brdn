﻿import { Crudable } from '@src/external/plugin-utils/business/models/crud/crudable.model';

// utils
import { DateUtils } from '@src/external/plugin-utils/utils/date-utils';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// Navigation Properties
import { Activity } from './activity.model';
import { User } from './user.model';

// components helpers
import { ComponentTypeEnum } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-definition.model';

export const ActivityAllowedUserClass: string = `ActivityAllowedUser`;
export const ActivityAllowedUserTable: string = `activity_allowed_user`;

/**
 * Column definitions for the `ActivityAllowedUser` class
 */
export enum ActivityAllowedUserColumns
{
	Id = 'Id',
	UserId = 'UserId',
	Timestamp = 'Timestamp',
	ActivityId = 'ActivityId',

	UserIdParent = 'UserIdParent',
	ActivityIdParent = 'ActivityIdParent',
}
export const ActivityAllowedUserColumnsFilter: string[] = [];
export const ActivityAllowedUserColumnsInsert: string[] = [ActivityAllowedUserColumns.ActivityId, ActivityAllowedUserColumns.UserId, ActivityAllowedUserColumns.Timestamp];
export const ActivityAllowedUserColumnsUpdate: string[] = [ActivityAllowedUserColumns.ActivityId, ActivityAllowedUserColumns.UserId, ActivityAllowedUserColumns.Timestamp];

/**
 * Implementations of the `ActivityAllowedUser` class
 */
export class ActivityAllowedUser extends Crudable
{
	public ActivityIdParent: Activity;
	public UserIdParent: User;

	public static FromJson(json: any): ActivityAllowedUser
	{
		const item: ActivityAllowedUser = new ActivityAllowedUser();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public ActivityId: number = null,
		public UserId: number = null,
		public Timestamp: Date = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`ActivityAllowedUser`, `activity_allowed_user`, CrudableTableDefinitionGender.Male, `Activity Allowed User`, `Activity Allowed Users`, true, true, true, true, true, true);

		this.SetColumnDefinition(ActivityAllowedUserColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ActivityAllowedUserColumns.ActivityId, `activity_id`, `ActivityId`, `Activity Id`, `Activity Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ActivityAllowedUserColumns.UserId, `user_id`, `UserId`, `User Id`, `User Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ActivityAllowedUserColumns.Timestamp, `timestamp`, `Timestamp`, `Timestamp`, `Timestamps`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
	}

	public Clone(): ActivityAllowedUser
	{
		const clone: ActivityAllowedUser = new ActivityAllowedUser();
		clone.Id = this.Id;
		clone.UserId = this.UserId;
		clone.Timestamp = this.Timestamp;
		clone.ActivityId = this.ActivityId;

		clone.UserIdParent = this.UserIdParent;
		clone.ActivityIdParent = this.ActivityIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.ActivityIdParent == null ? '' : this.ActivityIdParent.ToString()}`;
	}

	public ToJson(navigationProperties: boolean = false): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Timestamp))
			json['Timestamp'] = DateUtils.ToDateServer(this.Timestamp);
		if (navigationProperties)
		{
			if (!ObjectUtils.NullOrUndefined(this.ActivityIdParent))
				json['ActivityIdParent'] = this.ActivityIdParent.ToJson();
			if (!ObjectUtils.NullOrUndefined(this.UserIdParent))
				json['UserIdParent'] = this.UserIdParent.ToJson();
		}

		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: any): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.UserId = json['UserId'];
			this.ActivityId = json['ActivityId'];
			this.Timestamp = DateUtils.FromDateServer(json['Timestamp']);

			if (!ObjectUtils.NullOrUndefined(json['ActivityIdParent']))
			{
				this.ActivityIdParent = new Activity();
				this.ActivityIdParent.ParseFromJson(json['ActivityIdParent']);
			}
			if (!ObjectUtils.NullOrUndefined(json['UserIdParent']))
			{
				this.UserIdParent = new User();
				this.UserIdParent.ParseFromJson(json['UserIdParent']);
			}
		}
	}
}

/**
 * Implementations of the `ActivityAllowedUserFilter` class
 */
export class ActivityAllowedUserFilter extends Crudable
{
	public static FromJson(json: Object): ActivityAllowedUserFilter
	{
		const item: ActivityAllowedUserFilter = new ActivityAllowedUserFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public ActivityId: number[] = null,
		public UserId: number[] = null,
		public Timestamp: Date = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`ActivityAllowedUserFilter`, `activity_allowed_userFilter`, CrudableTableDefinitionGender.Male, `Activity Allowed User`, `Activity Allowed Users`, true, true, true, true, true, true);

		this.SetColumnDefinition(ActivityAllowedUserColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ActivityAllowedUserColumns.ActivityId, `activity_id`, `ActivityId`, `Activity Id`, `Activity Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ActivityAllowedUserColumns.UserId, `user_id`, `UserId`, `User Id`, `User Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(ActivityAllowedUserColumns.Timestamp, `timestamp`, `Timestamp`, `Timestamp`, `Timestamps`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
	}

	public Clone(): ActivityAllowedUserFilter
	{
		const clone: ActivityAllowedUserFilter = new ActivityAllowedUserFilter();
		clone.Id = this.Id;
		clone.UserId = this.UserId;
		clone.Timestamp = this.Timestamp;
		clone.ActivityId = this.ActivityId;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Timestamp))
			json['Timestamp'] = DateUtils.ToDateServer(this.Timestamp);
		if (!ObjectUtils.NullOrUndefined(this.UserId))
			json['UserId'] = Array.isArray(this.UserId) ? this.UserId : [this.UserId];
		if (!ObjectUtils.NullOrUndefined(this.ActivityId))
			json['ActivityId'] = Array.isArray(this.ActivityId) ? this.ActivityId : [this.ActivityId];


		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.UserId = json['UserId'];
			this.ActivityId = json['ActivityId'];
			this.Timestamp = DateUtils.FromDateServer(json['Timestamp']);
		}
	}
}