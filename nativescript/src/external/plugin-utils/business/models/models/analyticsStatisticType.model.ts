﻿import { Crudable } from '@src/external/plugin-utils/business/models/crud/crudable.model';

// utils
import { DateUtils } from '@src/external/plugin-utils/utils/date-utils';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// components helpers
import { ComponentTypeEnum } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-definition.model';

export const AnalyticsStatisticTypeClass: string = `AnalyticsStatisticType`;
export const AnalyticsStatisticTypeTable: string = `analytics_statistic_type`;

/**
 * Column definitions for the `AnalyticsStatisticType` class
 */
export enum AnalyticsStatisticTypeColumns
{
	Id = 'Id',
	Name = 'Name',
}
export const AnalyticsStatisticTypeColumnsFilter: string[] = [];
export const AnalyticsStatisticTypeColumnsInsert: string[] = [AnalyticsStatisticTypeColumns.Name];
export const AnalyticsStatisticTypeColumnsUpdate: string[] = [AnalyticsStatisticTypeColumns.Name];

/**
 * Implementations of the `AnalyticsStatisticType` class
 */
export class AnalyticsStatisticType extends Crudable
{
	public static FromJson(json: any): AnalyticsStatisticType
	{
		const item: AnalyticsStatisticType = new AnalyticsStatisticType();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public Name: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`AnalyticsStatisticType`, `analytics_statistic_type`, CrudableTableDefinitionGender.Male, `Analytics Statistic Type`, `Analytics Statistic Types`, true, true, true, true, true, true);

		this.SetColumnDefinition(AnalyticsStatisticTypeColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AnalyticsStatisticTypeColumns.Name, `name`, `Name`, `Name`, `Names`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): AnalyticsStatisticType
	{
		const clone: AnalyticsStatisticType = new AnalyticsStatisticType();
		clone.Id = this.Id;
		clone.Name = this.Name;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.Name}`;
	}

	public ToJson(navigationProperties: boolean = false): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json['Name'] = this.Name;
		if (navigationProperties)
		{
		}

		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: any): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Name = json['Name'];
		}
	}
}

/**
 * Implementations of the `AnalyticsStatisticTypeFilter` class
 */
export class AnalyticsStatisticTypeFilter extends Crudable
{
	public static FromJson(json: Object): AnalyticsStatisticTypeFilter
	{
		const item: AnalyticsStatisticTypeFilter = new AnalyticsStatisticTypeFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public Name: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`AnalyticsStatisticTypeFilter`, `analytics_statistic_typeFilter`, CrudableTableDefinitionGender.Male, `Analytics Statistic Type`, `Analytics Statistic Types`, true, true, true, true, true, true);

		this.SetColumnDefinition(AnalyticsStatisticTypeColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AnalyticsStatisticTypeColumns.Name, `name`, `Name`, `Name`, `Names`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): AnalyticsStatisticTypeFilter
	{
		const clone: AnalyticsStatisticTypeFilter = new AnalyticsStatisticTypeFilter();
		clone.Id = this.Id;
		clone.Name = this.Name;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json['Name'] = this.Name;


		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Name = json['Name'];
		}
	}
}