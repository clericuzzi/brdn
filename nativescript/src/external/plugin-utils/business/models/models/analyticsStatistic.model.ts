﻿import { Crudable } from '@src/external/plugin-utils/business/models/crud/crudable.model';

// utils
import { DateUtils } from '@src/external/plugin-utils/utils/date-utils';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// Navigation Properties
import { AnalyticsStatisticType } from './analyticsStatisticType.model';

// components helpers
import { ComponentTypeEnum } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-definition.model';

export const AnalyticsStatisticClass: string = `AnalyticsStatistic`;
export const AnalyticsStatisticTable: string = `analytics_statistic`;

/**
 * Column definitions for the `AnalyticsStatistic` class
 */
export enum AnalyticsStatisticColumns
{
	Id = 'Id',
	Name = 'Name',
	TypeId = 'TypeId',

	TypeIdParent = 'TypeIdParent',
}
export const AnalyticsStatisticColumnsFilter: string[] = [];
export const AnalyticsStatisticColumnsInsert: string[] = [AnalyticsStatisticColumns.TypeId, AnalyticsStatisticColumns.Name];
export const AnalyticsStatisticColumnsUpdate: string[] = [AnalyticsStatisticColumns.TypeId, AnalyticsStatisticColumns.Name];

/**
 * Implementations of the `AnalyticsStatistic` class
 */
export class AnalyticsStatistic extends Crudable
{
	public TypeIdParent: AnalyticsStatisticType;

	public static FromJson(json: any): AnalyticsStatistic
	{
		const item: AnalyticsStatistic = new AnalyticsStatistic();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public TypeId: number = null,
		public Name: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`AnalyticsStatistic`, `analytics_statistic`, CrudableTableDefinitionGender.Male, `Analytics Statistic`, `Analytics Statistics`, true, true, true, true, true, true);

		this.SetColumnDefinition(AnalyticsStatisticColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AnalyticsStatisticColumns.TypeId, `type_id`, `TypeId`, `Type Id`, `Type Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AnalyticsStatisticColumns.Name, `name`, `Name`, `Name`, `Names`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): AnalyticsStatistic
	{
		const clone: AnalyticsStatistic = new AnalyticsStatistic();
		clone.Id = this.Id;
		clone.Name = this.Name;
		clone.TypeId = this.TypeId;

		clone.TypeIdParent = this.TypeIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.TypeIdParent == null ? '' : this.TypeIdParent.ToString()}`;
	}

	public ToJson(navigationProperties: boolean = false): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json['Name'] = this.Name;
		if (navigationProperties)
		{
			if (!ObjectUtils.NullOrUndefined(this.TypeIdParent))
				json['TypeIdParent'] = this.TypeIdParent.ToJson();
		}

		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: any): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Name = json['Name'];
			this.TypeId = json['TypeId'];

			if (!ObjectUtils.NullOrUndefined(json['TypeIdParent']))
			{
				this.TypeIdParent = new AnalyticsStatisticType();
				this.TypeIdParent.ParseFromJson(json['TypeIdParent']);
			}
		}
	}
}

/**
 * Implementations of the `AnalyticsStatisticFilter` class
 */
export class AnalyticsStatisticFilter extends Crudable
{
	public static FromJson(json: Object): AnalyticsStatisticFilter
	{
		const item: AnalyticsStatisticFilter = new AnalyticsStatisticFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public TypeId: number[] = null,
		public Name: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`AnalyticsStatisticFilter`, `analytics_statisticFilter`, CrudableTableDefinitionGender.Male, `Analytics Statistic`, `Analytics Statistics`, true, true, true, true, true, true);

		this.SetColumnDefinition(AnalyticsStatisticColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AnalyticsStatisticColumns.TypeId, `type_id`, `TypeId`, `Type Id`, `Type Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(AnalyticsStatisticColumns.Name, `name`, `Name`, `Name`, `Names`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): AnalyticsStatisticFilter
	{
		const clone: AnalyticsStatisticFilter = new AnalyticsStatisticFilter();
		clone.Id = this.Id;
		clone.Name = this.Name;
		clone.TypeId = this.TypeId;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json['Name'] = this.Name;
		if (!ObjectUtils.NullOrUndefined(this.TypeId))
			json['TypeId'] = Array.isArray(this.TypeId) ? this.TypeId : [this.TypeId];


		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Name = json['Name'];
			this.TypeId = json['TypeId'];
		}
	}
}