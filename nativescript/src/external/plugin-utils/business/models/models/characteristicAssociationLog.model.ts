﻿import { Crudable } from '@src/external/plugin-utils/business/models/crud/crudable.model';

// utils
import { DateUtils } from '@src/external/plugin-utils/utils/date-utils';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// Navigation Properties
import { Characteristic } from './characteristic.model';

// components helpers
import { ComponentTypeEnum } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-definition.model';

export const CharacteristicAssociationLogClass: string = `CharacteristicAssociationLog`;
export const CharacteristicAssociationLogTable: string = `characteristic_association_log`;

/**
 * Column definitions for the `CharacteristicAssociationLog` class
 */
export enum CharacteristicAssociationLogColumns
{
	Id = 'Id',
	Action = 'Action',
	EntityId = 'EntityId',
	Timestamp = 'Timestamp',
	CharacteristicId = 'CharacteristicId',

	CharacteristicIdParent = 'CharacteristicIdParent',
}
export const CharacteristicAssociationLogColumnsFilter: string[] = [];
export const CharacteristicAssociationLogColumnsInsert: string[] = [CharacteristicAssociationLogColumns.CharacteristicId, CharacteristicAssociationLogColumns.EntityId, CharacteristicAssociationLogColumns.Action, CharacteristicAssociationLogColumns.Timestamp];
export const CharacteristicAssociationLogColumnsUpdate: string[] = [CharacteristicAssociationLogColumns.CharacteristicId, CharacteristicAssociationLogColumns.EntityId, CharacteristicAssociationLogColumns.Action, CharacteristicAssociationLogColumns.Timestamp];

/**
 * Implementations of the `CharacteristicAssociationLog` class
 */
export class CharacteristicAssociationLog extends Crudable
{
	public CharacteristicIdParent: Characteristic;

	public static FromJson(json: any): CharacteristicAssociationLog
	{
		const item: CharacteristicAssociationLog = new CharacteristicAssociationLog();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public CharacteristicId: number = null,
		public EntityId: number = null,
		public Action: number = null,
		public Timestamp: Date = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`CharacteristicAssociationLog`, `characteristic_association_log`, CrudableTableDefinitionGender.Male, `Characteristic Association Log`, `Characteristic Association Logs`, true, true, true, true, true, true);

		this.SetColumnDefinition(CharacteristicAssociationLogColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CharacteristicAssociationLogColumns.CharacteristicId, `characteristic_id`, `CharacteristicId`, `Characteristic Id`, `Characteristic Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CharacteristicAssociationLogColumns.EntityId, `entity_id`, `EntityId`, `Entity Id`, `Entity Ids`, ComponentTypeEnum.Number, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CharacteristicAssociationLogColumns.Action, `action`, `Action`, `Action`, `Actions`, ComponentTypeEnum.Number, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CharacteristicAssociationLogColumns.Timestamp, `timestamp`, `Timestamp`, `Timestamp`, `Timestamps`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
	}

	public Clone(): CharacteristicAssociationLog
	{
		const clone: CharacteristicAssociationLog = new CharacteristicAssociationLog();
		clone.Id = this.Id;
		clone.Action = this.Action;
		clone.EntityId = this.EntityId;
		clone.Timestamp = this.Timestamp;
		clone.CharacteristicId = this.CharacteristicId;

		clone.CharacteristicIdParent = this.CharacteristicIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.CharacteristicIdParent == null ? '' : this.CharacteristicIdParent.ToString()}`;
	}

	public ToJson(navigationProperties: boolean = false): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Action))
			json['Action'] = this.Action;
		if (!ObjectUtils.NullOrUndefined(this.EntityId))
			json['EntityId'] = this.EntityId;
		if (!ObjectUtils.NullOrUndefined(this.Timestamp))
			json['Timestamp'] = DateUtils.ToDateServer(this.Timestamp);
		if (navigationProperties)
		{
			if (!ObjectUtils.NullOrUndefined(this.CharacteristicIdParent))
				json['CharacteristicIdParent'] = this.CharacteristicIdParent.ToJson();
		}

		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: any): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Action = json['Action'];
			this.EntityId = json['EntityId'];
			this.CharacteristicId = json['CharacteristicId'];
			this.Timestamp = DateUtils.FromDateServer(json['Timestamp']);

			if (!ObjectUtils.NullOrUndefined(json['CharacteristicIdParent']))
			{
				this.CharacteristicIdParent = new Characteristic();
				this.CharacteristicIdParent.ParseFromJson(json['CharacteristicIdParent']);
			}
		}
	}
}

/**
 * Implementations of the `CharacteristicAssociationLogFilter` class
 */
export class CharacteristicAssociationLogFilter extends Crudable
{
	public static FromJson(json: Object): CharacteristicAssociationLogFilter
	{
		const item: CharacteristicAssociationLogFilter = new CharacteristicAssociationLogFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public CharacteristicId: number[] = null,
		public EntityId: number = null,
		public Action: number = null,
		public Timestamp: Date = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`CharacteristicAssociationLogFilter`, `characteristic_association_logFilter`, CrudableTableDefinitionGender.Male, `Characteristic Association Log`, `Characteristic Association Logs`, true, true, true, true, true, true);

		this.SetColumnDefinition(CharacteristicAssociationLogColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CharacteristicAssociationLogColumns.CharacteristicId, `characteristic_id`, `CharacteristicId`, `Characteristic Id`, `Characteristic Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CharacteristicAssociationLogColumns.EntityId, `entity_id`, `EntityId`, `Entity Id`, `Entity Ids`, ComponentTypeEnum.Number, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CharacteristicAssociationLogColumns.Action, `action`, `Action`, `Action`, `Actions`, ComponentTypeEnum.Number, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(CharacteristicAssociationLogColumns.Timestamp, `timestamp`, `Timestamp`, `Timestamp`, `Timestamps`, ComponentTypeEnum.Date, true, true, true, true, true, true, null, StringUtils.TransformDateToBr, null, null, null, `left`, false);
	}

	public Clone(): CharacteristicAssociationLogFilter
	{
		const clone: CharacteristicAssociationLogFilter = new CharacteristicAssociationLogFilter();
		clone.Id = this.Id;
		clone.Action = this.Action;
		clone.EntityId = this.EntityId;
		clone.Timestamp = this.Timestamp;
		clone.CharacteristicId = this.CharacteristicId;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Action))
			json['Action'] = this.Action;
		if (!ObjectUtils.NullOrUndefined(this.EntityId))
			json['EntityId'] = this.EntityId;
		if (!ObjectUtils.NullOrUndefined(this.Timestamp))
			json['Timestamp'] = DateUtils.ToDateServer(this.Timestamp);
		if (!ObjectUtils.NullOrUndefined(this.CharacteristicId))
			json['CharacteristicId'] = Array.isArray(this.CharacteristicId) ? this.CharacteristicId : [this.CharacteristicId];


		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Action = json['Action'];
			this.EntityId = json['EntityId'];
			this.CharacteristicId = json['CharacteristicId'];
			this.Timestamp = DateUtils.FromDateServer(json['Timestamp']);
		}
	}
}