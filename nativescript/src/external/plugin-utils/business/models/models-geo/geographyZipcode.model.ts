﻿import { Crudable } from '@src/external/plugin-utils/business/models/crud/crudable.model';

// utils
import { DateUtils } from '@src/external/plugin-utils/utils/date-utils';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// Navigation Properties
import { GeographyCity } from './geographyCity.model';
import { GeographyNeighbourhood } from './geographyNeighbourhood.model';

// components helpers
import { ComponentTypeEnum } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-definition.model';

export const GeographyZipcodeClass: string = `GeographyZipcode`;
export const GeographyZipcodeTable: string = `geography_zipcode`;

/**
 * Column definitions for the `GeographyZipcode` class
 */
export enum GeographyZipcodeColumns
{
	Id = 'Id',
	CityId = 'CityId',
	Zipcode = 'Zipcode',
	Location = 'Location',
	NeighbourhoodId = 'NeighbourhoodId',

	CityIdParent = 'CityIdParent',
	NeighbourhoodIdParent = 'NeighbourhoodIdParent',
}
export const GeographyZipcodeColumnsFilter: string[] = [];
export const GeographyZipcodeColumnsInsert: string[] = [GeographyZipcodeColumns.Zipcode, GeographyZipcodeColumns.CityId, GeographyZipcodeColumns.NeighbourhoodId, GeographyZipcodeColumns.Location];
export const GeographyZipcodeColumnsUpdate: string[] = [GeographyZipcodeColumns.Zipcode, GeographyZipcodeColumns.CityId, GeographyZipcodeColumns.NeighbourhoodId, GeographyZipcodeColumns.Location];

/**
 * Implementations of the `GeographyZipcode` class
 */
export class GeographyZipcode extends Crudable
{
	public CityIdParent: GeographyCity;
	public NeighbourhoodIdParent: GeographyNeighbourhood;

	public static FromJson(json: any): GeographyZipcode
	{
		const item: GeographyZipcode = new GeographyZipcode();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public Zipcode: string = null,
		public CityId: number = null,
		public NeighbourhoodId: number = null,
		public Location: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`GeographyZipcode`, `geography_zipcode`, CrudableTableDefinitionGender.Male, `Geography Zipcode`, `Geography Zipcodes`, true, true, true, true, true, true);

		this.SetColumnDefinition(GeographyZipcodeColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(GeographyZipcodeColumns.Zipcode, `zipcode`, `Zipcode`, `Zipcode`, `Zipcodes`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(GeographyZipcodeColumns.CityId, `city_id`, `CityId`, `City Id`, `City Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(GeographyZipcodeColumns.NeighbourhoodId, `neighbourhood_id`, `NeighbourhoodId`, `Neighbourhood Id`, `Neighbourhood Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(GeographyZipcodeColumns.Location, `location`, `Location`, `Location`, `Locations`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
	}

	public Clone(): GeographyZipcode
	{
		const clone: GeographyZipcode = new GeographyZipcode();
		clone.Id = this.Id;
		clone.CityId = this.CityId;
		clone.Zipcode = this.Zipcode;
		clone.Location = this.Location;
		clone.NeighbourhoodId = this.NeighbourhoodId;

		clone.CityIdParent = this.CityIdParent;
		clone.NeighbourhoodIdParent = this.NeighbourhoodIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.Zipcode}`;
	}

	public ToJson(navigationProperties: boolean = false): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Zipcode))
			json['Zipcode'] = this.Zipcode;
		if (!ObjectUtils.NullOrUndefined(this.Location))
			json['Location'] = this.Location;
		if (navigationProperties)
		{
			if (!ObjectUtils.NullOrUndefined(this.CityIdParent))
				json['CityIdParent'] = this.CityIdParent.ToJson();
			if (!ObjectUtils.NullOrUndefined(this.NeighbourhoodIdParent))
				json['NeighbourhoodIdParent'] = this.NeighbourhoodIdParent.ToJson();
		}

		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: any): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.CityId = json['CityId'];
			this.Zipcode = json['Zipcode'];
			this.Location = json['Location'];
			this.NeighbourhoodId = json['NeighbourhoodId'];

			if (!ObjectUtils.NullOrUndefined(json['CityIdParent']))
			{
				this.CityIdParent = new GeographyCity();
				this.CityIdParent.ParseFromJson(json['CityIdParent']);
			}
			if (!ObjectUtils.NullOrUndefined(json['NeighbourhoodIdParent']))
			{
				this.NeighbourhoodIdParent = new GeographyNeighbourhood();
				this.NeighbourhoodIdParent.ParseFromJson(json['NeighbourhoodIdParent']);
			}
		}
	}
}

/**
 * Implementations of the `GeographyZipcodeFilter` class
 */
export class GeographyZipcodeFilter extends Crudable
{
	public static FromJson(json: Object): GeographyZipcodeFilter
	{
		const item: GeographyZipcodeFilter = new GeographyZipcodeFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public Zipcode: string = null,
		public CityId: number[] = null,
		public NeighbourhoodId: number[] = null,
		public Location: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`GeographyZipcodeFilter`, `geography_zipcodeFilter`, CrudableTableDefinitionGender.Male, `Geography Zipcode`, `Geography Zipcodes`, true, true, true, true, true, true);

		this.SetColumnDefinition(GeographyZipcodeColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(GeographyZipcodeColumns.Zipcode, `zipcode`, `Zipcode`, `Zipcode`, `Zipcodes`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(GeographyZipcodeColumns.CityId, `city_id`, `CityId`, `City Id`, `City Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(GeographyZipcodeColumns.NeighbourhoodId, `neighbourhood_id`, `NeighbourhoodId`, `Neighbourhood Id`, `Neighbourhood Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(GeographyZipcodeColumns.Location, `location`, `Location`, `Location`, `Locations`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
	}

	public Clone(): GeographyZipcodeFilter
	{
		const clone: GeographyZipcodeFilter = new GeographyZipcodeFilter();
		clone.Id = this.Id;
		clone.CityId = this.CityId;
		clone.Zipcode = this.Zipcode;
		clone.Location = this.Location;
		clone.NeighbourhoodId = this.NeighbourhoodId;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Zipcode))
			json['Zipcode'] = this.Zipcode;
		if (!ObjectUtils.NullOrUndefined(this.Location))
			json['Location'] = this.Location;
		if (!ObjectUtils.NullOrUndefined(this.CityId))
			json['CityId'] = Array.isArray(this.CityId) ? this.CityId : [this.CityId];
		if (!ObjectUtils.NullOrUndefined(this.NeighbourhoodId))
			json['NeighbourhoodId'] = Array.isArray(this.NeighbourhoodId) ? this.NeighbourhoodId : [this.NeighbourhoodId];


		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.CityId = json['CityId'];
			this.Zipcode = json['Zipcode'];
			this.Location = json['Location'];
			this.NeighbourhoodId = json['NeighbourhoodId'];
		}
	}
}