﻿import { Crudable } from '@src/external/plugin-utils/business/models/crud/crudable.model';

// utils
import { DateUtils } from '@src/external/plugin-utils/utils/date-utils';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// Navigation Properties
import { GeographyCountry } from './geographyCountry.model';

// components helpers
import { ComponentTypeEnum } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-definition.model';

export const GeographyStateClass: string = `GeographyState`;
export const GeographyStateTable: string = `geography_state`;

/**
 * Column definitions for the `GeographyState` class
 */
export enum GeographyStateColumns
{
	Id = 'Id',
	Name = 'Name',
	CountryId = 'CountryId',
	Abbreviation = 'Abbreviation',

	CountryIdParent = 'CountryIdParent',
}
export const GeographyStateColumnsFilter: string[] = [];
export const GeographyStateColumnsInsert: string[] = [GeographyStateColumns.CountryId, GeographyStateColumns.Name, GeographyStateColumns.Abbreviation];
export const GeographyStateColumnsUpdate: string[] = [GeographyStateColumns.CountryId, GeographyStateColumns.Name, GeographyStateColumns.Abbreviation];

/**
 * Implementations of the `GeographyState` class
 */
export class GeographyState extends Crudable
{
	public CountryIdParent: GeographyCountry;

	public static FromJson(json: any): GeographyState
	{
		const item: GeographyState = new GeographyState();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public CountryId: number = null,
		public Name: string = null,
		public Abbreviation: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`GeographyState`, `geography_state`, CrudableTableDefinitionGender.Male, `Geography State`, `Geography States`, true, true, true, true, true, true);

		this.SetColumnDefinition(GeographyStateColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(GeographyStateColumns.CountryId, `country_id`, `CountryId`, `Country Id`, `Country Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(GeographyStateColumns.Name, `name`, `Name`, `Name`, `Names`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(GeographyStateColumns.Abbreviation, `abbreviation`, `Abbreviation`, `Abbreviation`, `Abbreviations`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): GeographyState
	{
		const clone: GeographyState = new GeographyState();
		clone.Id = this.Id;
		clone.Name = this.Name;
		clone.CountryId = this.CountryId;
		clone.Abbreviation = this.Abbreviation;

		clone.CountryIdParent = this.CountryIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.CountryIdParent == null ? '' : this.CountryIdParent.ToString()}`;
	}

	public ToJson(navigationProperties: boolean = false): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json['Name'] = this.Name;
		if (!ObjectUtils.NullOrUndefined(this.Abbreviation))
			json['Abbreviation'] = this.Abbreviation;
		if (navigationProperties)
		{
			if (!ObjectUtils.NullOrUndefined(this.CountryIdParent))
				json['CountryIdParent'] = this.CountryIdParent.ToJson();
		}

		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: any): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Name = json['Name'];
			this.CountryId = json['CountryId'];
			this.Abbreviation = json['Abbreviation'];

			if (!ObjectUtils.NullOrUndefined(json['CountryIdParent']))
			{
				this.CountryIdParent = new GeographyCountry();
				this.CountryIdParent.ParseFromJson(json['CountryIdParent']);
			}
		}
	}
}

/**
 * Implementations of the `GeographyStateFilter` class
 */
export class GeographyStateFilter extends Crudable
{
	public static FromJson(json: Object): GeographyStateFilter
	{
		const item: GeographyStateFilter = new GeographyStateFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public CountryId: number[] = null,
		public Name: string = null,
		public Abbreviation: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`GeographyStateFilter`, `geography_stateFilter`, CrudableTableDefinitionGender.Male, `Geography State`, `Geography States`, true, true, true, true, true, true);

		this.SetColumnDefinition(GeographyStateColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(GeographyStateColumns.CountryId, `country_id`, `CountryId`, `Country Id`, `Country Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(GeographyStateColumns.Name, `name`, `Name`, `Name`, `Names`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(GeographyStateColumns.Abbreviation, `abbreviation`, `Abbreviation`, `Abbreviation`, `Abbreviations`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): GeographyStateFilter
	{
		const clone: GeographyStateFilter = new GeographyStateFilter();
		clone.Id = this.Id;
		clone.Name = this.Name;
		clone.CountryId = this.CountryId;
		clone.Abbreviation = this.Abbreviation;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json['Name'] = this.Name;
		if (!ObjectUtils.NullOrUndefined(this.Abbreviation))
			json['Abbreviation'] = this.Abbreviation;
		if (!ObjectUtils.NullOrUndefined(this.CountryId))
			json['CountryId'] = Array.isArray(this.CountryId) ? this.CountryId : [this.CountryId];


		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Name = json['Name'];
			this.CountryId = json['CountryId'];
			this.Abbreviation = json['Abbreviation'];
		}
	}
}