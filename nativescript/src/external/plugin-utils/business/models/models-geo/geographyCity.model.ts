﻿import { Crudable } from '@src/external/plugin-utils/business/models/crud/crudable.model';

// utils
import { DateUtils } from '@src/external/plugin-utils/utils/date-utils';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// Navigation Properties
import { GeographyState } from './geographyState.model';

// components helpers
import { ComponentTypeEnum } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-definition.model';

export const GeographyCityClass: string = `GeographyCity`;
export const GeographyCityTable: string = `geography_city`;

/**
 * Column definitions for the `GeographyCity` class
 */
export enum GeographyCityColumns
{
	Id = 'Id',
	Name = 'Name',
	StateId = 'StateId',

	StateIdParent = 'StateIdParent',
}
export const GeographyCityColumnsFilter: string[] = [];
export const GeographyCityColumnsInsert: string[] = [GeographyCityColumns.StateId, GeographyCityColumns.Name];
export const GeographyCityColumnsUpdate: string[] = [GeographyCityColumns.StateId, GeographyCityColumns.Name];

/**
 * Implementations of the `GeographyCity` class
 */
export class GeographyCity extends Crudable
{
	public StateIdParent: GeographyState;

	public static FromJson(json: any): GeographyCity
	{
		const item: GeographyCity = new GeographyCity();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public StateId: number = null,
		public Name: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`GeographyCity`, `geography_city`, CrudableTableDefinitionGender.Male, `Geography City`, `Geography Citys`, true, true, true, true, true, true);

		this.SetColumnDefinition(GeographyCityColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(GeographyCityColumns.StateId, `state_id`, `StateId`, `State Id`, `State Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(GeographyCityColumns.Name, `name`, `Name`, `Name`, `Names`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): GeographyCity
	{
		const clone: GeographyCity = new GeographyCity();
		clone.Id = this.Id;
		clone.Name = this.Name;
		clone.StateId = this.StateId;

		clone.StateIdParent = this.StateIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.StateIdParent == null ? '' : this.StateIdParent.ToString()}`;
	}

	public ToJson(navigationProperties: boolean = false): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json['Name'] = this.Name;
		if (navigationProperties)
		{
			if (!ObjectUtils.NullOrUndefined(this.StateIdParent))
				json['StateIdParent'] = this.StateIdParent.ToJson();
		}

		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: any): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Name = json['Name'];
			this.StateId = json['StateId'];

			if (!ObjectUtils.NullOrUndefined(json['StateIdParent']))
			{
				this.StateIdParent = new GeographyState();
				this.StateIdParent.ParseFromJson(json['StateIdParent']);
			}
		}
	}
}

/**
 * Implementations of the `GeographyCityFilter` class
 */
export class GeographyCityFilter extends Crudable
{
	public static FromJson(json: Object): GeographyCityFilter
	{
		const item: GeographyCityFilter = new GeographyCityFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public StateId: number[] = null,
		public Name: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`GeographyCityFilter`, `geography_cityFilter`, CrudableTableDefinitionGender.Male, `Geography City`, `Geography Citys`, true, true, true, true, true, true);

		this.SetColumnDefinition(GeographyCityColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(GeographyCityColumns.StateId, `state_id`, `StateId`, `State Id`, `State Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(GeographyCityColumns.Name, `name`, `Name`, `Name`, `Names`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): GeographyCityFilter
	{
		const clone: GeographyCityFilter = new GeographyCityFilter();
		clone.Id = this.Id;
		clone.Name = this.Name;
		clone.StateId = this.StateId;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json['Name'] = this.Name;
		if (!ObjectUtils.NullOrUndefined(this.StateId))
			json['StateId'] = Array.isArray(this.StateId) ? this.StateId : [this.StateId];


		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Name = json['Name'];
			this.StateId = json['StateId'];
		}
	}
}