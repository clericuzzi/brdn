﻿import { Crudable } from '@src/external/plugin-utils/business/models/crud/crudable.model';

// utils
import { DateUtils } from '@src/external/plugin-utils/utils/date-utils';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// Navigation Properties
import { GeographyCity } from './geographyCity.model';

// components helpers
import { ComponentTypeEnum } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';
import { CrudableTableDefinitionGender } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-definition.model';

export const GeographyNeighbourhoodClass: string = `GeographyNeighbourhood`;
export const GeographyNeighbourhoodTable: string = `geography_neighbourhood`;

/**
 * Column definitions for the `GeographyNeighbourhood` class
 */
export enum GeographyNeighbourhoodColumns
{
	Id = 'Id',
	Name = 'Name',
	CityId = 'CityId',

	CityIdParent = 'CityIdParent',
}
export const GeographyNeighbourhoodColumnsFilter: string[] = [];
export const GeographyNeighbourhoodColumnsInsert: string[] = [GeographyNeighbourhoodColumns.CityId, GeographyNeighbourhoodColumns.Name];
export const GeographyNeighbourhoodColumnsUpdate: string[] = [GeographyNeighbourhoodColumns.CityId, GeographyNeighbourhoodColumns.Name];

/**
 * Implementations of the `GeographyNeighbourhood` class
 */
export class GeographyNeighbourhood extends Crudable
{
	public CityIdParent: GeographyCity;

	public static FromJson(json: any): GeographyNeighbourhood
	{
		const item: GeographyNeighbourhood = new GeographyNeighbourhood();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public CityId: number = null,
		public Name: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`GeographyNeighbourhood`, `geography_neighbourhood`, CrudableTableDefinitionGender.Male, `Geography Neighbourhood`, `Geography Neighbourhoods`, true, true, true, true, true, true);

		this.SetColumnDefinition(GeographyNeighbourhoodColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(GeographyNeighbourhoodColumns.CityId, `city_id`, `CityId`, `City Id`, `City Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(GeographyNeighbourhoodColumns.Name, `name`, `Name`, `Name`, `Names`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): GeographyNeighbourhood
	{
		const clone: GeographyNeighbourhood = new GeographyNeighbourhood();
		clone.Id = this.Id;
		clone.Name = this.Name;
		clone.CityId = this.CityId;

		clone.CityIdParent = this.CityIdParent;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `${this.CityIdParent == null ? '' : this.CityIdParent.ToString()}`;
	}

	public ToJson(navigationProperties: boolean = false): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json['Name'] = this.Name;
		if (navigationProperties)
		{
			if (!ObjectUtils.NullOrUndefined(this.CityIdParent))
				json['CityIdParent'] = this.CityIdParent.ToJson();
		}

		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: any): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Name = json['Name'];
			this.CityId = json['CityId'];

			if (!ObjectUtils.NullOrUndefined(json['CityIdParent']))
			{
				this.CityIdParent = new GeographyCity();
				this.CityIdParent.ParseFromJson(json['CityIdParent']);
			}
		}
	}
}

/**
 * Implementations of the `GeographyNeighbourhoodFilter` class
 */
export class GeographyNeighbourhoodFilter extends Crudable
{
	public static FromJson(json: Object): GeographyNeighbourhoodFilter
	{
		const item: GeographyNeighbourhoodFilter = new GeographyNeighbourhoodFilter();
		if (json)
			item.ParseFromJson(json);

		return item;
	}

	constructor(
		public Id: number = null,
		public CityId: number[] = null,
		public Name: string = null,
	)
	{
		super();
		this._InitDefinition();
	}
	private _InitDefinition(): void
	{
		this.SetTableDefinition(`GeographyNeighbourhoodFilter`, `geography_neighbourhoodFilter`, CrudableTableDefinitionGender.Male, `Geography Neighbourhood`, `Geography Neighbourhoods`, true, true, true, true, true, true);

		this.SetColumnDefinition(GeographyNeighbourhoodColumns.Id, `id`, `Id`, `Id`, `Ids`, ComponentTypeEnum.Number, true, true, false, false, false, true, null, null, null, null, null, `left`, false);
		this.SetColumnDefinition(GeographyNeighbourhoodColumns.CityId, `city_id`, `CityId`, `City Id`, `City Ids`, ComponentTypeEnum.AutoComplete, true, true, true, true, true, true, null, null, null, null, null, `left`, true);
		this.SetColumnDefinition(GeographyNeighbourhoodColumns.Name, `name`, `Name`, `Name`, `Names`, ComponentTypeEnum.Text, true, true, true, true, true, true, null, null, null, null, null, `left`, false);
	}

	public Clone(): GeographyNeighbourhoodFilter
	{
		const clone: GeographyNeighbourhoodFilter = new GeographyNeighbourhoodFilter();
		clone.Id = this.Id;
		clone.Name = this.Name;
		clone.CityId = this.CityId;

		clone.TableDefinition = this.TableDefinition;
		clone.ColumnDefinitions = this.ColumnDefinitions;

		clone.CustomProperties = this.CustomProperties;

		return clone;
	}
	public ToString(): string
	{
		return `null`;
	}

	public ToJson(): any
	{
		const json: any = {};
		if (!ObjectUtils.NullOrUndefined(this.Id))
			json['Id'] = this.Id;
		if (!ObjectUtils.NullOrUndefined(this.Name))
			json['Name'] = this.Name;
		if (!ObjectUtils.NullOrUndefined(this.CityId))
			json['CityId'] = Array.isArray(this.CityId) ? this.CityId : [this.CityId];


		return json;
	}
	public ParseToJson(): string
	{
		const json: any = this.ToJson();

		return JSON.stringify(json);
	}
	public ParseFromJson(json: Object): void
	{
		if (json)
		{
			this.Id = json['Id'];
			this.Name = json['Name'];
			this.CityId = json['CityId'];
		}
	}
}