﻿// models
import { BaseMessage } from './base-message.model';

/**
 * This class represents a response from the server containing a flag indicating the request's success
 */
export class StatusResponseMessage extends BaseMessage
{
    /**
     * A flag defining if the request was successfull or not
     */
    public Success: boolean;
}