﻿import * as _ from 'lodash';

// utils
import { JsonUtils } from '@src/external/plugin-utils/utils/json-utils';

// models
import { Crudable } from '../crud/crudable.model';
import { BaseMessage } from './base-message.model';
import { Dictionary } from '@src/external/plugin-utils/utils/dictionary-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

export class BatchResponseMessageList extends BaseMessage
{
    /**
     * The entities list
     */
    public Entities: Dictionary<any[]>;
    /**
     * The amount of data returned
     */
    public EntityCount: number;

    public HasKey(key: string): any
    {
        if (!ObjectUtils.NullOrUndefined(this.Entities))
            return !ObjectUtils.NullOrUndefined(this.Entities[key]);
        else
            return false;
    }
    public GetList<T extends Crudable>(key: string, baseObject: T): T[]
    {
        const entityList: any[] = this.Entities[key];
        if (ObjectUtils.NullUndefinedOrEmptyArray(entityList))
            return [];
        else
        {
            const entities: T[] = [];
            for (const entity of entityList)
            {
                baseObject.ParseFromJson(entity);
                entities.push(baseObject.Clone() as T);
            }

            return entities;
        }
    }
    public Exists<T extends Crudable>(key: string, baseObject: T): boolean
    {
        return false;
    }
}

/**
 * This class represents a response from the server containing a list of entities
 */
export class BatchResponseMessage<T> extends BaseMessage
{
    /**
     * The entities list
     */
    public Entities: T[];
    /**
     * The amount of data returned
     */
    public EntityCount: number;

    /**
     * Parses the returned data and returns it as an type T object
     * @param baseObject the base object to create the returned one
     */
    public Parse<T>(baseObject: T): void
    {
        const entities = [];
        for (let i = 0; i < this.Entities.length; i++)
        {
            const model: T = JsonUtils.ToInstanceOf(_.cloneDeep(baseObject), this.Entities[i]);
            if (baseObject instanceof Crudable)
            {
                const crudableModel: Crudable = (model as any) as Crudable;
                const clone: Crudable = crudableModel.Clone();

                entities.push(clone);
            }
            else
                entities.push(model);
        }

        this.Entities = entities;
    }
    /**
     * Parses the returned data and returns it as an type T object array
     * @param baseObject the base object to create the returned one
     */
    public ParseJSOnContent<T>(baseObject: T): void
    {
        if (this.Entities != null && this.Entities.length > 0)
        {
            const json = JSON.stringify(this.Entities);
            this.Entities = JsonUtils.ToInstanceOf([], json);
            this.Parse(baseObject);
        }
    }
}