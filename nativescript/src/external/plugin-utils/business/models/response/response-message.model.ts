﻿// models
import { BaseMessage } from './base-message.model';
import { BatchResponseMessage } from './batch-response-message.model';

// utils
import { JsonUtils } from '@src/external/plugin-utils/utils/json-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

/**
 * This class represents a response from the server containing a single entity
 */
export class ResponseMessage<T> extends BaseMessage
{
    /**
     * The returned entity
     */
    public Entity: T;

    /**
     * Parses the returned data and returns it as an type T object
     * @param baseObject the base object to create the returned one
     */
    public Parse(baseObject: T): T
    {
        const newInstance: T = ObjectUtils.Assign(baseObject, this.Entity);

        return newInstance;
    }
}
