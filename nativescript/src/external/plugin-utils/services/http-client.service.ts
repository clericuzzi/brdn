import { JsonUtils } from '../utils/json-utils';
import { Injectable } from '@angular/core';
import { timeout } from 'rxjs/operators';

import { HttpClient, HttpHeaders } from '@angular/common/http';
// models
import { RequestMessage, RequestMessageDefaults } from '../business/models/request/request-message.model';
// managers
import { PluginUtilsEncriptionManager } from '../business/managers/encryption.manager';

@Injectable()
export class HttpClientService
{
  private Headers: any = new HttpHeaders({ 'Content-Type': 'application/json', 'Accept': 'application/json', 'Access-Control-Allow-Origin': '*' });

  constructor(
    private _Http: HttpClient,
  )
  {
    this.Headers = new HttpHeaders();
    this.Headers.append(`Content-Type`, `application/json`);
    this.Headers.append(`Accept`, `application/json`);

    this.Headers.append(`Access-Control-Allow-Origin`, `*`);
    this.Headers.append(`Access-Control-Allow-Methods`, `GET, PUT, POST, DELETE, PATCH, OPTIONS`);
    this.Headers.append(`Access-Control-Allow-Headers`, `Cache-Control, Pragma, Origin, Authorization, Content-Type, X-Requested-With`);
  }

  public Get<T>(request: RequestMessage, baseObject: T, success: (response: T) => void, error: (response: any) => void, timeoutLimit: number = 5000): void
  {
    try
    {
      this._Http.get(request.Url, { responseType: 'text' })
        .pipe(timeout(timeoutLimit))
        .subscribe(
          (response: any) => this._ActionCallback<T>(baseObject, success, response)
          , err => error(err));
    }
    catch (err)
    {
      console.log(`GET ERROR CAUGHT: `, err);
    }
  }
  public Put<T>(request: RequestMessage, baseObject: T, success: (response: T) => void, error: (response: any) => void, timeoutLimit: number = 5000): void
  {
    try
    {
      this._Http.put(request.Url, request, { responseType: 'text' })
        .pipe(timeout(timeoutLimit))
        .subscribe(
          (response: any) => this._ActionCallback<T>(baseObject, success, response)
          , err => error(err));
    }
    catch (err)
    {
      console.log(`PUT ERROR CAUGHT: `, err);
    }
  }
  public Post<T>(request: RequestMessage, baseObject: T, success: (response: T) => void, error: (response: any) => void, timeoutLimit: number = 5000): void
  {
    try
    {
      this._Http.post(request.Url, request, { responseType: 'text' })
        .pipe(timeout(timeoutLimit))
        .subscribe(
          (response: any) => this._ActionCallback<T>(baseObject, success, response)
          , err => error(err));
    }
    catch (err)
    {
      console.log(`POST ERROR CAUGHT: `, err);
    }
  }

  private _ActionCallback<T>(baseObject: T, success: (response: T) => void, response: string): void
  {
    try
    {
      let json: any = {};
      if (!RequestMessageDefaults.Crypted)
      {
        json = JSON.parse(response);
        success(JsonUtils.ToInstanceOf<T>(baseObject, json));
      }
      else
      {
        json = JSON.parse(PluginUtilsEncriptionManager.AesDecryptString(response));
        const responseObject: T = JsonUtils.ToInstanceOf<T>(baseObject, json);
        success(responseObject);
      }
    }
    catch (err)
    {
      console.log(`POST CALLBACK ERROR: `, err);
    }
  }
}
