import { MatSnackBar } from '@angular/material';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

// utils
import { SnackUtils } from '@src/external/plugin-utils/utils/snack-utils';

// models
import { User } from '@src/external/plugin-utils/business/models/models/user.model'
import { Account } from '@src/external/plugin-utils/business/models/models/account.model';

// services
import { UserDataService } from 'clericuzzi-lib/services/user-data.service';
import { ProjectRoutingService } from '@src/services/project-routing.service';

// decoration
import { InputDecoration } from '@src/external/plugin-utils/utils/input-decoration';

// components
import { TextInputComponent } from 'clericuzzi-lib/modules/shared/form-controls/text-input/text-input.component';
import { SubjectListenerComponent } from 'clericuzzi-lib/modules/shared/subject-listener/subject-listener.component';

@Component({
  selector: 'app-login-auth',
  templateUrl: './login-auth.component.html',
  styleUrls: [ './login-auth.component.css' ]
})
export class LoginAuthComponent extends SubjectListenerComponent implements OnInit
{
  @ViewChild(`login`) _FieldLogin: TextInputComponent;
  @ViewChild(`password`) _FieldPassword: TextInputComponent;

  @ViewChild(`logoImg`) _LogoImg: ElementRef;
  @ViewChild(`container`) _Container: ElementRef;
  @ViewChild(`containerLogin`) _ContainerLogin: ElementRef;
  @ViewChild(`containerButton`) _ContainerButtonLogin: ElementRef;
  @ViewChild(`containerPassword`) _ContainerPassword: ElementRef;
  @ViewChild(`containerButtonForgot`) _ContainerButtonForgot: ElementRef;
  public User: User = new User();

  public Busy: boolean = false;
  public ErrorMessage: string;
  public HasErrorMessage: boolean = false;

  constructor(
    private _Snack: MatSnackBar,
    private _Routes: ProjectRoutingService,
    private _UserData: UserDataService,
  )
  {
    super();
  }

  ngOnInit()
  {
    if (window.location.href.indexOf(`localhost`) >= 0)
    {
      this.User.Email = `clericuzzi@decidimos.com.br`;
      this.User.Password = `123456`;
      // setTimeout(() => this.Login(), 450);
    }

    setTimeout(() => InputDecoration.ElementFadeIn(this._Container), 500);
    setTimeout(() => InputDecoration.AddClass(this._LogoImg, `component-animation-on-screen`, `component-animation-off-screen`), 250);
    setTimeout(() => InputDecoration.AddClass(this._ContainerLogin, `component-animation-on-screen`, `component-animation-off-screen`), 450);
    setTimeout(() => InputDecoration.AddClass(this._ContainerPassword, `component-animation-on-screen`, `component-animation-off-screen`), 650);
    setTimeout(() => InputDecoration.AddClass(this._ContainerButtonLogin, `component-animation-on-screen`, `component-animation-off-screen`), 850);
    setTimeout(() => InputDecoration.AddClass(this._ContainerButtonForgot, `component-animation-on-screen`, `component-animation-off-screen`), 1050);

    this.ListenTo(this._UserData.LoginService.UserLoggedIn, accounts => this._LoginCallback(accounts));
    this.ListenTo(this._UserData.LoginService.UserLoggedInError, message => this._LoginErrorCallback(message));

  }
  ngOnDestroy()
  {
  }

  public Login(): void
  {
    try
    {
      if (this.CanSubmit())
      {
        this.Busy = true;
        this._UserData.Login(this.User);
      }
      else
        SnackUtils.Open(this._Snack, `Preencha corretamente os campos!`, `Aviso`);
    }
    catch(e)
    {
      this.Busy = false;
      SnackUtils.OpenError(this._Snack, e);
    }
  }
  private _LoginCallback(accounts: Account[]): void
  {
    this.Busy = false;
    this._UserData.AccountsLoaded(accounts);

    this._Routes.Site();
  }
  private _LoginErrorCallback(message: string): void
  {
    this.Busy = false;
    SnackUtils.OpenError(this._Snack, message);
  }

  public CanSubmit(): boolean
  {
    if (this.Busy)
      return false;
    else if (this.User.Email != null && this.User.Password != null)
      return this.User.Email.length > 0 && this.User.Password.length >= 6;
    else
      return false;
  }
}