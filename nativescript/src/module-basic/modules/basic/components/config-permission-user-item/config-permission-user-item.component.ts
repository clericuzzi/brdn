import { Component, OnInit } from '@angular/core';

// utils
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// models
import { Permission } from '@src/external/plugin-utils/business/models/models/permission.model';

// behaviours
import { SelectListItem } from 'clericuzzi-lib/business/behaviours/select-list-item.behaviour';

@Component({
  selector: 'app-config-permission-user-item',
  templateUrl: './config-permission-user-item.component.html'
})
export class ConfigPermissionUserItemComponent extends SelectListItem<Permission> implements OnInit
{
  constructor()
  {
    super(null);
  }

  ngOnInit()
  {
  }

  public get Name(): string
  {
    if (ObjectUtils.NullOrUndefined(this.Model))
      return null;
    else
      return this.Model.Name;
  }
  public get Account(): string
  {
    if (ObjectUtils.NullOrUndefined(this.Model))
      return null;
    else
      return this.Model.AccountIdParent.Name.toString();
  }
  public get Priority(): string
  {
    if (ObjectUtils.NullOrUndefined(this.Model))
      return null;
    else
      return this.Model.Priority.toString();
  }
}