import { Component, OnInit } from '@angular/core';

// utils
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// models
import { Permission } from '@src/external/plugin-utils/business/models/models/permission.model';

// behaviours
import { SelectListItem } from 'clericuzzi-lib/business/behaviours/select-list-item.behaviour';

@Component({
  selector: 'app-config-permission-item',
  templateUrl: './config-permission-item.component.html'
})
export class ConfigPermissionItemComponent extends SelectListItem<Permission> implements OnInit
{
  constructor()
  {
    super(null);
  }

  ngOnInit() {
  }

  public get Account(): string
  {
    if (ObjectUtils.NullOrUndefined(this.Model) || ObjectUtils.NullOrUndefined(this.Model.AccountIdParent))
      return `falha ao recuperar a conta da permissão`;
    else
      return this.Model.AccountIdParent.Name;
  }
  public get Name(): string
  {
    if (ObjectUtils.NullOrUndefined(this.Model) || ObjectUtils.NullUndefinedOrEmpty(this.Model.Name))
      return `falha ao recuperar a permissão`;
    else
      return this.Model.Name;
  }
  public get Priority(): string
  {
    if (ObjectUtils.NullOrUndefined(this.Model) || ObjectUtils.NullOrUndefined(this.Model.Priority))
      return `falha ao recuperar a prioridade`;
    else
      return this.Model.Priority.toString();
  }
}