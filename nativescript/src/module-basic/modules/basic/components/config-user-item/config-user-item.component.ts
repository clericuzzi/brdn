import { MatDialog } from '@angular/material';
import { Component, OnInit } from '@angular/core';
// utils
import { ServerUrls } from '@src/external/plugin-utils/utils/server-urls';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';
// models
import { CustomField } from '@src/external/plugin-utils/business/models/crud/custom-field.model';
import { User, UserColumns, UserTable, UserClass } from '@src/external/plugin-utils/business/models/models/user.model';
import { UserPermissionColumns, UserPermissionClass } from '@src/external/plugin-utils/business/models/models/userPermission.model';
import { Permission, PermissionTable, PermissionClass } from '@src/external/plugin-utils/business/models/models/permission.model';
// services
import { EntitiesService } from 'clericuzzi-lib/services/entities.service';
// components
import { FromToComponent } from '../from-to/from-to.component';
import { MaterialPopupComponent } from 'clericuzzi-lib/modules/material/components/material-popup/material-popup.component';
import { ConfigPermissionUserItemComponent } from '../config-permission-user-item/config-permission-user-item.component';
// behaviours
import { SelectListItem } from 'clericuzzi-lib/business/behaviours/select-list-item.behaviour';

@Component({
  selector: 'app-config-user-item',
  templateUrl: './config-user-item.component.html'
})
export class ConfigUserItemComponent extends SelectListItem<User> implements OnInit
{
  constructor(
    private _Diag: MatDialog,
    private _EntitiesService: EntitiesService,
  )
  {
    super(null);
  }

  ngOnInit()
  {
  }

  public get Loaded(): boolean { return !ObjectUtils.NullOrUndefined(ObjectUtils.GetValue(this.Model, null, UserColumns.Name)); }

  public get Name(): string { return this.Model.Name; }
  public get Email(): string { return this.Model.Email; }
  public get Phone(): string { return StringUtils.ToPhoneBr(this.Model.Phone); }
  public get Timestamp(): string { return StringUtils.TransformDateToBr(this.Model.Timestamp); }

  public get IsActive(): string
  {
    var value: number = ObjectUtils.GetValue(this.Model, -1, UserColumns.Active);
    if (value < 0)
      return `carregando`;
    else
    {
      if (value == 0)
        return `inativo`;
      else
        return `ativo`;
    }
  }
  public get IsActiveClass(): string
  {
    var value: number = ObjectUtils.GetValue(this.Model, -1, UserColumns.Active);
    if (value < 0)
      return `null`;
    else
    {
      if (value == 0)
        return `color-delete font-medium font-bold`;
      else
        return `color-ok font-medium font-bold`;
    }
  }
  public get IsBlocked(): string
  {
    var value: number = ObjectUtils.GetValue(this.Model, -1, UserColumns.Blocked);
    if (value < 0)
      return `carregando`;
    else
    {
      if (value == 0)
        return `não bloqueado`;
      else
        return `bloqueado`;
    }
  }
  public get IsBlockedClass(): string
  {
    var value: number = ObjectUtils.GetValue(this.Model, -1, UserColumns.Blocked);
    if (value < 0)
      return `carregando`;
    else
    {
      if (value == 0)
        return `color-ok font-medium font-bold`;
      else
        return `color-delete font-medium font-bold`;
    }
  }

  public get ActiveActionTitle(): string
  {
    let value: boolean = ObjectUtils.GetValue(this.Model, true, UserColumns.Active);

    return value ? `Desligar usuário` : `Religar usuário`;
  }
  public get BlockActionTitle(): string
  {
    let value: boolean = ObjectUtils.GetValue(this.Model, true, UserColumns.Blocked);

    return value ? `Desbloquear usuário` : `Bloquear usuário`;
  }

  public ManagePermissions(): void
  {
    MaterialPopupComponent.Popup(this._Diag, FromToComponent, { UserId: this.Model.Id }, component => this._UpdatePermissionConfig(component));
  }
  private _UpdatePermissionConfig(component: FromToComponent<Permission>): void
  {
    component.Initialize(new Permission(), ConfigPermissionUserItemComponent as any, `Permissões adicionadas`, `Permissões disponíveis`)
    component.Configure(UserPermissionColumns.UserId, UserPermissionColumns.PermissionId, PermissionTable, UserClass, UserPermissionClass, true, false, `Permissões de ${this.Model.Name}`);
    component.ConfigureUpdate(`Alterar Permissões`, `Deseja realmente alterar as permissões do usuário selecionado?`, `Permissões alteradas com sucesso!`);
    component.GetInitialData(this.Model.Id, `user-id`, ServerUrls.GetUrl(`user-permission/get-definitions`));
  }

  public ToggleActive(): void
  {
    this.Model.Active = this.Model.Active == 1 ? 0 : 1;
    this._EntitiesService.Update(ServerUrls.GetUrl(`crud/${UserTable}/edit`), this.Model);
  }
  public ToggleBlocked(): void
  {
    this.Model.Blocked = this.Model.Blocked == 1 ? 0 : 1;
    this._EntitiesService.Update(ServerUrls.GetUrl(`crud/${UserTable}/edit`), this.Model);
  }
  public ResetPassword(): void
  {
    this._EntitiesService.Request(ServerUrls.GetUrl(`crud/${UserTable}/reset-password`), false, null, new CustomField(`model-id`, this.Model.Id));
  }
}