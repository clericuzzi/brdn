import { Component, OnInit } from '@angular/core';

// models
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// models
import { User } from '@src/external/plugin-utils/business/models/models/user.model';

// behaviours
import { SelectListItem } from 'clericuzzi-lib/business/behaviours/select-list-item.behaviour';

@Component({
  selector: 'app-config-activity-user-item',
  templateUrl: './config-activity-user-item.component.html'
})
export class ConfigActivityUserItemComponent extends SelectListItem<User> implements OnInit
{
  constructor()
  {
    super(null);
  }

  ngOnInit()
  {
  }

  public get Name(): string
  {
    if (!ObjectUtils.NullOrUndefined(this.Model))
      return this.Model.Name;
    else
      return `falha ao buscar o usuário`;
  }
  public get Info(): string
  {
    if (!ObjectUtils.NullOrUndefined(this.Model))
      return this.Model.Email;
    else
      return `falha ao buscar o usuário`;
  }
}