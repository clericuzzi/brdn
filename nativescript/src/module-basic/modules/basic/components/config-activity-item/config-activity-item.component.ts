import { MatDialog } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { Component, OnInit, ComponentFactoryResolver } from '@angular/core';

// utils
import { ServerUrls } from '@src/external/plugin-utils/utils/server-urls';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// models
import { User, UserTable } from '@src/external/plugin-utils/business/models/models/user.model';
import { ConfigActivityUserItemComponent } from '../config-activity-user-item/config-activity-user-item.component';
import { ConfigCharacteristicItemComponent } from '../config-characteristic-item/config-characteristic-item.component';
import { Characteristic, CharacteristicTable } from '@src/external/plugin-utils/business/models/models/characteristic.model';
import { Activity, ActivityClass, ActivityColumns } from '@src/external/plugin-utils/business/models/models/activity.model';
import { ActivityAllowedUserColumns, ActivityAllowedUserClass } from '@src/external/plugin-utils/business/models/models/activityAllowedUser.model';
import { ActivityCharacteristicMustColumns, ActivityCharacteristicMustClass } from '@src/external/plugin-utils/business/models/models/activityCharacteristicMust.model';
import { ActivityCharacteristicMustNotColumns, ActivityCharacteristicMustNotClass } from '@src/external/plugin-utils/business/models/models/activityCharacteristicMustNot.model';
import { ActivityCharacteristicOnConclusionAwardedColumns, ActivityCharacteristicOnConclusionAwardedClass } from '@src/external/plugin-utils/business/models/models/activityCharacteristicOnConclusionAwarded.model';
import { ActivityCharacteristicOnConclusionRevokedColumns, ActivityCharacteristicOnConclusionRevokedClass } from '@src/external/plugin-utils/business/models/models/activityCharacteristicOnConclusionRevoked.model';

// components
import { FromToComponent } from 'module-basic/modules/basic/components/from-to/from-to.component';
import { MaterialPopupComponent } from 'clericuzzi-lib/modules/material/components/material-popup/material-popup.component';

// behaviours
import { SelectListItem } from 'clericuzzi-lib/business/behaviours/select-list-item.behaviour';

@Component({
  selector: 'app-activity-list-item',
  templateUrl: './config-activity-item.component.html'
})
export class ConfigActivityItemComponent extends SelectListItem<Activity> implements OnInit
{
  constructor(
    private _Diag: MatDialog,

    public factory: ComponentFactoryResolver,
    public sanitizer: DomSanitizer,
  )
  {
    super(null);
  }

  ngOnInit()
  {
  }

  public get Name(): string
  {
    if (ObjectUtils.NullOrUndefined(this.Model))
      return `falha ao recuperar a tarefa`;
    else
      return this.Model.Name;
  }

  public ManageCharacteristicsMinus(): void
  {
    MaterialPopupComponent.Popup(this._Diag, FromToComponent, { UserId: this.Model.Id }, component => this.ManageCharacteristicsMinusConfig(component));
  }
  private ManageCharacteristicsMinusConfig(component: FromToComponent<Characteristic>): void
  {
    component.Initialize(new Characteristic(), ConfigCharacteristicItemComponent as any, `Características impeditivas disponíveis`, `Características impeditivas adicionadas`);
    component.Configure(ActivityCharacteristicMustNotColumns.ActivityId, ActivityCharacteristicMustNotColumns.CharacteristicId, CharacteristicTable, ActivityClass, ActivityCharacteristicMustNotClass, true, false, `Características impeditivas para ${this.Name}`);
    component.ConfigureUpdate(`Alterar características impeditivas`, `Deseja realmente alterar as características impeditivas da tarefa selecionada?`, `Características impeditivas alteradas com sucesso!`);
    component.GetInitialData(this.Model.Id, `activity-id`, ServerUrls.GetUrl(`from-to/activity-characteristic-negative`));
  }
  public ManageCharacteristicsPlus(): void
  {
    MaterialPopupComponent.Popup(this._Diag, FromToComponent, { UserId: this.Model.Id }, component => this.ManageCharacteristicsPlusConfig(component));
  }
  private ManageCharacteristicsPlusConfig(component: FromToComponent<Characteristic>): void
  {
    component.Initialize(new Characteristic(), ConfigCharacteristicItemComponent as any, `Características necessárias disponíveis`, `Características necessárias adicionadas`);
    component.Configure(ActivityCharacteristicMustColumns.ActivityId, ActivityCharacteristicMustColumns.CharacteristicId, CharacteristicTable, ActivityClass, ActivityCharacteristicMustClass, true, false, `Características necessárias para ${this.Name}`);
    component.ConfigureUpdate(`Alterar características necessárias`, `Deseja realmente alterar as características necessárias para a tarefa selecionada?`, `Características necessárias alteradas com sucesso!`);
    component.GetInitialData(this.Model.Id, `activity-id`, ServerUrls.GetUrl(`from-to/activity-characteristic-positive`));
  }

  public ManageCharacteristicsOnFinishRevoke(): void
  {
    MaterialPopupComponent.Popup(this._Diag, FromToComponent, { UserId: this.Model.Id }, component => this.ManageCharacteristicsOnFinishRevokeConfig(component));
  }
  private ManageCharacteristicsOnFinishRevokeConfig(component: FromToComponent<Characteristic>): void
  {
    component.Initialize(new Characteristic(), ConfigCharacteristicItemComponent as any, `Características a remover disponíveis`, `Características a remover adicionadas`);
    component.Configure(ActivityCharacteristicOnConclusionRevokedColumns.ActivityId, ActivityCharacteristicOnConclusionRevokedColumns.CharacteristicId, CharacteristicTable, ActivityClass, ActivityCharacteristicOnConclusionRevokedClass, true, false, `Características removidas ao completar ${this.Name}`);
    component.ConfigureUpdate(`Alterar características a remover`, `Deseja realmente alterar as características a remover da tarefa selecionada?`, `Características a remover alteradas com sucesso!`);
    component.GetInitialData(this.Model.Id, `activity-id`, ServerUrls.GetUrl(`from-to/activity-on-conclusion-revoke`));
  }

  public ManageCharacteristicsOnFinishAward(): void
  {
    MaterialPopupComponent.Popup(this._Diag, FromToComponent, { UserId: this.Model.Id }, component => this.ManageCharacteristicsOnFinishAwardConfig(component));
  }
  private ManageCharacteristicsOnFinishAwardConfig(component: FromToComponent<Characteristic>): void
  {
    component.Initialize(new Characteristic(), ConfigActivityUserItemComponent as any, `Características a adicionar disponíveis`, `Características a adicionar adicionadas`);
    component.Configure(ActivityCharacteristicOnConclusionAwardedColumns.ActivityId, ActivityCharacteristicOnConclusionAwardedColumns.CharacteristicId, CharacteristicTable, ActivityClass, ActivityCharacteristicOnConclusionAwardedClass, true, false, `Características adicionadas ao completar ${this.Name}`);
    component.ConfigureUpdate(`Alterar características a adicionar`, `Deseja realmente alterar as características a adicionar da tarefa selecionada?`, `Características a adicionar alteradas com sucesso!`);
    component.GetInitialData(this.Model.Id, `activity-id`, ServerUrls.GetUrl(`from-to/activity-on-conclusion-award`));
  }

  public ManageUsers(): void
  {
    MaterialPopupComponent.Popup(this._Diag, FromToComponent, { UserId: this.Model.Id }, component => this._ManageUsersConfig(component));
  }
  private _ManageUsersConfig(component: FromToComponent<User>): void
  {
    component.Initialize(new User(), ConfigActivityUserItemComponent as any, `Usuários disponíveis`, `Usuários adicionados`);
    component.Configure(ActivityAllowedUserColumns.ActivityId, ActivityAllowedUserColumns.UserId, UserTable, ActivityClass, ActivityAllowedUserClass, true, false, `Usuários de ${this.Name}`);
    component.ConfigureUpdate(`Alterar usuários`, `Deseja realmente alterar as usuários da tarefa selecionada?`, `Usuários alterados com sucesso!`);
    component.GetInitialData(this.Model.Id, `activity-id`, ServerUrls.GetUrl(`from-to/activity-user`))
  }

  public get HasDailyLimit(): boolean
  {
    return ObjectUtils.GetValue<number>(this.Model, 0, ActivityColumns.DailyCount) > 0;
  }
  public get DailyLimit(): string
  {
    return `Limite diário: ${this.Model.DailyCount}`;
  }
}
