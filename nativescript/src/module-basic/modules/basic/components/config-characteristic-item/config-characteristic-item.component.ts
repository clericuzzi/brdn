import { Component, OnInit } from '@angular/core';
// utils
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';
// models
import { Characteristic, CharacteristicColumns } from '@src/external/plugin-utils/business/models/models/characteristic.model';
// services
// components
// behaviours
import { SelectListItem } from 'clericuzzi-lib/business/behaviours/select-list-item.behaviour';

@Component({
  selector: 'app-config-characteristic-item',
  templateUrl: './config-characteristic-item.component.html'
})
export class ConfigCharacteristicItemComponent extends SelectListItem<Characteristic> implements OnInit
{
  constructor()
  {
    super(null);
  }

  ngOnInit()
  {
  }

  public get Name(): string
  {
    return ObjectUtils.GetValue(this.Model, `carregando`, CharacteristicColumns.Name);
  }
}