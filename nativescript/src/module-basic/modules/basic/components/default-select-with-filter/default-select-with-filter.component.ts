import { Subject } from 'rxjs';
import { ComponentType } from '@angular/cdk/portal';
import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';

// utils
import { CustomField } from '@src/external/plugin-utils/business/models/crud/custom-field.model';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// models
import { Crudable } from '@src/external/plugin-utils/business/models/crud/crudable.model';
import { RequestPagination } from '@src/external/plugin-utils/business/models/request/pagination/request-pagination.model';
import { BatchResponseMessage, BatchResponseMessageList } from '@src/external/plugin-utils/business/models/response/batch-response-message.model';

// services
import { AutoCompleteFetchService } from 'clericuzzi-lib/services/auto-complete-fetch.service';

// components
import { CrudFilterComponent } from 'clericuzzi-lib/modules/shared/crud/crud-filter/crud-filter.component';
import { CrudSelectComponent } from 'clericuzzi-lib/modules/shared/crud/crud-select/crud-select.component';
import { HostBindedComponent } from 'clericuzzi-lib/modules/shared/util-components/host-binded/host-binded.component';

// behaviours
import { SelectListItem } from 'clericuzzi-lib/business/behaviours/select-list-item.behaviour';
import { CrudableTableColumnDefinitionModel } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';
import { FormControl } from '@angular/forms';
import { FormControlComponent } from 'clericuzzi-lib/modules/shared/form-controls/form-control.component';

@Component({
  selector: 'module-basic-default-select-with-filter',
  templateUrl: './default-select-with-filter.component.html'
})
export class DefaultSelectWithFilterComponent<T extends Crudable, V extends Crudable> extends HostBindedComponent implements OnInit
{
  @ViewChild(`filter`) public PanelFilter: CrudFilterComponent;
  @ViewChild(`select`) public PanelSelect: CrudSelectComponent<T>;

  private _BaseObject: T;
  public OnFilterRequested: Subject<void> = new Subject<void>();
  public CustomFilterRequestedAction: Function;
  public CallbackHandler: Function;

  public HasFilterPanel: boolean = false;
  public FilterModel: V;
  public get FilterModelJson(): any
  {
    if (!ObjectUtils.NullOrUndefined(this.PanelFilter))
      return ObjectUtils.NullOrUndefined(this.PanelFilter.CurrentModel) ? null : this.PanelFilter.CurrentModel.ToJson();
    else
    {
      if (ObjectUtils.NullOrUndefined(this.FilterModel))
        return null;
      else
      {
        if (this.FilterModel instanceof Crudable)
          return this.FilterModel.ToJson();
        else
          return this.FilterModel;
      }
    }
  }
  constructor(
    private _ChangeDetector : ChangeDetectorRef,
    private _AutocompleteFetch: AutoCompleteFetchService,
  )
  {
    super(null);
  }

  ngOnInit()
  {
  }

  public get ItemsCount(): number
  {
    if (ObjectUtils.NullOrUndefined(this.PanelSelect) && ObjectUtils.NullOrUndefined(this.PanelSelect.Items))
      return this.PanelSelect.Items.length;
    else
      return 0;
  }


  public AddAction(key: string, title: string, action: Function, multiItem: boolean, iconClass: string, iconDefinition: string, selectedItemsNeeded: number): void
  {
    this.PanelSelect.AddAction(key, title, action, multiItem, iconClass, iconDefinition, selectedItemsNeeded);
  }
  public SetItems(items: T[], pagination: RequestPagination = null): void
  {
    this.PanelSelect.SetItems(items, pagination);
  }
  public SetFilter(filterObject: V, baseObject: T = null): void
  {
    this.FilterModel = filterObject;
    if (!ObjectUtils.NullOrUndefined(this.PanelFilter))
      this.PanelFilter.CurrentModel = filterObject;
    this.PanelSelect.AddRequestValue(`filter`, this.FilterModelJson);
    if (!ObjectUtils.NullOrUndefined(baseObject))
      this.PanelSelect.SetBaseObject(baseObject);
  }
  public Init(baseObject: T, filterModel: V, template: ComponentType<SelectListItem<T>>, multiselect: boolean = false, loadsOnStart: boolean = false): void
  {
    if (ObjectUtils.NullOrUndefined(template))
      throw Error(`O template não foi inicializado...`);

    this._BaseObject = baseObject;
    this.FilterModel = filterModel;
    this._ChangeDetector.detectChanges();

    this.PanelSelect.DataTemplate = template;
    this.PanelSelect.LoadsOnStart = loadsOnStart;
    this.PanelSelect.SetBaseObject(baseObject);
    this.PanelSelect.SetMultiSelect(multiselect);
    this.PanelSelect.AddRequestValue(`filter`, this.FilterModelJson);
    this.ListenTo(this.PanelSelect.OnCustomSelectCallback, response => this.HandleCallback(response));
  }
  public InitFilters(... columns: string[]): void
  {
    this.HasFilterPanel = true;
    this._ChangeDetector.detectChanges();

    if (!ObjectUtils.NullOrUndefined(this.PanelFilter))
    {
      this.PanelFilter.CurrentModel = this.FilterModel;
      this.ListenTo(this.PanelFilter.OnFilterRequested, filter => this.Reload());
    }

    for (let column of columns)
    {
      let columnDefinition: CrudableTableColumnDefinitionModel = this.FilterModel.GetColumnDefinition(column);
      let component: FormControlComponent<any> = this.PanelFilter.Addfilter(columnDefinition, `Pesquisa por ${columnDefinition.ReadableName.toLocaleLowerCase()}`);
      this.ListenTo(component.OnLeave, () => this._DefineSelectFilter())
    }
  }
  public GetAutocompleteData(url: string, ... customValues: CustomField[])
  {
    if (!ObjectUtils.NullOrUndefined(this.PanelFilter))
      this._AutocompleteFetch.FetchAutocompleteData(url, response => this.PanelFilter.LoadAutoCompleteData(response), ... customValues);
  }

  public HandleCallback(response: BatchResponseMessage<T>): void
  {
    let items: T[] = null;
    if (!ObjectUtils.NullOrUndefined(this.CallbackHandler))
      items = this.CallbackHandler(response);

    this.PanelSelect.Callback(response, this._BaseObject, items);
  }
  public HandleCallbackList(response: BatchResponseMessageList, dataKey: string): void
  {
    this.PanelSelect.LoaderHide();
    let items: T[] = response.GetList(dataKey, this._BaseObject);
    if (!ObjectUtils.NullOrUndefined(items))
      this.PanelSelect.SetItems(items);
  }

  private _DefineSelectFilter(): void
  {
    this.PanelSelect.AddRequestValue(`filter`, this.FilterModelJson);
  }
  public Reload(... optionalData: CustomField[]): void
  {
    if (ObjectUtils.NullOrUndefined(this.CustomFilterRequestedAction))
    {
      this._DefineSelectFilter();
      if (!ObjectUtils.NullUndefinedOrEmptyArray(optionalData))
        for (let option of optionalData)
          this.PanelSelect.AddRequestValue(option.Key, option.Value);

      this.PanelSelect.Reload();
    }
    else
      this.CustomFilterRequestedAction();
  }

  public LoadExternalData(url: string, ... customFields: CustomField[]): void
  {
    this._AutocompleteFetch.FetchAutocompleteData(url, response => this._LoadExternalDataCallback(response, customFields));
  }
  private _LoadExternalDataCallback(response: BatchResponseMessageList, customFields: CustomField[]): void
  {
    for (let customField of customFields)
      this.PanelFilter.FillAutoCompleteData(response, customField.Key, customField.Value as string);

    this.PanelFilter.Sync();
  }
}
