import 'hammerjs';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// clericuzzi lib module
import { SharedModule } from 'clericuzzi-lib/modules/shared/shared.module';
// from-to
import { FromToComponent } from './components/from-to/from-to.component';
// components
import { DefaultSelectWithFilterComponent } from './components/default-select-with-filter/default-select-with-filter.component';
// accounts
import { AccountSelectionComponent } from './components/account-selection/account-selection.component';
// crud & config list
import { CrudListItemComponent } from './components/crud-list-item/crud-list-item.component';
import { ConfigUserNewComponent } from './components/config-user-new/config-user-new.component';
import { ConfigUserItemComponent } from './components/config-user-item/config-user-item.component';
import { ConfigUserEditComponent } from './components/config-user-edit/config-user-edit.component';
import { ConfigActivityItemComponent } from './components/config-activity-item/config-activity-item.component';
import { ConfigPermissionItemComponent } from './components/config-permission-item/config-permission-item.component';
import { ConfigAccountListItemComponent } from './components/config-account-list-item/config-account-list-item.component';
import { ConfigActivityUserItemComponent } from './components/config-activity-user-item/config-activity-user-item.component';
import { ConfigCharacteristicItemComponent } from './components/config-characteristic-item/config-characteristic-item.component';
import { ConfigPermissionUserItemComponent } from './components/config-permission-user-item/config-permission-user-item.component';

@NgModule({
  imports: [
    HttpModule,
    FormsModule,
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,

    SharedModule,

  ],
  declarations: [
    FromToComponent,

    AccountSelectionComponent,
    DefaultSelectWithFilterComponent,

    CrudListItemComponent,
    ConfigUserNewComponent,
    ConfigUserItemComponent,
    ConfigUserEditComponent,
    ConfigActivityItemComponent,
    ConfigPermissionItemComponent,
    ConfigAccountListItemComponent,
    ConfigActivityUserItemComponent,
    ConfigCharacteristicItemComponent,
    ConfigPermissionUserItemComponent,
  ],
  exports: [
    HttpModule,
    FormsModule,
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,

    DefaultSelectWithFilterComponent,

    CrudListItemComponent,
  ],
  entryComponents: [
    FromToComponent,

    CrudListItemComponent,
    ConfigUserNewComponent,
    ConfigUserItemComponent,
    ConfigUserEditComponent,
    ConfigActivityItemComponent,
    ConfigPermissionItemComponent,
    ConfigAccountListItemComponent,
    ConfigActivityUserItemComponent,
    ConfigCharacteristicItemComponent,
    ConfigPermissionUserItemComponent,
  ]
})
export class ClericuzziModuleBasic { }
