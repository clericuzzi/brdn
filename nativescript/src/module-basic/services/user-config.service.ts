import { Injectable } from '@angular/core';

// utils
import { ServerUrls } from '@src/external/plugin-utils/utils/server-urls';

// models
import { User } from '@src/external/plugin-utils/business/models/models/user.model';
import { RequestMessage } from '@src/external/plugin-utils/business/models/request/request-message.model';
import { BatchResponseMessageList } from '@src/external/plugin-utils/business/models/response/batch-response-message.model';

// services
import { HttpService } from 'clericuzzi-lib/services/http.service';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';
import { StatusResponseMessage } from '@src/external/plugin-utils/business/models/response/status-response-message.model';
import { CustomField } from '@src/external/plugin-utils/business/models/crud/custom-field.model';

@Injectable()
export class UserConfigService
{
  constructor(
    private _Http: HttpService,
  )
  {
  }

  public NewUser(user: User, permissions: any[], callback: Function): void
  {
    let requestMesage: RequestMessage = new RequestMessage(ServerUrls.GetUrl(`crud/user/new`));
    requestMesage.Add(`model`, user.ToJson());
    requestMesage.Add(`password`, user.Password);
    requestMesage.Add(`permissions`, permissions);

    let response: StatusResponseMessage = new StatusResponseMessage();
    this._Http.Post(requestMesage, response, () => callback(response));
  }
  public UpdateDefinitions(url: string, paramValue: number, definitions: any[], callback: Function): void
  {
    let requestMesage: RequestMessage = new RequestMessage(url);
    requestMesage.Add(`param`, paramValue);
    requestMesage.Add(`definitions`, definitions);

    let response: StatusResponseMessage = new StatusResponseMessage();
    this._Http.Post(requestMesage, response, () => callback(response));
  }
  public GetDefinitions(url: string, paramName: string, paramValue: any, callback: Function, ... customFields: CustomField[]): void
  {
    let requestMesage: RequestMessage = new RequestMessage(url);
    if (!ObjectUtils.NullOrUndefined(paramValue))
      requestMesage.Add(paramName, paramValue);

    if (!ObjectUtils.NullUndefinedOrEmptyArray(customFields))
      for (let customField of customFields)
        requestMesage.Add(customField.Key, customField.Value);

    let response: BatchResponseMessageList = new BatchResponseMessageList();
    this._Http.Post(requestMesage, response, () => callback(response));
  }
}