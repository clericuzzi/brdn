/** This class defines the behavior for a link within a grid template */
export class CrudListItemModel
{
    constructor(
        /** the page that the user will be redirected when the item is clicked */
        public Link: string,
        /** the page module */
        public Prefix: string,
        /** the text displayed in the component */
        public Text: string,
        /** the action triggered when the user clicks on the item */
        public Action: () => void,
    )
    {
    }
}
