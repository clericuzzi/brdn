export interface IFromToable
{
    DefineItemProperty(propertyName: string): void;
    DefineUserProperty(propertyName: string): void;
}