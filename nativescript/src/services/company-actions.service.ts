import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';
// utils
import { ServerUrls } from '@src/external/plugin-utils/utils/server-urls';
import { SnackUtils } from '@src/external/plugin-utils/utils/snack-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';
// models
import { CompanyItem } from '@src/business/models/company-item.model';
import { CompanyCall } from '@src/entities/companyCall.model';
import { RequestMessage } from '@src/external/plugin-utils/business/models/request/request-message.model';
import { CompanyCallback } from '@src/entities/companyCallback.model';
import { ResponseMessage } from '@src/external/plugin-utils/business/models/response/response-message.model';
import { BatchResponseMessage } from '@src/external/plugin-utils/business/models/response/batch-response-message.model';
import { StatusResponseMessage } from '@src/external/plugin-utils/business/models/response/status-response-message.model';
// services
import { HttpService } from '@src/clericuzzi-lib/services/http.service';
import { UserDataService } from '@src/clericuzzi-lib/services/user-data.service';
// components
// behaviours

@Injectable()
export class CompanyActionsService
{
  public OnCompanyCallbackAdded: Subject<CompanyCallback> = new Subject<CompanyCallback>();
  public OnCompanyLoaded: Subject<any> = new Subject<any>();
  public CurrentModel: CompanyItem;
  constructor(
    private _Http: HttpService,
    private _UserData: UserDataService,
  )
  {
  }

  public CampaignId: number;

  public Clear(): void
  {
    this.CurrentModel = null;
  }

  public RequestCheck(companyId: number, callback: (response: StatusResponseMessage) => void): void
  {
    const request: RequestMessage = new RequestMessage(ServerUrls.GetUrl(`companies/request-check`));
    request.Add(`company-id`, companyId);

    const response: StatusResponseMessage = new StatusResponseMessage();
    this._Http.Post(request, response, () => callback(response));
  }

  public NewCall(callback: (response: BatchResponseMessage<CompanyCall>) => void): void
  {
    const request: RequestMessage = new RequestMessage(ServerUrls.GetUrl(`companies/new-call`));
    const newCall: CompanyCall = new CompanyCall(null, this._UserData.Id, this.CurrentModel.Id, null, null);
    request.Add(`model`, newCall);

    const response: BatchResponseMessage<CompanyCall> = new BatchResponseMessage<CompanyCall>();
    this._Http.Post(request, response, () => callback(response));
  }

  public NewLead(callback: () => void): void
  {
    let url: string = `companies/new-lead`;
    if (this.CampaignId === 5)
      url = `companies/new-lead-from-file`;

    const request: RequestMessage = new RequestMessage(ServerUrls.GetUrl(url));
    if (this.CampaignId === 5)
      request.Add(`import-file-id`, 1);
    else
      request.Add(`campaign-id`, this.CampaignId);

    const response: ResponseMessage<CompanyItem> = new ResponseMessage<CompanyItem>();
    this._Http.Post(request, response, () => this._NewLeadSohoCallback(response, callback));
  }
  public NewLeadMobile(callback: () => void): void
  {
    const request: RequestMessage = new RequestMessage(ServerUrls.GetUrl(`companies/new-lead`));

    const response: ResponseMessage<CompanyItem> = new ResponseMessage<CompanyItem>();
    this._Http.Post(request, response, () => this._NewLeadSohoCallback(response, callback));
  }
  public NewLeadSohoHighSpeed(callback: () => void): void
  {
    const request: RequestMessage = new RequestMessage(ServerUrls.GetUrl(`companies/new-lead`));

    const response: ResponseMessage<CompanyItem> = new ResponseMessage<CompanyItem>();
    this._Http.Post(request, response, () => this._NewLeadSohoCallback(response, callback));
  }
  private _NewLeadSohoCallback(response: ResponseMessage<CompanyItem>, callback: () => void): void
  {
    this.CurrentModel = response.Parse(new CompanyItem());
    if (!ObjectUtils.NullOrUndefined(this.CurrentModel))
      callback();
  }

  public LoadModel(): void
  {
    const request: RequestMessage = new RequestMessage(ServerUrls.GetUrl(`companies/load`));
    request.Add(`company-id`, this.CurrentModel.Id);

    const response: ResponseMessage<CompanyItem> = new ResponseMessage<CompanyItem>();
    this._Http.Post(request, response, () => this._LoadModelCallback(response));
  }
  private _LoadModelCallback(response: ResponseMessage<CompanyItem>): void
  {
    this.CurrentModel = response.Parse(new CompanyItem());

    this.OnCompanyLoaded.next();
  }

  public LoadUserCallbacks(callback: (response: BatchResponseMessage<CompanyCallback>) => void): void
  {
    const request: RequestMessage = new RequestMessage(ServerUrls.GetUrl(`callbacks/load`));

    const response: BatchResponseMessage<CompanyCallback> = new BatchResponseMessage<CompanyCallback>();
    this._Http.Post(request, response, () => callback(response));
  }
}
