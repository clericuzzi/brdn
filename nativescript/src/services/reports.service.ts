import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { LoaderService } from '@src/clericuzzi-lib/services/loader.service';
import { UserDataService } from '@src/clericuzzi-lib/services/user-data.service';
import { HttpService } from '@src/clericuzzi-lib/services/http.service';
import { RequestMessage } from '@src/external/plugin-utils/business/models/request/request-message.model';
import { ResponseMessage } from '@src/external/plugin-utils/business/models/response/response-message.model';
import { ServerUrls } from '@src/external/plugin-utils/utils/server-urls';
import { EmployeeFilter } from '@src/business/filters/employee.filter';
import { ReportCallsOrSales } from '@src/business/models/reports-calls-or-sales.model';
import { DateFilter } from '@src/business/filters/date.filter';
import { ReportMining } from '@src/business/models/report-mining.model';

// utils
// models
// services
// components
// behaviours

@Injectable()
export class ReportService
{
  constructor(
    private _Http: HttpService,
    private _Snack: MatSnackBar,
    private _Loader: LoaderService,
    private _UserData: UserDataService,
  )
  {
  }

  public Calls(filter: EmployeeFilter, callback: (response: ResponseMessage<ReportCallsOrSales>) => void): void
  {
    const request: RequestMessage = new RequestMessage(ServerUrls.GetUrl(`stats/calls`));
    request.Add(`to`, filter.DateTo);
    request.Add(`from`, filter.DateFrom);
    request.Add(`employee-ids`, filter.EmployeeIds);

    const response: ResponseMessage<ReportCallsOrSales> = new ResponseMessage<ReportCallsOrSales>();
    this._Http.Post(request, response, () => callback(response));
  }
  public Sells(filter: EmployeeFilter, callback: (response: ResponseMessage<ReportCallsOrSales>) => void): void
  {
    const request: RequestMessage = new RequestMessage(ServerUrls.GetUrl(`stats/sells`));
    request.Add(`to`, filter.DateTo);
    request.Add(`from`, filter.DateFrom);
    request.Add(`employee-ids`, filter.EmployeeIds);

    const response: ResponseMessage<ReportCallsOrSales> = new ResponseMessage<ReportCallsOrSales>();
    this._Http.Post(request, response, () => callback(response));
  }
  public Mining(filter: DateFilter, callback: (response: ResponseMessage<ReportMining>) => void): void
  {
    const request: RequestMessage = new RequestMessage(ServerUrls.GetUrl(`stats/mining`));
    request.Add(`to`, filter.DateTo);
    request.Add(`from`, filter.DateFrom);

    const response: ResponseMessage<ReportMining> = new ResponseMessage<ReportMining>();
    this._Http.Post(request, response, () => callback(response));
  }
  public Evaluations(filter: DateFilter, callback: (response: ResponseMessage<ReportMining>) => void): void
  {
    const request: RequestMessage = new RequestMessage(ServerUrls.GetUrl(`stats/evaluations`));
    request.Add(`to`, filter.DateTo);
    request.Add(`from`, filter.DateFrom);

    const response: ResponseMessage<ReportMining> = new ResponseMessage<ReportMining>();
    this._Http.Post(request, response, () => callback(response));
  }
  public Tabulations(filter: EmployeeFilter, callback: (response: ResponseMessage<ReportCallsOrSales>) => void): void
  {
    const request: RequestMessage = new RequestMessage(ServerUrls.GetUrl(`stats/tabulations`));
    request.Add(`to`, filter.DateTo);
    request.Add(`from`, filter.DateFrom);
    request.Add(`employee-ids`, filter.EmployeeIds);

    const response: ResponseMessage<ReportCallsOrSales> = new ResponseMessage<ReportCallsOrSales>();
    this._Http.Post(request, response, () => callback(response));
  }
}
