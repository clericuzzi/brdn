import { MatSnackBar } from '@angular/material';
// utils
import { SnackUtils } from '@src/external/plugin-utils/utils/snack-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';
// models
import { BatchResponseMessage, BatchResponseMessageList } from '@src/external/plugin-utils/business/models/response/batch-response-message.model';
import { StatusResponseMessage } from '@src/external/plugin-utils/business/models/response/status-response-message.model';
// services
import { HttpService } from 'clericuzzi-lib/services/http.service';
import { LoaderService } from 'clericuzzi-lib/services/loader.service';
// components
// behaviours

export abstract class ServiceItem
{
    /**
     * Default empty constructor for the list's initialization
     */
    constructor(
        public _Http: HttpService,
        public _Snack: MatSnackBar,
        public _Loader: LoaderService,
    )
    {
    }

    public ShowsLoader(showLoader: boolean): void
    {
        if (showLoader)
            this._Loader.Show();
    }

    public StatusActionCallback(response: StatusResponseMessage, callback: Function = null, successMessage: string = null, errorMessage: string = null, showLoader: boolean = true): void
    {
        if (this.ShowsLoader)
            this._Loader.Hide();

        if (!ObjectUtils.NullOrUndefined(callback))
            callback(response);
        SnackUtils.HandleStatusResponse(this._Snack, response, successMessage, errorMessage);
    }
    public BatchActionCallback(response: BatchResponseMessage<any>, callback: Function = null, successMessage: string = null, errorMessage: string = null, showLoader: boolean = true): void
    {
        if (this.ShowsLoader)
            this._Loader.Hide();

        if (!ObjectUtils.NullOrUndefined(callback))
            callback(response);
        SnackUtils.HandleBatchResponse(this._Snack, response, successMessage, errorMessage);
    }
    public BatchListActionCallback(response: BatchResponseMessageList, callback: Function = null, successMessage: string = null, errorMessage: string = null, showLoader: boolean = true): void
    {
        if (this.ShowsLoader)
            this._Loader.Hide();

        if (!ObjectUtils.NullOrUndefined(callback))
            callback(response);
        SnackUtils.HandleBatchListResponse(this._Snack, response, successMessage, errorMessage);
    }
}