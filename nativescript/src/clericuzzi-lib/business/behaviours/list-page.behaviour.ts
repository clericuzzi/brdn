import { ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material';
// utils
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';
// models
import { Crudable } from '@src/external/plugin-utils/business/models/crud/crudable.model';
// services
import { UserDataService } from '@src/clericuzzi-lib/services/user-data.service';
import { PageTitleService } from '@src/clericuzzi-lib/services/page-title.service';
// import { AutoCompleteFetchService } from '@src/clericuzzi-lib/services/auto-complete-fetch.service';
import { ComponentGeneratorService } from '@src/clericuzzi-lib/services/component-generator.service';
// components
import { CrudPopupComponent } from '@src/clericuzzi-lib/modules/shared/crud/crud-popup/crud-popup.component';
import { HostBindedComponent } from '@src/clericuzzi-lib/modules/shared/util-components/host-binded/host-binded.component';
import { MaterialPopupComponent } from '@src/clericuzzi-lib/modules/material/components/material-popup/material-popup.component';
import { CrudPopupComponentModel } from '@src/clericuzzi-lib/modules/shared/crud/crud-popup/crud-popup.model';
import { DefaultSelectWithFilterComponent } from '@src/module-basic/modules/basic/components/default-select-with-filter/default-select-with-filter.component';
// behaviours

export abstract class ListPage<T extends Crudable, V extends Crudable> extends HostBindedComponent
{
  public Title: string;
  public ListTitle: string;

  public CanDelete: boolean;
  public CanInsert: boolean;
  public CanUpdate: boolean;

  public ItemComponent: any;
  public InsertItemComponent: any;
  public UpdateItemComponent: any;
  public ColumnsFilter: string[];
  public ColumnsInsert: string[];
  public ColumnsUpdate: string[];

  public BaseObject: T;
  public FilterObject: V;
  public PopupWidth: string;
  public PopupHeigth: string;
  @ViewChild(`component`, { static: false }) public Component: DefaultSelectWithFilterComponent<T, V>;
  constructor(
    public _Diag: MatDialog,
    public _UserData: UserDataService,
    public _Generator: ComponentGeneratorService,
    public autoCompleteFetch: AutoCompleteFetchService,

    public pageTitle: PageTitleService,
  )
  {
    super(pageTitle);
    this.InitFactories(this._Generator.Factory, this._Generator.Sanitizer);
  }

  ngOnInit()
  {
    this.ListenTo(this._UserData.Accounts.OnAccountChanged, () => this.AccountChanged());
  }
  public Initialize(baseObject: T, filterObject: V): void
  {
    this.BaseObject = baseObject;
    this.FilterObject = filterObject;

    this.Component.PanelSelect.Title = baseObject.TableDefinition.ListTitle;
  }

  public ActionNewItem(forceDraw: boolean = false, key: string = `add`, iconClass: string = `material-icons`, iconName: string = `add`, minimumSelection: number = 0): void
  {
    this.Component.PanelSelect.AddAction(key, this.BaseObject.TableDefinition.NewItemTitle, () => this._AddItem(), true, iconClass, iconName, minimumSelection);
    if (forceDraw)
      this.Component.PanelSelect.InitActions();
  }
  public ActionUpdateItem(forceDraw: boolean = false, key: string = `edit`, iconClass: string = `material-icons`, iconName: string = `edit`, minimumSelection: number = 1): void
  {
    this.Component.PanelSelect.AddAction(key, this.BaseObject.TableDefinition.UpdateItemTitle, () => this._UpdateItem(), false, iconClass, iconName, minimumSelection);
    if (forceDraw)
      this.Component.PanelSelect.InitActions();
  }

  public AddPopupModel(): CrudPopupComponentModel
  {
    const crudPopupModel: CrudPopupComponentModel = CrudPopupComponentModel.NewInsert(this.BaseObject, ... this.ColumnsInsert);
    crudPopupModel.KeepInserting = true;

    return crudPopupModel;
  }
  public UpdatePopupModel(): CrudPopupComponentModel
  {
    const crudPopupModel: CrudPopupComponentModel = CrudPopupComponentModel.NewUpdate(this.Component.PanelSelect.SelectedItem, ... this.ColumnsUpdate);

    return crudPopupModel;
  }

  private AccountChanged(): void
  {
    this.Component.Reload();
  }
  public _AddItem(): void
  {
    MaterialPopupComponent.Popup(this._Diag, ObjectUtils.NullOrUndefined(this.InsertItemComponent) ? CrudPopupComponent : this.InsertItemComponent, this.AddPopupModel(), component => this.AddItemAfterOpen(component), () => this.AddItemAfterClose(), this.PopupWidth, this.PopupHeigth);
  }
  private _UpdateItem(): void
  {
    MaterialPopupComponent.Popup(this._Diag, ObjectUtils.NullOrUndefined(this.InsertItemComponent) ? CrudPopupComponent : this.UpdateItemComponent, this.UpdatePopupModel(), component => this.UpdateItemAfterOpen(component), () => this.UpdateItemAfterClose(), this.PopupWidth, this.PopupHeigth);
  }

  public AddItemAfterOpen(component: CrudPopupComponent): void
  {
  }
  public AddItemAfterClose(): void
  {
    this.Component.Reload();
  }
  public UpdateItemAfterOpen(component: CrudPopupComponent): void
  {
  }
  public UpdateItemAfterClose(): void
  {
    this.Component.Reload();
  }
}