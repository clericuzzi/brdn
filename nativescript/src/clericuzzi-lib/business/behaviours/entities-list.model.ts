import { ObjectUtils } from "../../utils/object-utils";

/**
 * A generic implementation for a class that holds a list of items from a particular type T
 */
export abstract class EntitiesList<T>
{
    /**
     * A list of items
     */
    public List: T[];

    /**
     * Default empty constructor for the list's initialization
     */
    constructor()
    {
        this.List = [];
    }

    /**
     * Adds a new sorting info to the list of fields to base the sorting on
     * @param items a list of items to be added
     */
    public Add(... items: T[]): void
    {
        for (let item of items)
            this.List.push(item);
    }
    /**
     * Provides a sorting info to be removed from the fields list
     * @param items a list of items to be removed
     */
    public Remove(... items: T[]): void
    {
        for (let item of items)
        {
            let index: number = this.List.indexOf(item);
            if (index >= 0)
                this.List.splice(index, 1);
        }
    }
    
    /**
     * Counts the items in the list
     */
    public get Count(): number
    {
        if (ObjectUtils.NullUndefinedOrEmptyArray(this.List))
            return 0;
        else
            return this.List.length;
    }

    /**
     * Retrieves an item from the list by its name
     * @param itemName the item to be searched
     * @returns the item where it's name matches with the given one
     */
    public abstract Get(itemName: string): T;

    /**
     * Compares the given string with all field names and returs TRUE if there are ANY items with 
     * a part of it's field name matchin the given string
     * @param itemName the item to be searched
     * @returns TRUE if the given string is contained in ANY of the field's name
     */
    public abstract Contains(itemName: string): boolean;
}