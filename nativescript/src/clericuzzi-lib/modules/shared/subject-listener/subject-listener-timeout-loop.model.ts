export class SubjectListenerTimeoutLoopModel
{
    public Name: string;
    public Action: Function;
    public IsAlive: boolean;
    public Timeout: number;
}