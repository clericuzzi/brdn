import { MatSnackBar } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { OnInit, HostBinding, ViewContainerRef, ComponentFactoryResolver, ViewChild, ElementRef } from '@angular/core';

// utils
import { SnackUtils } from '@src/external/plugin-utils/utils/snack-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';
import { InputDecoration } from '@src/external/plugin-utils/utils/input-decoration';

// models
import { BaseMessage } from '@src/external/plugin-utils/business/models/response/base-message.model';
import { ComboInputValueModel } from '../../form-controls/custom-inputs/combo-input/combo-input-value.model';
import { BatchResponseMessageList } from '@src/external/plugin-utils/business/models/response/batch-response-message.model';
import { CrudableTableColumnDefinitionModel } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';

// services
import { PageTitleService } from 'clericuzzi-lib/services/page-title.service';

// components
import { CrudItemComponent } from '../../crud/crud-item/crud-item.component';
import { ComboInputComponent } from '../../form-controls/custom-inputs/combo-input/combo-input.component';
import { FormControlComponent } from '../../form-controls/form-control.component';
import { SubjectListenerComponent } from '../../subject-listener/subject-listener.component';
import { AutocompleteInputComponent } from '../../form-controls/autocomplete-input/autocomplete-input.component';

export class HostBindedComponent extends SubjectListenerComponent implements OnInit
{
  @ViewChild(`closeButton`) private _CloseButton: ElementRef;
  @HostBinding(`class`) ComponentClass: string = `ng-star-inserted full-size flex-column`;
  public readonly ClassBinding: string = `ComponentClass`;

  private _PageTitle: PageTitleService;
  public FormItems: FormControlComponent<any>[] = [];

  public CurrentModel: any;

  public Busy: boolean;
  public Factory: ComponentFactoryResolver;
  public Sanitizer: DomSanitizer;
  constructor(
    public pageTitle: PageTitleService,
  )
  {
    super();
    this._PageTitle = pageTitle;
  }

  ngOnInit()
  {
  }

  private _ValidateFactories(): void
  {
    if (ObjectUtils.NullOrUndefined(this.Factory))
      throw new Error(`A fábrica de componentes não foi inicializada...`);

    if (ObjectUtils.NullOrUndefined(this.Sanitizer))
      throw new Error(`O limitador do DOM não foi inicializado...`);
  }

  public InitUpdateProperty(url: string, ...propertyNames: string[]): void
  {
    if (!ObjectUtils.NullUndefinedOrEmptyArray(this.FormItems))
      for (let propertyName of propertyNames)
      {
        let component: FormControlComponent<any> = this.FormItems.find(i => i.Property == propertyName);
        if (!ObjectUtils.NullOrUndefined(component))
        {
          component.UpdatesModelOnValueChanged = true;
          component.UpdatesModelOnValueChangedUrl = url;
        }
      }
  }
  public RevokeUpdateProperty(...propertyNames: string[]): void
  {
    if (!ObjectUtils.NullUndefinedOrEmptyArray(this.FormItems))
      for (let propertyName of propertyNames)
      {
        let component: FormControlComponent<any> = this.FormItems.find(i => i.Property == propertyName);
        if (!ObjectUtils.NullOrUndefined(component))
        {
          component.UpdatesModelOnValueChanged = false;
          component.UpdatesModelOnValueChangedUrl = null;
        }
      }
  }

  public InitFactories(factory: ComponentFactoryResolver, sanitizer: DomSanitizer): void
  {
    this.Factory = factory;
    this.Sanitizer = sanitizer;
  }
  public InitModel(model: any): void
  {
    this.CurrentModel = model;
  }
  public AddComponent<T>(container: ViewContainerRef, columnDefinition: CrudableTableColumnDefinitionModel, placeholder: string = null, onSendAction: Function = null, maxLength: number = 0, index: number = null): T
  {
    this._ValidateFactories();

    let component: FormControlComponent<any> = columnDefinition.CreateComponent(container, this.Factory, this.CurrentModel, null, this.Sanitizer, null, index)
    component.Required = !columnDefinition.IsNullable;
    component.MarginTop = -12;
    component.HasShadow = false;
    if (ObjectUtils.GetValue(columnDefinition.WidthPixels, 0) == 0)
      component.CustomClass = `margin-r flex-grow`;
    else
      component.CustomClass = `margin-r`;
    component.Columndefinition = columnDefinition;
    component.ComponentWidthPx = ObjectUtils.GetValue(columnDefinition.WidthPixels, null);
    component.ComponentWidthPercentage = null;
    if (maxLength > 0)
      component.MaxLength = maxLength;
    if (!ObjectUtils.NullUndefinedOrEmpty(placeholder))
      component.Placeholder = placeholder;

    this.FormItems.push(component);
    if (onSendAction != null)
      this.ListenTo(component.OnSend, () => onSendAction());

    return (component as any) as T;
  }

  public ClearComponent(componentName: string): void
  {
    let component: FormControlComponent<any> = this.GetComponent(componentName);
    if (!ObjectUtils.NullOrUndefined(component))
      component.ClearValue();
  }
  public GetComponent<T>(componentName: string): T
  {
    var component: FormControlComponent<any> = this.FormItems.find(i => i.Property == componentName);
    if (!ObjectUtils.NullOrUndefined(component))
      return (component as any) as T;
    else
      return null;
  }

  public Sync(model: any = null): void
  {
    if (!ObjectUtils.NullOrUndefined(model))
      this.CurrentModel = model;
    if (!ObjectUtils.NullUndefinedOrEmptyArray(this.FormItems))
      for (let component of this.FormItems)
      {
        if (!ObjectUtils.NullOrUndefined(this.CurrentModel) && !ObjectUtils.NullUndefinedOrEmpty(component.Property))
          component.Update(this.CurrentModel);
      }
  }
  public Validate(): void
  {
    if (!ObjectUtils.NullUndefinedOrEmptyArray(this.FormItems))
      for (let component of this.FormItems)
        component.Validate();
  }
  public ValidateModel(): boolean
  {
    let mandatoryComponents: FormControlComponent<any>[] = this.FormItems.filter(i => !i.Columndefinition.IsNullable);
    for (let mandatoryComponent of mandatoryComponents)
      if (!this.CurrentModel.ValidateProperty(mandatoryComponent.Columndefinition))
        return false;

    return true;
  }

  public GetTitle(): string
  {
    return this._PageTitle.Title;
  }
  public SetTitle(title: string): void
  {
    if (!ObjectUtils.NullUndefinedOrEmpty(title))
      this._PageTitle.SetTitle(title);
  }
  public ClearTitle(): void
  {
    if (!ObjectUtils.NullOrUndefined(this._PageTitle))
      this._PageTitle.SetTitle(null);
  }
  public ClearComponentClass(): void
  {
    this.ComponentClass = `ng-star-inserted`;
  }

  public AddClasses(...classNames: string[]): void
  {
    for (let className of classNames)
      InputDecoration.BindingAddClass(this, this.ClassBinding, className);
  }
  public RemoveClasses(...classNames: string[]): void
  {
    for (let className of classNames)
      InputDecoration.BindingRemoveClass(this, this.ClassBinding, className);
  }
  public Hide(animated: boolean = true): void
  {
    InputDecoration.BindingHide(this, this.ClassBinding, animated);
  }
  public Show(animated: boolean = true): void
  {
    InputDecoration.BindingShow(this, this.ClassBinding, animated);
  }

  public FillComboData(response: BatchResponseMessageList, dataKey: string, componentName: string, startSelected: boolean = false): void
  {
    let component: ComboInputComponent<Number> = this.GetComponent(componentName);
    if (!ObjectUtils.NullOrUndefined(component))
    {
      let items: ComboInputValueModel<number>[] = response.GetArrayItems(dataKey) as ComboInputValueModel<number>[];
      if (!ObjectUtils.NullUndefinedOrEmptyArray(items))
        component.SetItems(items);
      if (startSelected)
        component.Select(items[0].Id);
    }
  }
  public FillAutoCompleteData(response: BatchResponseMessageList, dataKey: string, componentName: string, startSelected: boolean = false): void
  {
    let component: AutocompleteInputComponent<Number> = this.GetComponent(componentName);
    if (!ObjectUtils.NullOrUndefined(component) && !ObjectUtils.NullOrUndefined(response) && response.HasKey(dataKey))
    {
      let items: ComboInputValueModel<number>[] = response.GetArrayItems(dataKey) as ComboInputValueModel<number>[];
      if (!ObjectUtils.NullUndefinedOrEmptyArray(items))
      {
        component.SetItems(items);
        if (startSelected)
          component.Select(items[0].Id);
      }
    }
  }

  public CloseComponent(): void
  {
    if (!ObjectUtils.NullOrUndefined(this._CloseButton))
      setTimeout(() => this._CloseButton.nativeElement.click(), 250);
  }
  public HandleResponse(response: BaseMessage, snackBar: MatSnackBar, errorMessage: string = null): boolean
  {
    let returnValue: boolean = false;
    if (ObjectUtils.NullOrUndefined(response) || ObjectUtils.NullOrUndefined(response.HasErrors))
      SnackUtils.OpenError(snackBar, ObjectUtils.NullUndefinedOrEmpty(errorMessage) ? `Falha no envio da requisição` : errorMessage);
    else
    {
      if (response.HasErrors)
        SnackUtils.OpenError(snackBar, ObjectUtils.NullUndefinedOrEmpty(errorMessage) ? `Falha no envio da requisição` : errorMessage);
      else
        returnValue = true;
    }

    return returnValue;
  }

  public SetUpdateUrl(url: string): void
  {
    if (ObjectUtils.NullUndefinedOrEmptyArray(this.FormItems))
      return;

    for (let formItem of this.FormItems)
      formItem.SetUpdateUrl(url);
  }
}