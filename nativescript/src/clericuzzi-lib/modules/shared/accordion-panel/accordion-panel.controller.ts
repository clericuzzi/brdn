import { AccordionPanelComponent } from './accordion-panel.component';
import { ComponentFactoryResolver, ViewContainerRef, Component } from '@angular/core';
import { SubjectListenerComponent } from '../subject-listener/subject-listener.component';

@Component({
    selector: 'app-accordion-controller',
    template: '',
  })
export class AccordionPanelController extends SubjectListenerComponent
{
    constructor(
        private Container: ViewContainerRef,
        private FactoryResolver: ComponentFactoryResolver,
    )
    {
        super();
    }

    public Items: AccordionPanelComponent[] = [];
    public IsAlive: boolean = true;
    public SingleSelection: boolean = true;

    public New(text: string, action: Function = null, materialIcon: string = null, fontAwesomeIcon: string = null, padding: number = 0, iconPadding: number = 12, separatorClass: string = `line-gray`): AccordionPanelComponent
    {
        var newPanel: AccordionPanelComponent = AccordionPanelComponent.New(this.Container, this.FactoryResolver, text, action, materialIcon, fontAwesomeIcon, padding, iconPadding, separatorClass);
        this.Items.push(newPanel);

        this.ListenTo(newPanel.OnStateOpened, opened => this._ValidateSelection(opened));
        return newPanel;
    }
    public Clear(): void
    {
        this.Container.clear();
    }

    private _ValidateSelection(opened: AccordionPanelComponent): void
    {
        if (this.SingleSelection)
            for (var item of this.Items)
                if (item != opened && item.Opened)
                    item.ForceClose();
    }
}