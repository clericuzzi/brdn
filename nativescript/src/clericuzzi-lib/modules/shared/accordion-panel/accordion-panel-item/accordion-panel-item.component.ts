import { Component, OnInit, HostBinding } from '@angular/core';
import { ObjectUtils } from '../../../../utils/object-utils';

@Component({
  selector: 'app-accordion-panel-item',
  templateUrl: './accordion-panel-item.component.html',
  styleUrls: ['./accordion-panel-item.component.css']
})
export class AccordionPanelItemComponent implements OnInit
{ 
  @HostBinding(`class`) ContainerClass: string = ``;

  public Text: string;
  public Image: string;
  public Counter: string;
  
  public Action: Function;
  public Height: number = 35;
  public Padding: number = 0;
  public IsDisabled: boolean = false;
  public IconPadding: number = 8;
  public MaterialIcon: string;
  public FontAwesomeIcon: string;

  public get HasIcon(): boolean
  {
    return !ObjectUtils.NullOrUndefined(this.Image) || !ObjectUtils.NullOrUndefined(this.FontAwesomeIcon)  || !ObjectUtils.NullOrUndefined(this.MaterialIcon);
  }
  
  public get ItemHeight(): string
  {
    return `${this.Height}px`;
  }
  public get ItemPadding(): string
  {
    return `${this.Padding}px`;
  }
  public get RightMargin(): string
  {
    return `${this.IconPadding}px`;
  }

  public SetHeight(value: number): AccordionPanelItemComponent
  {
    this.Height = value;

    return this;
  }
  public SetPadding(value: number): AccordionPanelItemComponent
  {
    this.Padding = value;

    return this;
  }

  constructor()
  {
  }

  ngOnInit()
  {
  }
  public ClickAction(): void
  {
    if (this.Action != null)
      this.Action();
  }
}