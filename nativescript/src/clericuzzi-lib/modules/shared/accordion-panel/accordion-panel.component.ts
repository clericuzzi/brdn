import { Subject } from 'rxjs';
import { Component, OnInit, ElementRef, ViewChild, ViewContainerRef, ComponentFactoryResolver, HostBinding } from '@angular/core';

// utils
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';
import { InputDecoration } from '@src/external/plugin-utils/utils/input-decoration';

// components
import { AccordionPanelItemComponent } from './accordion-panel-item/accordion-panel-item.component';

@Component({
  selector: 'app-accordion-panel',
  templateUrl: './accordion-panel.component.html',
  styleUrls: ['./accordion-panel.component.css']
})
export class AccordionPanelComponent extends AccordionPanelItemComponent implements OnInit
{
  @HostBinding(`class`) ContainerClass: string = ``;

  public OnStateOpened: Subject<AccordionPanelComponent> = new Subject<AccordionPanelComponent>();
  public static New(container: ViewContainerRef, factoryResolver: ComponentFactoryResolver, text: string, action: Function, materialIcon: string = null, fontAwesomeIcon: string = null, padding: number = 0, iconPadding: number = 12, separatorClass: string = `line-gray`): AccordionPanelComponent
  {
    let panel: AccordionPanelComponent;
    let factory = factoryResolver.resolveComponentFactory(AccordionPanelComponent);
    let component = factory.create(container.parentInjector);
    container.insert(component.hostView);

    panel = component['_component'];
    panel.Height = 30;
    panel.Text = text;
    panel.Action = action;
    panel.Padding = padding;
    panel.IconPadding = iconPadding;
    if (!ObjectUtils.NullOrUndefined(action))
      panel.Action = action;

    if (!ObjectUtils.NullUndefinedOrEmpty(separatorClass))
      InputDecoration.AddClass(panel.Separator, separatorClass);

    if (!ObjectUtils.NullUndefinedOrEmpty(materialIcon))
      panel.MaterialIcon = materialIcon;

    if (!ObjectUtils.NullUndefinedOrEmpty(fontAwesomeIcon))
      panel.FontAwesomeIcon = fontAwesomeIcon

    return panel;
  }

  public get HasItems(): boolean
  {
    return this.Items.length > 0;
  }
  public Items: AccordionPanelItemComponent[] = [];
  public Opened: boolean = false;
  public SeparatorClass: string;

  @ViewChild(`icon`) Icon: ElementRef;
  @ViewChild(`panel`) Panel: ElementRef;
  @ViewChild(`separator`) Separator: ElementRef;
  @ViewChild('items', { read: ViewContainerRef }) _ContainerItems: ViewContainerRef;

  constructor(
    public ComponentFactoryResolver: ComponentFactoryResolver,
  )
  {
    super();
  }

  ngOnInit()
  {
  }

  public AddItem(text: string, action: Function, materialIcon: string = null, fontAwesomeIcon: string = null): AccordionPanelItemComponent
  {
    let factory = this.ComponentFactoryResolver.resolveComponentFactory(AccordionPanelItemComponent);
    let component = factory.create(this._ContainerItems.parentInjector);
    this._ContainerItems.insert(component.hostView);

    let item: AccordionPanelItemComponent = component['_component'];
    item.Text = text;
    if (!ObjectUtils.NullOrUndefined(action))
      item.Action = action;

    if (!ObjectUtils.NullUndefinedOrEmpty(materialIcon))
      item.MaterialIcon = materialIcon;

    if (!ObjectUtils.NullUndefinedOrEmpty(fontAwesomeIcon))
      item.FontAwesomeIcon = fontAwesomeIcon;

    this.Items.push(item);

    return item;
  }

  public ForceClose(): void
  {
    if (this.HasItems && this.Opened)
      this.Toggle();
  }
  public Toggle(): void
  {
    if (!ObjectUtils.NullOrUndefined(this.Action))
      this.Action();

    if (!this.Opened)
    {
      if (!ObjectUtils.NullOrUndefined(this.Icon))
        InputDecoration.AddClass(this.Icon, `accordion-icon-opened`);
      if (!ObjectUtils.NullOrUndefined(this.Separator))
        InputDecoration.ElementFadeOut(this.Separator);
      if (!ObjectUtils.NullOrUndefined(this.Panel))
        InputDecoration.RemoveClass(this.Panel, `accordion-container-closed`);
    }
    else
    {
      if (!ObjectUtils.NullOrUndefined(this.Panel))
        InputDecoration.AddClass(this.Panel, `accordion-container-closed`);
      if (!ObjectUtils.NullOrUndefined(this.Separator))
        InputDecoration.ElementFadeIn(this.Separator);
      if (!ObjectUtils.NullOrUndefined(this.Icon))
        InputDecoration.RemoveClass(this.Icon, `accordion-icon-opened`);
    }

    this.Opened = !this.Opened;
    if (this.Opened)
      this.OnStateOpened.next(this);
  }
}