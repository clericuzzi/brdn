import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';

// 3rd party components
import * as XLSX from 'xlsx';
import { UploadFileModel } from './upload-file.model';
import { ObjectUtils } from '../../../utils/object-utils';

@Component({
  selector: 'app-upload-file',
  templateUrl: './upload-file.component.html',
  styleUrls: ['./upload-file.component.css']
})
export class UploadFileComponent
{
  @ViewChild(`input`) _Component: ElementRef;

  private _Files: File[];
  private _ArrayBuffer: any;
  private _CurrentFile: File;

  public OutgoingFiles: UploadFileModel[];  
  public get FileSelected(): boolean
  {
    return !ObjectUtils.NullUndefinedOrEmptyArray(this.OutgoingFiles) && this.OutgoingFiles.length == 1;
  }
  public get FilesSelected(): boolean
  {
    return !ObjectUtils.NullUndefinedOrEmptyArray(this.OutgoingFiles) && this.OutgoingFiles.length > 0;
  }
  public get SingleFile(): UploadFileModel
  {
    if (ObjectUtils.NullUndefinedOrEmptyArray(this.OutgoingFiles))
      return null;
    else
      return this.OutgoingFiles[0];
  }
  
  @Input() Accepts: string = "*";
  @Input() Disabled: boolean;
  @Input() IsMultiple: boolean;
  @Input() IsTextReader: boolean;
  
  @Output() OnFilesRead: EventEmitter<UploadFileModel[]> = new EventEmitter<UploadFileModel[]>();
  @Output() FilesChanged: EventEmitter<string[]> = new EventEmitter<string[]>();
  @Output() FileBeginRead: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  public Clear(): void
  {
    this._Component.nativeElement.value = ``;
    
    this._Files = null;
    this._ArrayBuffer = null;
    this._CurrentFile = null;
    this.OutgoingFiles = null;
  }
  public SelectFile(): void
  {
    this._Component.nativeElement.click();
  }

  private _ReadCsv(fileReader: FileReader): void
  {
    this._ArrayBuffer = fileReader.result;
    var data = new Uint8Array(this._ArrayBuffer);
    var arr = new Array();
    for(var i = 0; i != data.length; ++i)
      arr[i] = String.fromCharCode(data[i]);
      
    let textData: string[] = [];
    var result: string = arr.join("");
    var lines: string[] = result.split('\n');
    let output: any = [];
    for (let line of lines)
      output.push(line.split(","))

    this.OutgoingFiles.push(new UploadFileModel(fileReader, this._CurrentFile, null, output, null));
  }
  private _ReadTxt(fileReader: FileReader): void
  {
    var result: string = fileReader.result as string;
    var lines: string[] = result.split('\n');

    this.OutgoingFiles.push(new UploadFileModel(fileReader, this._CurrentFile, null, lines, null));
  }
  private _ReadXls(fileReader: FileReader): void
  {
    this._ArrayBuffer = fileReader.result;
    var data = new Uint8Array(this._ArrayBuffer);
    var arr = new Array();
    for(var i = 0; i != data.length; ++i)
      arr[i] = String.fromCharCode(data[i]);
      
    var bstr = arr.join("");
    var workbook = XLSX.read(bstr, { type:"binary" });
    var first_sheet_name = workbook.SheetNames[0];
    var worksheet = workbook.Sheets[first_sheet_name];
    
    let textData: string[] = [];
    let selectedData: any = XLSX.utils.sheet_to_json(worksheet, { defval: '' });
    for (const item of selectedData)
      textData.push(item);

    this.OutgoingFiles.push(new UploadFileModel(fileReader, this._CurrentFile, null, textData, null));
  }
  private _ReadData(fileReader: FileReader): void
  {
    this._ArrayBuffer = fileReader.result;
    var uintArray: Uint8Array = new Uint8Array(this._ArrayBuffer);
    var base64String: string = btoa(uintArray.reduce((data, byte) => data + String.fromCharCode(byte), ''));
    this.OutgoingFiles.push(new UploadFileModel(fileReader, this._CurrentFile, base64String, null, uintArray));
  }

  private _Upload(e: ProgressEvent): void
  {
    this.FileBeginRead.emit(this._CurrentFile.name);
    
    if (this.IsTextReader)
    {
      if (this._CurrentFile.name.endsWith("csv"))
        this._ReadCsv(e.target as FileReader);
      if (this._CurrentFile.name.endsWith("txt"))
        this._ReadTxt(e.target as FileReader);
    }
    else
    {
      if (this._CurrentFile.name.endsWith("xls") || this._CurrentFile.name.endsWith("xlsx"))
        this._ReadXls(e.target as FileReader);
      else
        this._ReadData(e.target as FileReader)
    }

    this._ReadFiles();
  }
  public UploadData(event: any) : void
  {
    this._Files = Array.from(event.target.files);
    let fileNames: string[] = [];
    for (let file of this._Files)
      fileNames.push(file.name);
    this.FilesChanged.emit(fileNames);

    this.OutgoingFiles = [];
    this._ReadFiles();
  }

  private _ReadFiles(): void
  {
    if (this._Files == null || this._Files.length == 0)
      this.OnFilesRead.emit(this.OutgoingFiles);
    else
      this._ReadFile();
  }
  private _ReadFile(): void
  {
    this._CurrentFile = this._Files.shift();    
    let fileReader: FileReader = new FileReader();
    fileReader.onload = (e: ProgressEvent) => { this._Upload(e) };
    if (this.IsTextReader)
      fileReader.readAsText(this._CurrentFile);
    else
      fileReader.readAsArrayBuffer(this._CurrentFile);
  }
}