import { Subject } from 'rxjs';
import { FormControlComponent } from '../form-control.component';
import { Component, OnInit, Renderer } from '@angular/core';

// utils
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// services
import { HttpService } from 'clericuzzi-lib/services/http.service';

@Component({
  selector: 'app-selection-box',
  templateUrl: './selection-box.component.html'
})
export class SelectionBoxComponent extends FormControlComponent<string> implements OnInit
{
  public OnSelectionChanged: Subject<boolean> = new Subject<boolean>();

  public IsValid: boolean;
  public SelectionClass: string;
  public Validate(): void
  {
  }

  constructor(
    public _Http: HttpService,
  )
  {
    super(_Http);
  }

  public Click(): void
  {
    let value: boolean = ObjectUtils.NullOrUndefined(this.Control.value) ? true : !this.Control.value;
    this.OnSelectionChanged.next(value);
  }
  public Toggle(isSelected: boolean): void
  {
    this.Control.setValue(isSelected);
  }
  ngOnInit()
  {
  }
  public FocusOut(): void
  {
    this._FocusOut();
  }
}
