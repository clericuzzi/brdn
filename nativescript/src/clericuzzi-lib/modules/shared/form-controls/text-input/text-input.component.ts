import { Component, OnInit, Renderer, Input } from '@angular/core';
import { FormControlComponent } from 'clericuzzi-lib/modules/shared/form-controls/form-control.component';

// utils
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// services
import { HttpService } from 'clericuzzi-lib/services/http.service';

@Component({
  selector: 'app-text-input',
  templateUrl: './text-input.component.html',
})
export class TextInputComponent extends FormControlComponent<string> implements OnInit
{
  constructor(
    public _Http: HttpService,
  )
  {
    super(_Http);
  }
  public get IsValid(): boolean
  {
    this.Validate();
    if (this.Required)
      return !ObjectUtils.NullUndefinedOrEmpty(this.Value);
    else
      return true;
  }

  ngOnInit()
  {
  }
  public Validate(): void
  {
    if (ObjectUtils.NullOrUndefined(this.Model))
      return;
    else
    {
      if (ObjectUtils.NullUndefinedOrEmpty(this.Value))
        this.Value = null;

      if (ObjectUtils.NullOrUndefined(this.Value))
        this.Control.setValue(``);
      this.Model[this.Property] = this.Value;
    }
  }
  public FocusOut(): void
  {
    this._FocusOut();
  }
}
