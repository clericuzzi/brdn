import { takeWhile } from 'rxjs/operators';
import { MatDatepickerInputEvent } from '@angular/material';
import { Component, OnInit, Renderer, ViewChild, OnDestroy, ChangeDetectorRef } from '@angular/core';

// utils
import { DateUtils } from '@src/external/plugin-utils/utils/date-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';
import { FormControlComponent } from 'clericuzzi-lib/modules/shared/form-controls/form-control.component';

// models
import { RequestMessage } from '@src/external/plugin-utils/business/models/request/request-message.model';
import { ComboInputValueModel } from '../custom-inputs/combo-input/combo-input-value.model';
import { StatusResponseMessage } from '@src/external/plugin-utils/business/models/response/status-response-message.model';

// component
import { ComboInputComponent } from '../custom-inputs/combo-input/combo-input.component';

// services
import { HttpService } from 'clericuzzi-lib/services/http.service';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';

@Component({
  selector: 'app-date-input',
  templateUrl: './date-input.component.html'
})
export class DateInputComponent extends FormControlComponent<Date> implements OnInit, OnDestroy
{
  @ViewChild(`hourInput`) InputHour: ComboInputComponent<number>;
  @ViewChild(`minuteInput`) InputMinute: ComboInputComponent<number>;

  public IsAlive: boolean = true;
  public HasHour: boolean = false;
  public HasMinute: boolean = false;
  constructor(
    public _Http: HttpService,
    public _ChangesDetector: ChangeDetectorRef,
  )
  {
    super(_Http);
  }
  public get IsValid(): boolean
  {
    this.Validate();
    if (this.Required)
      return (this.Value != null && this.Value != undefined && DateUtils.IsValid(this.Value));
    else
      return true;
  }

  public Hours: number;
  public Minutes: number;
  ngOnInit()
  {
    this.Sync();

    // this.InputHour.HasShadow = this.InputMinute.HasShadow = false;
    // this.InputHour.MarginBottom = this.InputMinute.MarginBottom = 0;
    // this.InputHour.ComponentHeight = this.InputMinute.ComponentHeight = this.ComponentHeight;

  }
  ngOnDestroy()
  {
    this.IsAlive = false;
  }

  public ShowHour(placeholder: string = null, min: number = 0, max: number = 24, suffix: string = `h`, width: number = 90): void
  {
    if (!ObjectUtils.NullOrUndefined(this.InputHour))
      return;
    this.HasHour = true;
    this._ChangesDetector.detectChanges();

    this.InitHour(placeholder, min, max, suffix, width);
    this.InputHour.OnValueChanged.pipe(takeWhile(() => this.IsAlive)).subscribe(value => this.SetHour(this.InputHour.Value), null, null);
  }
  public ShowMinute(placeholder: string = null, min: number = 0, max: number = 60, suffix: string = `m`, width: number = 90): void
  {
    if (!ObjectUtils.NullOrUndefined(this.InputMinute))
      return;
    this.HasMinute = true;
    this._ChangesDetector.detectChanges();

    this.InitMinute(placeholder, min, max, suffix, width);
    this.InputMinute.OnValueChanged.pipe(takeWhile(() => this.IsAlive)).subscribe(value => this.SetMinute(this.InputMinute.Value), null, null);
  }

  public Sync(): void
  {
    if (!ObjectUtils.NullOrUndefined(this.Model))
    {
      this.Value = this.Model[this.Property];
      if (!ObjectUtils.NullOrUndefined(this.Value))
      {
        this.Hours = this.Value.getHours();
        this.Minutes = this.Value.getMinutes();
        setTimeout(() => this._SyncHourMinute(), 250);
      }
      else
        this.ClearValue();
    }
    else
    {
      this.Value = null;
      this.ClearValue();
    }

    this.CurrentValue = this.Value;
  }
  private _SyncHourMinute(): void
  {
    if (!ObjectUtils.NullOrUndefined(this.Hours) && this.HasHour)
      this.InputHour.SetValue(this.Hours);
    if (!ObjectUtils.NullOrUndefined(this.Minutes) && this.HasMinute)
      this.InputMinute.SetValue(this.Minutes);
  }
  public Validate(): void
  {
    let componentValue: string = this.Input.nativeElement.value;
    if (!ObjectUtils.NullUndefinedOrEmpty(componentValue))
      componentValue = componentValue.trim();

    if (!ObjectUtils.NullUndefinedOrEmpty(componentValue))
    {
      componentValue = StringUtils.DigitsOnly(componentValue);
      this.Value = DateUtils.FromDateBr(componentValue);
      if (ObjectUtils.NullOrUndefined(this.Value) && (this.Value.getFullYear() < 1900 || this.Value.getFullYear() > 2100))
        this.Value = null;

      if (this.Value != null)
      {
        if (this.HasHour && !ObjectUtils.NullOrUndefined(this.Hours))
          this.Value.setHours(this.Hours);
        if (this.HasMinute && !ObjectUtils.NullOrUndefined(this.Minutes))
          this.Value.setMinutes(this.Minutes);

        this.Model[this.Property] = this.Value;
      }
      else
        this.ClearValue();
    }
    else
      this.ClearValue();
  }
  public DateChanged(e: MatDatepickerInputEvent<Date>): void
  {
    this.Value = e.value;
    this.Model[this.Property] = this.Value;

    this.OnValueChanged.next();
  }
  public Send(): void
  {
    this.OnSend.next();
  }
  public Focus(): void
  {
  }
  public FocusOut(): void
  {
    this._FocusOut();
  }

  private _CompareDates(force: boolean = false): boolean
  {
    if (ObjectUtils.NullOrUndefined(this.CurrentValue) != ObjectUtils.NullOrUndefined(this.Value))
      return true;
    else if (!ObjectUtils.NullOrUndefined(this.CurrentValue) && !ObjectUtils.NullOrUndefined(this.Value))
    {
      if (force)
        return true;
      else
        return this.CurrentValue.getFullYear() != this.Value.getFullYear() || this.CurrentValue.getMonth() != this.Value.getMonth() || this.CurrentValue.getDate() != this.Value.getDate();
    }
    else
      return force;
  }
  /**
   * Overrides the UpdateProperty method
   * This is needed to control the updating in the hour and minutes
   * @param force if TRUE the property update will be called on the server
   */
  public UpdateProperty(force: boolean = false): void
  {
    if (this._CompareDates(force))
      if (!ObjectUtils.NullUndefinedOrEmpty(this.UpdatesModelOnValueChangedUrl) && this.UpdatesModelOnValueChanged)
      {
        let request: RequestMessage = new RequestMessage(this.UpdatesModelOnValueChangedUrl);
        request.AddEntity(this.Model[`Id`]);
        request.Add(`property`, this.Property);
        request.Add(this.Property, this.ServerValue());

        let response: StatusResponseMessage = new StatusResponseMessage();
        this.Http.Post(request, response, null);
      }
  }
  public ServerValue(): string
  {
    if (ObjectUtils.NullOrUndefined(this.Value))
      return null;
    else
    {
      let value: Date = new Date(this.Value.getFullYear(), this.Value.getMonth(), this.Value.getDate());
      if (!ObjectUtils.NullOrUndefined(this.Hours))
        value.setHours(this.Hours);
      if (!ObjectUtils.NullOrUndefined(this.Minutes))
        value.setMinutes(this.Minutes);

      return DateUtils.ToDateServer(value);
    }
  }

  public SetHour(value: number): void
  {
    this.Hours = value;
    if (ObjectUtils.NullOrUndefined(value))
    {
      this.Minutes = value;
      this.InputMinute.SetValue(value);
    }

    this.InputHour.SetValue(value);
    this.UpdateProperty(true);
  }
  public SetMinute(value: number): void
  {
    this.Minutes = value;
    this.InputMinute.SetValue(value);
    this.UpdateProperty(true);
  }

  public InitHour(placeholder: string = null, min: number = 0, max: number = 24, suffix: string = `h`, width: number = 90): void
  {
    this.HasHour = true;
    let hours: ComboInputValueModel<number>[] = [];
    for (let i = min; i <= max; i++)
      hours.push(new ComboInputValueModel(i, `${(i < 10 ? `0${i}` : i)}${suffix}`));

    this.InputHour.Model = this;
    this.InputHour.Property = `Hours`;
    this.InputHour.Items = hours;
    this.InputHour.Placeholder = placeholder;
    this.InputHour.ComponentWidthPx = width;
  }
  public InitMinute(placeholder: string = null, min: number = 0, max: number = 60, suffix: string = `m`, width: number = 90): void
  {
    this.HasMinute = true;
    let items: ComboInputValueModel<number>[] = [];
    for (let i = min; i <= max; i++)
      items.push(new ComboInputValueModel(i, `${(i < 10 ? `0${i}` : i)}${suffix}`));

    this.InputMinute.Model = this;
    this.InputMinute.Property = `Minutes`;
    this.InputMinute.Items = items;
    this.InputMinute.Placeholder = placeholder;
    this.InputMinute.ComponentWidthPx = width;
  }
}
