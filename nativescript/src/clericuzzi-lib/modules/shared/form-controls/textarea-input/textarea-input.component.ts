import { Component, OnInit, Renderer } from '@angular/core';

// utils
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// services
import { HttpService } from 'clericuzzi-lib/services/http.service';

// components
import { TextInputComponent } from '../text-input/text-input.component';

@Component({
  selector: 'app-textarea-input',
  templateUrl: './textarea-input.component.html'
})
export class TextareaInputComponent extends TextInputComponent implements OnInit
{
  constructor(
    public _Http: HttpService,
  )
  {
    super(_Http);
  }

  ngOnInit()
  {
    super.ngOnInit();
  }
  public FocusOut(): void
  {
    this._FocusOut();
  }
  public SetDirty(): void
  {
    if (!ObjectUtils.NullOrUndefined(this.Control))
    {
      this.Control.markAsDirty();
      this.Control.markAsTouched();
    }
  }
}
