import { Component, OnInit, Input, Renderer } from '@angular/core';

// utils
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';

// services
import { HttpService } from 'clericuzzi-lib/services/http.service';

// components
import { FormControlComponent } from 'clericuzzi-lib/modules/shared/form-controls/form-control.component';

@Component({
  selector: 'app-number-input',
  templateUrl: './number-input.component.html'
})
export class NumberInputComponent extends FormControlComponent<number> implements OnInit
{
  @Input() Decimal: boolean = false;

  constructor(
    public _Http: HttpService,
  )
  {
    super(_Http);
  }
  public get IsValid(): boolean
  {
    this.Validate();
    if (this.Required)
      return (this.Value != null && this.Value != undefined && this.Value > 0);
    else
      return true;
  }

  ngOnInit()
  {
    this.Sync();
  }
  public Send(): void
  {
    this.OnSend.next();
  }
  public Validate(): void
  {
    if (ObjectUtils.NullOrUndefined(this.Model))
      return;

    var inputText: string = ObjectUtils.NullOrUndefined(this.Value) ? this.Input.nativeElement.value : this.Value.toString();
    if (this.Decimal)
      inputText = StringUtils.DecimalOnly(inputText);
    else
      inputText = StringUtils.DigitsOnly(inputText);

    if (inputText != null && inputText != undefined && inputText.length == 0)
      inputText = this.Required ? `0` : ``;

    this.Input.nativeElement.value = inputText;
    let value: string = inputText;
    if (value.length == 0)
      this.Model[this.Property] = null;
    else
    {
      if (this.Decimal)
        this.Model[this.Property] = Number.parseFloat(value);
      else
        this.Model[this.Property] = Number.parseInt(value);
    }
  }
  public FocusOut(): void
  {
    this._FocusOut();
  }
}
