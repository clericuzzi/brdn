import { Subject } from 'rxjs';
import { SafeStyle } from '@angular/platform-browser';
import { FormControl } from '@angular/forms';
import { Input, Output, ViewChild, ElementRef, HostBinding, Renderer } from '@angular/core';

// utils
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';
import { RequestMessage } from '@src/external/plugin-utils/business/models/request/request-message.model';
import { StatusResponseMessage } from '@src/external/plugin-utils/business/models/response/status-response-message.model';

// models
import { CustomFieldable } from '@src/external/plugin-utils/business/models/crud/custom-fieldable.model';
import { CrudableTableColumnDefinitionModel } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-column-definition.model';

// services
import { HttpService } from 'clericuzzi-lib/services/http.service';
import { InputDecoration } from '@src/external/plugin-utils/utils/input-decoration';
import { StringUtils } from '@src/external/plugin-utils/utils/string-utils';

export abstract class FormControlComponent<T> extends CustomFieldable
{
    constructor(
        public Http: HttpService,
    )
    {
        super();
    }

    public Columndefinition: CrudableTableColumnDefinitionModel;

    public IsDate: boolean = false;
    public IsText: boolean = false;
    public IsCombo: boolean = false;
    public ShowWarning: boolean = false;
    public IsAutoComplete: boolean = false;

    @Input() CustomClass: string = ``;
    @HostBinding(`class`) get Classes(): string
    {
        if (ObjectUtils.NullUndefinedOrEmpty(this.CustomClass))
            return ``;
        else
            return this.CustomClass;
    }

    @HostBinding(`style`) Style: SafeStyle;
    public get SpanStyle(): string
    {
        if (this.GridCol > 0 && this.GridSpan > 0)
            return `grid-column: ${this.GridCol} / span ${this.GridSpan};`;
        else
            return null;
    }

    public Control = new FormControl();
    @ViewChild(`matFormField`) _MatForm: any;
    @ViewChild(`inputField`) Input: ElementRef;

    @Input() Value: T;
    public CurrentValue: T;
    public Validated: boolean;
    public UpdatesModelOnValueChanged: boolean = false;
    public UpdatesModelOnValueChangedUrl: string;
    public SetUpdateUrl(url: string = null): void
    {
        this.UpdatesModelOnValueChanged = !ObjectUtils.NullUndefinedOrEmpty(url);
        this.UpdatesModelOnValueChangedUrl = url;
    }
    public UpdateProperty(): void
    {
        if (!ObjectUtils.NullUndefinedOrEmpty(this.UpdatesModelOnValueChangedUrl) && this.UpdatesModelOnValueChanged)
        {
            let request: RequestMessage = new RequestMessage(this.UpdatesModelOnValueChangedUrl);
            if (!ObjectUtils.NullOrUndefined(this.Model[`Id`]))
                request.AddEntity(this.Model[`Id`]);

            if (!ObjectUtils.NullUndefinedOrEmptyArray(this.CustomFields))
                for (let field of this.CustomFields)
                    request.Add(field.Key, field.Value);

            request.Add(`property`, this.Property);
            request.Add(this.Property, this.Value);
            request.Clear();

            let response: StatusResponseMessage = new StatusResponseMessage();
            this.Http.Post(request, response, null);
        }
    }

    @Input() IsUpperCase: boolean = false;
    @Input() IsLowerCase: boolean = false;

    @Input() FlexGrow: boolean = true;
    @Input() HasShadow: boolean = false;

    @Input() IconWidth: number = 0;
    @Input() IconColor: string = null;
    @Input() MaterialIcon: string = null;

    @Input() GridCol: number;
    @Input() GridSpan: number;
    @Input() MarginTop: number = null;
    @Input() MarginLeft: number = null;
    @Input() MarginRight: number = null;
    @Input() MarginBottom: number = null;

    @Input() PaddingTop: number = null;
    @Input() PaddingLeft: number = null;
    @Input() PaddingRight: number = null;
    @Input() PaddingBottom: number = null;

    @Input() BorderRadius: number = 6;
    @Input() BackgroundColor: string = null;

    @Input() ComponentHeight: number = 40;
    @Input() ComponentWidthPx: number = null;
    @Input() ComponentWidthPercentage: number = 100;

    @Input() Icon: any;
    @Input() Hint: string;
    @Input() Type: string = `text`;
    @Input() Model: any;
    @Input() Warning: string;
    @Input() CanClear: boolean = true;
    @Input() Disabled: boolean;
    @Input() Property: string;
    @Input() Required: boolean = false;
    @Input() ShowsHint: boolean = false;
    @Input() MaxLength: number = 100000;
    @Input() MinLength: number = 0;
    @Input() Placeholder: string;
    @Input() WarningMessage: string;

    @Output() OnSend: Subject<void> = new Subject<void>();
    @Output() OnLeave: Subject<void> = new Subject<void>();
    @Output() OnEnter: Subject<void> = new Subject<void>();
    @Output() OnValueChanged: Subject<void> = new Subject<void>();

    public get HasWarningMessage(): boolean
    {
        return !ObjectUtils.NullUndefinedOrEmpty(this.WarningMessage);
    }

    public ShowHint(): boolean
    {
        if (this.ShowsHint)
            return !ObjectUtils.NullUndefinedOrEmpty(this.Hint) || this.MaxLength < 100000;
        else
            return this.ShowsHint;
    }
    public HintDisplay(): string
    {
        let hint: string = ``;
        if (!ObjectUtils.NullUndefinedOrEmpty(this.Hint))
            hint = this.Hint;

        if (this.MaxLength < 100000 && !ObjectUtils.NullOrUndefined(this.Control) && !ObjectUtils.NullOrUndefined(this.Control.value))
        {
            let lengthHint: string = `${this.Input.nativeElement.value.length || 0}/${this.MaxLength}`;
            if (hint.length > 0)
                hint = `${hint}, ${lengthHint}`;
            else
                hint = lengthHint;
        }

        return hint;
    }

    public AddClasses(...classes: string[]): void
    {
        if (ObjectUtils.NullUndefinedOrEmpty(this.CustomClass))
            this.CustomClass = ``;
        this.CustomClass = StringUtils.AddClasses(this.CustomClass, ...classes);
    }
    public RemoveClasses(...classes: string[]): void
    {
        if (ObjectUtils.NullUndefinedOrEmpty(this.CustomClass))
            this.CustomClass = ``;
        this.CustomClass = StringUtils.RemoveClasses(this.CustomClass, ...classes);
    }

    public SetClass(): string
    {
        let classValue: string = `flex-row`;
        if (this.FlexGrow)
            classValue = `${classValue} flex-grow`;

        return classValue;
    }
    public SetStyle(): any
    {
        let style = {};
        style[`align-items`] = `center`;
        style[`align-content`] = `center`;

        if (!ObjectUtils.NullOrUndefined(this.MarginTop))
            style['margin-top'] = `${this.MarginTop}px`;
        if (!ObjectUtils.NullOrUndefined(this.MarginLeft))
            style['margin-left'] = `${this.MarginLeft}px`;
        if (!ObjectUtils.NullOrUndefined(this.MarginRight))
            style['margin-right'] = `${this.MarginRight}px`;
        if (!ObjectUtils.NullOrUndefined(this.MarginBottom))
            style['margin-bottom'] = `${this.MarginBottom}px`;

        if (!ObjectUtils.NullOrUndefined(this.PaddingTop))
            style['padding-top'] = `${this.PaddingTop}px`;
        if (!ObjectUtils.NullOrUndefined(this.PaddingLeft))
            style['padding-left'] = `${this.PaddingLeft}px`;
        if (!ObjectUtils.NullOrUndefined(this.PaddingRight))
            style['padding-right'] = `${this.PaddingRight}px`;
        if (!ObjectUtils.NullOrUndefined(this.PaddingBottom))
            style['padding-bottom'] = `${this.PaddingBottom}px`;

        if (!ObjectUtils.NullOrUndefined(this.BorderRadius))
            style['border-radius'] = `${this.BorderRadius}px`;
        if (!ObjectUtils.NullUndefinedOrEmpty(this.BackgroundColor))
            style['background-color'] = `${this.BackgroundColor}`;

        return style;
    }
    public SetIconStyle(): any
    {
        let styles = {
            'color': `#${this.IconColor}`,
            'width': `${this.IconWidth}px`,
            'text-align': 'center',
            'align-items': 'center',
            'justify-content': 'center',
        };
        return styles;
    }
    public SetTransformStyle(): any
    {
        let style = {};

        style['width'] = `100%`;
        if (this.IsUpperCase)
            style['text-transform'] = `uppercase`;
        if (this.IsLowerCase)
            style['text-transform'] = `lowercase`;

        return style;
    }

    public SetPristine(): void
    {
        if (!ObjectUtils.NullOrUndefined(this.Control))
        {
            this.Control.markAsPristine();
            this.Control.markAsUntouched();
        }

        if (!ObjectUtils.NullOrUndefined(this._MatForm[`_elementRef`]))
        {
            setTimeout(() => InputDecoration.AddClass(this._MatForm[`_elementRef`], `ng-pristine`, `ng-untouched`), 75);
            setTimeout(() => InputDecoration.RemoveClass(this._MatForm[`_elementRef`], `mat-form-field-invalid`, `ng-dirty`, `ng-touched`), 100);
        }
    }
    public SetDirty(): void
    {
        if (!ObjectUtils.NullOrUndefined(this.Control))
        {
            this.Control.markAsDirty();
            this.Control.markAsTouched();
        }

        if (!ObjectUtils.NullOrUndefined(this._MatForm[`_elementRef`]))
        {
            setTimeout(() => InputDecoration.RemoveClass(this._MatForm[`_elementRef`], `ng-pristine`, `ng-untouched`), 75);
            setTimeout(() => InputDecoration.AddClass(this._MatForm[`_elementRef`], `mat-form-field-invalid`, `ng-dirty`, `ng-touched`), 100);
        }
    }
    public abstract Validate(): void;
    public abstract get IsValid(): boolean;

    public Enable(): void
    {
        this.Disabled = false;
        this.Control.enable();
    }
    public Disable(): void
    {
        this.Disabled = true;
        this.Control.disable();
    }
    public Send(): void
    {
        this.Validate();
        if (this.Required && this.Value != null)
            this.OnSend.next();
    }
    public Sync(): void
    {
        if (!ObjectUtils.NullOrUndefined(this.Model))
        {
            this.Value = this.Model[this.Property];
            this.Validate();
        }
        else
        {
            this.Value = null;
            this.ClearValue();
        }

        this.CurrentValue = this.Value;
    }
    public Update(model: any): void
    {
        this.Model = model;
        this.Sync();
    }
    public ClearValue(): void
    {
        if (!this._CleaningValue)
        {
            this.Clearing();
            if (this.Control != null)
                this.Control.setValue(null);

            if (!ObjectUtils.NullOrUndefined(this.Model))
                this.Model[this.Property] = null;

            this.Value = null;
            this.FocusOut();
        }
    }
    private _CleaningValue: boolean = false;
    public Clearing(): void
    {
        this._CleaningValue = true;
        setTimeout(() => this._CleaningValue = false, 150);
    }

    /**
     * Defines a new value for the component
     * @param value the component's new value
     * @param boolCanUpdate a flag that indicates if, AT THIS MOMENT, we can trigger a property update
     */
    public SetValue(value: T, boolCanUpdate: boolean = true): void
    {
        this.Value = value;
        this.CurrentValue = value;
        this.Model[this.Property] = this.Value;
        if (this.UpdatesModelOnValueChanged && boolCanUpdate)
            this.UpdateProperty();
    }

    public Focus(): void
    {
        this.Input.nativeElement.focus();
    }
    public FocusIn(): void
    {
        this.OnEnter.next();
    }
    public abstract FocusOut(): void;
    public _FocusOut(): void
    {
        if (!ObjectUtils.NullOrUndefined(this.Input) && !this._CleaningValue)
        {
            this.Validate();
            if (ObjectUtils.NullOrUndefined(this.Input.nativeElement))
            {
                if (this.Input[`controlType`] == `mat-select`)
                    this.Value = this.Input[`value`];
                else
                    this.Value = null;
            }
            else
            {
                if (ObjectUtils.NullUndefinedOrEmpty(this.Input.nativeElement.value))
                {
                    if (!ObjectUtils.NullOrUndefined(this.Input))
                        this.Value = this.Input[`value`];
                    else
                        this.Value = null;
                }
            }
        }

        this._CheckOnLeave();
        this.CurrentValue = this.Value;
    }
    private _CheckOnLeave(): void
    {
        if (this.CurrentValue != this.Value)
        {
            if (this.UpdatesModelOnValueChanged)
                this.UpdateProperty();
        }
        this.OnLeave.next();
    }

    public SetModelProperty(model: any, property: string): void
    {
        this.Model = model;
        this.Property = property;
    }
}
