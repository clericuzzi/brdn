import { COMMA, ENTER } from '@angular/cdk/keycodes';

import { MatAutocomplete, MatChipInputEvent, MatAutocompleteSelectedEvent } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { Component, OnInit, ViewChild, ElementRef, ComponentFactoryResolver, OnDestroy } from '@angular/core';

// utils
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';
import { InputDecoration } from '@src/external/plugin-utils/utils/input-decoration';

// models
import { ComboInputValueModel } from '../custom-inputs/combo-input/combo-input-value.model';

// components
import { FormControlComponent } from '../form-control.component';
import { AutocompleteInputItemComponent } from './autocomplete-input-item/autocomplete-input-item.component';

// services
import { HttpService } from 'clericuzzi-lib/services/http.service';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';

@Component({
  selector: 'app-autocomplete-input',
  templateUrl: './autocomplete-input.component.html'
})
export class AutocompleteInputComponent<T> extends FormControlComponent<T> implements OnInit, OnDestroy
{
  public IsAlive: boolean = true;
  public IsValid: boolean;
  public Loading: boolean = false;
  public MultiSelect: boolean = false;
  public SeparatorKeysCodes: number[] = [ENTER, COMMA];
  public DropdownHidden: boolean = true;

  @ViewChild('auto') matAutocomplete: MatAutocomplete;

  public Values: T[] = [];
  public Items: ComboInputValueModel<T>[] = [];
  public SelectedItems: ComboInputValueModel<T>[] = [];
  public AvailableItems: ComboInputValueModel<T>[] = [];
  public SelectedValues: string[] = [];
  public FilteredItems: Observable<ComboInputValueModel<T>[]>;
  constructor(
    public _Http: HttpService,
    public Sanitizer: DomSanitizer,
    public FactoryResolver: ComponentFactoryResolver,
  )
  {
    super(_Http);
  }

  ngOnInit()
  {
  }
  ngOnDestroy()
  {
    this.IsAlive = false;
  }
  public get HasItems(): boolean
  {
    if (ObjectUtils.NullUndefinedOrEmptyArray(this.Items))
      return false;
    else
      return this.Items.length > 0;
  }
  public HasFilter(): boolean
  {
    if (ObjectUtils.NullOrUndefined(this.Control))
      return false;
    else
      return !ObjectUtils.NullUndefinedOrEmpty(this.Control.value);
  }

  public AddChip(event: MatChipInputEvent): void
  {
    // Add fruit only when MatAutocomplete is not open
    // To make sure this does not conflict with OptionSelected Event
    if (!this.matAutocomplete.isOpen)
    {
      const input = event.input;
      const value = event.value;

      // Add our fruit
      if ((value || '').trim())
      {
        this.SelectedValues.push(value.trim());
      }

      // Reset the input value
      if (input)
      {
        input.value = '';
      }

      this.Control.setValue(null);
    }
  }

  public RemoveItem(item: ComboInputValueModel<T>): void
  {
    const index: number = this.SelectedItems.indexOf(item);
    if (index >= 0)
      this.SelectedItems.splice(index, 1);

    this.AvailableItems = this.Items.filter(i => this.SelectedItems.indexOf(i) < 0);
    if (!this.MultiSelect && this.SelectedItems.length === 0)
      this.Control.enable();
  }
  public ItemSelected(e: MatAutocompleteSelectedEvent): void
  {
    if (this.SelectedItems.indexOf(e.option.value) >= 0)
      return;

    this.SelectedItems.push(e.option.value);
    this.Input.nativeElement.value = ``;
    this.Control.setValue(null);
    this.AvailableItems = this.Items.filter(i => this.SelectedItems.indexOf(i) < 0);

    if (!this.MultiSelect)
      this.Control.disable();
  }

  private _Filter(value: any): ComboInputValueModel<T>[]
  {
    if (ObjectUtils.NullUndefinedOrEmpty(value))
      return this.AvailableItems;
    else
    {
      let filterValue = ``;
      if (typeof value === 'string' || value instanceof String)
        filterValue = value.toLowerCase();
      else
        filterValue = value.Label.toLowerCase();

      return this.AvailableItems.filter(i => i.Label.toLowerCase().indexOf(filterValue) >= 0);
    }
  }

  public SetBehaviour(model: any, property: string, multiselect: boolean = false): void
  {
    this.SetModelProperty(model, property);
    this.MultiSelect = multiselect;
  }
  public Select(value: T): void
  {
    if (!ObjectUtils.NullOrUndefined(this.Model) && !ObjectUtils.NullOrUndefined(this.Property))
    {
      this.Model[this.Property] = value;
      this.Sync();
      this.OnValueChanged.next();
    }
  }
  public Sync(): void
  {
  }

  public SetItems(items: ComboInputValueModel<T>[]): void
  {
    this.Enable();
    this.Loading = false;

    this.Items = items;
    this.SelectedItems = [];
    this.SelectedValues = [];
    this.AvailableItems = items;

    this.FilteredItems = this.Control.valueChanges
      .pipe(startWith([]), map((item: string | null) => item ? this._Filter(item) : this.AvailableItems.slice()));
  }
  public SetItemsFromArray(source: any[], propertyId: string, propertyLabel: string): void
  {
    if (ObjectUtils.NullUndefinedOrEmptyArray(source))
      this.ClearItems();
    else
    {
      const items: ComboInputValueModel<T>[] = [];
      for (const item of source)
        items.push(new ComboInputValueModel<T>(item[propertyId], item[propertyLabel]));

      this.SetItems(items);
    }
  }


  public ClearItems(): void
  {
    this.ClearValue();
    this.Items = [];
    this.SelectedItems = [];
    this.SelectedValues = [];
    this.AvailableItems = [];
  }

  public Validate(): void
  {
  }
  public SendEvent(): void
  {
    this.Validate();
    this.OnSend.next();
  }
  public SetDirty(): void
  {
    if (!ObjectUtils.NullOrUndefined(this.Control))
    {
      this.Control.markAsDirty();
      this.Control.markAsTouched();
    }
  }

  public FocusOut(): void
  {
    this._FocusOut();
  }
}
