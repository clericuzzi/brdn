import { Subject } from 'rxjs';
import { Component, OnInit, ViewChild, ViewContainerRef, ComponentFactoryResolver } from '@angular/core';

// utils
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// models
import { Crudable } from '@src/external/plugin-utils/business/models/crud/crudable.model';
import { CrudableTableDefinitionModel, CrudableTableDefinitionActionModel } from '@src/external/plugin-utils/business/models/crud/definitions/crudable-table-definition.model';
import { CrudSelectActionsItemComponent } from './crud-select-actions-item/crud-select-actions-item.component';

// components
import { SubjectListenerComponent } from 'clericuzzi-lib/modules/shared/subject-listener/subject-listener.component';
import { ComponentGeneratorService } from 'clericuzzi-lib/services/component-generator.service';

@Component({
  selector: 'app-crud-select-actions',
  templateUrl: './crud-select-actions.component.html'
})
export class CrudSelectActionsComponent extends SubjectListenerComponent implements OnInit
{
  public OnDelete: Subject<void> = new Subject<void>();
  public OnUpdate: Subject<void> = new Subject<void>();
  public OnRefresh: Subject<void> = new Subject<void>();
  public OnActionInvoked: Subject<Function> = new Subject<Function>();

  @ViewChild('itemsPanel', { read: ViewContainerRef }) _ContainerItems: ViewContainerRef;
  private _TableDefinition: CrudableTableDefinitionModel;
  private _Components: CrudSelectActionsItemComponent[] = [];
  private _RefreshComponent: CrudSelectActionsItemComponent;
  constructor(
    private _ComponentGenerator: ComponentGeneratorService,
  )
  {
    super();
  }

  ngOnInit()
  {
  }

  private _DrawActions(): void
  {
    if (!ObjectUtils.NullOrUndefined(this._TableDefinition))
    {
      if (this._TableDefinition.CanDelete)
        this._DrawDelete();

      // if (this._TableDefinition.CanUpdate)
      //   this._DrawUpdate();

      if (this._TableDefinition.CanRefresh)
        this._DrawRefresh();
    }
    this.SelectionUpdated(0);
  }

  private _ActionDelete(): void
  {
    this.OnDelete.next();
  }
  private _ActionUpdate(): void
  {
    this.OnUpdate.next();
  }
  private _ActionRefresh(): void
  {
    this.OnRefresh.next();
    if (!ObjectUtils.NullOrUndefined(this._RefreshComponent))
      this._RefreshComponent.Disable();
  }

  public CanRefresh(): void
  {
    if (!ObjectUtils.NullOrUndefined(this._RefreshComponent))
      this._RefreshComponent.Enable();
  }
  public CannotRefrensh(): void
  {
    if (!ObjectUtils.NullOrUndefined(this._RefreshComponent))
      this._RefreshComponent.Disable();
  }

  /**
   * Redraws the panel's items
   */
  public Refresh(): void
  {
    this.Clear();
    this._DrawActions();
  }
  /**
   * Clears the actions panel
   */
  public Clear(): void
  {
    if (!ObjectUtils.NullOrUndefined(this._TableDefinition))
    {
      this._TableDefinition.CanDelete = false;
      this._TableDefinition.CanFilter = false;
      this._TableDefinition.CanInsert = false;
      this._TableDefinition.CanSearch = false;
      this._TableDefinition.CanUpdate = false;
    }

    this._Components = [];
    this._ContainerItems.clear();
  }
  public NewAction(actionModel: CrudableTableDefinitionActionModel, index: number = null): void
  {
    if (this._Components.filter(i => i.Key == actionModel.Key).length == 0)
    {
      let element: CrudSelectActionsItemComponent = this._ComponentGenerator.CreateComponent(CrudSelectActionsItemComponent, this._ContainerItems, index);
      element.Action = actionModel;
      if (actionModel.SelectedItemsNeeded == 0)
        element.Show();

      this._Components.push(element);
      this.ListenTo(element.OnItemActivated, actionModel => this._ItemClicked(actionModel));
    }
  }
  public RemoveAction(key: string): void
  {
    let elementToRemove: CrudSelectActionsItemComponent = ObjectUtils.First(this._Components, (function (i: CrudSelectActionsItemComponent): boolean { return i.Key == key; }));
    let index: number = this._Components.indexOf(elementToRemove);
    this._Components.splice(index, 1);
    this._ContainerItems.remove(index);
  }

  private _DrawDelete(): void
  {
    let element: CrudSelectActionsItemComponent = this._ComponentGenerator.CreateComponent(CrudSelectActionsItemComponent, this._ContainerItems);
    element.Action = new CrudableTableDefinitionActionModel(CrudActionPanelDefaults.Delete, `Remover ${this._TableDefinition.ReadableName}(s)`, this._ActionDelete.bind(this), true, `material-icons color-delete`, `delete`, 1);
    this._Components.push(element);

    this.ListenTo(element.OnItemActivated, actionModel => this._ItemClicked(actionModel));
  }
  private _DrawUpdate(): void
  {
    let element: CrudSelectActionsItemComponent = this._ComponentGenerator.CreateComponent(CrudSelectActionsItemComponent, this._ContainerItems);
    element.Action = new CrudableTableDefinitionActionModel(CrudActionPanelDefaults.Update, `Alterar ${this._TableDefinition.ReadableName}(s)`, this._ActionUpdate.bind(this), false, `material-icons`, `edit`, 1);
    this._Components.push(element);

    this.ListenTo(element.OnItemActivated, actionModel => this._ItemClicked(actionModel));
  }
  private _DrawRefresh(): void
  {
    let element: CrudSelectActionsItemComponent = this._ComponentGenerator.CreateComponent(CrudSelectActionsItemComponent, this._ContainerItems);
    element.Action = new CrudableTableDefinitionActionModel(CrudActionPanelDefaults.Refresh, `Atualizar`, this._ActionRefresh.bind(this), true, `material-icons`, `refresh`, 0);
    this._Components.push(element);

    this._RefreshComponent = element;
    this.ListenTo(element.OnItemActivated, actionModel => this._ItemClicked(actionModel));
  }

  private _ItemClicked(actionModel: CrudableTableDefinitionActionModel): void
  {
    if (!ObjectUtils.NullOrUndefined(actionModel.Action))
      this.OnActionInvoked.next(actionModel.Action);
  }

  public Init(baseObject: Crudable): void
  {
    this.Clear();
    this._TableDefinition = baseObject.TableDefinition;
    this._DrawActions();
  }
  public SelectionUpdated(itemsSelected: number): void
  {
    for (let component of this._Components)
      component.CheckDisplay(itemsSelected);
  }
  public DrawDeleteAction(): void { this._TableDefinition.CanDelete = true; this._DrawActions(); }
  public DrawInsertAction(): void { this._TableDefinition.CanInsert = true; this._DrawActions(); }
  public DrawSearchAction(): void { this._TableDefinition.CanSearch = true; this._DrawActions(); }
  public DrawUpdateAction(): void { this._TableDefinition.CanUpdate = true; this._DrawActions(); }
  public DrawRefreshAction(): void { this._TableDefinition.CanRefresh = true; this._DrawActions(); }
}

export enum CrudActionPanelDefaults
{
  Insert = "default-insert",
  Delete = "default-delete",
  Update = "default-update",
  Search = "default-search",
  Refresh = "default-refresh",
}