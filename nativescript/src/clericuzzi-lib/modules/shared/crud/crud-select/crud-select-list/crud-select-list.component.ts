import { Subject } from 'rxjs';
import { Component, OnInit, ViewChild, ViewContainerRef, ComponentFactoryResolver, ComponentFactory } from '@angular/core';

// utils
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// models
import { Crudable } from '@src/external/plugin-utils/business/models/crud/crudable.model';
import { ComponentType } from '@angular/cdk/portal';

// behavior
import { SelectListItem } from 'clericuzzi-lib/business/behaviours/select-list-item.behaviour';

// components
import { SubjectListenerComponent } from '../../../subject-listener/subject-listener.component';

// interfaces
import { SelectListItemModel } from 'clericuzzi-lib/business/models/select-list-item.model';

@Component({
  selector: 'app-crud-select-list',
  templateUrl: './crud-select-list.component.html'
})
export class CrudSelectListComponent<T extends Crudable> extends SubjectListenerComponent implements OnInit
{
  public OnSelectionChanged: Subject<Crudable[]> = new Subject<Crudable[]>();
  @ViewChild('itemsPanel', { read: ViewContainerRef }) _ContainerItems: ViewContainerRef;

  public Items: T[];
  private _MultiSelect: boolean = false;
  private _SelectedItems: number[];
  private _Elements: SelectListItem<T>[];
  private _Template: ComponentType<SelectListItem<T>>;
  constructor(
    private _FactoryResolver: ComponentFactoryResolver,
  )
  {
    super();
  }

  ngOnInit()
  {
    this._SelectedItems = [];
  }

  public Clear(): void
  {
    this.Items = [];
    this._Elements = [];
    this._ContainerItems.clear();
  }
  public ClearSelection(): void
  {
    this._ClearSelection();
    for (let element of this._Elements)
      element.Selected = false;
  }

  public SetMultiselect(multiSelect: boolean): void
  {
    this._MultiSelect = multiSelect;
  }
  public RemoveItems(ids: number[]): void
  {
    let newItems: T[] = this.Items.filter(i => ObjectUtils.NullOrUndefined(ids.find(j => j == i[i.PkProperty])));
    this.Clear();
    this.SetItems(newItems);
  }
  public SetItems(items: T[], template: ComponentType<SelectListItem<T>> = null): void
  {
    this.Items = items;
    this._Elements = [];
    if (!ObjectUtils.NullOrUndefined(template))
      this._Template = template;
    this._DrawItems();
  }
  private _DrawItems()
  {
    this._ContainerItems.clear();

    if (ObjectUtils.NullOrUndefined(this._Template))
      throw new Error("O template dos items não foi iniciado");
    if (ObjectUtils.NullUndefinedOrEmptyArray(this.Items))
      return;

    let factory = this._FactoryResolver.resolveComponentFactory(this._Template);
    let index: number = 0;
    for (let item of this.Items)
      this._DrawItem(item, index++, factory);

    if (!ObjectUtils.NullUndefinedOrEmptyArray(this._Elements))
    {
      this._Elements[0].FirstChild = true;
      this._Elements[this._Elements.length - 1].LastChild = true;
    }

    this.ClearSelection();
  }
  private _DrawItem(item: T, index: number, factory: ComponentFactory<SelectListItem<T>>): void
  {
    let component = factory.create(this._ContainerItems.parentInjector);
    this._ContainerItems.insert(component.hostView);

    let element: SelectListItem<T>;
    element = component['_component'];
    element.Index = index;
    element.Model = item;

    this._Elements.push(element);
    this.ListenTo(element.OnSelectionUpdated, selectionModel => this._UpdateSelection(selectionModel));
  }
  private _UpdateSelection(selectionModel: SelectListItemModel<T>)
  {
    if (this._MultiSelect)
    {
      if (selectionModel.Selected)
        this._Select(selectionModel.Id);
      else
        this._Unselect(selectionModel.Id);
    }
    else
    {
      this.ClearSelection();
      this._SelectedItems = [];
      selectionModel.Selected = true;
      this._Select(selectionModel.Id);
    }

    this._DispatchSelection();
  }
  private _DispatchSelection(): void
  {
    let selectedItems: Crudable[] = [];
    if (!ObjectUtils.NullUndefinedOrEmptyArray(this._SelectedItems))
    {
      for (let selectedItemId of this._SelectedItems)
      {
        let selectedItem: T = this.Items.find(i => this._CheckSelectedItem(i, selectedItemId));
        if (!ObjectUtils.NullOrUndefined(selectedItem))
          selectedItems.push(selectedItem);
      }
    }

    this.OnSelectionChanged.next(selectedItems);
  }
  private _CheckSelectedItem(item: T, selectedItemId: number): boolean
  {
    let key: number = item.GetKey();
    let returnValue: boolean = key == selectedItemId;

    return returnValue;
  }
  private _ClearSelection(): void
  {
    let elementIds: number[] = this._Elements.map(i => i.SelectionModel.Id);
    let unselectedIds: number[] = elementIds.filter(i => ObjectUtils.NullOrUndefined(this._SelectedItems.find(j => j == i)));

    for (let item of this._Elements)
    {
      let selected: boolean = ObjectUtils.NullOrUndefined(unselectedIds.find(i => i == item.SelectionModel.Id));
      item.SetSelection(selected);
    }
    this._SelectedItems = [];
  }
  private _Select(id: number): void
  {
    let element: SelectListItem<T> = this._Elements.find(i => i.Model[i.KeyProperty] == id);
    if (!ObjectUtils.NullOrUndefined(element))
      element.Selected = true;

    if (!this._SelectedItems.find(i => i == id))
      this._SelectedItems.push(id);
  }
  private _Unselect(id: number): void
  {
    if (this._SelectedItems.find(i => i == id))
      this._SelectedItems.splice(this._SelectedItems.indexOf(id), 1);
  }
}