import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';

// utils
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';
import { InputDecoration } from '@src/external/plugin-utils/utils/input-decoration';

// components
import { HostBindedComponent } from '../../util-components/host-binded/host-binded.component';

@Component({
  selector: 'app-crud-panel',
  templateUrl: './crud-panel.component.html'
})
export class CrudPanelComponent extends HostBindedComponent implements OnInit
{
  public Initialized: boolean = false;
  @ViewChild('_container', { read: ViewContainerRef }) public ItemsContainer: ViewContainerRef;
  public get HasTitle(): boolean
  {
    return !ObjectUtils.NullUndefinedOrEmpty(this.Title);
  }

  public Title: string;
  constructor()
  {
    super(null);
    InputDecoration.BindingAddClass(this, this.ClassBinding, `crud-panel-container`);
  }
  ngOnInit()
  {
  }
}