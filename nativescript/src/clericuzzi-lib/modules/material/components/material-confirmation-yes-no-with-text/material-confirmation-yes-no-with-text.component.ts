import { MAT_DIALOG_DATA } from '@angular/material';
import { Component, Inject, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MaterialPopupService } from '@src/clericuzzi-lib/services/material-popup.service';

// components

@Component({
  selector: 'app-material-confirmation-yes-no-with-text',
  templateUrl: './material-confirmation-yes-no-with-text.component.html'
})
export class MaterialConfirmationYesNoWithTextComponent implements OnInit
{
  @ViewChild(`input`, { static: false }) _TextInput: ElementRef;
  @ViewChild(`confirmButton`, { static: false }) _ConfirmButton: ElementRef;

  public Data: any;
  public Title: string;
  public Message: string;
  public LabelNo: string;
  public LabelYes: string;
  public DenyClass: string;
  public Placeholder: string;
  public ConfirmClass: string;
  public PasswordType: boolean;

  constructor(
    @Inject(MAT_DIALOG_DATA) private data: any,
    private _PopupServices: MaterialPopupService,
  )
  {
    this.Title = data.Title;
    this.Message = data.Message;
    this.LabelNo = data.LabelNo;
    this.LabelYes = data.LabelYes;
    this.DenyClass = data.DenyClass;
    this.Placeholder = data.Placeholder;
    this.ConfirmClass = data.ConfirmClass;
    this.PasswordType = data.PasswordType;
  }

  ngOnInit()
  {
  }

  public get ComponentType(): string
  {
    return this.PasswordType ? `password` : `text`;
  }

  public Confirm(): void
  {
    this._ConfirmButton.nativeElement.click();
  }
  public SendConfirmationText(): void
  {
    this._PopupServices.ConfirmationText = this._TextInput.nativeElement.value;
  }
}