import { Component, Inject } from '@angular/core';
import { ComponentType } from '@angular/cdk/portal';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
// utils
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';
// Components
import { MaterialConfirmationYesNoWithTextComponent } from '../material-confirmation-yes-no-with-text/material-confirmation-yes-no-with-text.component';
import { MaterialConfirmationYesNoComponent } from '../material-confirmation-yes-no/material-confirmation-yes-no.component';

@Component({
  selector: 'app-material-popup',
  templateUrl: './material-popup.component.html',
  styleUrls: ['./material-popup.component.css']
})
export class MaterialPopupComponent
{
  public static LabelNo: string;
  public static LabelYes: string;
  public static DenyClass: string;
  public static ConfirmClass: string;
  constructor(@Inject(MAT_DIALOG_DATA) private data: any)
  {
  }

  public static Popup<T, R>(diag: MatDialog, component: ComponentType<T>, dataValue: any, afterInit: Function = null, afterClose: Function = null, widthValue: string = '85vw', heightValue: string = '85vh'): MatDialogRef<T, R>
  {
    const diagData: any = { width: widthValue, height: heightValue, data: dataValue };
    const diagRef = diag.open(component, diagData);
    if (afterInit != null)
      diagRef.afterOpened().subscribe(result => MaterialPopupComponent._AfterPopupInit(diagRef, afterInit));
    if (!ObjectUtils.NullOrUndefined(afterClose))
      diagRef.afterClosed().subscribe(result => afterClose());

    return diagRef;
  }
  private static _AfterPopupInit<T, R>(dialog: MatDialogRef<T, any>, action: Function): void
  {
    if (dialog.componentInstance['_Component'] !== undefined)
      action(dialog.componentInstance['_Component']);
    else
      action(dialog.componentInstance);
  }

  public static Confirmation(diag: MatDialog, callback: Function, data: any, title: string, message: string, labelNo: string = null, labelYes: string = null, confirmClass: string = null, denyClass: string = null): void
  {
    if (ObjectUtils.NullUndefinedOrEmpty(labelNo))
      labelNo = MaterialPopupComponent.LabelNo;

    if (ObjectUtils.NullUndefinedOrEmpty(labelYes))
      labelYes = MaterialPopupComponent.LabelYes;

    if (ObjectUtils.NullUndefinedOrEmpty(denyClass))
      denyClass = MaterialPopupComponent.DenyClass;

    if (ObjectUtils.NullUndefinedOrEmpty(confirmClass))
      confirmClass = MaterialPopupComponent.ConfirmClass;

    const diagData: any = { data: { Title: title, Message: message, LabelNo: labelNo, LabelYes: labelYes, ConfirmClass: confirmClass, DenyClass: denyClass } };
    const diagRef = diag.open(MaterialConfirmationYesNoComponent, diagData);

    diagRef.afterClosed().subscribe(result => { if (result) callback(data); });
  }
  public static ConfirmationWithText(diag: MatDialog, callback: Function, data: any, title: string, message: string, placeholder: string = null, password: boolean = false, labelNo: string = null, labelYes: string = null, confirmClass: string = null, denyClass: string = null): void
  {
    if (ObjectUtils.NullUndefinedOrEmpty(labelNo))
      labelNo = MaterialPopupComponent.LabelNo;

    if (ObjectUtils.NullUndefinedOrEmpty(labelYes))
      labelYes = MaterialPopupComponent.LabelYes;

    if (ObjectUtils.NullUndefinedOrEmpty(denyClass))
      denyClass = MaterialPopupComponent.DenyClass;

    if (ObjectUtils.NullUndefinedOrEmpty(confirmClass))
      confirmClass = MaterialPopupComponent.ConfirmClass;

    const diagData: any = { data: { Title: title, Message: message, LabelNo: labelNo, LabelYes: labelYes, ConfirmClass: confirmClass, DenyClass: denyClass, Placeholder: placeholder, PasswordType: password } };
    const diagRef = diag.open(MaterialConfirmationYesNoWithTextComponent, diagData);

    diagRef.afterClosed().subscribe(result => { if (result) callback(result); });
  }
  public static ConfirmationWithComponent<T>(diag: MatDialog, component: ComponentType<T>, callback: Function, data: any, width: string = null, height: string = null): void
  {
    const diagData: any = { data: data, width: width, height: height };
    const diagRef = diag.open(component, diagData);

    diagRef.afterClosed().subscribe(result => { if (result) callback(); });
  }
}