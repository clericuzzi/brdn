import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-material-confirmation-yes-no',
  templateUrl: './material-confirmation-yes-no.component.html'
})
export class MaterialConfirmationYesNoComponent
{
  public Data: any;
  public Title: string;
  public Message: string;
  public LabelNo: string;
  public LabelYes: string;
  public DenyClass: string;
  public ConfirmClass: string;

  constructor(@Inject(MAT_DIALOG_DATA) private data: any)
  {
    this.Title = data.Title;
    this.Message = data.Message;
    this.LabelNo = data.LabelNo;
    this.LabelYes = data.LabelYes;
    this.DenyClass = data.DenyClass;
    this.ConfirmClass = data.ConfirmClass;
  }
}