import { DomSanitizer } from '@angular/platform-browser';
import { Injectable, ComponentFactoryResolver, ViewContainerRef, ComponentRef } from '@angular/core';

@Injectable()
export class ComponentGeneratorService
{
  constructor(
    public Factory: ComponentFactoryResolver,
    public Sanitizer: DomSanitizer,
  )
  {
  }

  public CmpRef: ComponentRef<any>;
  public Initialize(factory: ComponentFactoryResolver, sanitizer: DomSanitizer): void
  {
    this.Factory = factory;
    this.Sanitizer = sanitizer;
  }
  public CreateComponent<T>(type: any, container: ViewContainerRef, index: number = null): T
  {
    const factory = this.Factory.resolveComponentFactory(type);
    const component: ComponentRef<any> = factory.create(container.injector) as ComponentRef<any>;
    this.CmpRef = component;

    container.insert(component.hostView, index);

    return  component['_component'] as T;
  }
}
