﻿import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';

// utils

// models
import { Account } from '@src/external/plugin-utils/business/models/models/account.model';
import { StatusResponseMessage } from '@src/external/plugin-utils/business/models/response/status-response-message.model';

// services
import { HttpService } from './http.service';
import { UserSessionService } from './user-session.service';
import { User } from '@src/external/plugin-utils/business/models/models/user.model';
import { BatchResponseMessageList } from '@src/external/plugin-utils/business/models/response/batch-response-message.model';
import { RequestMessage, RequestMessageDefaults } from '@src/external/plugin-utils/business/models/request/request-message.model';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

@Injectable()
export class LoginService
{
    private _Validated: boolean = false;
    private _KeepValidating: boolean = false;

    public UserLoggedIn: Subject<Account[]> = new Subject<Account[]>();
    public UserValidated: Subject<Account[]> = new Subject<Account[]>();
    public UserLoggedOff: Subject<StatusResponseMessage> = new Subject<StatusResponseMessage>();
    public UserLoggedInError: Subject<string> = new Subject<string>();

    constructor(private _Http: HttpService
        , public _Session: UserSessionService)
    {
    }

    public LoggedIn: boolean = false;
    public LoggedOff: boolean = false;

    public BaseUrl: string;
    public LogInUrl: string;
    public ValidateUrl: string;
    public DefineBaseUrl(baseUrl: string): void
    {
        this.BaseUrl = baseUrl;
        this.LogInUrl = `${baseUrl}web/login/in`;
        this.ValidateUrl = `${baseUrl}web/login/validate`;
    }

    public Login(user: User)
    {
        if (this.LogInUrl === undefined || this.LogInUrl === '')
            throw new Error('LogIn url not initialized');

        else
        {
            const logInResponse: BatchResponseMessageList = new BatchResponseMessageList();

            const request = new RequestMessage(this.LogInUrl);
            request.Add(`email-info`, user.Email);
            request.Add(`password-info`, user.Password);
            this._Http.Post(request, logInResponse, () => this._LoginCallback(logInResponse));
        }
    }
    _LoginCallback(response: BatchResponseMessageList): void
    {
        let user = new User();
        let accounts: Account[];
        try
        {
            if (ObjectUtils.NullOrUndefined(response.Entities) && ObjectUtils.NullUndefinedOrEmpty(response.ErrorMessage))
            {
                this.UserLoggedInError.next(`Não foi possível se conectar ao servidor. Entre em contato com o suporte.`);
                return;
            }

            user = response.GetValue(`user`, new User());
            accounts = response.GetList(`account`, new Account());
            if (!response.HasErrors && !ObjectUtils.NullOrUndefined(user) && !ObjectUtils.NullUndefinedOrEmptyArray(accounts))
            {
                this.LoggedIn = true;
                this.LoggedOff = false;

                RequestMessageDefaults.UserId = user.Id;
                this.SaveLoginInfo(user);
                this.UserLoggedIn.next(accounts);
            }
            else
            {
                this.LoggedOff = true;
                let errorMessage: string = ``;

                if (response.HasErrors)
                    errorMessage = response.ErrorMessage;
                else if (ObjectUtils.NullOrUndefined(user))
                    errorMessage = `Usuário ou senha inválidos`;
                else if (ObjectUtils.NullUndefinedOrEmptyArray(accounts))
                    errorMessage = `Nenhuma conta ativa para o usuário, entre em contato com o administrador do sistema`;

                this.UserLoggedInError.next(errorMessage);
            }
        }
        catch (e)
        {
            this.UserLoggedInError.next(e);
        }
    }

    public InitValidate(): void
    {
        this._KeepValidating = true;
        if (!this._Validated)
            this.Validate();

        this._Validated = true;
        this.ValidateDelay();
    }
    public ValidateDelay(): void
    {
        setTimeout(() => this.Validate(), 300000);
    }
    public Validate(): void
    {
        if (!this.LoggedOff)
        {
            const request: RequestMessage = new RequestMessage(this.ValidateUrl);
            request.AddId(this._Session.Id);

            const response: BatchResponseMessageList = new BatchResponseMessageList();
            this._Http.Post(request, response, () => this._ValidateCallback(response));
        }
    }
    _ValidateCallback(response: BatchResponseMessageList): void
    {
        let user = new User();
        let accounts: Account[];
        try
        {
            user = response.GetValue(`user`, new User());
            accounts = response.GetList(`account`, new Account());
            if (!response.HasErrors && !ObjectUtils.NullOrUndefined(user) && !ObjectUtils.NullUndefinedOrEmptyArray(accounts))
            {
                this.LoggedIn = true;

                RequestMessageDefaults.UserId = user.Id;
                this.SaveLoginInfo(user);
                this.UserValidated.next(accounts);
            }
            else
            {
                let errorMessage: string = ``;

                if (response.HasErrors)
                    errorMessage = response.ErrorMessage;
                else if (ObjectUtils.NullOrUndefined(user))
                    errorMessage = `Usuário ou senha inválidos`;
                else if (ObjectUtils.NullUndefinedOrEmptyArray(accounts))
                    errorMessage = `Nenhuma conta ativa para o usuário, entre em contato com o administrador do sistema`;

                this.UserLoggedInError.next(errorMessage);
            }
        }
        catch (e)
        {
            this.UserLoggedInError.next(e);
        }

        if (this._KeepValidating)
            this.ValidateDelay();
    }

    public Logoff(emitEvent: boolean = true): void
    {
        this.LoggedOff = true;
        this._Validated = false;
        this._KeepValidating = false;
        this._Session.Delete(this._Session.SETTINGS_ID);
        this._Session.Delete(this._Session.SETTINGS_USER);
        this._Session.Delete(this._Session.SETTINGS_TOKEN);
        this._Session.Delete(this._Session.SETTINGS_LOGGED);
        this._Session.Delete(this._Session.SETTINGS_NAME_FULL);
        this._Session.Delete(this._Session.SETTINGS_NAME_LAST);
        this._Session.Delete(this._Session.SETTINGS_NAME_FIRST);
        this._Session.Delete(this._Session.SETTINGS_PERMISSION);
        this._Session.Delete(this._Session.SETTINGS_PERMISSION_LIST);

        this._Session.Save();

        if (emitEvent)
            this.UserLoggedOff.next();
    }
    public SaveUserInfo(user: User): void
    {
        try
        {
            this._Session.Id = user.Id;
            this._Session.User = user;
            this._Session.Logged = true;
            this._Session.NameFull = user.Name;
            this._Session.NameLast = user.Name;
            this._Session.NameFirst = user.Name;
            // this._Session.PermissionName = user.PermissionName;
            // this._Session.PermissionList = user.PermissionList;
        }
        catch (e)
        {
        }
    }
    public SaveLoginInfo(user: User): void
    {
        this.SaveUserInfo(user);

        this._Session.Save();
    }
    public LoadSession(): void
    {
        this._Session.Load();
    }
}