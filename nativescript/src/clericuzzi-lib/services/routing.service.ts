import { MatSnackBar } from '@angular/material';
import { Location } from '@angular/common';
import { Injectable } from '@angular/core';
import { Router, ActivatedRoute, UrlSegment } from '@angular/router';

// utils

// Services
import { UserDataService } from './user-data.service';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

@Injectable()
export class RoutingService
{
    constructor(
        private _Snack: MatSnackBar,
        private _Router: Router,
        private _Location: Location,
        private _UserData: UserDataService,
    )
    {
    }

    public Back(): void { this._Location.back(); }
    public ToRoute(route: string, preffix: string = null, delay: number = 0, param: any = null): void
    {
        if (ObjectUtils.NullUndefinedOrEmpty(route))
            route = preffix;
        else if (ObjectUtils.NullUndefinedOrEmpty(preffix))
            route = route;
        else
            route = `${preffix}/${route}`;

        if (delay === 0)
        {
            if (ObjectUtils.NullOrUndefined(param))
                this._Router.navigate([route]);
            else
                this._Router.navigate([route], { queryParams: { data: param } });
        }
        else
            setTimeout(() => { this.ToRoute(route, preffix, 0, param); }, delay);
    }
    public ToRouteNewTab(route: string, label: string = null, delay: number = 0, param: any = null): void
    {
        if (delay === 0)
        {
            if (label === null || label === undefined)
                label = route;
            if (ObjectUtils.NullOrUndefined(param))
                this._Router.navigate([route]).then(result => { window.open(route, `_blank`); });
            else
                this._Router.navigate([route], { queryParams: { data: param } }).then(result => { window.open(route, `_blank`); });
        }
        else
            setTimeout(() => { this.ToRouteNewTab(route, label, 0, param); }, delay);
    }

    public GetParams(route: ActivatedRoute): string
    {
        const params: any = null;
        route.queryParams
            .subscribe(params =>
            {
                params = params._value;
            });

        return params;
    }
    /**
     * Extracts a segment from the given route
     * @param route the route to have it's segment extracted
     * @param index the desired's segment index, starting from the tail to the head
     */
    public GetSegmentAt(route: ActivatedRoute, index: number = 0): string
    {
        if (ObjectUtils.NullOrUndefined(route))
            return null;
        else
        {
            const segments: UrlSegment[] = route.url[`_value`];
            if (ObjectUtils.NullUndefinedOrEmptyArray(segments))
                return null;
            else
                return segments[segments.length - (1 + index)].path;
        }
    }
    /**
     * Returns the last segment from a given route
     * @param route the route to have it's segment extracted
     */
    public GetLastSegment(route: ActivatedRoute, segmentAmount: number = 1): string
    {
        if (ObjectUtils.NullOrUndefined(route))
            return null;
        else
        {
            const segments: UrlSegment[] = route.url[`_value`];
            if (ObjectUtils.NullUndefinedOrEmptyArray(segments))
                return null;
            else
            {
                let returnValue: string = ``;
                if (segments.length < segmentAmount)
                    segmentAmount = segments.length;
                while (segmentAmount > 0)
                {
                    if (ObjectUtils.NullUndefinedOrEmpty(returnValue))
                        returnValue = segments[segments.length - segmentAmount--].path;
                    else
                    {
                        if (returnValue.length === 0)
                            returnValue = `${segments[segments.length - segmentAmount--].path}`;
                        else
                            returnValue = `${returnValue}/${segments[segments.length - segmentAmount--].path}`;
                    }
                }
                return returnValue;
            }
        }
    }
    /**
     * Returns the last segment from a given route
     * @param path the route to have it's segment extracted
     */
    public GetLastPath(path: string): string
    {
        if (ObjectUtils.NullUndefinedOrEmpty(path))
            return null;
        else
        {
            const segments: string[] = path.split('/');

            return segments[segments.length - 1];
        }
    }
}