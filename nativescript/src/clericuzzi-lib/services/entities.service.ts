import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';

// utils
import { SnackUtils } from '@src/external/plugin-utils/utils/snack-utils';
import { ObjectUtils } from '@src/external/plugin-utils/utils/object-utils';

// models
import { Crudable } from '@src/external/plugin-utils/business/models/crud/crudable.model';
import { CustomField } from '@src/external/plugin-utils/business/models/crud/custom-field.model';
import { RequestMessage } from '@src/external/plugin-utils/business/models/request/request-message.model';
import { BatchResponseMessage } from '@src/external/plugin-utils/business/models/response/batch-response-message.model';
import { StatusResponseMessage } from '@src/external/plugin-utils/business/models/response/status-response-message.model';

// services
import { HttpService } from './http.service';
import { LoaderService } from './loader.service';

@Injectable()
export class EntitiesService
{
  public OnDelete: Subject<number[]> = new Subject<number[]>();
  public OnInsert: Subject<Crudable[]> = new Subject<Crudable[]>();
  public OnRefreshRequested: Subject<void> = new Subject<void>();
  constructor(
    private _Http: HttpService,
    private _Snack: MatSnackBar,
    private _Loader: LoaderService,
  )
  {
  }

  public Requestrefresh(): void
  {
    this.OnRefreshRequested.next();
  }

  public Request(url: string, loader: boolean, callback: Function, ...customFields: CustomField[]): void
  {
    if (loader)
      this._Loader.Show();
    const request: RequestMessage = new RequestMessage(url);
    for (const customField of customFields)
      request.Add(customField.Key, customField.Value);

    const response: StatusResponseMessage = new StatusResponseMessage();
    this._Http.Post(request, response, () => this._RequestCallback(response, callback));
  }
  private _RequestCallback(response: StatusResponseMessage, callback: Function = null): void
  {
    this._Loader.Hide();
    SnackUtils.HandleStatusResponse(this._Snack, response);
    if (callback)
      callback(response.Success);
  }

  public Delete(url: string, ids: number[]): void
  {
    this._Loader.Show();
    const request: RequestMessage = new RequestMessage(url);
    request.Add(`entities`, ids);

    const response: StatusResponseMessage = new StatusResponseMessage();
    this._Http.Post(request, response, () => this._DeleteCallback(response, ids));
  }
  private _DeleteCallback(response: StatusResponseMessage, ids: number[]): void
  {
    this._Loader.Hide();
    if (ObjectUtils.GetValue(response.Success, false))
      this.OnDelete.next(ids);

    SnackUtils.HandleStatusResponse(this._Snack, response);
  }

  public Insert(url: string, models: any): void
  {
    this._Loader.Show();
    const request: RequestMessage = new RequestMessage(url);
    if (!Array.isArray(models))
      request.Add(`model`, ((models as any) as Crudable).ToJson());
    else
    {
      const entities: any[] = [];
      for (const model of models)
        entities.push(model.ToJson());
      request.Add(`entities`, entities);
    }

    const response: BatchResponseMessage<any> = new BatchResponseMessage<any>();
    this._Http.Post(request, response, () => this._InsertCallback(response));
  }
  private _InsertCallback(response: BatchResponseMessage<any>): void
  {
    this._Loader.Hide();
    if (!ObjectUtils.GetValue(response.HasErrors, true))
      this.OnInsert.next(response.Entities);

    SnackUtils.HandleBatchResponse(this._Snack, response);
  }

  public Update(url: string, model: any): void
  {
    this._Loader.Show();
    const request: RequestMessage = new RequestMessage(url);
    request.Add(`model`, model);

    const response: StatusResponseMessage = new StatusResponseMessage();
    this._Http.Post(request, response, () => this._UpdateCallback(response));
  }
  private _UpdateCallback(response: StatusResponseMessage): void
  {
    this._Loader.Hide();
    SnackUtils.HandleStatusResponse(this._Snack, response);
  }
}