import { Injectable } from '@angular/core';
import { UserDataService } from './user-data.service';

// services

@Injectable()
export class AuthService
{
  constructor(
    private UserData: UserDataService, )
  {
  }

  public get UserRole(): string
  {
    return this.UserData.UserPermission;
  }
  public CheckPermission(priority: number): boolean
  {
    return this.UserData.CheckPermission(priority);
  }
}