const dateUtils = require('clericuzzi-javascript/dist/utils/date.utils');
const typeUtils = require('clericuzzi-javascript/dist/utils/type.utils');
const arrayUtils = require('clericuzzi-javascript/dist/utils/array.utils');
const mysqlManager = require('clericuzzi-javascript-mysql/dist/business/managers/mysql.manager');

const requestManager = require('clericuzzi-javascript-http/business/managers/request.manager');
const responseManager = require('clericuzzi-javascript-http/business/managers/response.manager');

const getErrors = {
    UNKNOWN: { code: -78623546, humanMessage: `Erro desconhecido` },
    NO_TEST_LEFT: { code: -78623547, humanMessage: `Não há mais telefones válidos a serem testados` },
    NO_TESTED_INFO: { code: -78623548, humanMessage: `A informação de contatos testados não foi enviada`, devMessage: `testedIds not found...` },
}

module.exports.get = async (event, context, callback) =>
{
    try
    {
        context.callbackWaitsForEmptyEventLoop = false;
        const queryOptions = { sql: 'select `contact_id` as `id`, `contact_phone1` as `phone` from `brdn_contact` where `contact_phone1` is not null and `tested_abr` = 0 order by rand() limit 100;' };

        const result = await mysqlManager.query(queryOptions);
        if (typeUtils.isArray(result))
        {
            if (typeUtils.isArrayAndValid(result))
                responseManager.success(callback, result);
            else
                responseManager.fail(callback, getErrors.NO_TEST_LEFT);
        }
        else
            responseManager.fail(callback, getErrors.UNKNOWN);
    }
    catch (error)
    {
        responseManager.fail(callback, getErrors.UNKNOWN, error);
    }
};
module.exports.post = async (event, context, callback) =>
{
    try
    {
        context.callbackWaitsForEmptyEventLoop = false;
        const payload = requestManager.parse(event);
        const conn = await mysqlManager.newConnectionFromPool(true);

        if (!payload.testedIds)
            responseManager.fail(callback, getErrors.NO_TESTED_INFO);

        await manageTests(payload.testedIds, conn);
        await manageCompanies(payload.companies, conn);
        await managePhoneInfo(payload.phones, conn);

        conn.commit();
        responseManager.success(callback, true);
    }
    catch (error)
    {
        responseManager.fail(callback, getErrors.UNKNOWN, error);
    }
};

const clearPhonesInfo = async (phones, conn) =>
{
    const phoneIds = phones.map(i => i.id);
    await mysqlManager.query({ sql: `delete from \`brdn_operator_contact\` where \`contact_id\` in (${phoneIds.join(`,`)});` }, conn);
};
const updatePhonesInfo = async conn =>
{
    await mysqlManager.query({ sql: `update \`brdn_contact\` \`c\` set \`c\`.\`current_operator\` = (select \`o\`.\`name\` from \`brdn_operator_contact\` \`oc\` join \`brdn_operator\` \`o\` on \`o\`.\`id\` = \`oc\`.\`operator_id\` where \`oc\`.\`contact_id\` = \`c\`.\`contact_id\` order by \`oc\`.\`id\` asc limit 1);` }, conn);
};
const registerPhonesInfo = async (phones, companyIdMap, conn) =>
{
    let insertParams = [];
    for (let phone of phones)
        for (let history of phone.history)
            insertParams.push(`(null, ${companyIdMap.get(history.company)}, ${phone.id}, ${!history.until ? `null` : `'${history.until}'`})`);

    const insertCommand = `insert into \`brdn_operator_contact\` values ${insertParams.join(`,`)}`;
    await mysqlManager.query({ sql: insertCommand }, conn);
};

const getCompanies = async (conn) =>
{
    const queryOptions = { sql: 'select `name` from `brdn_operator`' };

    const existingCompanies = await mysqlManager.query(queryOptions, conn);
    if (existingCompanies.length > 0)
        return existingCompanies.map(i => i.name);
    else
        return [];
};
const getCompaniesIdMap = async (conn) =>
{
    const queryOptions = { sql: 'select `id`, `name` from `brdn_operator`' };
    const existingCompanies = await mysqlManager.query(queryOptions, conn);
    if (existingCompanies.length > 0)
    {
        const mapper = new Map();
        for (let existingCompany of existingCompanies)
            mapper.set(existingCompany.name, existingCompany.id);

        return mapper;
    }
    else
        return new Map();
};

const manageTests = async (testedIds, conn) =>
{
    await mysqlManager.query({ sql: `update \`brdn_contact\` set \`tested_abr\` = 1 where contact_id in (${testedIds.join(`,`)});` }, conn);
};
const managePhoneInfo = async (phones, conn) =>
{
    const companyIdMap = await getCompaniesIdMap(conn);
    await clearPhonesInfo(phones, conn);
    await updatePhonesInfo(conn);
    await registerPhonesInfo(phones, companyIdMap, conn);
};
const manageCompanies = async (companies, conn) =>
{
    const newCompanies = [];
    const existingCompanies = await getCompanies(conn);
    for (let company of companies)
        if (!existingCompanies.includes(company))
            newCompanies.push(company);

    if (newCompanies.length > 0)
    {
        const insertCommandValues = [];
        for (let newCompany of newCompanies)
            insertCommandValues.push(`(null, '${newCompany}')`);

        const insertCommand = `insert into \`brdn_operator\` values ${insertCommandValues.join(`,`)};`;
        const insertOptions = { sql: insertCommand };
        await mysqlManager.query(insertOptions, conn);
    }
};