'use strict';

const rdsManager = require('clericuzzi-javascript-aws/managers/rdsManager');
const requestManager = require('clericuzzi-javascript/models/communication/requestManager');
const responseManager = require('clericuzzi-javascript/models/communication/responseManager');

const jwk = require('../../../jwk');
const loginManager = require('clericuzzi-javascript-aws/managers/loginManager');

const dateUtils = require('clericuzzi-javascript/utils/dateUtils');
const typeUtils = require('clericuzzi-javascript/utils/typeUtils');
const sqlActions = require('clericuzzi-javascript/models/sql/sqlActions');

const crudTypes = require('../../enums/crudType.enum');

// datasources
const dataSourceInsert = async (model, conn) =>
{
    const result = await rdsManager.runQuery({ sql: 'insert into `data_source` values (null, ?, ?, ?)', values: [model.type, model.name, model.created_by] }, conn);
    if (!result || !result.insertId)
        throw Error(`insert failed`);

    const newId = result.insertId;
    if (typeUtils.isArrayAndValid(model.selectedChildren))
        for (const child of model.selectedChildren)
            await rdsManager.runQuery({ sql: 'insert into `data_source_child` values (null, ?, ?)', values: [newId, child] }, conn);
}
const dataSourceUpdate = async (model, conn) =>
{
    if (!model.id)
        throw Error(`an update cannot be executed without the item's id value`);

    const queryOptions = { sql: 'update `data_source` set `type_id` = ?, `name` = ? where `id` = ?', values: [model.type, model.name, model.id] };
    await rdsManager.runQuery(queryOptions, conn);

    await rdsManager.runQuery({ sql: 'delete from `data_source_child` where `parent_id` = ?', values: [model.id] }, conn);
    if (typeUtils.isArrayAndValid(model.selectedChildren))
    {
        for (const child of model.selectedChildren)
            await rdsManager.runQuery({ sql: 'insert into `data_source_child` values (null, ?, ?)', values: [model.id, child.value] }, conn);
    }
}
const dataSourceDelete = async (model, conn) =>
{
    if (!model.id)
        throw Error(`a delete cannot be executed without the item's id value`);

    await rdsManager.runQuery({ sql: 'delete from `data_source_child` where `child_id` = ?', values: [model.id] }, conn);
    await rdsManager.runQuery({ sql: 'delete from `data_source_child` where `parent_id` = ?', values: [model.id] }, conn);
    await rdsManager.runQuery({ sql: 'delete from `data_source` where `id` = ?', values: [model.id] }, conn);
}
const dataSourceEndpoint = async (action, model, conn) =>
{
    try
    {
        switch (action)
        {
            case sqlActions.Insert: await dataSourceInsert(model, conn); break;
            case sqlActions.Update: await dataSourceUpdate(model, conn); break;
            case sqlActions.Delete: await dataSourceDelete(model, conn); break;

            default: throw Error(`'${action} is not a valid sqlAction`);
        }
    }
    catch (err)
    {
        throw err;
    }
}
// datasources end

// criterion
const criterionInsert = async (model, conn) =>
{
    const result = await rdsManager.runQuery({ sql: 'insert into `criterion` values (null, ?, ?, ?, ?)', values: [model.name, model.metric, model.abbreviation, model.valueType] }, conn);
    if (!result || !result.insertId)
        throw Error(`insert failed`);
}
const criterionUpdate = async (model, conn) =>
{
    if (!model.id)
        throw Error(`an update cannot be executed without the item's id value`);

    const queryOptions = { sql: 'update `criterion` set `name` = ?, `metric` = ?, `abbreviation` = ?, `value_type` = ? where `id` = ?', values: [model.name, model.metric, model.abbreviation, model.valueType, model.id] };
    await rdsManager.runQuery(queryOptions, conn);
}
const criterionDelete = async (model, conn) =>
{
    if (!model.id)
        throw Error(`a delete cannot be executed without the item's id value`);

    await rdsManager.runQuery({ sql: 'delete from `criterion` where `id` = ?', values: [model.id] }, conn);
}
const criterionEndpoint = async (action, model, conn) =>
{
    try
    {
        switch (action)
        {
            case sqlActions.Insert: await criterionInsert(model, conn); break;
            case sqlActions.Update: await criterionUpdate(model, conn); break;
            case sqlActions.Delete: await criterionDelete(model, conn); break;

            default: throw Error(`'${action} is not a valid sqlAction`);
        }
    }
    catch (err)
    {
        throw err;
    }
}
// criterion end

// data input
const dataInputInsert = async (model, conn) =>
{
    const queryOptions = { sql: 'insert into `data_input` values (null, ?, ?, ?, ?, ?, year(?), month(?), now())', values: [model.dataSourceId, model.criterionId, model.amount, model.date, model.date, model.date] };
    const result = await rdsManager.runQuery(queryOptions, conn);
    if (!result || !result.insertId)
        throw Error(`insert failed`);
}
const dataInputUpdate = async (model, conn) =>
{
    if (!model.id)
        throw Error(`an update cannot be executed without the item's id value`);

    const queryOptions = { sql: 'update `data_input` set `data_source_id` = ?, `criterion_id` = ?, `amount` = ?, `date` = ?, `date_month` = year(?), `date_month` = month(?) where `id` = ?', values: [model.dataSourceId, model.criterionId, model.amount, model.date, model.date, model.date, model.id] };
    await rdsManager.runQuery(queryOptions, conn);
}
const dataInputDelete = async (model, conn) =>
{
    if (!model.id)
        throw Error(`a delete cannot be executed without the item's id value`);

    await rdsManager.runQuery({ sql: 'delete from `data_input` where `id` = ?', values: [model.id] }, conn);
}
const dataInputEndpoint = async (action, model, conn) =>
{
    try
    {
        switch (action)
        {
            case sqlActions.Insert: await dataInputInsert(model, conn); break;
            case sqlActions.Update: await dataInputUpdate(model, conn); break;
            case sqlActions.Delete: await dataInputDelete(model, conn); break;

            default: throw Error(`'${action} is not a valid sqlAction`);
        }
    }
    catch (err)
    {
        throw err;
    }
}
// data input end

// model
const modelInsert = async (model, conn) =>
{
    const queryOptions = { sql: 'insert into `model` values (null, ?, ?)', values: [model.name] };
    const result = await rdsManager.runQuery(queryOptions, conn);
    if (!result || !result.insertId)
        throw Error(`insert failed`);
}
const modelUpdate = async (model, conn) =>
{
    if (!model.id)
        throw Error(`an update cannot be executed without the item's id value`);

    const queryOptions = { sql: 'update `model` set `name` = ? where `id` = ?', values: [model.name, model.id] };
    await rdsManager.runQuery(queryOptions, conn);
}
const modelDelete = async (model, conn) =>
{
    if (!model.id)
        throw Error(`a delete cannot be executed without the item's id value`);

    await rdsManager.runQuery({ sql: 'delete from `model` where `id` = ?', values: [model.id] }, conn);
}
const modelEndpoint = async (action, model, conn) =>
{
    try
    {
        switch (action)
        {
            case sqlActions.Insert: await modelInsert(model, conn); break;
            case sqlActions.Update: await modelUpdate(model, conn); break;
            case sqlActions.Delete: await modelDelete(model, conn); break;

            default: throw Error(`'${action} is not a valid sqlAction`);
        }
    }
    catch (err)
    {
        throw err;
    }
}
// model end

// model criterion
const modelCriterionInsert = async (model, conn) =>
{
    const queryOptions = { sql: 'insert into `model_criterion` values (null, ?, ?, ?, ?, ?)', values: [model.modelId, model.criterionId, model.bias, model.consolidation, model.weight] };
    const result = await rdsManager.runQuery(queryOptions, conn);
    if (!result || !result.insertId)
        throw Error(`insert failed`);
}
const modelCriterionUpdate = async (model, conn) =>
{
    if (!model.id)
        throw Error(`an update cannot be executed without the item's id value`);

    const queryOptions = { sql: 'update `model_criterion` set `criterion_id` = ?, `bias` = ?, `consolidation` = ?, `weight` = ? where `id` = ?', values: [model.criterionId, model.bias, model.consolidation, model.weight, model.id] };
    await rdsManager.runQuery(queryOptions, conn);
}
const modelCriterionDelete = async (model, conn) =>
{
    if (!model.id)
        throw Error(`a delete cannot be executed without the item's id value`);

    await rdsManager.runQuery({ sql: 'delete from `model_criterion` where `id` = ?', values: [model.id] }, conn);
}
const modelCriterionEndpoint = async (action, model, conn) =>
{
    try
    {
        switch (action)
        {
            case sqlActions.Insert: await modelCriterionInsert(model, conn); break;
            case sqlActions.Update: await modelCriterionUpdate(model, conn); break;
            case sqlActions.Delete: await modelCriterionDelete(model, conn); break;

            default: throw Error(`'${action} is not a valid sqlAction`);
        }
    }
    catch (err)
    {
        throw err;
    }
}
// model criterion end

// model criterion goal
const modelCriterionGoalInsert = async (model, conn) =>
{
    const queryOptions = { sql: 'insert into `data_source_criterion_goal` values (null, ?, ?, ?, 1, 2020)', values: [model.dataSourceId, model.modelCriterionId, model.value] };
    const result = await rdsManager.runQuery(queryOptions, conn);
    if (!result || !result.insertId)
        throw Error(`insert failed`);
}
const modelCriterionGoalUpdate = async (model, conn) =>
{
    if (!model.id)
        throw Error(`an update cannot be executed without the item's id value`);

    const queryOptions = { sql: 'update `data_source_criterion_goal` set `data_source_id` = ?, `value` = ? where `id` = ?', values: [model.dataSourceId, model.value, model.id] };
    await rdsManager.runQuery(queryOptions, conn);
}
const modelCriterionGoalDelete = async (model, conn) =>
{
    if (!model.id)
        throw Error(`a delete cannot be executed without the item's id value`);

    await rdsManager.runQuery({ sql: 'delete from `data_source_criterion_goal` where `id` = ?', values: [model.id] }, conn);
}
const modelCriterionGoalEndpoint = async (action, model, conn) =>
{
    try
    {
        switch (action)
        {
            case sqlActions.Insert: await modelCriterionGoalInsert(model, conn); break;
            case sqlActions.Update: await modelCriterionGoalUpdate(model, conn); break;
            case sqlActions.Delete: await modelCriterionGoalDelete(model, conn); break;

            default: throw Error(`'${action} is not a valid sqlAction`);
        }
    }
    catch (err)
    {
        throw err;
    }
}
// model criterion goal end

// campaign
const campaignInsert = async (model, conn) =>
{
    const queryOptions = { sql: 'insert into `campaign` values (null, ?, ?, ?, ?)', values: [model.name, model.modelCriterionId, model.pointSchema] };
    const result = await rdsManager.runQuery(queryOptions, conn);
    if (!result || !result.insertId)
        throw Error(`insert failed`);

    const campaignId = result.insertId;
    for (const dataSourceId of model.dataSourceIds)
        await rdsManager.runQuery({ sql: 'insert into `campaign_data_source` values (null, ?, ?)', values: [campaignId, dataSourceId] }, conn);
}
const campaignUpdate = async (model, conn) =>
{
    if (!model.id)
        throw Error(`an update cannot be executed without the item's id value`);

    await rdsManager.runQuery({ sql: 'delete from `campaign_data_source` where `campaign_id` = ?', values: [model.id] }, conn);
    for (const dataSourceId of model.dataSourceIds)
        await rdsManager.runQuery({ sql: 'insert into `campaign_data_source` values (null, ?, ?)', values: [model.id, dataSourceId] }, conn);

    const queryOptions = { sql: 'update `campaign` set `name` = ?, `point_schema` = ? where `id` = ?', values: [model.name, model.pointSchema, model.id] };
    await rdsManager.runQuery(queryOptions, conn);
}
const campaignDelete = async (model, conn) =>
{
    if (!model.id)
        throw Error(`a delete cannot be executed without the item's id value`);

    await rdsManager.runQuery({ sql: 'delete from `campaign_data_source` where `campaign_id` = ?', values: [model.id] }, conn);
    await rdsManager.runQuery({ sql: 'delete from `campaign` where `id` = ?', values: [model.id] }, conn);
}
const campaignEndpoint = async (action, model, conn) =>
{
    try
    {
        switch (action)
        {
            case sqlActions.Insert: await campaignInsert(model, conn); break;
            case sqlActions.Update: await campaignUpdate(model, conn); break;
            case sqlActions.Delete: await campaignDelete(model, conn); break;

            default: throw Error(`'${action} is not a valid sqlAction`);
        }
    }
    catch (err)
    {
        throw err;
    }
}
// campaign end

// communication
const communicationInsert = async (model, conn) =>
{
    const queryOptions = { sql: 'insert into `communication` values (null, ?, ?, ?, now())', values: [model.title, model.link] };
    const result = await rdsManager.runQuery(queryOptions, conn);
    if (!result || !result.insertId)
        throw Error(`insert failed`);
}
const communicationUpdate = async (model, conn) =>
{
    if (!model.id)
        throw Error(`an update cannot be executed without the item's id value`);

    const queryOptions = { sql: 'update `communication` set `title` = ?, `link` = ? where `id` = ?', values: [model.title, model.link, model.id] };
    await rdsManager.runQuery(queryOptions, conn);
}
const communicationDelete = async (model, conn) =>
{
    if (!model.id)
        throw Error(`a delete cannot be executed without the item's id value`);

    await rdsManager.runQuery({ sql: 'delete from `communication` where `id` = ?', values: [model.id] }, conn);
}
const communicationEndpoint = async (action, model, conn) =>
{
    try
    {
        switch (action)
        {
            case sqlActions.Insert: await communicationInsert(model, conn); break;
            case sqlActions.Update: await communicationUpdate(model, conn); break;
            case sqlActions.Delete: await communicationDelete(model, conn); break;

            default: throw Error(`'${action} is not a valid sqlAction`);
        }
    }
    catch (err)
    {
        throw err;
    }
}
// communication end

// user group
const userGroupInsert = async (model, conn) =>
{
    const queryOptions = { sql: 'insert into `user_group` values (null, ?, ?, now())', values: [model.name] };
    const result = await rdsManager.runQuery(queryOptions, conn);
    if (!result || !result.insertId)
        throw Error(`insert failed`);

    const userGroupId = result.insertId;
    for (const groupUser of model.users)
        await rdsManager.runQuery({ sql: 'insert into `user_group_user` values (null, ?, ?)', values: [userGroupId, groupUser] }, conn);
}
const userGroupUpdate = async (model, conn) =>
{
    if (!model.id)
        throw Error(`an update cannot be executed without the item's id value`);

    await rdsManager.runQuery({ sql: 'delete from `user_group_user` where `user_group_id` = ?', values: [model.id] }, conn);
    for (const groupUser of model.users)
        await rdsManager.runQuery({ sql: 'insert into `user_group_user` values (null, ?, ?)', values: [model.id, groupUser] }, conn);

    const queryOptions = { sql: 'update `user_group` set `name` = ? where `id` = ?', values: [model.name, model.id] };
    await rdsManager.runQuery(queryOptions, conn);
}
const userGroupDelete = async (model, conn) =>
{
    if (!model.id)
        throw Error(`a delete cannot be executed without the item's id value`);

    await rdsManager.runQuery({ sql: 'delete from `user_group_user` where `user_group_id` = ?', values: [model.id] }, conn);
    await rdsManager.runQuery({ sql: 'delete from `user_group` where `id` = ?', values: [model.id] }, conn);
}
const userGroupEndpoint = async (action, model, conn) =>
{
    try
    {
        switch (action)
        {
            case sqlActions.Insert: await userGroupInsert(model, conn); break;
            case sqlActions.Update: await userGroupUpdate(model, conn); break;
            case sqlActions.Delete: await userGroupDelete(model, conn); break;

            default: throw Error(`'${action} is not a valid sqlAction`);
        }
    }
    catch (err)
    {
        throw err;
    }
}
// user group end

// message 
const messageInsert = async (model, conn) =>
{
    const queryOptions = { sql: 'insert into `message` values (null, ?, ?, ?, now())', values: [user, model.to, model.content] };
    const result = await rdsManager.runQuery(queryOptions, conn);
    if (!result || !result.insertId)
        throw Error(`insert failed`);
}
const messageEndpoint = async (action, model, conn) =>
{
    try
    {
        switch (action)
        {
            case sqlActions.Insert: await messageInsert(model, conn); break;

            default: throw Error(`'${action} is not a valid sqlAction`);
        }
    }
    catch (err)
    {
        throw err;
    }
}
// message end

// sale
const saleInsert = async (model, conn) =>
{
    const queryOptions = { sql: 'insert into `sale` values (null, ?, ?, ?, ?, ?, now())', values: [model.userId, model.value, model.amount, model.margin, model.cost] };
    const result = await rdsManager.runQuery(queryOptions, conn);
    if (!result || !result.insertId)
        throw Error(`insert failed`);

    const saleId = result.insertId;

    let dataInputInsertOptions = { sql: 'insert into `data_input` values (null, ?, ?, ?, ?, ?, year(?), month(?), now())', values: [model.dataSource, 3, model.userId, model.value, model.date, model.date, model.date] };
    let dataInputInsertResult = await rdsManager.runQuery(dataInputInsertOptions, conn);
    let dataInputValueId = dataInputInsertResult.insertId;

    dataInputInsertOptions = { sql: 'insert into `data_input` values (null, ?, ?, ?, ?, ?, year(?), month(?), now())', values: [model.dataSource, 8, model.userId, model.amount, model.date, model.date, model.date] };
    dataInputInsertResult = await rdsManager.runQuery(dataInputInsertOptions, conn);
    let dataInputQtdId = dataInputInsertResult.insertId;

    dataInputInsertOptions = { sql: 'insert into `data_input` values (null, ?, ?, ?, ?, ?, year(?), month(?), now())', values: [model.dataSource, 10, model.userId, (model.value * (model.margin / 100)).toFixed(6), model.date, model.date, model.date] };
    dataInputInsertResult = await rdsManager.runQuery(dataInputInsertOptions, conn);
    let dataInputMarginId = dataInputInsertResult.insertId;

    dataInputInsertOptions = { sql: 'insert into `data_input` values (null, ?, ?, ?, ?, ?, year(?), month(?), now())', values: [model.dataSource, 11, model.userId, model.cost, model.date, model.date, model.date] };
    dataInputInsertResult = await rdsManager.runQuery(dataInputInsertOptions, conn);
    let dataInputCostId = dataInputInsertResult.insertId;

    let saleDataInputsOptions = { sql: 'insert into `sale_data_input` values (null, ?, ?), (null, ?, ?), (null, ?, ?), (null, ?, ?)', values: [saleId, dataInputValueId, saleId, dataInputQtdId, saleId, dataInputMarginId, saleId, dataInputCostId] };
    const saleDatainputsInsertResult = await rdsManager.runQuery(saleDataInputsOptions, conn);
    if (!saleDatainputsInsertResult || !saleDatainputsInsertResult.insertId)
        throw Error(`insert failed`);
}
const saleUpdate = async (model, conn) =>
{
    if (!model.id)
        throw Error(`a delete cannot be executed without the item's id value`);

    await rdsManager.runQuery({ sql: 'delete from `communication` where `id` = ?', values: [model.id] }, conn);
}
const saleDelete = async (model, conn) =>
{
    if (!model.id)
        throw Error(`a delete cannot be executed without the item's id value`);

    await rdsManager.runQuery({ sql: 'delete from `communication` where `id` = ?', values: [model.id] }, conn);
}
const saleEndpoint = async (action, model, conn) =>
{
    try
    {
        switch (action)
        {
            case sqlActions.Insert: await saleInsert(model, conn); break;
            case sqlActions.Update: await saleUpdate(model, conn); break;
            case sqlActions.Delete: await saleDelete(model, conn); break;

            default: throw Error(`'${action} is not a valid sqlAction`);
        }
    }
    catch (err)
    {
        throw err;
    }
}
// communication end

module.exports.upsert = async (event, context, callback) =>
{
    let conn = null;

    try
    {
        context.callbackWaitsForEmptyEventLoop = false;
        const input = requestManager.parse(event);
        await loginManager.getUserByToken(input.token, jwk);
        conn = await rdsManager.newConnectionFromPool(true);

        if (!input.crudType)
            throw Error(`you must specify a value for the 'crudType'`);

        if (!input.sqlAction)
            throw Error(`you must specify a value for the 'sqlAction'`);

        if (!input.model)
            throw Error(`you must specify model to be affected`);

        const crudType = input.crudType;
        switch (crudType)
        {
            case crudTypes.Model: await modelEndpoint(input.sqlAction, input.model, conn); break;
            case crudTypes.Criterion: await criterionEndpoint(input.sqlAction, input.model, conn); break;
            case crudTypes.DataInput: await dataInputEndpoint(input.sqlAction, input.model, conn); break;
            case crudTypes.DataSource: await dataSourceEndpoint(input.sqlAction, input.model, conn); break;
            case crudTypes.ModelCriterion: await modelCriterionEndpoint(input.sqlAction, input.model, conn); break;
            case crudTypes.ModelCriterionGoal: await modelCriterionGoalEndpoint(input.sqlAction, input.model, conn); break;

            case crudTypes.Message: await messageEndpoint(input.sqlAction, input.model, conn); break;
            case crudTypes.Campaign: await campaignEndpoint(input.sqlAction, input.model, conn); break;
            case crudTypes.UserGroup: await userGroupEndpoint(input.sqlAction, input.model, conn); break;
            case crudTypes.Communication: await communicationEndpoint(input.sqlAction, input.model, conn); break;

            case crudTypes.Sale: await saleEndpoint(input.sqlAction, input.model, conn); break;

            default: throw Error(`not a valid crudType`);
        }

        conn.commit();
        responseManager.success(callback, { success: true });
    }
    catch (err)
    {
        responseManager.error(callback, err);
    }
    finally
    {
        if (typeUtils.isValid(conn))
            conn.release();
    }
};