'use strict';

const rdsManager = require('clericuzzi-javascript-aws/managers/rdsManager');
const responseManager = require('clericuzzi-javascript/models/communication/responseManager');

/**
 * executes a given query via the queryOptions format
 * if a connection is not provided, one will be allocated just for the purposes of this one query and will be released right away
 * this is the only method that is not built on top of a SqlBuilder, hence is the only one that should be invoked
 * from within the server side
 */
module.exports.runQuery = async (event, context, callback) =>
{
    try
    {
        context.callbackWaitsForEmptyEventLoop = false;
        await rdsManager.runQuery(event, context, callback);
    }
    catch (error)
    {
        responseManager.error(callback, error, 501);
    }
};

/**
 * default query method based in a SqlBuilder
 */
module.exports.query = async (event, context, callback) =>
{
    try
    {
        context.callbackWaitsForEmptyEventLoop = false;
        await rdsManager.query(event, context, callback);
    }
    catch (error)
    {
        responseManager.error(callback, error, 501);
    }
};

/**
 * default isnert method based in a SqlBuilder
 */
module.exports.insert = async (event, context, callback) =>
{
    try
    {
        context.callbackWaitsForEmptyEventLoop = false;
        await rdsManager.insert(event, context, callback);
    }
    catch (error)
    {
        responseManager.error(callback, error, 501);
    }
};

/**
 * default remove method based in a SqlBuilder
 */
module.exports.remove = async (event, context, callback) =>
{
    try
    {
        context.callbackWaitsForEmptyEventLoop = false;
        await rdsManager.remove(event, context, callback);
    }
    catch (error)
    {
        responseManager.error(callback, error, 501);
    }
};

/**
 * default update method based in a SqlBuilder
 */
module.exports.update = async (event, context, callback) =>
{
    try
    {
        context.callbackWaitsForEmptyEventLoop = false;
        await rdsManager.update(event, context, callback);
    }
    catch (error)
    {
        responseManager.error(callback, error, 501);
    }
};

/**
 * default pagination method based in a SqlBuilder
 */
module.exports.paginate = async (event, context, callback) =>
{
    try
    {
        context.callbackWaitsForEmptyEventLoop = false;
        await rdsManager.paginate(event, context, callback);
    }
    catch (error)
    {
        responseManager.error(callback, error, 501);
    }
};

/**
 * default listing method based in a SqlBuilder { id: x, label: y}
 */
module.exports.simpleList = async (event, context, callback) =>
{
    try
    {
        context.callbackWaitsForEmptyEventLoop = false;
        await rdsManager.simpleList(event, context, callback);
    }
    catch (error)
    {
        responseManager.error(callback, error, 501);
    }
};