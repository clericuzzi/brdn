const typeUtils = require('clericuzzi-javascript/dist/utils/type.utils');

const { Request, parse } = require('clericuzzi-javascript-express/src/managers/request.manager');
const { Response, error, success } = require('clericuzzi-javascript-express/src/managers/response.manager');

const mysqlManager = require('clericuzzi-javascript-mysql/dist/business/managers/mysql.manager');

const UserDataModel = require('brdn-node-server-entities/src/models/userData.model');
const SimplifiqueTargetModel = require('brdn-node-server-entities/src/models/simplifiqueTarget.model');

const InfoModel = require('brdn-node-server-entities/src/entities/brdn_company_simplifique_info.model');
const BillModel = require('brdn-node-server-entities/src/entities/brdn_company_simplifique_info_unpaid_bill.model');
const AgingModel = require('brdn-node-server-entities/src/entities/brdn_company_simplifique_info_unpaid_bill_aging.model');
const WalletModel = require('brdn-node-server-entities/src/entities/brdn_company_simplifique_info_wallet.model');
const WalletTeamMemberModel = require('brdn-node-server-entities/src/entities/brdn_company_simplifique_info_wallet_team_member.model');

/**
 * gets the next company to be checked on the simplify platform
 * if a company id is provided, that is the company that will be checked
 * 
 * @param {Request} req the request input data
 * @param {Response} res the request input data
 *
 * @returns {SimplifiqueTargetModel} the current target to be checked
 */
module.exports.fetch = async (req, res) =>
{
    let conn = null;
    const { userId, companyCnpj } = parse(req);
    try
    {
        conn = await mysqlManager.newConnectionFromPool(true);
        if (!userId)
            error(res, `the userId is mandatory`, `userId is null`, -12, 200);
        else
        {
            let result = new SimplifiqueTargetModel(null, userId);
            if (companyCnpj)
            {
                const { cnpj } = (await mysqlManager.query(conn, { sql: `select \`company_cnpj\` as \`cnpj\` from \`brdn_company\` where \`company_cnpj\` = ?`, values: [companyCnpj] }))[0];
                if (!cnpj)
                    error(res, `the cnpj '${cnpj}' does not exist in the database`, `invalidcnpj`, -13, 200);
                else
                    result.cnpj = cnpj;
            }
            else
            {
                const fetchResult = (await mysqlManager.query(conn, { sql: `select \`simplifiqueNextCompanyByUserId\` (?) as \`cnpj\``, values: [userId] }))[0];
                result.cnpj = fetchResult.cnpj;
            }

            success(res, result);
        }
    }
    catch (err)
    {
        error(res, err.message || err.sqlMessage, `could not get the current target`, -4);
    }
    finally
    {
        await mysqlManager.transactionCommit(conn);
    }
};

/**
 * receives the collected data from the macro and soteres it on the database
 *
 * @param {Request} req the request data
 * @param {Response} res the response data
 */
module.exports.submit = async (req, res) =>
{
    let conn = null;
    try
    {
        conn = await mysqlManager.newConnectionFromPool(true);
        const userData = getUserData(req);
        const { cnpj, userId } = req.body;

        await sendInfoData(conn, userData, cnpj);
        await sendOrderData(conn, userData, cnpj);
        await sendPlantData(conn, userData, cnpj);
        await sendCreditData(conn, userData, cnpj);
        await sendWalletData(conn, userData, cnpj);
        await sendContractData(conn, userData, cnpj);
        await sendPortabilityData(conn, userData, cnpj);

        await finishDataCollection(conn, userId, cnpj);

        await mysqlManager.transactionCommit(conn);
        success(res);
    }
    catch (err)
    {
        await mysqlManager.transactionRollback(conn);
        error(res, err.message, `could not save the data on the server`, -2);
    }
};

/**
 * extracts the userData from the initial request
 *
 * @param {Request} req the request data
 * @returns {UserDataModel} the data collected by the calling system
 */
const getUserData = req =>
{
    const { userData } = req.body;
    return userData;
};


/**
 * sends the user's orders related information
 *
 * @param {any} conn database connection
 * @param {UserDataModel} userData the collected data
 * @param {string} cnpj the company's cnpj
 */
const sendInfoData = async (conn, userData, cnpj) =>
{
    await mysqlManager.query(conn, { sql: `delete from \`brdn_company_simplifique_info\` where \`company_cnpj\` = ? `, values: [cnpj] });
    await InfoModel.new(userData, cnpj).insert(conn, null, false);
};
/**
 * sends the user's orders related information
 *
 * @param {any} conn database connection
 * @param {UserDataModel} userData the collected data
 * @param {string} cnpj the company's cnpj
 */
const sendOrderData = async (conn, userData, cnpj) =>
{
};
/**
 * sends the user's plant related information
 *
 * @param {any} conn database connection
 * @param {UserDataModel} userData the collected data
 * @param {string} cnpj the company's cnpj
 */
const sendPlantData = async (conn, userData, cnpj) =>
{
};
/**
 * sends the user's credit related information
 *
 * @param {any} conn database connection
 * @param {UserDataModel} userData the collected data
 * @param {string} cnpj the company's cnpj
 */
const sendCreditData = async (conn, userData, cnpj) =>
{
    if (typeUtils.isArrayAndValid(userData.billingData))
    {
        await mysqlManager.query(conn, { sql: `delete from \`brdn_company_simplifique_info_unpaid_bill\` where \`company_cnpj\` = ? `, values: [cnpj] });

        for (let bill of userData.billingData || [])
            await BillModel.new(bill, cnpj).insert(conn, null, false);
    }
    if (typeUtils.isArrayAndValid(userData.billingAgingData))
    {
        await mysqlManager.query(conn, { sql: `delete from \`brdn_company_simplifique_info_unpaid_bill_aging\` where \`company_cnpj\` = ? `, values: [cnpj] });

        for (let aging of userData.billingAgingData || [])
            await AgingModel.new(aging, cnpj).insert(conn, null, false);
    }
};
/**
 * sends the user's wallet related information
 *
 * @param {any} conn database connection
 * @param {UserDataModel} userData the collected data
 * @param {string} cnpj the company's cnpj
 */
const sendWalletData = async (conn, userData, cnpj) =>
{
    if (userData.walletLandSegment && userData.walletLandUpdatedIn && userData.walletMobileSegment, userData.walletMobileSegment)
    {
        await mysqlManager.query(conn, { sql: `delete from \`brdn_company_simplifique_info_wallet_team_member\` where \`wallet_id\` in (select \`id\` from \`brdn_company_simplifique_info_wallet\` where \`company_cnpj\` = ?) `, values: [cnpj] });
        await mysqlManager.query(conn, { sql: `delete from \`brdn_company_simplifique_info_wallet\` where \`company_cnpj\` = ? `, values: [cnpj] });

        const landWallet = WalletModel.new(cnpj, `fixa`, userData.walletLandSegment, userData.walletLandUpdatedIn);
        const mobileWallet = WalletModel.new(cnpj, `móvel`, userData.walletMobileSegment, userData.walletMobileSegment);

        await landWallet.insert(conn, null, false);
        await mobileWallet.insert(conn, null, false);

        for (let landMember of userData.walletLandTeam || [])
            await WalletTeamMemberModel.new(landMember, landWallet.id).insert(conn, null, false);
        for (let mobileMember of userData.walletMobileTeam || [])
            await WalletTeamMemberModel.new(mobileMember, mobileWallet.id).insert(conn, null, false);
    }
};
/**
 * sends the user's contract related information
 *
 * @param {any} conn database connection
 * @param {UserDataModel} userData the collected data
 * @param {string} cnpj the company's cnpj
 */
const sendContractData = async (conn, userData, cnpj) =>
{
};
/**
 * sends the user's portability related information
 *
 * @param {any} conn database connection
 * @param {UserDataModel} userData the collected data
 * @param {string} cnpj the company's cnpj
 */
const sendPortabilityData = async (conn, userData, cnpj) =>
{
};

/**
 * sends the OK signal, that the simplifique information was fetched successfully
 *
 * @param {any} conn database connection
 * @param {userId} userId the caller's id
 * @param {string} cnpj the company's cnpj
 */
const finishDataCollection = async (conn, userId, cnpj) =>
{
    await mysqlManager.query(conn, { sql: `update \`brdn_campaign_company\` set \`information_collected\` = 1, \`information_collected_in\` = now() where \`company_cnpj\` = ? and \`user_id\` = ? `, values: [cnpj, userId] });
};