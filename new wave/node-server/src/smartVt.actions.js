const dateUtils = require('clericuzzi-javascript/dist/utils/date.utils');
const typeUtils = require('clericuzzi-javascript/dist/utils/type.utils');
const arrayUtils = require('clericuzzi-javascript/dist/utils/array.utils');
const mysqlManager = require('clericuzzi-javascript-mysql/dist/business/managers/mysql.manager');

const { Request, parse } = require('clericuzzi-javascript-express/src/managers/request.manager');
const { Response, error, success } = require('clericuzzi-javascript-express/src/managers/response.manager');

const AvailabilityModel = require('./models/availability.model');

const getErrors = {
    UNKNOWN: { code: -78623546, humanMessage: `Erro inesperado` },
    NO_TEST_LEFT: { code: -78623547, humanMessage: `Não há mais telefones válidos a serem testados` },
    NO_TESTED_INFO: { code: -78623548, humanMessage: `A informação de contatos testados não foi enviada`, devMessage: `testedIds not found...` },
}

module.exports.fetch = async (req, res) =>
{
    let conn = null;
    let result = null;
    try
    {
        conn = await mysqlManager.newConnectionFromPool(true);
        // const { host } = parse(req);

        // const existingHostQuery = { sql: `select \`machine_name\` from \`brdn_machine_campaign\` where \`machine_name\` = ? `, values: [host] };
        // const existingHost = (await mysqlManager.query(conn, existingHostQuery))[0];

        // if (!existingHost)
        // {
        //     const insertQuery = { sql: `insert into \`brdn_machine_campaign\` values (null, ?, null)`, values: [host] };
        //     await mysqlManager.nonQuery(conn, insertQuery);
        // }

        // const { campaignId } = (await mysqlManager.query(conn, { sql: `select \`campaign_id\` as \`campaignId\` from \`brdn_machine_campaign\` where \`machine_name\` = ?`, values: [host] }))[0];
        // let resultQuery = { sql: ``, values: [] };
        // if (!campaignId)
        // {
        //     resultQuery.sql = `select * from \`smart_vt_test_pool\``;
        //     result = (await mysqlManager.query(conn, resultQuery))[0];
        // }
        // else
        // {
        //     resultQuery.sql = `call \`smart_vt_pool_by_campaign_id\` (?);`;
        //     resultQuery.values.push(campaignId);
        //     result = (await mysqlManager.nonQuery(conn, resultQuery))[0];
        // }

        // if (!typeUtils.isArrayAndValid(result) && !campaignId)
        // {
        //     await mysqlManager.nonQuery(conn, { sql: `call \`smart_vt_clear_old_tests\` ();` });
        //     result = (await mysqlManager.nonQuery(conn, resultQuery))[0];
        // }

        // await mysqlManager.transactionCommit(conn);
        // if (campaignId)
        //     result = result[`0`];

        // if (!typeUtils.isValid(result))
        //     result = {};

        // success(res, result);

        const item = await mysqlManager.query(conn, { sql: `select * from \`vt_test_pool\` order by rand() limit 1` });
        await mysqlManager.transactionCommit(conn);

        success(res, item);

    }
    catch (err)
    {
        await mysqlManager.transactionRollback(conn);
        error(res, err.message, getErrors.UNKNOWN.humanMessage, getErrors.UNKNOWN.code);
    }
};
module.exports.submit = async (req, res) =>
{
    let conn = null;
    try
    {
        conn = await mysqlManager.newConnectionFromPool(true);

        const { result, testItem } = parse(req);
        const availability = treatOutput(result, testItem);

        const updateQuery = { sql: `call \`smart_vt_send_data\` (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);`, values: [availability.address_id, availability.message, availability.closet_distance, availability.lines, availability.until_20, availability.until_20_top_speed, availability.until_20_description, availability.from_21_up_to_50, availability.from_21_up_to_50_top_speed, availability.from_21_up_to_50_description, availability.over_50, availability.over_50_top_speed, availability.over_50_description, availability.network, availability.box_id, availability.closed_id, availability.closet_top_speed, availability.tecnology, availability.tecnology_for_tv] };

        await mysqlManager.nonQuery(conn, updateQuery);
        await mysqlManager.transactionCommit(conn);
        success(res);
    }
    catch (err)
    {
        await mysqlManager.transactionRollback(conn);
        error(res, getErrors.UNKNOWN.humanMessage, getErrors.UNKNOWN.humanMessage, getErrors.UNKNOWN.code);
    }
};
module.exports.invalidate = async (req, res) =>
{
    let conn = null;
    try
    {
        conn = await mysqlManager.newConnectionFromPool(true);
        const { testItem } = parse(req);
        const updateQuery = { sql: `update \`brdn_address\` set \`tested_in\` = now(), \`available_for_testing\` = 0 where \`address_id\` = ?`, values: [testItem.address_id] };
        await mysqlManager.nonQuery(conn, updateQuery);

        await mysqlManager.transactionCommit(conn);
        success(res);
    }
    catch (err)
    {
        await mysqlManager.transactionRollback(conn);
        error(res, getErrors.UNKNOWN.humanMessage, getErrors.UNKNOWN.humanMessage, getErrors.UNKNOWN.code);
    }
};
module.exports.getLoginInfo = async (req, res) =>
{
    let conn = null;
    try
    {
        conn = await mysqlManager.newConnectionFromPool(true);
        const loginInfoQuery = { sql: `select \`url\`, \`login\`, \`password\` from \`brdn_macros_info\` where \`name\` = ?`, values: [`smartWebVt`] }
        const loginInfoResult = await mysqlManager.query(conn, loginInfoQuery);
        if (!loginInfoResult)
        {
            loginInfo = null;
            error(res, `Login: macro info failed...`, `no login info found`);
        }
        else
        {
            loginInfo = loginInfoResult[0];
            success(res, loginInfo);
        }
    }
    catch (err)
    {
        error(res, getErrors.UNKNOWN.humanMessage, getErrors.UNKNOWN.humanMessage, getErrors.UNKNOWN.code);
    }
};

/**
 * handles the result and returns an AvailabilityModel
 *
 * @param {string} message the message gotten
 * @param {string} testItem the item to be tested
 * @returns {AvailabilityModel} a new, and parsed, AvailabilityModel
 */
const treatOutput = (message, testItem) =>
{
    message = message.replace(/-/g, ``)
    message = message.replace(/\./g, ``)
    let values = message.split(`\n`);
    values = values.map(i =>
    {
        let newValue = i.trim();
        if (newValue.endsWith(`;`))
            newValue = newValue.substring(0, newValue.length - 1);

        return newValue.trim();
    });

    const over_50 = extractValue(values, `De 51 a 1 Gbps`);
    const until_20 = extractValue(values, `Até 20Mbps`);
    const from_21_up_to_50 = extractValue(values, `De 21 a 50Mbps`);

    return new AvailabilityModel(testItem.address_id, values[1], extractValue(values, `Distância do armário`), extractValue(values, `Quantidade`), until_20[0], until_20[1], until_20[2], from_21_up_to_50[0], from_21_up_to_50[1], from_21_up_to_50[2], over_50[0], over_50[1], over_50[2], extractValue(values, `Tipo armário`), extractValue(values, `Caixa`), extractValue(values, `Armário`), extractValue(values, `Velocidade Máxima do armário`), extractValue(values, `Tecnologia do Acesso`), extractValue(values, `Tecnologia de TV`));
};
const extractValue = (values, label) =>
{
    let line = values.find(i => i.startsWith(label));
    if (line)
    {
        line = line.substring(line.indexOf(`:`) + 1);
        line = line.split(`;`).map(i => i.trim());

        if (line.length > 1)
            return line;
        else
            return line[0];
    }

    throw new Error(`Extract value: '${label}' not found on return message`);
};