const { Request, parse } = require('clericuzzi-javascript-express/src/managers/request.manager');
const { Response, error, success } = require('clericuzzi-javascript-express/src/managers/response.manager');

const mysqlManager = require('clericuzzi-javascript-mysql/dist/business/managers/mysql.manager');

/**
 * gets the next company to be checked on the simplify platform
 * if a company id is provided, that is the company that will be checked
 * 
 * @param {Request} req the request input data
 * @param {Response} res the request input data
 *
 * @returns {SimplifiqueTargetModel} the current target to be checked
 */
module.exports.fetchUserList = async (req, res) =>
{
    try
    {
        const queryOptions = { sql: `select \`user_email\` as \`user\` from \`brdn_user\` order by 1` };
        const result = await mysqlManager.query(await mysqlManager.newConnectionFromPool(), queryOptions);

        success(res, result);
    }
    catch (err)
    {
        error(res, err.message || err.sqlMessage, `could not get the user list`, -4);
    }
};