const dateUtils = require('clericuzzi-javascript/dist/utils/date.utils');
const typeUtils = require('clericuzzi-javascript/dist/utils/type.utils');
const arrayUtils = require('clericuzzi-javascript/dist/utils/array.utils');
const mysqlManager = require('clericuzzi-javascript-mysql/dist/business/managers/mysql.manager');

const { Request, parse } = require('clericuzzi-javascript-express/src/managers/request.manager');
const { Response, error, success } = require('clericuzzi-javascript-express/src/managers/response.manager');

const getErrors = {
    UNKNOWN: { code: -78623546, humanMessage: `Erro inesperado` },
    NO_TEST_LEFT: { code: -78623547, humanMessage: `Não há mais telefones válidos a serem testados` },
    NO_TESTED_INFO: { code: -78623548, humanMessage: `A informação de contatos testados não foi enviada`, devMessage: `testedIds not found...` },
}

module.exports.fetch = async (req, res) =>
{
    let conn = null;
    try
    {
        conn = await mysqlManager.newConnectionFromPool(false);
        const queryOptions = { sql: 'select `contact_id` as `id`, `contact_phone1` as `phone` from `brdn_contact` where `contact_phone1` is not null and `tested_abr` = 0 order by rand() limit 100;' };

        const result = await mysqlManager.query(conn, queryOptions);
        if (typeUtils.isArray(result))
        {
            if (typeUtils.isArrayAndValid(result))
                success(res, result);
            else
                error(res, getErrors.NO_TEST_LEFT.humanMessage, getErrors.NO_TEST_LEFT.humanMessage, getErrors.NO_TEST_LEFT.code);
        }
        else
            error(res, getErrors.UNKNOWN.humanMessage, getErrors.UNKNOWN.humanMessage, getErrors.UNKNOWN.code);
    }
    catch (err)
    {
        error(res, getErrors.UNKNOWN, error);
    }
    finally
    {
        await mysqlManager.transactionCommit(conn);
    }
};
module.exports.submit = async (req, res) =>
{
    let conn = null;
    try
    {
        conn = await mysqlManager.newConnectionFromPool(true);
        const payload = parse(req);

        if (!payload.testedIds)
            error(res, getErrors.NO_TESTED_INFO);

        await manageTests(payload.testedIds, conn);
        await manageCompanies(payload.companies, conn);
        await managePhoneInfo(payload.phones, conn);

        await mysqlManager.transactionCommit(conn);
        success(res);
    }
    catch (err)
    {
        await mysqlManager.transactionRollback(conn);
        error(res, getErrors.UNKNOWN.humanMessage, err.message, getErrors.UNKNOWN.code);
    }
};

const clearPhonesInfo = async (phones, conn) =>
{
    const phoneIds = phones.map(i => i.id).filter(i => typeUtils.isValid(i));
    const query = { sql: `delete from \`brdn_operator_contact\` where \`contact_id\` in (${phoneIds.join(`,`)});` };
    await mysqlManager.nonQuery(conn, query);
};
const updatePhonesInfo = async conn =>
{
    const query = { sql: `update \`brdn_contact\` \`c\` set \`c\`.\`current_operator\` = (select \`o\`.\`name\` from \`brdn_operator_contact\` \`oc\` join \`brdn_operator\` \`o\` on \`o\`.\`id\` = \`oc\`.\`operator_id\` where \`oc\`.\`contact_id\` = \`c\`.\`contact_id\` order by \`oc\`.\`id\` asc limit 1);` };
    await mysqlManager.nonQuery(conn, query);
};
const registerPhonesInfo = async (phones, companyIdMap, conn) =>
{
    let insertParams = [];
    for (let phone of phones)
        for (let history of phone.history)
            if (typeUtils.isValid(phone.id))
                insertParams.push(`(null, ${companyIdMap.get(history.company)}, ${phone.id}, ${!history.until ? `null` : `'${history.until}'`})`);

    const insertCommand = `insert into \`brdn_operator_contact\` values ${insertParams.join(`,`)}`;
    await mysqlManager.nonQuery(conn, { sql: insertCommand });
};

const getCompanies = async (conn) =>
{
    const queryOptions = { sql: 'select `name` from `brdn_operator`' };

    const existingCompanies = await mysqlManager.query(conn, queryOptions);
    if (existingCompanies.length > 0)
        return existingCompanies.map(i => i.name);
    else
        return [];
};
const getCompaniesIdMap = async (conn) =>
{
    const queryOptions = { sql: 'select `id`, `name` from `brdn_operator`' };
    const existingCompanies = await mysqlManager.query(conn, queryOptions);
    if (existingCompanies.length > 0)
    {
        const mapper = new Map();
        for (let existingCompany of existingCompanies)
            mapper.set(existingCompany.name, existingCompany.id);

        return mapper;
    }
    else
        return new Map();
};

const manageTests = async (testedIds, conn) =>
{
    await mysqlManager.nonQuery(conn, { sql: `update \`brdn_contact\` set \`tested_abr\` = 1 where contact_id in (${testedIds.join(`,`)});` });
};
const managePhoneInfo = async (phones, conn) =>
{
    const companyIdMap = await getCompaniesIdMap(conn);
    await clearPhonesInfo(phones, conn);
    await updatePhonesInfo(conn);
    await registerPhonesInfo(phones, companyIdMap, conn);
};
const manageCompanies = async (companies, conn) =>
{
    const newCompanies = [];
    const existingCompanies = await getCompanies(conn);
    for (let company of companies)
        if (!existingCompanies.includes(company))
            newCompanies.push(company);

    if (newCompanies.length > 0)
    {
        const insertCommandValues = [];
        for (let newCompany of newCompanies)
            insertCommandValues.push(`(null, '${newCompany}')`);

        const insertCommand = `insert into \`brdn_operator\` values ${insertCommandValues.join(`,`)};`;
        const insertOptions = { sql: insertCommand };
        await mysqlManager.nonQuery(conn, insertOptions);
    }
};