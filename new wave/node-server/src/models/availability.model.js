module.exports = class AvailabilityModel
{
    constructor(address_id, message, closet_distance, lines, until_20, until_20_top_speed, until_20_description, from_21_up_to_50, from_21_up_to_50_top_speed, from_21_up_to_50_description, over_50, over_50_top_speed, over_50_description, network, box_id, closed_id, closet_top_speed, tecnology, tecnology_for_tv)
    {
        this.address_id = address_id || null;
        this.message = message || null;
        this.closet_distance = closet_distance || null;
        this.lines = lines || null;
        this.until_20 = until_20 || null;
        this.until_20_top_speed = until_20_top_speed || null;
        this.until_20_description = until_20_description || null;
        this.from_21_up_to_50 = from_21_up_to_50 || null;
        this.from_21_up_to_50_top_speed = from_21_up_to_50_top_speed || null;
        this.from_21_up_to_50_description = from_21_up_to_50_description || null;
        this.over_50 = over_50 || null;
        this.over_50_top_speed = over_50_top_speed || null;
        this.over_50_description = over_50_description || null;
        this.network = network || null;
        this.box_id = box_id || null;
        this.closed_id = closed_id || null;
        this.closet_top_speed = closet_top_speed || null;
        this.tecnology = tecnology || null;
        this.tecnology_for_tv = tecnology_for_tv || null;
    }
}