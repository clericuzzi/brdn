const mysqlManager = require('clericuzzi-javascript-mysql/dist/business/managers/mysql.manager');
if (require(`os`).hostname() === `clericuzzi-pc`)
    mysqlManager.configConnection(`brdn.dyndns.org`, `brdn_teste`, `teste`, `teste@123`, `3308`, 120000, 60000, 35);
else
    mysqlManager.configConnection(`192.168.0.71`, `brdn_teste`, `teste`, `teste@123`, `3306`, 120000, 60000, 35);

const AppManager = require('clericuzzi-javascript-express/src/managers/app.manager');
const app = new AppManager();

const abr = require('./src/abr.actions');
const smartVt = require('./src/smartVt.actions');
const simplifique = require('./src/simplifique.actions');
const simplifiqueExtension = require('./src/simplifiqueExtension.actions');

app.post(`/abr-fetch`, abr.fetch);
app.post(`/abr-submit`, abr.submit);

app.post(`/smartVt-fetch`, smartVt.fetch);
app.post(`/smartVt-submit`, smartVt.submit);
app.post(`/smartVt-invalidate`, smartVt.invalidate);
app.post(`/smartVt-getLoginInfo`, smartVt.getLoginInfo);

app.post(`/simplifique-fetch`, simplifique.fetch);
app.post(`/simplifique-submit`, simplifique.submit);

app.get(`/simplifiqueExtension-fetchUserList`, simplifiqueExtension.fetchUserList);

app.listen();