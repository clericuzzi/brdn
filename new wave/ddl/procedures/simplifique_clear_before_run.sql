delimiter ;
drop procedure if exists `simplifique_clear_info_before_run`;
delimiter |
create procedure `simplifique_clear_info_before_run` (companyId int(11))
begin

	delete from `brdn_company_simplifique_info` where `company_id` = companyId;
	delete from `brdn_company_simplifique_info_order` where `company_id` = companyId;
	delete from `brdn_company_simplifique_info_wallet` where `company_id` = companyId;
	delete from `brdn_company_simplifique_info_order_type` where `company_id` = companyId;
	delete from `brdn_company_simplifique_info_portability` where `company_id` = companyId;
	delete from `brdn_company_simplifique_info_unpaid_bill` where `company_id` = companyId;
	delete from `brdn_company_simplifique_info_unpaid_bill_aging` where `company_id` = companyId;
	delete from `brdn_company_simplifique_info_wallet_team_member` where `company_id` = companyId;

end|
delimiter ;