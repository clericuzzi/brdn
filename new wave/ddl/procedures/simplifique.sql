delimiter ;
drop procedure if exists `simplifique_clear_previous_tests`;
delimiter |
create procedure `simplifique_clear_previous_tests` ()
begin

	update `brdn_company` set `simplifique_test_date` = null;

end|
delimiter ;