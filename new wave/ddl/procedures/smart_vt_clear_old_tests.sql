delimiter ;
drop procedure if exists `smart_vt_clear_old_tests`;
delimiter |
create procedure `smart_vt_clear_old_tests` ()
begin

	update `brdn_address` set `tested_in` = null, `available_for_testing` = 1 where datediff(now(), `tested_in`) > 3;
    
end|
delimiter ;