drop view if exists `smart_vt_address_not_found`;
create view `smart_vt_address_not_found`
as
	select 
		`c`.`company_id`,
		`c`.`company_name`,
		`c`.`company_cnpj`,
        `a`.`address_street`,
        `a`.`address_zipcode`,
        `a`.`address_number`
	from
		`brdn_address` `a`
	join `brdn_company` `c` on `c`.`company_id` = `a`.`company_id`
	where
		`available_for_testing` = 0;
        
select * from `smart_vt_address_not_found`;