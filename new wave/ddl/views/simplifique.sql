drop view if exists `simplifique_status`;
create view `simplifique_status`
as
	select
		'empresas a testar (nesta leva)' `stat`,
		count(0) `quantidade`
    from
		`brdn_company_info` `bi`
	join
		`brdn_company` `bc` on `bc`.`company_id` = `bi`.`company_id` and `bc`.`simplifique_test_date` is null
	where (`bi`.`situation` = 'ATIVA' and `bi`.`size` = 'DEMAIS' and `bi`.`status` = 'OK') union all
    
    select '---------', null union all
    
    select 'público alvo', count(0) from `brdn_company_info` `bi` where (`bi`.`situation` = 'ATIVA' and `bi`.`size` = 'DEMAIS' and `bi`.`status` = 'OK') union all
    select '% de empresas', cast(((select count(0) from `brdn_company_info` `bi` where (`bi`.`situation` = 'ATIVA' and `bi`.`size` = 'DEMAIS' and `bi`.`status` = 'OK')) / (select count(0) from `brdn_company`)) * 100  as decimal(10,2)) union all
    
    select '---------', null union all
    
    select 'empresas ja testadas', count(0) from `brdn_company` `bc` where `bc`.`company_id` in (select `company_id` from `brdn_company_simplifique_info`) union all
    select 'empresas nunca testadas', count(0) from `brdn_company` `bc` where `bc`.`company_id` not in (select `company_id` from `brdn_company_simplifique_info`) union all
    
    select '---------', null union all
    
    select 'empresas com informação da receita', count(0) from `brdn_company` `bc` where `bc`.`company_id` in (select `company_id` from `brdn_company_info`) union all
    select 'empresas sem informação da receita', count(0) from `brdn_company` `bc` where `bc`.`company_id` not in (select `company_id` from `brdn_company_info`);

drop view if exists `simplifique_target`;
create view `simplifique_target`
as
	select
		`bc`.`company_id` `id`,
		`bc`.`company_cnpj` `cnpj`
    from
		`brdn_company_info` `bi`
	join
		`brdn_company` `bc` on `bc`.`company_id` = `bi`.`company_id` and `bc`.`simplifique_test_date` is null
	where (`bi`.`situation` = 'ATIVA' and `bi`.`size` = 'DEMAIS' and `bi`.`status` = 'OK')
    order by rand() limit 1;