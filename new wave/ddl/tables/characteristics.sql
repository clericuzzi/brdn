drop table if exists `brdn_characteristic_type`;
drop table if exists `brdn_characteristic`;

create table `brdn_characteristic_type` (
	`id` int(11) unsigned not null auto_increment,
	`name` varchar(120) not null,
    
	`removed` tinyint(1) not null default 0,
    constraint `pk|brdn_characteristic_type` primary key (`id`),
    constraint `uc|brdn_characteristic_type` unique (`name`)
) engine=`innodb` default charset=utf8;

create table `brdn_characteristic` (
	`id` int(11) unsigned not null auto_increment,
	`type_id` int(11) unsigned not null,
	`name` varchar(120) not null,
    
	`removed` tinyint(1) not null default 0,
    constraint `pk|brdn_characteristic` primary key (`id`),
    constraint `pk|brdn_characteristic|brdn_characteristic_type` foreign key (`type_id`) references `brdn_characteristic_type` (`id`),
    constraint `uc|brdn_characteristic` unique (`type_id`, `name`)
) engine=`innodb` default charset=utf8;