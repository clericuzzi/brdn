-- alter table `brdn_address` add column `tested_in` timestamp null after `company_id`;
-- create index `ix|brdn_address|tested_id` on `brdn_address` (`tested_in`);
-- alter table `brdn_address` add column `available_for_testing` tinyint(1) null default 1 after `tested_in`;
-- create index `ix|brdn_address|available_for_testing` on `brdn_address` (`available_for_testing`);

-- alter table`brdn_teste`.`brdn_address` drop index `ix|brdn_address|testing_pool`;
-- create index `ix|brdn_address|testing_pool` on `brdn_address` (`tested_in`, `available_for_testing`);

drop view if exists `smart_vt_status`;
create view `smart_vt_status`
as
	select 'endereços com prooblemas' as `stat`, count(0) as `amount` from `brdn_address` where `available_for_testing` = 0 union all
	select 'endereços testados' as `stat`, count(0) as `amount` from `brdn_address` where `tested_in` is not null union all
	select 'endereços NÃO testados' as `stat`, count(0) as `amount` from `brdn_address` where `tested_in` is null union all
	select 'endereços testados há mais de 3 dias', count(0) from `brdn_address` where datediff(now(), `tested_in`) >= 3 union all
	select 'endereços testados há mais de 10 dias', count(0) from `brdn_address` where datediff(now(), `tested_in`) >= 10 union all
	select 'endereços testados há mais de 15 dias', count(0) from `brdn_address` where datediff(now(), `tested_in`) >= 15;
    
drop view if exists `smart_vt_test_pool`;
create view `smart_vt_test_pool`
as
	select 
		`address_id`,
        `address_number`,
        `address_zipcode`
	from
		`brdn_address`
	where
		`tested_in` is null
	and `available_for_testing` = 1
    order by rand()
    limit 1;
    
drop view if exists `smart_vt_retest_pool`;
create view `smart_vt_retest_pool`
as
	select 
		`address_id`,
        `address_number`,
        `address_zipcode`
	from
		`brdn_address`
	where
		`tested_in` is not null
	and `available_for_testing` = 1
	order by 
		`tested_in` asc
	limit 1;
    
