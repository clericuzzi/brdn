drop table if exists `brdn_campaign_characteristic`;
drop table if exists `brdn_campaign_company`;
drop table if exists `brdn_campaign_user`;
drop table if exists `brdn_campaign`;
drop table if exists `brdn_product`;

create table `brdn_product` (
	`id` int(11) unsigned not null auto_increment,
	`name` varchar(120) not null,
    
	`removed` tinyint(1) not null default 0,
    constraint `pk|brdn_product` primary key (`id`),
    constraint `uc|brdn_product` unique (`name`)
) engine=`innodb` default charset=utf8;

create table `brdn_campaign` (
	`id` int(11) unsigned not null auto_increment,
    
	`product_id` int(11) unsigned not null,
	`name` varchar(120) not null,
    
	`removed` tinyint(1) not null default 0,
    constraint `pk|brdn_campaign` primary key (`id`),
    constraint `pk|brdn_campaign|brdn_product` foreign key (`product_id`) references `brdn_product` (`id`),
    constraint `uc|brdn_campaign` unique (`name`)
) engine=`innodb` default charset=utf8;

create table `brdn_campaign_characteristic` (
	`id` int(11) unsigned not null auto_increment,
	`campaign_id` int(11) unsigned not null,
	`characteristic_id` int(11) unsigned not null,
    
	`has` tinyint(1) unsigned not null default 1,
    
	`removed` tinyint(1) not null default 0,
    constraint `pk|campaign_characteristic` primary key (`id`),
    constraint `pk|campaign_characteristic|campaign` foreign key (`campaign_id`) references `brdn_campaign` (`id`),
    constraint `pk|campaign_characteristic|characteristic` foreign key (`characteristic_id`) references `brdn_characteristic` (`id`),
    constraint `uc|campaign_characteristic` unique (`campaign_id`, `characteristic_id`)
) engine=`innodb` default charset=utf8;

create table `brdn_campaign_company` (
	`id` int(11) unsigned not null auto_increment,
	`campaign_id` int(11) unsigned not null,
	`company_id` int(11) not null,
    
	`removed` tinyint(1) not null default 0,
    constraint `pk|campaign_company` primary key (`id`),
    constraint `pk|campaign_company|campaign` foreign key (`campaign_id`) references `brdn_campaign` (`id`),
    constraint `pk|campaign_company|company` foreign key (`company_id`) references `brdn_company` (`company_id`),
    constraint `uc|campaign_company` unique (`campaign_id`, `company_id`)
) engine=`innodb` default charset=utf8;

create table `brdn_campaign_user` (
	`id` int(11) unsigned not null auto_increment,
	`campaign_id` int(11) unsigned not null,
	`user_id` int(11) not null,
    
	`removed` tinyint(1) not null default 0,
    constraint `pk|campaign_user` primary key (`id`),
    constraint `pk|campaign_user|campaign` foreign key (`campaign_id`) references `brdn_campaign` (`id`),
    constraint `pk|campaign_user|user` foreign key (`user_id`) references `brdn_user` (`user_id`),
    constraint `uc|campaign_user` unique (`campaign_id`, `user_id`)
) engine=`innodb` default charset=utf8;