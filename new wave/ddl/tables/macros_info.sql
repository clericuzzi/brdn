drop table if exists `brdn_macros_info`;
create table `brdn_macros_info` (
	`id` int(11) unsigned not null auto_increment,
	`name` varchar(120) not null,
	`url` varchar(1200) not null,
	`login` varchar(120) not null,
	`password` varchar(120) not null,
    constraint `pk|brdn_macros_info` primary key (`id`),
    constraint `uc|brdn_macros_info` unique (`name`)
) engine=`innodb` default charset=utf8;

insert into `brdn_macros_info` values (null, 'simplifique', 'https://simplifiquevivoemp.com.br/default?ReturnUrl=%2f', '06584479471', 'Vivo@2204');
insert into `brdn_macros_info` values (null, 'smartWebVt', 'https://vivovendas.vivo.com.br/sales_ext/start.swe?SWECmd=GotoView&SWEView=NV+Dealer+Home+Page+View&SWERF=1&SWEHo=vivovendas.vivo.com.br&SWEBU=1&SWEApplet0=NV+Sales+Opportunity+List+Applet+-+Home+Page&SWERowId0=8-5M5ENWDO', '80561279', 'Vivo@2002');