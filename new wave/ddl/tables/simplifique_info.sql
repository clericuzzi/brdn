drop table if exists `brdn_company_simplifique_info_wallet_team_member`;
drop table if exists `brdn_company_simplifique_info_wallet`;
drop table if exists `brdn_company_simplifique_info_unpaid_bill`;
drop table if exists `brdn_company_simplifique_info_unpaid_bill_aging`;
drop table if exists `brdn_company_simplifique_info_portability`;
drop table if exists `brdn_company_simplifique_info_order`;
drop table if exists `brdn_company_simplifique_info_order_type`;
drop table if exists `brdn_company_simplifique_info`;
drop table if exists `brdn_simplifique_captcha_break`;

create table `brdn_simplifique_captcha_break` (
	`id` int(11) unsigned not null auto_increment,
	`user_id` int(11) null,
    
	`timestamp` timestamp not null,
    
	`removed` tinyint(1) not null default 0,
    constraint `pk|simplifique_captcha_break` primary key (`id`),
    constraint `pk|simplifique_captcha_break|brdn_user` foreign key (`user_id`) references `brdn_user` (`user_id`)
) engine=`innodb` default charset=utf8;

create table `brdn_company_simplifique_info` (
	`id` int(11) unsigned not null auto_increment,
	`company_cnpj` varchar(20) not null,
    
	`convergent` tinyint(1) null,
	`convergency_message` varchar(160) null,
    
	`orders` smallint null,
	`portabilities` smallint null,
    
	`timestamp` timestamp not null default now(),
	`removed` tinyint(1) not null default 0,
    constraint `pk|simplifique_info` primary key (`id`)
) engine=`innodb` default charset=utf8;

create table `brdn_company_simplifique_info_portability` (
	`id` int(11) unsigned not null auto_increment,
	`company_cnpj` varchar(20) not null,
    
	`phone` varchar(16) null,
	`protocol` varchar(40) null,
	`line_activation_date` date null,
	`window` varchar(40) null,
	`status` varchar(40) null,
	`type` varchar(40) null,
	`operator` varchar(40) null,
    
	`removed` tinyint(1) not null default 0,
    constraint `pk|simplifique_info_portability` primary key (`id`)
) engine=`innodb` default charset=utf8;

create table `brdn_company_simplifique_info_wallet` (
	`id` int(11) unsigned not null auto_increment,
	`company_cnpj` varchar(20) not null,
    
    `name` varchar(20) not null,
    `segment` varchar(20) not null,
    `updated_in` varchar(20) not null,
    
	`removed` tinyint(1) not null default 0,
    constraint `pk|wallet` primary key (`id`)
) engine=`innodb` default charset=utf8;

create table `brdn_company_simplifique_info_wallet_team_member` (
	`id` int(11) unsigned not null auto_increment,
	`wallet_id` int(11) unsigned not null,
    
	`team` varchar(40) null,
	`member` varchar(160) null,
    
	`removed` tinyint(1) not null default 0,
    constraint `pk|wallet_team_member` primary key (`id`),
    constraint `pk|wallet_team_member|wallet` foreign key (`wallet_id`) references `brdn_company_simplifique_info_wallet` (`id`)
) engine=`innodb` default charset=utf8;

create table `brdn_company_simplifique_info_unpaid_bill` (
	`id` int(11) unsigned not null auto_increment,
	`company_cnpj` varchar(20) not null,
    
	`system` varchar(160) null,
	`bill_id` varchar(160) null,
    
	`value` decimal(10,2) null,
    
	`due_date` date null,
	`cut_date` date null,
    
	`removed` tinyint(1) not null default 0,
    constraint `pk|brdn_company_simplifique_info_unpaid_bill` primary key (`id`)
) engine=`innodb` default charset=utf8;

create table `brdn_company_simplifique_info_unpaid_bill_aging` (
	`id` int(11) unsigned not null auto_increment,
	`company_cnpj` varchar(20) not null,
    
	`system` varchar(160) null,
    
    `due_amount` smallint not null default 0,
    `due_value` decimal(10,2) not null default 0,
    
    `days_amount_1_30` smallint not null default 0,
    `days_value_1_30` decimal(10,2) not null default 0,
    
    `days_amount_31_60` smallint not null default 0,
    `days_value_31_60` decimal(10,2) not null default 0,
    
    `days_amount_61_90` smallint not null default 0,
    `days_value_61_90` decimal(10,2) not null default 0,
    
    `days_amount_91_105` smallint not null default 0,
    `days_value_91_105` decimal(10,2) not null default 0,
    
    `days_amount_106_179` smallint not null default 0,
    `days_value_106_179` decimal(10,2) not null default 0,
    
    `days_amount_180_365` smallint not null default 0,
    `days_value_180_365` decimal(10,2) not null default 0,
    
    `years_amount_1_3` smallint not null default 0,
    `years_value_1_3` decimal(10,2) not null default 0,
    
    `years_amount_3_5` smallint not null default 0,
    `years_value_3_5` decimal(10,2) not null default 0,
    
    `years_amount_over_5` smallint not null default 0,
    `years_value_over_5` decimal(10,2) not null default 0,
    
    `total_amount` smallint not null default 0,
    `total_value` decimal(10,2) not null default 0,
    
    `over_30_amount` smallint not null default 0,
    `over_30_value` decimal(10,2) not null default 0,
    
	`removed` tinyint(1) not null default 0,
    constraint `pk|aging` primary key (`id`)
) engine=`innodb` default charset=utf8;

create table `brdn_company_simplifique_info_order_type` (
	`id` int(11) unsigned not null auto_increment,
	`name` int(11) not null,
    
	`removed` tinyint(1) not null default 0,
    constraint `pk|order_type` primary key (`id`),
    constraint `pk|order_type` unique(`name`)
) engine=`innodb` default charset=utf8;

create table `brdn_company_simplifique_info_order` (
	`id` int(11) unsigned not null auto_increment,
	`order_type_id` int(11) unsigned not null,
	`company_cnpj` varchar(20) not null,
    
	`last_update` date null,
    
	`order_id` varchar(160) null,
	`product` varchar(160) null,
	`tecnology` varchar(160) null,
	
    `order_date` date null,
	`contract_date` date null,
    
	`status_sale` varchar(160) null,
	`status_order` varchar(160) null,
	`status_modem` varchar(160) null,
	`status_router` varchar(160) null,
	`status_pendency` varchar(160) null,
    
	`removed` tinyint(1) not null default 0,
    constraint `pk|simplifique_info_order` primary key (`id`),
    constraint `pk|simplifique_info_order|order_type` foreign key (`order_type_id`) references `brdn_company_simplifique_info_order_type` (`id`)
) engine=`innodb` default charset=utf8;