delimiter ;
drop function if exists `simplifiqueNextCompanyByUserId`;
delimiter |
-- este procedure serve para identificar novas empresas do balde a serem consultadas pelo simplifique
-- enquanto ainda houverem empresas não alocadas no balde, esta função retorna um novo cnpj e o aloca
-- ao usuário passado
-- o campo `information_collected` serve de controle para que a macro não fique pulando entre vários casos
-- sempre que houver uma empresa alocada a um usuário e este campo não estiver verdadeiro (1) esta função
-- retorna rá o mesmo cnpj a fim de completar a consulta no simplifique
create function `simplifiqueNextCompanyByUserId` (userId int(11))
returns varchar(20)
begin
	
    -- buscamos as campanhas onde ainda existem empresas não alocadas a usuários
	set @campaignId = (select `bcu`.`campaign_id` as `campaignId` from `brdn_campaign_user` `bcu` where `user_id` = userId and (select count(0) from `brdn_campaign_company` `bcc` where `bcc`.`campaign_id` = `bcu`.`campaign_id` and `bcc`.`user_id` is null and `removed` = 0) > 0);
    if @campaignId is not null then
		
        set @companyCnpj = (select `company_cnpj` as `cnpj` from `brdn_campaign_company` where `campaign_id` = @campaignId and `user_id` is null order by rand() limit 1);
		update `brdn_campaign_company` set `user_id` = userId, `information_collected` = 0, `information_collected_in` = null where `campaign_id` = @campaignId and `company_cnpj` = @companyCnpj;
		return @companyCnpj;
		/*
		-- caso exista tal campanha, verificamos se existe alguma empresa que foi alocada para o usuário mas a consulta do simplifique não foi finalizada
		set @incompleteFetch = (select `company_cnpj` as `cnpj` from `brdn_campaign_company` where `campaign_id` = @campaignId and `user_id` = userId and `information_collected` = 0);
        if @incompleteFetch is not null then
			-- se sim, retornamos SEMPRE este cnpj
			return @incompleteFetch;
		else
			-- se não, buscamos uma nova empresa e a locamos ao usuário
            -- lembrando sempre de atribuir ZERO ao campo `information_collected`, a fim de enforçar a consutla do simplifique
			set @companyCnpj = (select `company_cnpj` as `cnpj` from `brdn_campaign_company` where `campaign_id` = @campaignId and `user_id` is null order by rand() limit 1);
			update `brdn_campaign_company` set `user_id` = userId, `information_collected` = 0, `information_collected_in` = null where `campaign_id` = @campaignId and `company_cnpj` = @companyCnpj;
			return @companyCnpj;
		end if;
        */
	else
		-- caos não existam campanhas disponíveis retornamos o erro abaixo
		set @message = (select concat('O usuário com o id \'', userId, '\' não tem nenhuma campanha disponível...'));
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = @message;
	end if;
    
end|
delimiter ;

delimiter ;
drop procedure if exists `simplifiqueSubmitCompany`;
delimiter |
create procedure `simplifiqueSubmitCompany` (companyCnpj varchar(20), userId int(11))
begin
	
    update `brdn_campaign_company` set `information_collected` = 1, `information_collected_in` = now() where `company_cnpj` = companyCnpj and `user_id` = userId;
		
end|
delimiter ;

