export default {
    userList: `userList`,
    currentUser: `currentUser`,
    selectedUser: `selectedUser`,
    currentPassword: `currentPassword`,
}