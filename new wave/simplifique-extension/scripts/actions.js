const storageConstants = {
    userList: `userList`,
    currentUser: `currentUser`,
    selectedUser: `selectedUser`,
    currentPassword: `currentPassword`,
};

async function pageStarted()
{
    function closeWindow()
    {
        window.close();
    };
    function loginAction(login, password)
    {
        if (!login)
        {
            alert(`Preencha o login`);
            return;
        }
        else if (!password)
        {
            alert(`Preencha a senha`);
            return;
        }
    };

    function loginChanged(e1, e2, e3, e4)
    {
        // console.log(`loginChanged newValue:`, e1, e2, e3, e4);
        // chrome.storage.sync.set({ loginInput: event.target.value });
    };
    function passwordChanged(event)
    {
        chrome.storage.sync.set({ passwordInput: event.target.value });
    };

    function appendItems(loginInput, items)
    {
        for (let item of items)
        {
            const option = document.createElement(`option`);
            option.value = item.user;
            option.innerHTML = item.user;
            loginInput.appendChild(option);
        }
    };
    async function getUsers(storageData, loginInput)
    {
        console.log(`chrome.storage.sync.get ${storageConstants.userList}`, storageData);
        if (storageData.userList)
            appendItems(loginInput, storageData.userList);
        else
        {
            const fetchParams = { method: `GET` };
            const result = await fetch(`http://localhost:5123/simplifiqueExtension-fetchUserList`, fetchParams);
            if (result)
            {
                const { data } = await result.json();
                chrome.storage.sync.set({ userList: data });
                appendItems(loginInput, data);
            }
        }
    };


    const close = document.getElementById(`close`);
    if (close)
        close.addEventListener(`click`, closeWindow);
    const loginInput = document.getElementById(`loginInput`);
    if (loginInput)
        loginInput.addEventListener(`input`, loginChanged);
    const passwordInput = document.getElementById(`passwordInput`);
    if (passwordInput)
        passwordInput.addEventListener(`input`, passwordChanged);

    const loginButton = document.getElementById(`loginButton`);
    if (loginButton)
        loginButton.addEventListener(`click`, () => loginAction(loginInput.value, passwordInput.value));

    chrome.storage.sync.get([passwordInput], result => passwordInput.value = result);
    chrome.storage.sync.get([storageConstants.userList], async storageData => await getUsers(storageData, loginInput));
};

document.addEventListener('DOMContentLoaded', pageStarted);