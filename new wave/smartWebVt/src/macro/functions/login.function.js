const ChromeDriver = require('clericuzzi-javascript-selenium/src/chrome/seleniumChromeDriver');

const paths = require('../../constants/paths.constants');
const values = require('../../constants/values.constant');

const httpManager = require('clericuzzi-javascript-http/dist/business/managers/http.manager');
let loginInfo = null;

const getLoginInfo = async () =>
{
    const loginInfoQuery = { sql: `select \`url\`, \`login\`, \`password\` from \`brdn_macros_info\` where \`name\` = ?`, values: [paths.macroName] }
    const loginInfoResult = await httpManager.post({}, values.endpointgetLoginInfo);
    if (!loginInfoResult || !loginInfoResult.success)
    {
        loginInfo = null;
        throw new Error(`Login: macro info failed...`);
    }
    else
        loginInfo = loginInfoResult.data;
};

/**
 * loga no painel da vivo
 *
 * @param {ChromeDriver} driver
 * @param {*} headless
 */
module.exports.login = async (driver, headless) =>
{
    try
    {
        await getLoginInfo();
        console.log(`login info gotten`);
        await driver.navigate(loginInfo.url);
        console.log(`navigated to login page`);

        await driver.wait(paths.loginScreenLogin, 15000);
        await driver.fill(paths.loginScreenLogin, loginInfo.login);
        await driver.sleep(750);
        await driver.fill(paths.loginScreenPassword, loginInfo.password);
        await driver.sleep(750);

        let targetValue = Number.parseInt(await driver.getText(paths.loginScreenVerificationTargetValue));
        let currentValue = Number.parseInt(await driver.getText(paths.loginScreenVerificationCurrentValue));
        let clickPosition = 0;

        const scrollRect = await driver.getRect(paths.loginScreenVerificationScroll);
        while (targetValue !== currentValue)
        {
            if (clickPosition === 0)
                clickPosition = Math.round((scrollRect.x + (scrollRect.width * (targetValue / 100))));
            else
            {
                if (targetValue > currentValue)
                    clickPosition++;
                else
                    clickPosition--;
            }

            const clickSettings = { x: clickPosition, y: Math.round(scrollRect.y + 2) };
            await driver.moveAndClick(clickSettings);

            targetValue = Number.parseInt(await driver.getText(paths.loginScreenVerificationTargetValue));
            currentValue = Number.parseInt(await driver.getText(paths.loginScreenVerificationCurrentValue));
            await driver.sleep(450);
        }
        console.log(`login button clicked`);

        await driver.click(paths.loginScreenActionButton);
        await driver.wait(paths.postLoginValidateIcon, 85000);

        console.log(`waiting for validation icon`);
        await driver.click(paths.postLoginValidateIcon);
        await driver.wait(paths.verificationScreenZip, 85000);
    }
    catch (err)
    {
        throw err;
    }
};

module.exports.validateLogin = async (driver) =>
{
    return !driver.wait(paths.loginScreenValidation, 8000, false);
};