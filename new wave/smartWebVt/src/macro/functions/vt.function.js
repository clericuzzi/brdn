const os = require('os');

const values = require('../../constants/values.constant');

const typeUtils = require('clericuzzi-javascript/dist/utils/type.utils');
const stringUtils = require('clericuzzi-javascript/dist/utils/string.utils');
const httpManager = require('clericuzzi-javascript-http/dist/business/managers/http.manager');
httpManager.setTimeoutDuration(60000);

const AvailabilityModel = require('../models/availability.model');

const paths = require('../../constants/paths.constants');
let conn = null;
let totalTested = 0;
let totalInvalid = 0;
module.exports.vt = async (driver, headless) =>
{
    let testItem = null;
    while (true)
    {
        try
        {
            const endpoint = values.endpointFetch;

            await driver.waitTrueEvaluation(canContinue, 65000, 750, false, null, driver);
            testItem = await httpManager.post({ host: os.hostname() }, endpoint);
            if (!testItem)
                break;
            testItem = testItem.data[0];

            console.log(`test item gotten`, JSON.stringify(testItem));
            if (await validateTestItem(testItem))
            {
                console.log(`validation successfull`);
                await fillInfo(driver, testItem, headless);
            }
        }
        catch (err)
        {
            if (testItem)
                await invalidateTestItem(testItem);
            if (driver)
                await driver.waitTrueEvaluation(canContinue, 65000, 750, false, null, driver);
        }
    }
};

const canContinue = async driver =>
{
    try
    {
        await driver.fill(paths.verificationScreenZip, ``);
        return true;
    }
    catch (err)
    {
        return false;
    }
}

const fillInfo = async (driver, testItem, headless) =>
{
    await driver.wait(paths.verificationScreenZip, 85000);

    await driver.sleep(1250);
    await driver.fill(paths.verificationScreenZip, testItem.address_zipcode);
    await driver.sleep(750);
    await driver.fill(paths.verificationScreenNumber, testItem.address_number);
    await driver.sleep(750);
    await driver.sendEnter(paths.verificationScreenZip);
    await driver.sleep(750);

    await driver.click(paths.verificationScreenButtonLocate);
    await driver.sleep(750);
    await driver.click(paths.verificationScreenButtonCheck);
    await driver.sleep(750);
    console.log(`test information filled`);

    await driver.waitTrueEvaluation(getResult, 15000, 500, true, null, driver);
    const result = await driver.getText(paths.verificationScreenResult);
    console.log(`result gotten`);
    if (typeUtils.isStringAndValid(result))
        await saveResult(testItem, result);
    else
    {
        console.log(`NOT FOUND`);
        await driver.sleep(75000000);
    }

    await driver.fillJs(paths.verificationScreenZip, ``);
    await driver.sleep(750);
    await driver.fillJs(paths.verificationScreenNumber, ``);
};
const getResult = async (driver) =>
{
    let result = await driver.getText(paths.verificationScreenResult);
    return typeUtils.isStringAndValid(result);
};
const saveResult = async (testItem, result) =>
{
    console.log(JSON.stringify({ result, testItem }));
    const returnValue = await httpManager.post({ result, testItem }, values.endpointSubmit);
    if (!returnValue || !returnValue.success)
        console.log(`ERRO`);
    console.log(`${totalTested++}: endereço testado:`, JSON.stringify(testItem), result);
};
const validateTestItem = async testItem =>
{
    testItem.address_number = testItem.address_number.toLocaleUpperCase();
    if (testItem.address_number.indexOf(`SN`) >= 0)
    {
        testItem.address_number = `SN`;
        return true;
    }
    else
    {
        if (!typeUtils.isStringAndValid(stringUtils.digitsOnly(testItem.address_number)))
            return await invalidateTestItem(testItem);
        if (!typeUtils.isStringAndValid(stringUtils.digitsOnly(testItem.address_zipcode)))
            return await invalidateTestItem(testItem);

        testItem.address_number = stringUtils.digitsOnly(testItem.address_number);
        testItem.address_zipcode = stringUtils.digitsOnly(testItem.address_zipcode);
        if (testItem.address_zipcode.length < 8)
            testItem.address_zipcode = `0${testItem.address_zipcode}`;

        return true;
    }
};
const invalidateTestItem = async testItem =>
{
    await httpManager.post({ testItem }, values.endpointInvalidate);

    console.log(`${totalInvalid++}: endereço inválido:`, JSON.stringify(testItem));
    return false;
};