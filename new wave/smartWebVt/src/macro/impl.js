const fileManager = require('clericuzzi-javascript/dist/business/managers/fileSystem.manager');
const processUtils = require('clericuzzi-javascript/dist/utils/process.utils');

const ChromeDriver = require('clericuzzi-javascript-selenium/src/chrome/seleniumChromeDriver')
const { vt } = require('./functions/vt.function');
const { login } = require('./functions/login.function');
module.exports = class Impl
{
    constructor(headless)
    {
        this.driver = null;
    }
    async run(headless)
    {
        while (true)
        {
            try
            {
                this.driver = new ChromeDriver(fileManager.currentPath(), headless);

                await login(this.driver, headless);
                await vt(this.driver, headless);
            }
            catch (err) 
            {
                await processUtils.sleep(1500);
                console.log(`\n\nOUTSIDE ERROR`, err, `\n\n`);
            }
            finally
            {
                this.kill();
                await processUtils.sleep(750);
            }
        }
    }
    kill()
    {
        try
        {
            this.driver.kill();
        }
        catch (er)
        {
            console.log(`\n\nERROR ON KILL\n`, er, `\n\n`);
        }
    }
}