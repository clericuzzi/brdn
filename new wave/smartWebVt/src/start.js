const Impl = require('./macro/impl');

module.exports = class Start
{
    async run(instances, headless)
    {
        const macros = [];
        for (let i = 0; i < instances; i++)
            macros.push(new Impl().run(headless));

        await Promise.all(macros);
    }
}