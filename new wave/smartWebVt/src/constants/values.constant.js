const os = require('os');
const hostName = os.hostname();

module.exports = {
    endpointFetch: `http://brdn.dyndns.org:5123/smartVt-fetch`,
    endpointSubmit: `http://brdn.dyndns.org:5123/smartVt-submit`,
    endpointInvalidate: `http://brdn.dyndns.org:5123/smartVt-invalidate`,
    endpointgetLoginInfo: `http://brdn.dyndns.org:5123/smartVt-getLoginInfo`,

    // endpointFetch: hostName !== `clericuzzi-pc` ? `http://localhost:5123/smartVt-fetch` : `http://brdn.dyndns.org:5123/smartVt-fetch`,
    // endpointSubmit: hostName !== `clericuzzi-pc` ? `http://localhost:5123/smartVt-submit` : `http://brdn.dyndns.org:5123/smartVt-submit`,
    // endpointInvalidate: hostName !== `clericuzzi-pc` ? `http://localhost:5123/smartVt-invalidate` : `http://brdn.dyndns.org:5123/smartVt-invalidate`,
    // endpointgetLoginInfo: hostName !== `clericuzzi-pc` ? `http://localhost:5123/smartVt-getLoginInfo` : `http://brdn.dyndns.org:5123/smartVt-getLoginInfo`,
}