module.exports = {
    macroName: `smartWebVt`,

    loginScreenLogin: `/html/body/div[1]/div[2]/div/center/table/tbody/tr[2]/td/div/form/div[1]/div/div/input`,
    loginScreenPassword: `/html/body/div[1]/div[2]/div/center/table/tbody/tr[2]/td/div/form/div[2]/div/div/input`,
    loginScreenValidation: `/html/body/div/h1`,
    loginScreenActionButton: `/html/body/div[1]/div[2]/div/center/table/tbody/tr[2]/td/div/form/div[4]/div/input`,
    loginScreenVerificationScroll: `/html/body/div[1]/div[2]/div/center/table/tbody/tr[2]/td/div/form/div[3]/div/div`,
    loginScreenVerificationTargetValue: `/html/body/div[1]/div[2]/div/center/table/tbody/tr[2]/td/div/form/span/center[2]`,
    loginScreenVerificationCurrentValue: `/html/body/div[1]/div[2]/div/center/table/tbody/tr[2]/td/div/form/div[3]/input`,

    verificationScreenZip: `/html/body/div[1]/div[1]/div[7]/div/div[6]/div/div[1]/div/div[2]/div[1]/div/div/div/form/div/span/div[3]/div[2]/div[2]/input`,
    verificationScreenNumber: `/html/body/div[1]/div[1]/div[7]/div/div[6]/div/div[1]/div/div[2]/div[1]/div/div/div/form/div/span/div[3]/div[3]/div[2]/input`,
    verificationScreenResult: `/html/body/div[1]/div[1]/div[7]/div/div[6]/div/div[1]/div/div[2]/div[2]/div/div/div/form/div/span/div[3]/div[2]/div[3]/textarea`,
    verificationScreenButtonCheck: `/html/body/div[1]/div[1]/div[7]/div/div[6]/div/div[1]/div/div[3]/div/div/div/form/span/div/div[2]/div[1]/button[3]/span`,
    verificationScreenButtonLocate: `/html/body/div[1]/div[1]/div[7]/div/div[6]/div/div[1]/div/div[2]/div[1]/div/div/div/form/div/span/div[3]/div[15]/div/button/span`,

    postLoginValidateIcon: `/html/body/div[1]/div/div[3]/div/div[1]/div[3]/ul/li[2]/a/span`
};