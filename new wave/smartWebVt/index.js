const params = process.argv.slice(2);
const Start = require('./src/start');

const start = new Start();

let headless = false;
let instances = 1;
if (params.length == 2)
{
    instances = params[0];
    headless = params[1] === `1`;
}
else if (params.length == 1)
    instances = params[0];

(async function action()
{
    try
    {
        await start.run(Number.parseInt(instances), headless);
    }
    catch (err)
    {
        console.log(`\n\nOUTSIDE ERROR\n`, err, `\n\n`);
    }
})();