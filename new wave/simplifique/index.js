const Start = require('./src/start');
const stringUtils = require('clericuzzi-javascript/dist/utils/string.utils');
const processUtils = require('clericuzzi-javascript/dist/utils/process.utils');

let cnpj = null;
let userId = null;
if (process.argv.length === 3)
    userId = Number.parseInt(stringUtils.digitsOnly(process.argv[2]) || 0);

if (process.argv.length === 4)
    cnpj = stringUtils.digitsOnly(process.argv[3]);

userId = 2;
const run = async () =>
{
    if (!userId)
    {
        console.log(`User Id não pode ser vazio`);
        await processUtils.sleep(5000);
        console.log(`Encerrando...`);
    }
    else
        while (true)
        {
            try
            {
                const start = new Start();
                await start.run(userId, cnpj);
            }
            catch (err)
            {
                console.log(err);
            }
        }
}

(async function init()
{
    await run();
})();