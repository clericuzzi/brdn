const values = require('../../constants/values.constants');
const httpManager = require('clericuzzi-javascript-http/business/managers/http.manager');

const UserDataModel = require('brdn-node-server-entities/src/models/userData.model');

/**
 * fetches the next target
 *
 * @param {number} userId the caller's id
 * @param {string} companyCnpj the desired comany's cnpj (optional)
 * @returns {string} the search's target
 */
module.exports.fetch = async (userId, companyId) =>
{
    try
    {
        const { data } = await httpManager.post({ userId, companyId }, values.endpointFetch);
        console.log(`alvo atual:`, data);
        return data;
    }
    catch (err)
    {
        console.log(`\n\ngetNext error:\n`, err);

        return null;
    }
}
/**
 * sends the collected data over to the server
 *
 * @param {UserDataModel} userData the collected data
 * @param {number} userId the coller's id
 * @param {string} cnpj the company's cnpj
 */
module.exports.submit = async (userData, userId, cnpj) =>
{
    try
    {
        await httpManager.post({ userData, userId, cnpj }, values.endpointSubmit);

        console.log(`informações da empresa '${cnpj}' enviadas ao servidor`)
    }
    catch (err)
    {
        console.log(`\n\ngetNext error:\n`, err);
    }
}