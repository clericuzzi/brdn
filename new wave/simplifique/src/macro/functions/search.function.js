const BillingBill = require('brdn-node-server-entities/src/models/billingBill.model');
const BillingAging = require('brdn-node-server-entities/src/models/billingAging.model');
const UserDataModel = require('brdn-node-server-entities/src/models/userData.model');
const TeamMemberModel = require('brdn-node-server-entities/src/models/teamMember.model');

const mysqlManager = require('clericuzzi-javascript-mysql/dist/business/managers/mysql.manager');

const ChromeDriver = require('clericuzzi-javascript-selenium/src/chrome/seleniumChromeDriver');
const captchaSolver = require('clericuzzi-javascript-selenium/src/2captcha/captchaSolver');

const { login } = require('./login.function');
const database = require('./database.function');

const values = require('../../constants/values.constants');
const xpaths = require('../../constants/xpaths.constants');

const stringUtils = require('clericuzzi-javascript/dist/utils/string.utils');

/**
 * implements the search
 *
 * @param {ChromeDriver} driver the web driver
 * @param {number} userId the caller's id
 * @param {string} cnpj the desired comany's cnpj (optional)
 */
module.exports.search = async (driver, userId, cnpj) =>
{
    while (true)
    {
        try
        {
            const shouldNavigate = !await driver.exists(xpaths.searchConfirmedComponent);
            if (shouldNavigate)
                await driver.navigate(values.searchUrl);

            if (!await driver.waitUntilExists(xpaths.searchConfirmedComponent, 5000, false))
                await login(driver);

            if (!await driver.exists(xpaths.searchConfirmedComponent))
                await driver.navigate(values.searchUrl);

            const target = await database.fetch(userId, cnpj);
            await searchItem(driver, target.userId, target.cnpj);
            await driver.navigate(values.searchUrl);
        }
        catch (err)
        {
            console.log(`loop failed`, err);
            await driver.navigate(values.searchUrl);
        }
        cnpj = null;
    }
};

/**
 * the actual data collecting search
 *
 * @param {ChromeDriver} driver the web driver
 * @param {number} userId the caller's id
 * @param {string} cnpj the desired comany's cnpj (optional)
 */
const searchItem = async (driver, userId, cnpj) =>
{
    let userData = new UserDataModel();
    try
    {
        await driver.waitUntilExists(xpaths.searchScreenSearchInput, 15000);
        await driver.fillJs(xpaths.searchScreenSearchInput, cnpj);
        // await driver.fillJs(xpaths.searchScreenSearchInput, `3269844000150`);
        await driver.sendTab(xpaths.searchScreenSearchInput);

        if (await driver.waitTrueEvaluation(isCaptchaShowing, 2500, 750, false, null, driver))
        {
            await driver.click(xpaths.searchCaptchaClose);
            await driver.sleep(1250);
        }
        await driver.click(xpaths.searchScreenSearchButton);

        if (await driver.waitUntilExistsAndIsDisplayed(xpaths.searchIntermitentCaptchaValidation, 5000, false))
            await captchaSolver.recaptchaClick(driver, xpaths.searchScreenRecaptchaFrame, xpaths.searchScreenRecaptchaButton);

        await driver.sleep(2500);
        await driver.waitTrueEvaluation(waitForCaptchaSolved, values.infiniteWait, 750, true, null, driver);

        await driver.waitTrueEvaluation(async () => await waitLoadingStarted(driver), 5000, 750, false);
        await driver.waitTrueEvaluation(async () => await waitLoadingComplete(driver), 60000, 750, false);

        await getOrderData(driver, userData);
        await getPlantData(driver, userData);
        await getCreditData(driver, userData);
        await getWalletData(driver, userData);
        await getContractData(driver, userData);
        await getConvergencyData(driver, userData);
        await getPortabilityData(driver, userData);

        await driver.sleep(values.timeoutSmall);
    }
    catch (err)
    {
        console.log(`error`, err);
    }
    finally
    {
        await database.submit(userData, userId, cnpj);
    }
};

/**
 * waits until the information loading has started
 *
 * @param {ChromeDriver} driver the web driver
 */
const waitLoadingStarted = async (driver) =>
{
    const divs = await driver.elementsByTag(`div`);
    for (let div of divs)
    {
        const divClass = await driver.getAttribute(div, `class`);
        if (divClass === `loadingoverlay`)
            return true;
    }

    return false;
};

/**
 * waits until the information is completely loaded
 *
 * @param {ChromeDriver} driver the web driver
 */
const waitLoadingComplete = async (driver) =>
{
    const divs = await driver.elementsByTag(`div`);
    for (let div of divs)
    {
        const divClass = await driver.getAttribute(div, `class`);
        if (divClass === `loadingoverlay`)
            return false;
    }

    return true;
};

/**
 * waits un til the captcha is not on screen anymore
 *
 * @param {ChromeDriver} driver the web driver
 */
const isCaptchaShowing = async driver =>
{
    const exists = await driver.exists(xpaths.searchCaptchaBackGround);
    const visible = await driver.isDisplayed(xpaths.searchCaptchaBackGround);

    return exists && visible;
};
/**
 * waits un til the captcha is not on screen anymore
 *
 * @param {ChromeDriver} driver the web driver
 */
const waitForCaptchaSolved = async driver =>
{
    const exists = await driver.exists(xpaths.searchCaptchaBackGround);
    const visible = await driver.isDisplayed(xpaths.searchCaptchaBackGround);

    return exists && !visible;
};

/**
 * gets all company's order data
 *
 * @param {ChromeDriver} driver the web driver
 * @param {UserDataModel} userData the acumulated user  data
 */
const getOrderData = async (driver, userData) =>
{
    try
    {
        userData.orders = stringUtils.digitsOnly(await driver.getText(xpaths.orders));
    }
    catch (err)
    {
        console.log(`getOrderData err:`, err);
        userData.appendError(`getOrderData  ${err.message}`);
    }
};
/**
 * gets all company's plant data
 *
 * @param {ChromeDriver} driver the web driver
 * @param {UserDataModel} userData the acumulated user  data
 */
const getPlantData = async (driver, userData) =>
{
    try
    {
    }
    catch (err)
    {
        console.log(`getPlantData err:`, err);
        userData.appendError(`getPlantData  ${err.message}`);
    }
};
/**
 * gets all company's credit data
 *
 * @param {ChromeDriver} driver the web driver
 * @param {UserDataModel} userData the acumulated user  data
 */
const getCreditData = async (driver, userData) =>
{
    try
    {
        await driver.click(xpaths.billingExpand);
        await driver.wait(xpaths.billing);

        userData.billingScore = await driver.getText(xpaths.billingScore);
        userData.billingScoreMessage = await driver.getText(xpaths.billingScoreMessage);
        userData.billingAgingTable = await BillingAging.fromText(await driver.getContent(xpaths.billingAgingTable));

        await driver.wait(xpaths.billingExpandFurther);
        await driver.click(xpaths.billingExpandFurther);
        userData.billingData = await BillingBill.fromText(await driver.getContent(xpaths.billingTable));
    }
    catch (err)
    {
        console.log(`getCreditData err:`, err);
        userData.appendError(`getCreditData  ${err.message}`);
    }
};
/**
 * gets all company's credit billing data
 *
 * @param {ChromeDriver} driver the web driver
 */
const getCreditDataBills = async (driver) =>
{
    const billData = [];
    const content = await driver.getContent(xpaths.billingTable);
    const rows = await driver.getChildrenByTag(xpaths.billingTable, `tr`);
    for (let row of rows)
    {
        const bill = await BillingBill.newBill(driver, row);
        if (bill)
            billData.push(bill);
    }

    return billData;
};

/**
 * gets all company's wallet data
 *
 * @param {ChromeDriver} driver the web driver
 * @param {UserDataModel} userData the acumulated user  data
 */
const getWalletData = async (driver, userData) =>
{
    try
    {
        userData.walletLandTeam = await getWalletDataTeam(driver, await driver.getChildrenByTag(xpaths.walletLandTeam, `tr`));
        userData.walletMobileTeam = await getWalletDataTeam(driver, await driver.getChildrenByTag(xpaths.walletMobileTeam, `tr`));

        userData.walletLandSegment = await driver.getText(xpaths.walletLandSegment);
        userData.walletLandUpdatedIn = await driver.getText(xpaths.walletLandUpdatedIn);

        userData.walletMobileSegment = await driver.getText(xpaths.walletMobileSegment);
        userData.walletMobileUpdatedIn = await driver.getText(xpaths.walletMobileUpdatedIn);
    }
    catch (err)
    {
        console.log(`getWalletData err:`, err);
        userData.appendError(`getWalletData  ${err.message}`);
    }
};

/**
 * retrieves the actual data from the table body
 *
 * @param {ChromeDriver} driver the web driver
 * @param {Array} teamDataRows the walet team's info table data 
 * @returns the resulting team data
 */
const getWalletDataTeam = async (driver, teamDataRows) =>
{
    const teamData = [];
    for (let row of teamDataRows)
    {
        const team = await TeamMemberModel.newTeam(driver, row);
        if (team)
            teamData.push(team);
    }

    return teamData;
};
/**
 * gets all company's contract data
 *
 * @param {ChromeDriver} driver the web driver
 * @param {UserDataModel} userData the acumulated user  data
 */
const getContractData = async (driver, userData) =>
{
    try
    {
        userData.contractInfo = await driver.getText(xpaths.contracts);
    }
    catch (err)
    {
        console.log(`getContractData err:`, err);
        userData.appendError(`getContractData  ${err.message}`);
    }
};
/**
 * gets all company's convergency data
 *
 * @param {ChromeDriver} driver the web driver
 * @param {UserDataModel} userData the acumulated user  data
 */
const getConvergencyData = async (driver, userData) =>
{
    try
    {
        userData.convergency = await driver.getText(xpaths.convergency);
        userData.convergencyMessage = await driver.getText(xpaths.convergencyMessage);
    }
    catch (err)
    {
        console.log(`getConvergencyData err:`, err);
        userData.appendError(`getConvergencyData  ${err.message}`);
    }
};
/**
 * gets all company's portability data
 *
 * @param {ChromeDriver} driver the web driver
 * @param {UserDataModel} userData the acumulated user  data
 */
const getPortabilityData = async (driver, userData) =>
{
    try
    {
        userData.portabilities = await driver.getText(xpaths.portabilities);
    }
    catch (err)
    {
        console.log(`getPortabilityData err:`, err);
        userData.appendError(`getPortabilityData  ${err.message}`);
    }
};