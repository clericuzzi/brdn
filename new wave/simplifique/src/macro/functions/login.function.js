const ChromeDriver = require('clericuzzi-javascript-selenium/src/chrome/seleniumChromeDriver');
const captchaSolver = require('clericuzzi-javascript-selenium/src/2captcha/captchaSolver');

const values = require('../../constants/values.constants');
const xpaths = require('../../constants/xpaths.constants');
const mysqlManager = require('clericuzzi-javascript-mysql/dist/business/managers/mysql.manager');

/**
 * logs in
 *
 * @param {ChromeDriver} driver the web driver
 */
module.exports.login = async driver =>
{
    const loginInfoQuery = { sql: `select \`url\`, \`login\`, \`password\` from \`brdn_macros_info\` where \`name\` = ?`, values: [values.macroName] }
    const loginInfoResult = await mysqlManager.exec(loginInfoQuery);
    if (!loginInfoResult)
        throw new Error(`Login: macro info failed...`);
    else
    {
        const loginInfo = loginInfoResult[0];
        await driver.navigate(loginInfo.url);

        await driver.wait(xpaths.loginScreenInputLogin, 15000);
        await driver.fillJs(xpaths.loginScreenInputLogin, loginInfo.login);
        await driver.sendTab(xpaths.loginScreenInputLogin);
        await driver.sleep(750);
        await driver.fill(xpaths.loginScreenInputPassword, loginInfo.password);

        // await captchaSolver.recaptchaSolve(driver, await driver.url(), xpaths.loginScreenRecaptcha, xpaths.loginScreenRecaptchaResponse, xpaths.loginScreenButtonLogin);
        // await driver.sleepRandom(3000, 7000);

        await captchaSolver.recaptchaClick(driver, xpaths.loginScreenRecaptchaFrame, xpaths.loginScreenRecaptchaButton);

        await driver.waitUntilNotExistsOrNotDisplayed(xpaths.loginScreenInputLogin, 150000);
        await driver.waitUntilExists(xpaths.loginConfirmedComponent, values.infiniteWait, 3000);
    }
};