const fileManager = require('clericuzzi-javascript/dist/business/managers/fileSystem.manager');
const httpManager = require('clericuzzi-javascript-http/business/managers/http.manager');
const captchaSolver = require('clericuzzi-javascript-selenium/src/2captcha/captchaSolver');
httpManager.setTimeoutDuration(35000);
captchaSolver.initSolver(`97df6a5f5be10a4ad46dac3266e79f05`, 8);

const ChromeDriver = require('clericuzzi-javascript-selenium/src/chrome/seleniumChromeDriver');
const { search } = require('./functions/search.function');
module.exports = class Impl
{
    /**
     * runs the simplifique verification
     *
     * @param {number} userId the caller's id
     * @param {string} cnpj the desired comany's cnpj (optional)
     */
    async run(userId, cnpj = null)
    {
        let driver = null;
        try
        {
            driver = new ChromeDriver(fileManager.currentPath());
            await search(driver, userId, cnpj);
        }
        catch (err)
        {
            if (userId)
            {
                console.log(`Run error:`, err);
                if (driver)
                    driver.kill();

                await this.run();
            }
            else
                console.log(`you need to provide an userId`);
        }
    }
}