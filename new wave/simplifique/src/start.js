const Impl = require('./macro/impl');
const mysqlManager = require('clericuzzi-javascript-mysql/dist/business/managers/mysql.manager');
mysqlManager.configConnection(`brdn.dyndns.org`, `brdn_teste`, `teste`, `teste@123`, `3308`, 60000);

module.exports = class Start
{
    /**
     * runs the simplifique verification
     *
     * @param {number} userId the caller's id
     * @param {string} cnpj the desired comany's cnpj (optional)
     */
    async run(userId, cnpj)
    {
        await Promise.all([
            new Impl().run(userId, cnpj),
        ]);
    }
}