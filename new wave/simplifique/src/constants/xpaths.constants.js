module.exports = {
    loginScreenInputLogin: `/html/body/form/div[3]/div[1]/div[1]/div/div[3]/div[2]/div/div[2]/input`,
    loginScreenButtonLogin: `/html/body/form/div[3]/div[1]/div[1]/div/div[3]/div[3]/div[2]/a`,
    loginScreenInputPassword: `/html/body/form/div[3]/div[1]/div[1]/div/div[3]/div[2]/div/div[4]/input`,

    loginScreenRecaptcha: `/html/body/form/div[3]/div[1]/div[1]/div/div[3]/div[2]/div/div[5]/div/div`,
    loginScreenRecaptchaFrame: `/html/body/form/div[3]/div[1]/div[1]/div/div[3]/div[2]/div/div[5]/div/div/div/div/iframe`,
    loginScreenRecaptchaButton: `/html/body/div[2]/div[3]/div[1]/div/div/span/div[1]`,
    loginScreenRecaptchaResponse: `/html/body/form/div[3]/div[1]/div[1]/div/div[3]/div[2]/div/div[5]/div/div/div/textarea`,

    loginConfirmedComponent: `/html/body/form/div[3]/div/div[3]/a`,
    searchConfirmedComponent: `/html/body/form/div[4]/div[1]/div/div[2]/div[1]/input[1]`,

    searchScreenSearchInput: `/html/body/form/div[4]/div[1]/div/div[2]/div[1]/input[1]`,
    searchScreenSearchButton: `/html/body/form/div[4]/div[1]/div/div[2]/div[2]/div/a`,
    searchScreenRecaptchaFrame: `/html/body/form/div[4]/div[6]/div/div/div[2]/div/div/div/div/div/iframe`,
    searchScreenRecaptchaButton: `/html/body/div[2]/div[3]/div[1]/div/div/span/div[1]`,

    searchCaptchaClose: `/html/body/form/div[4]/div[6]/div/div/div[1]/button/span`,
    searchCaptchaBackGround: `/html/body/form/div[4]/div[6]`,
    searchIntermitentCaptchaValidation: `/html/body/form/div[4]/div[6]/div/div/div[3]/a`,

    walletLandTeam: `/html/body/form/div[4]/div[1]/div/div[3]/div/div[4]/div[1]/div[1]/div[2]/div[2]/table/tbody`,
    walletLandSegment: `/html/body/form/div[4]/div[1]/div/div[3]/div/div[4]/div[1]/div[2]/div[2]/div[1]/table/tbody/tr[1]/td[2]`,
    walletLandUpdatedIn: `/html/body/form/div[4]/div[1]/div/div[3]/div/div[4]/div[1]/div[2]/div[2]/div[1]/table/tbody/tr[2]/td[2]`,
    walletMobileTeam: `/html/body/form/div[4]/div[1]/div/div[3]/div/div[4]/div[1]/div[1]/div[2]/div[2]/table/tbody`,
    walletMobileSegment: `/html/body/form/div[4]/div[1]/div/div[3]/div/div[4]/div[1]/div[1]/div[2]/div[1]/table/tbody/tr[1]/td[2]`,
    walletMobileUpdatedIn: `/html/body/form/div[4]/div[1]/div/div[3]/div/div[4]/div[1]/div[1]/div[2]/div[1]/table/tbody/tr[2]/td[2]`,

    convergency: `/html/body/form/div[4]/div[1]/div/div[3]/div/div[5]/span[2]`,
    convergencyMessage: `/html/body/form/div[4]/div[1]/div/div[3]/div/div[6]/div[1]`,

    billingTable: `/html/body/form/div[4]/div[1]/div/div[3]/div/div[10]/div[1]/div[2]/div[2]/div[1]/table/tbody`,
    billingAgingTable: `/html/body/form/div[4]/div[1]/div/div[3]/div/div[10]/div[1]/div[1]/div/table/tbody`,

    billingScore: `/html/body/form/div[4]/div[1]/div/div[3]/div/div[10]/div[1]/div[1]/table/thead/tr/th`,
    billingScoreMessage: `/html/body/form/div[4]/div[1]/div/div[3]/div/div[10]/div[1]/div[1]/table/tbody/tr/td`,
    billingExpand: `/html/body/form/div[4]/div[1]/div/div[3]/div/div[10]/div[2]`,
    billingExpandFurther: `/html/body/form/div[4]/div[1]/div/div[3]/div/div[10]/div[1]/div[2]/div[2]/div[2]`,

    portabilities: `/html/body/form/div[4]/div[1]/div/div[3]/div/div[11]/span[2]`,
    portabilitiesInfo: `/html/body/form/div[4]/div[1]/div/div[3]/div/div[12]/div[1]/table/tbody`,
    portabilitiesExpand: `/html/body/form/div[4]/div[1]/div/div[3]/div/div[12]/div[2]`,

    orders: `/html/body/form/div[4]/div[1]/div/div[3]/div/div[13]/span[2]`,

    contracts: `/html/body/form/div[4]/div[1]/div/div[3]/div/div[16]/div[2]`,
};