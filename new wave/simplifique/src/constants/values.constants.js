module.exports = {
    macroName: `simplifique`,
    searchUrl: `https://simplifiquevivoemp.com.br/360-cliente/consulta-cliente`,

    endpointFetch: `http://brdn.dyndns.org:5123/simplifique-fetch`,
    endpointSubmit: `http://brdn.dyndns.org:5123/simplifique-submit`,

    infiniteWait: 6500000,

    timeoutSmall: 2500,
};