const dateUtils = require('clericuzzi-javascript/dist/utils/date.utils');
const typeUtils = require('clericuzzi-javascript/dist/utils/type.utils');
const stringUtils = require('clericuzzi-javascript/dist/utils/string.utils');
const processUtils = require('clericuzzi-javascript/dist/utils/process.utils');

const mysqlManager = require('clericuzzi-javascript-mysql/dist/business/managers/mysql.manager');

let activitiesCache = [];
module.exports.get = async (i, conn) =>
{
    processUtils.setTimer();
    processUtils.logTimer(i, `Iniciando testes (${conn ? `conexão ativa` : `conexão fechada`})`);
    const queryOptions = { sql: `select \`company_id\`, \`company_cnpj\` from \`brdn_company\` where \`company_id\` not in (select \`company_id\` from \`brdn_company_info\`) and \`company_cnpj\` is not null order by rand() limit 1;` };
    processUtils.logTimer(i, `query definida`);
    try
    {
        const result = await mysqlManager.exec(queryOptions, conn);

        if (typeUtils.isArrayAndValid(result))
        {
            processUtils.logTimer(i, `alvo definido`, result);
            return result[0];
        }
        else
        {
            processUtils.logTimer(i, `INFORMAÇÃO INVÁLIDA!!!!`);
            throw new Error(`Get: no more companies to test...`);
        }
    }
    catch (err)
    {
        throw err;
    }
};

module.exports.store = async (companyId, companyInfo, conn) =>
{
    await manageCompany(companyId, companyInfo, conn);

    await managePartners(companyId, companyInfo, conn);
    await manageActivities(companyId, companyInfo, conn);
    conn.commit();
};

const deleteInfo = async (companyId, conn) =>
{
    let deleteQuery = { sql: `delete from \`brdn_company_info_activity\` where \`company_id\` = ?`, values: [companyId] }
    await mysqlManager.exec(deleteQuery, conn);

    deleteQuery = { sql: `delete from \`brdn_company_info_partner\` where \`company_id\` = ?`, values: [companyId] }
    await mysqlManager.exec(deleteQuery, conn);

    deleteQuery = { sql: `delete from \`brdn_company_info\` where \`company_id\` = ?`, values: [companyId] }
    await mysqlManager.exec(deleteQuery, conn);
};
const insertInfo = async (companyId, companyInfo, conn) =>
{
    let opened = typeUtils.isStringAndValid(companyInfo.abertura) ? dateUtils.dateBrToServer(companyInfo.abertura) : null;
    let situation = typeUtils.isStringAndValid(companyInfo.data_situacao) ? dateUtils.dateBrToServer(companyInfo.data_situacao) : null;
    let lastUpdate = companyInfo.ultima_atualizacao ? companyInfo.ultima_atualizacao.substring(0, 10) : null;
    let situationSpecialDate = typeUtils.isStringAndValid(companyInfo.data_situacao_especial) ? dateUtils.dateBrToServer(companyInfo.data_situacao_especial) : null;

    const insertQuery = { sql: `insert into \`brdn_company_info\` values (null, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`, values: [companyId, companyInfo.email, companyInfo.telefone, lastUpdate, companyInfo.situacao, situation, companyInfo.motivo_situacao, companyInfo.situacao_especial, situationSpecialDate, companyInfo.efr, companyInfo.tipo, companyInfo.porte, companyInfo.status, opened, companyInfo.natureza_juridica, companyInfo.capital_social, companyInfo.logradouro, companyInfo.uf, stringUtils.digitsOnly(companyInfo.cep), companyInfo.municipio, companyInfo.numero, companyInfo.complemento, companyInfo.bairro] };
    const result = await mysqlManager.exec(insertQuery, conn);

    return result.insertId;
};
const manageCompany = async (companyId, companyInfo, conn) =>
{
    await deleteInfo(companyId, conn);
    return await insertInfo(companyId, companyInfo, conn);
};

const managePartner = async (companyId, partner, conn) =>
{
    const query = { sql: `insert into \`brdn_company_info_partner\` values (null, ?, ?, ?)`, values: [companyId, partner.nome, partner.qual] };
    await mysqlManager.exec(query, conn);
};
const managePartners = async (companyId, companyInfo, conn) =>
{
    if (typeUtils.isArrayAndValid(companyInfo.qsa))
        for (let partner of companyInfo.qsa)
            await managePartner(companyId, partner, conn);
};

const getActivities = async  conn =>
{
    const query = { sql: `select * from \`brdn_company_activity\`` };
    activitiesCache = await mysqlManager.exec(query, conn);
};
const manageActivity = async (companyId, activity, type, conn) =>
{
    const existingActivity = activitiesCache.find(i => i.code === activity.code);
    if (existingActivity)
    {
        const query = { sql: `insert into \`brdn_company_info_activity\` values (null, ?, ?, ?)`, values: [type, companyId, existingActivity.id] };
        await mysqlManager.exec(query, conn);
    }
    else
    {
        let insertQuery = { sql: `insert into \`brdn_company_activity\` values (null, ?, ?)`, values: [activity.text, activity.code] };
        const activityInsertResult = await mysqlManager.exec(insertQuery, conn);

        insertQuery = { sql: `insert into \`brdn_company_info_activity\` values (null, ?, ?, ?)`, values: [type, companyId, activityInsertResult.insertId] };
        await mysqlManager.exec(insertQuery, conn);

        activitiesCache.push({ id: activityInsertResult.insertId, name: activity.text, code: activity.code });
    }
}
const manageActivities = async (companyId, companyInfo, conn) =>
{
    if (!typeUtils.isArrayAndValid(activitiesCache))
        await getActivities(conn);

    if (typeUtils.isArrayAndValid(companyInfo.atividade_principal))
        for (let activity of companyInfo.atividade_principal)
            await manageActivity(companyId, activity, 1, conn);

    if (typeUtils.isArrayAndValid(companyInfo.atividades_secundarias))
        for (let activity of companyInfo.atividades_secundarias)
            await manageActivity(companyId, activity, 2, conn);
};