const mysqlManager = require('clericuzzi-javascript-mysql/dist/business/managers/mysql.manager');
const processUtils = require('clericuzzi-javascript/dist/utils/process.utils');
const Impl = require('./impl');

let conn = null;
module.exports = class Start
{
    async run()
    {
        if (!conn)
            conn = await mysqlManager.newConnectionFromPool(false);
        try
        {
            await Promise.all([
                new Impl().run(conn),
            ]);

            // await Promise.race([
            //     new Impl().run(),
            //     new Promise((_, reject) => setTimeout(() => reject(new Error('timeout')), 300000)),
            // ]);
        }
        catch (err)
        {
            console.log(err);
            if (Impl.counter() === Impl.startingValue())
            {
                console.log(`\nNADA FOI CONSULTADO...\n`);
                await processUtils.sleep(30000);
                console.log(`\nReiniciando consultas após 5 minutos...\n`);
                await this.run();
            }
            else
            {
                console.log(`\nAguardando o reinício das consultas...\n`);
                await processUtils.sleep(30000);
                console.log(`\nReiniciando consultas após 2 minutos...\n`);
                await this.run();
            }
        }
    }
}