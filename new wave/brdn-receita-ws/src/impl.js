const Start = require('./start');
const mysqlManager = require('clericuzzi-javascript-mysql/dist/business/managers/mysql.manager');
mysqlManager.configConnection(`192.168.0.71`, `brdn_teste`, `teste`, `teste@123`, `3306`, 60000, 30000);
// mysqlManager.configConnection(`brdn.dyndns.org`, `brdn_teste`, `teste`, `teste@123`, `3308`, 60000, 30000);

const httpActions = require('./http.actions');
const processUtils = require('clericuzzi-javascript/dist/utils/process.utils');
const databaseActions = require('./database.actions');

let _totalDone = 1;
let _startingValue = 1;
module.exports = class
{
    static counter()
    {
        return _totalDone;
    }
    static startingValue()
    {
        return _startingValue;
    }
    async updateCount()
    {
        processUtils.logTimer(_totalDone, `Teste finalizado! ${_totalDone} foram avaliados com sucesso\n`);
        await processUtils.sleep(2500);

        _totalDone++;
    }

    async testing(conn) 
    {
        let result = await databaseActions.get(_totalDone, conn);
        while (result)
        {
            processUtils.logTimer(_totalDone, `buscando informação do alvo`);
            const companyInfo = await httpActions.getInfo(result.company_cnpj);
            processUtils.logTimer(_totalDone, `informação adquirida`);
            await databaseActions.store(result.company_id, companyInfo, conn);
            processUtils.logTimer(_totalDone, `resultado salvo com sucesso, aguardando a nova consulta...`);

            await this.updateCount();
            result = await databaseActions.get(_totalDone, conn);
        }

        console.log(`\nTestes finalizados!`);
        await new Promise(r => setTimeout(r, 15000000));
    };
    async run(conn)
    {
        _startingValue = _totalDone;
        try
        {
            await this.testing(conn);
        }
        catch (err)
        {
            console.log(`Error:`, err);
            console.log(`\nEntre em contato com o suporte...\n`);
            await new Promise(r => setTimeout(r, 15000));

            await this.testing(conn);
        }
    }
}