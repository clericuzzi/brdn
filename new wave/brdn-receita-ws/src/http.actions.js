const httpManager = require('clericuzzi-javascript-http/business/managers/http.manager');

module.exports.getInfo = async cnpj =>
{
    if (!cnpj)
        throw new Error(`HttpAction getInfo: invalid payload, a 'cnpj' is needed`);

    return await httpManager.get(null, `https://www.receitaws.com.br/v1/cnpj/${cnpj}`);
};