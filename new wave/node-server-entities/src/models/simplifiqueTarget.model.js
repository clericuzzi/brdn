module.exports = class SimplifiqueTargetModel
{
    /**
     *Creates an instance of SimplifiqueTargetModel.
     * @param {string} cnpj the company's CNPJ
     * @param {number} userId the caller's id
     */
    constructor(cnpj, userId)
    {
        this.cnpj = cnpj;
        this.userId = userId;
    }
}