const fileManager = require('clericuzzi-javascript/dist/business/managers/fileSystem.manager');
const textFileManager = require('clericuzzi-javascript/dist/business/managers/textFile.manager');
const defaultLines = {
    file: fileManager.currentPathFile(`current.txt`),
    separator: `</tr><tr class="">`
};
module.exports = class BillingBill
{
    /**
     * gets the line content as string
     *
     * @param {string} line
     * @returns {string} the string contained in the given line
     */
    static getLineString(line) 
    {
        line = line.replace(`</td>`, ``);
        line = line.substring(line.lastIndexOf(`>`) + 1);

        return line;
    };
    /**
     * gets the line content as a number
     *
     * @param {string} line
     * @returns {string} the number contained in the given line
     */
    static getLineValue(line) 
    {
        return Number.parseInt(this.getLineString(line));
    };
    /**
     * adds, if possible, the new item to the array
     *
     * @static
     * @param {BillingBill[]} array the list of accumulated items
     * @param {BillingBill} item the new item that should be appended
     * @returns {BillingBill[]} the updated array
     */
    static addNewItem(array, item)
    {
        if (item.billing && item.bill)
            array.push(item);

        return array;
    }

    /**
     *creates a list of new items based on the contents of the table
     *
     * @static
     * @param {string} content the data contained in the billing table
     * @returns {BillingBill[]} an array with all the arranged data
     */
    static fromText(content)
    {
        try
        {
            fileManager.delete(defaultLines.file);
            textFileManager.appendLine(defaultLines.file, content);
            const lines = textFileManager.readToArraySync(defaultLines.file);

            lines.pop();
            lines.shift();

            let items = [];
            let currentParams = [];
            const newContent = lines.map(i => i.trim());
            for (let line of newContent)
            {
                if (line === defaultLines.separator)
                {
                    items = this.addNewItem(items, new BillingBill(...currentParams));
                    currentParams = [];
                }
                else
                {
                    if (currentParams.length < 5)
                        currentParams.push(currentParams.length !== 2 ? this.getLineString(line) : this.getLineValue(line));
                }
            }

            if (currentParams.length === 5)
                items = this.addNewItem(items, new BillingBill(...currentParams));
            return items;
        }
        catch (err)
        {
            console.log(`\n\nBILLING BILL err:\n`, err);
            return [];
        }
    }

    /**
     *Creates an instance of BillingAging.
     * @param {string} billing the system's name
     * @param {string} bill the bill's id
     * @param {number} value the bill's value
     * @param {string} dueDate the bill's due date
     * @param {string} cutDate the bill's 
     */
    constructor(billing, bill, value, dueDate, cutDate)
    {
        this.billing = billing;
        this.bill = bill;
        this.value = value;
        this.dueDate = dueDate;
        this.cutDate = cutDate;
    }
}