const fileManager = require('clericuzzi-javascript/dist/business/managers/fileSystem.manager');
const textFileManager = require('clericuzzi-javascript/dist/business/managers/textFile.manager');
const defaultLines = {
    file: fileManager.currentPathFile(`current.txt`),
    separator: `</tr><tr class="">`
};
module.exports = class BillingAging
{
    /**
     * gets the line content as string
     *
     * @param {string} line
     * @returns {string} the string contained in the given line
     */
    static getLineString(line) 
    {
        line = line.replace(`</a></td>`, ``);
        line = line.substring(line.lastIndexOf(`>`) + 1);

        return line;
    };
    /**
     * gets the line content as a number
     *
     * @param {string} line
     * @returns {string} the number contained in the given line
     */
    static getLineValue(line) 
    {
        line = line.replace(`</td>`, ``);
        line = line.substring(line.lastIndexOf(`>`) + 1);

        return Number.parseInt(line);
    };
    /**
     * adds, if possible, the new item to the array
     *
     * @static
     * @param {BillingAging[]} array the list of accumulated items
     * @param {BillingAging} item the new item that should be appended
     * @returns {BillingAging[]} the updated array
     */
    static addNewItem(array, item)
    {
        if (item.system)
            array.push(item);

        return array;
    }

    /**
     *creates a list of new items based on the contents of the table
     *
     * @static
     * @param {string} content the data contained in the billing table
     * @returns {BillingAging[]} an array with all the arranged data
     */
    static fromText(content)
    {
        try
        {
            fileManager.delete(defaultLines.file);
            textFileManager.appendLine(defaultLines.file, content);
            const lines = textFileManager.readToArraySync(defaultLines.file);

            lines.pop();
            lines.shift();

            let items = [];
            let currentParams = [];
            const newContent = lines.map(i => i.trim());
            for (let line of newContent)
            {
                if (line === defaultLines.separator)
                {
                    items = this.addNewItem(items, new BillingAging(...currentParams));
                    currentParams = [];
                }
                else
                    currentParams.push(currentParams.length === 0 ? this.getLineString(line) : this.getLineValue(line));
            }

            items = this.addNewItem(items, new BillingAging(...currentParams));
            return items;
        }
        catch (err)
        {
            console.log(`\n\nBILLING AGING err:\n`, err);
            return [];
        }
    }

    /**
     *Creates an instance of BillingAging.
     * @param {string} system the system's name
     * @param {number} due_amount the amount due
     * @param {number} due_value the value due
     * @param {number} days_amount_1_30 the amount due
     * @param {number} days_value_1_30 the value due
     * @param {number} days_amount_31_60 the amount due
     * @param {number} days_value_31_60 the value due
     * @param {number} days_amount_61_90 the amount due
     * @param {number} days_value_61_90 the value due
     * @param {number} days_amount_91_105 the amount due
     * @param {number} days_value_91_105 the value due
     * @param {number} days_amount_106_179 the amount due
     * @param {number} days_value_106_179 the value due
     * @param {number} days_amount_180_365 the amount due
     * @param {number} days_value_180_365 the value due
     * @param {number} years_amount_1_3 the amount due
     * @param {number} years_value_1_3 the value due
     * @param {number} years_amount_3_5 the amount due
     * @param {number} years_value_3_5 the value due
     * @param {number} years_amount_over_5 the amount due
     * @param {number} years_value_over_5 the value due
     * @param {number} total_amount the amount due
     * @param {number} total_value the total amount due
     * @param {number} over_30_amount the amount due
     * @param {number} over_30_value the value due
     */
    constructor(system, due_amount, due_value, days_amount_1_30, days_value_1_30, days_amount_31_60, days_value_31_60, days_amount_61_90, days_value_61_90, days_amount_91_105, days_value_91_105, days_amount_106_179, days_value_106_179, days_amount_180_365, days_value_180_365, years_amount_1_3, years_value_1_3, years_amount_3_5, years_value_3_5, years_amount_over_5, years_value_over_5, total_amount, total_value, over_30_amount, over_30_value)
    {
        this.system = system;

        this.due_amount = due_amount;
        this.due_value = due_value;

        this.days_amount_1_30 = days_amount_1_30;
        this.days_value_1_30 = days_value_1_30;

        this.days_amount_31_60 = days_amount_31_60;
        this.days_value_31_60 = days_value_31_60;

        this.days_amount_61_90 = days_amount_61_90;
        this.days_value_61_90 = days_value_61_90;

        this.days_amount_91_105 = days_amount_91_105;
        this.days_value_91_105 = days_value_91_105;

        this.days_amount_106_179 = days_amount_106_179;
        this.days_value_106_179 = days_value_106_179;

        this.days_amount_180_365 = days_amount_180_365;
        this.days_value_180_365 = days_value_180_365;

        this.years_amount_1_3 = years_amount_1_3;
        this.years_value_1_3 = years_value_1_3;

        this.years_amount_3_5 = years_amount_3_5;
        this.years_value_3_5 = years_value_3_5;

        this.years_amount_over_5 = years_amount_over_5;
        this.years_value_over_5 = years_value_over_5;

        this.total_amount = total_amount;
        this.total_value = total_value;

        this.over_30_amount = over_30_amount;
        this.over_30_value = over_30_value;
    }
}