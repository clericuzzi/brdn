const TeamMemberModel = require('./teamMember.model');
const BillingBillModel = require('./billingBill.model');
const BillingAgingModel = require('./billingAging.model');

module.exports = class UserDataModel
{
    /**
     *Creates an instance of UserDataModel.
     * @param {string} errorData the errors description
     * @param {string} convergency YES or NO for convergency
     * @param {string} convergencyMessage the actual convergency message
     * @param {TeamMemberModel[]} walletLandTeam the land team members
     * @param {string} walletLandSegment land segment
     * @param {string} walletLandUpdatedIn land last update
     * @param {TeamMemberModel[]} walletMobileTeam the mobile team members
     * @param {string} walletMobileSegment mobile segment
     * @param {string} walletMobileUpdatedIn mobile last update
     * @param {string} activePlant the spreadsheet on base64
     * @param {string} orders the amound of orders
     * @param {string} contractInfo the contract information
     * @param {BillingBillModel[]} billingData the list of past due bills
     * @param {BillingAgingModel[]} billingAgingData the aging information
     * @param {string} billingScore the client's score
     * @param {string} billingScoreMessage the client's score message
     * @param {string} portabilities the amount of portabilities
     * @param {string} portabilitiesData the actual portabilities data
     */
    constructor(errorData, convergency, convergencyMessage, walletLandTeam, walletLandSegment, walletLandUpdatedIn, walletMobileTeam, walletMobileSegment, walletMobileUpdatedIn, activePlant, orders, contractInfo, billingData, billingAgingData, billingScore, billingScoreMessage, portabilities, portabilitiesData)
    {
        this.errorData = errorData;

        this.convergency = convergency;
        this.convergencyMessage = convergencyMessage;

        this.walletLandTeam = walletLandTeam;
        this.walletLandSegment = walletLandSegment;
        this.walletLandUpdatedIn = walletLandUpdatedIn;

        this.walletMobileTeam = walletMobileTeam;
        this.walletMobileSegment = walletMobileSegment;
        this.walletMobileUpdatedIn = walletMobileUpdatedIn;

        this.activePlant = activePlant;

        this.orders = orders;

        this.contractInfo = contractInfo;

        this.billingData = billingData;
        this.billingAgingData = billingAgingData;
        this.billingScore = billingScore;
        this.billingScoreMessage = billingScoreMessage;

        this.portabilities = portabilities;
        this.portabilitiesData = portabilitiesData;
    }

    appendError(errorMessage)
    {
        if (!this.errorData)
            this.errorData = errorMessage;
        else
            this.errorData = `${this.errorData}|${errorMessage}`;
    }
}