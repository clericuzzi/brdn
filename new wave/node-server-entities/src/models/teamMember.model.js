const fileManager = require('clericuzzi-javascript/dist/business/managers/fileSystem.manager');
const textFileManager = require('clericuzzi-javascript/dist/business/managers/textFile.manager');
const defaultLines = {
    file: fileManager.currentPathFile(`current.txt`),
    separator: `</tr><tr class="">`
};
module.exports = class TeamMemberModel
{
    /**
     * gets the line content as string
     *
     * @param {string} line
     * @returns {string} the string contained in the given line
     */
    static getLineString(line) 
    {
        line = line.replace(`</td>`, ``);
        line = line.substring(line.lastIndexOf(`>`) + 1);

        return line;
    };

    /**
     * creates a new isntance of the class based on a table row
     *
     * @static
     * @param {string} content the team member's table content
     * @returns a new instance of 'TeamMemberModel'
     */
    static async newTeam(content)
    {
        try
        {

            fileManager.delete(defaultLines.file);
            textFileManager.appendLine(defaultLines.file, content);
            const lines = textFileManager.readToArraySync(defaultLines.file);

            lines.pop();
            lines.shift();

            let items = [];
            let currentParams = [];
            const newContent = lines.map(i => i.trim());
            for (let line of newContent)
            {
                if (line === defaultLines.separator)
                {
                    items = this.addNewItem(items, new TeamMemberModel(...currentParams));
                    currentParams = [];
                }
                else
                {
                    if (currentParams.length < 2)
                        currentParams.push(this.getLineString(line));
                }
            }

            if (currentParams.length === 2)
                items = this.addNewItem(items, new TeamMemberModel(...currentParams));
            return items;
        }
        catch (err)
        {
            console.log(`\n\n TEAM MEMBER err:\n`, err);
            return [];
        }
    }

    /**
     *Creates an instance of TeamMemberModel.
     * @param {string} team the team's name
     * @param {string} member the team's member
     */
    constructor(team, member)
    {
        this.team = team;
        this.member = member;
    }
}