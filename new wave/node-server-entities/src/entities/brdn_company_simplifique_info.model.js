const CrudModel = require('clericuzzi-javascript-mysql/dist/models/crud.model');
const UserDataModel = require('../models/userData.model');

const columns = [`company_cnpj`, `convergent`, `convergency_message`, `score`, `score_message`, `orders`, `portabilities`, `timestamp`];
const fkColumns = [];
const tableName = `brdn_company_simplifique_info`;
module.exports = class BrdnCompanySimplifiqueInfoModel extends CrudModel
{
	/**
	 * creates a new 'BrdnCompanySimplifiqueInfoModel' instance based on the data received
	 *
	 * @static
	 * @param {UserDataModel} userData the data collected by the macro
	 * @param {string} cnpj the company's cnpj
	 */
	static new(userData, cnpj)
	{
		return new BrdnCompanySimplifiqueInfoModel(null, cnpj, userData.convergency, userData.convergencyMessage, userData.billingScore, userData.billingScoreMessage, userData.orders || null, userData.portabilities || null, 0, new Date(), 0);
	}

	constructor(id, company_cnpj, convergent, convergency_message, score, score_message, orders, portabilities, plant_file, timestamp, removed = 0)
	{
		super(tableName, columns, fkColumns);

		this.id = id;
		this.company_cnpj = company_cnpj;
		this.convergent = convergent;
		this.convergency_message = convergency_message;
		this.score = score;
		this.score_message = score_message;
		this.orders = orders;
		this.portabilities = portabilities;
		this.plant_file = plant_file;
		this.timestamp = timestamp;
		this.removed = removed;
	}
}