const CrudModel = require('clericuzzi-javascript-mysql/dist/models/crud.model');

const columns = [`name`];
const fkColumns = [];
const tableName = `brdn_company_simplifique_info_order_type`;
module.exports = class BrdnCompanySimplifiqueInfoOrderTypeModel extends CrudModel
{
	constructor(id, name, removed = 0)
	{
		super(tableName, columns, fkColumns);

		this.id = id;
		this.name = name;
		this.removed = removed;
	}
}