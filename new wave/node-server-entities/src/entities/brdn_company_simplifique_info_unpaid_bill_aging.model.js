const arrayUtils = require('clericuzzi-javascript/dist/utils/array.utils');
const CrudModel = require('clericuzzi-javascript-mysql/dist/models/crud.model');
const BillingAgingModel = require('../models/billingAging.model');

const columns = [`company_cnpj`, `system`, `due_amount`, `due_value`, `days_amount_1_30`, `days_value_1_30`, `days_amount_31_60`, `days_value_31_60`, `days_amount_61_90`, `days_value_61_90`, `days_amount_91_105`, `days_value_91_105`, `days_amount_106_179`, `days_value_106_179`, `days_amount_180_365`, `days_value_180_365`, `years_amount_1_3`, `years_value_1_3`, `years_amount_3_5`, `years_value_3_5`, `years_amount_over_5`, `years_value_over_5`, `total_amount`, `total_value`, `over_30_amount`, `over_30_value`];
const fkColumns = [];
const tableName = `brdn_company_simplifique_info_unpaid_bill_aging`;
module.exports = class BrdnCompanySimplifiqueInfoUnpaidBillAgingModel extends CrudModel
{
	/**
	 * creates a new 'BrdnCompanySimplifiqueInfoUnpaidBillAgingModel' instance based on the data received
	 *
	 * @static
	 * @param {BillingAgingModel} bill the data collected by the macro
	 * @param {string} cnpj the company's cnpj
	 */
	static new(bill, cnpj)
	{
		const data = arrayUtils.objectToPropertyList(bill);
		return new BrdnCompanySimplifiqueInfoUnpaidBillAgingModel(...data);
	}
	constructor(id, company_cnpj, system, due_amount, due_value, days_amount_1_30, days_value_1_30, days_amount_31_60, days_value_31_60, days_amount_61_90, days_value_61_90, days_amount_91_105, days_value_91_105, days_amount_106_179, days_value_106_179, days_amount_180_365, days_value_180_365, years_amount_1_3, years_value_1_3, years_amount_3_5, years_value_3_5, years_amount_over_5, years_value_over_5, total_amount, total_value, over_30_amount, over_30_value, removed = 0)
	{
		super(tableName, columns, fkColumns);

		this.id = id;
		this.company_cnpj = company_cnpj;
		this.system = system;
		this.due_amount = due_amount;
		this.due_value = due_value;
		this.days_amount_1_30 = days_amount_1_30;
		this.days_value_1_30 = days_value_1_30;
		this.days_amount_31_60 = days_amount_31_60;
		this.days_value_31_60 = days_value_31_60;
		this.days_amount_61_90 = days_amount_61_90;
		this.days_value_61_90 = days_value_61_90;
		this.days_amount_91_105 = days_amount_91_105;
		this.days_value_91_105 = days_value_91_105;
		this.days_amount_106_179 = days_amount_106_179;
		this.days_value_106_179 = days_value_106_179;
		this.days_amount_180_365 = days_amount_180_365;
		this.days_value_180_365 = days_value_180_365;
		this.years_amount_1_3 = years_amount_1_3;
		this.years_value_1_3 = years_value_1_3;
		this.years_amount_3_5 = years_amount_3_5;
		this.years_value_3_5 = years_value_3_5;
		this.years_amount_over_5 = years_amount_over_5;
		this.years_value_over_5 = years_value_over_5;
		this.total_amount = total_amount;
		this.total_value = total_value;
		this.over_30_amount = over_30_amount;
		this.over_30_value = over_30_value;
		this.removed = removed;
	}
}