const CrudModel = require('clericuzzi-javascript-mysql/dist/models/crud.model');

const columns = [`order_type_id`, `company_cnpj`, `last_update`, `order_id`, `product`, `tecnology`, `order_date`, `contract_date`, `status_sale`, `status_order`, `status_modem`, `status_router`, `status_pendency`];
const fkColumns = [];
const tableName = `brdn_company_simplifique_info_order`;
module.exports =  class BrdnCompanySimplifiqueInfoOrderModel extends CrudModel
{
	constructor(id, order_type_id, company_cnpj, last_update, order_id, product, tecnology, order_date, contract_date, status_sale, status_order, status_modem, status_router, status_pendency, removed = 0)
	{
		super(tableName, columns, fkColumns);

		this.id = id;
		this.order_type_id = order_type_id;
		this.company_cnpj = company_cnpj;
		this.last_update = last_update;
		this.order_id = order_id;
		this.product = product;
		this.tecnology = tecnology;
		this.order_date = order_date;
		this.contract_date = contract_date;
		this.status_sale = status_sale;
		this.status_order = status_order;
		this.status_modem = status_modem;
		this.status_router = status_router;
		this.status_pendency = status_pendency;
		this.removed = removed;
	}
}