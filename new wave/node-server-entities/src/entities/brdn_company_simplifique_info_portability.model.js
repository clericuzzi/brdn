const CrudModel = require('clericuzzi-javascript-mysql/dist/models/crud.model');

const columns = [`company_cnpj`, `phone`, `protocol`, `line_activation_date`, `window`, `status`, `type`, `operator`];
const fkColumns = [];
const tableName = `brdn_company_simplifique_info_portability`;
module.exports =  class BrdnCompanySimplifiqueInfoPortabilityModel extends CrudModel
{
	constructor(id, company_cnpj, phone, protocol, line_activation_date, window, status, type, operator, removed = 0)
	{
		super(tableName, columns, fkColumns);

		this.id = id;
		this.company_cnpj = company_cnpj;
		this.phone = phone;
		this.protocol = protocol;
		this.line_activation_date = line_activation_date;
		this.window = window;
		this.status = status;
		this.type = type;
		this.operator = operator;
		this.removed = removed;
	}
}