const CrudModel = require('clericuzzi-javascript-mysql/dist/models/crud.model');
const UserDataModel = require('../models/userData.model');

const columns = [`company_cnpj`, `name`, `segment`, `updated_in`];
const fkColumns = [];
const tableName = `brdn_company_simplifique_info_wallet`;
module.exports = class BrdnCompanySimplifiqueInfoWalletModel extends CrudModel
{
	/**
	 * creates a new 'BrdnCompanySimplifiqueInfoWalletModel' instance based on the data received
	 *
	 * @static
	 * @param {string} cnpj the company's cnpj
	 */
	static new(cnpj, name, segment, updatedIn)
	{
		return new BrdnCompanySimplifiqueInfoWalletModel(null, cnpj, name, segment, updatedIn, 0);
	}
	constructor(id, company_cnpj, name, segment, updated_in, removed = 0)
	{
		super(tableName, columns, fkColumns);

		this.id = id;
		this.company_cnpj = company_cnpj;
		this.name = name;
		this.segment = segment;
		this.updated_in = updated_in;
		this.removed = removed;
	}
}