const CrudModel = require('clericuzzi-javascript-mysql/dist/models/crud.model');
const BillingBillModel = require('../models/billingBill.model');

const columns = [`company_cnpj`, `system`, `bill_id`, `value`, `due_date`, `cut_date`];
const fkColumns = [];
const tableName = `brdn_company_simplifique_info_unpaid_bill`;
module.exports = class BrdnCompanySimplifiqueInfoUnpaidBillModel extends CrudModel
{
	/**
	 * creates a new 'BrdnCompanySimplifiqueInfoUnpaidBillModel' instance based on the data received
	 *
	 * @static
	 * @param {BillingBillModel} bill the data collected by the macro
	 * @param {string} cnpj the company's cnpj
	 */
	static new(bill, cnpj)
	{
		return new BrdnCompanySimplifiqueInfoUnpaidBillModel(null, cnpj, bill.billing, bill.bill, bill.value, bill.dueDate, bill.cutDate, 0);
	}
	constructor(id, company_cnpj, system, bill_id, value, due_date, cut_date, removed = 0)
	{
		super(tableName, columns, fkColumns);

		this.id = id;
		this.company_cnpj = company_cnpj;
		this.system = system;
		this.bill_id = bill_id;
		this.value = value;
		this.due_date = due_date;
		this.cut_date = cut_date;
		this.removed = removed;
	}
}