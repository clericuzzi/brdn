const CrudModel = require('clericuzzi-javascript-mysql/dist/models/crud.model');
const TeamMemberModel = require('../models/teamMember.model');

const columns = [`wallet_id`, `team`, `member`];
const fkColumns = [];
const tableName = `brdn_company_simplifique_info_wallet_team_member`;
module.exports = class BrdnCompanySimplifiqueInfoWalletTeamMemberModel extends CrudModel
{
	/**
	 * creates a new 'BrdnCompanySimplifiqueInfoWalletTeamMemberModel' instance based on the data received
	 *
	 * @static
	 * @param {TeamMemberModel} teamMember the data collected by the macro
	 * @param {number} walletId the wallet's id
	 */
	static new(teamMember, walletId)
	{
		return new BrdnCompanySimplifiqueInfoWalletTeamMemberModel(null, walletId, teamMember.team, teamMember.member, 0);
	}
	constructor(id, wallet_id, team, member, removed = 0)
	{
		super(tableName, columns, fkColumns);

		this.id = id;
		this.wallet_id = wallet_id;
		this.team = team;
		this.member = member;
		this.removed = removed;
	}
}