module.exports = {
	brdn_address: `brdn_address`,
	brdn_address_availability: `brdn_address_availability`,
	brdn_box: `brdn_box`,
	brdn_campaign: `brdn_campaign`,
	brdn_campaign_characteristic: `brdn_campaign_characteristic`,
	brdn_campaign_company: `brdn_campaign_company`,
	brdn_campaign_user: `brdn_campaign_user`,
	brdn_characteristic: `brdn_characteristic`,
	brdn_characteristic_type: `brdn_characteristic_type`,
	brdn_company: `brdn_company`,
	brdn_company_activity: `brdn_company_activity`,
	brdn_company_info: `brdn_company_info`,
	brdn_company_info_activity: `brdn_company_info_activity`,
	brdn_company_info_partner: `brdn_company_info_partner`,
	brdn_company_simplifique_info: `brdn_company_simplifique_info`,
	brdn_company_simplifique_info_order: `brdn_company_simplifique_info_order`,
	brdn_company_simplifique_info_order_type: `brdn_company_simplifique_info_order_type`,
	brdn_company_simplifique_info_portability: `brdn_company_simplifique_info_portability`,
	brdn_company_simplifique_info_unpaid_bill: `brdn_company_simplifique_info_unpaid_bill`,
	brdn_company_simplifique_info_unpaid_bill_aging: `brdn_company_simplifique_info_unpaid_bill_aging`,
	brdn_company_simplifique_info_wallet: `brdn_company_simplifique_info_wallet`,
	brdn_company_simplifique_info_wallet_team_member: `brdn_company_simplifique_info_wallet_team_member`,
	brdn_contact: `brdn_contact`,
	brdn_historic: `brdn_historic`,
	brdn_macros_info: `brdn_macros_info`,
	brdn_operator: `brdn_operator`,
	brdn_operator_contact: `brdn_operator_contact`,
	brdn_optional: `brdn_optional`,
	brdn_plain: `brdn_plain`,
	brdn_product: `brdn_product`,
	brdn_quiz: `brdn_quiz`,
	brdn_result: `brdn_result`,
	brdn_simplifique_captcha_break: `brdn_simplifique_captcha_break`,
	brdn_tab: `brdn_tab`,
	brdn_user: `brdn_user`,
	brdn_wardrobe: `brdn_wardrobe`,
	simplifique_target: `simplifique_target`,
	smart_vt_address_not_found: `smart_vt_address_not_found`,
	smart_vt_status: `smart_vt_status`,
	smart_vt_test_pool: `smart_vt_test_pool`,
	status_cnpj_check: `status_cnpj_check`,
}