module.exports = {
    endpointFetch: `http://brdn.dyndns.org:5123/abr-fetch`,
    endpointSubmit: `http://brdn.dyndns.org:5123/abr-submit`,

    endpointFetchLocal: `http://localhost:5123/abr-fetch`,
    endpointSubmitLocal: `http://localhost:5123/abr-submit`,
}