const values = require('../constants/values.constant');

const txtManager = require('clericuzzi-javascript/dist/business/managers/textFile.manager');
const fileManager = require('clericuzzi-javascript/dist/business/managers/fileSystem.manager');

const typeUtils = require('clericuzzi-javascript/dist/utils/type.utils');
const httpManager = require('clericuzzi-javascript-http/dist/business/managers/http.manager');

const ChromeDriver = require('clericuzzi-javascript-selenium/src/chrome/seleniumChromeDriver')
const captchaSolver = require('clericuzzi-javascript-selenium/src/2captcha/captchaSolver');

const resultFile = fileManager.currentPathFile(`result.txt`);
const captchaFile = fileManager.currentPathFile(`captcha.jpg`);
const phoneListFile = fileManager.currentPathFile(`phones.txt`);

const resultManager = require('./functions/getResult.function');
const navigateManager = require('./functions/navigateAndUpload.function');
httpManager.setTimeoutDuration(35000);
captchaSolver.initSolver(`97df6a5f5be10a4ad46dac3266e79f05`, 8);

const initFiles = () =>
{
    const currentPath = fileManager.currentPath();
    fileManager.deleteByExtention(currentPath, false, `.jpg`, `.txt`);
};

module.exports = class Impl
{
    constructor()
    {
        this.hasWork = true;
        this.payload = null;
        this.phoneId = new Map();
    }

    writePhoneList()
    {
        try
        {
            fileManager.fileCreateIfNotExists(phoneListFile);
            for (let item of this.payload)
            {
                this.phoneId.set(item.phone, item.id);
                txtManager.appendLine(phoneListFile, item.phone);
            }
        }
        catch (err)
        {
            console.log(`WRITE PHONE LIST ERROR`, err);
        }
    }
    async getPayload()
    {
        try
        {
            initFiles();
            this.payload = await httpManager.post({}, values.endpointFetch);
            this.phoneId = new Map();
            this.hasWork = typeUtils.isArrayAndValid(this.payload.data);
            if (this.hasWork)
            {
                this.payload = this.payload.data;
                this.writePhoneList();
            }
        }
        catch (err)
        {
            console.log(`getting payload ERROR:`, err);
        }
    }
    async sendPayload()
    {
        try
        {
            console.log(`sending payload...`);
            const payload = resultManager.parse(resultFile, this.phoneId);
            // console.log(JSON.stringify(payload));
            const result = await httpManager.post(payload, values.endpointSubmit);
            this.hasWork = false;
            console.log(`payload sent:`, result);
        }
        catch (err)
        {
            console.log(`getting payload ERROR:`, err);
        }
    }

    async run()
    {
        let driver = null;
        try
        {
            await this.getPayload();
            while (this.hasWork)
            {
                driver = new ChromeDriver(fileManager.currentPath());

                await navigateManager.run(driver, phoneListFile, captchaFile);
                await resultManager.run(driver, resultFile);

                driver.kill();

                await this.sendPayload();
                await this.getPayload();
            }
        }
        catch (err)
        {
            console.log(`Run error:`, err);
            if (driver)
                driver.kill();

            await this.run();
        }
    }
}