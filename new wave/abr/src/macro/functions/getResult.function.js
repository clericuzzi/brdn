const ChromeDriver = require('clericuzzi-javascript-selenium/src/chrome/seleniumChromeDriver');

const dateUtils = require('clericuzzi-javascript/dist/utils/date.utils');
const typeUtils = require('clericuzzi-javascript/dist/utils/type.utils');
const arrayUtils = require('clericuzzi-javascript/dist/utils/array.utils');
const txtManager = require('clericuzzi-javascript/dist/business/managers/textFile.manager');
const fileManager = require('clericuzzi-javascript/dist/business/managers/fileSystem.manager');

const tbody = `/html/body/div[1]/div[4]/div/table/tbody`;
const evaluateBody = async (driver) =>
{
    try
    {
        const rows = await driver.getImmediateChildrenByTag(tbody, `tr`);
        return typeUtils.isArrayAndValid(rows);
    }
    catch (err)
    {
        return false;
    }
};

/**
 * extrai o resultado
 *
 * @param {ChromeDriver} driver
 * @param {*} row
 * @param {*} resultFile
 */
const extractResultRow = async (driver, row, resultFile) =>
{
    const rowValues = [];
    const tds = await driver.getChildrenByTag(row, `td`);
    for (let td of tds)
        rowValues.push(await driver.getText(td));

    txtManager.appendLine(resultFile, rowValues.join(`\t`));
};
const parseRow = row =>
{
    try
    {
        const array = row.split(`\t`);
        array.pop();

        if (array.length === 4)
            return { phone: array[0], company: array[1].toLocaleLowerCase(), portabilityDate: dateUtils.parse(array[3]) };
        else
            return null;
    }
    catch (err)
    {
        return null;
    }
};
const parseRows = resultFile =>
{
    return txtManager.readToArraySync(resultFile).filter(i => i.split('\t').length === 5).map(i => parseRow(i));;
};
const parsePhones = (rows, phoneIdMap) =>
{
    const phones = [];
    let currentPhone = null;
    for (let row of rows)
    {
        if (row.phone)
        {
            if (currentPhone)
                phones.push(currentPhone);

            currentPhone = phoneCreate(row, phoneIdMap);
        }
        else
            phoneAddHistory(currentPhone, row);
    }
    if (currentPhone)
        phones.push(currentPhone);

    return phones;
};
const parseCompanies = rows =>
{
    const companies = arrayUtils.distinct(rows.map(i => i.company));
    arrayUtils.sortAsc(companies);

    return companies;
};

const phoneCreate = (row, phoneIdMap) =>
{
    const phone = { id: phoneIdMap.get(row.phone), phone: row.phone, history: [{ company: row.company, until: null }] };

    return phone;
}
const phoneAddHistory = (phone, row) =>
{
    phone.history.push({ company: row.company, until: row.portabilityDate })

    return phone;
}

module.exports.run = async (driver, resultFile) =>
{
    await driver.waitTrueEvaluation(() => evaluateBody(driver), 25000);

    fileManager.fileCreateIfNotExists(resultFile);
    const rows = await driver.getImmediateChildrenByTag(tbody, `tr`);
    for (let row of rows)
        await extractResultRow(driver, row, resultFile);
};
module.exports.parse = (resultFile, phoneIdMap) =>
{
    const rows = parseRows(resultFile);
    const phones = parsePhones(rows, phoneIdMap);
    const companies = parseCompanies(rows);
    const testedIds = Array.from(phoneIdMap.values());

    return { phones, companies, testedIds };
};