const ChromeDriver = require('clericuzzi-javascript-selenium/src/chrome/seleniumChromeDriver');

const url = `https://consultanumero.abrtelecom.com.br/consultanumero/consulta/consultaHistoricoRecenteCtg`;
const inputFile = `/html/body/div[1]/div[4]/div/form/table/tbody/tr[7]/td[1]/input`;
const captchaPath = `/html/body/div[1]/div[4]/div/form/table/tbody/tr[7]/td[3]/div[2]/img`;
const captchaInput = `/html/body/div[1]/div[4]/div/form/table/tbody/tr[7]/td[3]/input[2]`;
const actionButtonPath = `/html/body/div[1]/div[4]/div/form/table/tbody/tr[11]/td/button[2]`;
const firstTnInputPath = `/html/body/div[1]/div[4]/div/form/table/tbody/tr[9]/td/div/div[1]/input`;

const endingDataInput = `/html/body/div[1]/div[4]/div/form/table/tbody/tr[3]/td[3]/input[1]`;
const startingDataInput = `/html/body/div[1]/div[4]/div/form/table/tbody/tr[3]/td[1]/input[1]`;

const captchaSolver = require('clericuzzi-javascript-selenium/src/2captcha/captchaSolver');
const fileManager = require('clericuzzi-javascript/dist/business/managers/fileSystem.manager');

const dateUtils = require('clericuzzi-javascript/dist/utils/date.utils');

/**
 * preenche as informaçoes do cadastro
 *
 * @param {ChromeDriver} driver
 * @param {*} phoneListFile
 * @param {*} fillStartingDate
 */
const fillInfo = async (driver, phoneListFile, fillStartingDate) =>
{
    await driver.wait(inputFile);
    const input = await driver.getElement(inputFile);
    if (fillStartingDate)
    {
        const date = new Date();
        date.setDate(1);
        date.setMonth(date.getMonth() - 4);
        const beginDate = new Date();
        beginDate.setDate(1);
        beginDate.setFullYear(beginDate.getFullYear() - 8);

        await driver.sleep(750);
        await driver.fill(startingDataInput, dateUtils.toDateBr(beginDate));
        await driver.sleep(750);
        await driver.fill(endingDataInput, dateUtils.toDateBr(date));
        await driver.sleep(750);
        await driver.click(firstTnInputPath);
        await driver.sleep(750);
        await driver.fill(input, phoneListFile);
        await driver.sleep(750);

        await driver.click(startingDataInput);
        await driver.sleep(750);
        await driver.sendEnter(startingDataInput);
        await driver.sleep(750);

        await driver.click(endingDataInput);
        await driver.sleep(750);
        await driver.sendEnter(endingDataInput);
        await driver.sleep(750);
    }
    else
    {
        await driver.sleep(750);
        await driver.fill(input, phoneListFile);
    }
};

module.exports.run = async (driver, phoneListFile, captchaFile) =>
{
    fileManager.deleteByExtention(fileManager.currentPath(), `.jpg`);

    try
    {
        await driver.navigate(url);
        console.log(`\n\niniciando...`);

        await fillInfo(driver, phoneListFile, true);

        await driver.click(actionButtonPath);
        await driver.waitAlert();
        await driver.alertDismiss();

        await fillInfo(driver, phoneListFile, false);

        console.log(`Quebrando captcha...`);
        await driver.screenshotCropByXPath(captchaPath, captchaFile);
        const captchaAnswer = await captchaSolver.simpleCaptchaSolve(captchaFile);
        await driver.fill(captchaInput, captchaAnswer);
        console.log(`Captcha quebrado:`, captchaAnswer);

        await driver.click(actionButtonPath);
        await driver.waitAlert(5000, false);
        const hasAlert = await driver.alertHas();
        if (hasAlert)
        {
            await driver.alertDismiss();
            throw new Error(`navigateAndUpload.function run: invalid captcha`);
        }
    }
    catch (err)
    {
        console.log(`navigateAndUpload.function run: `, err);
        throw err;
    }
    finally
    {
        fileManager.delete(captchaFile);
    }
};