const Impl = require('./macro/impl');

module.exports = class Start
{
    async run()
    {
        await Promise.all([
            new Impl().run(),
        ]);
    }
}