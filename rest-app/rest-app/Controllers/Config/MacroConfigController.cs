﻿using System;
using System.Linq;
using RestApi.Database;
using RestApi.Entities;
using Microsoft.AspNetCore.Mvc;
using Clericuzzi.Lib.Http.Communication.Request;
using Clericuzzi.Lib.Http.Communication.Response;
using Clericuzzi.Lib.Http.Communication.Response.Json;
using Clericuzzi.WebApi.Data.Business.DatabaseContext;
using Clericuzzi.WebApi.Entities;
using Microsoft.EntityFrameworkCore;

namespace RestApi.Controllers.Config
{
    public class MacroConfigController : Controller
    {
        const string CURRENT_VERSION_PARAMETER = "current-service-version";
        [Route("api/macro-config/load-or-register")]
        [HttpPost()]
        public JsonResult LoadOrRegister([FromBody] RequestMessage request)
        {
            var updated = false;
            var response = new SingleResponse<MacroMachine>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                {
                    var existingMachine = context.GetDbSet<MacroMachine>().FirstOrDefault(i => i.MachineName.Equals(machineName));
                    if (existingMachine == null)
                    {
                        existingMachine = new MacroMachine { MachineName = machineName, Updated = 0, IsAlive = 0 };
                        existingMachine.SetStateAdded(context);
                    }

                    var currentConfiguration = context.GetDbSet<SystemParameter>().FirstOrDefault(i => i.Parameter.Equals(CURRENT_VERSION_PARAMETER));
                    if (currentConfiguration == null)
                    {
                        currentConfiguration = new SystemParameter() { Parameter = CURRENT_VERSION_PARAMETER, Value = "0.1" };
                        currentConfiguration.SetStateAdded(context);
                        context.SaveChanges();
                    }
                    response.AddParam("current-version", currentConfiguration.Value);

                    if (existingMachine.Updated == 1)
                    {
                        existingMachine.Updated = 0;
                        existingMachine.SetStateModified(context);
                        context.SaveChanges();

                        updated = true;
                    }
                    existingMachine.Load(context);
                    response.Entity = existingMachine;
                }
                if (updated)
                    response.Entity.Updated = 1;
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
    }
}