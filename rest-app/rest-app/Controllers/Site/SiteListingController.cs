﻿using System;
using System.Linq;
using RestApi.Database;
using Clericuzzi.WebApi.Geo;
using Microsoft.AspNetCore.Mvc;
using Clericuzzi.Lib.Http.Communication.Request;
using Clericuzzi.Lib.Http.Communication.Response;
using Clericuzzi.Lib.Http.Communication.Response.Json;
using Clericuzzi.Lib.Http.Communication.Request.Pagination;
using Clericuzzi.WebApi.Entities.Business.Models.Crud;
using RestApi.Entities;
using Clericuzzi.WebApi.Entities;
using Clericuzzi.WebApi.Activities;

namespace RestApi.Controllers.Site
{
    public class SiteListingController : Controller
    {
        /// <summary>
        /// lists the relationships for the user area's filter paneç
        /// </summary>
        /// <param name="request">request definitions</param>
        [Route("api/listing/campaign-screen")]
        [HttpPost()]
        public JsonResult CampaignScreen([FromBody] RequestMessage request)
        {
            var response = new BatchResponseList();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName, out RequestPagination pagination);
                using (var context = AppDatabaseMysql.CreateConnection())
                {
                    response.AddEntityList(Activity.TABLE_NAME, context.GetDbSet<Activity>().Select(i => new TupleModel<int>(i.Id, i.Name)).ToList<object>());
                    response.AddEntityList(Characteristic.TABLE_NAME, context.GetDbSet<Characteristic>().Select(i => new TupleModel<int>(i.Id, i.Name)).ToList<object>());
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }

        /// <summary>
        /// lists the relationships for the user area's filter paneç
        /// </summary>
        /// <param name="request">request definitions</param>
        [Route("api/listing/employees")]
        [HttpPost()]
        public JsonResult ListingEmployees([FromBody] RequestMessage request)
        {
            var response = new BatchResponseList();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName, out RequestPagination pagination);
                using (var context = AppDatabaseMysql.CreateConnection())
                {
                    var sellingTeam = context.GetDbSet<UserPermission>().Where(i => i.PermissionId.Equals(4)).Select(i => i.UserId).ToList();
                    response.AddEntityList(Clericuzzi.WebApi.Entities.User.TABLE_NAME, context.GetDbSet<Clericuzzi.WebApi.Entities.User>().Where(i => sellingTeam.Contains(i.Id)).Select(i => new TupleModel<int>(i.Id, i.Name)).ToList<object>());
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }

        /// <summary>
        /// lists the relationships for the user area's filter paneç
        /// </summary>
        /// <param name="request">request definitions</param>
        [Route("api/listing/crud-user-area")]
        [HttpPost()]
        public JsonResult ListingUserArea([FromBody] RequestMessage request)
        {
            var response = new BatchResponseList();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName, out RequestPagination pagination);
                using (var context = AppDatabaseMysql.CreateConnection())
                {
                    response.AddEntityList(GeographyState.TABLE_NAME, context.GetDbSet<GeographyState>().Select(i => new TupleModel<int>(i.Id, i.Abbreviation)).ToList<object>());
                    response.AddEntityList(Clericuzzi.WebApi.Entities.User.TABLE_NAME, context.GetDbSet<Clericuzzi.WebApi.Entities.User>().Select(i => new TupleModel<int>(i.Id, i.Name)).ToList<object>());
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
        /// <summary>
        /// lists the relationships for the user area's filter paneç
        /// </summary>
        /// <param name="request">request definitions</param>
        [Route("api/listing/crud-position-department-and-lines")]
        [HttpPost()]
        public JsonResult ListingPositionAndDepartment([FromBody] RequestMessage request)
        {
            var response = new BatchResponseList();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName, out RequestPagination pagination);
                var companyId = request.DeserializeTo<int>("company-id", compressed: compressed);
                using (var context = AppDatabaseMysql.CreateConnection())
                {
                    response.AddEntityList(CompanyLine.TABLE_NAME, context.GetDbSet<CompanyLine>().Where(i => i.CompanyId.Equals(companyId)).Select(i => new TupleModel<int>(i.Id, $"{i.Phone}")).ToList<object>());
                    response.AddEntityList(ContactPosition.TABLE_NAME, context.GetDbSet<ContactPosition>().Select(i => new TupleModel<int>(i.Id, i.Name)).ToList<object>());
                    response.AddEntityList(ContactDepartment.TABLE_NAME, context.GetDbSet<ContactDepartment>().Select(i => new TupleModel<int>(i.Id, i.Name)).ToList<object>());
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
        [Route("api/listing/contact-and-tabulation")]
        [HttpPost()]
        public JsonResult ListingContactAndTabulation([FromBody] RequestMessage request)
        {
            var response = new BatchResponseList();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName, out RequestPagination pagination);
                var companyId = request.DeserializeTo<int>("company-id", compressed: compressed);
                using (var context = AppDatabaseMysql.CreateConnection())
                {
                    response.AddEntityList(Tabulation.TABLE_NAME, context.GetDbSet<Tabulation>().Select(i => new TupleModel<int>(i.Id, i.Text)).ToList<object>());
                    response.AddEntityList(CompanyContact.TABLE_NAME, context.GetDbSet<CompanyContact>().Where(i => i.CompanyId.Equals(companyId)).OrderBy(i => i.Name).Select(i => new TupleModel<int>(i.Id, i.Name)).ToList<object>());
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
    }
}
