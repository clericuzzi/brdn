﻿using System;
using System.Linq;
using RestApi.Database;
using RestApi.Entities;
using System.Threading.Tasks;
using RestApi.Business.Enums;
using RestApi.Business.Models;
using Microsoft.AspNetCore.Mvc;
using RestApi.Business.Managers;
using Microsoft.EntityFrameworkCore;
using Clericuzzi.Lib.Http.Communication.Request;
using Clericuzzi.Lib.Http.Communication.Response;
using Clericuzzi.WebApi.Activities.Business.Managers;
using Clericuzzi.WebApi.Data.Business.DatabaseContext;
using Clericuzzi.Lib.Http.Communication.Response.Json;
using Clericuzzi.Lib.Http.Communication.Request.Pagination;

namespace RestApi.Controllers.Site
{
    public class SiteLeadsController : Controller
    {
        /// <summary>
        /// loads the given company's relationships
        /// </summary>
        /// <param name="request">the requested info</param>
        /// <returns>a company and its relationanships</returns>
        [Route("api/companies/new-lead")]
        [HttpPost()]
        public JsonResult NewLead([FromBody] RequestMessage request)
        {
            var response = new SingleResponse<CompanyItem>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName, out RequestPagination pagination);
                var campaignId = request.DeserializeTo<int>("campaign-id", compressed: compressed);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                {
                    var companies = LeadsManager.GetCurrentLead(userId, campaignId, context);
                    response.Entity = SiteCompanyController.LoadItems(companies, context, true).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
        /// <summary>
        /// loads the given company's relationships
        /// </summary>
        /// <param name="request">the requested info</param>
        /// <returns>a company and its relationanships</returns>
        [Route("api/companies/new-lead-from-file")]
        [HttpPost()]
        public JsonResult NewLeadFromFile([FromBody] RequestMessage request)
        {
            var response = new SingleResponse<CompanyItem>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName, out RequestPagination pagination);
                var fileId = request.DeserializeTo<int>("import-file-id", compressed: compressed);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                {
                    var companies = LeadsManager.GetCurrentLeadFromFile(userId, fileId, context);
                    response.Entity = SiteCompanyController.LoadItems(companies, context, true).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }

        [Route("api/companies/new-call")]
        [HttpPost()]
        public JsonResult NewCall([FromBody] RequestMessage request)
        {
            var response = new BatchResponse<CompanyCall>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName, out RequestPagination pagination);
                var model = request.DeserializeTo<CompanyCall>("model", compressed: compressed);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                {
                    //if (!ActivitiesManager.IsUserAllowed(userId, (int)ActivitiesEnum.VendaSoho, context))
                    //    throw new Exception("Você não tem permissão para realizar vendas de SOHO. Entre em contato com o administrador");

                    var existingCalls = context.GetDbSet<CompanyCall>().Where(i => i.CompanyId.Equals(model.CompanyId)).ToList();
                    var openCalls = existingCalls.Where(i => !i.TabulationId.HasValue).ToList();
                    if (openCalls?.Count > 0)
                        throw new Exception("Já existe uma ligação aguardando tabulação...");
                    else
                        model.SetStateAdded(context);

                    context.SaveChanges();
                    response.Entities = context.GetDbSet<CompanyCall>().Where(i => i.CompanyId.Equals(model.CompanyId)).Include(i => i.UserIdParent).Include(i => i.ContactIdParent).Include(i => i.TabulationIdParent).OrderByDescending(i => i.Id).ToList();
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
        [Route("api/companies/new-callback")]
        [HttpPost()]
        public JsonResult NewCallback([FromBody] RequestMessage request)
        {
            var response = new StatusResponse();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName, out RequestPagination pagination);
                var model = request.DeserializeTo<CompanyCallback>("model", compressed: compressed);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                {
                    //if (!ActivitiesManager.IsUserAllowed(userId, (int)ActivitiesEnum.VendaSoho, context))
                    //    throw new Exception("Você não tem permissão para realizar vendas de SOHO. Entre em contato com o administrador");

                    var existingCallbacks = context.GetDbSet<CompanyCallback>().Where(i => i.CompanyId.Equals(model.CompanyId)).ToList();
                    var openCallbacks = existingCallbacks.Where(i => i.Done.Equals(0)).ToList();
                    if (openCallbacks?.Count > 0)
                        throw new Exception("Já existe um agendamento para a empresa selecionada");
                    else
                        model.SetStateAdded(context);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
    }
}
