﻿using System;
using System.Linq;
using RestApi.Database;
using RestApi.Entities;
using Microsoft.AspNetCore.Mvc;
using Clericuzzi.Lib.Utils.Core;
using System.Collections.Generic;
using Clericuzzi.WebApi.Analytics;
using Clericuzzi.WebApi.Activities;
using RestApi.Business.Models.Charts.Calls;
using RestApi.Business.Models.Charts.Sells;
using RestApi.Business.Models.Charts.Mining;
using RestApi.Business.Models.Charts.Campaign;
using Clericuzzi.Lib.Http.Communication.Request;
using RestApi.Business.Models.Charts.Evaluations;
using RestApi.Business.Models.Charts.Tabulations;
using Clericuzzi.Lib.Http.Communication.Response;
using Clericuzzi.Lib.Http.Communication.Response.Json;

namespace RestApi.Controllers.Site
{
    public class SiteStatisticsController : Controller
    {
        [Route("api/stats/calls")]
        [HttpPost()]
        public JsonResult StatsCalls([FromBody] RequestMessage request)
        {
            var response = new SingleResponse<CallsChartModel>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId);
                var finalDate = request.DeserializeTo<DateTime>("to", compressed: compressed);
                var initialDate = request.DeserializeTo<DateTime>("from", compressed: compressed);
                var employeeIds = request.DeserializeTo<List<int>>("employee-ids", compressed: compressed);
                using (var context = AppDatabaseMysql.CreateConnection())
                {
                    finalDate.ToEnd();
                    initialDate.ToStart();
                    response.Entity = new CallsChartModel(initialDate, finalDate);
                    response.Entity.Calculate(context, employeeIds);
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }

        [Route("api/stats/sells")]
        [HttpPost()]
        public JsonResult StatsSells([FromBody] RequestMessage request)
        {
            var response = new SingleResponse<SellsChartModel>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId);
                var finalDate = request.DeserializeTo<DateTime>("to", compressed: compressed);
                var initialDate = request.DeserializeTo<DateTime>("from", compressed: compressed);
                var employeeIds = request.DeserializeTo<List<int>>("employee-ids", compressed: compressed);
                using (var context = AppDatabaseMysql.CreateConnection())
                {
                    response.Entity = new SellsChartModel(initialDate, finalDate);
                    response.Entity.Calculate(context, employeeIds);
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }

        [Route("api/stats/campaign")]
        [HttpPost()]
        public JsonResult StatsCampaign([FromBody] RequestMessage request)
        {
            var response = new BatchResponse<CampaignChartModel>();
            response.Entities = new List<CampaignChartModel>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId);
                using (var context = AppDatabaseMysql.CreateConnection())
                {
                    var chartModels = new List<CampaignChartModel>();
                    var campaignIds = new List<int>() { 5 };
                    var campaigns = context.GetDbSet<Activity>().Where(i => campaignIds.Contains(i.Id)).OrderBy(i => i.Name).ToList();
                    var unavailableLeads = context.GetDbSet<Lead>().Select(i => i.CompanyId).ToList();
                    foreach (var campaign in campaigns)
                    {
                        var chartModel = new CampaignChartModel(campaign.Id, campaign.Name);
                        chartModel.CalculateAvailableLeads(unavailableLeads, context);

                        response.Entities.Add(chartModel);
                    }
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }

        [Route("api/stats/mining")]
        [HttpPost()]
        public JsonResult StatsMining([FromBody] RequestMessage request)
        {
            var response = new SingleResponse<MiningChartModel>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId);
                var finalDate = request.DeserializeTo<DateTime>("to", compressed: compressed);
                var initialDate = request.DeserializeTo<DateTime>("from", compressed: compressed);
                using (var context = AppDatabaseMysql.CreateConnection())
                {
                    response.Entity = new MiningChartModel(initialDate, finalDate);
                    response.Entity.Calculate(context);
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }

        [Route("api/stats/evaluations")]
        [HttpPost()]
        public JsonResult StatsEvaluations([FromBody] RequestMessage request)
        {
            var response = new SingleResponse<EvaluationsChartModel>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId);
                var finalDate = request.DeserializeTo<DateTime>("to", compressed: compressed);
                var initialDate = request.DeserializeTo<DateTime>("from", compressed: compressed);
                using (var context = AppDatabaseMysql.CreateConnection())
                {
                    response.Entity = new EvaluationsChartModel(initialDate, finalDate);

                    var data = context.GetDbSet<AnalyticsStats>().Where(i => i.Timestamp >= response.Entity.From && i.Timestamp <= response.Entity.To).ToList();
                    response.Entity.CalculateAbr(data, context);
                    response.Entity.CalculateCnpj(data, context);
                    response.Entity.CalculateSmartCnpj(data, context);
                    response.Entity.CalculateSmartZipAndNumber(data, context);
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
        [Route("api/stats/tabulations")]
        [HttpPost()]
        public JsonResult StatsTabulations([FromBody] RequestMessage request)
        {
            var response = new SingleResponse<TabulationsChartModel>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId);
                var finalDate = request.DeserializeTo<DateTime>("to", compressed: compressed);
                var initialDate = request.DeserializeTo<DateTime>("from", compressed: compressed);
                var employeeIds = request.DeserializeTo<List<int>>("employee-ids", compressed: compressed);
                using (var context = AppDatabaseMysql.CreateConnection())
                {
                    response.Entity = new TabulationsChartModel(initialDate, finalDate);
                    response.Entity.Calculate(context, employeeIds);
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
    }
}
