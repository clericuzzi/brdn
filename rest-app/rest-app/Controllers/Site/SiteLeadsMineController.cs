﻿using System;
using System.Linq;
using RestApi.Database;
using System.Threading.Tasks;
using RestApi.Business.Enums;
using RestApi.Business.Models;
using Microsoft.AspNetCore.Mvc;
using Clericuzzi.WebApi.Activities;
using Clericuzzi.Lib.Http.Communication.Request;
using Clericuzzi.Lib.Http.Communication.Response;
using Clericuzzi.Lib.Http.Communication.Response.Json;
using Clericuzzi.Lib.Http.Communication.Request.Pagination;
using RestApi.Entities;
using Microsoft.EntityFrameworkCore;
using Clericuzzi.Lib.Utils.Core;

namespace RestApi.Controllers.Site
{
    public class SiteLeadsMineController : Controller
    {
        [Route("api/leads/mine")]
        [HttpPost()]
        public async Task<JsonResult> LeadsMine([FromBody] RequestMessage request)
        {
            var response = new BatchResponse<CompanyItem>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName, out RequestPagination pagination);
                var companyFilter = request.DeserializeTo<Company>("filter", compressed: compressed);
                using (var context = AppDatabaseMysql.CreateConnection())
                {
                    var leads = context.GetDbSet<Lead>().Where(i => i.UserId.Equals(userId)).Include(i => i.CompanyIdParent).ToList();
                    if (!string.IsNullOrEmpty(companyFilter?.Cnpj))
                        leads = leads.Where(i => i.CompanyIdParent.Cnpj.ClearSpecialChars(toLower: true).Contains(companyFilter?.Cnpj.ClearSpecialChars(toLower: true))).ToList();
                    if (!string.IsNullOrEmpty(companyFilter?.FantasyName))
                        leads = leads.Where(i => i.CompanyIdParent.FantasyName.ClearSpecialChars(toLower: true).Contains(companyFilter?.FantasyName.ClearSpecialChars(toLower: true))).ToList();

                    var myCompanies = context.GetDbSet<Company>().Where(i => i.UserId.Equals(userId)).Select(i => i.Id).ToList();
                    var companyIds = leads.Select(i => i.CompanyId).ToList();
                    companyIds.AddRange(myCompanies);
                    var companies = context.GetDbSet<Company>().Where(i => companyIds.Contains(i.Id)).ToList();

                    response.Entities = SiteCompanyController.LoadItems(companies, context, false);
                    response.Entities = response.Entities.OrderBy(i => i.FantasyName).ToList();
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
        [Route("api/leads/callback")]
        [HttpPost()]
        public async Task<JsonResult> LeadsCallback([FromBody] RequestMessage request)
        {
            var response = new BatchResponse<CompanyItem>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName, out RequestPagination pagination);
                using (var context = AppDatabaseMysql.CreateConnection())
                {
                    var callbacks = context.GetDbSet<CompanyCallback>().Where(i => i.UserId.Equals(userId)).ToList();
                    callbacks = callbacks.Where(i => i.Done.Equals(0)).ToList();
                    var companyIds = callbacks.Select(i => i.CompanyId).ToList();
                    var companies = context.GetDbSet<Company>().Where(i => companyIds.Contains(i.Id)).ToList();

                    response.Entities = SiteCompanyController.LoadItems(companies, context, false);
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
    }
}
