﻿using System;
using RestApi.Database;
using RestApi.Entities;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Clericuzzi.WebApi.Entities;
using Clericuzzi.Lib.Http.Communication.Request;
using Clericuzzi.Lib.Http.Communication.Response;
using Clericuzzi.Lib.Http.Communication.Response.Json;
using Clericuzzi.Lib.Http.Communication.Request.Pagination;
using System.Linq;
using Clericuzzi.WebApi.Data.Business.DatabaseContext;
using RestApi.Business.Enums;
using Clericuzzi.WebApi.Activities;
using RestApi.Business.Models;
using System.Collections.Generic;
using Clericuzzi.WebApi.Analytics.Business.Managers;
using Microsoft.EntityFrameworkCore;
using Clericuzzi.WebApi.Data.DatabaseContext;
using Clericuzzi.WebApi.Activities.Business.Managers;
using RestApi.Business.Managers;

namespace RestApi.Controllers.Site
{
    public class SiteCallbackController : Controller
    {
        /// <summary>
        /// loads the given company's relationships
        /// </summary>
        /// <param name="request">the requested info</param>
        /// <returns>a company and its relationanships</returns>
        [Route("api/callbacks/clear")]
        [HttpPost()]
        public JsonResult Clear([FromBody] RequestMessage request)
        {
            var response = new BatchResponse<CompanyCallback>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName, out RequestPagination pagination);
                var callbackId = request.DeserializeTo<int>("callback-id", crypted, compressed);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                {
                    var existingCallback = context.GetDbSet<CompanyCallback>().FirstOrDefault(i => i.Id.Equals(callbackId));
                    if (existingCallback != null)
                    {
                        existingCallback.Done = 1;
                        existingCallback.SetStateModified(context);
                        context.SaveChanges();
                    }
                    response.Entities = context.GetDbSet<CompanyCallback>().Include(i => i.CompanyIdParent).Where(i => i.UserId.Equals(userId) && i.Done.Equals(0)).OrderBy(i => i.CallTime).ToList();
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }

        /// <summary>
        /// loads the given company's relationships
        /// </summary>
        /// <param name="request">the requested info</param>
        /// <returns>a company and its relationanships</returns>
        [Route("api/callbacks/load")]
        [HttpPost()]
        public JsonResult Load([FromBody] RequestMessage request)
        {
            var response = new BatchResponse<CompanyCallback>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName, out RequestPagination pagination);
                using (var context = AppDatabaseMysql.CreateConnection())
                    response.Entities = context.GetDbSet<CompanyCallback>().Include(i => i.CompanyIdParent).Where(i => i.UserId.Equals(userId) && i.Done.Equals(0)).OrderBy(i => i.CallTime).ToList();
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
    }
}
