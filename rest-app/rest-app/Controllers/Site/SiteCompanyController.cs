﻿using System;
using System.Linq;
using RestApi.Database;
using RestApi.Entities;
using RestApi.Business.Enums;
using System.Threading.Tasks;
using RestApi.Business.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Clericuzzi.WebApi.Entities;
using Clericuzzi.WebApi.Activities;
using Microsoft.EntityFrameworkCore;
using Clericuzzi.WebApi.Bi.Business.Models;
using Clericuzzi.WebApi.Data.DatabaseContext;
using Clericuzzi.Lib.Http.Communication.Request;
using Clericuzzi.Lib.Http.Communication.Response;
using Clericuzzi.WebApi.Analytics.Business.Managers;
using Clericuzzi.WebApi.Data.Business.DatabaseContext;
using Clericuzzi.Lib.Http.Communication.Response.Json;
using Clericuzzi.Lib.Http.Communication.Request.Pagination;
using Clericuzzi.Lib.Utils.Core;

namespace RestApi.Controllers.Site
{
    public class SiteCompanyController : Controller
    {
        public static List<CompanyItem> LoadItems(List<Company> companies, DataContext context, bool fullLoad)
        {
            var response = new List<CompanyItem>();
            var idList = companies.Select(i => i.Id).ToList();
            var callbacks = new List<CompanyCallback>();
            var addresses = new List<CompanyAddress>();
            var addressIdList = new List<int>();
            var availabilities = new List<CompanyAvailability>();

            var lastNote = string.Empty;
            var pendingTest = false;

            var bills = new List<CompanyBill>();
            var calls = new List<CompanyCall>();
            var lines = new List<CompanyLine>();
            var tabs = context.GetDbSet<CompanyCall>().Where(i => idList.Contains(i.CompanyId) && i.TabulationId.HasValue).Include(i => i.TabulationIdParent).OrderByDescending(i => i.Id).ToList();
            var notes = context.GetDbSet<CompanyNote>().Where(i => idList.Contains(i.CompanyId)).Include(i => i.UserIdParent).Include(i => i.ContactIdParent).OrderByDescending(i => i.Id).ToList();
            var contacts = new List<CompanyContact>();
            var operators = new List<CompanyLineOperator>();
            var activeServices = new List<CompanyActiveService>();

            var companiesLastTabs = new List<Tuple<int, string>>();
            var companiesLastNotes = new List<Tuple<int, string>>();
            callbacks = context.GetDbSet<CompanyCallback>().Where(i => idList.Contains(i.CompanyId)).ToList();
            callbacks = callbacks.Where(i => i.Done.Equals(0)).ToList();
            if (fullLoad)
            {
                addresses = context.GetDbSet<CompanyAddress>().Where(i => idList.Contains(i.CompanyId)).ToList();
                addressIdList = addresses.Select(i => i.Id).ToList();
                availabilities = context.GetDbSet<CompanyAvailability>().Where(i => addressIdList.Contains(i.CompanyAddressId)).ToList();

                bills = context.GetDbSet<CompanyBill>().Where(i => idList.Contains(i.CompanyId)).OrderByDescending(i => i.Id).ToList();
                calls = context.GetDbSet<CompanyCall>().Where(i => idList.Contains(i.CompanyId)).Include(i => i.UserIdParent).Include(i => i.ContactIdParent).Include(i => i.TabulationIdParent).OrderByDescending(i => i.Id).ToList();
                lines = context.GetDbSet<CompanyLine>().Where(i => idList.Contains(i.CompanyId)).Include(i => i.OperatorIdParent).OrderByDescending(i => i.Id).ToList();
                contacts = context.GetDbSet<CompanyContact>().Where(i => idList.Contains(i.CompanyId)).OrderByDescending(i => i.Id).ToList();
                activeServices = context.GetDbSet<CompanyActiveService>().Where(i => idList.Contains(i.CompanyId)).OrderByDescending(i => i.Id).ToList();

                var lineIds = lines.Select(i => i.Id).ToList();
                operators = context.GetDbSet<CompanyLineOperator>().Where(i => lineIds.Contains(i.CompanyLineId)).OrderByDescending(i => i.Id).ToList();

                lastNote = context.GetDbSet<CompanyNote>().Where(i => idList.Contains(i.CompanyId)).OrderByDescending(i => i.Id).ToList().Select(i => i.Comment).FirstOrDefault();
                pendingTest = context.GetDbSet<CompanyCheckRequest>().Where(i => idList.Contains(i.CompanyId)).Count() > 0;
            }
            else
            {
                foreach (var item in companies)
                {
                    var companyNote = notes.Where(i => i.CompanyId.Equals(item.Id)).OrderByDescending(i => i.Id).FirstOrDefault();
                    if (companyNote != null)
                        companiesLastNotes.Add(new Tuple<int, string>(item.Id, companyNote.Comment));
                }
                notes = new List<CompanyNote>();
            }

            companies.ForEach(i => response.Add(new CompanyItem
            {
                Id = i.Id,
                IsGroup = i.IsGroup,
                ParentCompanyId = i.ParentCompanyId,
                Cnpj = i.Cnpj,
                Phone = i.Phone,
                Capital = i.Capital,
                AccountId = i.AccountId,
                Timestamp = i.Timestamp,
                FormalName = i.FormalName,
                Description = i.Description,
                FantasyName = i.FantasyName,

                WalletLand = i.WalletLand,
                WalletMobile = i.WalletMobile,
                ConvergingMessage = i.ConvergingMessage,

                LastTab = tabs.Where(j => j.CompanyId.Equals(i.Id)).OrderByDescending(j => j.Id).Select(j => $"").FirstOrDefault(),
                LastNote = companiesLastNotes.Where(j => j.Item1.Equals(i.Id)).Select(j => j.Item2).FirstOrDefault(),
                Callback = callbacks.Where(j => j.CompanyId.Equals(i.Id)).OrderByDescending(j => j.Id).Take(1).FirstOrDefault(),

                Bills = bills,
                Calls = calls.Where(j => j.CompanyId.Equals(i.Id)).ToList(),
                Lines = i.CreateLineInfo(lines, operators),
                Notes = notes.Where(j => j.CompanyId.Equals(i.Id)).ToList(),
                Contacts = contacts.Where(j => j.CompanyId.Equals(i.Id)).ToList(),
                Addresses = i.CreateAddressInfo(addresses, availabilities),
                ActiveServices = activeServices,
            }));

            return response;
        }
        public static List<int> GetCompanyIdsByCharacteristics(List<int> characteristicsMust, List<int> characteristicsMustNot, DataContext context)
        {
            List<int> positiveCompanyIds = _GetCompanyIdsByCharacteristics(characteristicsMust, characteristicsMustNot, context);

            return positiveCompanyIds;
        }
        public static IQueryable<Company> GetCompanyQueryByCharacteristics(List<int> characteristicsMust, List<int> characteristicsMustNot, DataContext context)
        {
            List<int> positiveCompanyIds = _GetCompanyIdsByCharacteristics(characteristicsMust, characteristicsMustNot, context);
            var query = context.GetDbSet<Company>().Where(i => positiveCompanyIds.Contains(i.Id));

            return query;
        }
        public static IQueryable<Company> GetCompanyQueryByCharacteristicsOrName(List<int> characteristicsMust, List<int> characteristicsMustNot, string name, DataContext context)
        {
            List<int> positiveCompanyIds = _GetCompanyIdsByCharacteristics(characteristicsMust, characteristicsMustNot, context);
            var query = context.GetDbSet<Company>().Where(i => positiveCompanyIds.Contains(i.Id));
            if (!string.IsNullOrEmpty(name))
            {
                var count = query.Count();
                if (count == 0)
                    query = context.GetDbSet<Company>().Where(i => i.FantasyName.Contains(name));
                else
                    query = query.Where(i => i.FantasyName.Contains(name));
            }

            return query;
        }
        static List<int> _GetCompanyIdsByCharacteristics(List<int> characteristicsMust, List<int> characteristicsMustNot, DataContext context)
        {
            var allCompanies = false;
            var positiveCompanyIds = new List<int>();
            var positiveCompanies = new List<CharacteristicAssociation>();
            var negativeCompanies = new List<int>();
            if (characteristicsMust?.Count > 0)
                positiveCompanies = context.GetDbSet<CharacteristicAssociation>().Where(i => characteristicsMust.Contains(i.CharacteristicId)).ToList();
            else
                allCompanies = true;
            if (characteristicsMustNot?.Count > 0)
                negativeCompanies = context.GetDbSet<CharacteristicAssociation>().Where(i => characteristicsMustNot.Contains(i.CharacteristicId)).Select(i => i.EntityId).ToList();

            if (!allCompanies)
            {
                var positiveCharacteristics = characteristicsMust.Count;
                var positiveGrouped = positiveCompanies.GroupBy(i => i.EntityId, i => i, (key, group) => new Tuple<int, int>(key, group.Count())).ToList();
                positiveCompanyIds = positiveGrouped.Where(i => i.Item2.Equals(positiveCharacteristics)).Select(i => i.Item1).ToList();
            }
            else
                positiveCompanyIds = context.GetDbSet<Company>().Select(i => i.Id).ToList();

            if (negativeCompanies.Count > 0)
                positiveCompanyIds = positiveCompanyIds.Where(i => !negativeCompanies.Contains(i)).ToList();
            return positiveCompanyIds;
        }
        public static void UpdateCompany(Company existingEntity, CompanyInfo companyInfo, int companyId, DataContext context)
        {
            var existingCompanySituation = context.GetDbSet<CompanySituation>().FirstOrDefault(i => i.Name.Equals(companyInfo.Company.SituationIdParent.Name));
            if (existingCompanySituation == null)
            {
                context.Add(companyInfo.Company.SituationIdParent);
                context.SaveChanges();
                existingCompanySituation = companyInfo.Company.SituationIdParent;
            }
            existingEntity.SituationId = existingCompanySituation.Id;
            existingEntity.SituationIdParent = null;

            existingEntity.IsGroup = companyInfo.Company?.IsGroup ?? existingEntity.IsGroup;
            existingEntity.ParentCompanyId = companyInfo.Company?.ParentCompanyId ?? existingEntity.ParentCompanyId;

            existingEntity.Cnpj = companyInfo.Company.Cnpj ?? existingEntity.Cnpj;
            existingEntity.Phone = companyInfo.Company.Phone ?? existingEntity.Phone;
            existingEntity.Capital = companyInfo.Company.Capital ?? existingEntity.Capital;
            existingEntity.FormalName = companyInfo.Company.FormalName ?? existingEntity.FormalName;
            existingEntity.DateOpened = companyInfo.Company.DateOpened ?? existingEntity.DateOpened;
            existingEntity.FantasyName = companyInfo.Company.FantasyName ?? existingEntity.FantasyName;
            existingEntity.JuridicNature = companyInfo.Company.JuridicNature ?? existingEntity.JuridicNature;
            existingEntity.DateLastUpdate = companyInfo.Company.DateLastUpdate ?? existingEntity.DateLastUpdate;
            existingEntity.PrimaryActivity = companyInfo.Company.PrimaryActivity ?? existingEntity.PrimaryActivity;
            if (existingEntity.DateOpened.HasValue && existingEntity.DateOpened.Value.Year < 1900)
                existingEntity.DateOpened = null;
            if (existingEntity.DateLastUpdate.HasValue && existingEntity.DateLastUpdate.Value.Year < 1900)
                existingEntity.DateLastUpdate = null;

            companyInfo.CompanyAddress.CompanyId = companyId;
            companyInfo.CompanyAddress.Street = companyInfo.CompanyAddress.Street + ", CEP: " + companyInfo.CompanyAddress.ZipcodeIdParent.Zipcode;
            companyInfo.CompanyAddress.ZipcodeId = null;
            companyInfo.CompanyAddress.ZipcodeIdParent = null;
            var existingLines = context.GetDbSet<CompanyLine>().Where(i => i.CompanyId.Equals(companyId)).ToList();
            foreach (var line in companyInfo.CompanyLines)
            {
                if (!existingLines.Select(i => i.Phone).Contains(line.Phone))
                {
                    line.CompanyId = companyId;
                    context.AddRange(line);
                }
            }
            context.AddRange(companyInfo.CompanyAddress);
            context.SaveChanges();
        }

        /// <summary>
        /// lists the user companies
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/companies/request-check")]
        [HttpPost()]
        public async Task<JsonResult> RequestCheck([FromBody] RequestMessage request)
        {
            var response = new StatusResponse();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName, out RequestPagination pagination);
                var companyId = request.DeserializeTo<int>("company-id", compressed: compressed, mandatory: false);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                {
                    var existingRequest = await context.GetDbSet<CompanyCheckRequest>().FirstOrDefaultAsync(i => i.CompanyId.Equals(companyId));
                    if (existingRequest == null)
                    {
                        _RequestCheckTemporaryAvailabilityMessage(companyId, context);

                        new CompanyCheckRequest() { CompanyId = companyId }.SetStateAdded(context);
                        AnalyticsManager.Add((int)AnalyticsEnum.UserActivityRequestCheck, context, 1, companyId, userId);
                    }
                    else
                        throw new Exception("Já existe uma solicitação para a empresa selecionada...");
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
        static void _RequestCheckTemporaryAvailabilityMessage(int companyId, DataContext context)
        {
            var availabilities = context.GetDbSet<CompanyAvailability>().Where(i => i.CompanyAddressIdParent.CompanyId.Equals(companyId)).ToList();
            if (availabilities?.Count > 0)
            {
                foreach (var availability in availabilities)
                {
                    availability.Message = "Aguardando teste...";
                    availability.Timestamp = DateTime.Now;
                    availability.Network = availability.Tecnology = availability.TecnologyForTv = null;
                    availability.Over50 = availability.Until20 = availability.From21UpTo50 = availability.Lines = availability.ClosetDistance = availability.ClosetTopSpeed = null;
                }

                context.UpdateRange(availabilities);
            }
            else
            {
                var addresses = context.GetDbSet<CompanyAddress>().Where(i => i.CompanyId.Equals(companyId)).Select(i => i.Id).ToList();
                if (addresses?.Count > 0)
                    foreach (var address in addresses)
                        new CompanyAvailability() { CompanyAddressId = address, Message = "Aguardando teste...", Timestamp = DateTime.Now }.SetStateAdded(context);
            }
        }
        /// <summary>
        /// request priority in a macro by a list of characteristics
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/companies/request-priority")]
        [HttpPost()]
        public async Task<JsonResult> RequestPriority([FromBody] RequestMessage request)
        {
            var response = new StatusResponse();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName, out RequestPagination pagination);
                var companyId = request.DeserializeTo<int>("check-type", compressed: compressed, mandatory: false);
                var characteristicsMust = request.DeserializeTo<List<int>>("characteristics-must", compressed: compressed, mandatory: false);
                var characteristicsMustNot = request.DeserializeTo<List<int>>("characteristics-must-not", compressed: compressed, mandatory: false);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                {
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }

        /// <summary>
        /// lists the user companies
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/companies/listing")]
        [HttpPost()]
        public async Task<JsonResult> Listing([FromBody] RequestMessage request)
        {
            var response = new BatchResponse<CompanyItem>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName, out RequestPagination pagination);
                var filter = request.DeserializeTo<CompanyFilter>("filter", compressed: compressed, mandatory: false);
                using (var context = AppDatabaseMysql.CreateConnection())
                {
                    var user = new User { Id = userId };
                    await user.LoadUserPermissionList(context);

                    var query = context.GetDbSet<Company>().OrderBy(i => i.FantasyName).AsQueryable();
                    if (!user.HasAnyPermissionsUnder((int)PermissionsEnum.UserLevel))
                    {
                        var companyIds = context.GetDbSet<ActivityItem>().Where(i => i.UserId.Equals(userId)).Select(i => i.EntityId).ToList();
                        query = query.Where(i => companyIds.Contains(i.Id));
                    }

                    if (!string.IsNullOrEmpty(filter?.FantasyName))
                        query = query.Where(i => i.FantasyName.Contains(filter.FantasyName));

                    if (!string.IsNullOrEmpty(filter?.Cnpj))
                        query = query.Where(i => i.Cnpj.Contains(filter.Cnpj));

                    var companies = query.Paginate(pagination);
                    response.Pagination = pagination;
                    response.Entities = LoadItems(companies.Entities, context, false);
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
        /// <summary>
        /// loads the given company's relationships
        /// </summary>
        /// <param name="request">the requested info</param>
        /// <returns>a company and its relationanships</returns>
        [Route("api/companies/load")]
        [HttpPost()]
        public async Task<JsonResult> Load([FromBody] RequestMessage request)
        {
            var response = new SingleResponse<CompanyItem>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName, out RequestPagination pagination);
                var companyId = request.DeserializeTo<int>("company-id", compressed: compressed, mandatory: false);
                using (var context = AppDatabaseMysql.CreateConnection())
                {
                    var user = new User { Id = userId };
                    await user.LoadUserPermissionList(context);

                    var companies = context.GetDbSet<Company>().Where(i => i.Id.Equals(companyId)).ToList();
                    response.Entity = LoadItems(companies, context, true).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }

        /// <summary>
        /// loads the given company's relationships
        /// </summary>
        /// <param name="request">the requested info</param>
        /// <returns>a company and its relationanships</returns>
        [Route("api/companies/load-address")]
        [HttpPost()]
        public async Task<JsonResult> LoadAddress([FromBody] RequestMessage request)
        {
            var response = new BatchResponse<AddressItem>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName, out RequestPagination pagination);
                var companyAddress = request.DeserializeTo<CompanyAddressFilter>("filter", compressed: compressed, mandatory: false);
                using (var context = AppDatabaseMysql.CreateConnection())
                {
                    var user = new User { Id = userId };
                    await user.LoadUserPermissionList(context);

                    var companyId = companyAddress.CompanyId.FirstOrDefault();
                    var companies = context.GetDbSet<Company>().Where(i => i.Id.Equals(companyId)).ToList();
                    var company = LoadItems(companies, context, true).FirstOrDefault();

                    response.Entities = company.Addresses;
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }

        /// <summary>
        /// lists the user companies
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/companies/involved")]
        [HttpPost()]
        public JsonResult CompaniesInvolved([FromBody] RequestMessage request)
        {
            var response = new BatchResponse<CompanyItem>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName, out RequestPagination pagination);
                var userIds = request.DeserializeTo<List<int>>("user-ids", crypted, compressed, false) ?? new List<int>();
                var chartItem = request.DeserializeTo<ChartItem>("chart-item", crypted, compressed);
                var siteReport = (SiteReportsEnum)request.DeserializeTo<int>("report-type", crypted, compressed);
                using (var context = AppDatabaseMysql.CreateConnection())
                {
                    IQueryable<Company> query = null;
                    switch (siteReport)
                    {
                        case SiteReportsEnum.CALLS:
                            query = _GetComaniesCalled(chartItem, context, userIds);
                            break;
                        case SiteReportsEnum.SALES:
                            query = _GetComaniesSoldTo(chartItem, context);
                            break;
                        case SiteReportsEnum.MINING:
                            query = _GetComaniesMined(chartItem, context);
                            break;
                        case SiteReportsEnum.EVALUATION:
                            query = _GetComaniesEvaluated(chartItem, context);
                            break;
                        case SiteReportsEnum.TABULATION:
                            query = _GetComaniesTabulated(chartItem, context, userIds);
                            break;
                        default:
                            break;
                    }

                    var companies = query.Paginate(pagination);
                    response.Pagination = pagination;
                    response.Entities = LoadItems(companies.Entities, context, false);
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }

        IQueryable<Company> _GetComaniesMined(ChartItem chartItem, DataContext context)
        {
            return null;
        }
        IQueryable<Company> _GetComaniesCalled(ChartItem chartItem, DataContext context, List<int> userIds)
        {
            var items = context.GetDbSet<CompanyCall>().Where(i => i.UserId.Equals(chartItem.Id)).ToList();
            items = items.Where(i => i.StartedIn > chartItem.From && i.StartedIn < chartItem.To).ToList();
            if (userIds?.Count > 0)
                items = items.Where(i => userIds.Contains(i.UserId)).ToList();

            var companyIds = items.Select(i => i.CompanyId).Distinct().ToList();

            return context.GetDbSet<Company>().Where(i => companyIds.Contains(i.Id));
        }
        IQueryable<Company> _GetComaniesSoldTo(ChartItem chartItem, DataContext context)
        {
            return null;
        }
        IQueryable<Company> _GetComaniesEvaluated(ChartItem chartItem, DataContext context)
        {
            return null;
        }
        IQueryable<Company> _GetComaniesTabulated(ChartItem chartItem, DataContext context, List<int> userIds)
        {
            var items = context.GetDbSet<CompanyCall>().Where(i => i.TabulationId.Equals(chartItem.Id) && i.TabulationId.HasValue).ToList();
            items = items.Where(i => i.StartedIn > chartItem.From && i.StartedIn < chartItem.To).ToList();
            if (userIds?.Count > 0)
                items = items.Where(i => userIds.Contains(i.UserId)).ToList();

            var companyIds = items.Select(i => i.CompanyId).Distinct().ToList();

            return context.GetDbSet<Company>().Where(i => companyIds.Contains(i.Id));
        }
    }
}