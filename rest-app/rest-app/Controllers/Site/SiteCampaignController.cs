﻿using System;
using System.Linq;
using RestApi.Database;
using RestApi.Entities;
using RestApi.Business.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Clericuzzi.WebApi.Activities;
using Clericuzzi.WebApi.Data.DatabaseContext;
using Clericuzzi.Lib.Http.Communication.Request;
using Clericuzzi.Lib.Http.Communication.Response;
using Clericuzzi.Lib.Http.Communication.Response.Json;
using Clericuzzi.WebApi.Data.Business.DatabaseContext;
using Clericuzzi.Lib.Http.Communication.Request.Pagination;
using Clericuzzi.Lib.Utils.Error;

namespace RestApi.Controllers.Site
{
    public class SiteCampaignController : Controller
    {
        static int _GetNewCompanies(List<int> companyIds, DataContext context)
        {
            var tabulatedCompanies = context.GetDbSet<CompanyCall>().Where(i => companyIds.Contains(i.CompanyId) && i.TabulationId.HasValue).Select(i => i.CompanyId).Distinct().ToList();
            var untabulatedCompanies = companyIds.Where(i => !tabulatedCompanies.Contains(i)).ToList();

            return untabulatedCompanies.Count;
        }
        static IQueryable<Company> _GetAvailableCompanies(int campaignId, List<int> allowed, List<int> notAllowed, DataContext context)
        {
            var leadCompanies = context.GetDbSet<Lead>().Where(i => i.ActivityId.Equals(campaignId)).Select(i => i.CompanyId).Distinct().ToList();
            var allowedCompanies = context.GetDbSet<CharacteristicAssociation>().Where(i => allowed.Contains(i.CharacteristicId)).Select(i => new { i.CharacteristicId, i.EntityId }).ToList();
            var notAllowedCompanies = context.GetDbSet<CharacteristicAssociation>().Where(i => notAllowed.Contains(i.CharacteristicId)).Select(i => i.EntityId).Distinct().ToList();

            var allowedCompanyIds = allowedCompanies.Select(i => i.EntityId).Distinct().ToList();
            allowedCompanyIds = allowedCompanyIds.Where(i => !leadCompanies.Contains(i)).ToList();
            allowedCompanyIds = allowedCompanyIds.Where(i => !notAllowedCompanies.Contains(i)).ToList();

            allowedCompanies = allowedCompanies.Where(i => allowedCompanyIds.Contains(i.EntityId)).ToList();

            var grouping = allowedCompanies.GroupBy(i => i.EntityId, (key, group) => new { Company = key, Characteristics = group.Count() }).ToList();
            var allowedCount = allowed.Count;
            allowedCompanyIds = grouping.Where(i => i.Characteristics >= allowedCount).Select(i => i.Company).Distinct().ToList();

            return context.GetDbSet<Company>().Where(i => allowedCompanyIds.Contains(i.Id));
        }

        /// <summary>
        /// loads the given activity conditions
        /// </summary>
        /// <param name="request">the request containing the given campaign id</param>
        /// <returns></returns>
        [Route("api/campaign/load-conditions")]
        [HttpPost()]
        public JsonResult LoadConditions([FromBody] RequestMessage request)
        {
            var response = new SingleResponse<CampaignInfo>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                var campaignId = request.DeserializeTo<int>("campaign-id", crypted, compressed);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                    response.Entity = new CampaignInfo() { AllowedCharacteristics = context.GetDbSet<ActivityCharacteristicMust>().Where(i => i.ActivityId.Equals(campaignId)).Select(i => i.CharacteristicId).ToList(), NotAllowedCharacteristics = context.GetDbSet<ActivityCharacteristicMustNot>().Where(i => i.ActivityId.Equals(campaignId)).Select(i => i.CharacteristicId).ToList() };
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }

        [Route("api/campaign/evaluate-by-conditions")]
        [HttpPost()]
        public JsonResult EvaluateByConditions([FromBody] RequestMessage request)
        {
            var response = new BatchResponse<CompanyItem>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName, out RequestPagination pagination);
                var campaign = request.DeserializeTo<int>("campaign-id", crypted, compressed);
                var allowed = request.DeserializeTo<List<int>>("allowed", crypted, compressed);
                var notAllowed = request.DeserializeTo<List<int>>("not-allowed", crypted, compressed);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                {
                    var query = _GetAvailableCompanies(campaign, allowed, notAllowed, context);
                    var companyIds = query.Select(i => i.Id).ToList();
                    var newCompanies = _GetNewCompanies(companyIds, context);

                    var companies = query.Paginate(pagination);

                    response.AddParam("new-companies", newCompanies);
                    response.Pagination = pagination;
                    response.Entities = SiteCompanyController.LoadItems(companies.Entities, context, false);
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }

        /// <summary>
        /// updates a given campaign with the provided allowed and not allowed characteristics
        /// </summary>
        /// <param name="request">the request date</param>
        [Route("api/campaign/update")]
        [HttpPost()]
        public JsonResult Update([FromBody] RequestMessage request)
        {
            var response = new StatusResponse();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName, out RequestPagination pagination);
                var campaign = request.DeserializeTo<int>("campaign-id", crypted, compressed);
                var allowed = request.DeserializeTo<List<int>>("allowed", crypted, compressed);
                var notAllowed = request.DeserializeTo<List<int>>("not-allowed", crypted, compressed);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                {
                    var existingCampaign = context.GetDbSet<Activity>().FirstOrDefault(i => i.Id.Equals(campaign));
                    if (existingCampaign == null)
                        throw new CampaignNotFoundException();

                    var currentAllowedCharacteristics = context.GetDbSet<ActivityCharacteristicMust>().Where(i => i.ActivityId.Equals(campaign)).ToList();
                    var currentNotAllowedCharacteristics = context.GetDbSet<ActivityCharacteristicMustNot>().Where(i => i.ActivityId.Equals(campaign)).ToList();

                    context.RemoveRange(currentAllowedCharacteristics);
                    context.RemoveRange(currentNotAllowedCharacteristics);
                    context.SaveChanges();

                    var newAllowedCharacteristics = new List<ActivityCharacteristicMust>();
                    allowed.ForEach(i => newAllowedCharacteristics.Add(new ActivityCharacteristicMust { ActivityId = campaign, CharacteristicId = i }));

                    var newNotAllowedCharacteristics = new List<ActivityCharacteristicMustNot>();
                    notAllowed.ForEach(i => newNotAllowedCharacteristics.Add(new ActivityCharacteristicMustNot { ActivityId = campaign, CharacteristicId = i }));

                    context.AddRange(newAllowedCharacteristics);
                    context.AddRange(newNotAllowedCharacteristics);
                    context.SaveChanges();
                }
                response.Success = true;
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
    }

    class CampaignNotFoundException : ExceptionBase
    {
        public CampaignNotFoundException() : base(-4080, "Nâo foi encontrada nenhuma campanha com estas configurações", null)
        {

        }
    }
}
