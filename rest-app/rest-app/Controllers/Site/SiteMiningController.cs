﻿using System;
using System.Linq;
using RestApi.Database;
using RestApi.Entities;
using System.Threading.Tasks;
using RestApi.Business.Enums;
using RestApi.Business.Models;
using Microsoft.AspNetCore.Mvc;
using RestApi.Business.Managers;
using Microsoft.EntityFrameworkCore;
using Clericuzzi.Lib.Http.Communication.Request;
using Clericuzzi.Lib.Http.Communication.Response;
using Clericuzzi.WebApi.Activities.Business.Managers;
using Clericuzzi.WebApi.Data.Business.DatabaseContext;
using Clericuzzi.Lib.Http.Communication.Response.Json;
using Clericuzzi.Lib.Http.Communication.Request.Pagination;
using Clericuzzi.Lib.Http.Communication.Entities;
using Clericuzzi.Lib.Utils.Error;
using RestApi.Business.Exceptions.Site.Mining;
using System.Collections.Generic;

namespace RestApi.Controllers.Site
{
    public class SiteMiningController : Controller
    {
        /// <summary>
        /// loads the given company's relationships
        /// </summary>
        /// <param name="request">the requested info</param>
        /// <returns>a company and its relationanships</returns>
        [Route("api/mining/current-targets")]
        [HttpPost()]
        public JsonResult CurrentTargets([FromBody] RequestMessage request)
        {
            var response = new BatchResponse<CompanyItem>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName, out RequestPagination pagination);
                var filter = request.DeserializeTo<MiningTarget>("filter", crypted, compressed);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                {
                    if (filter?.MiningType.Count > 0)
                    {
                        var checkType = filter?.MiningType.FirstOrDefault();
                        var query = context.GetDbSet<CompanyCheckRequest>().Where(i => i.CheckType.Equals(checkType)).Select(i => i.CompanyIdParent).OrderBy(i => i.FantasyName).AsQueryable();

                        var companies = query.Paginate(pagination);
                        response.Pagination = pagination;
                        response.Entities = SiteCompanyController.LoadItems(companies.Entities, context, false);
                    }
                    else
                        throw new MiningCurrentTargetSelectMiningTypeException();
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
        /// <summary>
        /// loads the given company's relationships
        /// </summary>
        /// <param name="request">the requested info</param>
        /// <returns>a company and its relationanships</returns>
        [Route("api/mining/set-target-filter")]
        [HttpPost()]
        public JsonResult SetTargetsFilter([FromBody] RequestMessage request)
        {
            var response = new BatchResponse<CompanyItem>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName, out RequestPagination pagination);
                var filter = request.DeserializeTo<MiningTarget>("filter", crypted, compressed);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                {
                    if (filter?.MiningType?.Count > 0)
                    {
                        var query = SiteCompanyController.GetCompanyQueryByCharacteristicsOrName(filter.CharacteristicsMust, filter.CharacteristicsMustNot, filter.Name, context);

                        var companies = query.Paginate(pagination);
                        response.Pagination = pagination;
                        response.Entities = SiteCompanyController.LoadItems(companies.Entities, context, false);
                    }
                    else
                        throw new MiningCurrentTargetSelectMiningTypeException();
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
        /// <summary>
        /// loads the given company's relationships
        /// </summary>
        /// <param name="request">the requested info</param>
        /// <returns>a company and its relationanships</returns>
        [Route("api/mining/set-targets")]
        [HttpPost()]
        public JsonResult SetTargets([FromBody] RequestMessage request)
        {
            var response = new StatusResponse();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName, out RequestPagination pagination);
                var filter = request.DeserializeTo<MiningTarget>("filter", crypted, compressed);
                if (filter?.MiningType.Count == 0)
                    throw new MiningCurrentTargetSelectMiningTypeException();

                var newCheckRequests = new List<CompanyCheckRequest>();
                var newCheckRequestsBatch = new List<CompanyCheckRequest>();
                using (var context = AppDatabaseMysql.CreateConnection(true))
                {
                    var checkRequestItems = context.GetDbSet<CompanyCheckRequest>().Where(i => filter.MiningType.Contains(i.CheckType)).ToList();
                    context.RemoveRange(checkRequestItems);
                    context.SaveChanges();

                    var companyIds = SiteCompanyController.GetCompanyIdsByCharacteristics(filter.CharacteristicsMust, filter.CharacteristicsMustNot, context);
                    foreach (var miningType in filter.MiningType)
                        foreach (var companyId in companyIds)
                            newCheckRequests.Add(new CompanyCheckRequest { CheckType = miningType, CompanyId = companyId });
                }

                newCheckRequestsBatch = newCheckRequests.Take(200).ToList();
                newCheckRequests = newCheckRequests.Skip(200).ToList();
                while (newCheckRequestsBatch.Count > 0)
                {
                    using (var context = AppDatabaseMysql.CreateConnection(true))
                    {
                        context.AddRange(newCheckRequestsBatch);
                        context.SaveChanges();
                    }

                    newCheckRequestsBatch = newCheckRequests.Take(200).ToList();
                    newCheckRequests = newCheckRequests.Skip(200).ToList();
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
        /// <summary>
        /// loads the given company's relationships
        /// </summary>
        /// <param name="request">the requested info</param>
        /// <returns>a company and its relationanships</returns>
        [Route("api/mining/clear-targets")]
        [HttpPost()]
        public JsonResult ClearTargets([FromBody] RequestMessage request)
        {
            var response = new StatusResponse();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName, out RequestPagination pagination);
                var filter = request.DeserializeTo<MiningTarget>("filter", crypted, compressed);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                {
                    if (filter?.MiningType.Count > 0)
                    {
                        var checkRequestItems = context.GetDbSet<CompanyCheckRequest>().Where(i => filter.MiningType.Contains(i.CheckType)).ToList();
                        context.RemoveRange(checkRequestItems);
                        context.SaveChanges();
                    }
                    else
                        throw new MiningCurrentTargetSelectMiningTypeException();
                }
                response.Success = true;
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
    }
}
