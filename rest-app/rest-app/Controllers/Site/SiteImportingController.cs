﻿using System;
using RestApi.Database;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Clericuzzi.Lib.Http.Communication.Request;
using Clericuzzi.Lib.Http.Communication.Response;
using Clericuzzi.Lib.Http.Communication.Response.Json;
using Clericuzzi.Lib.Http.Communication.Request.Pagination;
using RestApi.Entities;
using System.Collections.Generic;
using System.Linq;
using Clericuzzi.WebApi.Data.DatabaseContext.Mysql;
using Clericuzzi.WebApi.Data.Business.DatabaseContext;

namespace RestApi.Controllers.Site
{
    public class SiteImportingController : Controller
    {
        /// <summary>
        /// lists the user companies
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/importing/sfa-file")]
        [HttpPost()]
        public async Task<JsonResult> RequestCheck([FromBody] RequestMessage request)
        {
            var response = new StatusResponse();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName, out RequestPagination pagination);
                var importFile = request.DeserializeTo<ImportFile>("company-id", compressed: compressed, mandatory: false);
                var items = request.DeserializeTo<List<Tuple<string, string>>>("items", compressed: compressed, mandatory: false);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                {
                    _ImportFile(importFile, context);
                    _ImportCompanies(importFile.Id, items, context);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
        void _ImportFile(ImportFile importFile, MysqlContext context)
        {
            var existingFile = context.GetDbSet<ImportFile>().FirstOrDefault(i => i.CampaignName.Equals(importFile.CampaignName));
            if (existingFile != null)
                throw new Exception("Já existe uma campanha com este nome...");
            else
            {
                importFile.Timestamp = DateTime.Now;
                importFile.SetStateAdded(context);
                context.SaveChanges();
            }
        }
        static void _ImportCompanies(int importFileId, List<Tuple<string, string>> items, Clericuzzi.WebApi.Data.DatabaseContext.Mysql.MysqlContext context)
        {
            var currentCnpjs = items.Select(i => i.Item1).Distinct().ToList();
            var invalidCnpjs = context.GetDbSet<Company>().Where(i => currentCnpjs.Contains(i.Cnpj)).Select(i => i.Cnpj).ToList();

            var validItems = items.Where(i => !invalidCnpjs.Contains(i.Item1)).ToList();
            var newCompanies = new List<Company>();
            validItems.ForEach(i => newCompanies.Add(new Company { Cnpj = i.Item1, FantasyName = i.Item2, Timestamp = DateTime.Now }));
            context.AddRange(newCompanies);
            context.SaveChanges();

            var companyFileRelationship = new List<ImportFileCompany>();
            newCompanies.ForEach(i => companyFileRelationship.Add(new ImportFileCompany { CompanyId = i.Id, ImportFileId = importFileId }));
            context.AddRange(companyFileRelationship);
            context.SaveChanges();
        }
    }
}
