﻿using System;
using System.Linq;
using Newtonsoft.Json;
using RestApi.Entities;
using RestApi.Database;
using RestApi.Business.Enums;
using System.Threading.Tasks;
using RestApi.Business.Models;
using RestApi.Controllers.Site;
using Microsoft.AspNetCore.Mvc;
using Clericuzzi.Lib.Utils.Core;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Text.RegularExpressions;
using Clericuzzi.WebApi.Data.DatabaseContext;
using Clericuzzi.Lib.Http.Communication.Request;
using Clericuzzi.Lib.Http.Communication.Response;
using Clericuzzi.WebApi.Entities.Business.Managers;
using Clericuzzi.Lib.Http.Communication.Response.Json;
using Clericuzzi.WebApi.Analytics.Business.Managers;

namespace RestApi.Controllers.Macros.Google
{
    public class GoogleCnpjController : Controller
    {
        static string ACTION = ActionsEnum.CnpjGoogle.GetDescription();

        /// <summary>
        /// lists the characteristics associated with the given entity id
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/macro/cnpj-google-source")]
        [HttpPost()]
        public async Task<string> Source([FromBody] RequestMessage request)
        {
            var result = string.Empty;
            bool crypted = false;
            var response = new SingleResponse<Company>();
            try
            {
                request.ReadConfig(out crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                    response.Entity = await _GetNext(machineName, context);
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }
            finally
            {
                var json = Json(response, JsonSettings.PASCAL_CASE);
                result = JsonConvert.SerializeObject(json.Value);
                if (crypted)
                    result = CryptUtils.Encript(result);
            }

            return result;
        }
        /// <summary>
        /// lists the characteristics associated with the given entity id
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/macro/cnpj-google-clear")]
        [HttpPost()]
        public async Task<string> Clear([FromBody] RequestMessage request)
        {
            var result = string.Empty;
            bool crypted = false;
            var response = new SingleResponse<Company>();
            try
            {
                request.ReadConfig(out crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                {
                    await ConcurrencyManager.Clear(ACTION, null, machineName, context);

                    await context.SaveChangesAsync();
                    response.Entity = await _GetNext(machineName, context);
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }
            finally
            {
                var json = Json(response, JsonSettings.PASCAL_CASE);
                result = JsonConvert.SerializeObject(json.Value);
                if (crypted)
                    result = CryptUtils.Encript(result);
            }

            return result;
        }
        /// <summary>
        /// lists the characteristics associated with the given entity id
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/macro/cnpj-google-consolidate")]
        [HttpPost()]
        public async Task<string> Consolidate([FromBody] RequestMessage request)
        {
            var result = string.Empty;
            bool crypted = false;
            var response = new SingleResponse<Company>();
            try
            {
                request.ReadConfig(out crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                var companyId = request.DeserializeTo<int>("company-id", compressed: compressed);
                var companies = request.DeserializeTo<List<CompanyInfo>>("companies", compressed: compressed);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                {
                    await ConcurrencyManager.Clear(ACTION, null, machineName, context);
                    if (companies.Count > 0)
                    {
                        var existingEntity = await context.GetDbSet<Company>().FirstOrDefaultAsync(i => i.Id.Equals(companyId));
                        if (existingEntity != null)
                        {
                            var existingCompanyAddresses = context.GetDbSet<CompanyAddress>().Where(i => i.CompanyId.Equals(existingEntity.Id)).ToList();
                            foreach (var companyInfo in companies)
                            {
                                if (string.IsNullOrEmpty(companyInfo.Company.FantasyName))
                                    companyInfo.Company.FantasyName = companyInfo.Company.FormalName;

                                if (_IsSameCompany(existingEntity, existingCompanyAddresses, companyInfo, context))
                                {
                                    SiteCompanyController.UpdateCompany(existingEntity, companyInfo, companyId, context);
                                    AnalyticsManager.Add(AnalyticsEnum.GoogleCnpjCompanyUpdated.GetValue(), context, entityId: companyId);
                                }
                                else
                                {
                                    _CreateCompany(companyInfo, context);
                                    AnalyticsManager.Add(AnalyticsEnum.GoogleCnpjCompanyNew.GetValue(), context, entityId: companyInfo.Company.Id);
                                }
                            }
                        }
                    }

                    response.Entity = await _GetNext(machineName, context);
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }
            finally
            {
                var json = Json(response, JsonSettings.PASCAL_CASE);
                result = JsonConvert.SerializeObject(json.Value);
                if (crypted)
                    result = CryptUtils.Encript(result);
            }

            return result;
        }

        void _CreateCompany(CompanyInfo companyInfo, DataContext context)
        {
            var existingCompany = context.GetDbSet<Company>().FirstOrDefault(i => i.Cnpj.Equals(companyInfo.Company.Cnpj));
            if (existingCompany != null)
                return;
            else
            {
                var existingCompanySituation = context.GetDbSet<CompanySituation>().FirstOrDefault(i => i.Name.Equals(companyInfo.Company.SituationIdParent.Name));
                if (existingCompanySituation == null)
                {
                    context.Add(companyInfo.Company.SituationIdParent);
                    context.SaveChanges();
                    existingCompanySituation = companyInfo.Company.SituationIdParent;
                }
                companyInfo.Company.AccountId = 1;
                companyInfo.Company.SituationId = existingCompanySituation.Id;
                companyInfo.Company.SituationIdParent = null;

                if (companyInfo.Company.DateOpened.HasValue && companyInfo.Company.DateOpened.Value.Year < 1900)
                    companyInfo.Company.DateOpened = null;
                if (companyInfo.Company.DateLastUpdate.HasValue && companyInfo.Company.DateLastUpdate.Value.Year < 1900)
                    companyInfo.Company.DateLastUpdate = null;

                companyInfo.Company.Timestamp = DateTime.Now;
                context.Add(companyInfo.Company);
                context.SaveChanges();

                companyInfo.CompanyAddress.CompanyId = companyInfo.Company.Id;
                companyInfo.CompanyAddress.Street = companyInfo.CompanyAddress.Street + ", CEP: " + companyInfo.CompanyAddress.ZipcodeIdParent.Zipcode;
                companyInfo.CompanyAddress.ZipcodeId = null;
                companyInfo.CompanyAddress.ZipcodeIdParent = null;
                foreach (var line in companyInfo.CompanyLines)
                {
                    line.CompanyId = companyInfo.Company.Id;
                    context.AddRange(line);
                }
                context.AddRange(companyInfo.CompanyAddress);
                context.SaveChanges();
            }
        }
        bool _IsSameCompany(Company existingEntity, List<CompanyAddress> existingCompanyAddresses, CompanyInfo companyInfo, DataContext context)
        {
            if (!string.IsNullOrEmpty(companyInfo.Company.Phone) && _IsSameCompanyByPhone(existingEntity, companyInfo.Company))
                return true;

            return _IsSameCompanyByAddress(existingCompanyAddresses, companyInfo.CompanyAddress);
        }
        bool _IsSameCompanyByPhone(Company existingEntity, Company company)
        {
            return existingEntity.Phone == company.Phone;
        }
        bool _IsSameCompanyByAddress(List<CompanyAddress> existingCompanyAddresses, CompanyAddress companyAddress)
        {
            foreach (var existingCompanyAddress in existingCompanyAddresses)
            {
                var zip = Regex.Match(existingCompanyAddress.Street, RegexUtils.CEP)?.Groups.FirstOrDefault().Value?.DigitsOnly();
                if (!string.IsNullOrEmpty(zip) && zip.Equals(companyAddress.ZipcodeIdParent.Zipcode) && existingCompanyAddress.Number == companyAddress.Number)
                    return true;
            }

            return false;
        }


        async Task<Company> _GetNext(string machineName, DataContext context)
        {
            Company entity = null;
            var currentId = await ConcurrencyManager.GetId(ACTION, null, machineName, context);
            if (currentId.HasValue)
                entity = await context.GetDbSet<Company>().FirstOrDefaultAsync(i => i.Id.Equals(currentId));
            else
            {
                var busyItems = await ConcurrencyManager.GetIds(ACTION, context);
                var availableIds = context.GetDbSet<Company>().Where(i => string.IsNullOrEmpty(i.Cnpj)).Select(i => i.Id).ToList();
                currentId = availableIds.Where(i => !busyItems.Contains(i)).ToList().GetRandomItem();
                entity = await context.GetDbSet<Company>().FirstOrDefaultAsync(i => i.Id.Equals(currentId));

                await ConcurrencyManager.StoreId(entity.Id, ACTION, null, machineName, context);
            }

            if (entity == null)
                await ConcurrencyManager.Clear(ACTION, null, machineName, context);

            return entity;
        }
    }
}