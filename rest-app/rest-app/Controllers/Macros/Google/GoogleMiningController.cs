﻿using System;
using System.Linq;
using Newtonsoft.Json;
using RestApi.Entities;
using RestApi.Database;
using RestApi.Business.Enums;
using System.Threading.Tasks;
using RestApi.Business.Models;
using Microsoft.AspNetCore.Mvc;
using Clericuzzi.Lib.Utils.Core;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Clericuzzi.WebApi.Data.DatabaseContext;
using Clericuzzi.Lib.Http.Communication.Request;
using Clericuzzi.Lib.Http.Communication.Response;
using Clericuzzi.WebApi.Entities.Business.Managers;
using Clericuzzi.WebApi.Data.Business.DatabaseContext;
using Clericuzzi.Lib.Http.Communication.Response.Json;
using Clericuzzi.WebApi.Analytics.Business.Managers;

namespace RestApi.Controllers.Macros.Google
{
    public class GoogleMiningController : Controller
    {
        static string ACTION = ActionsEnum.MiningGoogle.GetDescription();

        /// <summary>
        /// receives the mined companies
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/macro/mining-google-send")]
        [HttpPost()]
        public async Task<JsonResult> Send([FromBody] RequestMessage request)
        {
            var result = string.Empty;
            bool crypted = false;
            var response = new StatusResponse();
            try
            {
                request.ReadConfig(out crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                var companies = request.DeserializeTo<List<CompanyItemMacro>>("companies", crypted, compressed);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                    await _ReceiveCompanies(companies, context);

                response.Success = true;
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
        /// <summary>
        /// lists the characteristics associated with the given entity id
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/macro/mining-google-source")]
        [HttpPost()]
        public async Task<JsonResult> Source([FromBody] RequestMessage request)
        {
            var result = string.Empty;
            bool crypted = false;
            var response = new SingleResponse<MiningGoogle>();
            try
            {
                request.ReadConfig(out crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                    response.Entity = await _GetNext(machineName, context);
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
        /// <summary>
        /// lists the characteristics associated with the given entity id
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/macro/mining-google-clear")]
        [HttpPost()]
        public async Task<JsonResult> Clear([FromBody] RequestMessage request)
        {
            var response = new SingleResponse<MiningGoogle>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                {
                    await ConcurrencyManager.Clear(ACTION, null, machineName, context);

                    response.Entity = await _GetNext(machineName, context);
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
        /// <summary>
        /// lists the characteristics associated with the given entity id
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/macro/mining-google-consolidate")]
        [HttpPost()]
        public async Task<JsonResult> Consolidate([FromBody] RequestMessage request)
        {
            var response = new SingleResponse<MiningGoogle>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                var miningId = request.DeserializeTo<int>("mining-id", crypted, compressed);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                {
                    await ConcurrencyManager.Clear(ACTION, null, machineName, context);
                    var existingMiningEntity = await context.GetDbSet<MiningGoogle>().FirstOrDefaultAsync(i => i.Id.Equals(miningId));
                    if (existingMiningEntity != null)
                    {
                        existingMiningEntity.SetStateDeleted(context);
                        await context.SaveChangesAsync();
                    }

                    response.Entity = await _GetNext(machineName, context);
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }

        async Task _ReceiveCompanies(List<CompanyItemMacro> companies, DataContext context)
        {
            var phones = companies.Select(i => i.Company.Phone).Distinct().ToList();
            if (companies.Count > 0)
            {
                foreach (var newCompany in companies)
                {
                    var valid = false;
                    // checks if the new company's phone already exists in the system
                    var existingCompany = await context.GetDbSet<Company>().Where(i => i.Phone.Equals(newCompany.Company.Phone)).FirstOrDefaultAsync();
                    if (existingCompany != null)
                    {
                        var existingAddress = await context.GetDbSet<CompanyAddress>().FirstOrDefaultAsync(i => i.Street.Equals(newCompany.Address));
                        valid = existingAddress == null;
                        if (valid)
                        {
                            // when an existing phone number arrives
                            // the new company is stored under the existing one
                            // and they both are marked as "within a group"
                            newCompany.Company.ParentCompanyId = existingCompany.Id;
                            newCompany.Company.IsGroup = 1;
                            newCompany.Company.Phone = null;
                            existingCompany.IsGroup = 1;
                            existingCompany.SetStateModified(context);
                        }
                    }
                    else
                        valid = true;

                    var company = newCompany.Company;
                    if (valid)
                    {
                        await company.NewCompany(newCompany.Address, context);
                        AnalyticsManager.Add(AnalyticsEnum.MiningActivitySuccessGoogle.GetValue(), context, entityId: company.Id);
                    }
                }
            }
        }
        async Task<MiningGoogle> _GetNext(string machineName, DataContext context)
        {
            MiningGoogle entity = null;
            var currentId = await ConcurrencyManager.GetId(ACTION, null, machineName, context);
            if (currentId.HasValue)
                entity = await context.GetDbSet<MiningGoogle>().Include(i => i.CityIdParent.StateIdParent).Include(i => i.CompanySubtypeIdParent).FirstOrDefaultAsync(i => i.Id.Equals(currentId));
            else
            {
                var busyItems = await ConcurrencyManager.GetIds(ACTION, context);
                entity = (await context.GetDbSet<MiningGoogle>().Include(i => i.CityIdParent.StateIdParent).Include(i => i.CompanySubtypeIdParent).Where(i => !busyItems.Contains(i.Id)).ToListAsync()).GetRandomItem();

                await ConcurrencyManager.StoreId(entity.Id, ACTION, null, machineName, context);
            }

            return entity;
        }
    }
}