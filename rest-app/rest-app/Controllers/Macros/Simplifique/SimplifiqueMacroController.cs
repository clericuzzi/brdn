﻿using System;
using System.Linq;
using Newtonsoft.Json;
using RestApi.Entities;
using RestApi.Database;
using RestApi.Business.Enums;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Clericuzzi.Lib.Utils.Core;
using Microsoft.EntityFrameworkCore;
using Clericuzzi.WebApi.Data.DatabaseContext;
using Clericuzzi.Lib.Http.Communication.Request;
using Clericuzzi.Lib.Http.Communication.Response;
using Clericuzzi.WebApi.Entities.Business.Managers;
using Clericuzzi.Lib.Http.Communication.Response.Json;
using Clericuzzi.WebApi.Data.Business.DatabaseContext;
using System.Collections.Generic;
using Clericuzzi.WebApi.Analytics.Business.Managers;

namespace RestApi.Controllers.Macros.Simplifique
{
    public class SimplifiqueMacroController : Controller
    {
        static string ACTION = ActionsEnum.Simplifique.GetDescription();

        /// <summary>
        /// lists the characteristics associated with the given entity id
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/macro/simplifique/source")]
        [HttpPost()]
        public async Task<JsonResult> Source([FromBody] RequestMessage request)
        {
            var response = new SingleResponse<Company>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                    response.Entity = await _GetNext(machineName, context);
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
        /// <summary>
        /// lists the characteristics associated with the given entity id
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/macro/simplifique/clear")]
        [HttpPost()]
        public async Task<JsonResult> Clear([FromBody] RequestMessage request)
        {
            var response = new SingleResponse<Company>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                {
                    await ConcurrencyManager.Clear(ACTION, null, machineName, context);

                    await context.SaveChangesAsync();
                    response.Entity = await _GetNext(machineName, context);
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
        /// <summary>
        /// lists the characteristics associated with the given entity id
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/macro/simplifique/consolidate")]
        [HttpPost()]
        public async Task<JsonResult> Consolidate([FromBody] RequestMessage request)
        {
            var response = new SingleResponse<Company>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                var companyId = request.DeserializeTo<int>("company-id", compressed: compressed);

                var companyBills = request.DeserializeTo<List<CompanyBill>>("company-bills", compressed: compressed);

                var companyName = request.DeserializeTo<string>("company-name", compressed: compressed);
                var convergingMessage = request.DeserializeTo<string>("converging-message", compressed: compressed);
                var companyWalletLand = request.DeserializeTo<string>("company-wallet-land", compressed: compressed);
                var companyWalletMobile = request.DeserializeTo<string>("company-wallet-mobile", compressed: compressed);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                {
                    await ConcurrencyManager.Clear(ACTION, null, machineName, context);
                    var existingCompany = context.GetDbSet<Company>().FirstOrDefault(i => i.Id.Equals(companyId));
                    if (existingCompany != null)
                    {
                        existingCompany.FormalName = companyName ?? existingCompany.FormalName;
                        existingCompany.WalletLand = companyWalletLand ?? existingCompany.WalletLand;
                        existingCompany.WalletMobile = companyWalletMobile ?? existingCompany.WalletMobile;
                        existingCompany.ConvergingMessage = convergingMessage ?? existingCompany.ConvergingMessage;
                        existingCompany.SetStateModified(context);
                    }

                    var existingBills = context.GetDbSet<CompanyBill>().Where(i => i.CompanyId.Equals(companyId)).ToList();
                    if (existingBills?.Count > 0)
                    {
                        var billing = companyBills.Select(i => i.Billing).Distinct().ToList();
                        var removeBills = existingBills.Where(i => billing.Contains(i.Billing)).ToList();
                        context.RemoveRange(removeBills);
                        context.SaveChanges();
                    }
                    if (companyBills?.Count > 0)
                    {
                        companyBills.ForEach(i => i.CompanyId = companyId);
                        context.AddRange(companyBills);
                    }

                    AnalyticsManager.Add((int)AnalyticsEnum.MiningSuccessSimplifique, context, 1, companyId);
                    await context.SaveChangesAsync();
                    response.Entity = await _GetNext(machineName, context);
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }

        async Task<Company> _GetNext(string machineName, DataContext context)
        {
            Company entity = null;
            var currentId = await ConcurrencyManager.GetId(ACTION, null, machineName, context);
            if (currentId.HasValue)
                entity = await context.GetDbSet<Company>().FirstOrDefaultAsync(i => i.Id.Equals(currentId));
            else
            {
                entity = _PriorityCompanies(context);
                if (entity == null)
                {
                    var busyItems = await ConcurrencyManager.GetIds(ACTION, context);
                    var availableIds = context.GetDbSet<Company>().Where(i => !string.IsNullOrEmpty(i.Cnpj) && string.IsNullOrEmpty(i.ConvergingMessage)).Select(i => i.Id).ToList();
                    currentId = availableIds.Where(i => !busyItems.Contains(i)).ToList().GetRandomItem();
                    entity = await context.GetDbSet<Company>().FirstOrDefaultAsync(i => i.Id.Equals(currentId));
                }

                await ConcurrencyManager.StoreId(entity.Id, ACTION, null, machineName, context);
            }

            return entity;
        }
        Company _PriorityCompanies(DataContext context)
        {
            var priority = context.GetDbSet<CompanyCheckRequest>().Where(i => i.CheckType.Equals((int)CompanyCheckTypeEnum.Simplifique)).GetRandomRow();
            if (priority != null)
            {
                priority.SetStateDeleted(context);
                return context.GetDbSet<Company>().Where(i => i.Id.Equals(priority.CompanyId)).GetRandomRow();
            }

            return null;
        }
    }
}
