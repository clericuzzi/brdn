﻿using System;
using System.Linq;
using RestApi.Entities;
using RestApi.Database;
using System.Threading.Tasks;
using RestApi.Business.Enums;
using RestApi.Business.Models;
using Microsoft.AspNetCore.Mvc;
using Clericuzzi.Lib.Utils.Core;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Clericuzzi.WebApi.Data.DatabaseContext;
using Clericuzzi.Lib.Http.Communication.Request;
using Clericuzzi.Lib.Http.Communication.Response;
using Clericuzzi.WebApi.Analytics.Business.Managers;
using Clericuzzi.WebApi.Data.Business.DatabaseContext;
using Clericuzzi.Lib.Http.Communication.Response.Json;
using Clericuzzi.WebApi.Analytics;
using Clericuzzi.WebApi.Data.DatabaseContext.Mysql;

namespace RestApi.Controllers.Macros.Abr
{
    public class AbrSohoMacroController : Controller
    {
        const int ACTIVITY_ID = (int)ActivitiesEnum.AbrTelecomSohoVt;
        static string ACTION = ActionsEnum.AbrTelecomSoho.GetDescription();

        /// <summary>
        /// lists the characteristics associated with the given entity id
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/macro/abr-soho/clear")]
        [HttpPost()]
        public JsonResult Clear([FromBody] RequestMessage request)
        {
            var finishedSaving = false;
            var response = new BatchResponse<CompanyLine>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                {
                    while (!finishedSaving)
                    {
                        try
                        {
                            response.Entities = _GetNext(context);
                            finishedSaving = true;
                        }
                        catch (DbUpdateConcurrencyException ex)
                        {
                            finishedSaving = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
        /// <summary>
        /// lists the characteristics associated with the given entity id
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/macro/abr-soho/source")]
        [HttpPost()]
        public JsonResult Source([FromBody] RequestMessage request)
        {
            var response = new BatchResponse<CompanyLine>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                    response.Entities = _GetNext(context);
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
        /// <summary>
        /// lists the characteristics associated with the given entity id
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/macro/abr-soho/consolidate")]
        [HttpPost()]
        public JsonResult Consolidate([FromBody] RequestMessage request)
        {
            var response = new BatchResponse<CompanyLine>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                var linesInfo = request.DeserializeTo<List<CompanyLineInfo>>("line-info", compressed: compressed);
                var deleteLines = request.DeserializeTo<bool>("delete-lines", compressed: compressed);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                {
                    if (!deleteLines)
                    {
                        var operators = context.GetDbSet<PhoneOperator>().ToList();
                        _ValidateOperators(linesInfo, operators, context);
                        var companyLines = linesInfo.Select(i => i.Id).Distinct().ToList();
                        var existingLines = context.GetDbSet<CompanyLine>().Where(i => companyLines.Contains(i.Id)).ToList();
                        var existingPortabilities = context.GetDbSet<CompanyLineOperator>().Where(i => companyLines.Contains(i.CompanyLineId)).ToList();
                        var groups = linesInfo.GroupBy(i => i.Id, i => i, (key, group) => new Tuple<int, List<CompanyLineInfo>>(key, group.ToList())).ToList();
                        foreach (var group in groups)
                            _ConsolidateGroup(group, existingLines, existingPortabilities, operators, context);

                        _CreateStatistics(existingLines.Select(i => i.CompanyId).ToList(), context);
                    }
                    else
                        _DeleteLineTestings(linesInfo, context);

                    context.SaveChanges();
                    response.Entities = _GetNext(context);
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }

        private void _CreateStatistics(List<int> companyLines, DataContext context)
        {
            var stats = new List<AnalyticsStats>();
            companyLines.ForEach(i => stats.Add(AnalyticsManager.New(AnalyticsEnum.MiningActivitySuccessAbr.GetValue(), entityId: i)));

            context.AddRange(stats);
        }

        void _DeleteLineTestings(List<CompanyLineInfo> linesInfo, DataContext context)
        {
            var companyLineIds = linesInfo.Select(i => i.Id).ToList();
            var existingLines = context.GetDbSet<CompanyLine>().Where(i => companyLineIds.Contains(i.Id)).ToList();
            if (existingLines?.Count > 0)
            {
                existingLines.ForEach(i => i.TestingTimestamp = DateTime.Now);

                context.UpdateRange(existingLines);
            }
        }

        void _ValidateOperators(List<CompanyLineInfo> linesInfo, List<PhoneOperator> existingOperators, DataContext context)
        {
            var operators = linesInfo.Select(i => i.Operator.ClearSpecialChars().Trim().FirstCharacterToUpper()).OrderBy(i => i).Distinct().ToList();
            var existingOperatorNames = existingOperators.Select(i => i.Name).ToList();

            var newOperators = operators.Where(i => !existingOperatorNames.Contains(i)).ToList();
            if (newOperators?.Count > 0)
            {
                var newPhoneOperators = new List<PhoneOperator>();
                newOperators.ForEach(i => newPhoneOperators.Add(new PhoneOperator() { Name = i }));

                context.AddRange(newPhoneOperators);
                context.SaveChanges();
            }
        }
        void _ConsolidateGroup(Tuple<int, List<CompanyLineInfo>> group, List<CompanyLine> existingLines, List<CompanyLineOperator> existingPortabilities, List<PhoneOperator> operators, DataContext context)
        {
            var items = group.Item2;
            foreach (var item in items)
                item.OperatorId = operators.Where(i => i.Name.ClearSpecialChars().Trim().FirstCharacterToUpper().Equals(item.Operator.ClearSpecialChars().Trim().FirstCharacterToUpper())).Select(i => i.Id).FirstOrDefault();

            if (items.Count > 1)
            {
                var portabilities = items.Take(items.Count - 1).ToList();
                foreach (var portability in portabilities)
                {
                    var existingPortability = _GetExistingPortability(existingPortabilities, portability);
                    if (existingPortability == null)
                    {
                        if (portability.Id > 0)
                            new CompanyLineOperator { CompanyLineId = portability.Id, OperatorId = portability.OperatorId, OperatorSince = portability.DateSince }.SetStateAdded(context);
                    }
                }
            }
            _ConsolidateGroupCurrentOperator(items.LastOrDefault(), existingLines, context);
        }

        CompanyLineOperator _GetExistingPortability(List<CompanyLineOperator> existingPortabilities, CompanyLineInfo portability)
        {
            var portabilities = existingPortabilities.Where(i => i.CompanyLineId.Equals(portability.Id) && i.OperatorId.Equals(portability.OperatorId)).ToList();
            if (portabilities?.Count > 0)
            {
                foreach (var existingPortability in portabilities)
                {
                    if (existingPortability.OperatorSince.HasValue && portability.DateSince.HasValue && portability.DateSince.Value.Date.Equals(existingPortability.OperatorSince.Value.Date))
                        return existingPortability;
                    else if (!existingPortability.OperatorSince.HasValue && !portability.DateSince.HasValue)
                        return existingPortability;
                }
            }

            return null;
        }
        void _ConsolidateGroupCurrentOperator(CompanyLineInfo companyLineInfo, List<CompanyLine> existingLines, DataContext context)
        {
            var existingLine = existingLines.FirstOrDefault(i => i.Id.Equals(companyLineInfo.Id));
            if (existingLine != null)
            {
                existingLine.OperatorId = companyLineInfo.OperatorId;
                existingLine.TestingTimestamp = DateTime.Now;
                if (companyLineInfo.DateSince.HasValue)
                    existingLine.OperatorSince = companyLineInfo.DateSince.Value;

                existingLine.SetStateModified(context);
            }
        }

        List<CompanyLine> _GetNext(DataContext context)
        {
            var lines = _PriorityCompanies(context);
            if ((lines?.Count ?? 0) == 0)
                lines = context.GetDbSet<CompanyLine>().Where(i => !i.TestingTimestamp.HasValue).GetRandomRows(300);
            if ((lines?.Count ?? 0) == 0)
            {
                var fifteenDaysAgo = DateTime.Now.AddDays(-10);
                lines = context.GetDbSet<CompanyLine>().Where(i => i.TestingTimestamp.Value.Date < fifteenDaysAgo.Date).GetRandomRows(300);
            }

            lines = _ValidateLines(lines, context);

            return lines;
        }
        List<CompanyLine> _ValidateLines(List<CompanyLine> lines, DataContext context)
        {
            var linesWithProblem = lines.Where(i => i.IsProblematic).ToList();
            var linesToRemove = new List<CompanyLine>();
            foreach (var item in linesWithProblem)
            {
                item.Phone = item.Phone.DigitsOnly();

                if (!_ItemIsValid(item, context))
                    linesToRemove.Add(item);
            }

            foreach (var item in linesToRemove)
            {
                lines.Remove(item);
                linesWithProblem.Remove(item);
            }

            context.RemoveRange(linesToRemove);
            context.UpdateRange(linesWithProblem);
            context.SaveChanges();

            var invalidLines = lines.Where(i => i.IsInvalid).ToList();
            var validLines = lines.Where(i => !invalidLines.Contains(i)).ToList();
            context.RemoveRange(invalidLines);
            context.SaveChanges();

            return validLines.Distinct().ToList();
        }
        bool _ItemIsValid(CompanyLine item, DataContext context)
        {
            var existingItem = context.GetDbSet<CompanyLine>().Where(i => i.StringValue.Equals(item.StringValue)).ToList();
            return existingItem?.Count == 1;
        }

        List<CompanyLine> _PriorityCompanies(DataContext context)
        {
            var priorities = context.GetDbSet<CompanyCheckRequest>().Where(i => i.CheckType.Equals((int)CompanyCheckTypeEnum.Abr)).GetRandomRows(25);
            if (priorities?.Count > 0)
            {
                var companyIds = priorities.Select(i => i.CompanyId).ToList();
                context.RemoveRange(priorities);
                context.SaveChanges();

                return context.GetDbSet<CompanyLine>().Where(i => companyIds.Contains(i.CompanyId)).ToList();
            }

            return null;
        }
    }
}