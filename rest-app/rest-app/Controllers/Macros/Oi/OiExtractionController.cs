﻿using System;
using System.Linq;
using RestApi.Database;
using RestApi.Entities;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Clericuzzi.WebApi.Entities;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Clericuzzi.Lib.Http.Utils.Constants;
using Clericuzzi.WebApi.Entities.Business;
using Clericuzzi.WebApi.Data.DatabaseContext;
using Clericuzzi.WebApi.Entities.Business.Enums;
using Clericuzzi.Lib.Http.Communication.Request;
using Clericuzzi.Lib.Http.Communication.Response;
using Clericuzzi.WebApi.Entities.Controllers.Crud;
using Clericuzzi.WebApi.Entities.Business.Managers;
using Clericuzzi.Lib.Http.Communication.Response.Json;
using Clericuzzi.WebApi.Data.Business.DatabaseContext;
using Clericuzzi.WebApi.Entities.Business.Models.Crud;
using Clericuzzi.Lib.Http.Communication.Request.Pagination;
using Clericuzzi.Lib.Utils.Core;

namespace RestApi.Controllers.Macros.Oi
{
    public class OiExtractionController : Controller
    {
        /// <summary>
        /// default update method
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/extraction/oi")]
        [HttpPost()]
        public JsonResult ExtractionOi([FromBody] RequestMessage request)
        {
            var response = new StatusResponse();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                var fileBase64 = request.DeserializeTo<string>("file", compressed: compressed, mandatory: false);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                {
                    var filename = $"content/{RandomStrings.RandomString(12)}.xlsx";
                    fileBase64.WriteFromBase64(fileBase64);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
    }
}
