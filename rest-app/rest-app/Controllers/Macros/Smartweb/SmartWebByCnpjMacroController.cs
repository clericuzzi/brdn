﻿using System;
using System.Linq;
using RestApi.Entities;
using RestApi.Database;
using System.Threading.Tasks;
using RestApi.Business.Enums;
using RestApi.Business.Models;
using Microsoft.AspNetCore.Mvc;
using Clericuzzi.Lib.Utils.Core;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Clericuzzi.WebApi.Data.DatabaseContext;
using Clericuzzi.Lib.Http.Communication.Request;
using Clericuzzi.Lib.Http.Communication.Response;
using Clericuzzi.WebApi.Entities.Business.Managers;
using Clericuzzi.WebApi.Analytics.Business.Managers;
using Clericuzzi.Lib.Http.Communication.Response.Json;
using Clericuzzi.WebApi.Data.Business.DatabaseContext;

namespace RestApi.Controllers.Macros.Smartweb
{
    public class SmartWebByCnpjMacroController : Controller
    {
        static string ACTION = ActionsEnum.SmartWebByCnpj.GetDescription();

        /// <summary>
        /// lists the characteristics associated with the given entity id
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/macro/smart-by-cnpj-source")]
        [HttpPost()]
        public async Task<JsonResult> Source([FromBody] RequestMessage request)
        {
            var response = new SingleResponse<Company>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                    response.Entity = await _GetNext(machineName, context);
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
        /// <summary>
        /// lists the characteristics associated with the given entity id
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/macro/smart-by-cnpj-clear")]
        [HttpPost()]
        public async Task<JsonResult> Clear([FromBody] RequestMessage request)
        {
            var response = new SingleResponse<Company>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                {
                    AnalyticsManager.Add((int)AnalyticsEnum.MiningActivityErrorSmartWebCnpj, context, 1);
                    await ConcurrencyManager.Clear(ACTION, null, machineName, context);
                    await context.SaveChangesAsync();

                    response.Entity = await _GetNext(machineName, context);
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }

        /// <summary>
        /// lists the characteristics associated with the given entity id
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/macro/smart-by-cnpj-consolidate")]
        [HttpPost()]
        public async Task<JsonResult> ByZipAndNumberConsolidate([FromBody] RequestMessage request)
        {
            var response = new SingleResponse<Company>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                var companyId = request.DeserializeTo<int>("company-id", compressed: compressed);
                var contactsInfo = request.DeserializeTo<List<CompanyContactInfo>>("items", compressed: compressed);
                var activeServices = request.DeserializeTo<List<CompanyActiveService>>("services", compressed: compressed);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                {
                    await ConcurrencyManager.Clear(ACTION, null, machineName, context);
                    await _Consolidate(companyId, contactsInfo, activeServices, context);

                    response.Entity = await _GetNext(machineName, context);
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }

        static async Task _Consolidate(int companyId, List<CompanyContactInfo> contactsInfo, List<CompanyActiveService> activeServices, DataContext context)
        {
            var existingCompany = await context.GetDbSet<Company>().FirstOrDefaultAsync(i => i.Id.Equals(companyId));
            if (existingCompany != null)
            {
                var existingServices = context.GetDbSet<CompanyActiveService>().Where(i => i.CompanyId.Equals(companyId)).ToList();
                if (existingServices?.Count > 0)
                {
                    context.RemoveRange(existingServices);
                    context.SaveChanges();
                }

                if (activeServices?.Count > 0)
                {
                    activeServices.ForEach(i => i.CompanyId = companyId);
                    context.AddRange(activeServices);
                    context.SaveChanges();
                }

                var companyLines = context.GetDbSet<CompanyLine>().Where(i => i.CompanyId.Equals(companyId)).ToList();
                var companyContacts = context.GetDbSet<CompanyContact>().Where(i => i.CompanyId.Equals(companyId)).ToList();

                await _ConsolidateLines(companyId, contactsInfo, companyLines, context);
                await _ConsolidateContacts(companyId, contactsInfo, companyContacts, context);
                await _ConsolidateContactLines(companyId, companyLines, companyContacts, contactsInfo, context);

                AnalyticsManager.Add((int)AnalyticsEnum.MiningActivitySuccessSmartWebCnpj, context, 1, companyId);
                await context.SaveChangesAsync();
            }
        }
        static async Task _ConsolidateLines(int companyId, List<CompanyContactInfo> contactsInfo, List<CompanyLine> companyLines, DataContext context)
        {
            foreach (var companyLine in companyLines)
                if (string.IsNullOrEmpty(companyLine.Phone) || companyLine.Phone.Length < 10 || companyLine.Phone.Length > 11)
                    companyLine.SetStateDeleted(context);
            context.SaveChanges();
            companyLines = companyLines.Where(i => !string.IsNullOrEmpty(i.Phone) && i.Phone.Length >= 10 && i.Phone.Length <= 11).ToList();

            var existingLines = companyLines.Select(i => i.Phone).ToList();

            var newPhones = new List<string>();
            contactsInfo.ForEach(i => i.Phones.ForEach(j => newPhones.Add(j)));
            var newLines = newPhones.Where(i => !existingLines.Contains(i)).ToList();
            if (newLines?.Count > 0)
            {
                var newCompanyLines = new List<CompanyLine>();
                foreach (var newLine in newLines)
                    newCompanyLines.Add(new CompanyLine { CompanyId = companyId, Phone = newLine });

                context.AddRange(newCompanyLines);
                await context.SaveChangesAsync();

                companyLines.AddRange(newCompanyLines);
            }
        }
        static async Task _ConsolidateContacts(int companyId, List<CompanyContactInfo> contactsInfo, List<CompanyContact> companyContacts, DataContext context)
        {
            var existingcontacts = companyContacts.Select(i => i.Name).Distinct().ToList();
            var newContactsByContact = contactsInfo.Where(i => !existingcontacts.Contains(i.Name)).ToList();

            var existingEmails = companyContacts.Select(i => i.Email).Distinct().ToList();
            var newContactsByEmail = contactsInfo.Where(i => !existingEmails.Contains(i.Email)).ToList();

            var newContacts = new List<CompanyContactInfo>();
            newContacts.AddRange(newContactsByEmail);
            newContacts.AddRange(newContactsByContact);
            newContacts = newContacts.Distinct().ToList();
            if (newContacts?.Count > 0)
            {
                var newCompanyContacts = new List<CompanyContact>();
                newContacts.ForEach(i => newCompanyContacts.Add(new CompanyContact { CompanyId = companyId, DepartmentId = 3, PositionId = 5, Email = i.Email, Name = i.Name, Timestamp = DateTime.Now, UserId = 51 }));

                context.AddRange(newCompanyContacts);
                await context.SaveChangesAsync();

                companyContacts.AddRange(newCompanyContacts);
            }
        }
        static async Task _ConsolidateContactLines(int companyId, List<CompanyLine> companyLines, List<CompanyContact> companyContacts, List<CompanyContactInfo> contactsInfo, DataContext context)
        {
            var currentRelationships = new List<CompanyContactLine>();
            foreach (var info in contactsInfo)
            {
                var currentContact = companyContacts.FirstOrDefault(i => i.Name.Equals(info.Name) || i.Email.Equals(info.Email));
                foreach (var phone in info.Phones)
                {
                    var currentLine = companyLines.FirstOrDefault(i => phone.Equals(i.Phone));
                    if (currentContact != null && currentLine != null)
                        currentRelationships.Add(new CompanyContactLine { CompanyContactId = currentContact.Id, CompanyLineId = currentLine.Id });
                }
            }

            currentRelationships = currentRelationships.Distinct().ToList();

            var existingRelationships = await context.GetDbSet<CompanyContactLine>().Where(i => i.CompanyContactIdParent.CompanyId.Equals(companyId)).ToListAsync();
            var newRelationships = currentRelationships.Where(i => !existingRelationships.Contains(i)).ToList();
            if (newRelationships?.Count > 0)
                context.AddRange(newRelationships);
        }
        async Task<Company> _GetNext(string machineName, DataContext context)
        {
            Company company = null;
            int? currentId = await ConcurrencyManager.GetId(ACTION, null, machineName, context);
            if (currentId.HasValue)
                company = await context.GetDbSet<Company>().FirstOrDefaultAsync(i => i.Id.Equals(currentId));
            else
            {
                company = _PriorityCompanies(context);
                if (company == null)
                {
                    var busyItems = await ConcurrencyManager.GetIds(ACTION, context);
                    var untestedCompanies = context.GetDbSet<Company>().Where(i => string.IsNullOrEmpty(i.FormalName) && !string.IsNullOrEmpty(i.Cnpj)).Select(i => i.Id).ToList();

                    untestedCompanies = untestedCompanies.Where(i => !busyItems.Contains(i)).Distinct().ToList();
                    currentId = untestedCompanies.GetRandomItem();
                    company = await context.GetDbSet<Company>().FirstOrDefaultAsync(i => i.Id.Equals(currentId));

                    await ConcurrencyManager.StoreId(company.Id, ACTION, null, machineName, context);
                }
            }

            return company;
        }
        Company _PriorityCompanies(DataContext context)
        {
            var priority = context.GetDbSet<CompanyCheckRequest>().Where(i => i.CheckType.Equals((int)CompanyCheckTypeEnum.SmartCnpj)).GetRandomRow();
            if (priority != null)
            {
                priority.SetStateDeleted(context);
                return context.GetDbSet<Company>().Where(i => i.Id.Equals(priority.CompanyId)).GetRandomRow();
            }

            return null;
        }
    }
}
