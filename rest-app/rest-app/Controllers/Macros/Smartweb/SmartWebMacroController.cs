﻿using System;
using System.Linq;
using RestApi.Entities;
using RestApi.Database;
using Clericuzzi.WebApi.Geo;
using System.Threading.Tasks;
using RestApi.Business.Enums;
using Microsoft.AspNetCore.Mvc;
using RestApi.Business.Managers;
using Clericuzzi.Lib.Utils.Core;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Clericuzzi.WebApi.Data.DatabaseContext;
using Clericuzzi.Lib.Http.Communication.Request;
using Clericuzzi.Lib.Http.Communication.Response;
using Clericuzzi.WebApi.Entities.Business.Managers;
using Clericuzzi.WebApi.Analytics.Business.Managers;
using Clericuzzi.WebApi.Data.Business.DatabaseContext;
using Clericuzzi.Lib.Http.Communication.Response.Json;

namespace RestApi.Controllers.Macros.Smartweb
{
    public class SmartWebMacroController : Controller
    {
        static string ACTION = ActionsEnum.SmartWebByZipAndNumber.GetDescription();

        /// <summary>
        /// lists the characteristics associated with the given entity id
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/macro/by-zip-and-number")]
        [HttpPost()]
        public async Task<JsonResult> ByZipAndNumber([FromBody] RequestMessage request)
        {
            var response = new SingleResponse<CompanyAddress>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                    response.Entity = await _GetNext(machineName, context);
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
        /// <summary>
        /// lists the characteristics associated with the given entity id
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/macro/by-zip-and-number-clear")]
        [HttpPost()]
        public async Task<JsonResult> ByZipAndNumberClear([FromBody] RequestMessage request)
        {
            var response = new SingleResponse<CompanyAddress>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                {
                    AnalyticsManager.Add((int)AnalyticsEnum.MiningActivityErrorSmartWeb, context);
                    await ConcurrencyManager.Clear(ACTION, null, machineName, context);

                    response.Entity = await _GetNext(machineName, context);
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
        /// <summary>
        /// lists the characteristics associated with the given entity id
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/macro/by-zip-and-number-consolidate")]
        [HttpPost()]
        public async Task<JsonResult> ByZipAndNumberConsolidate([FromBody] RequestMessage request)
        {
            var response = new SingleResponse<CompanyAddress>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                var skip = request.DeserializeTo<bool>("skip", compressed: compressed);
                var shouldDelete = request.DeserializeTo<bool>("should-delete", compressed: compressed);

                var uf = request.DeserializeTo<string>("uf", compressed: compressed);
                var zip = request.DeserializeTo<string>("zip", compressed: compressed);
                var city = request.DeserializeTo<string>("city", compressed: compressed);
                var number = request.DeserializeTo<string>("number", compressed: compressed);
                var neighbourhood = request.DeserializeTo<string>("neighbourhood", compressed: compressed);
                var availability = request.DeserializeTo<CompanyAvailability>("availability", compressed: compressed);
                var comanyAddressId = request.DeserializeTo<int>("company-address-id", compressed: compressed);

                using (var context = AppDatabaseMysql.CreateConnection(true))
                {
                    await ConcurrencyManager.Clear(ACTION, null, machineName, context);
                    context.SaveChanges();
                }

                using (var context = AppDatabaseMysql.CreateConnection(true))
                {
                    var existingCompanyAddress = await context.GetDbSet<CompanyAddress>().FirstOrDefaultAsync(i => i.Id.Equals(availability.CompanyAddressId));
                    if (shouldDelete)
                    {
                        if (existingCompanyAddress != null)
                        {
                            existingCompanyAddress.IsValid = 0;
                            AnalyticsManager.Add((int)AnalyticsEnum.VtCheckInvalidAddress, context, 1, existingCompanyAddress.CompanyId);
                        }
                    }
                    else
                    {
                        _ValidateZip(zip, uf, city, neighbourhood, context);
                        if (!skip)
                            await _ConsolidateZipAndNumber(availability, context);

                        existingCompanyAddress.IsValid = 1;

                        AnalyticsManager.Add((int)AnalyticsEnum.MiningActivitySuccessSmartWeb, context, 1, existingCompanyAddress.CompanyId);
                    }
                    await _ConsolidateAddressTestingTime(comanyAddressId, number, context);

                    response.Entity = await _GetNext(machineName, context);
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
        void _ValidateZip(string zip, string stateName, string cityName, string neighbourhoodName, DataContext context)
        {
            var existingZip = context.GetDbSet<GeographyZipcode>().Where(i => i.Zipcode.Equals(zip)).FirstOrDefault();
            if (existingZip == null)
            {
                var states = context.GetDbSet<GeographyState>().ToList();
                var state = states.Where(i => i.Abbreviation.ToLowerInvariant().Equals(stateName?.ToLowerInvariant())).FirstOrDefault();
                if (state == null)
                    return;

                var cities = context.GetDbSet<GeographyCity>().Where(i => i.StateId.Equals(state.Id)).ToList();
                var city = cities.Where(i => i.Name.ClearSpecialChars(toLower: true).Equals(cityName.ClearSpecialChars(toLower: true))).FirstOrDefault();
                if (city == null && !string.IsNullOrEmpty(cityName))
                {
                    city = new GeographyCity() { StateId = state.Id, Name = cityName.ClearSpecialChars(toLower: true).Trim() };
                    city.SetStateAdded(context);
                    context.SaveChanges();
                }

                var neighbourhoods = context.GetDbSet<GeographyNeighbourhood>().Where(i => i.CityId.Equals(city.Id)).ToList();
                var neighbourhood = neighbourhoods.Where(i => i.Name.ClearSpecialChars(toLower: true).Equals(neighbourhoodName.ClearSpecialChars(toLower: true))).FirstOrDefault();
                if (neighbourhood == null && !string.IsNullOrEmpty(neighbourhoodName))
                {
                    neighbourhood = new GeographyNeighbourhood() { CityId = city.Id, Name = neighbourhoodName.ClearSpecialChars(toLower: true).Trim() };
                    neighbourhood.SetStateAdded(context);
                    context.SaveChanges();
                }

                existingZip = new GeographyZipcode() { CityId = city?.Id, NeighbourhoodId = neighbourhood?.Id, Zipcode = zip };
                existingZip.SetStateAdded(context);
                context.SaveChanges();
            }

            if (existingZip != null)
            {
                var formattedZip = $"{zip.Substring(0, 5)}-{zip.Substring(5)}";
                var mobileZip = $"CEP: {zip.Substring(0, 5)}{zip.Substring(5)}";
                var existingZipAddresses = context.GetDbSet<CompanyAddress>().Where(i => i.Street.Contains(formattedZip) || i.Street.Contains(mobileZip)).ToList();
                existingZipAddresses = existingZipAddresses.Where(i => !i.ZipcodeId.HasValue).ToList();
                if (existingZipAddresses.Count > 0)
                {
                    existingZipAddresses.ForEach(i => i.ZipcodeId = existingZip.Id);
                    context.UpdateRange(existingZipAddresses);
                    context.SaveChanges();
                }
            }
        }

        async Task _ConsolidateZipAndNumber(CompanyAvailability availability, DataContext context)
        {
            var existingAvailability = await context.GetDbSet<CompanyAvailability>().FirstOrDefaultAsync(i => i.CompanyAddressId.Equals(availability.CompanyAddressId));
            if (existingAvailability == null)
            {
                availability.Timestamp = DateTime.Now;

                availability.SetStateAdded(context);
            }
            else
            {
                existingAvailability.Lines = availability.Lines;
                existingAvailability.Over50 = availability.Over50;
                existingAvailability.Message = availability.Message;
                existingAvailability.Network = availability.Network;
                existingAvailability.Until20 = availability.Until20;
                existingAvailability.Tecnology = availability.Tecnology;
                existingAvailability.Timestamp = availability.Timestamp;
                existingAvailability.From21UpTo50 = availability.From21UpTo50;
                existingAvailability.TecnologyForTv = availability.TecnologyForTv;
                existingAvailability.ClosetDistance = availability.ClosetDistance;
                existingAvailability.ClosetTopSpeed = availability.ClosetTopSpeed;
                existingAvailability.CompanyAddressId = availability.CompanyAddressId;
                existingAvailability.CompanyAddressIdParent = availability.CompanyAddressIdParent;

                existingAvailability.SetStateModified(context);
            }
            context.SaveChanges();
        }
        async Task _ConsolidateAddressTestingTime(int companyAddressId, string number, DataContext context)
        {
            var companyAddress = await context.GetDbSet<CompanyAddress>().FirstOrDefaultAsync(i => i.Id.Equals(companyAddressId));
            if (companyAddress != null)
            {
                if (!string.IsNullOrEmpty(number))
                    companyAddress.Number = number;
                companyAddress.TestingTimestamp = DateTime.Today;

                companyAddress.SetStateModified(context);
                context.SaveChanges();
            }
        }
        async Task<CompanyAddress> _GetNext(string machineName, DataContext context)
        {
            var untestedCompany = context.GetDbSet<CompanyAddress>().Include(i => i.CompanyIdParent).Where(i => !i.TestingTimestamp.HasValue).OrderByDescending(i => i.CompanyIdParent.Timestamp).Take(100).GetRandomRow();
            if (untestedCompany != null)
                return untestedCompany;
            else
            {
                var targetCompanies = context.GetDbSet<CompanyAddress>().Select(i => i.CompanyId).Distinct().ToList();
                var currentId = await MiningManager.NextMiningTarget(ACTION, machineName, (int)CompanyCheckTypeEnum.SmartZip, (int)AnalyticsEnum.MiningActivitySuccessSmartWeb, targetCompanies, context);

                var companyAddess = await context.GetDbSet<CompanyAddress>().Include(i => i.ZipcodeIdParent).Where(i => i.CompanyId.Equals(currentId)).OrderBy(i => i.TestingTimestamp).FirstOrDefaultAsync();
                return companyAddess;
            }
        }
    }
}
