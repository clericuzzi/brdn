﻿using System;
using System.Linq;
using RestApi.Entities;
using RestApi.Database;
using RestApi.Business.Enums;
using System.Threading.Tasks;
using RestApi.Business.Models;
using Microsoft.AspNetCore.Mvc;
using Clericuzzi.Lib.Utils.Core;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Clericuzzi.WebApi.Data.DatabaseContext;
using Clericuzzi.Lib.Http.Communication.Request;
using Clericuzzi.Lib.Http.Communication.Response;
using Clericuzzi.WebApi.Entities.Business.Managers;
using Clericuzzi.WebApi.Analytics.Business.Managers;
using Clericuzzi.WebApi.Data.Business.DatabaseContext;
using Clericuzzi.Lib.Http.Communication.Response.Json;

namespace RestApi.Controllers.Macros.Google
{
    public class CattaMiningController : Controller
    {
        static string ACTION = ActionsEnum.MiningGoogle.GetDescription();

        /// <summary>
        /// receives the mined companies
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/macro/mining-catta-send")]
        [HttpPost()]
        public async Task<JsonResult> Send([FromBody] RequestMessage request)
        {
            var response = new StatusResponse();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                var companies = request.DeserializeTo<List<CompanyItemMacro>>("companies", compressed: compressed);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                    await _ReceiveCompanies(companies, context);

                response.Success = true;
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
        /// <summary>
        /// lists the characteristics associated with the given entity id
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/macro/mining-catta-source")]
        [HttpPost()]
        public async Task<JsonResult> Source([FromBody] RequestMessage request)
        {
            var response = new SingleResponse<MiningGoogle>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                    response.Entity = await _GetNext(machineName, context);
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
        /// <summary>
        /// lists the characteristics associated with the given entity id
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/macro/mining-catta-clear")]
        [HttpPost()]
        public async Task<JsonResult> Clear([FromBody] RequestMessage request)
        {
            var response = new SingleResponse<MiningGoogle>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                {
                    AnalyticsManager.Add((int)AnalyticsEnum.MiningActivityErrorCatta, context);
                    await ConcurrencyManager.Clear(ACTION, null, machineName, context);

                    response.Entity = await _GetNext(machineName, context);
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
        /// <summary>
        /// lists the characteristics associated with the given entity id
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/macro/mining-catta-consolidate")]
        [HttpPost()]
        public async Task<JsonResult> Consolidate([FromBody] RequestMessage request)
        {
            var response = new SingleResponse<MiningGoogle>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                var miningId = request.DeserializeTo<int>("mining-id", compressed: compressed);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                {
                    await ConcurrencyManager.Clear(ACTION, null, machineName, context);
                    var existingMiningEntity = await context.GetDbSet<MiningGoogle>().FirstOrDefaultAsync(i => i.Id.Equals(miningId));
                    if (existingMiningEntity != null)
                    {
                        existingMiningEntity.SetStateDeleted(context);
                        await context.SaveChangesAsync();
                    }

                    response.Entity = await _GetNext(machineName, context);
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }

        async Task _ReceiveCompanies(List<CompanyItemMacro> companies, DataContext context)
        {
            var descriptions = companies.Select(i => i.Company.Description).Distinct().ToList();
            var existingCompanies = await context.GetDbSet<Company>().Where(i => descriptions.Contains(i.Description)).ToListAsync();
            var newCompanies = companies.Where(i => !existingCompanies.Contains(i.Company)).ToList();
            if (newCompanies.Count > 0)
            {
                foreach (var newCompany in newCompanies)
                {
                    var address = newCompany.Address;
                    var company = newCompany.Company;
                    await company.NewCompany(address, context);
                }
            }
        }
        async Task<MiningGoogle> _GetNext(string machineName, DataContext context)
        {
            MiningGoogle entity = null;
            var currentId = await ConcurrencyManager.GetId(ACTION, null, machineName, context);
            if (currentId.HasValue)
                entity = await context.GetDbSet<MiningGoogle>().Include(i => i.CityIdParent.StateIdParent).Include(i => i.CompanySubtypeIdParent).FirstOrDefaultAsync(i => i.Id.Equals(currentId));
            else
            {
                var busyItems = await ConcurrencyManager.GetIds(ACTION, context);
                entity = (await context.GetDbSet<MiningGoogle>().Include(i => i.CityIdParent.StateIdParent).Include(i => i.CompanySubtypeIdParent).Where(i => !busyItems.Contains(i.Id)).ToListAsync()).GetRandomItem();

                await ConcurrencyManager.StoreId(entity.Id, ACTION, null, machineName, context);
            }

            return entity;
        }
    }
}