﻿using System;
using System.Linq;
using RestApi.Entities;
using RestApi.Database;
using System.Threading.Tasks;
using RestApi.Business.Enums;
using RestApi.Business.Models;
using Microsoft.AspNetCore.Mvc;
using Clericuzzi.Lib.Utils.Core;
using System.Collections.Generic;
using Clericuzzi.WebApi.Data.DatabaseContext;
using Clericuzzi.Lib.Http.Communication.Request;
using Clericuzzi.Lib.Http.Communication.Response;
using Clericuzzi.WebApi.Entities.Business.Managers;
using Clericuzzi.WebApi.Analytics.Business.Managers;
using Clericuzzi.Lib.Http.Communication.Response.Json;
using Clericuzzi.WebApi.Data.Utils;

namespace RestApi.Controllers.Macros.Dnc.MaisInfo
{
    public class MacroDncMaisInfoController : Controller
    {
        static string ACTION = ActionsEnum.MaisInfo.GetDescription();

        /// <summary>
        /// lists the characteristics associated with the given entity id
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/macro/dnc/mais-info/source")]
        [HttpPost()]
        public async Task<JsonResult> Source([FromBody] RequestMessage request)
        {
            var response = new SingleResponse<List<DncMaisInfoModel>>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                    response.Entity = _GetNext(machineName, context);
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
        /// <summary>
        /// lists the characteristics associated with the given entity id
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/macro/dnc/mais-info/clear")]
        [HttpPost()]
        public async Task<JsonResult> Clear([FromBody] RequestMessage request)
        {
            var response = new SingleResponse<List<DncMaisInfoModel>>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                {
                    AnalyticsManager.Add((int)AnalyticsEnum.MiningCnpjCheckError, context);
                    await ConcurrencyManager.Clear(ACTION, null, machineName, context);

                    response.Entity = _GetNext(machineName, context);
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }

        /// <summary>
        /// lists the characteristics associated with the given entity id
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/macro/dnc/mais-info/consolidate")]
        [HttpPost()]
        public async Task<JsonResult> Consolidate([FromBody] RequestMessage request)
        {
            var response = new SingleResponse<List<DncMaisInfoModel>>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                var linesInfo = request.DeserializeTo<List<DncMaisInfoModel>>("lines-info", crypted, compressed);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                {
                    AnalyticsManager.Add((int)AnalyticsEnum.MiningCnpjCheckSuccess, context);
                    context.SaveChanges();
                    _Consolidate(linesInfo, context);

                    response.Entity = _GetNext(machineName, context);
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }

        void _Consolidate(List<DncMaisInfoModel> linesInfo, DataContext context)
        {
            var lineIds = linesInfo.Select(i => i.CompanyLineId).ToList();
            var existingLines = context.GetDbSet<CompanyLine>().Where(i => lineIds.Contains(i.Id)).ToList();
            foreach (var existingLine in existingLines)
            {
                var currentLineInfo = linesInfo.FirstOrDefault(i => i.CompanyLineId.Equals(existingLine.Id));
                if (currentLineInfo != null)
                {
                    existingLine.TestedTodayDnc = 1;
                    existingLine.IsValid = currentLineInfo.CanCall >= 1 ? 1 : 0;
                }
            }

            context.UpdateRange(existingLines);
            context.SaveChanges();
        }
        List<DncMaisInfoModel> _GetNext(string machineName, DataContext context, bool fromRecursion = false)
        {
            var numbers = context.GetDbSet<CompanyLine>().Where(i => i.TestedTodayDnc.Equals(0)).Select(i => new DncMaisInfoModel { CompanyLineId = i.Id, Phone = i.Phone, CanCall = 0 }).GetRandomRows(500);
            numbers = numbers.Where(i => i.Valid).ToList();
            if (numbers.Count > 0 || fromRecursion)
                return numbers;
            else
            {
                DataUtils.RunNonQuery(context, "update `company_line` set `tested_today_dnc` = 0;");
                return _GetNext(machineName, context, true);
            }
        }
    }
}
