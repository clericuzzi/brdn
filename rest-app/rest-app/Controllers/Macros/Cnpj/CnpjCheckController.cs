﻿using System;
using System.Linq;
using RestApi.Entities;
using RestApi.Database;
using System.Threading.Tasks;
using RestApi.Business.Enums;
using RestApi.Business.Models;
using Microsoft.AspNetCore.Mvc;
using RestApi.Controllers.Site;
using RestApi.Business.Managers;
using Clericuzzi.Lib.Utils.Core;
using System.Collections.Generic;
using Clericuzzi.WebApi.Data.DatabaseContext;
using Clericuzzi.Lib.Http.Communication.Request;
using Clericuzzi.Lib.Http.Communication.Response;
using Clericuzzi.WebApi.Entities.Business.Managers;
using Clericuzzi.WebApi.Analytics.Business.Managers;
using Clericuzzi.Lib.Http.Communication.Response.Json;

namespace RestApi.Controllers.Macros.Cnpj
{
    public class CnpjCheckController : Controller
    {
        static string ACTION = ActionsEnum.CnpjCheck.GetDescription();

        /// <summary>
        /// lists the characteristics associated with the given entity id
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/macro/cnpj-check/source")]
        [HttpPost()]
        public async Task<JsonResult> Source([FromBody] RequestMessage request)
        {
            var response = new SingleResponse<Company>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                    response.Entity = await _GetNext(machineName, context);
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
        /// <summary>
        /// lists the characteristics associated with the given entity id
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/macro/cnpj-check/clear")]
        [HttpPost()]
        public async Task<JsonResult> Clear([FromBody] RequestMessage request)
        {
            var response = new SingleResponse<Company>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                {
                    AnalyticsManager.Add((int)AnalyticsEnum.MiningCnpjCheckError, context);
                    await ConcurrencyManager.Clear(ACTION, null, machineName, context);

                    response.Entity = await _GetNext(machineName, context);
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
        /// <summary>
        /// lists the characteristics associated with the given entity id
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/macro/cnpj-check/consolidate")]
        [HttpPost()]
        public async Task<JsonResult> Consolidate([FromBody] RequestMessage request)
        {
            var response = new SingleResponse<Company>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                var companyId = request.DeserializeTo<int>("company-id", crypted, compressed);
                var companyInfo = request.DeserializeTo<CompanyInfo>("company-info", crypted, compressed);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                {
                    await ConcurrencyManager.Clear(ACTION, null, machineName, context);
                    if (companyInfo != null)
                    {
                        var existingCompany = context.GetDbSet<Company>().FirstOrDefault(i => i.Id.Equals(companyId));
                        if (existingCompany != null)
                            SiteCompanyController.UpdateCompany(existingCompany, companyInfo, companyId, context);

                        AnalyticsManager.Add((int)AnalyticsEnum.MiningCnpjCheckSuccess, context, entityId: companyId);
                    }
                    context.SaveChanges();

                    response.Entity = await _GetNext(machineName, context);
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }

        async Task<Company> _GetNext(string machineName, DataContext context)
        {
            var miningScope = context.GetDbSet<Company>().Where(i => !string.IsNullOrEmpty(i.Cnpj)).Select(i => i.Id).Distinct().ToList();
            var companyId = await MiningManager.NextMiningTarget(ACTION, machineName, (int)CompanyCheckTypeEnum.Cnpj, (int)ActionsEnum.CnpjCheck, miningScope, context);

            return context.GetDbSet<Company>().FirstOrDefault(i => i.Id.Equals(companyId));
        }
    }
}
