using System;
using System.Linq;
using RestApi.Database;
using RestApi.Entities;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Clericuzzi.WebApi.Entities;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Clericuzzi.Lib.Http.Utils.Constants;
using Clericuzzi.WebApi.Entities.Business;
using Clericuzzi.WebApi.Data.DatabaseContext;
using Clericuzzi.WebApi.Entities.Business.Enums;
using Clericuzzi.Lib.Http.Communication.Request;
using Clericuzzi.Lib.Http.Communication.Response;
using Clericuzzi.WebApi.Entities.Controllers.Crud;
using Clericuzzi.WebApi.Entities.Business.Managers;
using Clericuzzi.Lib.Http.Communication.Response.Json;
using Clericuzzi.WebApi.Data.Business.DatabaseContext;
using Clericuzzi.WebApi.Entities.Business.Models.Crud;
using Clericuzzi.Lib.Http.Communication.Request.Pagination;

namespace RestApi.Entities.Controllers.Crud
{
    /// <summary>
    /// A class for default crud actions regarding the `company_callback` table
    /// </summary>
    public partial class CompanyCallbackCrudController : Controller
    {
        /// <summary>
        /// default listing method
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/crud/company_callback/listing")]
        [HttpPost()]
        public JsonResult Listing([FromBody] RequestMessage request)
        {
            var response = new BatchResponseList();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                var filter = request.DeserializeTo<CompanyCallbackFilter>("filter", compressed: compressed, mandatory: false);
                using (var context = AppDatabaseMysql.CreateConnection())
                {
                    var query = context.GetDbSet<CompanyCallback>().AsQueryable();
                    query = FilterResults(query, filter, context);
                    var items = query.Select(i => GetListing(i)).ToList();
                    items = items.OrderBy(i => i.Label).ToList();

                    response.AddEntityList(CompanyCallback.TABLE_NAME, items.ToList<object>());
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }

        /// <summary>
        /// default select method
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/crud/company_callback/filter")]
        [HttpPost()]
        public async Task<JsonResult> Filter([FromBody] RequestMessage request)
        {
            var response = new BatchResponse<CompanyCallback>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName, out RequestPagination pagination);
                var filter = request.DeserializeTo<CompanyCallbackFilter>("filter", compressed: compressed, mandatory: false);
                using (var context = AppDatabaseMysql.CreateConnection())
                {
                    var query = context.GetDbSet<CompanyCallback>().OrderBy(i => i.UserIdParent.Name).AsQueryable();
					query = FilterResults(query, filter, context);

                    response.Entities = pagination.Paginate(query);
                    response.Pagination = pagination;

                    await LoadCustomFields(response.Entities, context);
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }

        /// <summary>
        /// default insert method
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/crud/company_callback/insert")]
        [HttpPost()]
        public JsonResult Insert([FromBody] RequestMessage request)
        {
            var response = new BatchResponse<CompanyCallback>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                var model = request.DeserializeTo<CompanyCallback>("model", compressed: compressed, mandatory: false);
                var items = request.DeserializeTo<List<CompanyCallback>>(FieldConstants.ENTITIES, compressed: compressed, mandatory: false);
                
				if (model != null)
                    items = new List<CompanyCallback> { model };

                var existingItems = items.Where(i => i.Id > 0).ToList();
                var nonExintingItems = items.Where(i => i.Id <= 0).ToList();
                if (nonExintingItems?.Count > 0)
				{
					using (var context = AppDatabaseMysql.CreateConnection(true))
					{
						context.AddRange(nonExintingItems);
						foreach (var item in nonExintingItems)
							LogManager.Crud(userId, item.Summary, LogActionsEnum.Insert, CompanyCallback.CLASS_NAME, context);
					}
				}

                response.Entities = nonExintingItems;
                if (existingItems.Count > 0)
                    throw new Exception("Alguns itens foram ignorados por já existirem na base de dados");
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
        /// <summary>
        /// default delete method
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/crud/company_callback/delete")]
        [HttpPost()]
        public JsonResult Delete([FromBody] RequestMessage request)
        {
            var response = new StatusResponse();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                var ids = request.DeserializeTo<List<int>>(FieldConstants.ENTITIES, compressed: compressed);
				if (ids?.Count > 0)
					using (var context = AppDatabaseMysql.CreateConnection(true))
					{
						var items = context.GetDbSet<CompanyCallback>().Where(i => ids.Contains(i.Id)).ToList();
						if (items.Count > 0)
						{
							context.RemoveRange(items);
							foreach (var item in items)
								LogManager.Crud(userId, item.Summary, LogActionsEnum.Delete, CompanyCallback.CLASS_NAME, context);
						}
					}

                response.Success = true;
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
        /// <summary>
        /// default update method
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/crud/company_callback/update")]
        [HttpPost()]
        public JsonResult Update([FromBody] RequestMessage request)
        {
            var response = new StatusResponse();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                var model = request.DeserializeTo<CompanyCallback>("model", compressed: compressed, mandatory: false);
                var entities = request.DeserializeTo<List<CompanyCallback>>(FieldConstants.ENTITIES, compressed: compressed, mandatory: false);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                {
                    if (model != null)
                        entities = new List<CompanyCallback> { model };

                    var ids = entities.Select(i => i.Id).ToList();
                    var items = context.GetDbSet<CompanyCallback>().Where(i => ids.Contains(i.Id)).ToList();
                    foreach (var item in items)
                    {
                        var entity = entities.Where(i => i.Id.Equals(item.Id)).FirstOrDefault();
                        if (item != null && entity != null)
                        {
                            item.Id = entity.Id;
							item.Done = entity.Done;
							item.UserId = entity.UserId;
							item.CallTime = entity.CallTime;
							item.CompanyId = entity.CompanyId;
							item.ContactId = entity.ContactId;
							item.Timestamp = entity.Timestamp;

                            item.SetStateModified(context);
							LogManager.Crud(userId, entity.Summary, LogActionsEnum.Update, CompanyCallback.CLASS_NAME, context);
                        }
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
        /// <summary>
        /// default update property method
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/crud/company_callback/update_property")]
        [HttpPost()]
        public async Task<JsonResult> UpdateProperty([FromBody] RequestMessage request)
        {
            var response = new StatusResponse();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId);
                var entityId = request.DeserializeTo<int>(FieldConstants.ENTITY, compressed: compressed);
                var entityProperty = request.DeserializeTo<string>("property", compressed: compressed);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                {
                    var entity = await context.GetDbSet<CompanyCallback>().Where(i => i.Id.Equals(entityId)).FirstOrDefaultAsync();
                    if (entity == null)
                        throw new Exception("A entidade selecionada não existe mais no banco de dados...");

					if (entityProperty.Equals(CompanyCallback.PROPERTY_USER_ID))
					{
						var userIdValue = request.DeserializeTo<int>(CompanyCallback.PROPERTY_USER_ID, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyCallback.CLASS_NAME, CompanyCallback.PROPERTY_USER_ID, entity.UserId, userIdValue, context);

                        entity.UserId = userIdValue;
					}
					else if (entityProperty.Equals(CompanyCallback.PROPERTY_COMPANY_ID))
					{
						var companyIdValue = request.DeserializeTo<int>(CompanyCallback.PROPERTY_COMPANY_ID, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyCallback.CLASS_NAME, CompanyCallback.PROPERTY_COMPANY_ID, entity.CompanyId, companyIdValue, context);

                        entity.CompanyId = companyIdValue;
					}
					else if (entityProperty.Equals(CompanyCallback.PROPERTY_CONTACT_ID))
					{
						var contactIdValue = request.DeserializeTo<int>(CompanyCallback.PROPERTY_CONTACT_ID, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyCallback.CLASS_NAME, CompanyCallback.PROPERTY_CONTACT_ID, entity.ContactId, contactIdValue, context);

                        entity.ContactId = contactIdValue;
					}
					else if (entityProperty.Equals(CompanyCallback.PROPERTY_CALL_TIME))
					{
						var callTimeValue = request.DeserializeTo<DateTime>(CompanyCallback.PROPERTY_CALL_TIME, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyCallback.CLASS_NAME, CompanyCallback.PROPERTY_CALL_TIME, entity.CallTime, callTimeValue, context);

                        entity.CallTime = callTimeValue;
					}
					else if (entityProperty.Equals(CompanyCallback.PROPERTY_DONE))
					{
						var doneValue = request.DeserializeTo<int>(CompanyCallback.PROPERTY_DONE, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyCallback.CLASS_NAME, CompanyCallback.PROPERTY_DONE, entity.Done, doneValue, context);

                        entity.Done = doneValue;
					}
					else if (entityProperty.Equals(CompanyCallback.PROPERTY_TIMESTAMP))
					{
						var timestampValue = request.DeserializeTo<DateTime>(CompanyCallback.PROPERTY_TIMESTAMP, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyCallback.CLASS_NAME, CompanyCallback.PROPERTY_TIMESTAMP, entity.Timestamp, timestampValue, context);

                        entity.Timestamp = timestampValue;
					}
                    entity.SetStateModified(context);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
    }
}