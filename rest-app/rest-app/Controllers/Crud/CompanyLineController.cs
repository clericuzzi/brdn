using System;
using System.Linq;
using System.Threading.Tasks;
using RestApi.Business.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Clericuzzi.WebApi.Data.DatabaseContext;
using Clericuzzi.WebApi.Entities.Business.Models.Crud;

namespace RestApi.Entities.Controllers.Crud
{
    /// <summary>
    /// A class for project specific endpoints regarding the `company_line` table
    /// </summary>
    public partial class CompanyLineCrudController : Controller
    {
        /// <summary>
        /// Custom fields loader, implement only if needed
        /// </summary>
        /// <param name="items">the entity's list</param>
        /// <param name="context">database connection</param>
        /// <returns>laods the custom information into the given entities</returns>
        private async Task LoadCustomFields(List<LineItem> items, DataContext context)
        {
            await Task.Delay(150);

            var lineIds = items.Select(i => i.Id).ToList();
            var operators = context.GetDbSet<CompanyLineOperator>().Where(i => lineIds.Contains(i.CompanyLineId)).ToList();

            items.ForEach(i => i.Operators = operators.Where(j => j.CompanyLineId.Equals(i.Id)).ToList());
        }

        /// <summary>
        /// default listing for the 'CompanyLine' class 
        /// </summary>
        /// <param name="item">the current instance</param>
        /// <returns>a new tuple model for showing in a combo or auto complete fields</returns>
        private TupleModel<int> GetListing(CompanyLine item)
        {
            return new TupleModel<int> { Id = item.Id, Label = item.Summary };
        }

        /// <summary>
        /// custom filters for the 'CompanyLine' class
        /// </summary>
        /// <param name="query">the ongoing query</param>
        /// <param name="filter">the filter object</param>
        /// <param name="context">database connection</param>
        /// <returns>the query filtered by the filter object</returns>
        public static IQueryable<CompanyLine> FilterResults(IQueryable<CompanyLine> query, CompanyLineFilter filter, DataContext context)
        {
            try
            {
                if (filter == null)
                    return query;
                else
                {
                    if (!string.IsNullOrEmpty(filter?.Phone))
                        query = query.Where(i => i.Phone.Contains(filter.Phone));
                    if (filter?.CompanyId?.Count > 0)
                        query = query.Where(i => filter.CompanyId.Contains(i.CompanyId));
                    if (filter?.OperatorId?.Count > 0)
                        query = query.Where(i => filter.OperatorId.Contains(i.OperatorId.Value));

                    return query;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}