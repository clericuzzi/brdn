using System;
using System.Linq;
using RestApi.Database;
using RestApi.Entities;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Clericuzzi.WebApi.Entities;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Clericuzzi.Lib.Http.Utils.Constants;
using Clericuzzi.WebApi.Entities.Business;
using Clericuzzi.WebApi.Data.DatabaseContext;
using Clericuzzi.WebApi.Entities.Business.Enums;
using Clericuzzi.Lib.Http.Communication.Request;
using Clericuzzi.Lib.Http.Communication.Response;
using Clericuzzi.WebApi.Entities.Controllers.Crud;
using Clericuzzi.WebApi.Entities.Business.Managers;
using Clericuzzi.Lib.Http.Communication.Response.Json;
using Clericuzzi.WebApi.Data.Business.DatabaseContext;
using Clericuzzi.WebApi.Entities.Business.Models.Crud;
using Clericuzzi.Lib.Http.Communication.Request.Pagination;

namespace RestApi.Entities.Controllers.Crud
{
    /// <summary>
    /// A class for default crud actions regarding the `company_availability` table
    /// </summary>
    public partial class CompanyAvailabilityCrudController : Controller
    {
        /// <summary>
        /// default listing method
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/crud/company_availability/listing")]
        [HttpPost()]
        public JsonResult Listing([FromBody] RequestMessage request)
        {
            var response = new BatchResponseList();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                var filter = request.DeserializeTo<CompanyAvailabilityFilter>("filter", compressed: compressed, mandatory: false);
                using (var context = AppDatabaseMysql.CreateConnection())
                {
                    var query = context.GetDbSet<CompanyAvailability>().AsQueryable();
                    query = FilterResults(query, filter, context);
                    var items = query.Select(i => GetListing(i)).ToList();
                    items = items.OrderBy(i => i.Label).ToList();

                    response.AddEntityList(CompanyAvailability.TABLE_NAME, items.ToList<object>());
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }

        /// <summary>
        /// default select method
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/crud/company_availability/filter")]
        [HttpPost()]
        public async Task<JsonResult> Filter([FromBody] RequestMessage request)
        {
            var response = new BatchResponse<CompanyAvailability>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName, out RequestPagination pagination);
                var filter = request.DeserializeTo<CompanyAvailabilityFilter>("filter", compressed: compressed, mandatory: false);
                using (var context = AppDatabaseMysql.CreateConnection())
                {
                    var query = context.GetDbSet<CompanyAvailability>().OrderBy(i => i.CompanyAddressIdParent.CompanyIdParent.AccountIdParent.Cnpj).AsQueryable();
					query = FilterResults(query, filter, context);

                    response.Entities = pagination.Paginate(query);
                    response.Pagination = pagination;

                    await LoadCustomFields(response.Entities, context);
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }

        /// <summary>
        /// default insert method
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/crud/company_availability/insert")]
        [HttpPost()]
        public JsonResult Insert([FromBody] RequestMessage request)
        {
            var response = new BatchResponse<CompanyAvailability>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                var model = request.DeserializeTo<CompanyAvailability>("model", compressed: compressed, mandatory: false);
                var items = request.DeserializeTo<List<CompanyAvailability>>(FieldConstants.ENTITIES, compressed: compressed, mandatory: false);
                
				if (model != null)
                    items = new List<CompanyAvailability> { model };

                var existingItems = items.Where(i => i.Id > 0).ToList();
                var nonExintingItems = items.Where(i => i.Id <= 0).ToList();
                if (nonExintingItems?.Count > 0)
				{
					using (var context = AppDatabaseMysql.CreateConnection(true))
					{
						context.AddRange(nonExintingItems);
						foreach (var item in nonExintingItems)
							LogManager.Crud(userId, item.Summary, LogActionsEnum.Insert, CompanyAvailability.CLASS_NAME, context);
					}
				}

                response.Entities = nonExintingItems;
                if (existingItems.Count > 0)
                    throw new Exception("Alguns itens foram ignorados por já existirem na base de dados");
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
        /// <summary>
        /// default delete method
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/crud/company_availability/delete")]
        [HttpPost()]
        public JsonResult Delete([FromBody] RequestMessage request)
        {
            var response = new StatusResponse();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                var ids = request.DeserializeTo<List<int>>(FieldConstants.ENTITIES, compressed: compressed);
				if (ids?.Count > 0)
					using (var context = AppDatabaseMysql.CreateConnection(true))
					{
						var items = context.GetDbSet<CompanyAvailability>().Where(i => ids.Contains(i.Id)).ToList();
						if (items.Count > 0)
						{
							context.RemoveRange(items);
							foreach (var item in items)
								LogManager.Crud(userId, item.Summary, LogActionsEnum.Delete, CompanyAvailability.CLASS_NAME, context);
						}
					}

                response.Success = true;
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
        /// <summary>
        /// default update method
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/crud/company_availability/update")]
        [HttpPost()]
        public JsonResult Update([FromBody] RequestMessage request)
        {
            var response = new StatusResponse();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                var model = request.DeserializeTo<CompanyAvailability>("model", compressed: compressed, mandatory: false);
                var entities = request.DeserializeTo<List<CompanyAvailability>>(FieldConstants.ENTITIES, compressed: compressed, mandatory: false);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                {
                    if (model != null)
                        entities = new List<CompanyAvailability> { model };

                    var ids = entities.Select(i => i.Id).ToList();
                    var items = context.GetDbSet<CompanyAvailability>().Where(i => ids.Contains(i.Id)).ToList();
                    foreach (var item in items)
                    {
                        var entity = entities.Where(i => i.Id.Equals(item.Id)).FirstOrDefault();
                        if (item != null && entity != null)
                        {
                            item.Id = entity.Id;
							item.Lines = entity.Lines;
							item.Over50 = entity.Over50;
							item.Message = entity.Message;
							item.Until20 = entity.Until20;
							item.Network = entity.Network;
							item.Over50Max = entity.Over50Max;
							item.Over50Sip = entity.Over50Sip;
							item.Tecnology = entity.Tecnology;
							item.Timestamp = entity.Timestamp;
							item.Until20Max = entity.Until20Max;
							item.Until20Sip = entity.Until20Sip;
							item.From21UpTo50 = entity.From21UpTo50;
							item.ClosetDistance = entity.ClosetDistance;
							item.ClosetTopSpeed = entity.ClosetTopSpeed;
							item.TecnologyForTv = entity.TecnologyForTv;
							item.From21UpTo50Max = entity.From21UpTo50Max;
							item.From21UpTo50Sip = entity.From21UpTo50Sip;
							item.CompanyAddressId = entity.CompanyAddressId;

                            item.SetStateModified(context);
							LogManager.Crud(userId, entity.Summary, LogActionsEnum.Update, CompanyAvailability.CLASS_NAME, context);
                        }
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
        /// <summary>
        /// default update property method
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/crud/company_availability/update_property")]
        [HttpPost()]
        public async Task<JsonResult> UpdateProperty([FromBody] RequestMessage request)
        {
            var response = new StatusResponse();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId);
                var entityId = request.DeserializeTo<int>(FieldConstants.ENTITY, compressed: compressed);
                var entityProperty = request.DeserializeTo<string>("property", compressed: compressed);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                {
                    var entity = await context.GetDbSet<CompanyAvailability>().Where(i => i.Id.Equals(entityId)).FirstOrDefaultAsync();
                    if (entity == null)
                        throw new Exception("A entidade selecionada não existe mais no banco de dados...");

					if (entityProperty.Equals(CompanyAvailability.PROPERTY_COMPANY_ADDRESS_ID))
					{
						var companyAddressIdValue = request.DeserializeTo<int>(CompanyAvailability.PROPERTY_COMPANY_ADDRESS_ID, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyAvailability.CLASS_NAME, CompanyAvailability.PROPERTY_COMPANY_ADDRESS_ID, entity.CompanyAddressId, companyAddressIdValue, context);

                        entity.CompanyAddressId = companyAddressIdValue;
					}
					else if (entityProperty.Equals(CompanyAvailability.PROPERTY_MESSAGE))
					{
						var messageValue = request.DeserializeTo<string>(CompanyAvailability.PROPERTY_MESSAGE, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyAvailability.CLASS_NAME, CompanyAvailability.PROPERTY_MESSAGE, entity.Message, messageValue, context);

                        entity.Message = messageValue;
					}
					else if (entityProperty.Equals(CompanyAvailability.PROPERTY_CLOSET_DISTANCE))
					{
						var closetDistanceValue = request.DeserializeTo<int?>(CompanyAvailability.PROPERTY_CLOSET_DISTANCE, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyAvailability.CLASS_NAME, CompanyAvailability.PROPERTY_CLOSET_DISTANCE, entity.ClosetDistance, closetDistanceValue, context);

                        entity.ClosetDistance = closetDistanceValue;
					}
					else if (entityProperty.Equals(CompanyAvailability.PROPERTY_LINES))
					{
						var linesValue = request.DeserializeTo<int?>(CompanyAvailability.PROPERTY_LINES, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyAvailability.CLASS_NAME, CompanyAvailability.PROPERTY_LINES, entity.Lines, linesValue, context);

                        entity.Lines = linesValue;
					}
					else if (entityProperty.Equals(CompanyAvailability.PROPERTY_UNTIL_20))
					{
						var until20Value = request.DeserializeTo<int?>(CompanyAvailability.PROPERTY_UNTIL_20, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyAvailability.CLASS_NAME, CompanyAvailability.PROPERTY_UNTIL_20, entity.Until20, until20Value, context);

                        entity.Until20 = until20Value;
					}
					else if (entityProperty.Equals(CompanyAvailability.PROPERTY_UNTIL_20_MAX))
					{
						var until20MaxValue = request.DeserializeTo<string>(CompanyAvailability.PROPERTY_UNTIL_20_MAX, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyAvailability.CLASS_NAME, CompanyAvailability.PROPERTY_UNTIL_20_MAX, entity.Until20Max, until20MaxValue, context);

                        entity.Until20Max = until20MaxValue;
					}
					else if (entityProperty.Equals(CompanyAvailability.PROPERTY_UNTIL_20_SIP))
					{
						var until20SipValue = request.DeserializeTo<int?>(CompanyAvailability.PROPERTY_UNTIL_20_SIP, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyAvailability.CLASS_NAME, CompanyAvailability.PROPERTY_UNTIL_20_SIP, entity.Until20Sip, until20SipValue, context);

                        entity.Until20Sip = until20SipValue;
					}
					else if (entityProperty.Equals(CompanyAvailability.PROPERTY_FROM_21_UP_TO_50))
					{
						var from21UpTo50Value = request.DeserializeTo<int?>(CompanyAvailability.PROPERTY_FROM_21_UP_TO_50, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyAvailability.CLASS_NAME, CompanyAvailability.PROPERTY_FROM_21_UP_TO_50, entity.From21UpTo50, from21UpTo50Value, context);

                        entity.From21UpTo50 = from21UpTo50Value;
					}
					else if (entityProperty.Equals(CompanyAvailability.PROPERTY_FROM_21_UP_TO_50_MAX))
					{
						var from21UpTo50MaxValue = request.DeserializeTo<string>(CompanyAvailability.PROPERTY_FROM_21_UP_TO_50_MAX, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyAvailability.CLASS_NAME, CompanyAvailability.PROPERTY_FROM_21_UP_TO_50_MAX, entity.From21UpTo50Max, from21UpTo50MaxValue, context);

                        entity.From21UpTo50Max = from21UpTo50MaxValue;
					}
					else if (entityProperty.Equals(CompanyAvailability.PROPERTY_FROM_21_UP_TO_50_SIP))
					{
						var from21UpTo50SipValue = request.DeserializeTo<int?>(CompanyAvailability.PROPERTY_FROM_21_UP_TO_50_SIP, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyAvailability.CLASS_NAME, CompanyAvailability.PROPERTY_FROM_21_UP_TO_50_SIP, entity.From21UpTo50Sip, from21UpTo50SipValue, context);

                        entity.From21UpTo50Sip = from21UpTo50SipValue;
					}
					else if (entityProperty.Equals(CompanyAvailability.PROPERTY_OVER_50))
					{
						var over50Value = request.DeserializeTo<int?>(CompanyAvailability.PROPERTY_OVER_50, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyAvailability.CLASS_NAME, CompanyAvailability.PROPERTY_OVER_50, entity.Over50, over50Value, context);

                        entity.Over50 = over50Value;
					}
					else if (entityProperty.Equals(CompanyAvailability.PROPERTY_OVER_50_MAX))
					{
						var over50MaxValue = request.DeserializeTo<string>(CompanyAvailability.PROPERTY_OVER_50_MAX, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyAvailability.CLASS_NAME, CompanyAvailability.PROPERTY_OVER_50_MAX, entity.Over50Max, over50MaxValue, context);

                        entity.Over50Max = over50MaxValue;
					}
					else if (entityProperty.Equals(CompanyAvailability.PROPERTY_OVER_50_SIP))
					{
						var over50SipValue = request.DeserializeTo<int?>(CompanyAvailability.PROPERTY_OVER_50_SIP, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyAvailability.CLASS_NAME, CompanyAvailability.PROPERTY_OVER_50_SIP, entity.Over50Sip, over50SipValue, context);

                        entity.Over50Sip = over50SipValue;
					}
					else if (entityProperty.Equals(CompanyAvailability.PROPERTY_NETWORK))
					{
						var networkValue = request.DeserializeTo<string>(CompanyAvailability.PROPERTY_NETWORK, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyAvailability.CLASS_NAME, CompanyAvailability.PROPERTY_NETWORK, entity.Network, networkValue, context);

                        entity.Network = networkValue;
					}
					else if (entityProperty.Equals(CompanyAvailability.PROPERTY_TECNOLOGY))
					{
						var tecnologyValue = request.DeserializeTo<string>(CompanyAvailability.PROPERTY_TECNOLOGY, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyAvailability.CLASS_NAME, CompanyAvailability.PROPERTY_TECNOLOGY, entity.Tecnology, tecnologyValue, context);

                        entity.Tecnology = tecnologyValue;
					}
					else if (entityProperty.Equals(CompanyAvailability.PROPERTY_CLOSET_TOP_SPEED))
					{
						var closetTopSpeedValue = request.DeserializeTo<int?>(CompanyAvailability.PROPERTY_CLOSET_TOP_SPEED, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyAvailability.CLASS_NAME, CompanyAvailability.PROPERTY_CLOSET_TOP_SPEED, entity.ClosetTopSpeed, closetTopSpeedValue, context);

                        entity.ClosetTopSpeed = closetTopSpeedValue;
					}
					else if (entityProperty.Equals(CompanyAvailability.PROPERTY_TECNOLOGY_FOR_TV))
					{
						var tecnologyForTvValue = request.DeserializeTo<string>(CompanyAvailability.PROPERTY_TECNOLOGY_FOR_TV, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyAvailability.CLASS_NAME, CompanyAvailability.PROPERTY_TECNOLOGY_FOR_TV, entity.TecnologyForTv, tecnologyForTvValue, context);

                        entity.TecnologyForTv = tecnologyForTvValue;
					}
					else if (entityProperty.Equals(CompanyAvailability.PROPERTY_TIMESTAMP))
					{
						var timestampValue = request.DeserializeTo<DateTime>(CompanyAvailability.PROPERTY_TIMESTAMP, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyAvailability.CLASS_NAME, CompanyAvailability.PROPERTY_TIMESTAMP, entity.Timestamp, timestampValue, context);

                        entity.Timestamp = timestampValue;
					}
                    entity.SetStateModified(context);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
    }
}