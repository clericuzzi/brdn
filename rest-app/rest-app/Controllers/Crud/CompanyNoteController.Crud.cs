using System;
using System.Linq;
using RestApi.Database;
using RestApi.Entities;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Clericuzzi.WebApi.Entities;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Clericuzzi.Lib.Http.Utils.Constants;
using Clericuzzi.WebApi.Entities.Business;
using Clericuzzi.WebApi.Data.DatabaseContext;
using Clericuzzi.WebApi.Entities.Business.Enums;
using Clericuzzi.Lib.Http.Communication.Request;
using Clericuzzi.Lib.Http.Communication.Response;
using Clericuzzi.WebApi.Entities.Controllers.Crud;
using Clericuzzi.WebApi.Entities.Business.Managers;
using Clericuzzi.Lib.Http.Communication.Response.Json;
using Clericuzzi.WebApi.Data.Business.DatabaseContext;
using Clericuzzi.WebApi.Entities.Business.Models.Crud;
using Clericuzzi.Lib.Http.Communication.Request.Pagination;

namespace RestApi.Entities.Controllers.Crud
{
    /// <summary>
    /// A class for default crud actions regarding the `company_note` table
    /// </summary>
    public partial class CompanyNoteCrudController : Controller
    {
        /// <summary>
        /// default listing method
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/crud/company_note/listing")]
        [HttpPost()]
        public JsonResult Listing([FromBody] RequestMessage request)
        {
            var response = new BatchResponseList();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                var filter = request.DeserializeTo<CompanyNoteFilter>("filter", compressed: compressed, mandatory: false);
                using (var context = AppDatabaseMysql.CreateConnection())
                {
                    var query = context.GetDbSet<CompanyNote>().Include(i => i.UserIdParent).AsQueryable();
                    query = FilterResults(query, filter, context);
                    var items = query.Select(i => GetListing(i)).ToList();
                    items = items.OrderBy(i => i.Label).ToList();

                    response.AddEntityList(CompanyNote.TABLE_NAME, items.ToList<object>());
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }

        /// <summary>
        /// default select method
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/crud/company_note/filter")]
        [HttpPost()]
        public async Task<JsonResult> Filter([FromBody] RequestMessage request)
        {
            var response = new BatchResponse<CompanyNote>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName, out RequestPagination pagination);
                var filter = request.DeserializeTo<CompanyNoteFilter>("filter", compressed: compressed, mandatory: false);
                using (var context = AppDatabaseMysql.CreateConnection())
                {
                    var query = context.GetDbSet<CompanyNote>().Include(i => i.UserIdParent).OrderBy(i => i.UserIdParent.Name).AsQueryable();
                    query = FilterResults(query, filter, context);

                    response.Entities = pagination.Paginate(query);
                    response.Pagination = pagination;

                    await LoadCustomFields(response.Entities, context);
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }

        /// <summary>
        /// default insert method
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/crud/company_note/insert")]
        [HttpPost()]
        public JsonResult Insert([FromBody] RequestMessage request)
        {
            var response = new BatchResponse<CompanyNote>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                var model = request.DeserializeTo<CompanyNote>("model", compressed: compressed, mandatory: false);
                var items = request.DeserializeTo<List<CompanyNote>>(FieldConstants.ENTITIES, compressed: compressed, mandatory: false);

                if (model != null)
                    items = new List<CompanyNote> { model };

                var existingItems = items.Where(i => i.Id > 0).ToList();
                var nonExintingItems = items.Where(i => i.Id <= 0).ToList();
                if (nonExintingItems?.Count > 0)
                {
                    using (var context = AppDatabaseMysql.CreateConnection(true))
                    {
                        context.AddRange(nonExintingItems);
                        foreach (var item in nonExintingItems)
                            LogManager.Crud(userId, item.Summary, LogActionsEnum.Insert, CompanyNote.CLASS_NAME, context);
                    }
                }

                response.Entities = nonExintingItems;
                if (existingItems.Count > 0)
                    throw new Exception("Alguns itens foram ignorados por já existirem na base de dados");
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
        /// <summary>
        /// default delete method
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/crud/company_note/delete")]
        [HttpPost()]
        public JsonResult Delete([FromBody] RequestMessage request)
        {
            var response = new StatusResponse();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                var ids = request.DeserializeTo<List<int>>(FieldConstants.ENTITIES, compressed: compressed);
                if (ids?.Count > 0)
                    using (var context = AppDatabaseMysql.CreateConnection(true))
                    {
                        var items = context.GetDbSet<CompanyNote>().Where(i => ids.Contains(i.Id)).ToList();
                        if (items.Count > 0)
                        {
                            context.RemoveRange(items);
                            foreach (var item in items)
                                LogManager.Crud(userId, item.Summary, LogActionsEnum.Delete, CompanyNote.CLASS_NAME, context);
                        }
                    }

                response.Success = true;
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
        /// <summary>
        /// default update method
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/crud/company_note/update")]
        [HttpPost()]
        public JsonResult Update([FromBody] RequestMessage request)
        {
            var response = new StatusResponse();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                var model = request.DeserializeTo<CompanyNote>("model", compressed: compressed, mandatory: false);
                var entities = request.DeserializeTo<List<CompanyNote>>(FieldConstants.ENTITIES, compressed: compressed, mandatory: false);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                {
                    if (model != null)
                        entities = new List<CompanyNote> { model };

                    var ids = entities.Select(i => i.Id).ToList();
                    var items = context.GetDbSet<CompanyNote>().Where(i => ids.Contains(i.Id)).ToList();
                    foreach (var item in items)
                    {
                        var entity = entities.Where(i => i.Id.Equals(item.Id)).FirstOrDefault();
                        if (item != null && entity != null)
                        {
                            item.Id = entity.Id;
                            item.UserId = entity.UserId;
                            item.Comment = entity.Comment;
                            item.CompanyId = entity.CompanyId;
                            item.ContactId = entity.ContactId;
                            item.Timestamp = entity.Timestamp;

                            item.SetStateModified(context);
                            LogManager.Crud(userId, entity.Summary, LogActionsEnum.Update, CompanyNote.CLASS_NAME, context);
                        }
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
        /// <summary>
        /// default update property method
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/crud/company_note/update_property")]
        [HttpPost()]
        public async Task<JsonResult> UpdateProperty([FromBody] RequestMessage request)
        {
            var response = new StatusResponse();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId);
                var entityId = request.DeserializeTo<int>(FieldConstants.ENTITY, compressed: compressed);
                var entityProperty = request.DeserializeTo<string>("property", compressed: compressed);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                {
                    var entity = await context.GetDbSet<CompanyNote>().Where(i => i.Id.Equals(entityId)).FirstOrDefaultAsync();
                    if (entity == null)
                        throw new Exception("A entidade selecionada não existe mais no banco de dados...");

                    if (entityProperty.Equals(CompanyNote.PROPERTY_USER_ID))
                    {
                        var userIdValue = request.DeserializeTo<int>(CompanyNote.PROPERTY_USER_ID, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyNote.CLASS_NAME, CompanyNote.PROPERTY_USER_ID, entity.UserId, userIdValue, context);

                        entity.UserId = userIdValue;
                    }
                    else if (entityProperty.Equals(CompanyNote.PROPERTY_COMPANY_ID))
                    {
                        var companyIdValue = request.DeserializeTo<int>(CompanyNote.PROPERTY_COMPANY_ID, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyNote.CLASS_NAME, CompanyNote.PROPERTY_COMPANY_ID, entity.CompanyId, companyIdValue, context);

                        entity.CompanyId = companyIdValue;
                    }
                    else if (entityProperty.Equals(CompanyNote.PROPERTY_CONTACT_ID))
                    {
                        var contactIdValue = request.DeserializeTo<int?>(CompanyNote.PROPERTY_CONTACT_ID, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyNote.CLASS_NAME, CompanyNote.PROPERTY_CONTACT_ID, entity.ContactId, contactIdValue, context);

                        entity.ContactId = contactIdValue;
                    }
                    else if (entityProperty.Equals(CompanyNote.PROPERTY_COMMENT))
                    {
                        var commentValue = request.DeserializeTo<string>(CompanyNote.PROPERTY_COMMENT, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyNote.CLASS_NAME, CompanyNote.PROPERTY_COMMENT, entity.Comment, commentValue, context);

                        entity.Comment = commentValue;
                    }
                    else if (entityProperty.Equals(CompanyNote.PROPERTY_TIMESTAMP))
                    {
                        var timestampValue = request.DeserializeTo<DateTime>(CompanyNote.PROPERTY_TIMESTAMP, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyNote.CLASS_NAME, CompanyNote.PROPERTY_TIMESTAMP, entity.Timestamp, timestampValue, context);

                        entity.Timestamp = timestampValue;
                    }
                    entity.SetStateModified(context);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
    }
}