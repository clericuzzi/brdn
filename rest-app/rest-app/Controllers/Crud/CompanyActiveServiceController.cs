using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Clericuzzi.WebApi.Entities;
using Microsoft.EntityFrameworkCore;
using Clericuzzi.WebApi.Data.DatabaseContext;
using Clericuzzi.WebApi.Entities.Business.Models.Crud;
using Clericuzzi.WebApi.Data.Business.DatabaseContext;

namespace RestApi.Entities.Controllers.Crud
{
    /// <summary>
    /// A class for project specific endpoints regarding the `company_active_service` table
    /// </summary>
    public partial class CompanyActiveServiceCrudController : Controller
    {
        /// <summary>
        /// Custom fields loader, implement only if needed
        /// </summary>
        /// <param name="items">the entity's list</param>
        /// <param name="context">database connection</param>
        /// <returns>laods the custom information into the given entities</returns>
        private async Task LoadCustomFields(List<CompanyActiveService> items, DataContext context)
        {
            await Task.Delay(150);
			// method sample
            // var peopleIds = items.Select(i => -i.PersonId).Distinct().ToList();
            // var productIds = items.Select(i => i.ProductId).Distinct().ToList();
            // var proposalIds = items.Select(i => i.Id).Distinct().ToList();

            // var people = context.GetDbSet<Person>().Where(i => peopleIds.Contains(i.Id)).ToList();
            // var products = context.GetDbSet<Product>().Where(i => productIds.Contains(i.Id)).ToList();
            // var userTasks = context.GetDbSet<UserTask>().Where(i => proposalIds.Contains(i.ProposalId)).ToList();

            // foreach (var item in items)
            //    item.LoadExternalInfo(people, products, userTasks, context);
        }

        /// <summary>
        /// default listing for the 'CompanyActiveService' class 
        /// </summary>
        /// <param name="item">the current instance</param>
        /// <returns>a new tuple model for showing in a combo or auto complete fields</returns>
        private TupleModel<int> GetListing(CompanyActiveService item)
        {
            return new TupleModel<int> { Id = item.Id, Label = item.Summary };
        }

		/// <summary>
        /// custom filters for the 'CompanyActiveService' class
        /// </summary>
        /// <param name="query">the ongoing query</param>
        /// <param name="filter">the filter object</param>
        /// <param name="context">database connection</param>
        /// <returns>the query filtered by the filter object</returns>
		public static IQueryable<CompanyActiveService> FilterResults(IQueryable<CompanyActiveService> query, CompanyActiveServiceFilter filter, DataContext context)
		{
			try
			{
				if (filter == null)
					return query;
				else
				{
					if (!string.IsNullOrEmpty(filter?.Tv))
						query = query.Where(i => i.Tv.Contains(filter.Tv));
					if (!string.IsNullOrEmpty(filter?.Qtd))
						query = query.Where(i => i.Qtd.Contains(filter.Qtd));
					if (!string.IsNullOrEmpty(filter?.Rpon))
						query = query.Where(i => i.Rpon.Contains(filter.Rpon));
					if (!string.IsNullOrEmpty(filter?.Order))
						query = query.Where(i => i.Order.Contains(filter.Order));
					if (!string.IsNullOrEmpty(filter?.Offer))
						query = query.Where(i => i.Offer.Contains(filter.Offer));
					if (!string.IsNullOrEmpty(filter?.Status))
						query = query.Where(i => i.Status.Contains(filter.Status));
					if (!string.IsNullOrEmpty(filter?.Invalid))
						query = query.Where(i => i.Invalid.Contains(filter.Invalid));
					if (!string.IsNullOrEmpty(filter?.Benefit))
						query = query.Where(i => i.Benefit.Contains(filter.Benefit));
					if (!string.IsNullOrEmpty(filter?.Product))
						query = query.Where(i => i.Product.Contains(filter.Product));
					if (!string.IsNullOrEmpty(filter?.Instance))
						query = query.Where(i => i.Instance.Contains(filter.Instance));
					if (filter?.CompanyId?.Count > 0)
						query = query.Where(i => filter.CompanyId.Contains(i.CompanyId));
					if (!string.IsNullOrEmpty(filter?.CreatedId))
						query = query.Where(i => i.CreatedId.Contains(filter.CreatedId));
					if (!string.IsNullOrEmpty(filter?.Ficticious))
						query = query.Where(i => i.Ficticious.Contains(filter.Ficticious));
					if (!string.IsNullOrEmpty(filter?.Portability))
						query = query.Where(i => i.Portability.Contains(filter.Portability));
					if (!string.IsNullOrEmpty(filter?.DoubleAccess))
						query = query.Where(i => i.DoubleAccess.Contains(filter.DoubleAccess));
					if (!string.IsNullOrEmpty(filter?.ChargeProfile))
						query = query.Where(i => i.ChargeProfile.Contains(filter.ChargeProfile));
					if (!string.IsNullOrEmpty(filter?.ServiceAddress))
						query = query.Where(i => i.ServiceAddress.Contains(filter.ServiceAddress));
					if (!string.IsNullOrEmpty(filter?.TecnologyVoice))
						query = query.Where(i => i.TecnologyVoice.Contains(filter.TecnologyVoice));
					if (!string.IsNullOrEmpty(filter?.ActivationDate))
						query = query.Where(i => i.ActivationDate.Contains(filter.ActivationDate));
					if (!string.IsNullOrEmpty(filter?.TecnologyAccess))
						query = query.Where(i => i.TecnologyAccess.Contains(filter.TecnologyAccess));
					if (!string.IsNullOrEmpty(filter?.AssociatedProduct))
						query = query.Where(i => i.AssociatedProduct.Contains(filter.AssociatedProduct));

					return query;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}
    }
}