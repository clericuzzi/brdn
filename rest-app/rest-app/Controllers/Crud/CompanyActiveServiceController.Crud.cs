using System;
using System.Linq;
using RestApi.Database;
using RestApi.Entities;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Clericuzzi.WebApi.Entities;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Clericuzzi.Lib.Http.Utils.Constants;
using Clericuzzi.WebApi.Entities.Business;
using Clericuzzi.WebApi.Data.DatabaseContext;
using Clericuzzi.WebApi.Entities.Business.Enums;
using Clericuzzi.Lib.Http.Communication.Request;
using Clericuzzi.Lib.Http.Communication.Response;
using Clericuzzi.WebApi.Entities.Controllers.Crud;
using Clericuzzi.WebApi.Entities.Business.Managers;
using Clericuzzi.Lib.Http.Communication.Response.Json;
using Clericuzzi.WebApi.Data.Business.DatabaseContext;
using Clericuzzi.WebApi.Entities.Business.Models.Crud;
using Clericuzzi.Lib.Http.Communication.Request.Pagination;

namespace RestApi.Entities.Controllers.Crud
{
    /// <summary>
    /// A class for default crud actions regarding the `company_active_service` table
    /// </summary>
    public partial class CompanyActiveServiceCrudController : Controller
    {
        /// <summary>
        /// default listing method
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/crud/company_active_service/listing")]
        [HttpPost()]
        public JsonResult Listing([FromBody] RequestMessage request)
        {
            var response = new BatchResponseList();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                var filter = request.DeserializeTo<CompanyActiveServiceFilter>("filter", compressed: compressed, mandatory: false);
                using (var context = AppDatabaseMysql.CreateConnection())
                {
                    var query = context.GetDbSet<CompanyActiveService>().AsQueryable();
                    query = FilterResults(query, filter, context);
                    var items = query.Select(i => GetListing(i)).ToList();
                    items = items.OrderBy(i => i.Label).ToList();

                    response.AddEntityList(CompanyActiveService.TABLE_NAME, items.ToList<object>());
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }

        /// <summary>
        /// default select method
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/crud/company_active_service/filter")]
        [HttpPost()]
        public async Task<JsonResult> Filter([FromBody] RequestMessage request)
        {
            var response = new BatchResponse<CompanyActiveService>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName, out RequestPagination pagination);
                var filter = request.DeserializeTo<CompanyActiveServiceFilter>("filter", compressed: compressed, mandatory: false);
                using (var context = AppDatabaseMysql.CreateConnection())
                {
                    var query = context.GetDbSet<CompanyActiveService>().OrderBy(i => i.CompanyIdParent.AccountIdParent.Cnpj).AsQueryable();
					query = FilterResults(query, filter, context);

                    response.Entities = pagination.Paginate(query);
                    response.Pagination = pagination;

                    await LoadCustomFields(response.Entities, context);
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }

        /// <summary>
        /// default insert method
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/crud/company_active_service/insert")]
        [HttpPost()]
        public JsonResult Insert([FromBody] RequestMessage request)
        {
            var response = new BatchResponse<CompanyActiveService>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                var model = request.DeserializeTo<CompanyActiveService>("model", compressed: compressed, mandatory: false);
                var items = request.DeserializeTo<List<CompanyActiveService>>(FieldConstants.ENTITIES, compressed: compressed, mandatory: false);
                
				if (model != null)
                    items = new List<CompanyActiveService> { model };

                var existingItems = items.Where(i => i.Id > 0).ToList();
                var nonExintingItems = items.Where(i => i.Id <= 0).ToList();
                if (nonExintingItems?.Count > 0)
				{
					using (var context = AppDatabaseMysql.CreateConnection(true))
					{
						context.AddRange(nonExintingItems);
						foreach (var item in nonExintingItems)
							LogManager.Crud(userId, item.Summary, LogActionsEnum.Insert, CompanyActiveService.CLASS_NAME, context);
					}
				}

                response.Entities = nonExintingItems;
                if (existingItems.Count > 0)
                    throw new Exception("Alguns itens foram ignorados por já existirem na base de dados");
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
        /// <summary>
        /// default delete method
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/crud/company_active_service/delete")]
        [HttpPost()]
        public JsonResult Delete([FromBody] RequestMessage request)
        {
            var response = new StatusResponse();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                var ids = request.DeserializeTo<List<int>>(FieldConstants.ENTITIES, compressed: compressed);
				if (ids?.Count > 0)
					using (var context = AppDatabaseMysql.CreateConnection(true))
					{
						var items = context.GetDbSet<CompanyActiveService>().Where(i => ids.Contains(i.Id)).ToList();
						if (items.Count > 0)
						{
							context.RemoveRange(items);
							foreach (var item in items)
								LogManager.Crud(userId, item.Summary, LogActionsEnum.Delete, CompanyActiveService.CLASS_NAME, context);
						}
					}

                response.Success = true;
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
        /// <summary>
        /// default update method
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/crud/company_active_service/update")]
        [HttpPost()]
        public JsonResult Update([FromBody] RequestMessage request)
        {
            var response = new StatusResponse();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                var model = request.DeserializeTo<CompanyActiveService>("model", compressed: compressed, mandatory: false);
                var entities = request.DeserializeTo<List<CompanyActiveService>>(FieldConstants.ENTITIES, compressed: compressed, mandatory: false);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                {
                    if (model != null)
                        entities = new List<CompanyActiveService> { model };

                    var ids = entities.Select(i => i.Id).ToList();
                    var items = context.GetDbSet<CompanyActiveService>().Where(i => ids.Contains(i.Id)).ToList();
                    foreach (var item in items)
                    {
                        var entity = entities.Where(i => i.Id.Equals(item.Id)).FirstOrDefault();
                        if (item != null && entity != null)
                        {
                            item.Id = entity.Id;
							item.Tv = entity.Tv;
							item.Qtd = entity.Qtd;
							item.Rpon = entity.Rpon;
							item.Order = entity.Order;
							item.Offer = entity.Offer;
							item.Status = entity.Status;
							item.Invalid = entity.Invalid;
							item.Benefit = entity.Benefit;
							item.Product = entity.Product;
							item.Instance = entity.Instance;
							item.CompanyId = entity.CompanyId;
							item.CreatedId = entity.CreatedId;
							item.Ficticious = entity.Ficticious;
							item.Portability = entity.Portability;
							item.DoubleAccess = entity.DoubleAccess;
							item.ChargeProfile = entity.ChargeProfile;
							item.ServiceAddress = entity.ServiceAddress;
							item.TecnologyVoice = entity.TecnologyVoice;
							item.ActivationDate = entity.ActivationDate;
							item.TecnologyAccess = entity.TecnologyAccess;
							item.AssociatedProduct = entity.AssociatedProduct;

                            item.SetStateModified(context);
							LogManager.Crud(userId, entity.Summary, LogActionsEnum.Update, CompanyActiveService.CLASS_NAME, context);
                        }
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
        /// <summary>
        /// default update property method
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/crud/company_active_service/update_property")]
        [HttpPost()]
        public async Task<JsonResult> UpdateProperty([FromBody] RequestMessage request)
        {
            var response = new StatusResponse();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId);
                var entityId = request.DeserializeTo<int>(FieldConstants.ENTITY, compressed: compressed);
                var entityProperty = request.DeserializeTo<string>("property", compressed: compressed);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                {
                    var entity = await context.GetDbSet<CompanyActiveService>().Where(i => i.Id.Equals(entityId)).FirstOrDefaultAsync();
                    if (entity == null)
                        throw new Exception("A entidade selecionada não existe mais no banco de dados...");

					if (entityProperty.Equals(CompanyActiveService.PROPERTY_COMPANY_ID))
					{
						var companyIdValue = request.DeserializeTo<int>(CompanyActiveService.PROPERTY_COMPANY_ID, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyActiveService.CLASS_NAME, CompanyActiveService.PROPERTY_COMPANY_ID, entity.CompanyId, companyIdValue, context);

                        entity.CompanyId = companyIdValue;
					}
					else if (entityProperty.Equals(CompanyActiveService.PROPERTY_INSTANCE))
					{
						var instanceValue = request.DeserializeTo<string>(CompanyActiveService.PROPERTY_INSTANCE, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyActiveService.CLASS_NAME, CompanyActiveService.PROPERTY_INSTANCE, entity.Instance, instanceValue, context);

                        entity.Instance = instanceValue;
					}
					else if (entityProperty.Equals(CompanyActiveService.PROPERTY_SERVICE_ADDRESS))
					{
						var serviceAddressValue = request.DeserializeTo<string>(CompanyActiveService.PROPERTY_SERVICE_ADDRESS, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyActiveService.CLASS_NAME, CompanyActiveService.PROPERTY_SERVICE_ADDRESS, entity.ServiceAddress, serviceAddressValue, context);

                        entity.ServiceAddress = serviceAddressValue;
					}
					else if (entityProperty.Equals(CompanyActiveService.PROPERTY_ASSOCIATED_PRODUCT))
					{
						var associatedProductValue = request.DeserializeTo<string>(CompanyActiveService.PROPERTY_ASSOCIATED_PRODUCT, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyActiveService.CLASS_NAME, CompanyActiveService.PROPERTY_ASSOCIATED_PRODUCT, entity.AssociatedProduct, associatedProductValue, context);

                        entity.AssociatedProduct = associatedProductValue;
					}
					else if (entityProperty.Equals(CompanyActiveService.PROPERTY_STATUS))
					{
						var statusValue = request.DeserializeTo<string>(CompanyActiveService.PROPERTY_STATUS, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyActiveService.CLASS_NAME, CompanyActiveService.PROPERTY_STATUS, entity.Status, statusValue, context);

                        entity.Status = statusValue;
					}
					else if (entityProperty.Equals(CompanyActiveService.PROPERTY_ORDER))
					{
						var orderValue = request.DeserializeTo<string>(CompanyActiveService.PROPERTY_ORDER, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyActiveService.CLASS_NAME, CompanyActiveService.PROPERTY_ORDER, entity.Order, orderValue, context);

                        entity.Order = orderValue;
					}
					else if (entityProperty.Equals(CompanyActiveService.PROPERTY_OFFER))
					{
						var offerValue = request.DeserializeTo<string>(CompanyActiveService.PROPERTY_OFFER, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyActiveService.CLASS_NAME, CompanyActiveService.PROPERTY_OFFER, entity.Offer, offerValue, context);

                        entity.Offer = offerValue;
					}
					else if (entityProperty.Equals(CompanyActiveService.PROPERTY_INVALID))
					{
						var invalidValue = request.DeserializeTo<string>(CompanyActiveService.PROPERTY_INVALID, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyActiveService.CLASS_NAME, CompanyActiveService.PROPERTY_INVALID, entity.Invalid, invalidValue, context);

                        entity.Invalid = invalidValue;
					}
					else if (entityProperty.Equals(CompanyActiveService.PROPERTY_TECNOLOGY_VOICE))
					{
						var tecnologyVoiceValue = request.DeserializeTo<string>(CompanyActiveService.PROPERTY_TECNOLOGY_VOICE, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyActiveService.CLASS_NAME, CompanyActiveService.PROPERTY_TECNOLOGY_VOICE, entity.TecnologyVoice, tecnologyVoiceValue, context);

                        entity.TecnologyVoice = tecnologyVoiceValue;
					}
					else if (entityProperty.Equals(CompanyActiveService.PROPERTY_TECNOLOGY_ACCESS))
					{
						var tecnologyAccessValue = request.DeserializeTo<string>(CompanyActiveService.PROPERTY_TECNOLOGY_ACCESS, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyActiveService.CLASS_NAME, CompanyActiveService.PROPERTY_TECNOLOGY_ACCESS, entity.TecnologyAccess, tecnologyAccessValue, context);

                        entity.TecnologyAccess = tecnologyAccessValue;
					}
					else if (entityProperty.Equals(CompanyActiveService.PROPERTY_CHARGE_PROFILE))
					{
						var chargeProfileValue = request.DeserializeTo<string>(CompanyActiveService.PROPERTY_CHARGE_PROFILE, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyActiveService.CLASS_NAME, CompanyActiveService.PROPERTY_CHARGE_PROFILE, entity.ChargeProfile, chargeProfileValue, context);

                        entity.ChargeProfile = chargeProfileValue;
					}
					else if (entityProperty.Equals(CompanyActiveService.PROPERTY_CREATED_ID))
					{
						var createdIdValue = request.DeserializeTo<string>(CompanyActiveService.PROPERTY_CREATED_ID, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyActiveService.CLASS_NAME, CompanyActiveService.PROPERTY_CREATED_ID, entity.CreatedId, createdIdValue, context);

                        entity.CreatedId = createdIdValue;
					}
					else if (entityProperty.Equals(CompanyActiveService.PROPERTY_ACTIVATION_DATE))
					{
						var activationDateValue = request.DeserializeTo<string>(CompanyActiveService.PROPERTY_ACTIVATION_DATE, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyActiveService.CLASS_NAME, CompanyActiveService.PROPERTY_ACTIVATION_DATE, entity.ActivationDate, activationDateValue, context);

                        entity.ActivationDate = activationDateValue;
					}
					else if (entityProperty.Equals(CompanyActiveService.PROPERTY_BENEFIT))
					{
						var benefitValue = request.DeserializeTo<string>(CompanyActiveService.PROPERTY_BENEFIT, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyActiveService.CLASS_NAME, CompanyActiveService.PROPERTY_BENEFIT, entity.Benefit, benefitValue, context);

                        entity.Benefit = benefitValue;
					}
					else if (entityProperty.Equals(CompanyActiveService.PROPERTY_PORTABILITY))
					{
						var portabilityValue = request.DeserializeTo<string>(CompanyActiveService.PROPERTY_PORTABILITY, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyActiveService.CLASS_NAME, CompanyActiveService.PROPERTY_PORTABILITY, entity.Portability, portabilityValue, context);

                        entity.Portability = portabilityValue;
					}
					else if (entityProperty.Equals(CompanyActiveService.PROPERTY_RPON))
					{
						var rponValue = request.DeserializeTo<string>(CompanyActiveService.PROPERTY_RPON, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyActiveService.CLASS_NAME, CompanyActiveService.PROPERTY_RPON, entity.Rpon, rponValue, context);

                        entity.Rpon = rponValue;
					}
					else if (entityProperty.Equals(CompanyActiveService.PROPERTY_TV))
					{
						var tvValue = request.DeserializeTo<string>(CompanyActiveService.PROPERTY_TV, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyActiveService.CLASS_NAME, CompanyActiveService.PROPERTY_TV, entity.Tv, tvValue, context);

                        entity.Tv = tvValue;
					}
					else if (entityProperty.Equals(CompanyActiveService.PROPERTY_FICTICIOUS))
					{
						var ficticiousValue = request.DeserializeTo<string>(CompanyActiveService.PROPERTY_FICTICIOUS, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyActiveService.CLASS_NAME, CompanyActiveService.PROPERTY_FICTICIOUS, entity.Ficticious, ficticiousValue, context);

                        entity.Ficticious = ficticiousValue;
					}
					else if (entityProperty.Equals(CompanyActiveService.PROPERTY_DOUBLE_ACCESS))
					{
						var doubleAccessValue = request.DeserializeTo<string>(CompanyActiveService.PROPERTY_DOUBLE_ACCESS, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyActiveService.CLASS_NAME, CompanyActiveService.PROPERTY_DOUBLE_ACCESS, entity.DoubleAccess, doubleAccessValue, context);

                        entity.DoubleAccess = doubleAccessValue;
					}
					else if (entityProperty.Equals(CompanyActiveService.PROPERTY_QTD))
					{
						var qtdValue = request.DeserializeTo<string>(CompanyActiveService.PROPERTY_QTD, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyActiveService.CLASS_NAME, CompanyActiveService.PROPERTY_QTD, entity.Qtd, qtdValue, context);

                        entity.Qtd = qtdValue;
					}
					else if (entityProperty.Equals(CompanyActiveService.PROPERTY_PRODUCT))
					{
						var productValue = request.DeserializeTo<string>(CompanyActiveService.PROPERTY_PRODUCT, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyActiveService.CLASS_NAME, CompanyActiveService.PROPERTY_PRODUCT, entity.Product, productValue, context);

                        entity.Product = productValue;
					}
                    entity.SetStateModified(context);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
    }
}