using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Clericuzzi.WebApi.Entities;
using Microsoft.EntityFrameworkCore;
using Clericuzzi.WebApi.Data.DatabaseContext;
using Clericuzzi.WebApi.Entities.Business.Models.Crud;
using Clericuzzi.WebApi.Data.Business.DatabaseContext;

namespace RestApi.Entities.Controllers.Crud
{
    /// <summary>
    /// A class for project specific endpoints regarding the `phone_operator` table
    /// </summary>
    public partial class PhoneOperatorCrudController : Controller
    {
        /// <summary>
        /// returns an operator by it's name
        /// if not found, a new one is created
        /// </summary>
        /// <param name="name">the desired operator's name</param>
        /// <param name="context">database connection</param>
        /// <returns>the phone operator that mathes the given name</returns>
        public static async Task<PhoneOperator> GetOperatorByName(string name, DataContext context)
        {
            var existingOperator = await context.GetDbSet<PhoneOperator>().FirstOrDefaultAsync(i => i.Name.Equals(name));
            if (existingOperator != null)
                return existingOperator;
            else
            {
                var newOperator = new PhoneOperator { Name = name };
                newOperator.SetStateAdded(context);
                await context.SaveChangesAsync();

                return newOperator;
            }
        }

        /// <summary>
        /// Custom fields loader, implement only if needed
        /// </summary>
        /// <param name="items">the entity's list</param>
        /// <param name="context">database connection</param>
        /// <returns>laods the custom information into the given entities</returns>
        private async Task LoadCustomFields(List<PhoneOperator> items, DataContext context)
        {
            await Task.Delay(150);
            // method sample
            // var peopleIds = items.Select(i => -i.PersonId).Distinct().ToList();
            // var productIds = items.Select(i => i.ProductId).Distinct().ToList();
            // var proposalIds = items.Select(i => i.Id).Distinct().ToList();

            // var people = context.GetDbSet<Person>().Where(i => peopleIds.Contains(i.Id)).ToList();
            // var products = context.GetDbSet<Product>().Where(i => productIds.Contains(i.Id)).ToList();
            // var userTasks = context.GetDbSet<UserTask>().Where(i => proposalIds.Contains(i.ProposalId)).ToList();

            // foreach (var item in items)
            //    item.LoadExternalInfo(people, products, userTasks, context);
        }

        /// <summary>
        /// default listing for the 'PhoneOperator' class 
        /// </summary>
        /// <param name="item">the current instance</param>
        /// <returns>a new tuple model for showing in a combo or auto complete fields</returns>
        private TupleModel<int> GetListing(PhoneOperator item)
        {
            return new TupleModel<int> { Id = item.Id, Label = item.Summary };
        }

        /// <summary>
        /// custom filters for the 'PhoneOperator' class
        /// </summary>
        /// <param name="query">the ongoing query</param>
        /// <param name="filter">the filter object</param>
        /// <param name="context">database connection</param>
        /// <returns>the query filtered by the filter object</returns>
        public static IQueryable<PhoneOperator> FilterResults(IQueryable<PhoneOperator> query, PhoneOperatorFilter filter, DataContext context)
        {
            try
            {
                if (filter == null)
                    return query;
                else
                {
                    if (!string.IsNullOrEmpty(filter?.Name))
                        query = query.Where(i => i.Name.Contains(filter.Name));

                    return query;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}