using System;
using System.Linq;
using RestApi.Database;
using RestApi.Entities;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Clericuzzi.WebApi.Entities;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Clericuzzi.Lib.Http.Utils.Constants;
using Clericuzzi.WebApi.Entities.Business;
using Clericuzzi.WebApi.Data.DatabaseContext;
using Clericuzzi.WebApi.Entities.Business.Enums;
using Clericuzzi.Lib.Http.Communication.Request;
using Clericuzzi.Lib.Http.Communication.Response;
using Clericuzzi.WebApi.Entities.Controllers.Crud;
using Clericuzzi.WebApi.Entities.Business.Managers;
using Clericuzzi.Lib.Http.Communication.Response.Json;
using Clericuzzi.WebApi.Data.Business.DatabaseContext;
using Clericuzzi.WebApi.Entities.Business.Models.Crud;
using Clericuzzi.Lib.Http.Communication.Request.Pagination;

namespace RestApi.Entities.Controllers.Crud
{
    /// <summary>
    /// A class for default crud actions regarding the `company` table
    /// </summary>
    public partial class CompanyCrudController : Controller
    {
        /// <summary>
        /// default listing method
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/crud/company/listing")]
        [HttpPost()]
        public JsonResult Listing([FromBody] RequestMessage request)
        {
            var response = new BatchResponseList();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                var filter = request.DeserializeTo<CompanyFilter>("filter", compressed: compressed, mandatory: false);
                using (var context = AppDatabaseMysql.CreateConnection())
                {
                    var query = context.GetDbSet<Company>().AsQueryable();
                    query = FilterResults(query, filter, context);
                    var items = query.Select(i => GetListing(i)).ToList();
                    items = items.OrderBy(i => i.Label).ToList();

                    response.AddEntityList(Company.TABLE_NAME, items.ToList<object>());
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }

        /// <summary>
        /// default select method
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/crud/company/filter")]
        [HttpPost()]
        public async Task<JsonResult> Filter([FromBody] RequestMessage request)
        {
            var response = new BatchResponse<Company>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName, out RequestPagination pagination);
                var filter = request.DeserializeTo<CompanyFilter>("filter", compressed: compressed, mandatory: false);
                using (var context = AppDatabaseMysql.CreateConnection())
                {
                    var query = context.GetDbSet<Company>().OrderBy(i => i.AccountIdParent.Cnpj).AsQueryable();
					query = FilterResults(query, filter, context);

                    response.Entities = pagination.Paginate(query);
                    response.Pagination = pagination;

                    await LoadCustomFields(response.Entities, context);
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }

        /// <summary>
        /// default insert method
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/crud/company/insert")]
        [HttpPost()]
        public JsonResult Insert([FromBody] RequestMessage request)
        {
            var response = new BatchResponse<Company>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                var model = request.DeserializeTo<Company>("model", compressed: compressed, mandatory: false);
                var items = request.DeserializeTo<List<Company>>(FieldConstants.ENTITIES, compressed: compressed, mandatory: false);
                
				if (model != null)
                    items = new List<Company> { model };

                var existingItems = items.Where(i => i.Id > 0).ToList();
                var nonExintingItems = items.Where(i => i.Id <= 0).ToList();
                if (nonExintingItems?.Count > 0)
				{
					using (var context = AppDatabaseMysql.CreateConnection(true))
					{
						context.AddRange(nonExintingItems);
						foreach (var item in nonExintingItems)
							LogManager.Crud(userId, item.Summary, LogActionsEnum.Insert, Company.CLASS_NAME, context);
					}
				}

                response.Entities = nonExintingItems;
                if (existingItems.Count > 0)
                    throw new Exception("Alguns itens foram ignorados por já existirem na base de dados");
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
        /// <summary>
        /// default delete method
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/crud/company/delete")]
        [HttpPost()]
        public JsonResult Delete([FromBody] RequestMessage request)
        {
            var response = new StatusResponse();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                var ids = request.DeserializeTo<List<int>>(FieldConstants.ENTITIES, compressed: compressed);
				if (ids?.Count > 0)
					using (var context = AppDatabaseMysql.CreateConnection(true))
					{
						var items = context.GetDbSet<Company>().Where(i => ids.Contains(i.Id)).ToList();
						if (items.Count > 0)
						{
							context.RemoveRange(items);
							foreach (var item in items)
								LogManager.Crud(userId, item.Summary, LogActionsEnum.Delete, Company.CLASS_NAME, context);
						}
					}

                response.Success = true;
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
        /// <summary>
        /// default update method
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/crud/company/update")]
        [HttpPost()]
        public JsonResult Update([FromBody] RequestMessage request)
        {
            var response = new StatusResponse();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                var model = request.DeserializeTo<Company>("model", compressed: compressed, mandatory: false);
                var entities = request.DeserializeTo<List<Company>>(FieldConstants.ENTITIES, compressed: compressed, mandatory: false);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                {
                    if (model != null)
                        entities = new List<Company> { model };

                    var ids = entities.Select(i => i.Id).ToList();
                    var items = context.GetDbSet<Company>().Where(i => ids.Contains(i.Id)).ToList();
                    foreach (var item in items)
                    {
                        var entity = entities.Where(i => i.Id.Equals(item.Id)).FirstOrDefault();
                        if (item != null && entity != null)
                        {
                            item.Id = entity.Id;
							item.IsGroup = entity.IsGroup;
							item.ParentCompanyId = entity.ParentCompanyId;
							item.Cnpj = entity.Cnpj;
							item.Rank = entity.Rank;
							item.Phone = entity.Phone;
							item.UserId = entity.UserId;
							item.Capital = entity.Capital;
							item.Latitude = entity.Latitude;
							item.AccountId = entity.AccountId;
							item.Longitude = entity.Longitude;
							item.Timestamp = entity.Timestamp;
							item.FormalName = entity.FormalName;
							item.WalletLand = entity.WalletLand;
							item.DateOpened = entity.DateOpened;
							item.Description = entity.Description;
							item.FantasyName = entity.FantasyName;
							item.SituationId = entity.SituationId;
							item.WalletMobile = entity.WalletMobile;
							item.SalesForceId = entity.SalesForceId;
							item.JuridicNature = entity.JuridicNature;
							item.DateLastUpdate = entity.DateLastUpdate;
							item.PrimaryActivity = entity.PrimaryActivity;
							item.CompanySubtypeId = entity.CompanySubtypeId;
							item.ConvergingMessage = entity.ConvergingMessage;

                            item.SetStateModified(context);
							LogManager.Crud(userId, entity.Summary, LogActionsEnum.Update, Company.CLASS_NAME, context);
                        }
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
        /// <summary>
        /// default update property method
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/crud/company/update_property")]
        [HttpPost()]
        public async Task<JsonResult> UpdateProperty([FromBody] RequestMessage request)
        {
            var response = new StatusResponse();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId);
                var entityId = request.DeserializeTo<int>(FieldConstants.ENTITY, compressed: compressed);
                var entityProperty = request.DeserializeTo<string>("property", compressed: compressed);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                {
                    var entity = await context.GetDbSet<Company>().Where(i => i.Id.Equals(entityId)).FirstOrDefaultAsync();
                    if (entity == null)
                        throw new Exception("A entidade selecionada não existe mais no banco de dados...");

					if (entityProperty.Equals(Company.PROPERTY_ACCOUNT_ID))
					{
						var accountIdValue = request.DeserializeTo<int>(Company.PROPERTY_ACCOUNT_ID, compressed: compressed);
                        LogManager.PropertyChange(userId, Company.CLASS_NAME, Company.PROPERTY_ACCOUNT_ID, entity.AccountId, accountIdValue, context);

                        entity.AccountId = accountIdValue;
					}
					else if (entityProperty.Equals(Company.PROPERTY_USER_ID))
					{
						var userIdValue = request.DeserializeTo<int?>(Company.PROPERTY_USER_ID, compressed: compressed);
                        LogManager.PropertyChange(userId, Company.CLASS_NAME, Company.PROPERTY_USER_ID, entity.UserId, userIdValue, context);

                        entity.UserId = userIdValue;
					}
					else if (entityProperty.Equals(Company.PROPERTY_COMPANY_SUBTYPE_ID))
					{
						var companySubtypeIdValue = request.DeserializeTo<int?>(Company.PROPERTY_COMPANY_SUBTYPE_ID, compressed: compressed);
                        LogManager.PropertyChange(userId, Company.CLASS_NAME, Company.PROPERTY_COMPANY_SUBTYPE_ID, entity.CompanySubtypeId, companySubtypeIdValue, context);

                        entity.CompanySubtypeId = companySubtypeIdValue;
					}
					else if (entityProperty.Equals(Company.PROPERTY_CNPJ))
					{
						var cnpjValue = request.DeserializeTo<string>(Company.PROPERTY_CNPJ, compressed: compressed);
                        LogManager.PropertyChange(userId, Company.CLASS_NAME, Company.PROPERTY_CNPJ, entity.Cnpj, cnpjValue, context);

                        entity.Cnpj = cnpjValue;
					}
					else if (entityProperty.Equals(Company.PROPERTY_PHONE))
					{
						var phoneValue = request.DeserializeTo<string>(Company.PROPERTY_PHONE, compressed: compressed);
                        LogManager.PropertyChange(userId, Company.CLASS_NAME, Company.PROPERTY_PHONE, entity.Phone, phoneValue, context);

                        entity.Phone = phoneValue;
					}
					else if (entityProperty.Equals(Company.PROPERTY_DESCRIPTION))
					{
						var descriptionValue = request.DeserializeTo<string>(Company.PROPERTY_DESCRIPTION, compressed: compressed);
                        LogManager.PropertyChange(userId, Company.CLASS_NAME, Company.PROPERTY_DESCRIPTION, entity.Description, descriptionValue, context);

                        entity.Description = descriptionValue;
					}
					else if (entityProperty.Equals(Company.PROPERTY_FORMAL_NAME))
					{
						var formalNameValue = request.DeserializeTo<string>(Company.PROPERTY_FORMAL_NAME, compressed: compressed);
                        LogManager.PropertyChange(userId, Company.CLASS_NAME, Company.PROPERTY_FORMAL_NAME, entity.FormalName, formalNameValue, context);

                        entity.FormalName = formalNameValue;
					}
					else if (entityProperty.Equals(Company.PROPERTY_FANTASY_NAME))
					{
						var fantasyNameValue = request.DeserializeTo<string>(Company.PROPERTY_FANTASY_NAME, compressed: compressed);
                        LogManager.PropertyChange(userId, Company.CLASS_NAME, Company.PROPERTY_FANTASY_NAME, entity.FantasyName, fantasyNameValue, context);

                        entity.FantasyName = fantasyNameValue;
					}
					else if (entityProperty.Equals(Company.PROPERTY_LATITUDE))
					{
						var latitudeValue = request.DeserializeTo<double?>(Company.PROPERTY_LATITUDE, compressed: compressed);
                        LogManager.PropertyChange(userId, Company.CLASS_NAME, Company.PROPERTY_LATITUDE, entity.Latitude, latitudeValue, context);

                        entity.Latitude = latitudeValue;
					}
					else if (entityProperty.Equals(Company.PROPERTY_LONGITUDE))
					{
						var longitudeValue = request.DeserializeTo<double?>(Company.PROPERTY_LONGITUDE, compressed: compressed);
                        LogManager.PropertyChange(userId, Company.CLASS_NAME, Company.PROPERTY_LONGITUDE, entity.Longitude, longitudeValue, context);

                        entity.Longitude = longitudeValue;
					}
					else if (entityProperty.Equals(Company.PROPERTY_TIMESTAMP))
					{
						var timestampValue = request.DeserializeTo<DateTime>(Company.PROPERTY_TIMESTAMP, compressed: compressed);
                        LogManager.PropertyChange(userId, Company.CLASS_NAME, Company.PROPERTY_TIMESTAMP, entity.Timestamp, timestampValue, context);

                        entity.Timestamp = timestampValue;
					}
					else if (entityProperty.Equals(Company.PROPERTY_RANK))
					{
						var rankValue = request.DeserializeTo<string>(Company.PROPERTY_RANK, compressed: compressed);
                        LogManager.PropertyChange(userId, Company.CLASS_NAME, Company.PROPERTY_RANK, entity.Rank, rankValue, context);

                        entity.Rank = rankValue;
					}
					else if (entityProperty.Equals(Company.PROPERTY_PRIMARY_ACTIVITY))
					{
						var primaryActivityValue = request.DeserializeTo<string>(Company.PROPERTY_PRIMARY_ACTIVITY, compressed: compressed);
                        LogManager.PropertyChange(userId, Company.CLASS_NAME, Company.PROPERTY_PRIMARY_ACTIVITY, entity.PrimaryActivity, primaryActivityValue, context);

                        entity.PrimaryActivity = primaryActivityValue;
					}
					else if (entityProperty.Equals(Company.PROPERTY_JURIDIC_NATURE))
					{
						var juridicNatureValue = request.DeserializeTo<string>(Company.PROPERTY_JURIDIC_NATURE, compressed: compressed);
                        LogManager.PropertyChange(userId, Company.CLASS_NAME, Company.PROPERTY_JURIDIC_NATURE, entity.JuridicNature, juridicNatureValue, context);

                        entity.JuridicNature = juridicNatureValue;
					}
					else if (entityProperty.Equals(Company.PROPERTY_CONVERGING_MESSAGE))
					{
						var convergingMessageValue = request.DeserializeTo<string>(Company.PROPERTY_CONVERGING_MESSAGE, compressed: compressed);
                        LogManager.PropertyChange(userId, Company.CLASS_NAME, Company.PROPERTY_CONVERGING_MESSAGE, entity.ConvergingMessage, convergingMessageValue, context);

                        entity.ConvergingMessage = convergingMessageValue;
					}
					else if (entityProperty.Equals(Company.PROPERTY_WALLET_MOBILE))
					{
						var walletMobileValue = request.DeserializeTo<string>(Company.PROPERTY_WALLET_MOBILE, compressed: compressed);
                        LogManager.PropertyChange(userId, Company.CLASS_NAME, Company.PROPERTY_WALLET_MOBILE, entity.WalletMobile, walletMobileValue, context);

                        entity.WalletMobile = walletMobileValue;
					}
					else if (entityProperty.Equals(Company.PROPERTY_WALLET_LAND))
					{
						var walletLandValue = request.DeserializeTo<string>(Company.PROPERTY_WALLET_LAND, compressed: compressed);
                        LogManager.PropertyChange(userId, Company.CLASS_NAME, Company.PROPERTY_WALLET_LAND, entity.WalletLand, walletLandValue, context);

                        entity.WalletLand = walletLandValue;
					}
					else if (entityProperty.Equals(Company.PROPERTY_CAPITAL))
					{
						var capitalValue = request.DeserializeTo<double?>(Company.PROPERTY_CAPITAL, compressed: compressed);
                        LogManager.PropertyChange(userId, Company.CLASS_NAME, Company.PROPERTY_CAPITAL, entity.Capital, capitalValue, context);

                        entity.Capital = capitalValue;
					}
					else if (entityProperty.Equals(Company.PROPERTY_DATE_OPENED))
					{
						var dateOpenedValue = request.DeserializeTo<DateTime?>(Company.PROPERTY_DATE_OPENED, compressed: compressed);
                        LogManager.PropertyChange(userId, Company.CLASS_NAME, Company.PROPERTY_DATE_OPENED, entity.DateOpened, dateOpenedValue, context);

                        entity.DateOpened = dateOpenedValue;
					}
					else if (entityProperty.Equals(Company.PROPERTY_SITUATION_ID))
					{
						var situationIdValue = request.DeserializeTo<int?>(Company.PROPERTY_SITUATION_ID, compressed: compressed);
                        LogManager.PropertyChange(userId, Company.CLASS_NAME, Company.PROPERTY_SITUATION_ID, entity.SituationId, situationIdValue, context);

                        entity.SituationId = situationIdValue;
					}
					else if (entityProperty.Equals(Company.PROPERTY_SALES_FORCE_ID))
					{
						var salesForceIdValue = request.DeserializeTo<string>(Company.PROPERTY_SALES_FORCE_ID, compressed: compressed);
                        LogManager.PropertyChange(userId, Company.CLASS_NAME, Company.PROPERTY_SALES_FORCE_ID, entity.SalesForceId, salesForceIdValue, context);

                        entity.SalesForceId = salesForceIdValue;
					}
					else if (entityProperty.Equals(Company.PROPERTY_DATE_LAST_UPDATE))
					{
						var dateLastUpdateValue = request.DeserializeTo<DateTime?>(Company.PROPERTY_DATE_LAST_UPDATE, compressed: compressed);
                        LogManager.PropertyChange(userId, Company.CLASS_NAME, Company.PROPERTY_DATE_LAST_UPDATE, entity.DateLastUpdate, dateLastUpdateValue, context);

                        entity.DateLastUpdate = dateLastUpdateValue;
					}
                    entity.SetStateModified(context);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
    }
}