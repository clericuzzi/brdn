using System;
using System.Linq;
using RestApi.Database;
using RestApi.Entities;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Clericuzzi.WebApi.Entities;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Clericuzzi.Lib.Http.Utils.Constants;
using Clericuzzi.WebApi.Entities.Business;
using Clericuzzi.WebApi.Data.DatabaseContext;
using Clericuzzi.WebApi.Entities.Business.Enums;
using Clericuzzi.Lib.Http.Communication.Request;
using Clericuzzi.Lib.Http.Communication.Response;
using Clericuzzi.WebApi.Entities.Controllers.Crud;
using Clericuzzi.WebApi.Entities.Business.Managers;
using Clericuzzi.Lib.Http.Communication.Response.Json;
using Clericuzzi.WebApi.Data.Business.DatabaseContext;
using Clericuzzi.WebApi.Entities.Business.Models.Crud;
using Clericuzzi.Lib.Http.Communication.Request.Pagination;
using RestApi.Business.Models;

namespace RestApi.Entities.Controllers.Crud
{
    /// <summary>
    /// A class for default crud actions regarding the `company_line` table
    /// </summary>
    public partial class CompanyLineCrudController : Controller
    {
        /// <summary>
        /// default listing method
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/crud/company_line/listing")]
        [HttpPost()]
        public JsonResult Listing([FromBody] RequestMessage request)
        {
            var response = new BatchResponseList();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                var filter = request.DeserializeTo<CompanyLineFilter>("filter", compressed: compressed, mandatory: false);
                using (var context = AppDatabaseMysql.CreateConnection())
                {
                    var query = context.GetDbSet<CompanyLine>().AsQueryable();
                    query = FilterResults(query, filter, context);
                    var items = query.Select(i => GetListing(i)).ToList();
                    items = items.OrderBy(i => i.Label).ToList();

                    response.AddEntityList(CompanyLine.TABLE_NAME, items.ToList<object>());
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }

        /// <summary>
        /// default select method
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/crud/company_line/filter")]
        [HttpPost()]
        public async Task<JsonResult> Filter([FromBody] RequestMessage request)
        {
            var response = new BatchResponse<LineItem>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName, out RequestPagination pagination);
                var filter = request.DeserializeTo<CompanyLineFilter>("filter", compressed: compressed, mandatory: false);
                using (var context = AppDatabaseMysql.CreateConnection())
                {
                    var query = context.GetDbSet<CompanyLine>().Include(i => i.OperatorIdParent).OrderBy(i => i.Phone).AsQueryable();
                    query = FilterResults(query, filter, context);

                    var entities = pagination.Paginate(query);
                    var items = new List<LineItem>();
                    entities.ForEach(i => items.Add(new LineItem { Id = i.Id, CompanyId = i.CompanyId, CompanyIdParent = i.CompanyIdParent, OperatorId = i.OperatorId, OperatorIdParent = i.OperatorIdParent, OperatorSince = i.OperatorSince, Phone = i.Phone, TestingTimestamp = i.TestingTimestamp }));

                    await LoadCustomFields(items, context);

                    response.Entities = items;
                    response.Pagination = pagination;
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }

        /// <summary>
        /// default insert method
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/crud/company_line/insert")]
        [HttpPost()]
        public JsonResult Insert([FromBody] RequestMessage request)
        {
            var response = new BatchResponse<CompanyLine>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                var model = request.DeserializeTo<CompanyLine>("model", compressed: compressed, mandatory: false);
                var items = request.DeserializeTo<List<CompanyLine>>(FieldConstants.ENTITIES, compressed: compressed, mandatory: false);

                if (model != null)
                    items = new List<CompanyLine> { model };

                var existingItems = items.Where(i => i.Id > 0).ToList();
                var nonExintingItems = items.Where(i => i.Id <= 0).ToList();
                if (nonExintingItems?.Count > 0)
                {
                    using (var context = AppDatabaseMysql.CreateConnection(true))
                    {
                        context.AddRange(nonExintingItems);
                        foreach (var item in nonExintingItems)
                            LogManager.Crud(userId, item.Summary, LogActionsEnum.Insert, CompanyLine.CLASS_NAME, context);
                    }
                }

                response.Entities = nonExintingItems;
                if (existingItems.Count > 0)
                    throw new Exception("Alguns itens foram ignorados por já existirem na base de dados");
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
        /// <summary>
        /// default delete method
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/crud/company_line/delete")]
        [HttpPost()]
        public JsonResult Delete([FromBody] RequestMessage request)
        {
            var response = new StatusResponse();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                var ids = request.DeserializeTo<List<int>>(FieldConstants.ENTITIES, compressed: compressed);
                if (ids?.Count > 0)
                    using (var context = AppDatabaseMysql.CreateConnection(true))
                    {
                        var items = context.GetDbSet<CompanyLine>().Where(i => ids.Contains(i.Id)).ToList();
                        if (items.Count > 0)
                        {
                            context.RemoveRange(items);
                            foreach (var item in items)
                                LogManager.Crud(userId, item.Summary, LogActionsEnum.Delete, CompanyLine.CLASS_NAME, context);
                        }
                    }

                response.Success = true;
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
        /// <summary>
        /// default update method
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/crud/company_line/update")]
        [HttpPost()]
        public JsonResult Update([FromBody] RequestMessage request)
        {
            var response = new StatusResponse();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                var model = request.DeserializeTo<LineItem>("model", compressed: compressed, mandatory: false);
                var entities = request.DeserializeTo<List<LineItem>>(FieldConstants.ENTITIES, compressed: compressed, mandatory: false);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                {
                    if (model != null)
                        entities = new List<LineItem> { model };

                    var ids = entities.Select(i => i.Id).ToList();
                    var items = context.GetDbSet<CompanyLine>().Where(i => ids.Contains(i.Id)).ToList();
                    foreach (var item in items)
                    {
                        var entity = entities.Where(i => i.Id.Equals(item.Id)).FirstOrDefault();
                        if (item != null && entity != null)
                        {
                            item.Id = entity.Id;
                            item.Phone = entity.Phone;
                            item.CompanyId = entity.CompanyId;
                            item.OperatorId = null;
                            item.OperatorSince = null;
                            item.TestingTimestamp = null;

                            item.SetStateModified(context);
                            LogManager.Crud(userId, entity.Summary, LogActionsEnum.Update, CompanyLine.CLASS_NAME, context);

                            var operators = context.GetDbSet<CompanyLineOperator>().Where(i => i.CompanyLineId.Equals(item.Id)).ToList();
                            if (operators?.Count > 0)
                                context.RemoveRange(operators);
                        }
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
        /// <summary>
        /// default update property method
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/crud/company_line/update_property")]
        [HttpPost()]
        public async Task<JsonResult> UpdateProperty([FromBody] RequestMessage request)
        {
            var response = new StatusResponse();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId);
                var entityId = request.DeserializeTo<int>(FieldConstants.ENTITY, compressed: compressed);
                var entityProperty = request.DeserializeTo<string>("property", compressed: compressed);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                {
                    var entity = await context.GetDbSet<CompanyLine>().Where(i => i.Id.Equals(entityId)).FirstOrDefaultAsync();
                    if (entity == null)
                        throw new Exception("A entidade selecionada não existe mais no banco de dados...");

                    if (entityProperty.Equals(CompanyLine.PROPERTY_COMPANY_ID))
                    {
                        var companyIdValue = request.DeserializeTo<int>(CompanyLine.PROPERTY_COMPANY_ID, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyLine.CLASS_NAME, CompanyLine.PROPERTY_COMPANY_ID, entity.CompanyId, companyIdValue, context);

                        entity.CompanyId = companyIdValue;
                    }
                    else if (entityProperty.Equals(CompanyLine.PROPERTY_PHONE))
                    {
                        var phoneValue = request.DeserializeTo<string>(CompanyLine.PROPERTY_PHONE, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyLine.CLASS_NAME, CompanyLine.PROPERTY_PHONE, entity.Phone, phoneValue, context);

                        entity.Phone = phoneValue;
                    }
                    else if (entityProperty.Equals(CompanyLine.PROPERTY_OPERATOR_ID))
                    {
                        var operatorIdValue = request.DeserializeTo<int?>(CompanyLine.PROPERTY_OPERATOR_ID, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyLine.CLASS_NAME, CompanyLine.PROPERTY_OPERATOR_ID, entity.OperatorId, operatorIdValue, context);

                        entity.OperatorId = operatorIdValue;
                    }
                    else if (entityProperty.Equals(CompanyLine.PROPERTY_OPERATOR_SINCE))
                    {
                        var operatorSinceValue = request.DeserializeTo<DateTime?>(CompanyLine.PROPERTY_OPERATOR_SINCE, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyLine.CLASS_NAME, CompanyLine.PROPERTY_OPERATOR_SINCE, entity.OperatorSince, operatorSinceValue, context);

                        entity.OperatorSince = operatorSinceValue;
                    }
                    else if (entityProperty.Equals(CompanyLine.PROPERTY_TESTING_TIMESTAMP))
                    {
                        var testingTimestampValue = request.DeserializeTo<DateTime?>(CompanyLine.PROPERTY_TESTING_TIMESTAMP, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyLine.CLASS_NAME, CompanyLine.PROPERTY_TESTING_TIMESTAMP, entity.TestingTimestamp, testingTimestampValue, context);

                        entity.TestingTimestamp = testingTimestampValue;
                    }
                    entity.SetStateModified(context);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
    }
}