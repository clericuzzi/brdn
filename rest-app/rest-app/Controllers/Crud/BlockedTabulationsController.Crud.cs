using System;
using System.Linq;
using RestApi.Database;
using RestApi.Entities;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Clericuzzi.WebApi.Entities;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Clericuzzi.Lib.Http.Utils.Constants;
using Clericuzzi.WebApi.Entities.Business;
using Clericuzzi.WebApi.Data.DatabaseContext;
using Clericuzzi.WebApi.Entities.Business.Enums;
using Clericuzzi.Lib.Http.Communication.Request;
using Clericuzzi.Lib.Http.Communication.Response;
using Clericuzzi.WebApi.Entities.Controllers.Crud;
using Clericuzzi.WebApi.Entities.Business.Managers;
using Clericuzzi.Lib.Http.Communication.Response.Json;
using Clericuzzi.WebApi.Data.Business.DatabaseContext;
using Clericuzzi.WebApi.Entities.Business.Models.Crud;
using Clericuzzi.Lib.Http.Communication.Request.Pagination;

namespace RestApi.Entities.Controllers.Crud
{
    /// <summary>
    /// A class for default crud actions regarding the `blocked_tabulations` table
    /// </summary>
    public partial class BlockedTabulationsCrudController : Controller
    {
        /// <summary>
        /// default listing method
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/crud/blocked_tabulations/listing")]
        [HttpPost()]
        public JsonResult Listing([FromBody] RequestMessage request)
        {
            var response = new BatchResponseList();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                var filter = request.DeserializeTo<BlockedTabulationsFilter>("filter", compressed: compressed, mandatory: false);
                using (var context = AppDatabaseMysql.CreateConnection())
                {
                    var query = context.GetDbSet<BlockedTabulations>().AsQueryable();
                    query = FilterResults(query, filter, context);
                    var items = query.Select(i => GetListing(i)).ToList();
                    items = items.OrderBy(i => i.Label).ToList();

                    response.AddEntityList(BlockedTabulations.TABLE_NAME, items.ToList<object>());
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }

        /// <summary>
        /// default select method
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/crud/blocked_tabulations/filter")]
        [HttpPost()]
        public async Task<JsonResult> Filter([FromBody] RequestMessage request)
        {
            var response = new BatchResponse<BlockedTabulations>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName, out RequestPagination pagination);
                var filter = request.DeserializeTo<BlockedTabulationsFilter>("filter", compressed: compressed, mandatory: false);
                using (var context = AppDatabaseMysql.CreateConnection())
                {
                    var query = context.GetDbSet<BlockedTabulations>().Include(i => i.TabulationIdParent).OrderBy(i => i.TabulationIdParent.TabulationTypeIdParent.Type).AsQueryable();
                    query = FilterResults(query, filter, context);

                    response.Entities = pagination.Paginate(query);
                    response.Pagination = pagination;

                    await LoadCustomFields(response.Entities, context);
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }

        /// <summary>
        /// default insert method
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/crud/blocked_tabulations/insert")]
        [HttpPost()]
        public JsonResult Insert([FromBody] RequestMessage request)
        {
            var response = new BatchResponse<BlockedTabulations>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                var model = request.DeserializeTo<BlockedTabulations>("model", compressed: compressed, mandatory: false);
                var items = request.DeserializeTo<List<BlockedTabulations>>(FieldConstants.ENTITIES, compressed: compressed, mandatory: false);

                if (model != null)
                    items = new List<BlockedTabulations> { model };

                var existingItems = items.Where(i => i.Id > 0).ToList();
                var nonExintingItems = items.Where(i => i.Id <= 0).ToList();
                if (nonExintingItems?.Count > 0)
                {
                    using (var context = AppDatabaseMysql.CreateConnection(true))
                    {
                        context.AddRange(nonExintingItems);
                        foreach (var item in nonExintingItems)
                            LogManager.Crud(userId, item.Summary, LogActionsEnum.Insert, BlockedTabulations.CLASS_NAME, context);
                    }
                }

                response.Entities = nonExintingItems;
                if (existingItems.Count > 0)
                    throw new Exception("Alguns itens foram ignorados por já existirem na base de dados");
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
        /// <summary>
        /// default delete method
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/crud/blocked_tabulations/delete")]
        [HttpPost()]
        public JsonResult Delete([FromBody] RequestMessage request)
        {
            var response = new StatusResponse();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                var ids = request.DeserializeTo<List<int>>(FieldConstants.ENTITIES, compressed: compressed);
                if (ids?.Count > 0)
                    using (var context = AppDatabaseMysql.CreateConnection(true))
                    {
                        var items = context.GetDbSet<BlockedTabulations>().Where(i => ids.Contains(i.Id)).ToList();
                        if (items.Count > 0)
                        {
                            context.RemoveRange(items);
                            foreach (var item in items)
                                LogManager.Crud(userId, item.Summary, LogActionsEnum.Delete, BlockedTabulations.CLASS_NAME, context);
                        }
                    }

                response.Success = true;
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
        /// <summary>
        /// default update method
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/crud/blocked_tabulations/update")]
        [HttpPost()]
        public JsonResult Update([FromBody] RequestMessage request)
        {
            var response = new StatusResponse();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                var model = request.DeserializeTo<BlockedTabulations>("model", compressed: compressed, mandatory: false);
                var entities = request.DeserializeTo<List<BlockedTabulations>>(FieldConstants.ENTITIES, compressed: compressed, mandatory: false);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                {
                    if (model != null)
                        entities = new List<BlockedTabulations> { model };

                    var ids = entities.Select(i => i.Id).ToList();
                    var items = context.GetDbSet<BlockedTabulations>().Where(i => ids.Contains(i.Id)).ToList();
                    foreach (var item in items)
                    {
                        var entity = entities.Where(i => i.Id.Equals(item.Id)).FirstOrDefault();
                        if (item != null && entity != null)
                        {
                            item.Id = entity.Id;
                            item.TabulationId = entity.TabulationId;

                            item.SetStateModified(context);
                            LogManager.Crud(userId, entity.Summary, LogActionsEnum.Update, BlockedTabulations.CLASS_NAME, context);
                        }
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
        /// <summary>
        /// default update property method
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/crud/blocked_tabulations/update_property")]
        [HttpPost()]
        public async Task<JsonResult> UpdateProperty([FromBody] RequestMessage request)
        {
            var response = new StatusResponse();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId);
                var entityId = request.DeserializeTo<int>(FieldConstants.ENTITY, compressed: compressed);
                var entityProperty = request.DeserializeTo<string>("property", compressed: compressed);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                {
                    var entity = await context.GetDbSet<BlockedTabulations>().Where(i => i.Id.Equals(entityId)).FirstOrDefaultAsync();
                    if (entity == null)
                        throw new Exception("A entidade selecionada não existe mais no banco de dados...");

                    if (entityProperty.Equals(BlockedTabulations.PROPERTY_TABULATION_ID))
                    {
                        var tabulationIdValue = request.DeserializeTo<int>(BlockedTabulations.PROPERTY_TABULATION_ID, compressed: compressed);
                        LogManager.PropertyChange(userId, BlockedTabulations.CLASS_NAME, BlockedTabulations.PROPERTY_TABULATION_ID, entity.TabulationId, tabulationIdValue, context);

                        entity.TabulationId = tabulationIdValue;
                    }
                    entity.SetStateModified(context);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
    }
}