using System;
using System.Linq;
using RestApi.Database;
using RestApi.Entities;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Clericuzzi.WebApi.Entities;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Clericuzzi.Lib.Http.Utils.Constants;
using Clericuzzi.WebApi.Entities.Business;
using Clericuzzi.WebApi.Data.DatabaseContext;
using Clericuzzi.WebApi.Entities.Business.Enums;
using Clericuzzi.Lib.Http.Communication.Request;
using Clericuzzi.Lib.Http.Communication.Response;
using Clericuzzi.WebApi.Entities.Controllers.Crud;
using Clericuzzi.WebApi.Entities.Business.Managers;
using Clericuzzi.Lib.Http.Communication.Response.Json;
using Clericuzzi.WebApi.Data.Business.DatabaseContext;
using Clericuzzi.WebApi.Entities.Business.Models.Crud;
using Clericuzzi.Lib.Http.Communication.Request.Pagination;

namespace RestApi.Entities.Controllers.Crud
{
    /// <summary>
    /// A class for default crud actions regarding the `company_address` table
    /// </summary>
    public partial class CompanyAddressCrudController : Controller
    {
        /// <summary>
        /// default listing method
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/crud/company_address/listing")]
        [HttpPost()]
        public JsonResult Listing([FromBody] RequestMessage request)
        {
            var response = new BatchResponseList();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                var filter = request.DeserializeTo<CompanyAddressFilter>("filter", compressed: compressed, mandatory: false);
                using (var context = AppDatabaseMysql.CreateConnection())
                {
                    var query = context.GetDbSet<CompanyAddress>().AsQueryable();
                    query = FilterResults(query, filter, context);
                    var items = query.Select(i => GetListing(i)).ToList();
                    items = items.OrderBy(i => i.Label).ToList();

                    response.AddEntityList(CompanyAddress.TABLE_NAME, items.ToList<object>());
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }

        /// <summary>
        /// default select method
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/crud/company_address/filter")]
        [HttpPost()]
        public async Task<JsonResult> Filter([FromBody] RequestMessage request)
        {
            var response = new BatchResponse<CompanyAddress>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName, out RequestPagination pagination);
                var filter = request.DeserializeTo<CompanyAddressFilter>("filter", compressed: compressed, mandatory: false);
                using (var context = AppDatabaseMysql.CreateConnection())
                {
                    var query = context.GetDbSet<CompanyAddress>().OrderBy(i => i.CompanyIdParent.AccountIdParent.Cnpj).AsQueryable();
					query = FilterResults(query, filter, context);

                    response.Entities = pagination.Paginate(query);
                    response.Pagination = pagination;

                    await LoadCustomFields(response.Entities, context);
                }
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }

        /// <summary>
        /// default insert method
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/crud/company_address/insert")]
        [HttpPost()]
        public JsonResult Insert([FromBody] RequestMessage request)
        {
            var response = new BatchResponse<CompanyAddress>();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                var model = request.DeserializeTo<CompanyAddress>("model", compressed: compressed, mandatory: false);
                var items = request.DeserializeTo<List<CompanyAddress>>(FieldConstants.ENTITIES, compressed: compressed, mandatory: false);
                
				if (model != null)
                    items = new List<CompanyAddress> { model };

                var existingItems = items.Where(i => i.Id > 0).ToList();
                var nonExintingItems = items.Where(i => i.Id <= 0).ToList();
                if (nonExintingItems?.Count > 0)
				{
					using (var context = AppDatabaseMysql.CreateConnection(true))
					{
						context.AddRange(nonExintingItems);
						foreach (var item in nonExintingItems)
							LogManager.Crud(userId, item.Summary, LogActionsEnum.Insert, CompanyAddress.CLASS_NAME, context);
					}
				}

                response.Entities = nonExintingItems;
                if (existingItems.Count > 0)
                    throw new Exception("Alguns itens foram ignorados por já existirem na base de dados");
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
        /// <summary>
        /// default delete method
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/crud/company_address/delete")]
        [HttpPost()]
        public JsonResult Delete([FromBody] RequestMessage request)
        {
            var response = new StatusResponse();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                var ids = request.DeserializeTo<List<int>>(FieldConstants.ENTITIES, compressed: compressed);
				if (ids?.Count > 0)
					using (var context = AppDatabaseMysql.CreateConnection(true))
					{
						var items = context.GetDbSet<CompanyAddress>().Where(i => ids.Contains(i.Id)).ToList();
						if (items.Count > 0)
						{
							context.RemoveRange(items);
							foreach (var item in items)
								LogManager.Crud(userId, item.Summary, LogActionsEnum.Delete, CompanyAddress.CLASS_NAME, context);
						}
					}

                response.Success = true;
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
        /// <summary>
        /// default update method
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/crud/company_address/update")]
        [HttpPost()]
        public JsonResult Update([FromBody] RequestMessage request)
        {
            var response = new StatusResponse();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId, out string machineName);
                var model = request.DeserializeTo<CompanyAddress>("model", compressed: compressed, mandatory: false);
                var entities = request.DeserializeTo<List<CompanyAddress>>(FieldConstants.ENTITIES, compressed: compressed, mandatory: false);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                {
                    if (model != null)
                        entities = new List<CompanyAddress> { model };

                    var ids = entities.Select(i => i.Id).ToList();
                    var items = context.GetDbSet<CompanyAddress>().Where(i => ids.Contains(i.Id)).ToList();
                    foreach (var item in items)
                    {
                        var entity = entities.Where(i => i.Id.Equals(item.Id)).FirstOrDefault();
                        if (item != null && entity != null)
                        {
                            item.Id = entity.Id;
							item.Street = entity.Street;
							item.Number = entity.Number;
							item.IsValid = entity.IsValid;
							item.CompanyId = entity.CompanyId;
							item.ZipcodeId = entity.ZipcodeId;
							item.Reference = entity.Reference;
							item.Compliment = entity.Compliment;
							item.TestingTimestamp = entity.TestingTimestamp;

                            item.SetStateModified(context);
							LogManager.Crud(userId, entity.Summary, LogActionsEnum.Update, CompanyAddress.CLASS_NAME, context);
                        }
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
        /// <summary>
        /// default update property method
        /// </summary>
        /// <param name="request">the request information</param>
        [Route("api/crud/company_address/update_property")]
        [HttpPost()]
        public async Task<JsonResult> UpdateProperty([FromBody] RequestMessage request)
        {
            var response = new StatusResponse();
            try
            {
                request.ReadConfig(out bool crypted, out bool compressed, out int userId, out int accountId);
                var entityId = request.DeserializeTo<int>(FieldConstants.ENTITY, compressed: compressed);
                var entityProperty = request.DeserializeTo<string>("property", compressed: compressed);
                using (var context = AppDatabaseMysql.CreateConnection(true))
                {
                    var entity = await context.GetDbSet<CompanyAddress>().Where(i => i.Id.Equals(entityId)).FirstOrDefaultAsync();
                    if (entity == null)
                        throw new Exception("A entidade selecionada não existe mais no banco de dados...");

					if (entityProperty.Equals(CompanyAddress.PROPERTY_COMPANY_ID))
					{
						var companyIdValue = request.DeserializeTo<int>(CompanyAddress.PROPERTY_COMPANY_ID, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyAddress.CLASS_NAME, CompanyAddress.PROPERTY_COMPANY_ID, entity.CompanyId, companyIdValue, context);

                        entity.CompanyId = companyIdValue;
					}
					else if (entityProperty.Equals(CompanyAddress.PROPERTY_STREET))
					{
						var streetValue = request.DeserializeTo<string>(CompanyAddress.PROPERTY_STREET, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyAddress.CLASS_NAME, CompanyAddress.PROPERTY_STREET, entity.Street, streetValue, context);

                        entity.Street = streetValue;
					}
					else if (entityProperty.Equals(CompanyAddress.PROPERTY_NUMBER))
					{
						var numberValue = request.DeserializeTo<string>(CompanyAddress.PROPERTY_NUMBER, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyAddress.CLASS_NAME, CompanyAddress.PROPERTY_NUMBER, entity.Number, numberValue, context);

                        entity.Number = numberValue;
					}
					else if (entityProperty.Equals(CompanyAddress.PROPERTY_COMPLIMENT))
					{
						var complimentValue = request.DeserializeTo<string>(CompanyAddress.PROPERTY_COMPLIMENT, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyAddress.CLASS_NAME, CompanyAddress.PROPERTY_COMPLIMENT, entity.Compliment, complimentValue, context);

                        entity.Compliment = complimentValue;
					}
					else if (entityProperty.Equals(CompanyAddress.PROPERTY_ZIPCODE_ID))
					{
						var zipcodeIdValue = request.DeserializeTo<int?>(CompanyAddress.PROPERTY_ZIPCODE_ID, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyAddress.CLASS_NAME, CompanyAddress.PROPERTY_ZIPCODE_ID, entity.ZipcodeId, zipcodeIdValue, context);

                        entity.ZipcodeId = zipcodeIdValue;
					}
					else if (entityProperty.Equals(CompanyAddress.PROPERTY_REFERENCE))
					{
						var referenceValue = request.DeserializeTo<string>(CompanyAddress.PROPERTY_REFERENCE, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyAddress.CLASS_NAME, CompanyAddress.PROPERTY_REFERENCE, entity.Reference, referenceValue, context);

                        entity.Reference = referenceValue;
					}
					else if (entityProperty.Equals(CompanyAddress.PROPERTY_TESTING_TIMESTAMP))
					{
						var testingTimestampValue = request.DeserializeTo<DateTime?>(CompanyAddress.PROPERTY_TESTING_TIMESTAMP, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyAddress.CLASS_NAME, CompanyAddress.PROPERTY_TESTING_TIMESTAMP, entity.TestingTimestamp, testingTimestampValue, context);

                        entity.TestingTimestamp = testingTimestampValue;
					}
					else if (entityProperty.Equals(CompanyAddress.PROPERTY_IS_VALID))
					{
						var isValidValue = request.DeserializeTo<int?>(CompanyAddress.PROPERTY_IS_VALID, compressed: compressed);
                        LogManager.PropertyChange(userId, CompanyAddress.CLASS_NAME, CompanyAddress.PROPERTY_IS_VALID, entity.IsValid, isValidValue, context);

                        entity.IsValid = isValidValue;
					}
                    entity.SetStateModified(context);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                response.AddInnerMostException(ex);
            }

            return Json(response, JsonSettings.PASCAL_CASE);
        }
    }
}