using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Clericuzzi.WebApi.Entities;
using Microsoft.EntityFrameworkCore;
using Clericuzzi.WebApi.Data.DatabaseContext;
using Clericuzzi.WebApi.Entities.Business.Models.Crud;
using Clericuzzi.WebApi.Data.Business.DatabaseContext;

namespace RestApi.Entities.Controllers.Crud
{
    /// <summary>
    /// A class for project specific endpoints regarding the `macro_machine` table
    /// </summary>
    public partial class MacroMachineCrudController : Controller
    {
        /// <summary>
        /// Custom fields loader, implement only if needed
        /// </summary>
        /// <param name="items">the entity's list</param>
        /// <param name="context">database connection</param>
        /// <returns>laods the custom information into the given entities</returns>
        private async Task LoadCustomFields(List<MacroMachine> items, DataContext context)
        {
            await Task.Delay(150);
			// method sample
            // var peopleIds = items.Select(i => -i.PersonId).Distinct().ToList();
            // var productIds = items.Select(i => i.ProductId).Distinct().ToList();
            // var proposalIds = items.Select(i => i.Id).Distinct().ToList();

            // var people = context.GetDbSet<Person>().Where(i => peopleIds.Contains(i.Id)).ToList();
            // var products = context.GetDbSet<Product>().Where(i => productIds.Contains(i.Id)).ToList();
            // var userTasks = context.GetDbSet<UserTask>().Where(i => proposalIds.Contains(i.ProposalId)).ToList();

            // foreach (var item in items)
            //    item.LoadExternalInfo(people, products, userTasks, context);
        }

        /// <summary>
        /// default listing for the 'MacroMachine' class 
        /// </summary>
        /// <param name="item">the current instance</param>
        /// <returns>a new tuple model for showing in a combo or auto complete fields</returns>
        private TupleModel<int> GetListing(MacroMachine item)
        {
            return new TupleModel<int> { Id = item.Id, Label = item.Summary };
        }

		/// <summary>
        /// custom filters for the 'MacroMachine' class
        /// </summary>
        /// <param name="query">the ongoing query</param>
        /// <param name="filter">the filter object</param>
        /// <param name="context">database connection</param>
        /// <returns>the query filtered by the filter object</returns>
		public static IQueryable<MacroMachine> FilterResults(IQueryable<MacroMachine> query, MacroMachineFilter filter, DataContext context)
		{
			try
			{
				if (filter == null)
					return query;
				else
				{
					if (!string.IsNullOrEmpty(filter?.MachineName))
						query = query.Where(i => i.MachineName.Contains(filter.MachineName));

					return query;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}
    }
}