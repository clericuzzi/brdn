﻿using Clericuzzi.Lib.Utils.Core;

namespace RestApi.Utils.Constants
{
    public class ServerConstants
    {
        public static bool IsLocal { get { return Host?.Equals(MYSQL_HOST_LOCAL) ?? false; } }
        public static string Host = string.Empty;

        public static string MysqlBase(string database = null)
        {
            return database ?? MYSQL_BASE;
        }

        public static string MYSQL_BASE = "brdn_erp";
        public static string MYSQL_USER = "brdn_user";
        public static string MYSQL_PASS = "260CgeFbpG5@tQzXE&IJ8Rdx";

        public static string MYSQL_HOST_WEB = "54.70.216.46";
        public static string MYSQL_HOST_LOCAL = "localhost";

        const string MYSQL_CONNECTION_STRING = "Host=_(@)_;Port=3306;Database=_(@)_;Uid=_(@)_;Pwd=_(@)_;SslMode=none;Charset=utf8;";
        const string MYSQL_CONNECTION_STRING_LOCAL = "Host=_(@)_;Port=3306;Database=_(@)_;Uid=_(@)_;Pwd=_(@)_;Charset=utf8;";
        public static string ConnString(string database = null)
        {
            var conn = (IsLocal ? MYSQL_CONNECTION_STRING_LOCAL : MYSQL_CONNECTION_STRING).ReplaceWithParamsDefaultWildcard(Host, MysqlBase(database), MYSQL_USER, MYSQL_PASS);

            return conn;
        }
    }
}