﻿using Clericuzzi.WebApi.Data.DatabaseContext.Mysql;
using Clericuzzi.WebApi.Entities.Data.Connections.Mysql;

namespace RestApi.Database
{
    public class AppDatabaseMysql
    {
        public static MysqlContext CreateConnection(bool beginTransaction = false, int timeout = -1)
        {
            return MysqlConnectionFactory.CreateConnection(beginTransaction, timeout);
        }
    }
}