﻿using RestApi.Database;
using Clericuzzi.Lib.Email;
using System.Globalization;
using RestApi.Utils.Constants;
using Clericuzzi.Lib.Utils.Core;
using System.Collections.Generic;
using RestApi.Business.Scheduler;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Localization;
using Microsoft.Extensions.Configuration;
using Clericuzzi.WebApi.Entities.Business;
using Microsoft.Extensions.DependencyInjection;
using Clericuzzi.Lib.Http.Communication.Request;
using Clericuzzi.WebApi.Entities.Data.Connections.Mysql;

namespace RestApi
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        private Microsoft.AspNetCore.Hosting.IHostingEnvironment CurrentEnvironment { get; set; }
        public Startup(Microsoft.AspNetCore.Hosting.IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();

            Configuration = builder.Build();
            CurrentEnvironment = env;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            EmailManager.Config("Brdn scheduler", "cesar.britto.imoveis@gmail.com", "cesar.britto.imoveis@gmail.com", "abcd@1234", "smtp.gmail.com", 465);
            EntitiesManager.Init(() => AppDatabaseMysql.CreateConnection(true), null, null);

            services.AddLocalization(opts => { opts.ResourcesPath = "Resources"; });
            services.AddSingleton<IHostedService, ServiceScheduler>();

            services.AddCors(o => o.AddPolicy("MyPolicy", builder => { builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader(); }));
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, Microsoft.AspNetCore.Hosting.IHostingEnvironment env)
        {
            var ci = ConsoleUtils.SetBrazilCulture();
            CastUtils.SetCulture(ci);

            // Configure the Localization middleware
            app.UseRequestLocalization(new RequestLocalizationOptions
            {
                DefaultRequestCulture = new RequestCulture(ci),
                SupportedCultures = new List<CultureInfo> { ci },
                SupportedUICultures = new List<CultureInfo> { ci, }
            });

            RequestMessageDefaults.Crypted = false;
            RequestMessageDefaults.Compressed = false;
            CryptUtils.InitSecret("ikKRUxCmHL86nzBim&X0v1M*xm@CtGGx36V7usRrrXfHH#0$i!WuHeEDcJ%s4Wukvv8FU@Z9sOCPdeGj5!nTnLU&0Qd@iAX0i9J");
            ServerConstants.Host = ServerConstants.MYSQL_HOST_LOCAL;
            if (env.IsDevelopment())
            {
                ServerConstants.Host = ServerConstants.MYSQL_HOST_WEB;
                //ServerConstants.Host = ServerConstants.MYSQL_HOST_LOCAL;
                //ServerConstants.MYSQL_USER = ServerConstants.Host.Equals(ServerConstants.MYSQL_HOST_LOCAL) ? "root" : ServerConstants.MYSQL_USER;
                //ServerConstants.MYSQL_PASS = ServerConstants.Host.Equals(ServerConstants.MYSQL_HOST_LOCAL) ? "12qwaszx" : ServerConstants.MYSQL_PASS;
            }

            MysqlConnectionFactory.SetConnectionString(ServerConstants.ConnString());
            app.UseCors("MyPolicy");
            app.UseMvc();
        }
    }
}