﻿<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <title>Untitled Document</title>
      <style type="text/css">body {font-family:arial;margin:20px;}td {font-size:12px !important;padding:3px;}@page {margin-bottom:0;}</style>
   </head>
   <body>
      <table cellpadding="0" cellspacing="0" style="width:900px; margin: 0 auto; text-align: left;" border="0">
         <tbody>
            <tr>
               <td style="border-bottom: 1px dashed #000;font-weight: bold; background: #E4E6FF" colspan="4">Dados Cadastrais: </td>
            </tr>
            <tr>
               <td>Nome: @_NAME_@</td>
               <td>Beneficio: @_NB_@</td>
               <td>CPF: @_CPF_@</td>
               <td>Data Nascimento: @_BIRTH_@</td>
            </tr>
            <tr>
               <td colspan="2">Endereço: @_ADDRESS_@</td>
               <td colspan="2">Espécie: @_BENEFIT_TYPE_@</td>
            </tr>
            <tr>
               <td>Bairro: @_NEIGHBOURHOOD_@</td>
               <td>Cidade: @_CITY_@</td>
               <td>UF: @_UF_@</td>
               <td>CEP: @_CEP_@</td>
            </tr>
            <tr>
               <td colspan="4">Telefone: @_PHONES_@</td>
            </tr>
            <tr>
               <td colspan="4"></td>
            </tr>
            <tr>
               <td style="border-bottom: 1px dashed #000;font-weight: bold; background: #E4E6FF" colspan="4"><font>Dados Bancários:</font></td>
            </tr>
            <tr>
               <td>Código Banco: @_BANK_CODE_@</td>
               <td>Agencia: @_AGENCY_@</td>
               <td>Conta: @_ACCOUNT_@</td>
            </tr>
            <tr>
               <td colspan="4"></td>
            </tr>
            <tr>
               <td style="border-bottom: 1px dashed #000;font-weight: bold ; background: #E4E6FF" colspan="4">Dados Financeiros</td>
            </tr>
            <tr>
               <td>Valor Benefício: R$ @_BENEFIT_VALUE_@</td>
               <td>Total em Empréstimos: R$ @_LOAN_VALUES_@</td>
               <td>&nbsp;</td>
               <td>&nbsp;</td>
            </tr>
            <tr>
               <td>Margem Consignável: R$ @_MARGIN_@</td>
               <td>Valor Consignado: R$ @_VALUE_MARGIN_@</td>
               <td>Margem Disponível: R$ @_FREE_MARGIN_@</td>
               <td>Margem Cartão: R$ @_CARD_MARGIN_@</td>
            </tr>
            <tr>
               <td colspan="4"></td>
            </tr>
            <tr>
               <td style="border-bottom: 1px dashed #000;font-weight: bold ; background: #E4E6FF" colspan="4"><font>Contratos</font></td>
            </tr>
         </tbody>
      </table>
      <table cellpadding="0" cellspacing="0" style="width:900px; margin: 0 auto; text-align: left;" border="0">
         <tbody>
            <tr>
               <td nowrap=""><b>Empréstimo</b></td>
               <td nowrap=""><b>Banco</b></td>
               <td nowrap=""><b>Inicio</b></td>
               <td nowrap=""><b>Fim</b></td>
               <td nowrap=""><b>Vl. Financ.</b></td>
               <td nowrap=""><b>Vl. Quitacao</b></td>
               <td nowrap=""><b>Vl. Parcela</b></td>
               <td nowrap=""><b>Parcelas</b></td>
               <td nowrap=""><b>Resta</b></td>
               <td nowrap=""><b>Nº Contrato</b></td>
               <td><b>Refin. Disp.</b></td>
               <td><b>Porta. Disp.</b></td>
            </tr>
            <tr>
               <td nowrap="" style="text-align:left; font-size: 11px !important;"><label class="hint" data-hint="EMPRÉSTIMO CONSIGNADO">@_LOAN_TYPE_@</label></td>
               <td nowrap="" style="text-align:left; font-size: 11px !important;">@_LOAN_BANK_@</td>
               <td nowrap="" style="text-align:left; font-size: 11px !important;">@_LOAN_START_@</td>
               <td nowrap="" style="text-align:left; font-size: 11px !important;">@_LOAN_END_@</td>
               <td nowrap="" style="text-align:left; font-size: 11px !important;">R$ @_LOAN_VALUE_@</td>
               <td nowrap="" style="text-align:left; font-size: 11px !important;">R$ @_LOAN_PAYMENT_@</td>
               <td nowrap="" style="text-align:left; font-size: 11px !important;">R$ @_LOAN_BILL_@</td>
               <td nowrap="" style="text-align:left; font-size: 11px !important;">@_LOAN_CURRENT_BILL_@/ @_LOAN_TOTAL_BILLS_@</td>
               <td nowrap="" style="text-align:left; font-size: 11px !important;">@_LOAN_REMAINING_BILLS_@</td>
               <td nowrap="" style="text-align:left; font-size: 11px !important;">@_LOAN_CONTRACT_@</td>
               <td nowrap="" style="text-align:left; font-size: 11px !important;">R$ @_LOAN_REFINANCE_@</td>
               <td nowrap="" style="text-align:left; font-size: 11px !important;">R$ @_LOAN_PORTABILITY_@</td>
            </tr>
            <tr>
               <td colspan="13"></td>
            </tr>
            <tr></tr>
         </tbody>
      </table>
      <table cellpadding="0" cellspacing="0" style="width:900px; margin: 0 auto; text-align: left;" border="0">
         <tbody>
            <tr>
               <td style="border-bottom: 1px dashed #000;font-weight: bold ; background: #E4E6FF" colspan="13"><font>Possibilidade de negócio em margem: @_BUSINESS_MARGIN_@</font></td>
            </tr>
         </tbody>
      </table>
   </body>
</html>

