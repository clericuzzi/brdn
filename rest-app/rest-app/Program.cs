﻿using System;
using System.Linq;
using Clericuzzi.Lib.Utils.Core;
using Microsoft.AspNetCore.Hosting;

namespace RestApi
{
    public class Program
    {
        const string URL = "http://0.0.0.0:5018";
        public static void Main(string[] args)
            {
            _ClearPdfFiles();

            var host = new WebHostBuilder()
                .UseKestrel()
                .UseStartup<Startup>()
                .UseIISIntegration()
                .UseUrls(URL)
                .Build();

            host.Run();
        }
        static void _ClearPdfFiles()
        {
            var pdfFiles = ConsoleUtils.LocationEntry.GetFiles().Where(i => i.ToLowerInvariant().EndsWith(".pdf")).ToList();
            foreach (var file in pdfFiles)
                file.DeleteFile();
        }
    }
}