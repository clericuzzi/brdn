﻿using RestApi.Entities;
using System.Collections.Generic;

namespace RestApi.Business.Models
{
    public class CompanyItem : Company
    {
        public string LastTab { get; set; }
        public string LastNote { get; set; }

        public CompanyCallback Callback { get; set; }

        public List<LineItem> Lines { get; set; }
        public List<AddressItem> Addresses { get; set; }

        public List<CompanyBill> Bills { get; set; }
        public List<CompanyCall> Calls { get; set; }
        public List<CompanyNote> Notes { get; set; }
        public List<CompanyContact> Contacts { get; set; }
        public List<CompanyActiveService> ActiveServices { get; set; }
    }
}
