﻿using RestApi.Entities;

namespace RestApi.Business.Models
{
    public class AddressItem : CompanyAddress
    {
        public CompanyAvailability Availability { get; set; }
    }
}
