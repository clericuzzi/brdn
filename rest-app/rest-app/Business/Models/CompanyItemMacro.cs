﻿using RestApi.Entities;

namespace RestApi.Business.Models
{
    public class CompanyItemMacro
    {
        public string Address { get; set; }
        public Company Company { get; set; }
    }
}