using System;
using System.Linq;
using RestApi.Entities;
using System.Threading.Tasks;
using Clericuzzi.Lib.Utils.Core;

namespace RestApi.Entities
{
    /// <summary>
    /// Filter extension for project specific purposes
    /// </summary>
    public partial class CompanyLineFilter
    {
    }
    /// <summary>
    /// Class extension for project specific behaviour
    /// </summary>
    public partial class CompanyLine
    {
        public string StringValue
        {
            get
            {
                return $"{Phone}".DigitsOnly();
            }
        }
        public bool IsProblematic
        {
            get
            {
                var isZeroOut = StringValue.Replace("0", string.Empty).Length == 0;
                var localStringValue = $"{Phone}";
                var isLessThanLimit = StringValue.Length < 10;
                var isGreaterThanLimit = StringValue.Length > 11;

                if (isZeroOut)
                {
                }

                return isGreaterThanLimit || isLessThanLimit || isZeroOut || !StringValue.Equals(localStringValue);
            }
        }
        public bool IsInvalid
        {
            get
            {
                var isZeroOut = StringValue.Replace("0", string.Empty).Length == 0;
                var isLessThanLimit = StringValue.Length < 10;
                var isGreaterThanLimit = StringValue.Length > 11;

                if (isZeroOut)
                {
                }

                return isGreaterThanLimit || isLessThanLimit || isZeroOut;
            }
        }
    }
}