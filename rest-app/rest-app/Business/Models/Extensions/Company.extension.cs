using System;
using System.Linq;
using System.Threading.Tasks;
using RestApi.Business.Models;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Clericuzzi.WebApi.Data.DatabaseContext;
using Clericuzzi.WebApi.Data.Business.DatabaseContext;

namespace RestApi.Entities
{
    /// <summary>
    /// Filter extension for project specific purposes
    /// </summary>
    public partial class CompanyFilter
    {
    }
    /// <summary>
    /// Class extension for project specific behaviour
    /// </summary>
    public partial class Company
    {
        internal List<AddressItem> CreateAddressInfo(List<CompanyAddress> addresses, List<CompanyAvailability> availabilities)
        {
            var addressInfo = addresses.Where(i => i.CompanyId.Equals(Id)).Select(i => new AddressItem { Id = i.Id, CompanyId = i.CompanyId, Number = i.Number, Street = i.Street, Compliment = i.Compliment, Reference = i.Reference, TestingTimestamp = i.TestingTimestamp, ZipcodeId = i.ZipcodeId }).ToList();
            addressInfo.ForEach(i => i.Availability = availabilities.FirstOrDefault(j => j.CompanyAddressId.Equals(i.Id)));

            return addressInfo;
        }
        internal List<LineItem> CreateLineInfo(List<CompanyLine> lines, List<CompanyLineOperator> operators)
        {
            var lineInfo = lines.Where(i => i.CompanyId.Equals(Id)).Select(i => new LineItem { Id = i.Id, CompanyId = i.CompanyId, Phone = i.Phone, OperatorId = i.OperatorId, OperatorIdParent = i.OperatorIdParent, OperatorSince = i.OperatorSince, TestingTimestamp = i.TestingTimestamp }).ToList();
            lineInfo.ForEach(i => i.Operators = operators.Where(j => j.CompanyLineId.Equals(i.Id)).ToList());

            return lineInfo;
        }
        public async Task NewCompany(string address, DataContext context)
        {
            try
            {
                AccountId = 1;
                Timestamp = DateTime.Now;
                this.SetStateAdded(context);
                await context.SaveChangesAsync();

                var companyAddress = new CompanyAddress { CompanyId = Id, Street = address };
                companyAddress.ExtractNumber();
                companyAddress.SetStateAdded(context);
                await context.SaveChangesAsync();
            }
            catch (Exception)
            {
            }
        }
    }
}