using System;
using System.Linq;
using RestApi.Entities;
using System.Threading.Tasks;
using Clericuzzi.Lib.Utils.Core;

namespace RestApi.Entities
{
    /// <summary>
    /// Filter extension for project specific purposes
    /// </summary>
    public partial class ImportFileFilter
	{
	}
    /// <summary>
    /// Class extension for project specific behaviour
    /// </summary>
    public partial class ImportFile
    {
    }
}