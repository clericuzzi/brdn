using System;
using System.Linq;
using RestApi.Entities;
using System.Threading.Tasks;
using Clericuzzi.Lib.Utils.Core;

namespace RestApi.Entities
{
    /// <summary>
    /// Filter extension for project specific purposes
    /// </summary>
    public partial class CompanyAddressFilter
    {
    }
    /// <summary>
    /// Class extension for project specific behaviour
    /// </summary>
    public partial class CompanyAddress
    {
        public void ExtractNumber()
        {
            try
            {
                if (!string.IsNullOrEmpty(Street))
                {
                    _ExtractNumberPrefferedMethod();
                    if (string.IsNullOrEmpty(Number))
                        _ExtractNumberLeadingWithNumber();
                    if (string.IsNullOrEmpty(Number))
                        _ExtractNumberTryingSectinosExceptLastOne();
                }
            }
            catch (Exception)
            {
                Number = null;
            }
        }
        void _ExtractNumberPrefferedMethod()
        {
            var sections = Street.Split(',');
            var numberSection = sections[1];
            Number = numberSection.Substring(0, numberSection.IndexOf("-")).DigitsOnly().Trim();
            if (string.IsNullOrEmpty(Number))
                Number = null;
        }
        void _ExtractNumberLeadingWithNumber()
        {
            var sections = Street.Split(',');
            var numberSection = sections[0];
            Number = numberSection.DigitsOnly().Trim();
            if (string.IsNullOrEmpty(Number))
                Number = null;
        }
        void _ExtractNumberTryingSectinosExceptLastOne()
        {
            var sections = Street.Split(',');
            sections = sections.Take(sections.Length - 1).ToArray();
            foreach (var section in sections)
            {
                if (section.Contains("-"))
                {
                    var parts = section.Split('-');
                    var number = parts[0].ClearSpecialChars().DigitsOnly().Trim();
                    if (!string.IsNullOrEmpty(number))
                    {
                        Number = number;
                        return;
                    }
                }
            }

            Number = null;
        }
    }
}