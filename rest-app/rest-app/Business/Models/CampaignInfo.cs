﻿using System.Collections.Generic;

namespace RestApi.Business.Models
{
    public class CampaignInfo
    {
        public List<int> AllowedCharacteristics { get; set; }
        public List<int> NotAllowedCharacteristics { get; set; }
    }
}
