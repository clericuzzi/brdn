﻿using System.Collections.Generic;

namespace RestApi.Business.Models
{
    public class MiningTarget
    {
        public string Name { get; set; }
        public List<int> MiningType { get; set; }
        public List<int> CharacteristicsMust { get; set; }
        public List<int> CharacteristicsMustNot { get; set; }
    }
}
