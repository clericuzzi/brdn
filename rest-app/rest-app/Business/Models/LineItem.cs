﻿using RestApi.Entities;
using System.Collections.Generic;

namespace RestApi.Business.Models
{
    public class LineItem : CompanyLine
    {
        public List<CompanyLineOperator> Operators { get; set; }
    }
}
