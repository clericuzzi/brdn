﻿using System;
using System.Linq;
using RestApi.Entities;
using Clericuzzi.WebApi.Data.DatabaseContext;

namespace RestApi.Business.Models.Charts.Mining
{
    public class MiningChartModel : ChartModelBehaviour
    {
        public int TotalMined { get; set; }
        public int TotalToMine { get; set; }
        public int TotalLines { get; set; }
        public int TotalLinesTested { get; set; }
        public int TotalLinesUntested { get; set; }
        public int TotalAddresses { get; set; }
        public int TotalAddressesTested { get; set; }
        public int TotalAddressesUntested { get; set; }

        public MiningChartModel(DateTime from, DateTime to) : base(from, to)
        {
        }

        public void Calculate(DataContext context)
        {
            var companiesMined = context.GetDbSet<Company>().Where(i => i.Timestamp >= From && i.Timestamp <= To).ToList();
            companiesMined = companiesMined.Where(i => i.CompanySubtypeId.HasValue).ToList();

            var companyIds = companiesMined.Select(i => i.Id).ToList();

            var companyLines = context.GetDbSet<CompanyLine>().Where(i => companyIds.Contains(i.CompanyId)).ToList();
            var totalLines = companyLines.Count();
            var totalLinesTested = companyLines.Where(i => i.TestingTimestamp.HasValue).Count();

            var addresses = context.GetDbSet<CompanyAddress>().Where(i => companyIds.Contains(i.CompanyId)).ToList();
            var totalAddresses = addresses.Count();

            var addressIds = addresses.Select(i => i.Id).ToList();
            var totalAddressesTested = context.GetDbSet<CompanyAvailability>().Where(i => addressIds.Contains(i.CompanyAddressId)).Count();

            TotalMined = companiesMined.Count();
            TotalToMine = context.GetDbSet<MiningGoogle>().Count();
            TotalLines = totalLines;
            TotalLinesTested = totalLinesTested;
            TotalLinesUntested = totalLines - totalLinesTested;
            TotalAddresses = totalAddresses;
            TotalAddressesTested = totalAddressesTested;
            TotalAddressesUntested = totalAddresses - totalAddressesTested;
        }
    }
}