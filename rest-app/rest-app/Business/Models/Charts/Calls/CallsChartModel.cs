﻿using System;
using System.Linq;
using RestApi.Entities;
using Clericuzzi.Lib.Utils.Core;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Clericuzzi.WebApi.Bi.Business.Models;
using Clericuzzi.WebApi.Data.DatabaseContext;

namespace RestApi.Business.Models.Charts.Calls
{
    public class CallsChartModel : ChartModelBehaviour
    {
        public List<ChartItem> Data { get; set; } = new List<ChartItem>();

        public CallsChartModel(DateTime from, DateTime to) : base(from, to)
        {
        }

        public virtual void Calculate(DataContext context, List<int> employeeIds)
        {
            var items = context.GetDbSet<CompanyCall>().Include(i => i.UserIdParent).Where(i => i.StartedIn >= From && i.StartedIn <= To).ToList();
            if (employeeIds?.Count > 0)
                items = items.Where(i => employeeIds.Contains(i.UserId)).ToList();

            var dailyItems = items.GroupBy(i => new { i.UserId, i.UserIdParent.Name, i.StartedIn.Date }, i => i, (key, group) => new ChartItem() { Id = key.UserId, Item = key.Name, Amount = group.Count(), Date = key.Date, From = this.From, To = this.To, LabelX = key.Date.ToString("dd/MM") }).OrderBy(i => i.Item).ThenBy(i => i.Date).ToList();

            ChartItem.FillDates(dailyItems, From, To);
            dailyItems.ForEach(i => i.LabelX = i.Date.ToString("dd/MM"));
            dailyItems = dailyItems.OrderBy(i => i.Item).ThenBy(i => i.Date).ToList();

            Data = dailyItems;
        }
    }
}
