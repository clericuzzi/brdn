﻿using System.Linq;
using System.Collections.Generic;
using Clericuzzi.WebApi.Data.DatabaseContext;
using Clericuzzi.WebApi.Activities.Business.Managers;

namespace RestApi.Business.Models.Charts.Campaign
{
    public class CampaignChartModel
    {
        public int CampaignId { get; set; }
        public int AvailableNow { get; set; }
        public string CampaignName { get; set; }

        public CampaignChartModel()
        {
        }
        public CampaignChartModel(int id, string name)
        {
            CampaignId = id;
            CampaignName = name;
        }

        public void CalculateAvailableLeads(List<int> unavailableCompanies, DataContext context)
        {
            var availableCompanies = ActivitiesManagerActivity.EntitiesByActivity(CampaignId, context);
            availableCompanies = availableCompanies.Where(i => !unavailableCompanies.Contains(i)).ToList();

            AvailableNow = availableCompanies.Count;
        }
    }
}
