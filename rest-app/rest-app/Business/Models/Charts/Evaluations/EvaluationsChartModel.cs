﻿using System;
using System.Linq;
using RestApi.Entities;
using RestApi.Business.Enums;
using System.Collections.Generic;
using Clericuzzi.WebApi.Analytics;
using Clericuzzi.WebApi.Bi.Business.Models;
using Clericuzzi.WebApi.Data.DatabaseContext;
using Clericuzzi.Lib.Utils.Core;

namespace RestApi.Business.Models.Charts.Evaluations
{
    public class EvaluationsChartModel : ChartModelBehaviour
    {
        public int AbrTested { get; set; }
        public int AbrUntested { get; set; }

        public int CnpjTested { get; set; }
        public int CnpjUntested { get; set; }

        public int SmartByCnpjTested { get; set; }
        public int SmartByCnpjUntested { get; set; }

        public int SmartByZipNumberTested { get; set; }
        public int SmartByZipNumberUntested { get; set; }

        public List<ChartItem> Abr { get; set; }
        public List<ChartItem> Cnpj { get; set; }
        public List<ChartItem> Simplifique { get; set; }
        public List<ChartItem> SmartByCnpj { get; set; }
        public List<ChartItem> SmartByZipNumber { get; set; }

        public EvaluationsChartModel(DateTime from, DateTime to) : base(from, to)
        {
        }

        public void CalculateAbr(List<AnalyticsStats> data, DataContext context)
        {
            var universe = context.GetDbSet<CompanyLine>().Count();
            AbrTested = context.GetDbSet<AnalyticsStats>().Where(i => i.StatisticId.Equals((int)AnalyticsEnum.MiningActivitySuccessAbr)).Select(i => i.EntityId).Distinct().Count();
            AbrUntested = universe - AbrTested;

            var distinctCompanies = data.Where(i => i.StatisticId.Equals((int)AnalyticsEnum.MiningActivitySuccessAbr)).GroupBy(i => new { i.Timestamp.Date, i.EntityId }, (key, group) => new { key.Date, key.EntityId }).ToList();
            Abr = distinctCompanies.GroupBy(i => i.Date, (key, group) => new ChartItem() { Id = AnalyticsEnum.MiningActivitySuccessAbr.GetValue(), Item = "Teste Abr", LabelX = key.Date.ToString("dd/MM"), Amount = group.Count(), Date = key.Date }).ToList();
            ChartItem.FillDates(Abr, From, To, i => i.Date.ToString("dd/MM"));
            Abr = Abr.OrderBy(i => i.LabelX).ToList();
        }
        public void CalculateCnpj(List<AnalyticsStats> data, DataContext context)
        {
            var universe = context.GetDbSet<Company>().Where(i => !string.IsNullOrEmpty(i.Cnpj)).Count();
            CnpjTested = context.GetDbSet<AnalyticsStats>().Where(i => i.StatisticId.Equals((int)AnalyticsEnum.MiningCnpjCheckSuccess)).Select(i => i.EntityId).Distinct().Count();
            CnpjUntested = universe - CnpjTested;

            Cnpj = data.Where(i => i.StatisticId.Equals((int)AnalyticsEnum.MiningCnpjCheckSuccess)).GroupBy(i => i.Timestamp.Date, i => i.Amount, (key, group) => new ChartItem() { Id = (int)AnalyticsEnum.MiningCnpjCheckSuccess, Item = "Cnpj", LabelX = key.ToString("dd/MM"), Amount = group.Sum(), Date = key }).ToList();
            ChartItem.FillDates(Cnpj, From, To, i => i.Date.ToString("dd/MM"));
            Cnpj = Cnpj.OrderBy(i => i.LabelX).ToList();
        }
        public void CalculateSmartCnpj(List<AnalyticsStats> data, DataContext context)
        {
            var universe = context.GetDbSet<Company>().Where(i => !string.IsNullOrEmpty(i.Cnpj)).Count();
            SmartByCnpjTested = context.GetDbSet<AnalyticsStats>().Where(i => i.StatisticId.Equals((int)AnalyticsEnum.MiningActivitySuccessSmartWebCnpj)).Select(i => i.EntityId).Distinct().Count();
            SmartByCnpjUntested = universe - SmartByCnpjTested;

            var distinctCompanies = data.Where(i => i.StatisticId.Equals(AnalyticsEnum.MiningActivitySuccessSmartWebCnpj.GetValue())).GroupBy(i => new { i.Timestamp.Date, i.EntityId }, (key, group) => new { key.Date, key.EntityId }).ToList();
            SmartByCnpj = distinctCompanies.GroupBy(i => i.Date, (key, group) => new ChartItem() { Id = (int)AnalyticsEnum.MiningActivitySuccessSmartWebCnpj, Item = "Smart Cnpj", LabelX = key.ToString("dd/MM"), Amount = group.Count(), Date = key }).ToList();
            ChartItem.FillDates(SmartByCnpj, From, To, i => i.Date.ToString("dd/MM"));
            SmartByCnpj = SmartByCnpj.OrderBy(i => i.LabelX).ToList();
        }
        public void CalculateSimplifique(List<AnalyticsStats> data, DataContext context)
        {
            Simplifique = data.Where(i => i.StatisticId.Equals((int)AnalyticsEnum.MiningSuccessSimplifique)).GroupBy(i => i.Timestamp.Date, i => i.Amount, (key, group) => new ChartItem() { Id = (int)AnalyticsEnum.MiningSuccessSimplifique, Item = "Simplifique", LabelX = key.ToString("dd/MM"), Amount = group.Sum(), Date = key }).ToList();
            ChartItem.FillDates(Simplifique, From, To, i => i.Date.ToString("dd/MM"));
            Simplifique = Simplifique.OrderBy(i => i.LabelX).ToList();
        }
        public void CalculateSmartZipAndNumber(List<AnalyticsStats> data, DataContext context)
        {
            var universe = context.GetDbSet<CompanyAddress>().Count();
            SmartByZipNumberTested = context.GetDbSet<AnalyticsStats>().Where(i => i.StatisticId.Equals((int)AnalyticsEnum.MiningActivitySuccessSmartWeb)).Select(i => i.EntityId).Distinct().Count();
            SmartByZipNumberUntested = universe - SmartByZipNumberTested;

            var distinctCompanies = data.Where(i => i.StatisticId.Equals(AnalyticsEnum.MiningActivitySuccessSmartWeb.GetValue())).GroupBy(i => new { i.Timestamp.Date, i.EntityId }, (key, group) => new { key.Date, key.EntityId }).ToList();
            SmartByZipNumber = distinctCompanies.GroupBy(i => i.Date, (key, group) => new ChartItem() { Id = (int)AnalyticsEnum.MiningActivitySuccessSmartWeb, Item = "Smart Cep/Nº", LabelX = key.ToString("dd/MM"), Amount = group.Count(), Date = key }).ToList();
            ChartItem.FillDates(SmartByZipNumber, From, To, i => i.Date.ToString("dd/MM"));
            SmartByZipNumber = SmartByZipNumber.OrderBy(i => i.LabelX).ToList();
        }
    }
}
