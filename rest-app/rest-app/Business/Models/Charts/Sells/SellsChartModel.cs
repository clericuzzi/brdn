﻿using System;
using System.Linq;
using RestApi.Entities;
using RestApi.Business.Enums;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Clericuzzi.WebApi.Data.DatabaseContext;

namespace RestApi.Business.Models.Charts.Sells
{
    public class SellsChartModel : ChartModelBehaviour
    {
        public List<Tuple<string, int>> Distribution { get; set; } = new List<Tuple<string, int>>();
        public List<Tuple<string, List<Tuple<string, int>>>> Data { get; set; } = new List<Tuple<string, List<Tuple<string, int>>>>();

        public SellsChartModel(DateTime from, DateTime to) : base(from, to)
        {
        }

        public void Calculate(DataContext context, List<int> employeeIds)
        {
            var sells = context.GetDbSet<CompanyCall>().Include(i => i.UserIdParent).Where(i => i.StartedIn >= From && i.StartedIn <= To).ToList();
            sells = sells.Where(i => i.TabulationId.Equals((int)TabulationsEnum.VENDA_FEITA)).ToList();
            if (employeeIds?.Count > 0)
                sells = sells.Where(i => employeeIds.Contains(i.UserId)).ToList();
            else
                employeeIds = sells.Select(i => i.UserId).Distinct().ToList();

            foreach (var employee in employeeIds)
            {
                var emplayeeName = sells.First(i => i.UserId.Equals(employee)).UserIdParent.Name;
                var employeeData = sells.Where(i => i.UserId.Equals(employee)).GroupBy(i => i.StartedIn.Date.ToString("dd/MM"), i => i, (key, group) => new Tuple<string, int>(key, group.Count())).ToList();
                var employeeStats = new Tuple<string, List<Tuple<string, int>>>(emplayeeName, employeeData);

                Data.Add(employeeStats);
                Distribution.Add(new Tuple<string, int>(emplayeeName, employeeData.Sum(i => i.Item2)));
            }
        }
    }
}
