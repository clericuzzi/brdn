﻿using System;
using System.Linq;
using RestApi.Entities;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using RestApi.Business.Models.Charts.Calls;
using Clericuzzi.WebApi.Bi.Business.Models;
using Clericuzzi.WebApi.Data.DatabaseContext;

namespace RestApi.Business.Models.Charts.Tabulations
{
    public class TabulationsChartModel : CallsChartModel
    {
        public TabulationsChartModel(DateTime from, DateTime to) : base(from, to)
        {
        }

        public override void Calculate(DataContext context, List<int> employeeIds)
        {
            var items = context.GetDbSet<CompanyCall>().Include(i => i.TabulationIdParent).Where(i => i.StartedIn >= From && i.StartedIn <= To && i.TabulationId.HasValue).ToList();
            if (employeeIds?.Count > 0)
                items = items.Where(i => employeeIds.Contains(i.UserId)).ToList();

            var dailyItems = items.GroupBy(i => new { i.TabulationId, i.TabulationIdParent.Text, i.StartedIn.Date }, i => i, (key, group) => new ChartItem() { Id = key.TabulationId.Value, Item = key.Text, Amount = group.Count(), Date = key.Date, From = this.From, To = this.To, LabelX = key.Text }).OrderBy(i => i.Item).ThenBy(i => i.Date).ToList();

            ChartItem.FillDates(dailyItems, From, To);
            dailyItems.ForEach(i => i.LabelX = i.Date.ToString("dd/MM"));
            dailyItems = dailyItems.OrderBy(i => i.Item).ThenBy(i => i.Date).ToList();

            Data = dailyItems;
        }
    }
}
