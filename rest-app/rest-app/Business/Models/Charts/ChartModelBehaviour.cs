﻿using System;
using Newtonsoft.Json;
using Clericuzzi.Lib.Utils.Core;

namespace RestApi.Business.Models.Charts
{
    public class ChartModelBehaviour
    {
        [JsonIgnore]
        public DateTime To { get; set; }
        [JsonIgnore]
        public DateTime From { get; set; }

        public ChartModelBehaviour(DateTime from, DateTime to)
        {
            to.ToEnd();
            from.ToStart();

            To = to;
            From = from;
        }
    }
}
