﻿using System.Collections.Generic;

namespace RestApi.Business.Models
{
    public class CompanyContactInfo
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public List<string> Phones { get; set; }
    }
}