﻿using RestApi.Entities;
using System.Collections.Generic;

namespace RestApi.Business.Models
{
    public class CompanyInfo
    {
        public Company Company { get; set; }
        public CompanyAddress CompanyAddress { get; set; }
        public List<CompanyLine> CompanyLines { get; set; }
    }
}
