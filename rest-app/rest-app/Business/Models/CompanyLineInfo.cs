﻿using System;

namespace RestApi.Business.Models
{
    public class CompanyLineInfo
    {
        public int Id { get; set; }
        public int OperatorId { get; set; }

        public string Number { get; set; }
        public string Operator { get; set; }
        public DateTime? DateSince { get; set; }

        public CompanyLineInfo()
        {

        }
    }
}
