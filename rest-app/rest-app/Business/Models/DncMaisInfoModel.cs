﻿using Clericuzzi.Lib.Utils.Core;

namespace RestApi.Business.Models
{
    public class DncMaisInfoModel
    {
        public int CompanyLineId { get; set; }
        public string Ddd { get; set; }
        public string Phone { get; set; }
        public int CanCall { get; set; }
        public bool Valid { get { return Ddd?.DigitsOnly().Length == 2 && Phone?.DigitsOnly().Length >= 8; } }
    }
}
