﻿using System.ComponentModel;

namespace RestApi.Business.Enums
{
    public enum ProductsEnum
    {
        [Description("soho")]
        Soho = 1,

        [Description("movel")]
        Movel = 2,

        [Description("soho-high-speed")]
        SohoHighSpeed = 3,
    }
}