﻿using System.ComponentModel;

namespace RestApi.Business.Enums
{
    /// <summary>
    /// This enumeration contains the different types of mining
    /// statistics that the user might request from the web app
    /// </summary>
    public enum StatsMiningTypeEnum
    {
        [Description("Abr mining system")]
        Abr = 0,
        [Description("Catta mining system")]
        Catta = 1,
        [Description("Google mining system")]
        Google = 2,
        [Description("SmartWeb mining system")]
        AbSmartWeb = 3,
    }
}