﻿using System.ComponentModel;

namespace RestApi.Business.Enums
{
    public enum PermissionsEnum
    {
        [Description("defines a management level user")]
        ManagementLevel = 200,

        [Description("defines a admin level user")]
        AdminLevel = 100,

        [Description("defines a low level user")]
        UserLevel = 300,
    }
}