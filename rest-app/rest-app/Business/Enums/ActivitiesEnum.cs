﻿using System.ComponentModel;

namespace RestApi.Business.Enums
{
    public enum ActivitiesEnum
    {
        [Description("venda-soho")]
        VendaSoho = 1,

        [Description("abrtelecom-soho-vt")]
        AbrTelecomSohoVt = 2,

        [Description("venda-soho-alta-velocidade")]
        VendaSohoAltaVelocidade = 4,
    }
}