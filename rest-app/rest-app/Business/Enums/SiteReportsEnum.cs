﻿using System.ComponentModel;

namespace RestApi.Business.Enums
{
    public enum SiteReportsEnum
    {
        [Description("calls")]
        CALLS = 1,
        [Description("sales")]
        SALES = 2,
        [Description("mining")]
        MINING = 3,
        [Description("evaluations")]
        EVALUATION = 4,
        [Description("tabulations")]
        TABULATION = 5,
    }
}