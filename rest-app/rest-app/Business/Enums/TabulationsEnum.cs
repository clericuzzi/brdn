﻿using System.ComponentModel;

namespace RestApi.Business.Enums
{
    public enum TabulationsEnum
    {
        [Description("cliente-sem-interesse")]
        CLIENTE_SEM_INTERESSE = 1,
        [Description("ligar-novamente")]
        LIGAR_NOVAMENTE = 2,
        [Description("cliente-ja-e-vivo")]
        CLIENTE_JA_E_VIVO = 3,
        [Description("cliente-fidelizado-com-outra-operaora")]
        CLIENTE_FIDELIZADO_COM_OUTRA_OPERAORA = 4,
        [Description("nao-atende")]
        NAO_ATENDE = 5,
        [Description("cliente-pessoa-fisica")]
        CLIENTE_PESSOA_FISICA = 6,
        [Description("cliente-sem-perfil")]
        CLIENTE_SEM_PERFIL = 7,
        [Description("em-negociacao")]
        EM_NEGOCIACAO = 8,
        [Description("numero-nao-existe")]
        NUMERO_NAO_EXISTE = 9,
        [Description("erro-de-cadastro")]
        ERRO_DE_CADASTRO = 10,
        [Description("venda-feita")]
        VENDA_FEITA = 11,
        [Description("recusa-cliente-diz-que-nossas-ofertas-sao-caras")]
        RECUSA_CLIENTE_DIZ_QUE_NOSSAS_OFERTAS_SAO_CARAS = 12,
        [Description("recusa-cliente-sem-perfil")]
        RECUSA_CLIENTE_SEM_PERFIL = 13,
        [Description("recusa-cliente-insatisfeito")]
        RECUSA_CLIENTE_INSATISFEITO = 14,
        [Description("recusa-ja-e-atendido-pela-concorrencia")]
        RECUSA_JA_E_ATENDIDO_PELA_CONCORRENCIA = 15,
        [Description("sem-cobertura")]
        SEM_COBERTURA = 16,
        [Description("recusa-nao-perturbe")]
        RECUSA_NAO_PERTURBE = 17,
    }
}
