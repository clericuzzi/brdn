﻿using System.ComponentModel;

namespace RestApi.Business.Enums
{
    /// <summary>
    /// Enumerates actions that should be logged
    /// </summary>
    public enum ActionsEnum
    {
        [Description("mining-catta")]
        MiningCatta = 2,

        [Description("smart-web-by-zip-and-number")]
        SmartWebByZipAndNumber = 3,

        [Description("simplifique")]
        Simplifique = 6,

        [Description("mais-info")]
        MaisInfo= 7,

        [Description("smart-web-by-cnpj")]
        SmartWebByCnpj = 8,

        [Description("mining-google")]
        MiningGoogle = 9,

        [Description("cnpj-google")]
        CnpjGoogle = 10,

        [Description("cnpj-check")]
        CnpjCheck = 11,

        [Description("abr-telecom-soho")]
        AbrTelecomSoho = 12,
    }
}