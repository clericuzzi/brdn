﻿using System.ComponentModel;

namespace RestApi.Business.Enums
{
    public enum CompanyCheckTypeEnum
    {
        [Description("smart-zip")]
        SmartZip = 1,

        [Description("smart-cnpj")]
        SmartCnpj = 2,

        [Description("cnpj-google")]
        CnpjGoogle = 3,

        [Description("mining-google")]
        MiningGoogle = 4,

        [Description("simplifique")]
        Simplifique = 5,

        [Description("abr")]
        Abr = 6,

        [Description("cnpj")]
        Cnpj = 7,
    }
}
