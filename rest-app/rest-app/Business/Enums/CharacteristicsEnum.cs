﻿using System.ComponentModel;

namespace RestApi.Business.Enums
{
    public enum CharacteristicsEnum
    {
        [Description("soho-vt")]
        SohoVt = 1,
        [Description("soho-gpon")]
        SohoGpon = 5,
        [Description("soho-metalico")]
        SohoMetalico = 6,

        [Description("operadora-tim")]
        OperadoraTim = 3,
        [Description("operadora-vivo")]
        OperadoraVivo = 2,
        [Description("operadora-claro")]
        OperadoraClaro = 4,

        [Description("abrtelecom-teste-soho-realizado")]
        AbrTelecomTesteSohoRealizado = 7,
    }
}