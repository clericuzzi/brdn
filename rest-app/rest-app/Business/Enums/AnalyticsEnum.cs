﻿using System.ComponentModel;

namespace RestApi.Business.Enums
{
    public enum AnalyticsEnum
    {
        [Description("user-activity-call")]
        UserActivityCall = 1,
        [Description("user-activity-request-check")]
        UserActivityRequestCheck = 2,

        [Description("mining-activity-error-abr")]
        MiningActivityErrorAbr = 10,
        [Description("mining-activity-error-catta")]
        MiningActivityErrorCatta = 4,
        [Description("mining-activity-error-google")]
        MiningActivityErrorGoogle = 6,
        [Description("mining-activity-error-smart-web")]
        MiningActivityErrorSmartWeb = 8,
        [Description("mining-activity-error-smart-web-cnpj")]
        MiningActivityErrorSmartWebCnpj = 12,
        [Description("mining-activity-success-abr")]
        MiningActivitySuccessAbr = 9,
        [Description("mining-activity-success-catta")]
        MiningActivitySuccessCatta = 3,
        [Description("mining-activity-success-google")]
        MiningActivitySuccessGoogle = 5,
        [Description("mining-activity-success-smart-web")]
        MiningActivitySuccessSmartWeb = 7,
        [Description("mining-activity-success-smart-web-cnpj")]
        MiningActivitySuccessSmartWebCnpj = 11,

        [Description("mining-success-simplifique")]
        MiningSuccessSimplifique = 13,

        [Description("mining-cnpj-check-error")]
        MiningCnpjCheckError = 14,
        [Description("mining-cnpj-check-success")]
        MiningCnpjCheckSuccess = 15,

        [Description("vt-check-invalid-address")]
        VtCheckInvalidAddress = 16,

        [Description("google-cnpj-company-new")]
        GoogleCnpjCompanyNew = 17,
        [Description("google-cnpj-company-updated")]
        GoogleCnpjCompanyUpdated = 18,
    }
}