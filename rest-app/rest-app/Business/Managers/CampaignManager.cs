﻿using System.Collections.Generic;
using System.Linq;
using Clericuzzi.WebApi.Activities;
using Clericuzzi.WebApi.Data.DatabaseContext;

namespace RestApi.Business.Managers
{
    /// <summary>
    /// Manages the specific project rules regarding activities
    /// </summary>
    public static class CampaignManager
    {
        /// <summary>
        /// specific project rule
        /// this method will screen all eligible entities, and remove all available companies outside the eligible users area
        /// </summary>
        /// <param name="currentUser">the user </param>
        /// <param name="eligibleEntities">a list of already eligible entities, based on the activy rules only</param>
        /// <param name="context">database connection</param>
        /// <returns>a list of eligible entities based in the areas for all eligible users</returns>
        public static bool CampaignRequiresVt(int campaignId, DataContext context)
        {
            var characteristics = context.GetDbSet<ActivityCharacteristicMust>().Where(i => i.ActivityId.Equals(campaignId)).Select(i => i.CharacteristicId).ToList();
            return characteristics?.Exists(i => i.Equals(1)) ?? false;
        }
    }
}