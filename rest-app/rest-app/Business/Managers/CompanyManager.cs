﻿using System.Linq;
using RestApi.Entities;
using System.Collections.Generic;
using Clericuzzi.WebApi.Data.DatabaseContext;

namespace RestApi.Business.Managers
{
    public static class CompanyManager
    {
        public static int? GetUntabulatedCompany(int userId, List<int> companyIds, DataContext context)
        {
            var allCalls = context.GetDbSet<CompanyCall>().Where(i => companyIds.Contains(i.CompanyId)).ToList();
            if (allCalls?.Count > 0)
            {
                foreach (var companyId in companyIds)
                {
                    var calls = allCalls.Where(i => i.CompanyId.Equals(companyId)).ToList();
                    if (calls?.Count > 0)
                    {
                        var openCall = calls.Where(i => i.UserId.Equals(userId) && !i.TabulationId.HasValue).FirstOrDefault();
                        if (openCall != null)
                            return openCall.CompanyId;
                    }
                    else
                        return companyId;
                }
                return null;
            }

            return companyIds.FirstOrDefault();
        }
    }
}