﻿using System;
using System.Linq;
using RestApi.Entities;
using Clericuzzi.Lib.Utils.Core;
using System.Collections.Generic;
using Clericuzzi.WebApi.Data.DatabaseContext;
using Clericuzzi.WebApi.Activities.Business.Managers;
using Clericuzzi.WebApi.Data.Business.DatabaseContext;

namespace RestApi.Business.Managers
{
    public static class LeadsManager
    {
        static List<Company> _NewLead(int userId, int activityId, DataContext context)
        {
            var unavailableCompanies = context.GetDbSet<Lead>().Select(i => i.CompanyId).ToList();
            var availableCompanies = ActivitiesManagerActivity.EntitiesByActivity(activityId, context);
            availableCompanies = availableCompanies.Where(i => !unavailableCompanies.Contains(i)).ToList();
            if (availableCompanies?.Count > 0)
            {
                var companyId = 0;
                var campaignRequiresVt = CampaignManager.CampaignRequiresVt(activityId, context);
                if (!campaignRequiresVt)
                {
                    companyId = _TabulationFilter(availableCompanies, context);
                }
                else
                {
                    var testedAddresses = context.GetDbSet<CompanyAddress>().Where(i => availableCompanies.Contains(i.CompanyId)).ToList();
                    testedAddresses = testedAddresses?.Where(i => i.TestingTimestamp.HasValue).OrderByDescending(i => i.TestingTimestamp).ToList();
                    var testedCompanies = testedAddresses.Select(i => i.CompanyId).ToList();

                    availableCompanies = availableCompanies.Where(i => testedCompanies.Contains(i)).ToList();
                    companyId = _TabulationFilter(availableCompanies, context);
                }

                new Lead { ActivityId = activityId, UserId = userId, CompanyId = companyId, Timestamp = DateTime.Now }.SetStateAdded(context);
                context.SaveChanges();

                return context.GetDbSet<Company>().Where(i => i.Id.Equals(companyId)).ToList();
            }
            else
                throw new Exception("Não há mais empresas disponíveis");
        }
        static int _TabulationFilter(List<int> availableCompanies, DataContext context)
        {
            var forbiddenTabulations = context.GetDbSet<BlockedTabulations>().Select(i => i.TabulationId).ToList();

            var tabulatedCalls = context.GetDbSet<CompanyCall>().Where(i => i.TabulationId.HasValue).ToList();
            tabulatedCalls = tabulatedCalls.Where(i => availableCompanies.Contains(i.CompanyId)).ToList();

            var tabulatedCompanyIds = tabulatedCalls.Select(i => i.CompanyId).Distinct().ToList();
            var untabulatedCompanies = availableCompanies.Where(i => !tabulatedCompanyIds.Contains(i)).ToList();
            if (untabulatedCompanies.Count > 0)
                return untabulatedCompanies.GetRandomItem();
            else
            {
                tabulatedCalls = tabulatedCalls.Where(i => forbiddenTabulations.Contains(i.TabulationId.Value)).ToList();
                var forbiddenCompanyIds = tabulatedCalls.Select(i => i.CompanyId).Distinct().ToList();

                availableCompanies = availableCompanies.Where(i => !forbiddenCompanyIds.Contains(i)).ToList();

                return availableCompanies.GetRandomItem();
            }
        }

        public static List<Company> GetCurrentLead(int userId, int activityId, DataContext context)
        {
            var leads = context.GetDbSet<Lead>().Where(i => i.UserId.Equals(userId)).ToList();
            var companies = leads.Where(i => i.ActivityId.Equals(activityId)).Select(i => i.CompanyId).ToList();
            if (companies?.Count > 0)
            {
                var openCompanyId = CompanyManager.GetUntabulatedCompany(userId, companies, context);
                if (openCompanyId == null)
                    return _NewLead(userId, activityId, context);
                else
                    return context.GetDbSet<Company>().Where(i => i.Id.Equals(openCompanyId.Value)).ToList();
            }
            else
                return _NewLead(userId, activityId, context);
        }

        static List<Company> _NewLeadFromFile(int userId, int fileimportId, DataContext context)
        {
            // TODO
            return new List<Company>();
            //var activityId = 5;
            //var unavailableCompanies = context.GetDbSet<Lead>().Select(i => i.CompanyId).ToList();
            //var availableCompanies = context.GetDbSet<ImportFileCompany>().Where(i => i.ImportFileId.Equals(fileimportId) && i.CompanyIdParent.CnpjTestingTimestamp.HasValue).Select(i => i.CompanyId).ToList();
            //availableCompanies = availableCompanies.Where(i => !unavailableCompanies.Contains(i)).ToList();

            //if (availableCompanies?.Count > 0)
            //{
            //    var companyId = availableCompanies.GetRandomItem();
            //    new Lead { ActivityId = activityId, UserId = userId, CompanyId = companyId, Timestamp = DateTime.Now }.SetStateAdded(context);

            //    return context.GetDbSet<Company>().Where(i => i.Id.Equals(companyId)).ToList();
            //}
            //else
            //    throw new Exception("Não há mais empresas disponíveis");
        }
        public static List<Company> GetCurrentLeadFromFile(int userId, int importFileId, DataContext context)
        {
            var leads = context.GetDbSet<Lead>().Where(i => i.UserId.Equals(userId)).ToList();
            var availableCompanies = context.GetDbSet<ImportFileCompany>().Where(i => i.ImportFileId.Equals(importFileId)).Select(i => i.CompanyId).ToList();
            var companies = leads.Where(i => i.ActivityId.Equals(5)).Select(i => i.CompanyId).ToList();
            companies = companies.Where(i => availableCompanies.Contains(i)).ToList();
            if (companies?.Count > 0)
            {
                var openCompanyId = CompanyManager.GetUntabulatedCompany(userId, companies, context);
                if (openCompanyId == null)
                    return _NewLeadFromFile(userId, importFileId, context);
                else
                    return context.GetDbSet<Company>().Where(i => i.Id.Equals(openCompanyId.Value)).ToList();
            }
            else
                return _NewLeadFromFile(userId, importFileId, context);
        }
    }
}
