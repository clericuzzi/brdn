﻿using System;
using System.Linq;
using RestApi.Entities;
using System.Threading.Tasks;
using Clericuzzi.Lib.Utils.Core;
using System.Collections.Generic;
using Clericuzzi.WebApi.Analytics;
using Clericuzzi.WebApi.Data.DatabaseContext;
using Clericuzzi.WebApi.Entities.Business.Managers;
using Clericuzzi.WebApi.Data.Business.DatabaseContext;

namespace RestApi.Business.Managers
{
    public class MiningManager
    {
        public static async Task<int> NextMiningTarget(string action, string machineName, int checkType, int miningType, List<int> universe, DataContext context)
        {
            int target = 0;
            int? concurrentAction = await ConcurrencyManager.GetId(action, null, machineName, context);
            if (concurrentAction.HasValue)
                return concurrentAction.Value;
            else
            {
                var priorityCheck = context.GetDbSet<CompanyCheckRequest>().Where(i => i.CheckType.Equals(checkType)).GetRandomRow();
                if (priorityCheck != null)
                {
                    target = priorityCheck.CompanyId;
                    priorityCheck.SetStateDeleted(context);
                }
                else
                {
                    var testedCompanies = context.GetDbSet<AnalyticsStats>().Where(i => i.StatisticId.Equals(miningType)).Where(i => i.EntityId.HasValue).Select(i => i.EntityId.Value).Distinct().ToList();
                    var untestedCompanies = universe.Where(i => !testedCompanies.Contains(i)).ToList();
                    if (untestedCompanies.Count == 0)
                        target = _GetOldest(testedCompanies, miningType, context);

                    target = untestedCompanies.GetRandomItem();
                }

                await ConcurrencyManager.StoreId(target, action, null, machineName, context);
            }

            return target;
        }

        static int _GetOldest(List<int> testedCompanies, int miningType, DataContext context)
        {
            var stats = context.GetDbSet<AnalyticsStats>().Where(i => i.StatisticId.Equals(miningType)).ToList();
            stats = stats.Where(i => testedCompanies.Contains(i.EntityId.Value)).ToList();
            var grouping = stats.GroupBy(i => i.EntityId, i => i, (key, group) => new { CompanyId = key, MaxDate = group.ToList().Select(i => i.Timestamp).OrderByDescending(i => i).FirstOrDefault() }).ToList();

            return grouping.OrderBy(i => i.MaxDate).Select(i => i.CompanyId.Value).FirstOrDefault();
        }
    }
}
