﻿using Clericuzzi.Lib.Utils.Error;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestApi.Business.Exceptions.Site.Mining
{
    public class MiningCurrentTargetSelectMiningTypeException : ExceptionBase
    {
        public MiningCurrentTargetSelectMiningTypeException() : base(-14, "Selecione um tipo de mineração")
        {
        }
    }
}
