﻿using System;
using System.Linq;
using RestApi.Database;
using System.Threading;
using Clericuzzi.Lib.Email;
using System.Threading.Tasks;
using Clericuzzi.WebApi.Entities;
using RestApi.Business.ScheduledActions;
using RestApi.Business.Scheduler.ScheduleManagers;
using Clericuzzi.WebApi.Entities.Business.Scheduler;

namespace RestApi.Business.Scheduler
{
    public class ServiceScheduler : HostedServiceBase
    {
        public static bool _EmailSent = false;
        public override async Task ExecuteAsync(CancellationToken stopToken)
        {
            if (SchedulerManager.IsWindows())
                return;

            int step = 4;
            SchedulerManager.NewService(context => DatabaseIntegrityManager.CompanyDups(context), step++, false);
            SchedulerManager.NewService(context => DatabaseIntegrityManager.DailyTesting(context), step++, false);
            SchedulerManager.NewService(context => DatabaseIntegrityManager.MiningHealth(context), step++, false);
            SchedulerManager.NewService(context => DatabaseIntegrityManager.ComanyLocation(context), step++, false);
            SchedulerManager.NewService(context => DatabaseIntegrityManager.ValidateCompanyLines(context), step++, false);
            SchedulerManager.NewService(context => DatabaseIntegrityManager.SetCompanyCharacteristics(context), step++, false);
            SchedulerManager.NewService(context => DatabaseIntegrityManager.ValidatePhoneOperatorVivo(context), step++, false);
            SchedulerManager.SetSyncPeriod(35);

            while (true)
            {
                try
                {
                    using (var context = AppDatabaseMysql.CreateConnection())
                    {
                        var param = context.GetDbSet<SystemParameter>().FirstOrDefault(i => i.Parameter.Equals("scheduled-routines"));
                        if (param?.Value.Equals("1") ?? false)
                            SchedulerManager.ComputeMinute(context);
                    }
                }
                catch (Exception ex)
                {
                    SendEmail(ex: ex);
                }

                await Task.Delay(TimeSpan.FromMinutes(1), stopToken);
            }
        }
        public static void SendEmail(Exception ex)
        {
            var errorMessage = ex.StackTrace + "\r\n\r\n" + ex.InnerException + "_______________________\r\n\r\n";
            var inner = ex.InnerException;
            while (inner != null)
            {
                errorMessage += inner.StackTrace + "\r\n\r\n" + ex.InnerException + "_______________________\r\n\r\n";
                inner = inner.InnerException;
            }

            EmailManager.SimpleSmtp("Pedro", "dev.clericuzzi@gmail.com", $"ERROR {DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss")} -> {ex.Message} (has innerException {ex.InnerException != null})", errorMessage);
        }
    }
}