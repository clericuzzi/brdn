﻿using Clericuzzi.WebApi.Data.Utils;
using Clericuzzi.WebApi.Data.DatabaseContext;

namespace RestApi.Business.Scheduler.ScheduleManagers
{
    public static class DatabaseIntegrityManager
    {
        public static void CompanyDups(DataContext context)
        {
            var filepath = @"Data/Routines/company-dups.query.txt";
            DataUtils.RunScriptFromFile(context, filepath);
        }
        public static void DailyTesting(DataContext context)
        {
            var filepath = @"Data/Routines/company-characteristics-daily-testing.query.txt";
            DataUtils.RunScriptFromFile(context, filepath);
        }
        public static void MiningHealth(DataContext context)
        {
            var filepath = @"Data/Routines/mining-health.query.txt";
            DataUtils.RunScriptFromFile(context, filepath);
        }
        public static void ComanyLocation(DataContext context)
        {
            var filepath = @"Data/Routines/company-location.query.txt";
            DataUtils.RunScriptFromFile(context, filepath);
        }
        public static void ValidateCompanyLines(DataContext context)
        {
            var filepath = @"Data/Routines/company-line-validation.query.txt";
            DataUtils.RunScriptFromFile(context, filepath);
        }
        public static void SetCompanyCharacteristics(DataContext context)
        {
            var filepath = @"Data/Routines/company-characteristics.query.txt";
            DataUtils.RunScriptFromFile(context, filepath);
        }
        public static void ValidatePhoneOperatorVivo(DataContext context)
        {
            var filepath = @"Data/Routines/company-line-validation-vivo.query.txt";
            DataUtils.RunScriptFromFile(context, filepath);
        }
    }
}
