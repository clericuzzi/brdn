﻿using System;
using System.Linq;
using Clericuzzi.Lib.Utils.Core;
using System.Collections.Generic;
using Clericuzzi.WebApi.Activities;
using Clericuzzi.WebApi.Data.DatabaseContext;
using Clericuzzi.WebApi.Activities.Business.Managers;

namespace RestApi.Business.Scheduler.ScheduleManagers
{
    /// <summary>
    /// Gives a unique spin to the activity creation routine
    /// After we get a list of available entities, it is possible
    /// to have other passes filtering the data
    /// </summary>
    public static class ActivityScheduleManager
    {
        private static Dictionary<int, List<Func<int, List<int>, DataContext, int>>> _ActivityFilters = new Dictionary<int, List<Func<int, List<int>, DataContext, int>>>();
        public static void AddActivityFilter(int activityId, Func<int, List<int>, DataContext, int> action)
        {
            if (!_ActivityFilters.ContainsKey(activityId))
                _ActivityFilters.Add(activityId, new List<Func<int, List<int>, DataContext, int>>());

            var activityList = _ActivityFilters[activityId];
            if (!activityList.Contains(action))
                activityList.Add(action);
        }

        /// <summary>
        /// the activity creation routine
        /// it is implemented locally because each project might deal
        /// differently with the "available entities".
        /// there is a way of getting a list of available entities, but
        /// each project might have further restrictions, that is why
        /// this routine exists locally
        /// </summary>
        public static void ActivityCreation(DataContext context)
        {
            ActivitiesManagerActivity.StartCreation();
            var activities = context.GetDbSet<Activity>().Select(i => i.Id).ToList();
            foreach (var activityId in activities)
                _ActivityCreation(activityId, context);

            ActivitiesManagerActivity.EndCreation(context);
        }
        /// <summary>
        /// goes through each activity creating its items
        /// </summary>
        /// <param name="activityId">the current activity</param>
        /// <param name="context">database connection</param>
        static void _ActivityCreation(int activityId, DataContext context)
        {
            var userSlots = ActivitiesManagerActivity.UsersByActivity(activityId, context);
            if (ActivitiesManagerActivity.HasSlotsAvailable(userSlots))
                _ActivityCreationAction(activityId, userSlots, context);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="activityId">the current activity</param>
        /// <param name="userSlots">a list of users and the amount of activity slots available for each one of them</param>
        /// <param name="context">database connection</param>
        static void _ActivityCreationAction(int activityId, List<Tuple<int, int>> userSlots, DataContext context)
        {
            // the list of eligible entities based in the activity rules
            var eligibleEntities = ActivitiesManagerActivity.EntitiesByActivity(activityId, context);
            while (ActivitiesManagerActivity.HasSlotsAvailable(userSlots))
            {
                // gets all the available users, so we can iterate through them
                var availaberUsers = userSlots.Select(i => i.Item1).OrderBy(i => new Guid()).ToList();
                foreach (var currentUser in availaberUsers)
                {
                    // each iteration is composed of the same steps
                    // - getting an entity
                    // - creating the activity
                    // - decrementing the activity slots
                    // the selected entity will be decided here "_ActivityCreationFilter"
                    var entityId = _ActivityCreationFilter(activityId, eligibleEntities, currentUser, context);
                    ActivitiesManagerActivity.Create(currentUser, activityId, entityId);
                    userSlots = ActivitiesManagerActivity.DecrementSlot(userSlots, currentUser);

                    eligibleEntities.Remove(entityId);
                }
            }
        }

        /// <summary>
        /// here is where the project's specifif filters might be applied
        /// Suppose you have to restict the available entities set even more,
        /// here is where you should apply this restictions
        /// </summary>
        /// <param name="activityId">the current activity</param>
        /// <param name="eligibleEntities">a list of already eligible entities, based on the activy rules only</param>
        /// <param name="eligibleUsers">a list of users enrolled in this activity</param>
        /// <param name="context">database connection</param>
        /// <returns>the project filtered list of eligibleEntities</returns>
        private static int _ActivityCreationFilter(int activityId, List<int> eligibleEntities, int currentUser, DataContext context)
        {
            var selectedEntity = 0;
            if (_ActivityFilters.ContainsKey(activityId))
            {
                var actions = _ActivityFilters[activityId];
                foreach (var action in actions)
                    selectedEntity = action(currentUser, eligibleEntities, context);
            }
            else
                selectedEntity = eligibleEntities.GetRandomItem();

            return selectedEntity;
        }
    }
}