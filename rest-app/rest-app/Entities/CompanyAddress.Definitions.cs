﻿using Clericuzzi.WebApi;
using Clericuzzi.WebApi.Entities;

using System;
using System.Linq;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Collections.Generic;
using Clericuzzi.Lib.Http.Core.Crud;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using Clericuzzi.WebApi.Data.DatabaseContext;
using System.ComponentModel.DataAnnotations.Schema;
using Clericuzzi.WebApi.Geo;

namespace RestApi.Entities
{
    [Table("company_address")]
    public partial class CompanyAddress : Crudable
    {
        // TABLE MAPPING CONSTANTS
        /// <summary>
        /// The database table's name that this class represents
        /// </summary>
        public const string TABLE_NAME = "company_address";
		/// <summary>
		/// The Company Address's class name
		/// </summary>
		public const string CLASS_NAME = "CompanyAddress";
		/// <summary>
		/// The CompanyAddress's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The CompanyAddress's CompanyId property
		/// </summary>
		public const string PROPERTY_COMPANY_ID = "CompanyId";
		/// <summary>
		/// The CompanyAddress's CompanyId parent property
		/// </summary>
		public const string PROPERTY_COMPANY_ID_PARENT = "CompanyIdParent";
		/// <summary>
		/// The CompanyAddress's Street property
		/// </summary>
		public const string PROPERTY_STREET = "Street";
		/// <summary>
		/// The CompanyAddress's Number property
		/// </summary>
		public const string PROPERTY_NUMBER = "Number";
		/// <summary>
		/// The CompanyAddress's Compliment property
		/// </summary>
		public const string PROPERTY_COMPLIMENT = "Compliment";
		/// <summary>
		/// The CompanyAddress's ZipcodeId property
		/// </summary>
		public const string PROPERTY_ZIPCODE_ID = "ZipcodeId";
		/// <summary>
		/// The CompanyAddress's ZipcodeId parent property
		/// </summary>
		public const string PROPERTY_ZIPCODE_ID_PARENT = "ZipcodeIdParent";
		/// <summary>
		/// The CompanyAddress's Reference property
		/// </summary>
		public const string PROPERTY_REFERENCE = "Reference";
		/// <summary>
		/// The CompanyAddress's TestingTimestamp property
		/// </summary>
		public const string PROPERTY_TESTING_TIMESTAMP = "TestingTimestamp";
		/// <summary>
		/// The CompanyAddress's IsValid property
		/// </summary>
		public const string PROPERTY_IS_VALID = "IsValid";

        /// <summary>
		/// Represents the id database column
		/// </summary>
		[Key]
		[Column("id")]
		public int Id { get; set; }
		/// <summary>
		/// Represents the company_id database column
		/// </summary>
		[ForeignKey("CompanyIdParent")]
		[Column("company_id")]
		public int CompanyId { get; set; }
		/// <summary>
		/// Represents the street database column
		/// </summary>
		[Column("street")]
		public string Street { get; set; }
		/// <summary>
		/// Represents the number database column
		/// </summary>
		[Column("number")]
		public string Number { get; set; }
		/// <summary>
		/// Represents the compliment database column
		/// </summary>
		[Column("compliment")]
		public string Compliment { get; set; }
		/// <summary>
		/// Represents the zipcode_id database column
		/// </summary>
		[ForeignKey("ZipcodeIdParent")]
		[Column("zipcode_id")]
		public int? ZipcodeId { get; set; }
		/// <summary>
		/// Represents the reference database column
		/// </summary>
		[Column("reference")]
		public string Reference { get; set; }
		/// <summary>
		/// Represents the testing_timestamp database column
		/// </summary>
		[Column("testing_timestamp")]
		public DateTime? TestingTimestamp { get; set; }
		/// <summary>
		/// Represents the is_valid database column
		/// </summary>
		[Column("is_valid")]
		public int? IsValid { get; set; }
		
		/// <summary>
		/// Represents the company_id foreign key object
		/// </summary>
		public Company CompanyIdParent { get; set; }
		/// <summary>
		/// Represents the zipcode_id foreign key object
		/// </summary>
		public GeographyZipcode ZipcodeIdParent { get; set; }

        /// <summary>
        /// A string representation of a single object
        /// </summary>
        public override string ToString()
        {
            return Summary;
        }
		
        // ABSTRACT IMPLEMENTATIONS
        /// <summary>
        /// Clears all navigation properties
        /// </summary>
        public override void ClearRelationalData()
        {
			CompanyIdParent = null;
			ZipcodeIdParent = null;
        }
        /// <summary>
        /// Clones an object
        /// </summary>
        /// <returns>a new object with the same property values</returns>
        public override Crudable Clone()
        {
			var clone = new CompanyAddress()
			{
				Id = Id,
				CompanyId = CompanyId,
				Street = Street,
				Number = Number,
				Compliment = Compliment,
				ZipcodeId = ZipcodeId,
				Reference = Reference,
				TestingTimestamp = TestingTimestamp,
				IsValid = IsValid,
			};
			
			clone.CompanyIdParent = CompanyIdParent;
			clone.ZipcodeIdParent = ZipcodeIdParent;

            return clone;
        }
	}
}