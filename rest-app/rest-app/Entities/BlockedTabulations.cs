﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Clericuzzi.Lib.Utils.Core;
using System.Collections.Generic;
using Clericuzzi.WebApi.Data.DatabaseContext;

namespace RestApi.Entities
{
    /// <summary>
    /// Filter defaults for the 'BlockedTabulations' class
    /// </summary>
    public partial class BlockedTabulationsFilter
	{
		/// <summary>
		/// the 'Id' propery
		/// </summary>
		public int? Id { get; set; }
		/// <summary>
		/// the 'TabulationId' propery
		/// </summary>
		public List<int> TabulationId { get; set; }
        /// <summary>
        /// the tabulation text
        /// </summary>
		public string Tabulation { get; set; }
    }

    /// <summary>
    /// Class that maps a database table
    /// </summary>
    public partial class BlockedTabulations
    {
        /// <summary>
        /// The sumary tries to identify a single object
        /// </summary>
		public override string Summary => $"{TabulationIdParent?.Summary ?? TabulationId.ToString()}".RemoveDoubleSpaces().Trim();

        /// <summary>
        /// Default constructor
        /// </summary>
		public BlockedTabulations()
        {
        }

		/// <summary>
        /// Loads all the external references to this type
        /// </summary>
        /// <param name="context">database connection</param>
		public async Task Load(DataContext context)
		{
			await Task.Delay(150);
			// await this.LoadReferences(context);
		}

        /// <summary>
        /// Defines the equality rule for this Type
        /// </summary>
        /// <param name="value">an object to be compared to</param>
        /// <returns>a flag that indicates if a given object is "Equals" to this instance</returns>
        public override bool Equals(object value)
        {
			BlockedTabulations item = value as BlockedTabulations;

			return !Object.ReferenceEquals(null, item)
				&& int.Equals(Id, item.Id);
        }
        /// <summary>
        /// Default implementation of get hashCode
        /// </summary>
        /// <returns>a hash key wanna be</returns>
        public override int GetHashCode()
        {
            unchecked
			{
				// Choose large primes to avoid hashing collisions
				const int HashingBase = (int) 2166136261;
				const int HashingMultiplier = 16777619;

				int hash = HashingBase;
				hash = (hash * HashingMultiplier) ^ (!int.ReferenceEquals(null, Id) ? Id.GetHashCode() : 0);

				return hash;
			}
        }
    }
}