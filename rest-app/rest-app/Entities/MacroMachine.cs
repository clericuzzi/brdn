﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Clericuzzi.Lib.Utils.Core;
using System.Collections.Generic;
using Clericuzzi.WebApi.Data.DatabaseContext;
using Microsoft.EntityFrameworkCore;

namespace RestApi.Entities
{
    /// <summary>
    /// Filter defaults for the 'MacroMachine' class
    /// </summary>
    public partial class MacroMachineFilter
	{
		/// <summary>
		/// the 'Id' propery
		/// </summary>
		public int? Id { get; set; }
		/// <summary>
		/// the 'MachineName' propery
		/// </summary>
		public string MachineName { get; set; }
		/// <summary>
		/// the 'IsAlive' propery
		/// </summary>
		public int? IsAlive { get; set; }
		/// <summary>
		/// the 'Updated' propery
		/// </summary>
		public int? Updated { get; set; }
	}

    /// <summary>
    /// Class that maps a database table
    /// </summary>
    public partial class MacroMachine
    {
        /// <summary>
        /// The sumary tries to identify a single object
        /// </summary>
		public override string Summary => $"{MachineName}".RemoveDoubleSpaces().Trim();

        public List<MacroMachineConfig> Configurations { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
		public MacroMachine()
        {
        }

		/// <summary>
        /// Loads all the external references to this type
        /// </summary>
        /// <param name="context">database connection</param>
		public void Load(DataContext context)
		{
            Configurations = context.GetDbSet<MacroMachineConfig>().Include(i => i.MachineIdParent).Include(i => i.MacroIdParent).Where(i => i.MachineId.Equals(Id)).ToList();
			// await this.LoadReferences(context);
		}

        /// <summary>
        /// Defines the equality rule for this Type
        /// </summary>
        /// <param name="value">an object to be compared to</param>
        /// <returns>a flag that indicates if a given object is "Equals" to this instance</returns>
        public override bool Equals(object value)
        {
			MacroMachine item = value as MacroMachine;

			return !Object.ReferenceEquals(null, item)
				&& int.Equals(Id, item.Id);
        }
        /// <summary>
        /// Default implementation of get hashCode
        /// </summary>
        /// <returns>a hash key wanna be</returns>
        public override int GetHashCode()
        {
            unchecked
			{
				// Choose large primes to avoid hashing collisions
				const int HashingBase = (int) 2166136261;
				const int HashingMultiplier = 16777619;

				int hash = HashingBase;
				hash = (hash * HashingMultiplier) ^ (!int.ReferenceEquals(null, Id) ? Id.GetHashCode() : 0);

				return hash;
			}
        }
    }
}