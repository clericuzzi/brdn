﻿using Clericuzzi.WebApi;
using Clericuzzi.WebApi.Entities;

using System;
using System.Linq;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Collections.Generic;
using Clericuzzi.Lib.Http.Core.Crud;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using Clericuzzi.WebApi.Data.DatabaseContext;
using System.ComponentModel.DataAnnotations.Schema;

namespace RestApi.Entities
{
    [Table("company_subtype")]
    public partial class CompanySubtype : Crudable
    {
        // TABLE MAPPING CONSTANTS
        /// <summary>
        /// The database table's name that this class represents
        /// </summary>
        public const string TABLE_NAME = "company_subtype";
		/// <summary>
		/// The Company Subtype's class name
		/// </summary>
		public const string CLASS_NAME = "CompanySubtype";
		/// <summary>
		/// The CompanySubtype's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The CompanySubtype's CompanyTypeId property
		/// </summary>
		public const string PROPERTY_COMPANY_TYPE_ID = "CompanyTypeId";
		/// <summary>
		/// The CompanySubtype's CompanyTypeId parent property
		/// </summary>
		public const string PROPERTY_COMPANY_TYPE_ID_PARENT = "CompanyTypeIdParent";
		/// <summary>
		/// The CompanySubtype's Subtype property
		/// </summary>
		public const string PROPERTY_SUBTYPE = "Subtype";

        /// <summary>
		/// Represents the id database column
		/// </summary>
		[Key]
		[Column("id")]
		public int Id { get; set; }
		/// <summary>
		/// Represents the company_type_id database column
		/// </summary>
		[ForeignKey("CompanyTypeIdParent")]
		[Column("company_type_id")]
		public int CompanyTypeId { get; set; }
		/// <summary>
		/// Represents the subtype database column
		/// </summary>
		[Column("subtype")]
		public string Subtype { get; set; }
		
		/// <summary>
		/// Represents the company_type_id foreign key object
		/// </summary>
		public CompanyType CompanyTypeIdParent { get; set; }

        /// <summary>
        /// A string representation of a single object
        /// </summary>
        public override string ToString()
        {
            return Summary;
        }
		
        // ABSTRACT IMPLEMENTATIONS
        /// <summary>
        /// Clears all navigation properties
        /// </summary>
        public override void ClearRelationalData()
        {
			CompanyTypeIdParent = null;
        }
        /// <summary>
        /// Clones an object
        /// </summary>
        /// <returns>a new object with the same property values</returns>
        public override Crudable Clone()
        {
			var clone = new CompanySubtype()
			{
				Id = Id,
				CompanyTypeId = CompanyTypeId,
				Subtype = Subtype,
			};
			
			clone.CompanyTypeIdParent = CompanyTypeIdParent;

            return clone;
        }
	}
}