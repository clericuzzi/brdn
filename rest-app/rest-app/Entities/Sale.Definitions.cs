﻿using Clericuzzi.WebApi;
using Clericuzzi.WebApi.Entities;

using System;
using System.Linq;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Collections.Generic;
using Clericuzzi.Lib.Http.Core.Crud;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using Clericuzzi.WebApi.Data.DatabaseContext;
using System.ComponentModel.DataAnnotations.Schema;

namespace RestApi.Entities
{
    [Table("sale")]
    public partial class Sale : Crudable
    {
        // TABLE MAPPING CONSTANTS
        /// <summary>
        /// The database table's name that this class represents
        /// </summary>
        public const string TABLE_NAME = "sale";
		/// <summary>
		/// The Sale's class name
		/// </summary>
		public const string CLASS_NAME = "Sale";
		/// <summary>
		/// The Sale's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The Sale's CompanyId property
		/// </summary>
		public const string PROPERTY_COMPANY_ID = "CompanyId";
		/// <summary>
		/// The Sale's CompanyId parent property
		/// </summary>
		public const string PROPERTY_COMPANY_ID_PARENT = "CompanyIdParent";
		/// <summary>
		/// The Sale's UserId property
		/// </summary>
		public const string PROPERTY_USER_ID = "UserId";
		/// <summary>
		/// The Sale's UserId parent property
		/// </summary>
		public const string PROPERTY_USER_ID_PARENT = "UserIdParent";
		/// <summary>
		/// The Sale's ProductId property
		/// </summary>
		public const string PROPERTY_PRODUCT_ID = "ProductId";
		/// <summary>
		/// The Sale's ProductId parent property
		/// </summary>
		public const string PROPERTY_PRODUCT_ID_PARENT = "ProductIdParent";
		/// <summary>
		/// The Sale's Timestamp property
		/// </summary>
		public const string PROPERTY_TIMESTAMP = "Timestamp";
		/// <summary>
		/// The Sale's ApprovedBy property
		/// </summary>
		public const string PROPERTY_APPROVED_BY = "ApprovedBy";
		/// <summary>
		/// The Sale's ApprovedBy parent property
		/// </summary>
		public const string PROPERTY_APPROVED_BY_PARENT = "ApprovedByParent";
		/// <summary>
		/// The Sale's ApprovedIn property
		/// </summary>
		public const string PROPERTY_APPROVED_IN = "ApprovedIn";

        /// <summary>
		/// Represents the id database column
		/// </summary>
		[Key]
		[Column("id")]
		public int Id { get; set; }
		/// <summary>
		/// Represents the company_id database column
		/// </summary>
		[ForeignKey("CompanyIdParent")]
		[Column("company_id")]
		public int CompanyId { get; set; }
		/// <summary>
		/// Represents the user_id database column
		/// </summary>
		[ForeignKey("UserIdParent")]
		[Column("user_id")]
		public int UserId { get; set; }
		/// <summary>
		/// Represents the product_id database column
		/// </summary>
		[ForeignKey("ProductIdParent")]
		[Column("product_id")]
		public int ProductId { get; set; }
		/// <summary>
		/// Represents the timestamp database column
		/// </summary>
		[Column("timestamp")]
		public DateTime Timestamp { get; set; }
		/// <summary>
		/// Represents the approved_by database column
		/// </summary>
		[ForeignKey("ApprovedByParent")]
		[Column("approved_by")]
		public int ApprovedBy { get; set; }
		/// <summary>
		/// Represents the approved_in database column
		/// </summary>
		[Column("approved_in")]
		public DateTime? ApprovedIn { get; set; }
		
		/// <summary>
		/// Represents the company_id foreign key object
		/// </summary>
		public Company CompanyIdParent { get; set; }
		/// <summary>
		/// Represents the user_id foreign key object
		/// </summary>
		public User UserIdParent { get; set; }
		/// <summary>
		/// Represents the product_id foreign key object
		/// </summary>
		public Product ProductIdParent { get; set; }
		/// <summary>
		/// Represents the approved_by foreign key object
		/// </summary>
		public User ApprovedByParent { get; set; }

        /// <summary>
        /// A string representation of a single object
        /// </summary>
        public override string ToString()
        {
            return Summary;
        }
		
        // ABSTRACT IMPLEMENTATIONS
        /// <summary>
        /// Clears all navigation properties
        /// </summary>
        public override void ClearRelationalData()
        {
			CompanyIdParent = null;
			UserIdParent = null;
			ProductIdParent = null;
			ApprovedByParent = null;
        }
        /// <summary>
        /// Clones an object
        /// </summary>
        /// <returns>a new object with the same property values</returns>
        public override Crudable Clone()
        {
			var clone = new Sale()
			{
				Id = Id,
				CompanyId = CompanyId,
				UserId = UserId,
				ProductId = ProductId,
				Timestamp = Timestamp,
				ApprovedBy = ApprovedBy,
				ApprovedIn = ApprovedIn,
			};
			
			clone.CompanyIdParent = CompanyIdParent;
			clone.UserIdParent = UserIdParent;
			clone.ProductIdParent = ProductIdParent;
			clone.ApprovedByParent = ApprovedByParent;

            return clone;
        }
	}
}