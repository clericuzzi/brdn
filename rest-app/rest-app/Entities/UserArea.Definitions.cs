﻿using Clericuzzi.WebApi;
using Clericuzzi.WebApi.Entities;

using System;
using System.Linq;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Collections.Generic;
using Clericuzzi.Lib.Http.Core.Crud;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using Clericuzzi.WebApi.Data.DatabaseContext;
using System.ComponentModel.DataAnnotations.Schema;

namespace RestApi.Entities
{
    [Table("user_area")]
    public partial class UserArea : Crudable
    {
        // TABLE MAPPING CONSTANTS
        /// <summary>
        /// The database table's name that this class represents
        /// </summary>
        public const string TABLE_NAME = "user_area";
		/// <summary>
		/// The User Area's class name
		/// </summary>
		public const string CLASS_NAME = "UserArea";
		/// <summary>
		/// The UserArea's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The UserArea's UserId property
		/// </summary>
		public const string PROPERTY_USER_ID = "UserId";
		/// <summary>
		/// The UserArea's UserId parent property
		/// </summary>
		public const string PROPERTY_USER_ID_PARENT = "UserIdParent";
		/// <summary>
		/// The UserArea's Cities property
		/// </summary>
		public const string PROPERTY_CITIES = "Cities";
		/// <summary>
		/// The UserArea's Neighbourhoods property
		/// </summary>
		public const string PROPERTY_NEIGHBOURHOODS = "Neighbourhoods";

        /// <summary>
		/// Represents the id database column
		/// </summary>
		[Key]
		[Column("id")]
		public int Id { get; set; }
		/// <summary>
		/// Represents the user_id database column
		/// </summary>
		[ForeignKey("UserIdParent")]
		[Column("user_id")]
		public int UserId { get; set; }
		/// <summary>
		/// Represents the cities database column
		/// </summary>
		[Column("cities")]
		public string Cities { get; set; }
		/// <summary>
		/// Represents the neighbourhoods database column
		/// </summary>
		[Column("neighbourhoods")]
		public string Neighbourhoods { get; set; }
		
		/// <summary>
		/// Represents the user_id foreign key object
		/// </summary>
		public User UserIdParent { get; set; }

        /// <summary>
        /// A string representation of a single object
        /// </summary>
        public override string ToString()
        {
            return Summary;
        }
		
        // ABSTRACT IMPLEMENTATIONS
        /// <summary>
        /// Clears all navigation properties
        /// </summary>
        public override void ClearRelationalData()
        {
			UserIdParent = null;
        }
        /// <summary>
        /// Clones an object
        /// </summary>
        /// <returns>a new object with the same property values</returns>
        public override Crudable Clone()
        {
			var clone = new UserArea()
			{
				Id = Id,
				UserId = UserId,
				Cities = Cities,
				Neighbourhoods = Neighbourhoods,
			};
			
			clone.UserIdParent = UserIdParent;

            return clone;
        }
	}
}