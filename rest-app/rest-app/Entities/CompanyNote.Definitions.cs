﻿using Clericuzzi.WebApi;
using Clericuzzi.WebApi.Entities;

using System;
using System.Linq;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Collections.Generic;
using Clericuzzi.Lib.Http.Core.Crud;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using Clericuzzi.WebApi.Data.DatabaseContext;
using System.ComponentModel.DataAnnotations.Schema;

namespace RestApi.Entities
{
    [Table("company_note")]
    public partial class CompanyNote : Crudable
    {
        // TABLE MAPPING CONSTANTS
        /// <summary>
        /// The database table's name that this class represents
        /// </summary>
        public const string TABLE_NAME = "company_note";
		/// <summary>
		/// The Company Note's class name
		/// </summary>
		public const string CLASS_NAME = "CompanyNote";
		/// <summary>
		/// The CompanyNote's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The CompanyNote's UserId property
		/// </summary>
		public const string PROPERTY_USER_ID = "UserId";
		/// <summary>
		/// The CompanyNote's UserId parent property
		/// </summary>
		public const string PROPERTY_USER_ID_PARENT = "UserIdParent";
		/// <summary>
		/// The CompanyNote's CompanyId property
		/// </summary>
		public const string PROPERTY_COMPANY_ID = "CompanyId";
		/// <summary>
		/// The CompanyNote's CompanyId parent property
		/// </summary>
		public const string PROPERTY_COMPANY_ID_PARENT = "CompanyIdParent";
		/// <summary>
		/// The CompanyNote's ContactId property
		/// </summary>
		public const string PROPERTY_CONTACT_ID = "ContactId";
		/// <summary>
		/// The CompanyNote's ContactId parent property
		/// </summary>
		public const string PROPERTY_CONTACT_ID_PARENT = "ContactIdParent";
		/// <summary>
		/// The CompanyNote's Comment property
		/// </summary>
		public const string PROPERTY_COMMENT = "Comment";
		/// <summary>
		/// The CompanyNote's Timestamp property
		/// </summary>
		public const string PROPERTY_TIMESTAMP = "Timestamp";

        /// <summary>
		/// Represents the id database column
		/// </summary>
		[Key]
		[Column("id")]
		public int Id { get; set; }
		/// <summary>
		/// Represents the user_id database column
		/// </summary>
		[ForeignKey("UserIdParent")]
		[Column("user_id")]
		public int UserId { get; set; }
		/// <summary>
		/// Represents the company_id database column
		/// </summary>
		[ForeignKey("CompanyIdParent")]
		[Column("company_id")]
		public int CompanyId { get; set; }
		/// <summary>
		/// Represents the contact_id database column
		/// </summary>
		[ForeignKey("ContactIdParent")]
		[Column("contact_id")]
		public int? ContactId { get; set; }
		/// <summary>
		/// Represents the comment database column
		/// </summary>
		[Column("comment")]
		public string Comment { get; set; }
		/// <summary>
		/// Represents the timestamp database column
		/// </summary>
		[Column("timestamp")]
		public DateTime Timestamp { get; set; }
		
		/// <summary>
		/// Represents the user_id foreign key object
		/// </summary>
		public User UserIdParent { get; set; }
		/// <summary>
		/// Represents the company_id foreign key object
		/// </summary>
		public Company CompanyIdParent { get; set; }
		/// <summary>
		/// Represents the contact_id foreign key object
		/// </summary>
		public CompanyContact ContactIdParent { get; set; }

        /// <summary>
        /// A string representation of a single object
        /// </summary>
        public override string ToString()
        {
            return Summary;
        }
		
        // ABSTRACT IMPLEMENTATIONS
        /// <summary>
        /// Clears all navigation properties
        /// </summary>
        public override void ClearRelationalData()
        {
			UserIdParent = null;
			CompanyIdParent = null;
			ContactIdParent = null;
        }
        /// <summary>
        /// Clones an object
        /// </summary>
        /// <returns>a new object with the same property values</returns>
        public override Crudable Clone()
        {
			var clone = new CompanyNote()
			{
				Id = Id,
				UserId = UserId,
				CompanyId = CompanyId,
				ContactId = ContactId,
				Comment = Comment,
				Timestamp = Timestamp,
			};
			
			clone.UserIdParent = UserIdParent;
			clone.CompanyIdParent = CompanyIdParent;
			clone.ContactIdParent = ContactIdParent;

            return clone;
        }
	}
}