﻿using Clericuzzi.WebApi;
using Clericuzzi.WebApi.Entities;

using System;
using System.Linq;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Collections.Generic;
using Clericuzzi.Lib.Http.Core.Crud;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using Clericuzzi.WebApi.Data.DatabaseContext;
using System.ComponentModel.DataAnnotations.Schema;
using Clericuzzi.WebApi.Activities;

namespace RestApi.Entities
{
    [Table("lead")]
    public partial class Lead : Crudable
    {
        // TABLE MAPPING CONSTANTS
        /// <summary>
        /// The database table's name that this class represents
        /// </summary>
        public const string TABLE_NAME = "lead";
		/// <summary>
		/// The Lead's class name
		/// </summary>
		public const string CLASS_NAME = "Lead";
		/// <summary>
		/// The Lead's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The Lead's UserId property
		/// </summary>
		public const string PROPERTY_USER_ID = "UserId";
		/// <summary>
		/// The Lead's UserId parent property
		/// </summary>
		public const string PROPERTY_USER_ID_PARENT = "UserIdParent";
		/// <summary>
		/// The Lead's CompanyId property
		/// </summary>
		public const string PROPERTY_COMPANY_ID = "CompanyId";
		/// <summary>
		/// The Lead's CompanyId parent property
		/// </summary>
		public const string PROPERTY_COMPANY_ID_PARENT = "CompanyIdParent";
		/// <summary>
		/// The Lead's ActivityId property
		/// </summary>
		public const string PROPERTY_ACTIVITY_ID = "ActivityId";
		/// <summary>
		/// The Lead's ActivityId parent property
		/// </summary>
		public const string PROPERTY_ACTIVITY_ID_PARENT = "ActivityIdParent";
		/// <summary>
		/// The Lead's Timestamp property
		/// </summary>
		public const string PROPERTY_TIMESTAMP = "Timestamp";

        /// <summary>
		/// Represents the id database column
		/// </summary>
		[Key]
		[Column("id")]
		public int Id { get; set; }
		/// <summary>
		/// Represents the user_id database column
		/// </summary>
		[ForeignKey("UserIdParent")]
		[Column("user_id")]
		public int? UserId { get; set; }
		/// <summary>
		/// Represents the company_id database column
		/// </summary>
		[ForeignKey("CompanyIdParent")]
		[Column("company_id")]
		public int CompanyId { get; set; }
		/// <summary>
		/// Represents the activity_id database column
		/// </summary>
		[ForeignKey("ActivityIdParent")]
		[Column("activity_id")]
		public int ActivityId { get; set; }
		/// <summary>
		/// Represents the timestamp database column
		/// </summary>
		[Column("timestamp")]
		public DateTime? Timestamp { get; set; }
		
		/// <summary>
		/// Represents the user_id foreign key object
		/// </summary>
		public User UserIdParent { get; set; }
		/// <summary>
		/// Represents the company_id foreign key object
		/// </summary>
		public Company CompanyIdParent { get; set; }
		/// <summary>
		/// Represents the activity_id foreign key object
		/// </summary>
		public Activity ActivityIdParent { get; set; }

        /// <summary>
        /// A string representation of a single object
        /// </summary>
        public override string ToString()
        {
            return Summary;
        }
		
        // ABSTRACT IMPLEMENTATIONS
        /// <summary>
        /// Clears all navigation properties
        /// </summary>
        public override void ClearRelationalData()
        {
			UserIdParent = null;
			CompanyIdParent = null;
			ActivityIdParent = null;
        }
        /// <summary>
        /// Clones an object
        /// </summary>
        /// <returns>a new object with the same property values</returns>
        public override Crudable Clone()
        {
			var clone = new Lead()
			{
				Id = Id,
				UserId = UserId,
				CompanyId = CompanyId,
				ActivityId = ActivityId,
				Timestamp = Timestamp,
			};
			
			clone.UserIdParent = UserIdParent;
			clone.CompanyIdParent = CompanyIdParent;
			clone.ActivityIdParent = ActivityIdParent;

            return clone;
        }
	}
}