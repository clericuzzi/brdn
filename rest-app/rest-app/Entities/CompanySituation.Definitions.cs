﻿using Clericuzzi.WebApi;
using Clericuzzi.WebApi.Entities;

using System;
using System.Linq;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Collections.Generic;
using Clericuzzi.Lib.Http.Core.Crud;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using Clericuzzi.WebApi.Data.DatabaseContext;
using System.ComponentModel.DataAnnotations.Schema;

namespace RestApi.Entities
{
    [Table("company_situation")]
    public partial class CompanySituation : Crudable
    {
        // TABLE MAPPING CONSTANTS
        /// <summary>
        /// The database table's name that this class represents
        /// </summary>
        public const string TABLE_NAME = "company_situation";
		/// <summary>
		/// The Company Situation's class name
		/// </summary>
		public const string CLASS_NAME = "CompanySituation";
		/// <summary>
		/// The CompanySituation's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The CompanySituation's Name property
		/// </summary>
		public const string PROPERTY_NAME = "Name";

        /// <summary>
		/// Represents the id database column
		/// </summary>
		[Key]
		[Column("id")]
		public int Id { get; set; }
		/// <summary>
		/// Represents the name database column
		/// </summary>
		[Column("name")]
		public string Name { get; set; }
		
        /// <summary>
        /// A string representation of a single object
        /// </summary>
        public override string ToString()
        {
            return Summary;
        }
		
        // ABSTRACT IMPLEMENTATIONS
        /// <summary>
        /// Clears all navigation properties
        /// </summary>
        public override void ClearRelationalData()
        {
        }
        /// <summary>
        /// Clones an object
        /// </summary>
        /// <returns>a new object with the same property values</returns>
        public override Crudable Clone()
        {
			var clone = new CompanySituation()
			{
				Id = Id,
				Name = Name,
			};
			
            return clone;
        }
	}
}