﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Clericuzzi.Lib.Utils.Core;
using System.Collections.Generic;
using Clericuzzi.WebApi.Data.DatabaseContext;

namespace RestApi.Entities
{
    /// <summary>
    /// Filter defaults for the 'CompanyContactLine' class
    /// </summary>
    public partial class CompanyContactLineFilter
	{
		/// <summary>
		/// the 'Id' propery
		/// </summary>
		public int? Id { get; set; }
		/// <summary>
		/// the 'CompanyContactId' propery
		/// </summary>
		public List<int> CompanyContactId { get; set; }
		/// <summary>
		/// the 'CompanyLineId' propery
		/// </summary>
		public List<int> CompanyLineId { get; set; }
	}

    /// <summary>
    /// Class that maps a database table
    /// </summary>
    public partial class CompanyContactLine
    {
        /// <summary>
        /// The sumary tries to identify a single object
        /// </summary>
		public override string Summary => $"{CompanyContactIdParent?.Summary ?? CompanyContactId.ToString()}".RemoveDoubleSpaces().Trim();

        /// <summary>
        /// Default constructor
        /// </summary>
		public CompanyContactLine()
        {
        }

		/// <summary>
        /// Loads all the external references to this type
        /// </summary>
        /// <param name="context">database connection</param>
		public async Task Load(DataContext context)
		{
			await Task.Delay(150);
			// await this.LoadReferences(context);
		}

        /// <summary>
        /// Defines the equality rule for this Type
        /// </summary>
        /// <param name="value">an object to be compared to</param>
        /// <returns>a flag that indicates if a given object is "Equals" to this instance</returns>
        public override bool Equals(object value)
        {
			CompanyContactLine item = value as CompanyContactLine;

			return !Object.ReferenceEquals(null, item)
				&& int.Equals(CompanyLineId, item.CompanyLineId)
				&& int.Equals(CompanyContactId, item.CompanyContactId);
        }
        /// <summary>
        /// Default implementation of get hashCode
        /// </summary>
        /// <returns>a hash key wanna be</returns>
        public override int GetHashCode()
        {
            unchecked
			{
				// Choose large primes to avoid hashing collisions
				const int HashingBase = (int) 2166136261;
				const int HashingMultiplier = 16777619;

				int hash = HashingBase;
				hash = (hash * HashingMultiplier) ^ (!int.ReferenceEquals(null, CompanyLineId) ? CompanyLineId.GetHashCode() : 0);
				hash = (hash * HashingMultiplier) ^ (!int.ReferenceEquals(null, CompanyContactId) ? CompanyContactId.GetHashCode() : 0);

				return hash;
			}
        }
    }
}