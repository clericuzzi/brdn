﻿using Clericuzzi.WebApi;
using Clericuzzi.WebApi.Entities;

using System;
using System.Linq;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Collections.Generic;
using Clericuzzi.Lib.Http.Core.Crud;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using Clericuzzi.WebApi.Data.DatabaseContext;
using System.ComponentModel.DataAnnotations.Schema;

namespace RestApi.Entities
{
    [Table("company_contact_line")]
    public partial class CompanyContactLine : Crudable
    {
        // TABLE MAPPING CONSTANTS
        /// <summary>
        /// The database table's name that this class represents
        /// </summary>
        public const string TABLE_NAME = "company_contact_line";
		/// <summary>
		/// The Company Contact Line's class name
		/// </summary>
		public const string CLASS_NAME = "CompanyContactLine";
		/// <summary>
		/// The CompanyContactLine's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The CompanyContactLine's CompanyContactId property
		/// </summary>
		public const string PROPERTY_COMPANY_CONTACT_ID = "CompanyContactId";
		/// <summary>
		/// The CompanyContactLine's CompanyContactId parent property
		/// </summary>
		public const string PROPERTY_COMPANY_CONTACT_ID_PARENT = "CompanyContactIdParent";
		/// <summary>
		/// The CompanyContactLine's CompanyLineId property
		/// </summary>
		public const string PROPERTY_COMPANY_LINE_ID = "CompanyLineId";
		/// <summary>
		/// The CompanyContactLine's CompanyLineId parent property
		/// </summary>
		public const string PROPERTY_COMPANY_LINE_ID_PARENT = "CompanyLineIdParent";

        /// <summary>
		/// Represents the id database column
		/// </summary>
		[Key]
		[Column("id")]
		public int Id { get; set; }
		/// <summary>
		/// Represents the company_contact_id database column
		/// </summary>
		[ForeignKey("CompanyContactIdParent")]
		[Column("company_contact_id")]
		public int CompanyContactId { get; set; }
		/// <summary>
		/// Represents the company_line_id database column
		/// </summary>
		[ForeignKey("CompanyLineIdParent")]
		[Column("company_line_id")]
		public int CompanyLineId { get; set; }
		
		/// <summary>
		/// Represents the company_contact_id foreign key object
		/// </summary>
		public CompanyContact CompanyContactIdParent { get; set; }
		/// <summary>
		/// Represents the company_line_id foreign key object
		/// </summary>
		public CompanyLine CompanyLineIdParent { get; set; }

        /// <summary>
        /// A string representation of a single object
        /// </summary>
        public override string ToString()
        {
            return Summary;
        }
		
        // ABSTRACT IMPLEMENTATIONS
        /// <summary>
        /// Clears all navigation properties
        /// </summary>
        public override void ClearRelationalData()
        {
			CompanyContactIdParent = null;
			CompanyLineIdParent = null;
        }
        /// <summary>
        /// Clones an object
        /// </summary>
        /// <returns>a new object with the same property values</returns>
        public override Crudable Clone()
        {
			var clone = new CompanyContactLine()
			{
				Id = Id,
				CompanyContactId = CompanyContactId,
				CompanyLineId = CompanyLineId,
			};
			
			clone.CompanyContactIdParent = CompanyContactIdParent;
			clone.CompanyLineIdParent = CompanyLineIdParent;

            return clone;
        }
	}
}