﻿using Clericuzzi.WebApi;
using Clericuzzi.WebApi.Entities;

using System;
using System.Linq;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Collections.Generic;
using Clericuzzi.Lib.Http.Core.Crud;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using Clericuzzi.WebApi.Data.DatabaseContext;
using System.ComponentModel.DataAnnotations.Schema;

namespace RestApi.Entities
{
    [Table("company_bill")]
    public partial class CompanyBill : Crudable
    {
        // TABLE MAPPING CONSTANTS
        /// <summary>
        /// The database table's name that this class represents
        /// </summary>
        public const string TABLE_NAME = "company_bill";
		/// <summary>
		/// The Company Bill's class name
		/// </summary>
		public const string CLASS_NAME = "CompanyBill";
		/// <summary>
		/// The CompanyBill's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The CompanyBill's CompanyId property
		/// </summary>
		public const string PROPERTY_COMPANY_ID = "CompanyId";
		/// <summary>
		/// The CompanyBill's CompanyId parent property
		/// </summary>
		public const string PROPERTY_COMPANY_ID_PARENT = "CompanyIdParent";
		/// <summary>
		/// The CompanyBill's Billing property
		/// </summary>
		public const string PROPERTY_BILLING = "Billing";
		/// <summary>
		/// The CompanyBill's Serial property
		/// </summary>
		public const string PROPERTY_SERIAL = "Serial";
		/// <summary>
		/// The CompanyBill's Value property
		/// </summary>
		public const string PROPERTY_VALUE = "Value";
		/// <summary>
		/// The CompanyBill's Due property
		/// </summary>
		public const string PROPERTY_DUE = "Due";

        /// <summary>
		/// Represents the id database column
		/// </summary>
		[Key]
		[Column("id")]
		public int Id { get; set; }
		/// <summary>
		/// Represents the company_id database column
		/// </summary>
		[ForeignKey("CompanyIdParent")]
		[Column("company_id")]
		public int CompanyId { get; set; }
		/// <summary>
		/// Represents the billing database column
		/// </summary>
		[Column("billing")]
		public string Billing { get; set; }
		/// <summary>
		/// Represents the serial database column
		/// </summary>
		[Column("serial")]
		public string Serial { get; set; }
		/// <summary>
		/// Represents the value database column
		/// </summary>
		[Column("value")]
		public double? Value { get; set; }
		/// <summary>
		/// Represents the due database column
		/// </summary>
		[Column("due")]
		public DateTime? Due { get; set; }
		
		/// <summary>
		/// Represents the company_id foreign key object
		/// </summary>
		public Company CompanyIdParent { get; set; }

        /// <summary>
        /// A string representation of a single object
        /// </summary>
        public override string ToString()
        {
            return Summary;
        }
		
        // ABSTRACT IMPLEMENTATIONS
        /// <summary>
        /// Clears all navigation properties
        /// </summary>
        public override void ClearRelationalData()
        {
			CompanyIdParent = null;
        }
        /// <summary>
        /// Clones an object
        /// </summary>
        /// <returns>a new object with the same property values</returns>
        public override Crudable Clone()
        {
			var clone = new CompanyBill()
			{
				Id = Id,
				CompanyId = CompanyId,
				Billing = Billing,
				Serial = Serial,
				Value = Value,
				Due = Due,
			};
			
			clone.CompanyIdParent = CompanyIdParent;

            return clone;
        }
	}
}