﻿using Clericuzzi.WebApi;
using Clericuzzi.WebApi.Entities;

using System;
using System.Linq;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Collections.Generic;
using Clericuzzi.Lib.Http.Core.Crud;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using Clericuzzi.WebApi.Data.DatabaseContext;
using System.ComponentModel.DataAnnotations.Schema;

namespace RestApi.Entities
{
    [Table("tabulation_type")]
    public partial class TabulationType : Crudable
    {
        // TABLE MAPPING CONSTANTS
        /// <summary>
        /// The database table's name that this class represents
        /// </summary>
        public const string TABLE_NAME = "tabulation_type";
		/// <summary>
		/// The Tabulation Type's class name
		/// </summary>
		public const string CLASS_NAME = "TabulationType";
		/// <summary>
		/// The TabulationType's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The TabulationType's Type property
		/// </summary>
		public const string PROPERTY_TYPE = "Type";

        /// <summary>
		/// Represents the id database column
		/// </summary>
		[Key]
		[Column("id")]
		public int Id { get; set; }
		/// <summary>
		/// Represents the type database column
		/// </summary>
		[Column("type")]
		public string Type { get; set; }
		
        /// <summary>
        /// A string representation of a single object
        /// </summary>
        public override string ToString()
        {
            return Summary;
        }
		
        // ABSTRACT IMPLEMENTATIONS
        /// <summary>
        /// Clears all navigation properties
        /// </summary>
        public override void ClearRelationalData()
        {
        }
        /// <summary>
        /// Clones an object
        /// </summary>
        /// <returns>a new object with the same property values</returns>
        public override Crudable Clone()
        {
			var clone = new TabulationType()
			{
				Id = Id,
				Type = Type,
			};
			
            return clone;
        }
	}
}