﻿using Clericuzzi.WebApi;
using Clericuzzi.WebApi.Entities;

using System;
using System.Linq;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Collections.Generic;
using Clericuzzi.Lib.Http.Core.Crud;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using Clericuzzi.WebApi.Data.DatabaseContext;
using System.ComponentModel.DataAnnotations.Schema;

namespace RestApi.Entities
{
    [Table("macro_machine_config")]
    public partial class MacroMachineConfig : Crudable
    {
        // TABLE MAPPING CONSTANTS
        /// <summary>
        /// The database table's name that this class represents
        /// </summary>
        public const string TABLE_NAME = "macro_machine_config";
		/// <summary>
		/// The Configuração de mineração's class name
		/// </summary>
		public const string CLASS_NAME = "MacroMachineConfig";
		/// <summary>
		/// The MacroMachineConfig's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The MacroMachineConfig's MachineId property
		/// </summary>
		public const string PROPERTY_MACHINE_ID = "MachineId";
		/// <summary>
		/// The MacroMachineConfig's MachineId parent property
		/// </summary>
		public const string PROPERTY_MACHINE_ID_PARENT = "MachineIdParent";
		/// <summary>
		/// The MacroMachineConfig's MacroId property
		/// </summary>
		public const string PROPERTY_MACRO_ID = "MacroId";
		/// <summary>
		/// The MacroMachineConfig's MacroId parent property
		/// </summary>
		public const string PROPERTY_MACRO_ID_PARENT = "MacroIdParent";
		/// <summary>
		/// The MacroMachineConfig's Instances property
		/// </summary>
		public const string PROPERTY_INSTANCES = "Instances";
		/// <summary>
		/// The MacroMachineConfig's Starting property
		/// </summary>
		public const string PROPERTY_STARTING = "Starting";
		/// <summary>
		/// The MacroMachineConfig's Ending property
		/// </summary>
		public const string PROPERTY_ENDING = "Ending";
		/// <summary>
		/// The MacroMachineConfig's IsAlive property
		/// </summary>
		public const string PROPERTY_IS_ALIVE = "IsAlive";
		/// <summary>
		/// The MacroMachineConfig's TurnOffAfter property
		/// </summary>
		public const string PROPERTY_TURN_OFF_AFTER = "TurnOffAfter";
		/// <summary>
		/// The MacroMachineConfig's FromBackground property
		/// </summary>
		public const string PROPERTY_FROM_BACKGROUND = "FromBackground";

        /// <summary>
		/// Represents the id database column
		/// </summary>
		[Key]
		[Column("id")]
		public int Id { get; set; }
		/// <summary>
		/// Represents the machine_id database column
		/// </summary>
		[ForeignKey("MachineIdParent")]
		[Column("machine_id")]
		public int MachineId { get; set; }
		/// <summary>
		/// Represents the macro_id database column
		/// </summary>
		[ForeignKey("MacroIdParent")]
		[Column("macro_id")]
		public int MacroId { get; set; }
		/// <summary>
		/// Represents the instances database column
		/// </summary>
		[Column("instances")]
		public int Instances { get; set; }
		/// <summary>
		/// Represents the starting database column
		/// </summary>
		[Column("starting")]
		public int? Starting { get; set; }
		/// <summary>
		/// Represents the ending database column
		/// </summary>
		[Column("ending")]
		public int? Ending { get; set; }
		/// <summary>
		/// Represents the is_alive database column
		/// </summary>
		[Column("is_alive")]
		public int IsAlive { get; set; }
		/// <summary>
		/// Represents the turn_off_after database column
		/// </summary>
		[Column("turn_off_after")]
		public int TurnOffAfter { get; set; }
		/// <summary>
		/// Represents the from_background database column
		/// </summary>
		[Column("from_background")]
		public int FromBackground { get; set; }
		
		/// <summary>
		/// Represents the machine_id foreign key object
		/// </summary>
		public MacroMachine MachineIdParent { get; set; }
		/// <summary>
		/// Represents the macro_id foreign key object
		/// </summary>
		public MacroLogin MacroIdParent { get; set; }

        /// <summary>
        /// A string representation of a single object
        /// </summary>
        public override string ToString()
        {
            return Summary;
        }
		
        // ABSTRACT IMPLEMENTATIONS
        /// <summary>
        /// Clears all navigation properties
        /// </summary>
        public override void ClearRelationalData()
        {
			MachineIdParent = null;
			MacroIdParent = null;
        }
        /// <summary>
        /// Clones an object
        /// </summary>
        /// <returns>a new object with the same property values</returns>
        public override Crudable Clone()
        {
			var clone = new MacroMachineConfig()
			{
				Id = Id,
				MachineId = MachineId,
				MacroId = MacroId,
				Instances = Instances,
				Starting = Starting,
				Ending = Ending,
				IsAlive = IsAlive,
				TurnOffAfter = TurnOffAfter,
				FromBackground = FromBackground,
			};
			
			clone.MachineIdParent = MachineIdParent;
			clone.MacroIdParent = MacroIdParent;

            return clone;
        }
	}
}