﻿using Clericuzzi.WebApi;
using Clericuzzi.WebApi.Entities;

using System;
using System.Linq;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Collections.Generic;
using Clericuzzi.Lib.Http.Core.Crud;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using Clericuzzi.WebApi.Data.DatabaseContext;
using System.ComponentModel.DataAnnotations.Schema;

namespace RestApi.Entities
{
    [Table("company_check_request")]
    public partial class CompanyCheckRequest : Crudable
    {
        // TABLE MAPPING CONSTANTS
        /// <summary>
        /// The database table's name that this class represents
        /// </summary>
        public const string TABLE_NAME = "company_check_request";
		/// <summary>
		/// The Company Check Request's class name
		/// </summary>
		public const string CLASS_NAME = "CompanyCheckRequest";
		/// <summary>
		/// The CompanyCheckRequest's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The CompanyCheckRequest's CheckType property
		/// </summary>
		public const string PROPERTY_CHECK_TYPE = "CheckType";
		/// <summary>
		/// The CompanyCheckRequest's CompanyId property
		/// </summary>
		public const string PROPERTY_COMPANY_ID = "CompanyId";
		/// <summary>
		/// The CompanyCheckRequest's CompanyId parent property
		/// </summary>
		public const string PROPERTY_COMPANY_ID_PARENT = "CompanyIdParent";

        /// <summary>
		/// Represents the id database column
		/// </summary>
		[Key]
		[Column("id")]
		public int Id { get; set; }
		/// <summary>
		/// Represents the check_type database column
		/// </summary>
		[Column("check_type")]
		public int CheckType { get; set; }
		/// <summary>
		/// Represents the company_id database column
		/// </summary>
		[ForeignKey("CompanyIdParent")]
		[Column("company_id")]
		public int CompanyId { get; set; }
		
		/// <summary>
		/// Represents the company_id foreign key object
		/// </summary>
		public Company CompanyIdParent { get; set; }

        /// <summary>
        /// A string representation of a single object
        /// </summary>
        public override string ToString()
        {
            return Summary;
        }
		
        // ABSTRACT IMPLEMENTATIONS
        /// <summary>
        /// Clears all navigation properties
        /// </summary>
        public override void ClearRelationalData()
        {
			CompanyIdParent = null;
        }
        /// <summary>
        /// Clones an object
        /// </summary>
        /// <returns>a new object with the same property values</returns>
        public override Crudable Clone()
        {
			var clone = new CompanyCheckRequest()
			{
				Id = Id,
				CheckType = CheckType,
				CompanyId = CompanyId,
			};
			
			clone.CompanyIdParent = CompanyIdParent;

            return clone;
        }
	}
}