﻿using Clericuzzi.WebApi;
using Clericuzzi.WebApi.Entities;

using System;
using System.Linq;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Collections.Generic;
using Clericuzzi.Lib.Http.Core.Crud;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using Clericuzzi.WebApi.Data.DatabaseContext;
using System.ComponentModel.DataAnnotations.Schema;

namespace RestApi.Entities
{
    [Table("contact_department")]
    public partial class ContactDepartment : Crudable
    {
        // TABLE MAPPING CONSTANTS
        /// <summary>
        /// The database table's name that this class represents
        /// </summary>
        public const string TABLE_NAME = "contact_department";
		/// <summary>
		/// The Contact Department's class name
		/// </summary>
		public const string CLASS_NAME = "ContactDepartment";
		/// <summary>
		/// The ContactDepartment's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The ContactDepartment's AccountId property
		/// </summary>
		public const string PROPERTY_ACCOUNT_ID = "AccountId";
		/// <summary>
		/// The ContactDepartment's AccountId parent property
		/// </summary>
		public const string PROPERTY_ACCOUNT_ID_PARENT = "AccountIdParent";
		/// <summary>
		/// The ContactDepartment's Name property
		/// </summary>
		public const string PROPERTY_NAME = "Name";

        /// <summary>
		/// Represents the id database column
		/// </summary>
		[Key]
		[Column("id")]
		public int Id { get; set; }
		/// <summary>
		/// Represents the account_id database column
		/// </summary>
		[ForeignKey("AccountIdParent")]
		[Column("account_id")]
		public int AccountId { get; set; }
		/// <summary>
		/// Represents the name database column
		/// </summary>
		[Column("name")]
		public string Name { get; set; }
		
		/// <summary>
		/// Represents the account_id foreign key object
		/// </summary>
		public Account AccountIdParent { get; set; }

        /// <summary>
        /// A string representation of a single object
        /// </summary>
        public override string ToString()
        {
            return Summary;
        }
		
        // ABSTRACT IMPLEMENTATIONS
        /// <summary>
        /// Clears all navigation properties
        /// </summary>
        public override void ClearRelationalData()
        {
			AccountIdParent = null;
        }
        /// <summary>
        /// Clones an object
        /// </summary>
        /// <returns>a new object with the same property values</returns>
        public override Crudable Clone()
        {
			var clone = new ContactDepartment()
			{
				Id = Id,
				AccountId = AccountId,
				Name = Name,
			};
			
			clone.AccountIdParent = AccountIdParent;

            return clone;
        }
	}
}