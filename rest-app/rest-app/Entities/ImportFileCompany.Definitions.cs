﻿using Clericuzzi.WebApi;
using Clericuzzi.WebApi.Entities;

using System;
using System.Linq;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Collections.Generic;
using Clericuzzi.Lib.Http.Core.Crud;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using Clericuzzi.WebApi.Data.DatabaseContext;
using System.ComponentModel.DataAnnotations.Schema;

namespace RestApi.Entities
{
    [Table("import_file_company")]
    public partial class ImportFileCompany : Crudable
    {
        // TABLE MAPPING CONSTANTS
        /// <summary>
        /// The database table's name that this class represents
        /// </summary>
        public const string TABLE_NAME = "import_file_company";
		/// <summary>
		/// The Import File Company's class name
		/// </summary>
		public const string CLASS_NAME = "ImportFileCompany";
		/// <summary>
		/// The ImportFileCompany's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The ImportFileCompany's ImportFileId property
		/// </summary>
		public const string PROPERTY_IMPORT_FILE_ID = "ImportFileId";
		/// <summary>
		/// The ImportFileCompany's ImportFileId parent property
		/// </summary>
		public const string PROPERTY_IMPORT_FILE_ID_PARENT = "ImportFileIdParent";
		/// <summary>
		/// The ImportFileCompany's CompanyId property
		/// </summary>
		public const string PROPERTY_COMPANY_ID = "CompanyId";
		/// <summary>
		/// The ImportFileCompany's CompanyId parent property
		/// </summary>
		public const string PROPERTY_COMPANY_ID_PARENT = "CompanyIdParent";

        /// <summary>
		/// Represents the id database column
		/// </summary>
		[Key]
		[Column("id")]
		public int Id { get; set; }
		/// <summary>
		/// Represents the import_file_id database column
		/// </summary>
		[ForeignKey("ImportFileIdParent")]
		[Column("import_file_id")]
		public int ImportFileId { get; set; }
		/// <summary>
		/// Represents the company_id database column
		/// </summary>
		[ForeignKey("CompanyIdParent")]
		[Column("company_id")]
		public int CompanyId { get; set; }
		
		/// <summary>
		/// Represents the import_file_id foreign key object
		/// </summary>
		public ImportFile ImportFileIdParent { get; set; }
		/// <summary>
		/// Represents the company_id foreign key object
		/// </summary>
		public Company CompanyIdParent { get; set; }

        /// <summary>
        /// A string representation of a single object
        /// </summary>
        public override string ToString()
        {
            return Summary;
        }
		
        // ABSTRACT IMPLEMENTATIONS
        /// <summary>
        /// Clears all navigation properties
        /// </summary>
        public override void ClearRelationalData()
        {
			ImportFileIdParent = null;
			CompanyIdParent = null;
        }
        /// <summary>
        /// Clones an object
        /// </summary>
        /// <returns>a new object with the same property values</returns>
        public override Crudable Clone()
        {
			var clone = new ImportFileCompany()
			{
				Id = Id,
				ImportFileId = ImportFileId,
				CompanyId = CompanyId,
			};
			
			clone.ImportFileIdParent = ImportFileIdParent;
			clone.CompanyIdParent = CompanyIdParent;

            return clone;
        }
	}
}