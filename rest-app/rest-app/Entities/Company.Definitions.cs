﻿using Clericuzzi.WebApi;
using Clericuzzi.WebApi.Entities;

using System;
using System.Linq;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Collections.Generic;
using Clericuzzi.Lib.Http.Core.Crud;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using Clericuzzi.WebApi.Data.DatabaseContext;
using System.ComponentModel.DataAnnotations.Schema;

namespace RestApi.Entities
{
    [Table("company")]
    public partial class Company : Crudable
    {
        // TABLE MAPPING CONSTANTS
        /// <summary>
        /// The database table's name that this class represents
        /// </summary>
        public const string TABLE_NAME = "company";
		/// <summary>
		/// The Company's class name
		/// </summary>
		public const string CLASS_NAME = "Company";
		/// <summary>
		/// The Company's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
        /// <summary>
        /// The Company's AccountId property
        /// </summary>
        public const string PROPERTY_ACCOUNT_ID = "AccountId";
        /// <summary>
        /// The Company's AccountId parent property
        /// </summary>
        public const string PROPERTY_ACCOUNT_ID_PARENT = "AccountIdParent";
        /// <summary>
        /// The Company's AccountId property
        /// </summary>
        public const string PROPERTY_PARENT_COMPANY_ID = "ParentCompanyId";
        /// <summary>
        /// The Company's AccountId parent property
        /// </summary>
        public const string PROPERTY_PARENT_COMPANY_ID_PARENT = "ParentCompanyIdParent";
        /// <summary>
        /// The Company's IsGroup property
        /// </summary>
        public const string PROPERTY_IS_GROUP = "IsGroup";
        /// <summary>
        /// The Company's UserId property
        /// </summary>
        public const string PROPERTY_USER_ID = "UserId";
        /// <summary>
        /// The Company's UserId parent property
        /// </summary>
        public const string PROPERTY_USER_ID_PARENT = "UserIdParent";
		/// <summary>
		/// The Company's CompanySubtypeId property
		/// </summary>
		public const string PROPERTY_COMPANY_SUBTYPE_ID = "CompanySubtypeId";
		/// <summary>
		/// The Company's CompanySubtypeId parent property
		/// </summary>
		public const string PROPERTY_COMPANY_SUBTYPE_ID_PARENT = "CompanySubtypeIdParent";
		/// <summary>
		/// The Company's Cnpj property
		/// </summary>
		public const string PROPERTY_CNPJ = "Cnpj";
		/// <summary>
		/// The Company's Phone property
		/// </summary>
		public const string PROPERTY_PHONE = "Phone";
		/// <summary>
		/// The Company's Description property
		/// </summary>
		public const string PROPERTY_DESCRIPTION = "Description";
		/// <summary>
		/// The Company's FormalName property
		/// </summary>
		public const string PROPERTY_FORMAL_NAME = "FormalName";
		/// <summary>
		/// The Company's FantasyName property
		/// </summary>
		public const string PROPERTY_FANTASY_NAME = "FantasyName";
		/// <summary>
		/// The Company's Latitude property
		/// </summary>
		public const string PROPERTY_LATITUDE = "Latitude";
		/// <summary>
		/// The Company's Longitude property
		/// </summary>
		public const string PROPERTY_LONGITUDE = "Longitude";
		/// <summary>
		/// The Company's Timestamp property
		/// </summary>
		public const string PROPERTY_TIMESTAMP = "Timestamp";
		/// <summary>
		/// The Company's Rank property
		/// </summary>
		public const string PROPERTY_RANK = "Rank";
		/// <summary>
		/// The Company's PrimaryActivity property
		/// </summary>
		public const string PROPERTY_PRIMARY_ACTIVITY = "PrimaryActivity";
		/// <summary>
		/// The Company's JuridicNature property
		/// </summary>
		public const string PROPERTY_JURIDIC_NATURE = "JuridicNature";
		/// <summary>
		/// The Company's ConvergingMessage property
		/// </summary>
		public const string PROPERTY_CONVERGING_MESSAGE = "ConvergingMessage";
		/// <summary>
		/// The Company's WalletMobile property
		/// </summary>
		public const string PROPERTY_WALLET_MOBILE = "WalletMobile";
		/// <summary>
		/// The Company's WalletLand property
		/// </summary>
		public const string PROPERTY_WALLET_LAND = "WalletLand";
		/// <summary>
		/// The Company's Capital property
		/// </summary>
		public const string PROPERTY_CAPITAL = "Capital";
		/// <summary>
		/// The Company's DateOpened property
		/// </summary>
		public const string PROPERTY_DATE_OPENED = "DateOpened";
		/// <summary>
		/// The Company's SituationId property
		/// </summary>
		public const string PROPERTY_SITUATION_ID = "SituationId";
		/// <summary>
		/// The Company's SituationId parent property
		/// </summary>
		public const string PROPERTY_SITUATION_ID_PARENT = "SituationIdParent";
		/// <summary>
		/// The Company's SalesForceId property
		/// </summary>
		public const string PROPERTY_SALES_FORCE_ID = "SalesForceId";
		/// <summary>
		/// The Company's DateLastUpdate property
		/// </summary>
		public const string PROPERTY_DATE_LAST_UPDATE = "DateLastUpdate";

        /// <summary>
		/// Represents the id database column
		/// </summary>
		[Key]
		[Column("id")]
		public int Id { get; set; }
		/// <summary>
		/// Represents the account_id database column
		/// </summary>
		[ForeignKey("AccountIdParent")]
		[Column("account_id")]
		public int AccountId { get; set; }
        /// <summary>
        /// the 'ParentCompanyId ' propery
        /// </summary>
		[ForeignKey("ParentCompanyIdParent")]
        [Column("parent_company_id")]
        public int? ParentCompanyId { get; set; }
        /// <summary>
        /// the 'IsGroup' propery
        /// </summary>
        [Column("is_group")]
        public int IsGroup { get; set; }
        /// <summary>
        /// Represents the user_id database column
        /// </summary>
        [ForeignKey("UserIdParent")]
		[Column("user_id")]
		public int? UserId { get; set; }
		/// <summary>
		/// Represents the company_subtype_id database column
		/// </summary>
		[ForeignKey("CompanySubtypeIdParent")]
		[Column("company_subtype_id")]
		public int? CompanySubtypeId { get; set; }
		/// <summary>
		/// Represents the cnpj database column
		/// </summary>
		[Column("cnpj")]
		public string Cnpj { get; set; }
		/// <summary>
		/// Represents the phone database column
		/// </summary>
		[Column("phone")]
		public string Phone { get; set; }
		/// <summary>
		/// Represents the description database column
		/// </summary>
		[Column("description")]
		public string Description { get; set; }
		/// <summary>
		/// Represents the formal_name database column
		/// </summary>
		[Column("formal_name")]
		public string FormalName { get; set; }
		/// <summary>
		/// Represents the fantasy_name database column
		/// </summary>
		[Column("fantasy_name")]
		public string FantasyName { get; set; }
		/// <summary>
		/// Represents the latitude database column
		/// </summary>
		[Column("latitude")]
		public double? Latitude { get; set; }
		/// <summary>
		/// Represents the longitude database column
		/// </summary>
		[Column("longitude")]
		public double? Longitude { get; set; }
		/// <summary>
		/// Represents the timestamp database column
		/// </summary>
		[Column("timestamp")]
		public DateTime Timestamp { get; set; }
		/// <summary>
		/// Represents the rank database column
		/// </summary>
		[Column("rank")]
		public string Rank { get; set; }
		/// <summary>
		/// Represents the primary_activity database column
		/// </summary>
		[Column("primary_activity")]
		public string PrimaryActivity { get; set; }
		/// <summary>
		/// Represents the juridic_nature database column
		/// </summary>
		[Column("juridic_nature")]
		public string JuridicNature { get; set; }
		/// <summary>
		/// Represents the converging_message database column
		/// </summary>
		[Column("converging_message")]
		public string ConvergingMessage { get; set; }
		/// <summary>
		/// Represents the wallet_mobile database column
		/// </summary>
		[Column("wallet_mobile")]
		public string WalletMobile { get; set; }
		/// <summary>
		/// Represents the wallet_land database column
		/// </summary>
		[Column("wallet_land")]
		public string WalletLand { get; set; }
		/// <summary>
		/// Represents the capital database column
		/// </summary>
		[Column("capital")]
		public double? Capital { get; set; }
		/// <summary>
		/// Represents the date_opened database column
		/// </summary>
		[Column("date_opened")]
		public DateTime? DateOpened { get; set; }
		/// <summary>
		/// Represents the situation_id database column
		/// </summary>
		[ForeignKey("SituationIdParent")]
		[Column("situation_id")]
		public int? SituationId { get; set; }
		/// <summary>
		/// Represents the sales_force_id database column
		/// </summary>
		[Column("sales_force_id")]
		public string SalesForceId { get; set; }
		/// <summary>
		/// Represents the date_last_update database column
		/// </summary>
		[Column("date_last_update")]
		public DateTime? DateLastUpdate { get; set; }
		
		/// <summary>
		/// Represents the account_id foreign key object
		/// </summary>
		public Account AccountIdParent { get; set; }
		/// <summary>
		/// Represents the user_id foreign key object
		/// </summary>
		public User UserIdParent { get; set; }
		/// <summary>
		/// Represents the company_subtype_id foreign key object
		/// </summary>
		public CompanySubtype CompanySubtypeIdParent { get; set; }
        /// <summary>
        /// Represents the situation_id foreign key object
        /// </summary>
        public CompanySituation SituationIdParent { get; set; }
        /// <summary>
        /// Represents the parent_company_id foreign key object
        /// </summary>
        public Company ParentCompanyIdParent { get; set; }

        /// <summary>
        /// A string representation of a single object
        /// </summary>
        public override string ToString()
        {
            return Summary;
        }
		
        // ABSTRACT IMPLEMENTATIONS
        /// <summary>
        /// Clears all navigation properties
        /// </summary>
        public override void ClearRelationalData()
        {
			UserIdParent = null;
			AccountIdParent = null;
			SituationIdParent = null;
            ParentCompanyIdParent = null;
			CompanySubtypeIdParent = null;
        }
        /// <summary>
        /// Clones an object
        /// </summary>
        /// <returns>a new object with the same property values</returns>
        public override Crudable Clone()
        {
			var clone = new Company()
			{
				Id = Id,
				AccountId = AccountId,
				UserId = UserId,
				CompanySubtypeId = CompanySubtypeId,
				Cnpj = Cnpj,
				Phone = Phone,
				Description = Description,
				FormalName = FormalName,
				FantasyName = FantasyName,
				Latitude = Latitude,
				Longitude = Longitude,
				Timestamp = Timestamp,
				Rank = Rank,
				PrimaryActivity = PrimaryActivity,
				JuridicNature = JuridicNature,
				ConvergingMessage = ConvergingMessage,
				WalletMobile = WalletMobile,
				WalletLand = WalletLand,
				Capital = Capital,
				DateOpened = DateOpened,
				SituationId = SituationId,
				SalesForceId = SalesForceId,
				DateLastUpdate = DateLastUpdate,
			};
			
			clone.UserIdParent = UserIdParent;
            clone.AccountIdParent = AccountIdParent;
			clone.SituationIdParent = SituationIdParent;
			clone.ParentCompanyIdParent = ParentCompanyIdParent;
			clone.CompanySubtypeIdParent = CompanySubtypeIdParent;

            return clone;
        }
	}
}