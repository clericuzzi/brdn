﻿using Clericuzzi.WebApi;
using Clericuzzi.WebApi.Entities;

using System;
using System.Linq;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Collections.Generic;
using Clericuzzi.Lib.Http.Core.Crud;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using Clericuzzi.WebApi.Data.DatabaseContext;
using System.ComponentModel.DataAnnotations.Schema;

namespace RestApi.Entities
{
    [Table("company_line")]
    public partial class CompanyLine : Crudable
    {
        // TABLE MAPPING CONSTANTS
        /// <summary>
        /// The database table's name that this class represents
        /// </summary>
        public const string TABLE_NAME = "company_line";
        /// <summary>
        /// The Company Line's class name
        /// </summary>
        public const string CLASS_NAME = "CompanyLine";
        /// <summary>
        /// The CompanyLine's Id property
        /// </summary>
        public const string PROPERTY_ID = "Id";
        /// <summary>
        /// The CompanyLine's CompanyId property
        /// </summary>
        public const string PROPERTY_COMPANY_ID = "CompanyId";
        /// <summary>
        /// The CompanyLine's CompanyId parent property
        /// </summary>
        public const string PROPERTY_COMPANY_ID_PARENT = "CompanyIdParent";
        /// <summary>
        /// The CompanyLine's Phone property
        /// </summary>
        public const string PROPERTY_PHONE = "Phone";
        /// <summary>
        /// The CompanyLine's Mobile property
        /// </summary>
        public const string PROPERTY_MOBILE = "Mobile";
        /// <summary>
        /// The CompanyLine's OperatorId property
        /// </summary>
        public const string PROPERTY_OPERATOR_ID = "OperatorId";
        /// <summary>
        /// The CompanyLine's OperatorId parent property
        /// </summary>
        public const string PROPERTY_OPERATOR_ID_PARENT = "OperatorIdParent";
        /// <summary>
        /// The CompanyLine's OperatorSince property
        /// </summary>
        public const string PROPERTY_OPERATOR_SINCE = "OperatorSince";
        /// <summary>
        /// The CompanyLine's TestingTimestamp property
        /// </summary>
        public const string PROPERTY_TESTING_TIMESTAMP = "TestingTimestamp";
        /// <summary>
        /// The CompanyLine's TestedTodayDnc property
        /// </summary>
        public const string PROPERTY_TESTED_TODAY_DNC = "TestedTodayDnc";
        /// <summary>
        /// The CompanyLine's TestedTodayProcon property
        /// </summary>
        public const string PROPERTY_TESTED_TODAY_PROCON = "TestedTodayProcon";
        /// <summary>
        /// The CompanyLine's IsValid property
        /// </summary>
        public const string PROPERTY_IS_VALID = "IsValid";

        /// <summary>
		/// Represents the id database column
		/// </summary>
		[Key]
        [Column("id")]
        public int Id { get; set; }
        /// <summary>
        /// Represents the company_id database column
        /// </summary>
        [ForeignKey("CompanyIdParent")]
        [Column("company_id")]
        public int CompanyId { get; set; }
        /// <summary>
        /// Represents the phone database column
        /// </summary>
        [Column("phone")]
        public string Phone { get; set; }
        /// <summary>
        /// Represents the mobile database column
        /// </summary>
        [Column("mobile")]
        public int Mobile { get; set; }
        /// <summary>
        /// Represents the operator_id database column
        /// </summary>
        [ForeignKey("OperatorIdParent")]
        [Column("operator_id")]
        public int? OperatorId { get; set; }
        /// <summary>
        /// Represents the operator_since database column
        /// </summary>
        [Column("operator_since")]
        public DateTime? OperatorSince { get; set; }
        /// <summary>
        /// Represents the testing_timestamp database column
        /// </summary>
        [Column("testing_timestamp")]
        public DateTime? TestingTimestamp { get; set; }
        /// <summary>
        /// Represents the tested_today_dnc database column
        /// </summary>
        [Column("tested_today_dnc")]
        public int TestedTodayDnc { get; set; }
        /// <summary>
        /// Represents the tested_today_procon database column
        /// </summary>
        [Column("tested_today_procon")]
        public int TestedTodayProcon { get; set; }
        /// <summary>
        /// Represents the is_valid database column
        /// </summary>
        [Column("is_valid")]
        public int IsValid { get; set; }

        /// <summary>
        /// Represents the company_id foreign key object
        /// </summary>
        public Company CompanyIdParent { get; set; }
        /// <summary>
        /// Represents the operator_id foreign key object
        /// </summary>
        public PhoneOperator OperatorIdParent { get; set; }

        /// <summary>
        /// A string representation of a single object
        /// </summary>
        public override string ToString()
        {
            return Summary;
        }

        // ABSTRACT IMPLEMENTATIONS
        /// <summary>
        /// Clears all navigation properties
        /// </summary>
        public override void ClearRelationalData()
        {
            CompanyIdParent = null;
            OperatorIdParent = null;
        }
        /// <summary>
        /// Clones an object
        /// </summary>
        /// <returns>a new object with the same property values</returns>
        public override Crudable Clone()
        {
            var clone = new CompanyLine()
            {
                Id = Id,
                CompanyId = CompanyId,
                Phone = Phone,
                Mobile = Mobile,
                OperatorId = OperatorId,
                OperatorSince = OperatorSince,
                TestingTimestamp = TestingTimestamp,
                TestedTodayDnc = TestedTodayDnc,
                TestedTodayProcon = TestedTodayProcon,
                IsValid = IsValid,
            };

            clone.CompanyIdParent = CompanyIdParent;
            clone.OperatorIdParent = OperatorIdParent;

            return clone;
        }
    }
}