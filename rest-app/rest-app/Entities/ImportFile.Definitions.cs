﻿using Clericuzzi.WebApi;
using Clericuzzi.WebApi.Entities;

using System;
using System.Linq;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Collections.Generic;
using Clericuzzi.Lib.Http.Core.Crud;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using Clericuzzi.WebApi.Data.DatabaseContext;
using System.ComponentModel.DataAnnotations.Schema;

namespace RestApi.Entities
{
    [Table("import_file")]
    public partial class ImportFile : Crudable
    {
        // TABLE MAPPING CONSTANTS
        /// <summary>
        /// The database table's name that this class represents
        /// </summary>
        public const string TABLE_NAME = "import_file";
		/// <summary>
		/// The Import File's class name
		/// </summary>
		public const string CLASS_NAME = "ImportFile";
		/// <summary>
		/// The ImportFile's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The ImportFile's UserId property
		/// </summary>
		public const string PROPERTY_USER_ID = "UserId";
		/// <summary>
		/// The ImportFile's UserId parent property
		/// </summary>
		public const string PROPERTY_USER_ID_PARENT = "UserIdParent";
		/// <summary>
		/// The ImportFile's CampaignName property
		/// </summary>
		public const string PROPERTY_CAMPAIGN_NAME = "CampaignName";
		/// <summary>
		/// The ImportFile's Active property
		/// </summary>
		public const string PROPERTY_ACTIVE = "Active";
		/// <summary>
		/// The ImportFile's Timestamp property
		/// </summary>
		public const string PROPERTY_TIMESTAMP = "Timestamp";

        /// <summary>
		/// Represents the id database column
		/// </summary>
		[Key]
		[Column("id")]
		public int Id { get; set; }
		/// <summary>
		/// Represents the user_id database column
		/// </summary>
		[ForeignKey("UserIdParent")]
		[Column("user_id")]
		public int UserId { get; set; }
		/// <summary>
		/// Represents the campaign_name database column
		/// </summary>
		[Column("campaign_name")]
		public string CampaignName { get; set; }
		/// <summary>
		/// Represents the active database column
		/// </summary>
		[Column("active")]
		public int Active { get; set; }
		/// <summary>
		/// Represents the timestamp database column
		/// </summary>
		[Column("timestamp")]
		public DateTime Timestamp { get; set; }
		
		/// <summary>
		/// Represents the user_id foreign key object
		/// </summary>
		public User UserIdParent { get; set; }

        /// <summary>
        /// A string representation of a single object
        /// </summary>
        public override string ToString()
        {
            return Summary;
        }
		
        // ABSTRACT IMPLEMENTATIONS
        /// <summary>
        /// Clears all navigation properties
        /// </summary>
        public override void ClearRelationalData()
        {
			UserIdParent = null;
        }
        /// <summary>
        /// Clones an object
        /// </summary>
        /// <returns>a new object with the same property values</returns>
        public override Crudable Clone()
        {
			var clone = new ImportFile()
			{
				Id = Id,
				UserId = UserId,
				CampaignName = CampaignName,
				Active = Active,
				Timestamp = Timestamp,
			};
			
			clone.UserIdParent = UserIdParent;

            return clone;
        }
	}
}