﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Clericuzzi.Lib.Utils.Core;
using System.Collections.Generic;
using Clericuzzi.WebApi.Data.DatabaseContext;

namespace RestApi.Entities
{
    /// <summary>
    /// Filter defaults for the 'Company' class
    /// </summary>
    public partial class CompanyFilter
	{
		/// <summary>
		/// the 'Id' propery
		/// </summary>
		public int? Id { get; set; }
        /// <summary>
        /// the 'AccountId' propery
        /// </summary>
        public List<int> AccountId { get; set; }
        /// <summary>
        /// the 'ParentCompanyId ' propery
        /// </summary>
        public List<int> ParentCompanyId { get; set; }
        /// <summary>
        /// the 'IsGroup' propery
        /// </summary>
        public int IsGroup { get; set; }
        /// <summary>
        /// the 'UserId' propery
        /// </summary>
        public List<int> UserId { get; set; }
		/// <summary>
		/// the 'CompanySubtypeId' propery
		/// </summary>
		public List<int> CompanySubtypeId { get; set; }
		/// <summary>
		/// the 'Cnpj' propery
		/// </summary>
		public string Cnpj { get; set; }
		/// <summary>
		/// the 'Phone' propery
		/// </summary>
		public string Phone { get; set; }
		/// <summary>
		/// the 'Description' propery
		/// </summary>
		public string Description { get; set; }
		/// <summary>
		/// the 'FormalName' propery
		/// </summary>
		public string FormalName { get; set; }
		/// <summary>
		/// the 'FantasyName' propery
		/// </summary>
		public string FantasyName { get; set; }
		/// <summary>
		/// the 'Latitude' propery
		/// </summary>
		public double? Latitude { get; set; }
		/// <summary>
		/// the 'Longitude' propery
		/// </summary>
		public double? Longitude { get; set; }
		/// <summary>
		/// the 'Timestamp' propery
		/// </summary>
		public DateTime? Timestamp { get; set; }
		/// <summary>
		/// the 'Rank' propery
		/// </summary>
		public string Rank { get; set; }
		/// <summary>
		/// the 'Capital' propery
		/// </summary>
		public double? Capital { get; set; }
		/// <summary>
		/// the 'DateOpened' propery
		/// </summary>
		public DateTime? DateOpened { get; set; }
		/// <summary>
		/// the 'SituationId' propery
		/// </summary>
		public List<int> SituationId { get; set; }
		/// <summary>
		/// the 'SalesForceId' propery
		/// </summary>
		public string SalesForceId { get; set; }
		/// <summary>
		/// the 'DateLastUpdate' propery
		/// </summary>
		public DateTime? DateLastUpdate { get; set; }
	}

    /// <summary>
    /// Class that maps a database table
    /// </summary>
    public partial class Company
    {
        /// <summary>
        /// The sumary tries to identify a single object
        /// </summary>
		public override string Summary => $"{AccountIdParent?.Summary ?? AccountId.ToString()}".RemoveDoubleSpaces().Trim();

        /// <summary>
        /// Default constructor
        /// </summary>
		public Company()
        {
			this.Timestamp = DateTime.Now;
        }

		/// <summary>
        /// Loads all the external references to this type
        /// </summary>
        /// <param name="context">database connection</param>
		public async Task Load(DataContext context)
		{
			await Task.Delay(150);
			// await this.LoadReferences(context);
		}

        /// <summary>
        /// Defines the equality rule for this Type
        /// </summary>
        /// <param name="value">an object to be compared to</param>
        /// <returns>a flag that indicates if a given object is "Equals" to this instance</returns>
        public override bool Equals(object value)
        {
			Company item = value as Company;

			return !Object.ReferenceEquals(null, item)
				&& int.Equals(Id, item.Id);
        }
        /// <summary>
        /// Default implementation of get hashCode
        /// </summary>
        /// <returns>a hash key wanna be</returns>
        public override int GetHashCode()
        {
            unchecked
			{
				// Choose large primes to avoid hashing collisions
				const int HashingBase = (int) 2166136261;
				const int HashingMultiplier = 16777619;

				int hash = HashingBase;
				hash = (hash * HashingMultiplier) ^ (!int.ReferenceEquals(null, Id) ? Id.GetHashCode() : 0);

				return hash;
			}
        }
    }
}