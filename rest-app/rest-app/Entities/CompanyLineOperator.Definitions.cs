﻿using Clericuzzi.WebApi;
using Clericuzzi.WebApi.Entities;

using System;
using System.Linq;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Collections.Generic;
using Clericuzzi.Lib.Http.Core.Crud;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using Clericuzzi.WebApi.Data.DatabaseContext;
using System.ComponentModel.DataAnnotations.Schema;

namespace RestApi.Entities
{
    [Table("company_line_operator")]
    public partial class CompanyLineOperator : Crudable
    {
        // TABLE MAPPING CONSTANTS
        /// <summary>
        /// The database table's name that this class represents
        /// </summary>
        public const string TABLE_NAME = "company_line_operator";
		/// <summary>
		/// The Company Line Operator's class name
		/// </summary>
		public const string CLASS_NAME = "CompanyLineOperator";
		/// <summary>
		/// The CompanyLineOperator's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The CompanyLineOperator's CompanyLineId property
		/// </summary>
		public const string PROPERTY_COMPANY_LINE_ID = "CompanyLineId";
		/// <summary>
		/// The CompanyLineOperator's CompanyLineId parent property
		/// </summary>
		public const string PROPERTY_COMPANY_LINE_ID_PARENT = "CompanyLineIdParent";
		/// <summary>
		/// The CompanyLineOperator's OperatorId property
		/// </summary>
		public const string PROPERTY_OPERATOR_ID = "OperatorId";
		/// <summary>
		/// The CompanyLineOperator's OperatorId parent property
		/// </summary>
		public const string PROPERTY_OPERATOR_ID_PARENT = "OperatorIdParent";
		/// <summary>
		/// The CompanyLineOperator's OperatorSince property
		/// </summary>
		public const string PROPERTY_OPERATOR_SINCE = "OperatorSince";

        /// <summary>
		/// Represents the id database column
		/// </summary>
		[Key]
		[Column("id")]
		public int Id { get; set; }
		/// <summary>
		/// Represents the company_line_id database column
		/// </summary>
		[ForeignKey("CompanyLineIdParent")]
		[Column("company_line_id")]
		public int CompanyLineId { get; set; }
		/// <summary>
		/// Represents the operator_id database column
		/// </summary>
		[ForeignKey("OperatorIdParent")]
		[Column("operator_id")]
		public int? OperatorId { get; set; }
		/// <summary>
		/// Represents the operator_since database column
		/// </summary>
		[Column("operator_since")]
		public DateTime? OperatorSince { get; set; }
		
		/// <summary>
		/// Represents the company_line_id foreign key object
		/// </summary>
		public CompanyLine CompanyLineIdParent { get; set; }
		/// <summary>
		/// Represents the operator_id foreign key object
		/// </summary>
		public PhoneOperator OperatorIdParent { get; set; }

        /// <summary>
        /// A string representation of a single object
        /// </summary>
        public override string ToString()
        {
            return Summary;
        }
		
        // ABSTRACT IMPLEMENTATIONS
        /// <summary>
        /// Clears all navigation properties
        /// </summary>
        public override void ClearRelationalData()
        {
			CompanyLineIdParent = null;
			OperatorIdParent = null;
        }
        /// <summary>
        /// Clones an object
        /// </summary>
        /// <returns>a new object with the same property values</returns>
        public override Crudable Clone()
        {
			var clone = new CompanyLineOperator()
			{
				Id = Id,
				CompanyLineId = CompanyLineId,
				OperatorId = OperatorId,
				OperatorSince = OperatorSince,
			};
			
			clone.CompanyLineIdParent = CompanyLineIdParent;
			clone.OperatorIdParent = OperatorIdParent;

            return clone;
        }
	}
}