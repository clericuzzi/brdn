﻿using Clericuzzi.WebApi;
using Clericuzzi.WebApi.Entities;

using System;
using System.Linq;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Collections.Generic;
using Clericuzzi.Lib.Http.Core.Crud;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using Clericuzzi.WebApi.Data.DatabaseContext;
using System.ComponentModel.DataAnnotations.Schema;

namespace RestApi.Entities
{
    [Table("company_availability")]
    public partial class CompanyAvailability : Crudable
    {
        // TABLE MAPPING CONSTANTS
        /// <summary>
        /// The database table's name that this class represents
        /// </summary>
        public const string TABLE_NAME = "company_availability";
		/// <summary>
		/// The Company Availability's class name
		/// </summary>
		public const string CLASS_NAME = "CompanyAvailability";
		/// <summary>
		/// The CompanyAvailability's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The CompanyAvailability's CompanyAddressId property
		/// </summary>
		public const string PROPERTY_COMPANY_ADDRESS_ID = "CompanyAddressId";
		/// <summary>
		/// The CompanyAvailability's CompanyAddressId parent property
		/// </summary>
		public const string PROPERTY_COMPANY_ADDRESS_ID_PARENT = "CompanyAddressIdParent";
		/// <summary>
		/// The CompanyAvailability's Message property
		/// </summary>
		public const string PROPERTY_MESSAGE = "Message";
		/// <summary>
		/// The CompanyAvailability's ClosetDistance property
		/// </summary>
		public const string PROPERTY_CLOSET_DISTANCE = "ClosetDistance";
		/// <summary>
		/// The CompanyAvailability's Lines property
		/// </summary>
		public const string PROPERTY_LINES = "Lines";
		/// <summary>
		/// The CompanyAvailability's Until20 property
		/// </summary>
		public const string PROPERTY_UNTIL_20 = "Until20";
		/// <summary>
		/// The CompanyAvailability's Until20Max property
		/// </summary>
		public const string PROPERTY_UNTIL_20_MAX = "Until20Max";
		/// <summary>
		/// The CompanyAvailability's Until20Sip property
		/// </summary>
		public const string PROPERTY_UNTIL_20_SIP = "Until20Sip";
		/// <summary>
		/// The CompanyAvailability's From21UpTo50 property
		/// </summary>
		public const string PROPERTY_FROM_21_UP_TO_50 = "From21UpTo50";
		/// <summary>
		/// The CompanyAvailability's From21UpTo50Max property
		/// </summary>
		public const string PROPERTY_FROM_21_UP_TO_50_MAX = "From21UpTo50Max";
		/// <summary>
		/// The CompanyAvailability's From21UpTo50Sip property
		/// </summary>
		public const string PROPERTY_FROM_21_UP_TO_50_SIP = "From21UpTo50Sip";
		/// <summary>
		/// The CompanyAvailability's Over50 property
		/// </summary>
		public const string PROPERTY_OVER_50 = "Over50";
		/// <summary>
		/// The CompanyAvailability's Over50Max property
		/// </summary>
		public const string PROPERTY_OVER_50_MAX = "Over50Max";
		/// <summary>
		/// The CompanyAvailability's Over50Sip property
		/// </summary>
		public const string PROPERTY_OVER_50_SIP = "Over50Sip";
		/// <summary>
		/// The CompanyAvailability's Network property
		/// </summary>
		public const string PROPERTY_NETWORK = "Network";
		/// <summary>
		/// The CompanyAvailability's Tecnology property
		/// </summary>
		public const string PROPERTY_TECNOLOGY = "Tecnology";
		/// <summary>
		/// The CompanyAvailability's ClosetTopSpeed property
		/// </summary>
		public const string PROPERTY_CLOSET_TOP_SPEED = "ClosetTopSpeed";
		/// <summary>
		/// The CompanyAvailability's TecnologyForTv property
		/// </summary>
		public const string PROPERTY_TECNOLOGY_FOR_TV = "TecnologyForTv";
		/// <summary>
		/// The CompanyAvailability's Timestamp property
		/// </summary>
		public const string PROPERTY_TIMESTAMP = "Timestamp";

        /// <summary>
		/// Represents the id database column
		/// </summary>
		[Key]
		[Column("id")]
		public int Id { get; set; }
		/// <summary>
		/// Represents the company_address_id database column
		/// </summary>
		[ForeignKey("CompanyAddressIdParent")]
		[Column("company_address_id")]
		public int CompanyAddressId { get; set; }
		/// <summary>
		/// Represents the message database column
		/// </summary>
		[Column("message")]
		public string Message { get; set; }
		/// <summary>
		/// Represents the closet_distance database column
		/// </summary>
		[Column("closet_distance")]
		public int? ClosetDistance { get; set; }
		/// <summary>
		/// Represents the lines database column
		/// </summary>
		[Column("lines")]
		public int? Lines { get; set; }
		/// <summary>
		/// Represents the until_20 database column
		/// </summary>
		[Column("until_20")]
		public int? Until20 { get; set; }
		/// <summary>
		/// Represents the until_20_max database column
		/// </summary>
		[Column("until_20_max")]
		public string Until20Max { get; set; }
		/// <summary>
		/// Represents the until_20_sip database column
		/// </summary>
		[Column("until_20_sip")]
		public int? Until20Sip { get; set; }
		/// <summary>
		/// Represents the from_21_up_to_50 database column
		/// </summary>
		[Column("from_21_up_to_50")]
		public int? From21UpTo50 { get; set; }
		/// <summary>
		/// Represents the from_21_up_to_50_max database column
		/// </summary>
		[Column("from_21_up_to_50_max")]
		public string From21UpTo50Max { get; set; }
		/// <summary>
		/// Represents the from_21_up_to_50_sip database column
		/// </summary>
		[Column("from_21_up_to_50_sip")]
		public int? From21UpTo50Sip { get; set; }
		/// <summary>
		/// Represents the over_50 database column
		/// </summary>
		[Column("over_50")]
		public int? Over50 { get; set; }
		/// <summary>
		/// Represents the over_50_max database column
		/// </summary>
		[Column("over_50_max")]
		public string Over50Max { get; set; }
		/// <summary>
		/// Represents the over_50_sip database column
		/// </summary>
		[Column("over_50_sip")]
		public int? Over50Sip { get; set; }
		/// <summary>
		/// Represents the network database column
		/// </summary>
		[Column("network")]
		public string Network { get; set; }
		/// <summary>
		/// Represents the tecnology database column
		/// </summary>
		[Column("tecnology")]
		public string Tecnology { get; set; }
		/// <summary>
		/// Represents the closet_top_speed database column
		/// </summary>
		[Column("closet_top_speed")]
		public int? ClosetTopSpeed { get; set; }
		/// <summary>
		/// Represents the tecnology_for_tv database column
		/// </summary>
		[Column("tecnology_for_tv")]
		public string TecnologyForTv { get; set; }
		/// <summary>
		/// Represents the timestamp database column
		/// </summary>
		[Column("timestamp")]
		public DateTime Timestamp { get; set; }
		
		/// <summary>
		/// Represents the company_address_id foreign key object
		/// </summary>
		public CompanyAddress CompanyAddressIdParent { get; set; }

        /// <summary>
        /// A string representation of a single object
        /// </summary>
        public override string ToString()
        {
            return Summary;
        }
		
        // ABSTRACT IMPLEMENTATIONS
        /// <summary>
        /// Clears all navigation properties
        /// </summary>
        public override void ClearRelationalData()
        {
			CompanyAddressIdParent = null;
        }
        /// <summary>
        /// Clones an object
        /// </summary>
        /// <returns>a new object with the same property values</returns>
        public override Crudable Clone()
        {
			var clone = new CompanyAvailability()
			{
				Id = Id,
				CompanyAddressId = CompanyAddressId,
				Message = Message,
				ClosetDistance = ClosetDistance,
				Lines = Lines,
				Until20 = Until20,
				Until20Max = Until20Max,
				Until20Sip = Until20Sip,
				From21UpTo50 = From21UpTo50,
				From21UpTo50Max = From21UpTo50Max,
				From21UpTo50Sip = From21UpTo50Sip,
				Over50 = Over50,
				Over50Max = Over50Max,
				Over50Sip = Over50Sip,
				Network = Network,
				Tecnology = Tecnology,
				ClosetTopSpeed = ClosetTopSpeed,
				TecnologyForTv = TecnologyForTv,
				Timestamp = Timestamp,
			};
			
			clone.CompanyAddressIdParent = CompanyAddressIdParent;

            return clone;
        }
	}
}