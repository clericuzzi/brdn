﻿using Clericuzzi.WebApi;
using Clericuzzi.WebApi.Entities;

using System;
using System.Linq;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Collections.Generic;
using Clericuzzi.Lib.Http.Core.Crud;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using Clericuzzi.WebApi.Data.DatabaseContext;
using System.ComponentModel.DataAnnotations.Schema;

namespace RestApi.Entities
{
    [Table("company_active_service")]
    public partial class CompanyActiveService : Crudable
    {
        // TABLE MAPPING CONSTANTS
        /// <summary>
        /// The database table's name that this class represents
        /// </summary>
        public const string TABLE_NAME = "company_active_service";
		/// <summary>
		/// The Company Active Service's class name
		/// </summary>
		public const string CLASS_NAME = "CompanyActiveService";
		/// <summary>
		/// The CompanyActiveService's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The CompanyActiveService's CompanyId property
		/// </summary>
		public const string PROPERTY_COMPANY_ID = "CompanyId";
		/// <summary>
		/// The CompanyActiveService's CompanyId parent property
		/// </summary>
		public const string PROPERTY_COMPANY_ID_PARENT = "CompanyIdParent";
		/// <summary>
		/// The CompanyActiveService's Instance property
		/// </summary>
		public const string PROPERTY_INSTANCE = "Instance";
		/// <summary>
		/// The CompanyActiveService's ServiceAddress property
		/// </summary>
		public const string PROPERTY_SERVICE_ADDRESS = "ServiceAddress";
		/// <summary>
		/// The CompanyActiveService's AssociatedProduct property
		/// </summary>
		public const string PROPERTY_ASSOCIATED_PRODUCT = "AssociatedProduct";
		/// <summary>
		/// The CompanyActiveService's Status property
		/// </summary>
		public const string PROPERTY_STATUS = "Status";
		/// <summary>
		/// The CompanyActiveService's Order property
		/// </summary>
		public const string PROPERTY_ORDER = "Order";
		/// <summary>
		/// The CompanyActiveService's Offer property
		/// </summary>
		public const string PROPERTY_OFFER = "Offer";
		/// <summary>
		/// The CompanyActiveService's Invalid property
		/// </summary>
		public const string PROPERTY_INVALID = "Invalid";
		/// <summary>
		/// The CompanyActiveService's TecnologyVoice property
		/// </summary>
		public const string PROPERTY_TECNOLOGY_VOICE = "TecnologyVoice";
		/// <summary>
		/// The CompanyActiveService's TecnologyAccess property
		/// </summary>
		public const string PROPERTY_TECNOLOGY_ACCESS = "TecnologyAccess";
		/// <summary>
		/// The CompanyActiveService's ChargeProfile property
		/// </summary>
		public const string PROPERTY_CHARGE_PROFILE = "ChargeProfile";
		/// <summary>
		/// The CompanyActiveService's CreatedId property
		/// </summary>
		public const string PROPERTY_CREATED_ID = "CreatedId";
		/// <summary>
		/// The CompanyActiveService's ActivationDate property
		/// </summary>
		public const string PROPERTY_ACTIVATION_DATE = "ActivationDate";
		/// <summary>
		/// The CompanyActiveService's Benefit property
		/// </summary>
		public const string PROPERTY_BENEFIT = "Benefit";
		/// <summary>
		/// The CompanyActiveService's Portability property
		/// </summary>
		public const string PROPERTY_PORTABILITY = "Portability";
		/// <summary>
		/// The CompanyActiveService's Rpon property
		/// </summary>
		public const string PROPERTY_RPON = "Rpon";
		/// <summary>
		/// The CompanyActiveService's Tv property
		/// </summary>
		public const string PROPERTY_TV = "Tv";
		/// <summary>
		/// The CompanyActiveService's Ficticious property
		/// </summary>
		public const string PROPERTY_FICTICIOUS = "Ficticious";
		/// <summary>
		/// The CompanyActiveService's DoubleAccess property
		/// </summary>
		public const string PROPERTY_DOUBLE_ACCESS = "DoubleAccess";
		/// <summary>
		/// The CompanyActiveService's Qtd property
		/// </summary>
		public const string PROPERTY_QTD = "Qtd";
		/// <summary>
		/// The CompanyActiveService's Product property
		/// </summary>
		public const string PROPERTY_PRODUCT = "Product";

        /// <summary>
		/// Represents the id database column
		/// </summary>
		[Key]
		[Column("id")]
		public int Id { get; set; }
		/// <summary>
		/// Represents the company_id database column
		/// </summary>
		[ForeignKey("CompanyIdParent")]
		[Column("company_id")]
		public int CompanyId { get; set; }
		/// <summary>
		/// Represents the instance database column
		/// </summary>
		[Column("instance")]
		public string Instance { get; set; }
		/// <summary>
		/// Represents the service_address database column
		/// </summary>
		[Column("service_address")]
		public string ServiceAddress { get; set; }
		/// <summary>
		/// Represents the associated_product database column
		/// </summary>
		[Column("associated_product")]
		public string AssociatedProduct { get; set; }
		/// <summary>
		/// Represents the status database column
		/// </summary>
		[Column("status")]
		public string Status { get; set; }
		/// <summary>
		/// Represents the order database column
		/// </summary>
		[Column("order")]
		public string Order { get; set; }
		/// <summary>
		/// Represents the offer database column
		/// </summary>
		[Column("offer")]
		public string Offer { get; set; }
		/// <summary>
		/// Represents the invalid database column
		/// </summary>
		[Column("invalid")]
		public string Invalid { get; set; }
		/// <summary>
		/// Represents the tecnology_voice database column
		/// </summary>
		[Column("tecnology_voice")]
		public string TecnologyVoice { get; set; }
		/// <summary>
		/// Represents the tecnology_access database column
		/// </summary>
		[Column("tecnology_access")]
		public string TecnologyAccess { get; set; }
		/// <summary>
		/// Represents the charge_profile database column
		/// </summary>
		[Column("charge_profile")]
		public string ChargeProfile { get; set; }
		/// <summary>
		/// Represents the created_id database column
		/// </summary>
		[Column("created_id")]
		public string CreatedId { get; set; }
		/// <summary>
		/// Represents the activation_date database column
		/// </summary>
		[Column("activation_date")]
		public string ActivationDate { get; set; }
		/// <summary>
		/// Represents the benefit database column
		/// </summary>
		[Column("benefit")]
		public string Benefit { get; set; }
		/// <summary>
		/// Represents the portability database column
		/// </summary>
		[Column("portability")]
		public string Portability { get; set; }
		/// <summary>
		/// Represents the rpon database column
		/// </summary>
		[Column("rpon")]
		public string Rpon { get; set; }
		/// <summary>
		/// Represents the tv database column
		/// </summary>
		[Column("tv")]
		public string Tv { get; set; }
		/// <summary>
		/// Represents the ficticious database column
		/// </summary>
		[Column("ficticious")]
		public string Ficticious { get; set; }
		/// <summary>
		/// Represents the double_access database column
		/// </summary>
		[Column("double_access")]
		public string DoubleAccess { get; set; }
		/// <summary>
		/// Represents the qtd database column
		/// </summary>
		[Column("qtd")]
		public string Qtd { get; set; }
		/// <summary>
		/// Represents the product database column
		/// </summary>
		[Column("product")]
		public string Product { get; set; }
		
		/// <summary>
		/// Represents the company_id foreign key object
		/// </summary>
		public Company CompanyIdParent { get; set; }

        /// <summary>
        /// A string representation of a single object
        /// </summary>
        public override string ToString()
        {
            return Summary;
        }
		
        // ABSTRACT IMPLEMENTATIONS
        /// <summary>
        /// Clears all navigation properties
        /// </summary>
        public override void ClearRelationalData()
        {
			CompanyIdParent = null;
        }
        /// <summary>
        /// Clones an object
        /// </summary>
        /// <returns>a new object with the same property values</returns>
        public override Crudable Clone()
        {
			var clone = new CompanyActiveService()
			{
				Id = Id,
				CompanyId = CompanyId,
				Instance = Instance,
				ServiceAddress = ServiceAddress,
				AssociatedProduct = AssociatedProduct,
				Status = Status,
				Order = Order,
				Offer = Offer,
				Invalid = Invalid,
				TecnologyVoice = TecnologyVoice,
				TecnologyAccess = TecnologyAccess,
				ChargeProfile = ChargeProfile,
				CreatedId = CreatedId,
				ActivationDate = ActivationDate,
				Benefit = Benefit,
				Portability = Portability,
				Rpon = Rpon,
				Tv = Tv,
				Ficticious = Ficticious,
				DoubleAccess = DoubleAccess,
				Qtd = Qtd,
				Product = Product,
			};
			
			clone.CompanyIdParent = CompanyIdParent;

            return clone;
        }
	}
}