﻿using Clericuzzi.WebApi;
using Clericuzzi.WebApi.Entities;

using System;
using System.Linq;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Collections.Generic;
using Clericuzzi.Lib.Http.Core.Crud;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using Clericuzzi.WebApi.Data.DatabaseContext;
using System.ComponentModel.DataAnnotations.Schema;

namespace RestApi.Entities
{
    [Table("blocked_tabulations")]
    public partial class BlockedTabulations : Crudable
    {
        // TABLE MAPPING CONSTANTS
        /// <summary>
        /// The database table's name that this class represents
        /// </summary>
        public const string TABLE_NAME = "blocked_tabulations";
		/// <summary>
		/// The Blocked Tabulations's class name
		/// </summary>
		public const string CLASS_NAME = "BlockedTabulations";
		/// <summary>
		/// The BlockedTabulations's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The BlockedTabulations's TabulationId property
		/// </summary>
		public const string PROPERTY_TABULATION_ID = "TabulationId";
		/// <summary>
		/// The BlockedTabulations's TabulationId parent property
		/// </summary>
		public const string PROPERTY_TABULATION_ID_PARENT = "TabulationIdParent";

        /// <summary>
		/// Represents the id database column
		/// </summary>
		[Key]
		[Column("id")]
		public int Id { get; set; }
		/// <summary>
		/// Represents the tabulation_id database column
		/// </summary>
		[ForeignKey("TabulationIdParent")]
		[Column("tabulation_id")]
		public int TabulationId { get; set; }
		
		/// <summary>
		/// Represents the tabulation_id foreign key object
		/// </summary>
		public Tabulation TabulationIdParent { get; set; }

        /// <summary>
        /// A string representation of a single object
        /// </summary>
        public override string ToString()
        {
            return Summary;
        }
		
        // ABSTRACT IMPLEMENTATIONS
        /// <summary>
        /// Clears all navigation properties
        /// </summary>
        public override void ClearRelationalData()
        {
			TabulationIdParent = null;
        }
        /// <summary>
        /// Clones an object
        /// </summary>
        /// <returns>a new object with the same property values</returns>
        public override Crudable Clone()
        {
			var clone = new BlockedTabulations()
			{
				Id = Id,
				TabulationId = TabulationId,
			};
			
			clone.TabulationIdParent = TabulationIdParent;

            return clone;
        }
	}
}