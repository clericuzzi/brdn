﻿using Clericuzzi.WebApi;
using Clericuzzi.WebApi.Entities;

using System;
using System.Linq;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Collections.Generic;
using Clericuzzi.Lib.Http.Core.Crud;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using Clericuzzi.WebApi.Data.DatabaseContext;
using System.ComponentModel.DataAnnotations.Schema;
using Clericuzzi.WebApi.Geo;

namespace RestApi.Entities
{
    [Table("mining_google")]
    public partial class MiningGoogle : Crudable
    {
        // TABLE MAPPING CONSTANTS
        /// <summary>
        /// The database table's name that this class represents
        /// </summary>
        public const string TABLE_NAME = "mining_google";
		/// <summary>
		/// The Mining Google's class name
		/// </summary>
		public const string CLASS_NAME = "MiningGoogle";
		/// <summary>
		/// The MiningGoogle's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The MiningGoogle's CityId property
		/// </summary>
		public const string PROPERTY_CITY_ID = "CityId";
		/// <summary>
		/// The MiningGoogle's CityId parent property
		/// </summary>
		public const string PROPERTY_CITY_ID_PARENT = "CityIdParent";
		/// <summary>
		/// The MiningGoogle's CompanySubtypeId property
		/// </summary>
		public const string PROPERTY_COMPANY_SUBTYPE_ID = "CompanySubtypeId";
		/// <summary>
		/// The MiningGoogle's CompanySubtypeId parent property
		/// </summary>
		public const string PROPERTY_COMPANY_SUBTYPE_ID_PARENT = "CompanySubtypeIdParent";
		/// <summary>
		/// The MiningGoogle's Priority property
		/// </summary>
		public const string PROPERTY_PRIORITY = "Priority";

        /// <summary>
		/// Represents the id database column
		/// </summary>
		[Key]
		[Column("id")]
		public int Id { get; set; }
		/// <summary>
		/// Represents the city_id database column
		/// </summary>
		[ForeignKey("CityIdParent")]
		[Column("city_id")]
		public int CityId { get; set; }
		/// <summary>
		/// Represents the company_subtype_id database column
		/// </summary>
		[ForeignKey("CompanySubtypeIdParent")]
		[Column("company_subtype_id")]
		public int CompanySubtypeId { get; set; }
		/// <summary>
		/// Represents the priority database column
		/// </summary>
		[Column("priority")]
		public int Priority { get; set; }
		
		/// <summary>
		/// Represents the city_id foreign key object
		/// </summary>
		public GeographyCity CityIdParent { get; set; }
		/// <summary>
		/// Represents the company_subtype_id foreign key object
		/// </summary>
		public CompanySubtype CompanySubtypeIdParent { get; set; }

        /// <summary>
        /// A string representation of a single object
        /// </summary>
        public override string ToString()
        {
            return Summary;
        }
		
        // ABSTRACT IMPLEMENTATIONS
        /// <summary>
        /// Clears all navigation properties
        /// </summary>
        public override void ClearRelationalData()
        {
			CityIdParent = null;
			CompanySubtypeIdParent = null;
        }
        /// <summary>
        /// Clones an object
        /// </summary>
        /// <returns>a new object with the same property values</returns>
        public override Crudable Clone()
        {
			var clone = new MiningGoogle()
			{
				Id = Id,
				CityId = CityId,
				CompanySubtypeId = CompanySubtypeId,
				Priority = Priority,
			};
			
			clone.CityIdParent = CityIdParent;
			clone.CompanySubtypeIdParent = CompanySubtypeIdParent;

            return clone;
        }
	}
}