﻿using Clericuzzi.WebApi;
using Clericuzzi.WebApi.Entities;

using System;
using System.Linq;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Collections.Generic;
using Clericuzzi.Lib.Http.Core.Crud;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using Clericuzzi.WebApi.Data.DatabaseContext;
using System.ComponentModel.DataAnnotations.Schema;

namespace RestApi.Entities
{
    [Table("company_call")]
    public partial class CompanyCall : Crudable
    {
        // TABLE MAPPING CONSTANTS
        /// <summary>
        /// The database table's name that this class represents
        /// </summary>
        public const string TABLE_NAME = "company_call";
		/// <summary>
		/// The Company Call's class name
		/// </summary>
		public const string CLASS_NAME = "CompanyCall";
		/// <summary>
		/// The CompanyCall's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The CompanyCall's UserId property
		/// </summary>
		public const string PROPERTY_USER_ID = "UserId";
		/// <summary>
		/// The CompanyCall's UserId parent property
		/// </summary>
		public const string PROPERTY_USER_ID_PARENT = "UserIdParent";
		/// <summary>
		/// The CompanyCall's CompanyId property
		/// </summary>
		public const string PROPERTY_COMPANY_ID = "CompanyId";
		/// <summary>
		/// The CompanyCall's CompanyId parent property
		/// </summary>
		public const string PROPERTY_COMPANY_ID_PARENT = "CompanyIdParent";
		/// <summary>
		/// The CompanyCall's ContactId property
		/// </summary>
		public const string PROPERTY_CONTACT_ID = "ContactId";
		/// <summary>
		/// The CompanyCall's ContactId parent property
		/// </summary>
		public const string PROPERTY_CONTACT_ID_PARENT = "ContactIdParent";
		/// <summary>
		/// The CompanyCall's TabulationId property
		/// </summary>
		public const string PROPERTY_TABULATION_ID = "TabulationId";
		/// <summary>
		/// The CompanyCall's TabulationId parent property
		/// </summary>
		public const string PROPERTY_TABULATION_ID_PARENT = "TabulationIdParent";
		/// <summary>
		/// The CompanyCall's StartedIn property
		/// </summary>
		public const string PROPERTY_STARTED_IN = "StartedIn";
		/// <summary>
		/// The CompanyCall's EndedIn property
		/// </summary>
		public const string PROPERTY_ENDED_IN = "EndedIn";

        /// <summary>
		/// Represents the id database column
		/// </summary>
		[Key]
		[Column("id")]
		public int Id { get; set; }
		/// <summary>
		/// Represents the user_id database column
		/// </summary>
		[ForeignKey("UserIdParent")]
		[Column("user_id")]
		public int UserId { get; set; }
		/// <summary>
		/// Represents the company_id database column
		/// </summary>
		[ForeignKey("CompanyIdParent")]
		[Column("company_id")]
		public int CompanyId { get; set; }
		/// <summary>
		/// Represents the contact_id database column
		/// </summary>
		[ForeignKey("ContactIdParent")]
		[Column("contact_id")]
		public int? ContactId { get; set; }
		/// <summary>
		/// Represents the tabulation_id database column
		/// </summary>
		[ForeignKey("TabulationIdParent")]
		[Column("tabulation_id")]
		public int? TabulationId { get; set; }
		/// <summary>
		/// Represents the started_in database column
		/// </summary>
		[Column("started_in")]
		public DateTime StartedIn { get; set; }
		/// <summary>
		/// Represents the ended_in database column
		/// </summary>
		[Column("ended_in")]
		public DateTime? EndedIn { get; set; }
		
		/// <summary>
		/// Represents the user_id foreign key object
		/// </summary>
		public User UserIdParent { get; set; }
		/// <summary>
		/// Represents the company_id foreign key object
		/// </summary>
		public Company CompanyIdParent { get; set; }
		/// <summary>
		/// Represents the contact_id foreign key object
		/// </summary>
		public CompanyContact ContactIdParent { get; set; }
		/// <summary>
		/// Represents the tabulation_id foreign key object
		/// </summary>
		public Tabulation TabulationIdParent { get; set; }

        /// <summary>
        /// A string representation of a single object
        /// </summary>
        public override string ToString()
        {
            return Summary;
        }
		
        // ABSTRACT IMPLEMENTATIONS
        /// <summary>
        /// Clears all navigation properties
        /// </summary>
        public override void ClearRelationalData()
        {
			UserIdParent = null;
			CompanyIdParent = null;
			ContactIdParent = null;
			TabulationIdParent = null;
        }
        /// <summary>
        /// Clones an object
        /// </summary>
        /// <returns>a new object with the same property values</returns>
        public override Crudable Clone()
        {
			var clone = new CompanyCall()
			{
				Id = Id,
				UserId = UserId,
				CompanyId = CompanyId,
				ContactId = ContactId,
				TabulationId = TabulationId,
				StartedIn = StartedIn,
				EndedIn = EndedIn,
			};
			
			clone.UserIdParent = UserIdParent;
			clone.CompanyIdParent = CompanyIdParent;
			clone.ContactIdParent = ContactIdParent;
			clone.TabulationIdParent = TabulationIdParent;

            return clone;
        }
	}
}