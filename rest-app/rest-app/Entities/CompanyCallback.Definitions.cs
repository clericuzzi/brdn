﻿using Clericuzzi.WebApi;
using Clericuzzi.WebApi.Entities;

using System;
using System.Linq;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Collections.Generic;
using Clericuzzi.Lib.Http.Core.Crud;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using Clericuzzi.WebApi.Data.DatabaseContext;
using System.ComponentModel.DataAnnotations.Schema;

namespace RestApi.Entities
{
    [Table("company_callback")]
    public partial class CompanyCallback : Crudable
    {
        // TABLE MAPPING CONSTANTS
        /// <summary>
        /// The database table's name that this class represents
        /// </summary>
        public const string TABLE_NAME = "company_callback";
		/// <summary>
		/// The Company Callback's class name
		/// </summary>
		public const string CLASS_NAME = "CompanyCallback";
		/// <summary>
		/// The CompanyCallback's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The CompanyCallback's UserId property
		/// </summary>
		public const string PROPERTY_USER_ID = "UserId";
		/// <summary>
		/// The CompanyCallback's UserId parent property
		/// </summary>
		public const string PROPERTY_USER_ID_PARENT = "UserIdParent";
		/// <summary>
		/// The CompanyCallback's CompanyId property
		/// </summary>
		public const string PROPERTY_COMPANY_ID = "CompanyId";
		/// <summary>
		/// The CompanyCallback's CompanyId parent property
		/// </summary>
		public const string PROPERTY_COMPANY_ID_PARENT = "CompanyIdParent";
		/// <summary>
		/// The CompanyCallback's ContactId property
		/// </summary>
		public const string PROPERTY_CONTACT_ID = "ContactId";
		/// <summary>
		/// The CompanyCallback's ContactId parent property
		/// </summary>
		public const string PROPERTY_CONTACT_ID_PARENT = "ContactIdParent";
		/// <summary>
		/// The CompanyCallback's CallTime property
		/// </summary>
		public const string PROPERTY_CALL_TIME = "CallTime";
		/// <summary>
		/// The CompanyCallback's Done property
		/// </summary>
		public const string PROPERTY_DONE = "Done";
		/// <summary>
		/// The CompanyCallback's Timestamp property
		/// </summary>
		public const string PROPERTY_TIMESTAMP = "Timestamp";

        /// <summary>
		/// Represents the id database column
		/// </summary>
		[Key]
		[Column("id")]
		public int Id { get; set; }
		/// <summary>
		/// Represents the user_id database column
		/// </summary>
		[ForeignKey("UserIdParent")]
		[Column("user_id")]
		public int UserId { get; set; }
		/// <summary>
		/// Represents the company_id database column
		/// </summary>
		[ForeignKey("CompanyIdParent")]
		[Column("company_id")]
		public int CompanyId { get; set; }
		/// <summary>
		/// Represents the contact_id database column
		/// </summary>
		[ForeignKey("ContactIdParent")]
		[Column("contact_id")]
		public int ContactId { get; set; }
		/// <summary>
		/// Represents the call_time database column
		/// </summary>
		[Column("call_time")]
		public DateTime CallTime { get; set; }
		/// <summary>
		/// Represents the done database column
		/// </summary>
		[Column("done")]
		public int Done { get; set; }
		/// <summary>
		/// Represents the timestamp database column
		/// </summary>
		[Column("timestamp")]
		public DateTime Timestamp { get; set; }
		
		/// <summary>
		/// Represents the user_id foreign key object
		/// </summary>
		public User UserIdParent { get; set; }
		/// <summary>
		/// Represents the company_id foreign key object
		/// </summary>
		public Company CompanyIdParent { get; set; }
		/// <summary>
		/// Represents the contact_id foreign key object
		/// </summary>
		public CompanyContact ContactIdParent { get; set; }

        /// <summary>
        /// A string representation of a single object
        /// </summary>
        public override string ToString()
        {
            return Summary;
        }
		
        // ABSTRACT IMPLEMENTATIONS
        /// <summary>
        /// Clears all navigation properties
        /// </summary>
        public override void ClearRelationalData()
        {
			UserIdParent = null;
			CompanyIdParent = null;
			ContactIdParent = null;
        }
        /// <summary>
        /// Clones an object
        /// </summary>
        /// <returns>a new object with the same property values</returns>
        public override Crudable Clone()
        {
			var clone = new CompanyCallback()
			{
				Id = Id,
				UserId = UserId,
				CompanyId = CompanyId,
				ContactId = ContactId,
				CallTime = CallTime,
				Done = Done,
				Timestamp = Timestamp,
			};
			
			clone.UserIdParent = UserIdParent;
			clone.CompanyIdParent = CompanyIdParent;
			clone.ContactIdParent = ContactIdParent;

            return clone;
        }
	}
}