﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Clericuzzi.Lib.Utils.Core;
using System.Collections.Generic;
using Clericuzzi.WebApi.Data.DatabaseContext;

namespace RestApi.Entities
{
    /// <summary>
    /// Filter defaults for the 'CompanyAvailability' class
    /// </summary>
    public partial class CompanyAvailabilityFilter
	{
		/// <summary>
		/// the 'Id' propery
		/// </summary>
		public int? Id { get; set; }
		/// <summary>
		/// the 'CompanyAddressId' propery
		/// </summary>
		public List<int> CompanyAddressId { get; set; }
		/// <summary>
		/// the 'Message' propery
		/// </summary>
		public string Message { get; set; }
		/// <summary>
		/// the 'ClosetDistance' propery
		/// </summary>
		public int? ClosetDistance { get; set; }
		/// <summary>
		/// the 'Lines' propery
		/// </summary>
		public int? Lines { get; set; }
		/// <summary>
		/// the 'Until20' propery
		/// </summary>
		public int? Until20 { get; set; }
		/// <summary>
		/// the 'From21UpTo50' propery
		/// </summary>
		public int? From21UpTo50 { get; set; }
		/// <summary>
		/// the 'Over50' propery
		/// </summary>
		public int? Over50 { get; set; }
		/// <summary>
		/// the 'Network' propery
		/// </summary>
		public string Network { get; set; }
		/// <summary>
		/// the 'Tecnology' propery
		/// </summary>
		public string Tecnology { get; set; }
		/// <summary>
		/// the 'ClosetTopSpeed' propery
		/// </summary>
		public int? ClosetTopSpeed { get; set; }
		/// <summary>
		/// the 'TecnologyForTv' propery
		/// </summary>
		public string TecnologyForTv { get; set; }
		/// <summary>
		/// the 'Timestamp' propery
		/// </summary>
		public DateTime? Timestamp { get; set; }
	}

    /// <summary>
    /// Class that maps a database table
    /// </summary>
    public partial class CompanyAvailability
    {
        /// <summary>
        /// The sumary tries to identify a single object
        /// </summary>
		public override string Summary => $"{CompanyAddressIdParent?.Summary ?? CompanyAddressId.ToString()}".RemoveDoubleSpaces().Trim();

        /// <summary>
        /// Default constructor
        /// </summary>
		public CompanyAvailability()
        {
			this.Timestamp = DateTime.Now;
        }

		/// <summary>
        /// Loads all the external references to this type
        /// </summary>
        /// <param name="context">database connection</param>
		public async Task Load(DataContext context)
		{
			await Task.Delay(150);
			// await this.LoadReferences(context);
		}

        /// <summary>
        /// Defines the equality rule for this Type
        /// </summary>
        /// <param name="value">an object to be compared to</param>
        /// <returns>a flag that indicates if a given object is "Equals" to this instance</returns>
        public override bool Equals(object value)
        {
			CompanyAvailability item = value as CompanyAvailability;

			return !Object.ReferenceEquals(null, item)
				&& int.Equals(Id, item.Id);
        }
        /// <summary>
        /// Default implementation of get hashCode
        /// </summary>
        /// <returns>a hash key wanna be</returns>
        public override int GetHashCode()
        {
            unchecked
			{
				// Choose large primes to avoid hashing collisions
				const int HashingBase = (int) 2166136261;
				const int HashingMultiplier = 16777619;

				int hash = HashingBase;
				hash = (hash * HashingMultiplier) ^ (!int.ReferenceEquals(null, Id) ? Id.GetHashCode() : 0);

				return hash;
			}
        }
    }
}