﻿using Clericuzzi.WebApi;
using Clericuzzi.WebApi.Entities;

using System;
using System.Linq;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Collections.Generic;
using Clericuzzi.Lib.Http.Core.Crud;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using Clericuzzi.WebApi.Data.DatabaseContext;
using System.ComponentModel.DataAnnotations.Schema;

namespace RestApi.Entities
{
    [Table("company_contact")]
    public partial class CompanyContact : Crudable
    {
        // TABLE MAPPING CONSTANTS
        /// <summary>
        /// The database table's name that this class represents
        /// </summary>
        public const string TABLE_NAME = "company_contact";
		/// <summary>
		/// The Company Contact's class name
		/// </summary>
		public const string CLASS_NAME = "CompanyContact";
		/// <summary>
		/// The CompanyContact's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The CompanyContact's UserId property
		/// </summary>
		public const string PROPERTY_USER_ID = "UserId";
		/// <summary>
		/// The CompanyContact's UserId parent property
		/// </summary>
		public const string PROPERTY_USER_ID_PARENT = "UserIdParent";
		/// <summary>
		/// The CompanyContact's CompanyId property
		/// </summary>
		public const string PROPERTY_COMPANY_ID = "CompanyId";
		/// <summary>
		/// The CompanyContact's CompanyId parent property
		/// </summary>
		public const string PROPERTY_COMPANY_ID_PARENT = "CompanyIdParent";
		/// <summary>
		/// The CompanyContact's PositionId property
		/// </summary>
		public const string PROPERTY_POSITION_ID = "PositionId";
		/// <summary>
		/// The CompanyContact's PositionId parent property
		/// </summary>
		public const string PROPERTY_POSITION_ID_PARENT = "PositionIdParent";
		/// <summary>
		/// The CompanyContact's DepartmentId property
		/// </summary>
		public const string PROPERTY_DEPARTMENT_ID = "DepartmentId";
		/// <summary>
		/// The CompanyContact's DepartmentId parent property
		/// </summary>
		public const string PROPERTY_DEPARTMENT_ID_PARENT = "DepartmentIdParent";
		/// <summary>
		/// The CompanyContact's Name property
		/// </summary>
		public const string PROPERTY_NAME = "Name";
		/// <summary>
		/// The CompanyContact's Email property
		/// </summary>
		public const string PROPERTY_EMAIL = "Email";
		/// <summary>
		/// The CompanyContact's Timestamp property
		/// </summary>
		public const string PROPERTY_TIMESTAMP = "Timestamp";

        /// <summary>
		/// Represents the id database column
		/// </summary>
		[Key]
		[Column("id")]
		public int Id { get; set; }
		/// <summary>
		/// Represents the user_id database column
		/// </summary>
		[ForeignKey("UserIdParent")]
		[Column("user_id")]
		public int UserId { get; set; }
		/// <summary>
		/// Represents the company_id database column
		/// </summary>
		[ForeignKey("CompanyIdParent")]
		[Column("company_id")]
		public int CompanyId { get; set; }
		/// <summary>
		/// Represents the position_id database column
		/// </summary>
		[ForeignKey("PositionIdParent")]
		[Column("position_id")]
		public int PositionId { get; set; }
		/// <summary>
		/// Represents the department_id database column
		/// </summary>
		[ForeignKey("DepartmentIdParent")]
		[Column("department_id")]
		public int DepartmentId { get; set; }
		/// <summary>
		/// Represents the name database column
		/// </summary>
		[Column("name")]
		public string Name { get; set; }
		/// <summary>
		/// Represents the email database column
		/// </summary>
		[Column("email")]
		public string Email { get; set; }
		/// <summary>
		/// Represents the timestamp database column
		/// </summary>
		[Column("timestamp")]
		public DateTime Timestamp { get; set; }
		
		/// <summary>
		/// Represents the user_id foreign key object
		/// </summary>
		public User UserIdParent { get; set; }
		/// <summary>
		/// Represents the company_id foreign key object
		/// </summary>
		public Company CompanyIdParent { get; set; }
		/// <summary>
		/// Represents the position_id foreign key object
		/// </summary>
		public ContactPosition PositionIdParent { get; set; }
		/// <summary>
		/// Represents the department_id foreign key object
		/// </summary>
		public ContactDepartment DepartmentIdParent { get; set; }

        /// <summary>
        /// A string representation of a single object
        /// </summary>
        public override string ToString()
        {
            return Summary;
        }
		
        // ABSTRACT IMPLEMENTATIONS
        /// <summary>
        /// Clears all navigation properties
        /// </summary>
        public override void ClearRelationalData()
        {
			UserIdParent = null;
			CompanyIdParent = null;
			PositionIdParent = null;
			DepartmentIdParent = null;
        }
        /// <summary>
        /// Clones an object
        /// </summary>
        /// <returns>a new object with the same property values</returns>
        public override Crudable Clone()
        {
			var clone = new CompanyContact()
			{
				Id = Id,
				UserId = UserId,
				CompanyId = CompanyId,
				PositionId = PositionId,
				DepartmentId = DepartmentId,
				Name = Name,
				Email = Email,
				Timestamp = Timestamp,
			};
			
			clone.UserIdParent = UserIdParent;
			clone.CompanyIdParent = CompanyIdParent;
			clone.PositionIdParent = PositionIdParent;
			clone.DepartmentIdParent = DepartmentIdParent;

            return clone;
        }
	}
}