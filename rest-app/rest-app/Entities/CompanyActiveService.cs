﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Clericuzzi.Lib.Utils.Core;
using System.Collections.Generic;
using Clericuzzi.WebApi.Data.DatabaseContext;

namespace RestApi.Entities
{
    /// <summary>
    /// Filter defaults for the 'CompanyActiveService' class
    /// </summary>
    public partial class CompanyActiveServiceFilter
	{
		/// <summary>
		/// the 'Id' propery
		/// </summary>
		public int? Id { get; set; }
		/// <summary>
		/// the 'CompanyId' propery
		/// </summary>
		public List<int> CompanyId { get; set; }
		/// <summary>
		/// the 'Instance' propery
		/// </summary>
		public string Instance { get; set; }
		/// <summary>
		/// the 'ServiceAddress' propery
		/// </summary>
		public string ServiceAddress { get; set; }
		/// <summary>
		/// the 'AssociatedProduct' propery
		/// </summary>
		public string AssociatedProduct { get; set; }
		/// <summary>
		/// the 'Status' propery
		/// </summary>
		public string Status { get; set; }
		/// <summary>
		/// the 'Order' propery
		/// </summary>
		public string Order { get; set; }
		/// <summary>
		/// the 'Offer' propery
		/// </summary>
		public string Offer { get; set; }
		/// <summary>
		/// the 'Invalid' propery
		/// </summary>
		public string Invalid { get; set; }
		/// <summary>
		/// the 'TecnologyVoice' propery
		/// </summary>
		public string TecnologyVoice { get; set; }
		/// <summary>
		/// the 'TecnologyAccess' propery
		/// </summary>
		public string TecnologyAccess { get; set; }
		/// <summary>
		/// the 'ChargeProfile' propery
		/// </summary>
		public string ChargeProfile { get; set; }
		/// <summary>
		/// the 'CreatedId' propery
		/// </summary>
		public string CreatedId { get; set; }
		/// <summary>
		/// the 'ActivationDate' propery
		/// </summary>
		public string ActivationDate { get; set; }
		/// <summary>
		/// the 'Benefit' propery
		/// </summary>
		public string Benefit { get; set; }
		/// <summary>
		/// the 'Portability' propery
		/// </summary>
		public string Portability { get; set; }
		/// <summary>
		/// the 'Rpon' propery
		/// </summary>
		public string Rpon { get; set; }
		/// <summary>
		/// the 'Tv' propery
		/// </summary>
		public string Tv { get; set; }
		/// <summary>
		/// the 'Ficticious' propery
		/// </summary>
		public string Ficticious { get; set; }
		/// <summary>
		/// the 'DoubleAccess' propery
		/// </summary>
		public string DoubleAccess { get; set; }
		/// <summary>
		/// the 'Qtd' propery
		/// </summary>
		public string Qtd { get; set; }
		/// <summary>
		/// the 'Product' propery
		/// </summary>
		public string Product { get; set; }
	}

    /// <summary>
    /// Class that maps a database table
    /// </summary>
    public partial class CompanyActiveService
    {
        /// <summary>
        /// The sumary tries to identify a single object
        /// </summary>
		public override string Summary => $"{CompanyIdParent?.Summary ?? CompanyId.ToString()}".RemoveDoubleSpaces().Trim();

        /// <summary>
        /// Default constructor
        /// </summary>
		public CompanyActiveService()
        {
        }

		/// <summary>
        /// Loads all the external references to this type
        /// </summary>
        /// <param name="context">database connection</param>
		public async Task Load(DataContext context)
		{
			await Task.Delay(150);
			// await this.LoadReferences(context);
		}

        /// <summary>
        /// Defines the equality rule for this Type
        /// </summary>
        /// <param name="value">an object to be compared to</param>
        /// <returns>a flag that indicates if a given object is "Equals" to this instance</returns>
        public override bool Equals(object value)
        {
			CompanyActiveService item = value as CompanyActiveService;

			return !Object.ReferenceEquals(null, item)
				&& int.Equals(Id, item.Id);
        }
        /// <summary>
        /// Default implementation of get hashCode
        /// </summary>
        /// <returns>a hash key wanna be</returns>
        public override int GetHashCode()
        {
            unchecked
			{
				// Choose large primes to avoid hashing collisions
				const int HashingBase = (int) 2166136261;
				const int HashingMultiplier = 16777619;

				int hash = HashingBase;
				hash = (hash * HashingMultiplier) ^ (!int.ReferenceEquals(null, Id) ? Id.GetHashCode() : 0);

				return hash;
			}
        }
    }
}