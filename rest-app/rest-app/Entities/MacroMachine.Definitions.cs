﻿using Clericuzzi.WebApi;
using Clericuzzi.WebApi.Entities;

using System;
using System.Linq;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Collections.Generic;
using Clericuzzi.Lib.Http.Core.Crud;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using Clericuzzi.WebApi.Data.DatabaseContext;
using System.ComponentModel.DataAnnotations.Schema;

namespace RestApi.Entities
{
    [Table("macro_machine")]
    public partial class MacroMachine : Crudable
    {
        // TABLE MAPPING CONSTANTS
        /// <summary>
        /// The database table's name that this class represents
        /// </summary>
        public const string TABLE_NAME = "macro_machine";
		/// <summary>
		/// The Máquina de mineração's class name
		/// </summary>
		public const string CLASS_NAME = "MacroMachine";
		/// <summary>
		/// The MacroMachine's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The MacroMachine's MachineName property
		/// </summary>
		public const string PROPERTY_MACHINE_NAME = "MachineName";
		/// <summary>
		/// The MacroMachine's IsAlive property
		/// </summary>
		public const string PROPERTY_IS_ALIVE = "IsAlive";
		/// <summary>
		/// The MacroMachine's Updated property
		/// </summary>
		public const string PROPERTY_UPDATED = "Updated";

        /// <summary>
		/// Represents the id database column
		/// </summary>
		[Key]
		[Column("id")]
		public int Id { get; set; }
		/// <summary>
		/// Represents the machine_name database column
		/// </summary>
		[Column("machine_name")]
		public string MachineName { get; set; }
		/// <summary>
		/// Represents the is_alive database column
		/// </summary>
		[Column("is_alive")]
		public int IsAlive { get; set; }
		/// <summary>
		/// Represents the updated database column
		/// </summary>
		[Column("updated")]
		public int Updated { get; set; }
		
        /// <summary>
        /// A string representation of a single object
        /// </summary>
        public override string ToString()
        {
            return Summary;
        }
		
        // ABSTRACT IMPLEMENTATIONS
        /// <summary>
        /// Clears all navigation properties
        /// </summary>
        public override void ClearRelationalData()
        {
        }
        /// <summary>
        /// Clones an object
        /// </summary>
        /// <returns>a new object with the same property values</returns>
        public override Crudable Clone()
        {
			var clone = new MacroMachine()
			{
				Id = Id,
				MachineName = MachineName,
				IsAlive = IsAlive,
				Updated = Updated,
			};
			
            return clone;
        }
	}
}