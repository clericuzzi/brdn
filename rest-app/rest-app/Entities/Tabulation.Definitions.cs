﻿using Clericuzzi.WebApi;
using Clericuzzi.WebApi.Entities;

using System;
using System.Linq;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Collections.Generic;
using Clericuzzi.Lib.Http.Core.Crud;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using Clericuzzi.WebApi.Data.DatabaseContext;
using System.ComponentModel.DataAnnotations.Schema;

namespace RestApi.Entities
{
    [Table("tabulation")]
    public partial class Tabulation : Crudable
    {
        // TABLE MAPPING CONSTANTS
        /// <summary>
        /// The database table's name that this class represents
        /// </summary>
        public const string TABLE_NAME = "tabulation";
		/// <summary>
		/// The Tabulation's class name
		/// </summary>
		public const string CLASS_NAME = "Tabulation";
		/// <summary>
		/// The Tabulation's Id property
		/// </summary>
		public const string PROPERTY_ID = "Id";
		/// <summary>
		/// The Tabulation's TabulationTypeId property
		/// </summary>
		public const string PROPERTY_TABULATION_TYPE_ID = "TabulationTypeId";
		/// <summary>
		/// The Tabulation's TabulationTypeId parent property
		/// </summary>
		public const string PROPERTY_TABULATION_TYPE_ID_PARENT = "TabulationTypeIdParent";
		/// <summary>
		/// The Tabulation's Text property
		/// </summary>
		public const string PROPERTY_TEXT = "Text";

        /// <summary>
		/// Represents the id database column
		/// </summary>
		[Key]
		[Column("id")]
		public int Id { get; set; }
		/// <summary>
		/// Represents the tabulation_type_id database column
		/// </summary>
		[ForeignKey("TabulationTypeIdParent")]
		[Column("tabulation_type_id")]
		public int TabulationTypeId { get; set; }
		/// <summary>
		/// Represents the text database column
		/// </summary>
		[Column("text")]
		public string Text { get; set; }
		
		/// <summary>
		/// Represents the tabulation_type_id foreign key object
		/// </summary>
		public TabulationType TabulationTypeIdParent { get; set; }

        /// <summary>
        /// A string representation of a single object
        /// </summary>
        public override string ToString()
        {
            return Summary;
        }
		
        // ABSTRACT IMPLEMENTATIONS
        /// <summary>
        /// Clears all navigation properties
        /// </summary>
        public override void ClearRelationalData()
        {
			TabulationTypeIdParent = null;
        }
        /// <summary>
        /// Clones an object
        /// </summary>
        /// <returns>a new object with the same property values</returns>
        public override Crudable Clone()
        {
			var clone = new Tabulation()
			{
				Id = Id,
				TabulationTypeId = TabulationTypeId,
				Text = Text,
			};
			
			clone.TabulationTypeIdParent = TabulationTypeIdParent;

            return clone;
        }
	}
}