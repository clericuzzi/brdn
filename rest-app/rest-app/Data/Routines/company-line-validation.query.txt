insert into `company_line` select null, `id`, `ddd`, `phone`, case when substring(`phone`, 1, 1) in ('9', '8', '7') then 1 else 0 end, null, null, null from `company` where `id` not in (select `company_id` from `company_line`) and length(ifnull(`ddd`, '')) > 0 and length(ifnull(`phone`, '')) > 0;
delete from `company_line` where length(ifnull(`ddd`, '')) = 0 or length(ifnull(`phone`, '')) = 0;
delete from `company_line` where `ddd` like '0%' or `phone` like '0%';
