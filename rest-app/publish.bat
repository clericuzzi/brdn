rd /s /q "[output]"

cd rest-app
dotnet restore
dotnet build
dotnet publish -o "..\[output]" -c release

cd ..
cd "[output]"
del *.pdb

cd ..
git add "[output]" -f