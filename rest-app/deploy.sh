git config --global credential.helper cache
git config --global credential.helper 'cache --timeout=3600'

cd /home/ubuntu/git-repo/brdn/
sudo git pull

sudo systemctl stop kestrel-rest-brdn.service
sudo systemctl disable kestrel-rest-brdn.service
sudo rm -rf /etc/systemd/system/kestrel-rest-brdn.service
sudo rm -rf /var/aspnetcore-apps
cd /var
sudo mkdir aspnetcore-apps
cd aspnetcore-apps/
sudo mkdir rest-brdn
cd rest-brdn/
sudo mkdir content
sudo cp -rf /home/ubuntu/git-repo/brdn/rest-app/\[output\]/. /var/aspnetcore-apps/rest-brdn
sudo cp -rf /home/ubuntu/git-repo/brdn/rest-app/kestrel-rest-brdn.service /etc/systemd/system/kestrel-rest-brdn.service
sudo systemctl enable kestrel-rest-brdn.service
sudo systemctl start kestrel-rest-brdn.service
sudo systemctl status kestrel-rest-brdn.service

sudo chmod 777 /var/aspnetcore-apps/rest-brdn
sudo chmod 777 /var/aspnetcore-apps/rest-brdn/content/

sudo ln -sfn /home/ubuntu/git-repo/brdn/rest-app/deploy.sh /usr/local/bin/deploy-brdn
